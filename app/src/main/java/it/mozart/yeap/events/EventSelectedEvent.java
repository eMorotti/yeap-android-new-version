package it.mozart.yeap.events;

public class EventSelectedEvent {

    private String id;

    public EventSelectedEvent(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
