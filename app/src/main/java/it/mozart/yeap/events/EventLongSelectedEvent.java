package it.mozart.yeap.events;

import android.view.View;

public class EventLongSelectedEvent {

    private String id;
    private String title;
    private View view;

    public EventLongSelectedEvent(String id, String title, View view) {
        this.id = id;
        this.title = title;
        this.view = view;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public View getView() {
        return view;
    }
}
