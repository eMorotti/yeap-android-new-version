package it.mozart.yeap.events;

public class MessaggioChatActionEvent {

    private boolean open;

    public MessaggioChatActionEvent(boolean open) {
        this.open = open;
    }

    public boolean isOpen() {
        return open;
    }
}
