package it.mozart.yeap.events;

import android.view.View;

public class AvatarSelectedEvent {

    private String path;
    private View view;
    private boolean event;

    public AvatarSelectedEvent(String path, View view, boolean event) {
        this.path = path;
        this.view = view;
        this.event = event;
    }

    public String getPath() {
        return path;
    }

    public View getView() {
        return view;
    }

    public boolean isEvent() {
        return event;
    }
}
