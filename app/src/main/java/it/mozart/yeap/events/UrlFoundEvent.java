package it.mozart.yeap.events;

import com.schinizer.rxunfurl.model.PreviewData;

public class UrlFoundEvent {

    private String id;
    private String url;
    private PreviewData previewData;

    public UrlFoundEvent(String id, String url, PreviewData previewData) {
        this.id = id;
        this.url = url;
        this.previewData = previewData;
    }

    public String getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public PreviewData getPreviewData() {
        return previewData;
    }
}
