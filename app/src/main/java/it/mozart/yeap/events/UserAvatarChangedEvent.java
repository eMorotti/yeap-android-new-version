package it.mozart.yeap.events;

public class UserAvatarChangedEvent {

    private String uuid;

    public UserAvatarChangedEvent(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }
}
