package it.mozart.yeap.events;

import android.view.View;

public class UserImageSelectedEvent {

    private String id;
    private View view;

    public UserImageSelectedEvent(String id, View view) {
        this.id = id;
        this.view = view;
    }

    public String getId() {
        return id;
    }

    public View getView() {
        return view;
    }
}
