package it.mozart.yeap.events;

import android.view.View;

public class ImageSelectedEvent {

    private String path;
    private String caption;
    private View view;
    private boolean url;
    private boolean event;

    public ImageSelectedEvent(String path, View view) {
        this.path = path;
        this.view = view;
        this.url = false;
    }

    public ImageSelectedEvent(String path, View view, boolean url) {
        this.path = path;
        this.view = view;
        this.url = url;
    }

    public ImageSelectedEvent(String path, View view, boolean url, boolean event) {
        this.path = path;
        this.view = view;
        this.url = url;
        this.event = event;
    }

    public ImageSelectedEvent(String path, String caption, View view) {
        this.path = path;
        this.caption = caption;
        this.view = view;
        this.url = false;
    }

    public String getPath() {
        return path;
    }

    public String getCaption() {
        return caption;
    }

    public View getView() {
        return view;
    }

    public boolean isUrl() {
        return url;
    }

    public boolean isEvent() {
        return event;
    }
}
