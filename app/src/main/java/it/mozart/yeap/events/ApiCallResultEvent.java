package it.mozart.yeap.events;

public class ApiCallResultEvent {

    private String requestId;

    public ApiCallResultEvent(String requestId) {
        this.requestId = requestId;
    }

    public String getRequestId() {
        return requestId;
    }
}
