package it.mozart.yeap.events;

public class GroupSelectedEvent {

    private String id;

    public GroupSelectedEvent(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
