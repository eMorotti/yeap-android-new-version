package it.mozart.yeap.events;

public class StartUrlFindEvent {

    private String messageId;
    private String url;

    public StartUrlFindEvent(String messageId, String url) {
        this.messageId = messageId;
        this.url = url;
    }

    public String getMessageId() {
        return messageId;
    }

    public String getUrl() {
        return url;
    }
}
