package it.mozart.yeap.events;

public class StartImageDownloadEvent {

    private String idMessaggio;
    private boolean ignoraControllo;

    public StartImageDownloadEvent(String idMessaggio, boolean ignoraControllo) {
        this.idMessaggio = idMessaggio;
        this.ignoraControllo = ignoraControllo;
    }

    public String getIdMessaggio() {
        return idMessaggio;
    }

    public boolean isIgnoraControllo() {
        return ignoraControllo;
    }
}
