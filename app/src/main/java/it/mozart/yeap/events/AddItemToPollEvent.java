package it.mozart.yeap.events;

public class AddItemToPollEvent {

    private String sondaggioId;
    private boolean isSpecial;
    private boolean isWhere;

    public AddItemToPollEvent(String sondaggioId) {
        this.sondaggioId = sondaggioId;
    }

    public AddItemToPollEvent(String sondaggioId, boolean isSpecial, boolean isWhere) {
        this.sondaggioId = sondaggioId;
        this.isSpecial = isSpecial;
        this.isWhere = isWhere;
    }

    public String getSondaggioId() {
        return sondaggioId;
    }

    public boolean isSpecial() {
        return isSpecial;
    }

    public boolean isWhere() {
        return isWhere;
    }
}
