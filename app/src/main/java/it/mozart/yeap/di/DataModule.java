package it.mozart.yeap.di;

import android.content.Context;

import org.codejargon.feather.Provides;

import javax.inject.Singleton;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.helpers.CreationModelHelper;

public class DataModule {

    @Singleton
    @Provides
    public Realm provideRealm(Context context) {
        return App.getRealm();
    }

    @Singleton
    @Provides
    public CreationModelHelper provideCreatorHelper() {
        return new CreationModelHelper();
    }
}
