package it.mozart.yeap.di;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.codejargon.feather.Provides;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import it.mozart.yeap.api.services.ContactApi;
import it.mozart.yeap.api.services.EventApi;
import it.mozart.yeap.api.services.GroupApi;
import it.mozart.yeap.api.services.PollApi;
import it.mozart.yeap.api.services.ProfileApi;
import it.mozart.yeap.api.services.UploadApi;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.utility.Utils;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.schedulers.Schedulers;

public class NetworkModule {

    @Singleton
    @Provides
    public OkHttpClient provideOkHttp() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .addInterceptor(loggingInterceptor)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request originalRequest = chain.request();

                        String accessToken = AccountProvider.getInstance().getAccessToken();
                        if (accessToken != null) {
                            Request authorisedRequest = originalRequest.newBuilder()
                                    .header("x-access-token", accessToken)
                                    .build();
                            return chain.proceed(authorisedRequest);
                        }
                        return chain.proceed(originalRequest);
                    }
                })
                .build();
    }

    @Singleton
    @Provides
    public Retrofit provideRetrofit(OkHttpClient okHttpClient) {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy'-'MM'-'dd' 'HH':'mm':'ss")
                .create();

        return new Retrofit.Builder()
                .baseUrl(Utils.getApiURLSecure() + "/")
                .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build();
    }

    @Provides
    public EventApi eventService(Retrofit retrofit) {
        return retrofit.create(EventApi.class);
    }

    @Provides
    public GroupApi groupService(Retrofit retrofit) {
        return retrofit.create(GroupApi.class);
    }

    @Provides
    public UploadApi uploadService(Retrofit retrofit) {
        return retrofit.create(UploadApi.class);
    }

    @Provides
    public ProfileApi profileService(Retrofit retrofit) {
        return retrofit.create(ProfileApi.class);
    }

    @Provides
    public PollApi pollService(Retrofit retrofit) {
        return retrofit.create(PollApi.class);
    }

    @Provides
    public ContactApi contactService(Retrofit retrofit) {
        return retrofit.create(ContactApi.class);
    }
}
