package it.mozart.yeap.di;

import android.content.Context;

import org.codejargon.feather.Provides;

import javax.inject.Singleton;

import it.mozart.yeap.App;
import it.mozart.yeap.helpers.HomeScrollHelper;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.services.syncAdapter.contacts.ContactsManager;

public class AppModule {

    @Provides
    public Context provideContext() {
        return App.getContext();
    }

    @Provides
    public ContactsManager provideContactsManager() {
        return new ContactsManager();
    }

    @Singleton
    @Provides
    public NotificationsHelper provideSyncUnitNotifications() {
        return new NotificationsHelper();
    }

    @Singleton
    @Provides
    public HomeScrollHelper provideHomeHelper() {
        return new HomeScrollHelper();
    }
}
