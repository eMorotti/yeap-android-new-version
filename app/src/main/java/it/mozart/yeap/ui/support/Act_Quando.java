package it.mozart.yeap.ui.support;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TimePicker;
import android.widget.Toast;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.parceler.Parcels;

import java.util.UUID;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.api.models.APIError;
import it.mozart.yeap.api.models.SondaggioVoceApiModel;
import it.mozart.yeap.api.services.PollApi;
import it.mozart.yeap.common.QuandoInfo;
import it.mozart.yeap.events.ApiCallResultEvent;
import it.mozart.yeap.realm.PollItem;
import it.mozart.yeap.ui.base.BaseActivityWithSyncService;
import it.mozart.yeap.utility.APIErrorUtils;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.DateUtils;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class Act_Quando extends BaseActivityWithSyncService {

    @BindView(R.id.data_da)
    Button mDataDa;
    @BindView(R.id.data_da_remove)
    ImageButton mDataDaRemove;
    @BindView(R.id.data_da_error)
    ImageButton mDataDaError;
    @BindView(R.id.ora_da)
    Button mOraDa;
    @BindView(R.id.ora_da_remove)
    ImageButton mOraDaRemove;
    @BindView(R.id.ora_da_error)
    ImageButton mOraDaError;
    @BindView(R.id.data_a)
    Button mDataA;
    @BindView(R.id.data_a_remove)
    ImageButton mDataARemove;
    @BindView(R.id.data_a_error)
    ImageButton mDataAError;
    @BindView(R.id.ora_a)
    Button mOraA;
    @BindView(R.id.ora_a_remove)
    ImageButton mOraARemove;
    @BindView(R.id.ora_a_error)
    ImageButton mOraAError;

    @BindView(R.id.conferma)
    Button mConferma;

    @OnClick(R.id.data_da)
    void dataDaCLicked() {
        showDatePicker(mDataDa, true);
    }

    @OnClick(R.id.data_da_remove)
    void dataDaRemoveCLicked() {
        mDataDaRemove.setVisibility(View.GONE);
        mModel.setDataDa(null);
        mDataDa.setText(null);
        checkDateCompliance();
    }

    @OnClick(R.id.ora_da)
    void oraDaCLicked() {
        showTimePicker(mOraDa, true);
    }

    @OnClick(R.id.ora_da_remove)
    void oraDaRemoveCLicked() {
        mOraDaRemove.setVisibility(View.GONE);
        mModel.setOraDa(null);
        mOraDa.setText(null);
        checkDateCompliance();
    }

    @OnClick(R.id.data_a)
    void dataACLicked() {
        showDatePicker(mDataA, false);
    }

    @OnClick(R.id.data_a_remove)
    void dataARemoveCLicked() {
        mDataARemove.setVisibility(View.GONE);
        mModel.setDataA(null);
        mDataA.setText(null);
        checkDateCompliance();
    }

    @OnClick(R.id.ora_a)
    void oraACLicked() {
        showTimePicker(mOraA, false);
    }

    @OnClick(R.id.ora_a_remove)
    void oraARemoveCLicked() {
        mOraARemove.setVisibility(View.GONE);
        mModel.setOraA(null);
        mOraA.setText(null);
        checkDateCompliance();
    }

    @OnClick(R.id.conferma)
    void confermaClicked() {
        if (mOraAError.getVisibility() == View.VISIBLE || mDataAError.getVisibility() == View.VISIBLE) {
            Toast.makeText(this, getString(R.string.sysyem_quando_error_message), Toast.LENGTH_SHORT).show();
        } else {
            if (mFromPoll) {
                crea();
            } else {
                Intent intent = new Intent();
                intent.putExtra(Constants.INTENT_QUANDO, Parcels.wrap(mModel));
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }

    // SUPPORT

    @Inject
    PollApi pollApi;

    private QuandoInfo mModel;
    private String mTitle;
    private String mSubtitle;
    private String mEventId;
    private String mPollId;
    private boolean mFromPoll;

    private static final String PARAM_EVENT_ID = "paramEventId";
    private static final String PARAM_POLL_ID = "paramPollId";
    private static final String PARAM_TITLE = "paramTitle";
    private static final String PARAM_SUBTITLE = "paramSubtitle";
    private static final String PARAM_QUANDO = "paramQuando";
    private static final String PARAM_FROM_POLL = "paramFromPoll";

    // SYSTEM

    public static Intent newIntent(String title, String subtitle, QuandoInfo quandoInfo) {
        Intent intent = new Intent(App.getContext(), Act_Quando.class);
        intent.putExtra(PARAM_TITLE, title);
        intent.putExtra(PARAM_SUBTITLE, subtitle);
        intent.putExtra(PARAM_QUANDO, Parcels.wrap(quandoInfo));
        intent.putExtra(PARAM_FROM_POLL, false);
        return intent;
    }

    public static Intent newIntentFromPoll(String eventId, String pollId, String title, String subtitle) {
        Intent intent = new Intent(App.getContext(), Act_Quando.class);
        intent.putExtra(PARAM_EVENT_ID, eventId);
        intent.putExtra(PARAM_POLL_ID, pollId);
        intent.putExtra(PARAM_TITLE, title);
        intent.putExtra(PARAM_SUBTITLE, subtitle);
        intent.putExtra(PARAM_FROM_POLL, true);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUseBus(true);
        setToolbarTitle(mTitle);
        setToolbarSubtitle(mSubtitle);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_quando;
    }

    @Override
    protected void initVariables() {

    }

    @Override
    protected void loadParameters(Bundle extras) {
        mEventId = extras.getString(PARAM_EVENT_ID);
        mPollId = extras.getString(PARAM_POLL_ID);
        mTitle = extras.getString(PARAM_TITLE, "");
        mSubtitle = extras.getString(PARAM_SUBTITLE, null);
        if (extras.containsKey(PARAM_QUANDO)) {
            mModel = Parcels.unwrap(extras.getParcelable(PARAM_QUANDO));
        }
        mFromPoll = extras.getBoolean(PARAM_FROM_POLL);
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    protected void initialize() {
        // Init
        if (mModel == null) {
            mModel = new QuandoInfo();

            mConferma.setVisibility(View.GONE);
        } else {
            if (mModel.getDataDa() != null && mModel.getOraDa() != null) {
                mDataDa.setText(DateUtils.formatDate(mModel.getDataDa(), "dd MMM yyyy"));
                mOraDa.setText(DateUtils.formatDate(mModel.getOraDa(), "HH:mm"));
                mDataDaRemove.setVisibility(View.VISIBLE);
                mOraDaRemove.setVisibility(View.VISIBLE);
            }
            if (mModel.getDataA() != null && mModel.getOraA() != null) {
                mDataA.setText(DateUtils.formatDate(mModel.getDataA(), "dd MMM yyyy"));
                mOraA.setText(DateUtils.formatDate(mModel.getOraA(), "HH:mm"));
                mDataARemove.setVisibility(View.VISIBLE);
                mOraARemove.setVisibility(View.VISIBLE);
            }

            mConferma.setVisibility(mModel.isSomeInfoPresent() ? View.VISIBLE : View.GONE);
        }
        // Listener per visualizzare o nascondere il tasto conferma
        mModel.setListener(new QuandoInfo.OnInfoChange() {
            @Override
            public void infoChanged(boolean infoPresent) {
                mConferma.setVisibility(infoPresent ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ApiCallResultEvent event) {
        if (event.getRequestId().equals(getRequestId())) {
            cancelTimer();
            finish();
            overridePendingTransition(0, R.anim.slide_out_bottom_slow);
        }
    }

    // FUNCTIONS

    private void showDatePicker(final Button button, final boolean isDa) {
        DateTime date;
        try {
            date = DateTimeFormat.forPattern("dd MMM yyyy").parseDateTime(button.getText().toString());
        } catch (Exception ex) {
            date = DateTime.now();
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                if (view.isShown()) {
                    DateTime newDate = new DateTime(year, monthOfYear + 1, dayOfMonth, 0, 0);
                    button.setText(DateUtils.formatDate(newDate, "dd MMM yyyy"));
                    if (isDa) {
                        mModel.setDataDa(newDate.toDate());
                        mDataDaRemove.setVisibility(View.VISIBLE);

                        if (mOraDa.getText().length() == 0) {
                            DateTime dateTime = DateTime.now();
                            dateTime.withDate(newDate.toLocalDate());
                            mModel.setOraDa(dateTime.toDate());
                            mOraDa.setText(DateUtils.formatDate(dateTime, "HH:mm"));
                            mOraDaRemove.setVisibility(View.VISIBLE);
                        }
                    } else {
                        mModel.setDataA(newDate.toDate());
                        mDataARemove.setVisibility(View.VISIBLE);

                        if (mOraA.getText().length() == 0) {
                            DateTime dateTime = DateTime.now();
                            dateTime.withDate(newDate.toLocalDate());
                            mModel.setOraA(dateTime.toDate());
                            mOraA.setText(DateUtils.formatDate(dateTime, "HH:mm"));
                            mOraARemove.setVisibility(View.VISIBLE);
                        }
                    }
                    checkDateCompliance();
                }
            }
        }, date.getYear(), date.getMonthOfYear() - 1, date.getDayOfMonth());
        if (mModel.getDataDa() != null && !isDa) {
            datePickerDialog.getDatePicker().setMinDate(mModel.getDataDa().getTime());
        } else if (isDa && mModel.getDataA() != null) {
            datePickerDialog.getDatePicker().setMaxDate(mModel.getDataA().getTime());
        }
        datePickerDialog.setTitle(getString(R.string.system_quando_choose_date));
        datePickerDialog.show();
    }

    private void showTimePicker(final Button button, final boolean isDa) {
        DateTime time;
        try {
            time = DateTimeFormat.forPattern("HH:mm").parseDateTime(button.getText().toString());
        } catch (Exception ex) {
            time = DateTime.now();
        }

        final TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                if (view.isShown()) {
                    DateTime newTime = new DateTime();
                    newTime = newTime.withHourOfDay(hourOfDay);
                    newTime = newTime.withMinuteOfHour(minute);
                    button.setText(DateUtils.formatDate(newTime, "HH:mm"));
                    if (isDa) {
                        mModel.setOraDa(newTime.toDate());
                        mOraDaRemove.setVisibility(View.VISIBLE);
                    } else {
                        mModel.setOraA(newTime.toDate());
                        mOraARemove.setVisibility(View.VISIBLE);
                    }
                    checkDateCompliance();
                }
            }
        }, time.getHourOfDay(), time.getMinuteOfHour(), true);
        timePickerDialog.setTitle(getString(R.string.system_quando_choose_time));
        timePickerDialog.show();
    }

    private void checkDateCompliance() {
        mOraDaError.setVisibility(View.GONE);
        mDataDaError.setVisibility(View.GONE);
        mOraAError.setVisibility(View.GONE);
        mDataAError.setVisibility(View.GONE);

        if (mModel.getDataA() == null || mModel.getDataDa() == null) {
            return;
        }

        if (mModel.getDataDa().getTime() / 86400000 > mModel.getDataA().getTime() / 86400000) {
            mDataDaError.setVisibility(View.VISIBLE);
            mDataAError.setVisibility(View.VISIBLE);
        }

        if (mModel.getDataDa().getTime() / 86400000 < mModel.getDataA().getTime() / 86400000) {
            return;
        }

        if (mModel.getOraDa() == null || mModel.getOraA() == null) {
            return;
        }

        if (Integer.parseInt(DateUtils.formatDate(mModel.getOraDa(), "HHmm")) > Integer.parseInt(DateUtils.formatDate(mModel.getOraA(), "HHmm"))) {
            mOraDaError.setVisibility(View.VISIBLE);
            mOraAError.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Creo la voce sondaggio
     */
    private void crea() {
        getDynamicView().showLoading();
        setRequestId(UUID.randomUUID().toString());
        registerSubscription(pollApi.createPollItem(getRequestId(), mEventId, mPollId, new SondaggioVoceApiModel(mModel))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<PollItem>() {
                    @Override
                    public void call(PollItem voce) {
                        startTimer();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        getDynamicView().hide();

                        APIError error = APIErrorUtils.parseError(Act_Quando.this, throwable);
                        Toast.makeText(Act_Quando.this, error.message(), Toast.LENGTH_SHORT).show();
                    }
                })
        );
    }
}
