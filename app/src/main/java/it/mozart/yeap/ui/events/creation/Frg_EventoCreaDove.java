package it.mozart.yeap.ui.events.creation;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.parceler.Parcels;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import it.mozart.yeap.R;
import it.mozart.yeap.common.DoveInfo;
import it.mozart.yeap.helpers.AnalitycsProvider;
import it.mozart.yeap.helpers.CreationModelHelper;
import it.mozart.yeap.realm.PollItem;
import it.mozart.yeap.ui.base.BaseFragment;
import it.mozart.yeap.ui.main.MainCreationControl;
import it.mozart.yeap.ui.support.Act_Dove;
import it.mozart.yeap.ui.views.AnimCheckBox;
import it.mozart.yeap.ui.views.InterceptLinearLayout;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.Utils;

public class Frg_EventoCreaDove extends BaseFragment implements MainCreationControl {

    @BindView(R.id.dove_layout)
    InterceptLinearLayout mDoveLayout;
    @BindView(R.id.dove)
    TextView mDove;
    @BindView(R.id.delete)
    ImageButton mDoveDelete;
    @BindView(R.id.dove_switch)
    ImageButton mDoveSwitch;
    @BindView(R.id.dove_sondaggio_layout)
    InterceptLinearLayout mDoveSondaggioLayout;
    @BindView(R.id.poll_layout)
    ViewGroup mPollLayout;
    @BindView(R.id.perm_items_check)
    AnimCheckBox mPermItems;

    @OnClick(R.id.dove)
    void doveClicked() {
        mSelectedView = mDove;
        doveSelected(creationModelHelper.getEvent().getEventInfo().getWhere() != null);
    }

    @OnClick(R.id.delete)
    void doveDeleteClicked() {
        mDove.setText(null);
        mDoveDelete.setVisibility(View.GONE);
        creationModelHelper.getEvent().getEventInfo().updateDoveInfo(null);
    }

    @OnClick(R.id.dove_switch)
    void doveSwitchClicked() {
        switchClicked(!mIsSondaggio);
    }

    @OnClick(R.id.dove_layout)
    void doveLayoutClicked() {
        switchClicked(false);
    }

    @OnClick(R.id.dove_sondaggio_layout)
    void doveSondaggioLayoutClicked() {
        switchClicked(true);
    }

    @OnClick({R.id.perm_items, R.id.perm_items_check})
    void permInvClicked() {
        mPermItems.setChecked(!mPermItems.isChecked());
        creationModelHelper.getWherePoll().setUsersCanAddItems(mPermItems.isChecked());
    }

    @OnClick(R.id.layout)
    void layoutClicked() {
        Utils.hideKeyboard(getActivity());
    }

    // SUPPORT

    @Inject
    CreationModelHelper creationModelHelper;

    private TextView mSelectedView;
    private boolean mIsSondaggio;

    private static final int DOVE_CODE = 3002;

    // SYSTEM

    @Override
    protected int getLayoutId() {
        return R.layout.frg_evento_crea_dove;
    }

    @Override
    protected void initValues() {

    }

    @Override
    protected void loadParameters(Bundle arguments) {

    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {
    }

    @Override
    protected void saveInfos(Bundle outState) {
    }

    @Override
    protected void initialize() {
        mIsSondaggio = creationModelHelper.isWherePollSelected();

        DoveInfo doveInfo = creationModelHelper.getEvent().getEventInfo().generateDoveModel();
        mDove.setText(doveInfo != null ? doveInfo.getDove() : null);
        if (mDove.getText().length() != 0) {
            mDoveDelete.setVisibility(View.VISIBLE);
        } else {
            mDoveDelete.setVisibility(View.GONE);
        }

        mPermItems.setChecked(creationModelHelper.getWherePoll().isUsersCanAddItems(), false);

        switchClicked(mIsSondaggio);
        initPoll();
    }

    @Override
    public void onStart() {
        super.onStart();
        // Aggiorno il titolo, il sottotitolo e l'icona della toolbar dell'activity
        if (getActivity() instanceof Act_EventoCrea) {
            ((Act_EventoCrea) getActivity()).setToolbarTitleAndIconFromCrea(creationModelHelper.getEvent().getTitle(), R.drawable.ic_arrow_back_white_24dp);
            ((Act_EventoCrea) getActivity()).setToolbarSubtitleFromCrea(getString(R.string.nuova_evento_crea_info_dove_subtitle));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case DOVE_CODE:
                    doveManage((DoveInfo) Parcels.unwrap(data.getParcelableExtra(Constants.INTENT_DOVE)));
                    break;
            }
        }
    }

    @Override
    public boolean canGoOn() {
        if (mIsSondaggio) {
            return true;
        } else {
            if (creationModelHelper.getEvent().getEventInfo().getWhere() != null) {
                return true;
            }
        }

        if (!mIsSondaggio) {
            Toast.makeText(getContext(), getString(R.string.evento_crea_dove_mancante), Toast.LENGTH_SHORT).show();
        }

        return false;
    }

    // FUNCTIONS

    /**
     * Crea il titolo per l'activity Dove
     *
     * @return titolo
     */
    private String createDoveTitle() {
        return getString(R.string.nuova_evento_dove_title);
    }

    /**
     * Crea il sottotitolo per l'activity Dove
     *
     * @return sottotitolo
     */
    private String createDoveSubtitle() {
        return getString(R.string.nuova_evento_dove_subtitle);
    }

    private void doveSelected(boolean alreadyPresent) {
        // Apro subito l'activity se il model è null
        if (!alreadyPresent) {
            AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "setActivityWhere", "creation");
            startActivityForResult(Act_Dove.newIntent(createDoveTitle(), createDoveSubtitle(), null), DOVE_CODE);
        }
    }

    private void doveManage(DoveInfo doveInfo) {
        AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "setActivityWhereConfirm", "creation");
        // Aggiorno layout
        if (doveInfo != null && doveInfo.getDove() != null) {
            mSelectedView.setText(doveInfo.getDove());
        } else {
            mSelectedView.setText(null);
            doveInfo = null;
        }

        if (mIsSondaggio) {
            int position = Integer.parseInt(mSelectedView.getTag().toString());
            List<PollItem> list = creationModelHelper.getWherePoll().getPollItems();
            if (list.size() >= position) {
                list.get(position - 1).updateDoveInfo(doveInfo);
            } else {
                PollItem pollItem = new PollItem();
                pollItem.updateDoveInfo(doveInfo);
                creationModelHelper.getWherePoll().getPollItems().add(pollItem);
                updateVoceView(mPollLayout.getChildAt(position - 1), pollItem, position);

                View view = LayoutInflater.from(getContext()).inflate(R.layout.item_evento_crea_voce_sondaggio, mPollLayout, false);
                updateVoceView(view, null, mPollLayout.getChildCount() + 1);
                mPollLayout.addView(view);
            }
        } else {
            creationModelHelper.getEvent().getEventInfo().updateDoveInfo(doveInfo);
            mDoveDelete.setVisibility(View.VISIBLE);
        }
    }

    private void switchClicked(boolean isSondaggio) {
        if (isSondaggio) {
            mDoveLayout.setInterceptAll(true);
            mDoveLayout.setAlpha(0.3f);
            mDoveSondaggioLayout.setInterceptAll(false);
            mDoveSondaggioLayout.setAlpha(1f);

            mDoveSwitch.setBackgroundResource(R.drawable.circle_white);
            mDoveSwitch.setColorFilter(Color.BLACK);
        } else {
            mDoveLayout.setInterceptAll(false);
            mDoveLayout.setAlpha(1f);
            mDoveSondaggioLayout.setInterceptAll(true);
            mDoveSondaggioLayout.setAlpha(0.3f);

            mDoveSwitch.setBackgroundResource(R.drawable.circle_transparent_white);
            mDoveSwitch.setColorFilter(Color.WHITE);
        }
        mIsSondaggio = isSondaggio;
        creationModelHelper.setWherePollSelected(mIsSondaggio);
    }

    private void initPoll() {
        mPollLayout.removeAllViews();
        int i = 1;
        for (PollItem item : creationModelHelper.getWherePoll().getPollItems()) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.item_evento_crea_voce_sondaggio, mPollLayout, false);
            updateVoceView(view, item, i);
            mPollLayout.addView(view);
            i++;
        }

        View view = LayoutInflater.from(getContext()).inflate(R.layout.item_evento_crea_voce_sondaggio, mPollLayout, false);
        updateVoceView(view, null, i);
        mPollLayout.addView(view);
    }

    private void updateVoceView(@NonNull View view, final PollItem voce, final int position) {
        final TextView dove = view.findViewById(R.id.item);
        ImageButton delete = view.findViewById(R.id.delete);

        dove.setHint(getString(R.string.nuova_evento_crea_dettaglio_dove_hint));
        if (voce != null) {
            dove.setText(voce.getWhere());
            delete.setVisibility(View.VISIBLE);
        } else {
            delete.setVisibility(View.GONE);
        }

        dove.setTag(String.valueOf(position));
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPollLayout.getChildCount() == 1) {
                    updateVoceView(mPollLayout.getChildAt(0), null, 1);
                } else {
                    mPollLayout.removeViewAt(position - 1);
                    creationModelHelper.getWherePoll().getPollItems().remove(position - 1);
                    for (int i = position - 1; i < mPollLayout.getChildCount(); i++) {
                        if (creationModelHelper.getWherePoll().getPollItems().size() > i) {
                            updateVoceView(mPollLayout.getChildAt(i), creationModelHelper.getWherePoll().getPollItems().get(i), i + 1);
                        } else {
                            updateVoceView(mPollLayout.getChildAt(i), null, i + 1);
                        }
                    }
                }
            }
        });

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectedView = dove;
                doveSelected(voce != null);
            }
        });
    }
}
