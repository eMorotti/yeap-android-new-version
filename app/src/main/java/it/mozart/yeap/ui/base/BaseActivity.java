package it.mozart.yeap.ui.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.ui.views.DynamicView;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.Prefs;
import it.mozart.yeap.utility.Utils;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public abstract class BaseActivity extends AppCompatActivity {

    protected FirebaseAnalytics mFirebaseAnalytics;

    private String mRequestId;

    protected Toolbar mToolbar;
    protected ViewGroup mToolbarSpecial;
    protected Bundle mSavedInstanceState;

    private CountDownTimer mCountDownTimer;
    private DynamicView mDynamicView;
    private boolean mIgnoreToolbar = false;
    private boolean mMantainBus = false;
    private boolean mUseBus = false;
    private boolean mSavedInfoPresent = false;
    private boolean mIgnoreUpdateApp = false;

    private CompositeSubscription mCompositeSubscription;
    private List<Subscription> subscriptions;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ButterKnife.bind(this);
        App.feather().injectFields(this);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        mCompositeSubscription = new CompositeSubscription();
        if (subscriptions == null) {
            subscriptions = new ArrayList<>();
        }

        initVariables();

        if (getIntent().getExtras() != null) {
            loadParameters(getIntent().getExtras());
        }

        mSavedInstanceState = savedInstanceState;
        mSavedInfoPresent = false;
        if (savedInstanceState != null) {
            mSavedInfoPresent = true;
            loadInfos(savedInstanceState);
        }

        boolean hasToolbar = true;
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolbar == null)
            hasToolbar = false;

        if (hasToolbar) {
            setSupportActionBar(mToolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayShowTitleEnabled(true);
            }
            if (!mIgnoreToolbar) {
                mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
                mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
            }
        }
        mToolbarSpecial = findViewById(R.id.toolbar_special);
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        FrameLayout root = new FrameLayout(this);
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        root.addView(inflater.inflate(layoutResID, null));
        mDynamicView = new DynamicView(this);
        root.addView(mDynamicView);

        super.setContentView(root);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        initialize();
    }

    protected abstract int getLayoutId();

    protected abstract void initVariables();

    protected abstract void loadParameters(Bundle extras);

    protected abstract void loadInfos(Bundle savedInstanceState);

    protected abstract void saveInfos(Bundle outState);

    // Il presenter è pronto per l'utilizzo
    protected abstract void initialize();

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        saveInfos(outState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            if (Prefs.getString(Constants.UPDATE_APP, "").equals(packageInfo.versionName)) {
                if (!mIgnoreUpdateApp) {
                    showUpdateAppDialog();
                }
            } else {
                Prefs.remove(Constants.UPDATE_APP);
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mUseBus && !EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        if (!mIgnoreUpdateApp) {
            registerReceiver(updateAppVersionReceiver, new IntentFilter("it.mozart.yeap.UPDATE_APP"));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mUseBus && !mMantainBus) {
            EventBus.getDefault().unregister(this);
        }
        if (!mIgnoreUpdateApp) {
            try {
                unregisterReceiver(updateAppVersionReceiver);
            } catch (Exception ignored) {
                // TODO capire perchè capita
            }
        }
        Utils.hideKeyboard(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mUseBus && mMantainBus) {
            EventBus.getDefault().unregister(this);
        }
        unsubscribeSubscriptions();
        mSavedInstanceState = null;
    }

    protected void registerSubscription(Subscription subscription) {
        mCompositeSubscription.add(subscription);
    }

    private void unsubscribeSubscriptions() {
        mCompositeSubscription.unsubscribe();
        for (int i = 0; i < subscriptions.size(); i++) {
            subscriptions.get(i).unsubscribe();
        }
    }

    protected View getRootView() {
        if (findViewById(R.id.coordinator) != null)
            return findViewById(R.id.coordinator);
        return findViewById(android.R.id.content);
    }

    // EventBus viene deregistrato sull' onPause
    protected void setUseBus(boolean useBus) {
        this.mUseBus = useBus;
    }

    // EventBus viene deregistrato sull' onDestroy
    protected void setMantainBus(boolean mantainBus) {
        this.mMantainBus = mantainBus;
        this.mUseBus = mantainBus;
    }

    // Evito il settaggio iniziale della toolbar (da usare prima del super())
    protected void setIgnoreToolbar(boolean ignoreToolbar) {
        this.mIgnoreToolbar = ignoreToolbar;
    }

    protected void setIgnoreUpdateApp(boolean ignoreUpdateApp) {
        this.mIgnoreUpdateApp = ignoreUpdateApp;
    }

    protected boolean isSavedInfoPresent() {
        return mSavedInfoPresent;
    }

    // Imposta titolo del layout speciale della toolbar
    protected void setToolbarTitle(String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        if (mToolbarSpecial != null) {
            ((TextView) mToolbarSpecial.findViewById(R.id.toolbar_title)).setText(title);
        }
    }

    // Imposta sottotitolo del layout speciale della toolbar
    protected void setToolbarSubtitle(String subtitle) {
        if (mToolbarSpecial != null) {
            TextView subtitleText = mToolbarSpecial.findViewById(R.id.toolbar_subtitle);
            subtitleText.setText(subtitle);
            if (subtitle != null) {
                subtitleText.setVisibility(View.VISIBLE);
            } else {
                subtitleText.setVisibility(View.GONE);
            }
        }
    }

    // Recupero imageview della toolbar
    protected ImageView getToolbarIcon() {
        ViewGroup toolbarSpecial = (ViewGroup) findViewById(R.id.toolbar_special);
        return toolbarSpecial.findViewById(R.id.toolbar_avatar);
    }

    protected void changeToolbarIconVisibility(int visibility) {
        ViewGroup toolbarSpecial = (ViewGroup) findViewById(R.id.toolbar_special);
        toolbarSpecial.findViewById(R.id.toolbar_avatar_container).setVisibility(visibility);
    }

    // Recupero title della toolbar
    protected TextView getToolbarTitle() {
        ViewGroup toolbarSpecial = (ViewGroup) findViewById(R.id.toolbar_special);
        return toolbarSpecial.findViewById(R.id.toolbar_title);
    }

    // Recupero status della toolbar
    protected TextView getToolbarStatus() {
        ViewGroup toolbarSpecial = (ViewGroup) findViewById(R.id.toolbar_special);
        return toolbarSpecial.findViewById(R.id.toolbar_status);
    }

    // Recupero sottotitolo della toolbar
    protected TextView getToolbarSubtitle() {
        ViewGroup toolbarSpecial = (ViewGroup) findViewById(R.id.toolbar_special);
        return toolbarSpecial.findViewById(R.id.toolbar_subtitle);
    }

    // Recupero layout contenente titolo e sottotitolo della toolbar
    protected View getToolbarLayout() {
        ViewGroup toolbarSpecial = (ViewGroup) findViewById(R.id.toolbar_special);
        return toolbarSpecial.findViewById(R.id.toolbar_layout);
    }

    public DynamicView getDynamicView() {
        return mDynamicView;
    }

    protected void startTimer() {
        mCountDownTimer = new CountDownTimer(15000, 15000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                onFinishTimer();
            }
        }.start();
    }

    protected void onFinishTimer() {
        mRequestId = "";
        Toast.makeText(this, getString(R.string.general_error_socket_timeout_for_api), Toast.LENGTH_SHORT).show();
        getDynamicView().hide();
    }

    protected void cancelTimer() {
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }
    }

    protected void setRequestId(String requestId) {
        mRequestId = requestId;
    }

    protected String getRequestId() {
        return mRequestId;
    }

    public void showToolbarSpecial(boolean ignoreAvatar) {
        mToolbarSpecial.findViewById(R.id.toolbar_title).setVisibility(View.VISIBLE);
        mToolbarSpecial.findViewById(R.id.toolbar_subtitle).setVisibility(View.VISIBLE);
        if (!ignoreAvatar) {
            mToolbarSpecial.findViewById(R.id.toolbar_avatar_container).setVisibility(View.VISIBLE);
        }
    }

    public void hideToolbarSpecial(boolean ignoreAvatar) {
        mToolbarSpecial.findViewById(R.id.toolbar_title).setVisibility(View.GONE);
        mToolbarSpecial.findViewById(R.id.toolbar_subtitle).setVisibility(View.GONE);
        if (!ignoreAvatar) {
            mToolbarSpecial.findViewById(R.id.toolbar_avatar_container).setVisibility(View.GONE);
        }
    }

    private final BroadcastReceiver updateAppVersionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            showUpdateAppDialog();
        }
    };

    private void showUpdateAppDialog() {
        new MaterialDialog.Builder(this)
                .title(getString(R.string.system_update_app_title))
                .content(getString(R.string.system_update_app_content))
                .positiveText(getString(R.string.system_update_app_update))
                .negativeText(getString(R.string.system_update_app_close))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        String appPackageName = getPackageName();
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                        finishAffinity();
                    }
                })
                .autoDismiss(false)
                .cancelable(false)
                .show();
    }
}