package it.mozart.yeap.ui.signin;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.parceler.Parcels;

import java.util.HashSet;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.api.models.APIError;
import it.mozart.yeap.api.services.EventApi;
import it.mozart.yeap.api.services.GroupApi;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.Group;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.GroupDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.services.sync.SyncTotal;
import it.mozart.yeap.services.syncAdapter.contacts.ContactSyncJob;
import it.mozart.yeap.services.syncAdapter.events.ContactsSyncDoneEvent;
import it.mozart.yeap.services.syncAdapter.events.ContactsSyncFailedEvent;
import it.mozart.yeap.ui.base.BaseActivity;
import it.mozart.yeap.ui.home.Act_Home;
import it.mozart.yeap.utility.APIErrorUtils;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.DateUtils;
import it.mozart.yeap.utility.Prefs;
import it.mozart.yeap.utility.Utils;
import it.mozart.yeap.utility.backup.GoogleDriveBackup;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class Act_Sync extends BaseActivity {

    @BindView(R.id.progress)
    ProgressBar mProgress;
    @BindView(R.id.failed)
    TextView mFailed;
    @BindView(R.id.retry)
    Button mRetry;

    @OnClick(R.id.retry)
    void retryClicked() {
        hideRetry();
        if (!mIgnoreRestore) {
            new MaterialDialog.Builder(this)
                    .title(getString(R.string.backup_restore_popup_title))
                    .content(getString(R.string.backup_restore_popup_content))
                    .positiveText(getString(R.string.backup_restore_popup_positive))
                    .negativeText(getString(R.string.backup_restore_popup_negative))
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            mGoogleDriveBackup.start();
                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            syncWithoutRestore();
                        }
                    })
                    .cancelable(false)
                    .show();
        } else {
            syncWithoutRestore();
        }
    }

    @Inject
    EventApi eventApi;
    @Inject
    GroupApi groupApi;

    private GoogleDriveBackup mGoogleDriveBackup;
    private User mUtente;
    private static HashSet<String> mEventIds;
    private static HashSet<String> mEventIdsTemp;
    private static HashSet<String> mGroupIds;
    private static HashSet<String> mGroupIdsTemp;
    private boolean mFromBackup;
    private boolean mIgnoreRestore;

    private static final String PARAM_UTENTE = "paramUtente";
    private static final String PARAM_RESTORE = "paramRestore";

    // SYSTEM

    public static Intent newIntent(User utente, boolean ignoreRestore) {
        Intent intent = new Intent(App.getContext(), Act_Sync.class);
        intent.putExtra(PARAM_UTENTE, Parcels.wrap(utente));
        intent.putExtra(PARAM_RESTORE, ignoreRestore);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUseBus(true);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_sync;
    }

    @Override
    protected void initVariables() {
        mEventIds = new HashSet<>();
        mGroupIds = new HashSet<>();
    }

    @Override
    protected void loadParameters(Bundle extras) {
        mUtente = Parcels.unwrap(extras.getParcelable(PARAM_UTENTE));
        mIgnoreRestore = extras.getBoolean(PARAM_RESTORE, false);
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (!mIgnoreRestore) {
            mGoogleDriveBackup.stop();
        }
    }

    @Override
    protected void initialize() {
        if (!mIgnoreRestore) {
            mGoogleDriveBackup = new GoogleDriveBackup();
            if (mGoogleDriveBackup.isGooglePlayServicesAvailable(this, true)) {
                mGoogleDriveBackup.init(this);
                mGoogleDriveBackup.setListener(new GoogleDriveBackup.DriveConnectionListener() {
                    @Override
                    public void connected() {
                        sync();
                    }

                    @Override
                    public void error() {
                        syncWithoutRestore();
                    }

                    @Override
                    public void onBackupSuccess(String testo) {

                    }

                    @Override
                    public void onBackupError(String error) {

                    }

                    @Override
                    public void onRestoreSuccess(String testo, long timestamp) {
                        if (timestamp != 0) {
                            Prefs.putString(Constants.BACKUP_DATE, DateUtils.formatDate(timestamp, "dd/MM/yyyy, HH:mm"));
                        }

                        App.initRealm(mUtente.getUuid());
                        App.initFeather();

                        try {
                            createAccount();
                            App.jobManager().addJobInBackground(new ContactSyncJob(false));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        mFromBackup = true;
                    }

                    @Override
                    public void onRestoreError(String error) {
                        syncWithoutRestore();
                    }

                    @Override
                    public void onRestoreNoBackup() {
                        syncWithoutRestore();
                    }
                });
                showDialog();
            } else {
                mIgnoreRestore = true;
                syncWithoutRestore();
            }
        } else {
            syncWithoutRestore();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case GoogleDriveBackup.REQUEST_RESOLVE_ERROR:
                if (resultCode == RESULT_OK) {
                    mGoogleDriveBackup.start();
                } else {
                    showDialog();
                }
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ContactsSyncDoneEvent event) {
        syncDb();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ContactsSyncFailedEvent event) {
        showRetry();
        Toast.makeText(this, getString(R.string.general_error_message), Toast.LENGTH_LONG).show();
    }

    // FUNCTIONS

    private void showDialog() {
        new MaterialDialog.Builder(this)
                .title(getString(R.string.backup_restore_popup_title))
                .content(getString(R.string.backup_restore_popup_content))
                .positiveText(getString(R.string.backup_restore_popup_positive))
                .negativeText(getString(R.string.backup_restore_popup_negative))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        mGoogleDriveBackup.start();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        syncWithoutRestore();
                    }
                })
                .cancelable(false)
                .show();
    }

    /**
     * Sync con backup
     */
    private void sync() {
        if (!Utils.isNetworkAvailable(this)) {
            showRetry();
            Toast.makeText(this, getString(R.string.general_error_internet), Toast.LENGTH_LONG).show();
            return;
        }

        mProgress.setVisibility(View.VISIBLE);
        mGoogleDriveBackup.restoreRealm();
    }

    /**
     * Sync senza backup
     */
    private void syncWithoutRestore() {
        if (!Utils.isNetworkAvailable(this)) {
            showRetry();
            Toast.makeText(this, getString(R.string.general_error_internet), Toast.LENGTH_LONG).show();
            return;
        }

        mProgress.setVisibility(View.VISIBLE);

        App.initRealm(mUtente.getUuid());
        App.initFeather();

        try {
            createAccount();
            App.jobManager().addJobInBackground(new ContactSyncJob(false));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        mFromBackup = false;
    }

    /**
     * Creazione utente "IO"
     *
     * @throws Exception eccezione
     */
    private void createAccount() throws Exception {
        Realm realm = App.getRealm();
        //noinspection TryFinallyCanBeTryWithResources
        try {
            // Controllo se già presente
            final User myself = new UserDao(realm).getUser(mUtente.getUuid());
            if (myself != null) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(@NonNull Realm realm) {
                        myself.setNickname(mUtente.getNickname());
                        myself.setPhone(mUtente.getPhone());
                        myself.setMe(true);
                        myself.setInYeap(true);
                    }
                });
            } else {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(@NonNull Realm realm) {
                        User newMyself = realm.createObject(User.class, mUtente.getUuid());
                        newMyself.setPhone(mUtente.getPhone());
                        newMyself.setUuid(mUtente.getUuid());
                        newMyself.setNickname(mUtente.getNickname());
                        newMyself.setMe(true);
                        newMyself.setInYeap(true);
                    }
                });
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            realm.close();
        }
    }

    /**
     * Sync db
     */
    private void syncDb() {
        Realm realm = App.getRealm();
        mEventIds.clear();
        mGroupIds.clear();
        //noinspection TryFinallyCanBeTryWithResources
        try {
            for (Event event : realm.where(Event.class).findAll()) {
                mEventIds.add(event.getUuid());
            }
//            for (Group group : realm.where(Group.class).findAll()) {
//                mGroupIds.add(group.getUuid());
//            }
        } finally {
            realm.close();
        }
        registerSubscription(syncEventi()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Void>() {
                    @Override
                    public void call(Void aVoid) {
                        Prefs.putBoolean(Constants.LOGIN_COMPLETED, true);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(Act_Sync.this, Act_Home.class));
                                overridePendingTransition(0, 0);
                                finish();
                            }
                        }, 1000);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();

                        showRetry();
                        Toast.makeText(Act_Sync.this, getString(R.string.general_error_message), Toast.LENGTH_LONG).show();
                    }
                })
        );
//        Observable.concat(syncGruppi(), syncEventi())
//                .subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Action1<Void>() {
//                    @Override
//                    public void call(Void aVoid) {
//                        Prefs.putBoolean(Constants.LOGIN_COMPLETED, true);
//
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                startActivity(new Intent(Act_Sync.this, Act_Home.class));
//                                overridePendingTransition(0, 0);
//                                finish();
//                            }
//                        }, 1000);
//                    }
//                }, new Action1<Throwable>() {
//                    @Override
//                    public void call(Throwable throwable) {
//                        throwable.printStackTrace();
//
//                        showRetry();
//                        Toast.makeText(Act_Sync.this, getString(R.string.general_error_message), Toast.LENGTH_LONG).show();
//                    }
//                });
    }

    /**
     * Sync dei gruppi
     *
     * @return void
     */
    private Observable<Void> syncGruppi() {
        return groupApi.getGroups()
                .flatMap(new Func1<List<Group>, Observable<Group>>() {
                    @Override
                    public Observable<Group> call(List<Group> gruppi) {
                        mGroupIdsTemp = new HashSet<>();
                        return Observable.from(gruppi);
                    }
                })
                .map(new Func1<Group, Group>() {
                    @Override
                    public Group call(Group gruppo) {
                        mGroupIdsTemp.add(gruppo.getUuid());
                        Realm realm = App.getRealm();
                        //noinspection TryFinallyCanBeTryWithResources
                        try {
                            Group realmGruppo = new GroupDao(realm).getGroup(gruppo.getUuid());
                            if (realmGruppo == null || mFromBackup) {
                                gruppo = SyncTotal.syncGruppo(realm, gruppo);
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        } finally {
                            realm.close();
                        }
                        return gruppo;
                    }
                })
                .toList()
                .flatMap(new Func1<List<Group>, Observable<Void>>() {
                    @Override
                    public Observable<Void> call(List<Group> groups) {
                        mGroupIds.removeAll(mGroupIdsTemp);
                        for (String uuid : mGroupIds) {
                            syncGroup(uuid);
                        }
                        return Observable.just(null);
                    }
                });
    }

    /**
     * Sync specifico di un gruppo
     *
     * @param uuid uuid del gruppo
     */
    private void syncGroup(final String uuid) {
        groupApi.getGroup(uuid)
                .toBlocking()
                .subscribe(new Action1<Group>() {
                    @Override
                    public void call(Group gruppo) {
                        Realm realm = App.getRealm();
                        //noinspection TryFinallyCanBeTryWithResources
                        try {
                            SyncTotal.syncGruppo(realm, gruppo);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        } finally {
                            realm.close();
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        APIError error = APIErrorUtils.parseError(Act_Sync.this, throwable);
                        if (error.status() == 403) {
                            Realm realm = App.getRealm();
                            //noinspection TryFinallyCanBeTryWithResources
                            try {
                                final Group gruppo = new GroupDao(realm).getGroup(uuid);
                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(@NonNull Realm realm) {
                                        User user = new User();
                                        user.setUuid(AccountProvider.getInstance().getAccountId());
                                        gruppo.getGuests().remove(user);
                                        gruppo.setMyselfPresent(false);
                                    }
                                });
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            } finally {
                                realm.close();
                            }
                        }
                    }
                });
    }

    /**
     * Sync delle attività
     *
     * @return void
     */
    private Observable<Void> syncEventi() {
        return eventApi.getEvents()
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.computation())
                .flatMap(new Func1<List<Event>, Observable<Event>>() {
                    @Override
                    public Observable<Event> call(List<Event> events) {
                        mEventIdsTemp = new HashSet<>();
                        return Observable.from(events);
                    }
                })
                .map(new Func1<Event, Event>() {
                    @Override
                    public Event call(Event evento) {
                        mEventIdsTemp.add(evento.getUuid());
                        Realm realm = App.getRealm();
                        //noinspection TryFinallyCanBeTryWithResources
                        try {
                            Event realmAttivita = new EventDao(realm).getEvent(evento.getUuid());
                            if (realmAttivita == null || mFromBackup) {
                                evento = SyncTotal.syncEvento(realm, evento);
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        } finally {
                            realm.close();
                        }
                        return evento;
                    }
                })
                .toList()
                .flatMap(new Func1<List<Event>, Observable<Void>>() {
                    @Override
                    public Observable<Void> call(List<Event> eventi) {
                        mEventIds.removeAll(mEventIdsTemp);
                        for (String uuid : mEventIds) {
                            syncEvent(uuid);
                        }
                        return Observable.just(null);
                    }
                });
    }

    /**
     * Sync specifico di un'attività
     *
     * @param uuid uuid dell'attività
     */
    private void syncEvent(final String uuid) {
        eventApi.getEvent(uuid)
                .toBlocking()
                .subscribe(new Action1<Event>() {
                    @Override
                    public void call(Event evento) {
                        Realm realm = App.getRealm();
                        //noinspection TryFinallyCanBeTryWithResources
                        try {
                            SyncTotal.syncEvento(realm, evento);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        } finally {
                            realm.close();
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        APIError error = APIErrorUtils.parseError(Act_Sync.this, throwable);
                        if (error.status() == 403) {
                            Realm realm = App.getRealm();
                            //noinspection TryFinallyCanBeTryWithResources
                            try {
                                final Event evento = new EventDao(realm).getEvent(uuid);
                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(@NonNull Realm realm) {
                                        User user = new User();
                                        user.setUuid(AccountProvider.getInstance().getAccountId());
                                        evento.getGuests().remove(user);
                                        evento.setMyselfPresent(false);
                                    }
                                });
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            } finally {
                                realm.close();
                            }
                        }
                    }
                });
    }

    /**
     * Mostra retry
     */
    private void showRetry() {
        mFailed.setVisibility(View.VISIBLE);
        mRetry.setVisibility(View.VISIBLE);
        mProgress.setVisibility(View.GONE);
    }

    /**
     * Nascondi retry
     */
    private void hideRetry() {
        mFailed.setVisibility(View.GONE);
        mRetry.setVisibility(View.GONE);
        mProgress.setVisibility(View.VISIBLE);
    }
}
