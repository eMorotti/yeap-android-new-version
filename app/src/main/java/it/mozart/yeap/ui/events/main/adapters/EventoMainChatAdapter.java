package it.mozart.yeap.ui.events.main.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;
import it.mozart.yeap.events.MessaggioChatActionEvent;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Message;
import it.mozart.yeap.ui.main.ChatMessageBinder;
import it.mozart.yeap.ui.main.ChatMessageListener;
import it.mozart.yeap.ui.main.views.ChatMessageStandardLayoutLeft;
import it.mozart.yeap.ui.main.views.ChatMessageStandardLayoutRight;
import it.mozart.yeap.ui.main.views.ChatMessageSystemLayout;
import it.mozart.yeap.utility.Constants;

public class EventoMainChatAdapter extends RealmRecyclerViewAdapter<Message, RecyclerView.ViewHolder> implements ChatMessageListener {

    private Context mContext;
    private SimpleDateFormat mFormatter;
    private long mNumNewMessages;
    private HashMap<String, String> mSelectedMessageID;

    public EventoMainChatAdapter(final Context context, RealmResults<Message> data, long numNewMessages) {
        super(data, true);
        mContext = context;
        mNumNewMessages = numNewMessages;
        mFormatter = new SimpleDateFormat("EEEE, MMMM dd, yyyy", Locale.getDefault());
        mSelectedMessageID = new HashMap<>();

        registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                int position = layoutManager.findFirstVisibleItemPosition();
                if (position == 0) {
                    layoutManager.scrollToPositionWithOffset(0, 0);
                }
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        Message messaggio = getItem(position);
        if (messaggio == null) {
            return Constants.ADAPTER_TYPE_SYSTEM;
        }

        if (messaggio.getType() == Constants.MESSAGGIO_TYPE_SYSTEM) {
            return Constants.ADAPTER_TYPE_SYSTEM;
        } else {
            if (messaggio.getCreator().getUuid().equals(AccountProvider.getInstance().getAccountId())) {
                return Constants.ADAPTER_TYPE_STANDARD_RIGHT;
            } else {
                return Constants.ADAPTER_TYPE_STANDARD_LEFT;
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case Constants.ADAPTER_TYPE_STANDARD_LEFT:
                return new MainChatViewHolderLeft(new ChatMessageStandardLayoutLeft(mContext));
            case Constants.ADAPTER_TYPE_STANDARD_RIGHT:
                return new MainChatViewHolderRight(new ChatMessageStandardLayoutRight(mContext));
            default:
                return new MainChatSystemViewHolder(new ChatMessageSystemLayout(mContext));
        }
    }

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        if (holder instanceof ChatMessageBinder) {
            ((ChatMessageBinder) holder).unbind();
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Message messaggio = getItem(position);
        if (messaggio == null)
            return;

        switch (getItemViewType(position)) {
            case Constants.ADAPTER_TYPE_STANDARD_LEFT:
                bindChatLeftAndRight(holder, messaggio, position, true);
                break;
            case Constants.ADAPTER_TYPE_STANDARD_RIGHT:
                bindChatLeftAndRight(holder, messaggio, position, false);
                break;
            default:
                bindChatSystem(holder, messaggio, position);
                break;
        }
    }

    private void bindChatLeftAndRight(RecyclerView.ViewHolder holder, Message messaggio, int position, boolean isLeft) {
        if (!messaggio.isValid())
            return;

        if (isLeft) {
            MainChatViewHolderLeft realHolder = (MainChatViewHolderLeft) holder;
            ((ChatMessageStandardLayoutLeft) realHolder.itemView).bind(messaggio, position, this);
        } else {
            MainChatViewHolderRight realHolder = (MainChatViewHolderRight) holder;
            ((ChatMessageStandardLayoutRight) realHolder.itemView).bind(messaggio, position, this);
        }
    }

    private void bindChatSystem(RecyclerView.ViewHolder holder, Message messaggio, int position) {
        if (!messaggio.isValid())
            return;

        MainChatSystemViewHolder realHolder = (MainChatSystemViewHolder) holder;
        ((ChatMessageSystemLayout) realHolder.itemView).bind(messaggio, position, this);
    }

    public int getSize() {
        return getItemCount();
    }

    // Ritorno gli attuali elementi caricati
    @Override
    public List<Message> getMessaggi() {
        return getData();
    }

    // Formatto la data del messaggio
    public long updateStickyDate(int position) {
        try {
            Message messaggio = getItem(position);
            return messaggio != null ? messaggio.getCreatedAtDate() : 0;
        } catch (Exception ignored) { // ignoro visto che è relativo al fatto di aver cancellato tutti i messaggi
            return 0;
        }
    }

    // Recupero il formatter
    @Override
    public SimpleDateFormat getFormatter() {
        return mFormatter;
    }

    @Override
    public int getNumItems() {
        return getItemCount();
    }

    @Override
    public int getItemType(int position) {
        return getItemViewType(position);
    }

    @Override
    public long getNumNewMessages() {
        return mNumNewMessages;
    }

    @Override
    public HashMap<String, String> getSelectedMessageIds() {
        return mSelectedMessageID;
    }

    public void resetSelectedMessageID() {
        this.mSelectedMessageID.clear();
        notifyDataSetChanged();
    }

    public void updateNumberNewMessages(long numNewMessages) {
        mNumNewMessages = numNewMessages;
    }

    private void longClicked(RecyclerView.ViewHolder holder) {
        int position = holder.getLayoutPosition();
        Message messaggio = getItem(position);
        if (messaggio != null) {
            if (mSelectedMessageID.containsKey(messaggio.getUuid()))
                mSelectedMessageID.remove(messaggio.getUuid());
            else
                mSelectedMessageID.put(messaggio.getUuid(), messaggio.getCreator().getChatName());
            notifyItemChanged(position);
            EventBus.getDefault().post(new MessaggioChatActionEvent(mSelectedMessageID.size() != 0));
        }
    }

    // VIEW HOLDER

    private class MainChatViewHolderLeft extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        MainChatViewHolderLeft(ChatMessageStandardLayoutLeft v) {
            super(v);

            v.setOnClickListener(this);
            v.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mSelectedMessageID.size() != 0) {
                onLongClick(v);
            }
        }

        @Override
        public boolean onLongClick(View v) {
            longClicked(this);
            return true;
        }
    }

    private class MainChatViewHolderRight extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        MainChatViewHolderRight(ChatMessageStandardLayoutRight v) {
            super(v);

            v.setOnClickListener(this);
            v.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mSelectedMessageID.size() != 0) {
                onLongClick(v);
            }
        }

        @Override
        public boolean onLongClick(View v) {
            longClicked(this);
            return true;
        }
    }

    private class MainChatSystemViewHolder extends RecyclerView.ViewHolder {

        MainChatSystemViewHolder(View v) {
            super(v);
        }
    }
}
