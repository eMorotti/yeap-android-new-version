package it.mozart.yeap.ui.views;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Build;
import android.support.annotation.Keep;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;

import it.mozart.yeap.R;
import it.mozart.yeap.utility.Utils;

public class PercentBgView extends View {

    private Paint mPaint;
    private Path mPath;
    private ObjectAnimator mAnimator;

    private RectF mRect;
    private float mPercent;
    private float mRadius;
    private int mBgColor = Color.LTGRAY;
    private int mColor = Color.RED;
    private int mMax;

    private static final int DEFAULT_MAX = 100;

    public PercentBgView(Context context) {
        super(context);
        initialize(null);
    }

    public PercentBgView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialize(attrs);
    }

    public PercentBgView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(attrs);
    }

    @SuppressWarnings("unused")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public PercentBgView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initialize(attrs);
    }

    private void initialize(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.PercentBgView, 0, 0);
            mBgColor = a.getColor(R.styleable.PercentBgView_pbv_bg_color, mBgColor);
            mColor = a.getColor(R.styleable.PercentBgView_pbv_color, mColor);
            a.recycle();
        }

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.FILL);
        mPath = new Path();
        mRect = new RectF();

        mMax = DEFAULT_MAX;
        mRadius = Utils.dpToPx(14, getContext());
        mPercent = 0;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        mPaint.setColor(mBgColor);
        mRect.set(0, 0, getWidth(), getHeight());
        canvas.drawRoundRect(mRect, mRadius, mRadius, mPaint);

        mPath.reset();
        mPath.addRoundRect(mRect, mRadius, mRadius, Path.Direction.CW);
        canvas.clipPath(mPath);

        mPaint.setColor(mColor);
        mRect.set(0, 0, getWidth() * mPercent, getHeight());
        canvas.drawRect(mRect, mPaint);
    }

    public void setMax(int max) {
        mMax = max;
    }

    @Keep
    private void setPercent(float percent) {
        mPercent = percent;
        invalidate();
    }

    public void setColor(int color) {
        mColor = color;
        invalidate();
    }

    public void setBgColor(int color) {
        mBgColor = color;
        invalidate();
    }

    public void updatePercent(int current) {
        float percent = (float) current / (float) mMax;

        if (mAnimator != null) {
            mAnimator.cancel();
        }

        mAnimator = ObjectAnimator.ofFloat(this, "percent", mPercent, percent);
        mAnimator.setDuration(200);
        mAnimator.start();
    }
}
