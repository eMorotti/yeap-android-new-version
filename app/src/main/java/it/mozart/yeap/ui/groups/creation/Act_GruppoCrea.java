package it.mozart.yeap.ui.groups.creation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import it.mozart.yeap.R;
import it.mozart.yeap.helpers.CreationModelHelper;
import it.mozart.yeap.ui.base.BaseActivityWithSyncService;
import it.mozart.yeap.ui.main.Frg_MainPartecipanti;
import it.mozart.yeap.ui.main.MainCreationControl;
import it.mozart.yeap.ui.views.BorderFabLayout;
import it.mozart.yeap.utility.Utils;

public class Act_GruppoCrea extends BaseActivityWithSyncService {

    @BindView(R.id.fab_layout)
    BorderFabLayout mFabLayout;

    @OnClick(R.id.fab_layout)
    void fabClicked() {
        // Nascondo tastiera
        Utils.hideKeyboard(this);

        Fragment fragment = null;
        MainCreationControl currentFragment = Utils.safeCast(getSupportFragmentManager().findFragmentById(R.id.content), MainCreationControl.class);

        // Creo il nuovo fragment basandomi sul numero di quelli già presenti nel backstack
        switch (getSupportFragmentManager().getBackStackEntryCount()) {
            case 0:
                if (currentFragment != null) {
                    if (!currentFragment.canGoOn())
                        break;
                }
                fragment = Frg_MainPartecipanti.newInstance(false);
                break;
            case 1:
                if (currentFragment != null) {
                    if (!currentFragment.canGoOn())
                        break;
                }
                fragment = new Frg_GruppoCreaRiepilogo();
                mFabLayout.hide();
                break;
        }
        if (fragment != null) {
            // Sostituisco con il nuovo fragment
            getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.enter_right, R.anim.exit_left, R.anim.enter_left, R.anim.exit_right)
                    .replace(R.id.content, fragment)
                    .addToBackStack(null)
                    .commit();
        }
    }

    // SUPPORT

    @Inject
    CreationModelHelper creationModelHelper;

    // SYSTEM

    @Override
    protected int getLayoutId() {
        return R.layout.act_gruppo_crea;
    }

    @Override
    protected void initVariables() {

    }

    @Override
    protected void loadParameters(Bundle extras) {

    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    protected void initialize() {
        mFabLayout.getFabButton().setImageResource(R.drawable.ic_arrow_forward_white_24dp);

        // Creo l'attività e mostro il primo step
        creationModelHelper.createGroup();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content, new Frg_GruppoCreaInfo())
                .commit();

        mFabLayout.forceHide();
        mFabLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                mFabLayout.show();
            }
        }, 500);
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content);
        if (fragment != null && fragment instanceof Frg_GruppoCreaRiepilogo) {
            if (((Frg_GruppoCreaRiepilogo) fragment).cancelApiCall()) {
                return;
            }
        }

        if (!getSupportFragmentManager().popBackStackImmediate())
            returnToPreviousActivity();
        else {
            mFabLayout.show();
        }
    }

    // FUNCTIONS

    /**
     * Aggiorna il titolo e l'icona della toolbar
     *
     * @param title titolo della toolbar
     * @param icon  icona della navigazione
     */
    public void setToolbarTitleAndIconFromCrea(String title, int icon) {
        if (mToolbar != null) {
            mToolbar.setNavigationIcon(icon);
            setToolbarTitle(title);
        }
    }

    /**
     * Aggiorna il sottotitolo della toolbar
     *
     * @param subtitle sottotitolo della toolbar
     */
    public void setToolbarSubtitleFromCrea(String subtitle) {
        if (mToolbar != null && mToolbarSpecial != null) {
            setToolbarSubtitle(subtitle);
        }
    }

    /**
     * Ritorno all'activity precedente, con animazione se possibile
     */
    private void returnToPreviousActivity() {
        if (!Utils.isLollipop()) {
            super.onBackPressed();
            overridePendingTransition(0, R.anim.slide_out_bottom_slow);
            return;
        }

        mFabLayout.hide(new BorderFabLayout.OnVisibilityChangeListener() {
            @Override
            public void onHidden() {
                setResult(Activity.RESULT_CANCELED, new Intent());
                finish();
            }

            @Override
            public void onShown() {

            }
        });
    }
}
