package it.mozart.yeap.ui.sondaggi;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import butterknife.BindView;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import it.mozart.yeap.R;
import it.mozart.yeap.events.UserImageSelectedEvent;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.Poll;
import it.mozart.yeap.realm.PollItem;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.PollDao;
import it.mozart.yeap.realm.dao.VoteDao;
import it.mozart.yeap.ui.base.BaseFragment;
import it.mozart.yeap.ui.events.main.events.PollItemMoreDetailSelectedEvent;
import it.mozart.yeap.ui.sondaggi.adapter.SondaggioVoceVotiAdapter;
import it.mozart.yeap.ui.views.AutoRefreshImageView;
import it.mozart.yeap.utility.DateUtils;

public class Frg_DettaglioSondaggioVoce extends BaseFragment {

    @BindView(R.id.avatar)
    AutoRefreshImageView mAvatar;
    @BindView(R.id.utente)
    TextView mUtente;
    @BindView(R.id.time)
    TextView mTime;
    @BindView(R.id.more)
    ImageButton mMore;

    @BindView(R.id.suggerimento)
    TextView mSuggerimento;
    @BindView(R.id.dove_layout)
    View mDoveLayout;
    @BindView(R.id.dove)
    TextView mDove;
    @BindView(R.id.quando_layout)
    View mQuandoLayout;
    @BindView(R.id.quando)
    TextView mQuando;
    @BindView(R.id.nuovo)
    TextView mNuovo;

    @BindView(R.id.layout)
    View mLayout;

    @BindView(R.id.voti_header)
    TextView mVotiHeader;
    @BindView(R.id.recyclerview)
    RecyclerView mRecyclerView;

    // SUPPORT

    @Inject
    Realm realm;

    private Event mEvento;
    private PollItem mSondaggioVoce;
    private SondaggioVoceVotiAdapter mAdapter;
    private String mId;

    private static final String PARAM_ID = "paramId";

    // SYSTEM

    public static Frg_DettaglioSondaggioVoce newInstance(String sondaggioVoceId) {
        Bundle args = new Bundle();
        args.putString(PARAM_ID, sondaggioVoceId);
        Frg_DettaglioSondaggioVoce fragment = new Frg_DettaglioSondaggioVoce();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_dettaglio_sondaggio_voce;
    }

    @Override
    protected void initValues() {

    }

    @Override
    protected void loadParameters(Bundle extras) {
        mId = extras.getString(PARAM_ID);
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mSondaggioVoce != null) {
            mSondaggioVoce.removeAllChangeListeners();
        }
        if (mEvento != null) {
            mEvento.removeAllChangeListeners();
        }
    }

    @Override
    protected void initialize() {
        PollDao pollDao = new PollDao(realm);
        EventDao eventDao = new EventDao(realm);

        mSondaggioVoce = pollDao.getPollItem(mId);
        final Poll sondaggio = pollDao.getPoll(mSondaggioVoce.getPollUuid());
        if (sondaggio == null) {
            Toast.makeText(getContext(), getString(R.string.general_error_sondaggio_non_presente), Toast.LENGTH_LONG).show();
            getActivity().finish();
        } else {
            mEvento = eventDao.getEvent(sondaggio.getEventUuid());
            if (mEvento == null) {
                Toast.makeText(getContext(), getString(R.string.general_error_evento_non_presente), Toast.LENGTH_LONG).show();
                getActivity().finish();
            } else {
                mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                mAdapter = new SondaggioVoceVotiAdapter(getContext(), new VoteDao(realm).getListVotePollItemYes(mId));
                mRecyclerView.setAdapter(mAdapter);
                bindInfo(mSondaggioVoce);

                mEvento.addChangeListener(new RealmChangeListener<Event>() {
                    @Override
                    public void onChange(@NonNull Event element) {
                        if (element.isValid() && mSondaggioVoce != null && mSondaggioVoce.isValid()) {
                            bindInfo(mSondaggioVoce);
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                });
                mSondaggioVoce.addChangeListener(new RealmChangeListener<PollItem>() {
                    @Override
                    public void onChange(@NonNull PollItem element) {
                        if (element.isValid()) {
                            bindInfo(element);
                        }
                    }
                });
                mLayout.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        getActivity().onTouchEvent(event);
                        return true;
                    }
                });
            }
        }
    }

    // FUNCTIONS

    void bindInfo(final PollItem voce) {
        // Gestisco utente
        if (voce.getCreator() != null) {
            if (voce.getCreatorUuid().equals(AccountProvider.getInstance().getAccountId())) {
                mUtente.setText(String.format(getString(R.string.sondaggio_voce_utente_propone_by_me), voce.getCreator().getChatName()));
            } else {
                mUtente.setText(String.format(getString(R.string.sondaggio_voce_utente_propone), voce.getCreator().getChatName()));
            }

            mAvatar.setIgnoreBind(false);
            mAvatar.bindThumb(voce.getCreatorUuid());
            mAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post(new UserImageSelectedEvent(voce.getCreatorUuid(), mAvatar));
                }
            });
        }

        mTime.setText(DateUtils.smartFormatDate(getContext(), voce.getCreatedAt(), "dd/MM/yy, HH:mm", ", HH:mm"));

        if (mEvento.isMyselfPresent()) {
            mMore.setVisibility(View.VISIBLE);
            mMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post(new PollItemMoreDetailSelectedEvent(voce.getUuid(), mMore));
                }
            });
        } else {
            mMore.setVisibility(View.GONE);
        }

        mNuovo.setVisibility(voce.isNuovo() ? View.VISIBLE : View.GONE);

        if (voce.getWhere() != null) {
            mDove.setText(voce.getWhere());
            mDoveLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Uri mapUri = Uri.parse("geo:" + voce.getLat() + "," + voce.getLng() + "?q=" + voce.getWhere());
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, mapUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    if (mapIntent.resolveActivity(getContext().getPackageManager()) != null) {
                        startActivity(mapIntent);
                    } else {
                        Toast.makeText(getContext(), getString(R.string.general_error_no_app_found), Toast.LENGTH_SHORT).show();
                    }
                }
            });
            mDoveLayout.setVisibility(View.VISIBLE);
            mQuandoLayout.setVisibility(View.GONE);
            mSuggerimento.setVisibility(View.GONE);
        } else if (voce.isQuandoPresent()) {
            mQuando.setText(DateUtils.createQuandoText(voce.generateQuandoModel()));
            mQuandoLayout.setVisibility(View.VISIBLE);
            mDoveLayout.setVisibility(View.GONE);
            mSuggerimento.setVisibility(View.GONE);
        } else {
            mSuggerimento.setText(voce.getWhat());
            mSuggerimento.setVisibility(View.VISIBLE);
            mQuandoLayout.setVisibility(View.GONE);
            mDoveLayout.setVisibility(View.GONE);
        }

        switch (mAdapter.getItemCount()) {
            case 0:
                mVotiHeader.setText(getString(R.string.voti_header_nessuno));
                break;
            case 1:
                mVotiHeader.setText(getString(R.string.voti_header_singolo));
                break;
            default:
                mVotiHeader.setText(String.format(getString(R.string.voti_header_multiplo), mAdapter.getItemCount()));
                break;
        }
    }
}
