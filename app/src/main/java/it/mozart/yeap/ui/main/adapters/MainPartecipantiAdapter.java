package it.mozart.yeap.ui.main.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.HashSet;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderAdapter;
import io.realm.RealmResults;
import it.mozart.yeap.R;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.ui.main.Frg_MainPartecipanti;
import it.mozart.yeap.ui.main.events.MainPartecipantiUserSelectedEvent;
import it.mozart.yeap.ui.views.AnimCheckBox;
import it.mozart.yeap.ui.views.AutoRefreshImageView;
import it.mozart.yeap.ui.views.RecyclerViewIndex;

public class MainPartecipantiAdapter extends RecyclerView.Adapter<MainPartecipantiAdapter.MainViewHolder>
        implements StickyHeaderAdapter<MainPartecipantiAdapter.HeaderViewHolder>, RecyclerViewIndex.AdapterItemIndex {

    private Context mContext;
    private RealmResults<User> mList;
    private List<Frg_MainPartecipanti.HeaderItem> mHeaderList;
    private List<String> mCheckList;
    private List<String> mCheckInvitedList;
    private HashSet<User> mAlreadyInList;
    private HashSet<User> mInvitedAlreadyInList;

    public MainPartecipantiAdapter(Context context, RealmResults<User> list, List<Frg_MainPartecipanti.HeaderItem> headers,
                                   List<String> checked, List<String> checkedInvitated, HashSet<User> alreadyInList, HashSet<User> invitedAlreadyInList) {
        this.mContext = context;
        this.mList = list;
        this.mHeaderList = headers;
        this.mCheckList = checked;
        this.mCheckInvitedList = checkedInvitated;
        this.mAlreadyInList = alreadyInList;
        this.mInvitedAlreadyInList = invitedAlreadyInList;
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_partecipanti_utente, parent, false);
        return new MainViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MainViewHolder holder, int position) {
        final User utente = mList.get(position);
        holder.name.setText(utente.getChatName());
        holder.check.setOnCheckedChangeListener(null);

        if ((mAlreadyInList != null && mAlreadyInList.contains(utente)) || (mInvitedAlreadyInList != null && mInvitedAlreadyInList.contains(utente))) {
            holder.desc.setText(mContext.getString(R.string.gruppo_utente_gia_presente));

            holder.check.setEnabled(false);
        } else if (utente.getUuid() != null) {
            holder.desc.setText(utente.getPhone());

            holder.check.setEnabled(true);
            holder.check.setChecked(mCheckList.indexOf(utente.getUuid()) != -1, false);
            holder.check.setOnCheckedChangeListener(new AnimCheckBox.OnCheckedChangeListener() {
                @Override
                public void onChange(boolean checked) {
                    if (!checked) {
                        mCheckList.remove(utente.getUuid());
                    } else {
                        mCheckList.add(utente.getUuid());
                    }
                    EventBus.getDefault().post(new MainPartecipantiUserSelectedEvent(utente, !checked));
                }
            });
        } else {
            holder.desc.setText(utente.getPhone());

            holder.check.setEnabled(true);
            holder.check.setChecked(mCheckInvitedList.indexOf(utente.getPhone()) != -1, false);
            holder.check.setOnCheckedChangeListener(new AnimCheckBox.OnCheckedChangeListener() {
                @Override
                public void onChange(boolean checked) {
                    if (!checked) {
                        mCheckInvitedList.remove(utente.getPhone());
                    } else {
                        mCheckInvitedList.add(utente.getPhone());
                    }
                    EventBus.getDefault().post(new MainPartecipantiUserSelectedEvent(utente, !checked));
                }
            });
        }

        holder.image.bindThumb(utente.getUuid());
        holder.yeap.setVisibility(utente.isInYeap() ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public long getHeaderId(int position) {
        return getId(position);
    }

    @Override
    public HeaderViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_partecipanti_header, parent, false);
        return new HeaderViewHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(HeaderViewHolder viewholder, int position) {
        try {
            char c = mList.get(position).getChatName(true).charAt(0);
            if ((c < 65 || c > 90) && (c < 97 || c > 122)) {
                c = '#';
            }
            viewholder.header.setText(String.valueOf(c).toUpperCase());
        } catch (Exception ignored) {
        }
    }

    private int getId(int position) {
        for (int i = mHeaderList.size() - 1; i >= 0; i--) {
            if (position >= mHeaderList.get(i).getPosition())
                return mHeaderList.get(i).getId();
        }
        return mHeaderList.get(0).getId();
    }

    @Override
    public int getWordPosition(String word) {
        for (int i = 0; i < mHeaderList.size(); i++) {
            char c = mHeaderList.get(i).getCharacter();
            if (word.equals("#")) {
                return 0;
            }
            if (word.equalsIgnoreCase(String.valueOf(c))) {
                return mHeaderList.get(i).getPosition();
            }
        }
        return -1;
    }

    public void updateAdapter(RealmResults<User> list, List<Frg_MainPartecipanti.HeaderItem> headers, List<String> checked, List<String> checkedInvited) {
        this.mList = list;
        this.mHeaderList = headers;
        this.mCheckList = checked;
        this.mCheckInvitedList = checkedInvited;
        notifyDataSetChanged();
    }

    public RealmResults<User> getList() {
        return mList;
    }

    class MainViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.checkbox)
        AnimCheckBox check;
        @BindView(R.id.image)
        AutoRefreshImageView image;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.desc)
        TextView desc;
        @BindView(R.id.yeap)
        View yeap;

        MainViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (check.isEnabled()) {
                check.performClick();
            }
        }
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.header)
        TextView header;

        HeaderViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
