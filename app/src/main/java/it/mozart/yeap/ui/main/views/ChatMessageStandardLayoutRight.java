package it.mozart.yeap.ui.main.views;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;

import it.mozart.yeap.R;

public class ChatMessageStandardLayoutRight extends ChatMessageStandardLayout {

    public ChatMessageStandardLayoutRight(Context context) {
        super(context);
    }

    public ChatMessageStandardLayoutRight(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ChatMessageStandardLayoutRight(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @SuppressWarnings("unused")
    public ChatMessageStandardLayoutRight(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.item_chat_right;
    }

    @Override
    protected boolean isOtherMessage() {
        return false;
    }

    @Override
    protected int getLinkColor() {
        return ContextCompat.getColor(getContext(), R.color.textLinkRight);
    }
}
