package it.mozart.yeap.ui.events.main;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.MyTabLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.PopupMenu;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import javax.inject.Inject;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.api.models.APIError;
import it.mozart.yeap.api.models.EventoCambioStatoApiModel;
import it.mozart.yeap.api.models.SondaggioApiModel;
import it.mozart.yeap.api.services.EventApi;
import it.mozart.yeap.api.services.PollApi;
import it.mozart.yeap.events.AddItemToPollEvent;
import it.mozart.yeap.events.ApiCallResultEvent;
import it.mozart.yeap.events.AvatarSelectedEvent;
import it.mozart.yeap.events.ImageSelectedEvent;
import it.mozart.yeap.events.StartImageDownloadEvent;
import it.mozart.yeap.events.StartUrlFindEvent;
import it.mozart.yeap.events.UserImageSelectedEvent;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.helpers.AnalitycsProvider;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.Message;
import it.mozart.yeap.realm.Poll;
import it.mozart.yeap.realm.PollItem;
import it.mozart.yeap.realm.Vote;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.realm.dao.PollDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.realm.dao.VoteDao;
import it.mozart.yeap.services.imageDownload.ImageDownloadJob;
import it.mozart.yeap.services.messaggi.UrlPreviewJob;
import it.mozart.yeap.services.realmJob.EventDeleteJob;
import it.mozart.yeap.services.voti.VotiService;
import it.mozart.yeap.ui.base.BaseActivityWithSyncService;
import it.mozart.yeap.ui.events.main.adapters.EventoMainPagerAdapter;
import it.mozart.yeap.ui.events.main.events.EventDeleteEvent;
import it.mozart.yeap.ui.events.main.events.EventInfoSelectedEvent;
import it.mozart.yeap.ui.events.main.events.EventoUpdateStatus;
import it.mozart.yeap.ui.events.main.events.PollItemMoreClickedEvent;
import it.mozart.yeap.ui.events.main.events.PollMoreSelectedEvent;
import it.mozart.yeap.ui.events.main.events.PollSelectedEvent;
import it.mozart.yeap.ui.events.main.events.ShowPopupInvitedEvent;
import it.mozart.yeap.ui.events.main.events.ShowVotiEvent;
import it.mozart.yeap.ui.events.main.events.ShowVotiInteresseEvent;
import it.mozart.yeap.ui.events.main.events.VoteEventClickedEvent;
import it.mozart.yeap.ui.events.main.events.VoteInterestEventClickedEvent;
import it.mozart.yeap.ui.events.main.events.VotePollItemClickedEvent;
import it.mozart.yeap.ui.home.Act_Home;
import it.mozart.yeap.ui.main.Act_MainAggiungiPartecipante;
import it.mozart.yeap.ui.profile.Act_Profilo;
import it.mozart.yeap.ui.sondaggi.Act_AggiungiVoceSondaggio;
import it.mozart.yeap.ui.sondaggi.Act_DettaglioSondaggio;
import it.mozart.yeap.ui.sondaggi.Act_DettaglioSondaggioVoci;
import it.mozart.yeap.ui.sondaggi.Act_GestisciSondaggio;
import it.mozart.yeap.ui.sondaggi.Act_SondaggioChiudi;
import it.mozart.yeap.ui.support.Act_ContactPhotoPopup;
import it.mozart.yeap.ui.support.Act_Dove;
import it.mozart.yeap.ui.support.Act_ImageDetail;
import it.mozart.yeap.ui.support.Act_Quando;
import it.mozart.yeap.ui.support.events.UserSelectedEvent;
import it.mozart.yeap.ui.views.AnimatedTextView;
import it.mozart.yeap.ui.views.BorderFabLayout;
import it.mozart.yeap.ui.views.NotSwipeableViewPager;
import it.mozart.yeap.utility.APIErrorUtils;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.Prefs;
import it.mozart.yeap.utility.SettingUtils;
import it.mozart.yeap.utility.Utils;
import it.mozart.yeap.utility.VoteUtils;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class Act_EventoMain extends BaseActivityWithSyncService {

    @BindView(R.id.status_bar)
    View mStatusBar;
    @BindView(R.id.viewpager)
    NotSwipeableViewPager mViewPager;
    @BindView(R.id.tab_layout)
    MyTabLayout mTabLayout;
    @BindView(R.id.exit_text)
    TextView mExitText;
    @BindView(R.id.fab_layout)
    BorderFabLayout mFabLayout;

    @BindView(R.id.reveal_layout)
    View mRevealLayout;

    @OnClick(R.id.fab_layout)
    void fabClicked() {
        if (mAnimationInProgress) {
            return;
        }

        if (mEvento == null || !mEvento.isMyselfPresent()) {
            return;
        }

        if (mViewPager.getCurrentItem() == 2) {
            AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "createPoll", "activity");
            animateReveal(mFabLayout, Act_GestisciSondaggio.newIntentCrea(mId, null));
        } else if (mViewPager.getCurrentItem() == 3) {
            if (mCanAddGuests) {
                AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "addParticipants", "activity");
                animateReveal(mFabLayout, Act_MainAggiungiPartecipante.newIntent(mId, true));
            } else {
                Toast.makeText(this, getString(R.string.main_evento_no_permission_add_users), Toast.LENGTH_SHORT).show();
            }
        }
    }

    // SUPPORT

    @Inject
    Realm realm;
    @Inject
    EventApi eventApi;
    @Inject
    PollApi pollApi;

    @BindColor(R.color.colorGradientEnd)
    int mColorGradientEnd;

    private MessageDao mMessageDao;
    private PollDao mPollDao;
    private List<String> mMessageIdsForUrlFind;
    private Event mEvento;
    private String mId;
    private boolean mCanAddGuests;
    private boolean mAnimationInProgress;
    private boolean mOpenPollCheck;
    private int mPage;

    private static final String PARAM_ID = "paramId";
    private static final String PARAM_SHARE_TEXT = "paramShareText";
    private static final String PARAM_PAGE = "paramPage";

    // SYSTEM

    public static Intent newIntent(String attivitaId) {
        Intent intent = new Intent(App.getContext(), Act_EventoMain.class);
        intent.putExtra(PARAM_ID, attivitaId);
        return intent;
    }

    public static Intent newIntent(String attivitaId, String shareText) {
        Intent intent = new Intent(App.getContext(), Act_EventoMain.class);
        intent.putExtra(PARAM_ID, attivitaId);
        intent.putExtra(PARAM_SHARE_TEXT, shareText);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUseBus(true);

        if (Utils.isLollipop()) {
            mStatusBar.getLayoutParams().height = Utils.getStatusBarHeight(this);
        }

        if (getToolbarLayout() != null) {
            getToolbarSubtitle().setVisibility(View.GONE);
        }

        if (mToolbar != null) {
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mAnimationInProgress) {
                        return;
                    }

                    if (isTaskRoot()) {
                        startActivity(Act_Home.newIntent(0));
                        finish();
                    } else {
                        Act_EventoMain.super.onBackPressed();
                    }
                }
            });
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_main_evento;
    }

    @Override
    protected void initVariables() {
        mMessageIdsForUrlFind = new ArrayList<>();
        mAnimationInProgress = false;
    }

    @Override
    protected void loadParameters(Bundle extras) {
        mId = extras.getString(PARAM_ID);
        mPage = extras.getInt(PARAM_PAGE, 0);
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {
    }

    @Override
    protected void saveInfos(Bundle outState) {
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mEvento != null && mPollDao != null) {
            updateTab(0, false, mEvento.getNumInfoUpdates(), 0);
            updateTab(1, false, mEvento.getNumNewMessages(), 0);
            updateTab(2, false, mEvento.getNumPollUpdates(), mPollDao.getNumberOfPolls(mEvento.getUuid()));
            updateTab(3, false, mEvento.getNumUserUpdates(), mEvento.getGuests().size());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mRevealLayout.setVisibility(View.INVISIBLE);
        App.updateEventVisibility(mId, true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.updateEventVisibility(mId, false);
    }

    @Override
    public void onBackPressed() {
        if (mAnimationInProgress) {
            return;
        }

        if (!getDynamicView().isContentShown()) {
            getDynamicView().hide();
            cancelTimer();
        } else if (mViewPager.getCurrentItem() != 0) {
            mViewPager.setCurrentItem(0, true);
        } else if (isTaskRoot()) {
            startActivity(Act_Home.newIntent(0));
            finish();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        if (!mEvento.isMyselfPresent()) {
            inflater.inflate(R.menu.menu_evento_main_abbandonato, menu);
        } else {
            inflater.inflate(R.menu.menu_evento_main, menu);
            MenuItem invitare = menu.findItem(R.id.menu_partecipanti);
            MenuItem modifica = menu.findItem(R.id.menu_modifica);
            MenuItem annulla = menu.findItem(R.id.menu_annulla);
            MenuItem conferma = menu.findItem(R.id.menu_conferma);
            MenuItem ripristina = menu.findItem(R.id.menu_ripristina);
            if (!mEvento.getAdminUuid().equals(AccountProvider.getInstance().getAccountId())) {
                modifica.setVisible(false);
                annulla.setVisible(false);
                conferma.setVisible(false);
                ripristina.setVisible(false);
            } else if (mEvento.getStatus() == Constants.EVENTO_ANNULLATO) {
                annulla.setVisible(false);
                conferma.setVisible(false);
                ripristina.setVisible(true);
            } else if (mEvento.getStatus() == Constants.EVENTO_CONFERMATO) {
                conferma.setVisible(false);
                annulla.setVisible(true);
                ripristina.setVisible(false);
            } else {
                conferma.setVisible(true);
                annulla.setVisible(true);
                ripristina.setVisible(false);
            }

            invitare.setVisible(mCanAddGuests);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_partecipanti:
                if (mCanAddGuests) {
                    AnalitycsProvider.getInstance().logCustomType(mFirebaseAnalytics, "dotsActivityAction", "addParticipants");
                    startActivity(Act_MainAggiungiPartecipante.newIntent(mId, true));
                } else {
                    Toast.makeText(this, getString(R.string.main_evento_no_permission_add_users), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.menu_modifica:
                if (!mAnimationInProgress) {
                    AnalitycsProvider.getInstance().logCustomType(mFirebaseAnalytics, "dotsActivityAction", "modify");
                    startActivity(Act_EventoModifica.newIntent(mId));
                }
                break;
            case R.id.menu_conferma:
                confermaEvento();
                break;
            case R.id.menu_annulla:
                annullaEvento();
                break;
            case R.id.menu_ripristina:
                AnalitycsProvider.getInstance().logCustomType(mFirebaseAnalytics, "dotsActivityAction", "organizeActivity");
                getDynamicView().showLoading();
                setRequestId(UUID.randomUUID().toString());
                registerSubscription(eventApi.changeStatus(getRequestId(), mId, new EventoCambioStatoApiModel(Constants.EVENTO_IN_PIANIFICAZIONE))
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<Event>() {
                            @Override
                            public void call(Event evento) {
                                startTimer();
                            }
                        }, new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                getDynamicView().hide();

                                APIError error = APIErrorUtils.parseError(Act_EventoMain.this, throwable);
                                Toast.makeText(Act_EventoMain.this, error.message(), Toast.LENGTH_LONG).show();
                            }
                        }));
                break;
            case R.id.menu_delete:
                if (!mAnimationInProgress) {
                    AnalitycsProvider.getInstance().logCustomType(mFirebaseAnalytics, "dotsActivityAction", "delete");
                    new MaterialDialog.Builder(this)
                            .title(getString(R.string.main_evento_elimina_title))
                            .content(getString(R.string.main_evento_elimina_content))
                            .positiveText(getString(R.string.general_elimina))
                            .negativeText(getString(R.string.general_annulla))
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    getDynamicView().showLoading();
                                    App.jobManager().addJobInBackground(new EventDeleteJob(mId));
                                }
                            })
                            .show();
                }
                break;
            case R.id.menu_esci_da_attivita:
                AnalitycsProvider.getInstance().logCustomType(mFirebaseAnalytics, "dotsActivityAction", "abandon");
                new MaterialDialog.Builder(this)
                        .title(getString(R.string.main_evento_esci_title))
                        .content(getString(R.string.main_evento_esci_content))
                        .positiveText(getString(R.string.general_abbandona))
                        .negativeText(getString(R.string.general_annulla))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                getDynamicView().showLoading();
                                setRequestId(UUID.randomUUID().toString());
                                eventApi.exit(getRequestId(), mId)
                                        .subscribeOn(Schedulers.newThread())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new Action1<Object>() {
                                            @Override
                                            public void call(Object obj) {
                                            }
                                        }, new Action1<Throwable>() {
                                            @Override
                                            public void call(Throwable throwable) {
                                                getDynamicView().hide();

                                                APIError error = APIErrorUtils.parseError(Act_EventoMain.this, throwable);
                                                Toast.makeText(Act_EventoMain.this, error.message(), Toast.LENGTH_SHORT).show();
                                            }
                                        });
                            }
                        }).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mEvento != null) {
            mEvento.removeAllChangeListeners();
        }
        mMessageDao.markAllAsReadAsync(true, mId);
        App.resetEventVisibility();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        intent.putExtra(PARAM_PAGE, mViewPager.getCurrentItem());
        super.onNewIntent(intent);
        recreate();
    }

    @Override
    protected void initialize() {
        // Init attività
        mEvento = new EventDao(realm).getEvent(mId);
        if (mEvento == null) {
            Toast.makeText(this, getString(R.string.general_error_evento_non_presente), Toast.LENGTH_LONG).show();
            finish();
            return;
        } else {
            mMessageDao = new MessageDao(realm);
            mPollDao = new PollDao(realm);
            mCanAddGuests = mEvento.isUsersCanInvite() || AccountProvider.getInstance().getAccountId().equals(mEvento.getAdminUuid());

            updateToolbarTitleAndImage(mEvento);
            mExitText.setVisibility(mEvento.isMyselfPresent() ? View.GONE : View.VISIBLE);

            mEvento.addChangeListener(new RealmChangeListener<Event>() {
                @Override
                public void onChange(@NonNull Event element) {
                    if (element.isValid()) {
                        invalidateOptionsMenu();
                        mCanAddGuests = element.isUsersCanInvite() || AccountProvider.getInstance().getAccountId().equals(element.getAdminUuid());
                        updateToolbarTitleAndImage(element);
                        updateTabLayout(element);
                        mExitText.setVisibility(element.isMyselfPresent() ? View.GONE : View.VISIBLE);
                    }
                }
            });
        }

        // Init Tablayout e Viewpager
        mViewPager.setSwipe(true);
        mViewPager.setAdapter(new EventoMainPagerAdapter(this, getSupportFragmentManager(), mId));
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.setCurrentItem(mPage);
        mTabLayout.setupWithViewPager(mViewPager);
        mTabLayout.setWithRoundBorder(true);
        mTabLayout.addOnTabSelectedListener(new MyTabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(MyTabLayout.Tab tab) {
                if (tab.getCustomView() != null) {
                    tab.getCustomView().findViewById(R.id.container).setAlpha(1f);
                }
                Utils.hideKeyboard(Act_EventoMain.this);
                App.updateEventPage(tab.getPosition());
            }

            @Override
            public void onTabUnselected(MyTabLayout.Tab tab) {
                if (tab.getCustomView() != null) {
                    tab.getCustomView().findViewById(R.id.container).setAlpha(0.3f);
                }
                if (tab.getPosition() == 0) {
                    mMessageDao.markInfoAsReadAsync(mId);
                } else if (tab.getPosition() == 1) {
                    mMessageDao.markChatAsReadAsync(true, mId);
                } else if (tab.getPosition() == 2) {
                    mMessageDao.markPollAsReadAsync(mId);
                } else if (tab.getPosition() == 3) {
                    mMessageDao.markUsersAsReadAsync(true, mId);
                }
            }

            @Override
            public void onTabReselected(MyTabLayout.Tab tab) {

            }
        });

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                mFabLayout.updateAnimationValue(position, positionOffset);
            }

            @Override
            public void onPageSelected(final int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        updateTab(0, true, mEvento.getNumInfoUpdates(), 0);
        updateTab(1, true, mEvento.getNumNewMessages(), 0);
        updateTab(2, true, mEvento.getNumPollUpdates(), mPollDao.getNumberOfPolls(mEvento.getUuid()));
        updateTab(3, true, mEvento.getNumUserUpdates(), mEvento.getGuests().size());

        initFabLayout();
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onEvent(StartUrlFindEvent event) {
        if (!mMessageIdsForUrlFind.contains(event.getMessageId())) {
            mMessageIdsForUrlFind.add(event.getMessageId());
            new UrlPreviewJob(event.getMessageId(), event.getUrl(), null).execute();
        }
    }

    @Subscribe
    public void onEvent(UserSelectedEvent event) {
        if (!mAnimationInProgress) {
            AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "viewProfile", "activity");
            startActivity(Act_Profilo.newIntent(event.getId()));
        }
    }

    @Subscribe
    public void onEvent(UserImageSelectedEvent event) {
        if (!mAnimationInProgress) {
            int[] location = new int[2];
            event.getView().getLocationInWindow(location);
            startActivity(Act_ContactPhotoPopup.newIntent(event.getId(), location[0], location[1], event.getView().getWidth() / 2));
            overridePendingTransition(0, 0);
        }
    }

    @Subscribe
    public void onEvent(ImageSelectedEvent event) {
        if (mAnimationInProgress) {
            return;
        }

        if (event.getPath() != null) {
            if (Utils.checkIfImageExists(event)) { //TODO non funziona
                Intent intent = Act_ImageDetail.newIntent(event.getPath(), event.getCaption());
                if (Utils.isLollipop()) {
                    Pair<View, String> p1 = Pair.create(event.getView(), getString(R.string.transition_image));
                    //noinspection unchecked
                    ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(this, p1);
                    startActivity(intent, optionsCompat.toBundle());
                } else {
                    startActivity(intent);
                }
            } else {
                Toast.makeText(this, R.string.general_error_image_not_found, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, R.string.general_error_image_not_found, Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onEvent(AvatarSelectedEvent event) {
        if (mAnimationInProgress) {
            return;
        }

        int placeholder = event.isEvent() ? R.drawable.avatar_attivita : R.drawable.avatar_gruppo;
        Intent intent = Act_ImageDetail.newIntentSharedAnimation(event.getPath(), 0, placeholder);
        if (Utils.isLollipop()) {
            Pair<View, String> p1 = Pair.create(event.getView(), getString(R.string.transition_image));
            //noinspection unchecked
            ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(this, p1);
            startActivity(intent, optionsCompat.toBundle());
        } else {
            startActivity(intent);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(final StartImageDownloadEvent event) {
        if (SettingUtils.checkIfCanDownload(this) || event.isIgnoraControllo()) {
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(@NonNull Realm realm) {
                    final Message messaggio = new MessageDao(realm).getMessage(event.getIdMessaggio());
                    if (messaggio == null) {
                        return;
                    }

                    messaggio.setImageState(Constants.IMAGE_STATE_DOWNLOADING);
                    App.jobManager().addJobInBackground(new ImageDownloadJob(messaggio.getUuid(), messaggio.getMediaUrl()));
                }
            });
        } else {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(@NonNull Realm realm) {
                    final Message messaggio = new MessageDao(realm).getMessage(event.getIdMessaggio());
                    if (messaggio == null) {
                        return;
                    }

                    messaggio.setImageState(Constants.IMAGE_STATE_DOWNLOAD_FAILED);
                }
            });
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ApiCallResultEvent event) {
        if (event.getRequestId().equals(getRequestId())) {
            cancelTimer();
            getDynamicView().hide();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(VotePollItemClickedEvent event) {
        AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "votePollItem", "activity");
        VoteUtils.votePollItem(this, realm, event.getSondaggioVoceId(), event.isChecked());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(VoteEventClickedEvent event) {
        voteEvent(event.getType());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(VoteInterestEventClickedEvent event) {
        voteInterestEvent(event.getType());
    }

    @Subscribe
    public void onEvent(AddItemToPollEvent event) {
        if (event.isSpecial()) {
            if (event.isWhere()) {
                startActivity(Act_Dove.newIntentFromPoll(mId, event.getSondaggioId(), getString(R.string.sondaggio_nuovo_dove_title), getString(R.string.sondaggio_nuovo_dove_subtitle)));
            } else {
                startActivity(Act_Quando.newIntentFromPoll(mId, event.getSondaggioId(), getString(R.string.sondaggio_nuovo_quando_title), getString(R.string.sondaggio_nuovo_quando_subtitle)));
            }
        } else {
            AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "createPollItem", "activity");
            startActivity(Act_AggiungiVoceSondaggio.newIntent(event.getSondaggioId()));
            overridePendingTransition(R.anim.slide_in_bottom_slow, R.anim.fade_out);
        }
    }

    @Subscribe
    public void onEvent(PollMoreSelectedEvent event) {
        final Poll sondaggio = mPollDao.getPoll(event.getId());
        if (sondaggio == null) {
            return;
        }
        AnalitycsProvider.getInstance().logCustom(mFirebaseAnalytics, "dotsPoll");

        PopupMenu popupMenu = new PopupMenu(this, event.getView());
        popupMenu.getMenuInflater().inflate(R.menu.menu_sondaggio_dettaglio, popupMenu.getMenu());
        MenuItem eliminaSondaggio = popupMenu.getMenu().findItem(R.id.menu_delete);
        MenuItem chiudiSondaggio = popupMenu.getMenu().findItem(R.id.menu_close);
        if (sondaggio.getType() == Constants.POLL_TYPE_WHERE || sondaggio.getType() == Constants.POLL_TYPE_WHEN) {
            if (sondaggio.getType() == Constants.POLL_TYPE_WHEN) {
                chiudiSondaggio.setTitle(getString(R.string.menu_chiudi_sondaggio_when));
            } else {
                chiudiSondaggio.setTitle(getString(R.string.menu_chiudi_sondaggio_where));
            }
            popupMenu.getMenu().removeItem(R.id.menu_delete);
            if (!sondaggio.getCreatorUuid().equals(AccountProvider.getInstance().getAccountId()) && !mEvento.getAdminUuid().equals(AccountProvider.getInstance().getAccountId())) {
                chiudiSondaggio.setEnabled(false);
            } else {
                chiudiSondaggio.setEnabled(true);
            }
        } else {
            popupMenu.getMenu().removeItem(R.id.menu_close);
            if (!sondaggio.getCreatorUuid().equals(AccountProvider.getInstance().getAccountId()) && !mEvento.getAdminUuid().equals(AccountProvider.getInstance().getAccountId())) {
                eliminaSondaggio.setEnabled(false);
            } else {
                eliminaSondaggio.setEnabled(true);
            }
        }

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_delete:
                        AnalitycsProvider.getInstance().logCustomType(mFirebaseAnalytics, "dotsPollAction", "delete");
                        new MaterialDialog.Builder(Act_EventoMain.this)
                                .title(getString(R.string.sondaggio_popup_elimina_titolo))
                                .content(getString(R.string.sondaggio_popup_elimina_messaggio))
                                .positiveText(getString(R.string.general_elimina))
                                .negativeText(getString(R.string.general_annulla))
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        setRequestId(UUID.randomUUID().toString());
                                        getDynamicView().showLoading();
                                        registerSubscription(pollApi.deletePoll(getRequestId(), sondaggio.getEventUuid(), sondaggio.getUuid())
                                                .subscribeOn(Schedulers.newThread())
                                                .observeOn(AndroidSchedulers.mainThread())
                                                .subscribe(new Action1<Object>() {
                                                    @Override
                                                    public void call(Object obj) {
                                                        startTimer();
                                                    }
                                                }, new Action1<Throwable>() {
                                                    @Override
                                                    public void call(Throwable throwable) {
                                                        getDynamicView().hide();

                                                        APIError error = APIErrorUtils.parseError(Act_EventoMain.this, throwable);
                                                        Toast.makeText(Act_EventoMain.this, error.message(), Toast.LENGTH_SHORT).show();
                                                    }
                                                }));
                                    }
                                })
                                .show();
                        break;
                    case R.id.menu_close:
                        startActivity(Act_SondaggioChiudi.newIntent(mId, sondaggio.getType() == Constants.POLL_TYPE_WHERE));
                        break;
                }
                return true;
            }
        });

        popupMenu.show();
    }

    @Subscribe
    public void onEvent(PollSelectedEvent event) {
        startActivity(Act_DettaglioSondaggio.newIntent(event.getId()));
        overridePendingTransition(0, 0);
    }

    @Subscribe
    public void onEvent(PollItemMoreClickedEvent event) {
        AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "viewPollItem", "activity");
        startActivity(Act_DettaglioSondaggioVoci.newIntent(event.getSondaggioId(), event.getSondaggioVoceId()));
        overridePendingTransition(0, 0);
    }

    @Subscribe(sticky = true)
    public void onEvent(ShowPopupInvitedEvent event) {
        if (Prefs.getString(Constants.SHOW_INVITE_POPUP_EVENTO + mId) != null) {
            EventBus.getDefault().removeStickyEvent(event);
            showInvitePopup();
        }
    }

    @Subscribe
    public void onEvent(final EventInfoSelectedEvent event) {
        PopupMenu popupMenu = new PopupMenu(this, event.getView());
        popupMenu.setGravity(Gravity.END);
        popupMenu.getMenuInflater().inflate(R.menu.menu_evento_info_selected, popupMenu.getMenu());
        MenuItem apriSondaggio = popupMenu.getMenu().findItem(R.id.menu_open_poll);
        MenuItem mostraSuMappa = popupMenu.getMenu().findItem(R.id.menu_show_map);
        MenuItem aggiungiACalendario = popupMenu.getMenu().findItem(R.id.menu_add_calendar);

        if (mEvento.getStatus() == Constants.EVENTO_ANNULLATO) {
            apriSondaggio.setVisible(false);
        } else {
            apriSondaggio.setVisible(true);
            apriSondaggio.setEnabled(mEvento.getStatus() == Constants.EVENTO_IN_PIANIFICAZIONE && AccountProvider.getInstance().getAccountId().equals(mEvento.getAdminUuid()));
        }
        if (event.isWhere()) {
            mostraSuMappa.setVisible(true);
            aggiungiACalendario.setVisible(false);
        } else {
            mostraSuMappa.setVisible(false);
            aggiungiACalendario.setVisible(true);
        }

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_open_poll:
                        openPoll(event.isWhere());
                        break;
                    case R.id.menu_show_map:
                        Uri mapUri = Uri.parse("geo:" + mEvento.getEventInfo().getLat() + "," + mEvento.getEventInfo().getLng() + "?q=" + mEvento.getEventInfo().getWhere());
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, mapUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        if (mapIntent.resolveActivity(getPackageManager()) != null) {
                            startActivity(mapIntent);
                        } else {
                            Toast.makeText(Act_EventoMain.this, getString(R.string.general_error_no_app_found), Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case R.id.menu_add_calendar:
                        Intent intent = new Intent(Intent.ACTION_EDIT);
                        intent.setType("vnd.android.cursor.item/event");
                        DateTime from = mEvento.getEventInfo().generaleDateTimeFrom();
                        DateTime to = mEvento.getEventInfo().generaleDateTimeTo();
                        DateTime single = null;
                        if (from == null && to != null) {
                            single = to;
                        } else if (from != null && to == null) {
                            single = from;
                        } else if (from == null) {
                            break;
                        }
                        intent.putExtra("beginTime", single != null ? single.toDate().getTime() : from.toDate().getTime());
                        intent.putExtra("endTime", single != null ? single.toDate().getTime() : to.toDate().getTime());
                        intent.putExtra("title", mEvento.getEventInfo().getWhat());
                        startActivity(intent);
                        break;
                }
                return true;
            }
        });

        popupMenu.show();
    }

    @Subscribe
    public void onEvent(ShowVotiEvent event) {
        startActivity(Act_EventoDettaglioVoti.newIntent(mId));
        overridePendingTransition(0, 0);
    }

    @Subscribe
    public void onEvent(ShowVotiInteresseEvent event) {
        startActivity(Act_EventoDettaglioVoti.newIntentInteressati(mId));
        overridePendingTransition(0, 0);
    }

    @Subscribe
    public void onEvent(EventoUpdateStatus event) {
        if (event.getStatus() == Constants.EVENTO_ANNULLATO) {
            annullaEvento();
        } else if (event.getStatus() == Constants.EVENTO_CONFERMATO) {
            confermaEvento();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventDeleteEvent event) {
        getDynamicView().hide();
        if (event.isSuccess()) {
            finish();
        } else {
            Toast.makeText(this, getString(R.string.general_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    // FUNCTIONS

    /**
     * Aggiorno il tab
     *
     * @param position        posizione del tab
     * @param isCreate        creo il tab
     * @param numNotification numero notifiche
     */
    @SuppressWarnings("ConstantConditions")
    @SuppressLint("InflateParams")
    private void updateTab(int position, boolean isCreate, long numNotification, int numItems) {
        View view;
        if (isCreate) {
            view = LayoutInflater.from(this).inflate(R.layout.system_tab, null);
            view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

            TextView text = view.findViewById(R.id.tab_text);
            ImageView image = view.findViewById(R.id.tab_image);
            ViewGroup container = view.findViewById(R.id.container);
            switch (position) {
                case 0:
                    text.setText(getString(R.string.main_evento_info));
                    image.setImageResource(R.drawable.tab_ico_proposte);
                    container.setAlpha(mViewPager.getCurrentItem() == 0 ? 1f : 0.3f);
                    break;
                case 1:
                    text.setText(getString(R.string.main_evento_chat));
                    image.setImageResource(R.drawable.tab_ico_chat);
                    container.setAlpha(mViewPager.getCurrentItem() == 1 ? 1f : 0.3f);
                    break;
                case 2:
                    text.setText(String.format(Locale.getDefault(), "%d %s", numItems, getResources().getQuantityString(R.plurals.main_evento_sondaggi, numItems)));
                    image.setImageResource(R.drawable.ico_sondaggi);
                    container.setAlpha(mViewPager.getCurrentItem() == 2 ? 1f : 0.3f);
                    break;
                default:
                    text.setText(String.format(Locale.getDefault(), "%d %s", numItems, getResources().getQuantityString(R.plurals.main_evento_invitati, numItems)));
                    image.setImageResource(R.drawable.ico_invitati);
                    container.setAlpha(mViewPager.getCurrentItem() == 3 ? 1f : 0.3f);
                    break;
            }
        } else {
            view = mTabLayout.getTabAt(position).getCustomView();
            TextView text = view.findViewById(R.id.tab_text);
            switch (position) {
                case 2:
                    text.setText(String.format(Locale.getDefault(), "%d %s", numItems, getResources().getQuantityString(R.plurals.main_evento_sondaggi, numItems)));
                    break;
                case 3:
                    text.setText(String.format(Locale.getDefault(), "%d %s", numItems, getResources().getQuantityString(R.plurals.main_evento_invitati, numItems)));
                    break;
            }
        }

        AnimatedTextView badge = view.findViewById(R.id.tab_badge);
        if (isCreate) {
            badge.setTextColor(ContextCompat.getColor(this, R.color.textLightBlue));
            badge.setTextSize(11);
        }
        badge.setVisibility(numNotification == 0 ? View.GONE : View.VISIBLE);
        badge.setValue(numNotification);

        if (isCreate) {
            mTabLayout.getTabAt(position).setCustomView(view);
        }
    }

    /**
     * Gestisco lo scroll del viewpager a seconda della tastiera
     *
     * @param oldBottom oldBottom
     * @param bottom    bottom
     */
    public void onLayoutChange(int oldBottom, int bottom) {
        if (bottom > oldBottom) {
            onLayoutChange(true);
        } else if (bottom < oldBottom) {
            onLayoutChange(false);
        }
    }

    /**
     * Gestisco lo scroll del viewpager
     *
     * @param enable abilita scroll
     */
    public void onLayoutChange(boolean enable) {
        if (enable) {
            mViewPager.setSwipe(true);
        } else {
            mViewPager.setSwipe(false);
        }
    }

    /**
     * Aggiorna titolo e l'avatar della toolbar
     *
     * @param evento evento
     */
    private void updateToolbarTitleAndImage(Event evento) {
        if (mToolbar != null && evento != null && evento.getTitle() != null) {
            setToolbarTitle(evento.getTitle());
            if (evento.getStatus() == Constants.EVENTO_ANNULLATO) {
                getToolbarStatus().setVisibility(View.VISIBLE);
                getToolbarTitle().setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            } else {
                getToolbarStatus().setVisibility(View.GONE);
                getToolbarTitle().setPaintFlags(0);
            }
        }
    }

    /**
     * Aggiorno badge tablayout
     *
     * @param evento evento
     */
    private void updateTabLayout(Event evento) {
        updateTab(0, false, evento.getNumInfoUpdates(), 0);
        updateTab(1, false, evento.getNumNewMessages(), 0);
        updateTab(2, false, evento.getNumPollUpdates(), mPollDao.getNumberOfPolls(evento.getUuid()));
        updateTab(3, false, evento.getNumUserUpdates(), evento.getGuests().size());

        initFabLayout();
    }

    /**
     * Imposto il FabLayout
     */
    private void initFabLayout() {
        SparseArray<String> texts = new SparseArray<>();
        texts.put(2, getString(R.string.system_fab_poll));
        texts.put(3, getString(R.string.system_fab_participant));
        mFabLayout.setTextResources(texts);
        if (mEvento.isMyselfPresent()) {
            mFabLayout.setPositionTypes(Arrays.asList(BorderFabLayout.POSITION_TYPE.HIDE, BorderFabLayout.POSITION_TYPE.HIDE,
                    BorderFabLayout.POSITION_TYPE.DEFAULT, mCanAddGuests ? BorderFabLayout.POSITION_TYPE.DEFAULT : BorderFabLayout.POSITION_TYPE.DISABLE));
        } else {
            mFabLayout.setPositionTypes(Arrays.asList(BorderFabLayout.POSITION_TYPE.HIDE, BorderFabLayout.POSITION_TYPE.HIDE,
                    BorderFabLayout.POSITION_TYPE.DISABLE, BorderFabLayout.POSITION_TYPE.DISABLE));
        }

        mFabLayout.updateAnimationValue(mViewPager.getCurrentItem(), 0);
    }

    /**
     * Esegue l'animazione di reveal al click del crea proposta/sondaggio
     *
     * @param view   view
     * @param intent intent
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void animateReveal(final View view, final Intent intent) {
        if (mAnimationInProgress) {
            return;
        }

        if (!Utils.isLollipop()) {
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_bottom_slow, R.anim.fade_out);
            return;
        }

        mAnimationInProgress = true;

        // Calcolo posizione da cui far partire l'animazione di reveal
        float cx = (int) (view.getX() + view.getWidth() / 2);
        float cy = (int) (view.getY() + view.getHeight() / 2);
        // Radius finale
        float finalRadius = (float) Math.hypot(cx, cy);
        float initialRadius = view.getWidth() / 2;
        if (view instanceof BorderFabLayout) {
            initialRadius = ((BorderFabLayout) view).getFabButton().getWidth() / 2;
            cy += ((BorderFabLayout) view).getFabButton().getY();
        }

        // Creo l'animazione
        AnimatorSet animatorSet = new AnimatorSet();
        Animator animator = ViewAnimationUtils.createCircularReveal(mRevealLayout, (int) cx, (int) cy, initialRadius, finalRadius);
        ValueAnimator valueAnimator = ObjectAnimator.ofInt(mRevealLayout, "backgroundColor", Color.WHITE, Color.WHITE, mColorGradientEnd);
        valueAnimator.setEvaluator(new ArgbEvaluator());
        animatorSet.playTogether(animator, valueAnimator);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mRevealLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mAnimationInProgress = false;
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });
        animatorSet.start();
    }

    /**
     * Voto la proposta
     *
     * @param type tipo del voto
     */
    private void voteEvent(final int type) {
        final VoteDao voteDao = new VoteDao(realm);
        final UserDao userDao = new UserDao(realm);

        try {
            Vote voto = voteDao.getVoteEventFromUser(mId, AccountProvider.getInstance().getAccountId());
            if (voto == null) {
                voto = new Vote();
                voto.setUuid(UUID.randomUUID().toString());
                voto.setCreatedAt(new Date().getTime());
                voto.setCreator(userDao.getUser(AccountProvider.getInstance().getAccountId()));
                voto.setLoading(true);
                voto.setEventUuid(mId);
                voto.setType(type);

                final Vote finalVoto = voto;
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(@NonNull Realm realm) {
                        Vote newVoto = voteDao.createMyVoteEvent(finalVoto, mEvento);
                        VotiService.sendVote(Act_EventoMain.this, newVoto.getUuid(), mId);
                    }
                });
            } else {
                if (voto.isLoading()) {
                    return;
                }

                final Vote finalVoto1 = voto;
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(@NonNull Realm realm) {
                        finalVoto1.setCreatedAt(new Date().getTime());
                        finalVoto1.setLoading(true);
                        finalVoto1.setType(type);
                        VotiService.sendVote(Act_EventoMain.this, finalVoto1.getUuid(), mId);
                    }
                });
            }

            AnalitycsProvider.getInstance().logCustom(mFirebaseAnalytics, "voteActivity");
        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(this, getString(R.string.general_error_realm), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Voto l'interesse per la proposta
     *
     * @param type tipo del voto
     */
    private void voteInterestEvent(final int type) {
        final VoteDao voteDao = new VoteDao(realm);
        final UserDao userDao = new UserDao(realm);

        try {
            Vote voto = voteDao.getVoteInterestEventFromUser(mId, AccountProvider.getInstance().getAccountId());
            if (voto == null) {
                voto = new Vote();
                voto.setUuid(UUID.randomUUID().toString());
                voto.setCreatedAt(new Date().getTime());
                voto.setCreator(userDao.getUser(AccountProvider.getInstance().getAccountId()));
                voto.setLoading(true);
                voto.setEventUuid(mId);
                voto.setType(type);
                voto.setVoteType(Constants.VOTE_TYPE_INTEREST);

                final Vote finalVoto = voto;
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(@NonNull Realm realm) {
                        Vote newVoto = voteDao.createMyVoteInterestEvent(finalVoto, mEvento);
                        VotiService.sendVote(Act_EventoMain.this, newVoto.getUuid(), mId);
                    }
                });
            } else {
                if (voto.isLoading()) {
                    return;
                }

                final Vote finalVoto1 = voto;
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(@NonNull Realm realm) {
                        finalVoto1.setCreatedAt(new Date().getTime());
                        finalVoto1.setLoading(true);
                        finalVoto1.setType(type);
                        VotiService.sendVote(Act_EventoMain.this, finalVoto1.getUuid(), mId);
                    }
                });
            }

            AnalitycsProvider.getInstance().logCustom(mFirebaseAnalytics, "interestActivity");
        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(this, getString(R.string.general_error_realm), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Mostro il popup per l'invito degli utenti non ancora in yeap
     */
    private void showInvitePopup() {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                new MaterialDialog.Builder(Act_EventoMain.this)
                        .title(getString(R.string.system_invite_partecipanti_in_yeap_title))
                        .content(getString(R.string.system_invite_partecipanti_in_yeap_content))
                        .positiveText(getString(R.string.system_invite_partecipanti_in_yeap_conferma))
                        .negativeText(getString(R.string.system_invite_partecipanti_in_yeap_annulla))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                Intent smsIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + Prefs.getString(Constants.SHOW_INVITE_POPUP_EVENTO + mId)));
                                smsIntent.putExtra("sms_body", getString(R.string.system_invita_testo_evento));
                                startActivity(smsIntent);
                                Prefs.remove(Constants.SHOW_INVITE_POPUP_EVENTO + mId);
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                Prefs.remove(Constants.SHOW_INVITE_POPUP_EVENTO + mId);
                            }
                        })
                        .cancelable(false)
                        .show();
            }
        });
    }

    private void annullaEvento() {
        AnalitycsProvider.getInstance().logCustomType(mFirebaseAnalytics, "dotsActivityAction", "cancelActivity");
        new MaterialDialog.Builder(this)
                .title(getString(R.string.main_evento_annulla_evento_popup_title))
                .content(getString(R.string.main_evento_annulla_evento_popup_message))
                .positiveText(getString(R.string.general_annulla_attivita))
                .negativeText(getString(R.string.general_no))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        getDynamicView().showLoading();
                        setRequestId(UUID.randomUUID().toString());
                        registerSubscription(eventApi.changeStatus(getRequestId(), mId, new EventoCambioStatoApiModel(Constants.EVENTO_ANNULLATO))
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new Action1<Event>() {
                                    @Override
                                    public void call(Event evento) {
                                        startTimer();
                                    }
                                }, new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable throwable) {
                                        getDynamicView().hide();

                                        APIError error = APIErrorUtils.parseError(Act_EventoMain.this, throwable);
                                        Toast.makeText(Act_EventoMain.this, error.message(), Toast.LENGTH_LONG).show();
                                    }
                                }));
                    }
                })
                .show();
    }

    private void confermaEvento() {
        AnalitycsProvider.getInstance().logCustomType(mFirebaseAnalytics, "dotsActivityAction", "confirmActivity");
        startActivity(Act_EventoConferma.newIntent(mId));
    }

    private void openPoll(final boolean isWhere) {
        mOpenPollCheck = true;
        String permessoText;
        if (isWhere) {
            permessoText = getString(R.string.nuova_evento_crea_sondaggio_dove_permesso);
        } else {
            permessoText = getString(R.string.nuova_evento_crea_sondaggio_quando_permesso);
        }

        new MaterialDialog.Builder(this)
                .title(getString(R.string.sondaggio_apri_title))
                .checkBoxPrompt(permessoText, true, new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        mOpenPollCheck = isChecked;
                    }
                })
                .positiveText(getString(R.string.general_apri_sondaggio))
                .negativeText(getString(R.string.general_annulla))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        setRequestId(UUID.randomUUID().toString());

                        SondaggioApiModel sondaggioApiModel = new SondaggioApiModel();
                        sondaggioApiModel.setPollItems(new ArrayList<PollItem>());
                        sondaggioApiModel.setUsersCanAddItems(mOpenPollCheck);

                        final Observable<Object> objectObservable;
                        if (isWhere) {
                            sondaggioApiModel.setQuestion(getString(R.string.sondaggio_dove_question));
                            PollItem pollItem = new PollItem();
                            pollItem.updateDoveInfo(mEvento.getEventInfo().generateDoveModel());
                            sondaggioApiModel.getPollItems().add(pollItem);
                            objectObservable = eventApi.openWherePoll(getRequestId(), mId, sondaggioApiModel);
                        } else {
                            sondaggioApiModel.setQuestion(getString(R.string.sondaggio_quando_question));
                            PollItem pollItem = new PollItem();
                            pollItem.updateQuandoInfo(mEvento.getEventInfo().generateQuandoModel());
                            sondaggioApiModel.getPollItems().add(pollItem);
                            objectObservable = eventApi.openWhenPoll(getRequestId(), mId, sondaggioApiModel);
                        }

                        getDynamicView().showLoading();
                        registerSubscription(objectObservable
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new Action1<Object>() {
                                    @Override
                                    public void call(Object o) {
                                        startTimer();
                                    }
                                }, new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable throwable) {
                                        getDynamicView().hide();

                                        APIError error = APIErrorUtils.parseError(Act_EventoMain.this, throwable);
                                        Toast.makeText(Act_EventoMain.this, error.message(), Toast.LENGTH_LONG).show();
                                    }
                                }));
                    }
                })
                .show();
    }
}
