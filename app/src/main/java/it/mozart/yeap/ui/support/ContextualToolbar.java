package it.mozart.yeap.ui.support;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;

public class ContextualToolbar implements ActionMode.Callback {

    private int menuResource;
    private onCallbackListener listener;

    public ContextualToolbar(int menuResource, onCallbackListener listener) {
        this.menuResource = menuResource;
        this.listener = listener;
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        mode.getMenuInflater().inflate(menuResource, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        if (listener != null)
            listener.onItemSelected(item);
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        if (listener != null)
            listener.onDestroy();
    }

    public interface onCallbackListener {
        void onItemSelected(MenuItem item);

        void onDestroy();
    }
}
