package it.mozart.yeap.ui.events.main.events;

public class PollSelectedEvent {

    private String id;

    public PollSelectedEvent(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
