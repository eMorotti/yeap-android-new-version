package it.mozart.yeap.ui.events.main.events;

public class PollItemMoreClickedEvent {

    private String sondaggioId;
    private String sondaggioVoceId;

    public PollItemMoreClickedEvent(String sondaggioId, String sondaggioVoceId) {
        this.sondaggioId = sondaggioId;
        this.sondaggioVoceId = sondaggioVoceId;
    }

    public String getSondaggioId() {
        return sondaggioId;
    }

    public String getSondaggioVoceId() {
        return sondaggioVoceId;
    }
}
