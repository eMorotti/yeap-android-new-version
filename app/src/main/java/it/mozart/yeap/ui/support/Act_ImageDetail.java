package it.mozart.yeap.ui.support;

import android.content.Intent;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.SharedElementCallback;
import android.transition.Transition;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.ui.base.BaseActivityWithSyncService;
import it.mozart.yeap.ui.views.TouchImageView;
import it.mozart.yeap.utility.Utils;

public class Act_ImageDetail extends BaseActivityWithSyncService {

    @BindView(R.id.status_bar)
    View mStatusBar;
    @BindView(R.id.bg)
    View mBg;
    @BindView(R.id.image)
    TouchImageView mImageView;
    @BindView(R.id.caption)
    TextView mCaptionView;

    // SUPPORT

    private GestureDetector mGestureDetector;
    private String mImage;
    private String mCaption;
    private boolean mIgnoreFirst;
    private float mInitialScale;
    private float mCornerRadius;
    private float mCurrentCornerRadius;
    private int mPlaceholder;

    private static final String PARAM_IMAGE = "paramImage";
    private static final String PARAM_PLACEHOLDER = "paramPlaceholder";
    private static final String PARAM_CAPTION = "paramCaption";
    private static final String PARAM_CORNER_RADIUS = "paramCornerRadius";

    // SYSTEM

    public static Intent newIntent(String image) {
        Intent intent = new Intent(App.getContext(), Act_ImageDetail.class);
        intent.putExtra(PARAM_IMAGE, image);
        return intent;
    }

    public static Intent newIntent(String image, int placeholder) {
        Intent intent = new Intent(App.getContext(), Act_ImageDetail.class);
        intent.putExtra(PARAM_IMAGE, image);
        intent.putExtra(PARAM_PLACEHOLDER, placeholder);
        return intent;
    }

    public static Intent newIntentSharedAnimation(String image, float cornerRadius, int placeholder) {
        Intent intent = new Intent(App.getContext(), Act_ImageDetail.class);
        intent.putExtra(PARAM_IMAGE, image);
        intent.putExtra(PARAM_CORNER_RADIUS, cornerRadius);
        intent.putExtra(PARAM_PLACEHOLDER, placeholder);
        return intent;
    }

    public static Intent newIntent(String image, String caption) {
        Intent intent = new Intent(App.getContext(), Act_ImageDetail.class);
        intent.putExtra(PARAM_IMAGE, image);
        if (caption != null)
            intent.putExtra(PARAM_CAPTION, caption);
        return intent;
    }

    public static Intent newIntentSharedAnimation(String image, String caption, float cornerRadius) {
        Intent intent = new Intent(App.getContext(), Act_ImageDetail.class);
        intent.putExtra(PARAM_IMAGE, image);
        if (caption != null)
            intent.putExtra(PARAM_CAPTION, caption);
        intent.putExtra(PARAM_CORNER_RADIUS, cornerRadius);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);

            if (Utils.isLollipop()) {
                mStatusBar.getLayoutParams().height = Utils.getStatusBarHeight(this);
            }
        }

        if (Utils.isLollipop()) {
            postponeEnterTransition();

            if (mCornerRadius == 0) {
                // Ignoro il clip nel caso non ci siano modifiche al bordi TODO problema se non lo metto (da sistemare poi)
                mImageView.setIgnoreClip(true);
            }

            mCurrentCornerRadius = mImageView.getCornerRadius();
            setEnterSharedElementCallback(new SharedElementCallback() {
                @Override
                public void onSharedElementStart(List<String> sharedElementNames, List<View> sharedElements, List<View> sharedElementSnapshots) {
                    super.onSharedElementStart(sharedElementNames, sharedElements, sharedElementSnapshots);

                    ((TouchImageView) sharedElements.get(0)).setCornerRadius(mCornerRadius);
                    ((TouchImageView) sharedElements.get(0)).setStartCornerRadius(mCornerRadius);
                }

                @Override
                public void onSharedElementEnd(List<String> sharedElementNames, List<View> sharedElements, List<View> sharedElementSnapshots) {
                    super.onSharedElementEnd(sharedElementNames, sharedElements, sharedElementSnapshots);

                    ((TouchImageView) sharedElements.get(0)).setCornerRadius(mCurrentCornerRadius);
                    ((TouchImageView) sharedElements.get(0)).setStartCornerRadius(mCornerRadius);
                }
            });
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_image_detail;
    }

    @Override
    protected void initVariables() {
        mIgnoreFirst = true;
    }

    @Override
    protected void loadParameters(Bundle extras) {
        mImage = extras.getString(PARAM_IMAGE);
        mPlaceholder = extras.getInt(PARAM_PLACEHOLDER, 0);
        mCaption = extras.getString(PARAM_CAPTION);
        mCornerRadius = extras.getFloat(PARAM_CORNER_RADIUS, 0);
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        if (mImage != null) {
            inflater.inflate(R.menu.menu_image_detail, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_share:
                Uri uri = Uri.parse(mImage);
                if (!uri.isAbsolute()) {
                    uri = Uri.fromFile(new File(mImage));
                }
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_STREAM, uri);
                startActivity(Intent.createChooser(intent, getString(R.string.general_condividi)));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (Utils.isLollipop()) {
            // Catturo l'evento
            if (mGestureDetector != null) {
                mGestureDetector.onTouchEvent(ev);
            }
            if (ev.getAction() == MotionEvent.ACTION_UP) {
                // Controllo se si sia spostato almeno un po'
                if (mImageView.getTranslationY() > 100) {
                    if (Utils.isLollipop())
                        ActivityCompat.finishAfterTransition(this);
                    else
                        finish();
                } else {
                    // Ripristino stato iniziale
                    mImageView.animate().translationY(0).translationX(0).scaleY(1).scaleX(1).start();
                    mBg.animate().alpha(1f).start();
                    mToolbar.animate().alpha(1f).start();
                    if (mCaptionView.getVisibility() != View.GONE)
                        mCaptionView.animate().alpha(1f).start();
                }
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onBackPressed() {
        mCurrentCornerRadius = mImageView.getCornerRadius();
        super.onBackPressed();
    }

    @Override
    protected void initialize() {
        mCaptionView.setText(mCaption);
        mCaptionView.setVisibility(mCaption == null || mCaption.isEmpty() ? View.GONE : View.VISIBLE);

        if (mImage == null) {
            Glide.with(this).load(mPlaceholder).listener(new RequestListener<Integer, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, Integer model, Target<GlideDrawable> target, boolean isFirstResource) {
                    if (Utils.isLollipop()) {
                        startPostponedEnterTransition();
                    }
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, Integer model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                    if (Utils.isLollipop()) {
                        startPostponedEnterTransition();
                    }
                    return false;
                }
            }).into(mImageView);
        } else {
            Glide.with(this).load(mImage).listener(new RequestListener<String, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    if (Utils.isLollipop()) {
                        startPostponedEnterTransition();
                    }
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                    if (Utils.isLollipop()) {
                        startPostponedEnterTransition();
                    }
                    return false;
                }
            }).error(mPlaceholder).into(mImageView);
        }

        if (Utils.isLollipop()) {
            Transition sharedElementEnterTransition = getWindow().getSharedElementEnterTransition();
            sharedElementEnterTransition.addListener(new Transition.TransitionListener() {
                @Override
                public void onTransitionStart(Transition transition) {

                }

                @Override
                public void onTransitionEnd(Transition transition) {
                    float[] v = new float[9];
                    mImageView.getImageMatrix().getValues(v);
                    mInitialScale = v[Matrix.MSCALE_Y];
                    mGestureDetector = new GestureDetector(Act_ImageDetail.this, new GestureListener());
                }

                @Override
                public void onTransitionCancel(Transition transition) {

                }

                @Override
                public void onTransitionPause(Transition transition) {

                }

                @Override
                public void onTransitionResume(Transition transition) {

                }
            });
        } else {
            float[] v = new float[9];
            mImageView.getImageMatrix().getValues(v);
            mInitialScale = v[Matrix.MSCALE_Y];
        }
    }

    // DETECTOR

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            float[] v = new float[9];
            mImageView.getImageMatrix().getValues(v);

            if (mIgnoreFirst) {
                mIgnoreFirst = false;
            } else {
                if (v[Matrix.MSCALE_Y] < mInitialScale + 0.1f && v[Matrix.MSCALE_Y] > mInitialScale - 0.1f) {
                    if (mImageView.getTranslationY() >= 0) {
                        if (mImageView.getTranslationY() - distanceY / 2 < 0)
                            mImageView.setTranslationY(0);
                        else
                            mImageView.setTranslationY(mImageView.getTranslationY() - distanceY / 2);

                        mImageView.setTranslationX(mImageView.getTranslationX() - distanceX / 2);

                        float alpha = Math.abs(mImageView.getTranslationY() / (Utils.screenHeight(Act_ImageDetail.this) / 3));
                        alpha = Math.min(Math.max(alpha, 0f), 1f);
                        mBg.setAlpha(1f - alpha);
                        mToolbar.setAlpha(1f - alpha);
                        if (mCaptionView.getVisibility() != View.GONE)
                            mCaptionView.setAlpha(1f - alpha);
                        mImageView.setScaleX(Utils.toRange(1f - alpha, 0, 1f, 0.5f, 1f));
                        mImageView.setScaleY(Utils.toRange(1f - alpha, 0, 1f, 0.5f, 1f));
                    } else {
                        mImageView.setTranslationY(0);
                    }
                }
            }

            return super.onScroll(e1, e2, distanceX, distanceY);
        }
    }
}
