package it.mozart.yeap.ui.groups.main;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.MyTabLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.util.Pair;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Arrays;
import java.util.Locale;
import java.util.UUID;

import javax.inject.Inject;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.api.models.APIError;
import it.mozart.yeap.api.services.GroupApi;
import it.mozart.yeap.events.ApiCallResultEvent;
import it.mozart.yeap.events.EventSelectedEvent;
import it.mozart.yeap.events.ImageSelectedEvent;
import it.mozart.yeap.events.StartImageDownloadEvent;
import it.mozart.yeap.events.UserImageSelectedEvent;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.helpers.AnalitycsProvider;
import it.mozart.yeap.realm.Group;
import it.mozart.yeap.realm.Message;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.GroupDao;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.services.imageDownload.ImageDownloadJob;
import it.mozart.yeap.services.realmJob.GroupDeleteJob;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.ui.base.BaseActivityWithSyncService;
import it.mozart.yeap.ui.events.creation.Act_EventoCrea;
import it.mozart.yeap.ui.events.main.Act_EventoMain;
import it.mozart.yeap.ui.groups.main.adapters.GruppoMainPagerAdapter;
import it.mozart.yeap.ui.groups.main.events.GroupDeleteEvent;
import it.mozart.yeap.ui.groups.main.events.ShowPopupInvitedGroup;
import it.mozart.yeap.ui.home.Act_Home;
import it.mozart.yeap.ui.main.Act_MainAggiungiPartecipante;
import it.mozart.yeap.ui.profile.Act_Profilo;
import it.mozart.yeap.ui.support.Act_ContactPhotoPopup;
import it.mozart.yeap.ui.support.Act_ImageDetail;
import it.mozart.yeap.ui.support.events.UserSelectedEvent;
import it.mozart.yeap.ui.views.AnimatedTextView;
import it.mozart.yeap.ui.views.BorderFabLayout;
import it.mozart.yeap.ui.views.NotSwipeableViewPager;
import it.mozart.yeap.utility.APIErrorUtils;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.Prefs;
import it.mozart.yeap.utility.SettingUtils;
import it.mozart.yeap.utility.Utils;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class Act_GruppoMain extends BaseActivityWithSyncService {

    @BindView(R.id.status_bar)
    View mStatusBar;
    @BindView(R.id.viewpager)
    NotSwipeableViewPager mViewPager;
    @BindView(R.id.tab_layout)
    MyTabLayout mTabLayout;
    @BindView(R.id.reveal_layout)
    View mRevealLayout;
    @BindView(R.id.fab_layout)
    BorderFabLayout mFabLayout;

    @OnClick(R.id.fab_layout)
    void fabLayoutClicked() {
        if (mAnimationInProgress) {
            return;
        }

        if (mGruppo == null || !mGruppo.isMyselfPresent()) {
            return;
        }

        if (mViewPager.getCurrentItem() == 1) {
            AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "createActivity", "group");
            animateReveal(mFabLayout, Act_EventoCrea.newIntent(mId));
        } else if (mViewPager.getCurrentItem() == 2) {
            if (!mCanAddGuests) {
                Toast.makeText(this, getString(R.string.main_gruppo_no_permission_add_users), Toast.LENGTH_SHORT).show();
            } else {
                AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "addParticipants", "group");
                animateReveal(mFabLayout, Act_MainAggiungiPartecipante.newIntent(mId, false));
            }
        }
    }

    // SUPPORT

    @Inject
    Realm realm;
    @Inject
    GroupApi groupApi;
    @Inject
    NotificationsHelper notifications;

    @BindColor(R.color.colorGradientEnd)
    int mColorGradientEnd;

    private MessageDao mMessaggioDao;
    private EventDao mEventoDao;
    private View.OnLayoutChangeListener mLayoutListener;
    private Group mGruppo;
    private String mId;
    private boolean mCanAddGuests;
    private boolean mAnimationInProgress;

    private static final String PARAM_ID = "paramId";

    // SYSTEM

    public static Intent newIntent(String gruppoId) {
        Intent intent = new Intent(App.getContext(), Act_GruppoMain.class);
        intent.putExtra(PARAM_ID, gruppoId);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUseBus(true);

        if (getToolbarLayout() != null) {
            getToolbarSubtitle().setVisibility(View.GONE);
        }

        if (Utils.isLollipop()) {
            mStatusBar.getLayoutParams().height = Utils.getStatusBarHeight(this);
        }

        if (mToolbar != null) {
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mAnimationInProgress) {
                        return;
                    }

                    if (isTaskRoot()) {
                        startActivity(Act_Home.newIntent(0));
                        finish();
                    } else {
                        Act_GruppoMain.super.onBackPressed();
                    }
                }
            });
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_main_gruppo;
    }

    @Override
    protected void initVariables() {
        mAnimationInProgress = false;
    }

    @Override
    protected void loadParameters(Bundle extras) {
        mId = extras.getString(PARAM_ID);
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {
    }

    @Override
    protected void saveInfos(Bundle outState) {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        if (!mGruppo.isMyselfPresent()) {
            inflater.inflate(R.menu.menu_gruppo_main_abbandonato, menu);
        } else {
            inflater.inflate(R.menu.menu_gruppo_main, menu);
            if (!mGruppo.getAdminUuid().equals(AccountProvider.getInstance().getAccountId())) {
                MenuItem modifica = menu.findItem(R.id.menu_modifica);
                modifica.setVisible(false);
            }

            MenuItem invitare = menu.findItem(R.id.menu_partecipanti);
            invitare.setVisible(mCanAddGuests);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_partecipanti:
                if (mCanAddGuests) {
                    AnalitycsProvider.getInstance().logCustomType(mFirebaseAnalytics, "dotsGroupAction", "addParticipants");
                    startActivity(Act_MainAggiungiPartecipante.newIntent(mId, false));
                } else {
                    Toast.makeText(this, getString(R.string.main_gruppo_no_permission_add_users), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.menu_modifica:
                if (!mAnimationInProgress) {
                    AnalitycsProvider.getInstance().logCustomType(mFirebaseAnalytics, "dotsGroupAction", "modify");
                    startActivity(Act_GruppoModifica.newIntent(mId));
                }
                break;
            case R.id.menu_delete:
                if (!mAnimationInProgress) {
                    AnalitycsProvider.getInstance().logCustomType(mFirebaseAnalytics, "dotsGroupAction", "delete");
                    new MaterialDialog.Builder(this)
                            .title(getString(R.string.main_gruppo_elimina_title))
                            .content(getString(R.string.main_gruppo_elimina_content))
                            .positiveText(getString(R.string.general_elimina))
                            .negativeText(getString(R.string.general_annulla))
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    getDynamicView().showLoading();
                                    App.jobManager().addJobInBackground(new GroupDeleteJob(mId));
                                }
                            })
                            .show();
                }
                break;
            case R.id.menu_esci_da_gruppo:
                AnalitycsProvider.getInstance().logCustomType(mFirebaseAnalytics, "dotsGroupAction", "abandon");
                new MaterialDialog.Builder(this)
                        .title(getString(R.string.main_gruppo_esci_title))
                        .content(getString(R.string.main_gruppo_esci_content))
                        .positiveText(getString(R.string.general_abbandona))
                        .negativeText(getString(R.string.general_annulla))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                getDynamicView().showLoading();
                                setRequestId(UUID.randomUUID().toString());
                                groupApi.exit(getRequestId(), mId)
                                        .subscribeOn(Schedulers.newThread())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new Action1<Object>() {
                                            @Override
                                            public void call(Object obj) {
                                            }
                                        }, new Action1<Throwable>() {
                                            @Override
                                            public void call(Throwable throwable) {
                                                getDynamicView().hide();

                                                APIError error = APIErrorUtils.parseError(Act_GruppoMain.this, throwable);
                                                Toast.makeText(Act_GruppoMain.this, error.message(), Toast.LENGTH_SHORT).show();
                                            }
                                        });
                            }
                        }).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGruppo != null) {
            updateTab(0, false, mGruppo.getNumNewMessages(), 0);
            updateTab(1, false, 0, mEventoDao.getNumEventsFromGroup(mGruppo.getUuid()));
            updateTab(2, false, mGruppo.getNumUserUpdates(), mGruppo.getGuests().size());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mRevealLayout.setVisibility(View.INVISIBLE);
        App.updateGroupVisibility(mId, true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.updateGroupVisibility(mId, false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        App.resetGroupVisibility();
        if (mGruppo != null) {
            mGruppo.removeAllChangeListeners();
        }
        if (mLayoutListener != null) {
            ((View) mViewPager.getParent()).removeOnLayoutChangeListener(mLayoutListener);
        }
        mMessaggioDao.markAllAsReadAsync(false, mId);
    }

    @Override
    public void onBackPressed() {
        if (mAnimationInProgress) {
            return;
        }

        if (!getDynamicView().isContentShown()) {
            getDynamicView().hide();
            cancelTimer();
        } else if (mViewPager.getCurrentItem() != 0) {
            mViewPager.setCurrentItem(0, true);
        } else if (isTaskRoot()) {
            startActivity(Act_Home.newIntent(0));
            finish();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        recreate();
    }

    @Override
    protected void initialize() {
        GroupDao groupDao = new GroupDao(realm);
        // Init gruppo
        mGruppo = groupDao.getGroup(mId);
        if (mGruppo == null) {
            Toast.makeText(this, getString(R.string.general_error_gruppo_non_presente), Toast.LENGTH_LONG).show();
            finish();
            return;
        } else {
            mEventoDao = new EventDao(realm);
            mMessaggioDao = new MessageDao(realm);
            mCanAddGuests = mGruppo.isUsersCanInvite() || AccountProvider.getInstance().getAccountId().equals(mGruppo.getAdminUuid());

            updateToolbarTitleAndAvatar(mGruppo);
            changeToolbarIconVisibility(View.VISIBLE);

            mGruppo.addChangeListener(new RealmChangeListener<Group>() {
                @Override
                public void onChange(@NonNull Group element) {
                    if (element.isValid()) {
                        invalidateOptionsMenu();
                        mCanAddGuests = element.isUsersCanInvite() || AccountProvider.getInstance().getAccountId().equals(element.getAdminUuid());
                        updateToolbarTitleAndAvatar(element);
                        updateTabLayout(element);
                    }
                }
            });
        }

        // Init Tablayout e Viewpager
        mViewPager.setSwipe(true);
        mViewPager.setAdapter(new GruppoMainPagerAdapter(this, getSupportFragmentManager(), mId));
        mViewPager.setOffscreenPageLimit(2);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                mFabLayout.updateAnimationValue(position, positionOffset);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mTabLayout.setWithRoundBorder(true);
        mTabLayout.setupWithViewPager(mViewPager);
        mTabLayout.addOnTabSelectedListener(new MyTabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(MyTabLayout.Tab tab) {
                if (tab.getCustomView() != null) {
                    tab.getCustomView().findViewById(R.id.container).setAlpha(1f);
                }
                Utils.hideKeyboard(Act_GruppoMain.this);
                App.updateGroupPage(tab.getPosition());
            }

            @Override
            public void onTabUnselected(MyTabLayout.Tab tab) {
                if (tab.getCustomView() != null) {
                    tab.getCustomView().findViewById(R.id.container).setAlpha(0.6f);
                }
                if (tab.getPosition() == 0) {
                    mMessaggioDao.markChatAsReadAsync(false, mId);
                } else if (tab.getPosition() == 2) {
                    mMessaggioDao.markUsersAsReadAsync(false, mId);
                }
            }

            @Override
            public void onTabReselected(MyTabLayout.Tab tab) {

            }
        });

        ((View) mViewPager.getParent()).addOnLayoutChangeListener(mLayoutListener = new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (oldBottom == 0)
                    return;

                // Se la tastiera viene chiusa riabilito lo swipe del viewpager
                // Se la tastiera viene aperta disabilito lo swipe del viewpager
                if (bottom > oldBottom) {
                    mViewPager.setSwipe(true);
                } else if (bottom < oldBottom) {
                    mViewPager.setSwipe(false);
                }
            }
        });

        updateTab(0, true, mGruppo.getNumNewMessages(), 0);
        updateTab(1, true, 0, mEventoDao.getNumEventsFromGroup(mGruppo.getUuid()));
        updateTab(2, true, mGruppo.getNumUserUpdates(), mGruppo.getGuests().size());

        initFabLayout();
    }

    @Subscribe
    public void onEvent(UserSelectedEvent event) {
        if (!mAnimationInProgress) {
            AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "viewProfile", "group");
            startActivity(Act_Profilo.newIntent(event.getId()));
        }
    }

    @Subscribe
    public void onEvent(ImageSelectedEvent event) {
        if (mAnimationInProgress) {
            return;
        }

        if (event.getPath() != null) {
            if (Utils.checkIfImageExists(event)) { //TODO non funziona
                Intent intent = Act_ImageDetail.newIntent(event.getPath(), event.getCaption());
                if (Utils.isLollipop()) {
                    Pair<View, String> p1 = Pair.create(event.getView(), getString(R.string.transition_image));
                    //noinspection unchecked
                    ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(this, p1);
                    startActivity(intent, optionsCompat.toBundle());
                } else {
                    startActivity(intent);
                }
            } else {
                Toast.makeText(this, R.string.general_error_image_not_found, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, R.string.general_error_image_not_found, Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(final StartImageDownloadEvent event) {
        if (SettingUtils.checkIfCanDownload(this) || event.isIgnoraControllo()) {
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(@NonNull Realm realm) {
                    final Message messaggio = new MessageDao(realm).getMessage(event.getIdMessaggio());
                    if (messaggio == null) {
                        return;
                    }

                    messaggio.setImageState(Constants.IMAGE_STATE_DOWNLOADING);
                    App.jobManager().addJobInBackground(new ImageDownloadJob(messaggio.getUuid(), messaggio.getMediaUrl()));
                }
            });
        } else {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(@NonNull Realm realm) {
                    final Message messaggio = new MessageDao(realm).getMessage(event.getIdMessaggio());
                    if (messaggio == null) {
                        return;
                    }

                    messaggio.setImageState(Constants.IMAGE_STATE_DOWNLOAD_FAILED);
                }
            });
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ApiCallResultEvent event) {
        if (event.getRequestId().equals(getRequestId())) {
            cancelTimer();
            getDynamicView().hide();
        }
    }

    @Subscribe
    public void onEvent(UserImageSelectedEvent event) {
        if (!mAnimationInProgress) {
            int[] location = new int[2];
            event.getView().getLocationInWindow(location);
            startActivity(Act_ContactPhotoPopup.newIntent(event.getId(), location[0], location[1], event.getView().getWidth() / 2));
            overridePendingTransition(0, 0);
        }
    }

    @Subscribe
    public void onEvent(EventSelectedEvent event) {
        if (!mAnimationInProgress) {
            notifications.cancelAllNotification(this);
            startActivity(Act_EventoMain.newIntent(event.getId()));
        }
    }

    @Subscribe(sticky = true)
    public void onEvent(ShowPopupInvitedGroup event) {
        if (Prefs.getString(Constants.SHOW_INVITE_POPUP_GRUPPO + mId) != null) {
            EventBus.getDefault().removeStickyEvent(event);
            showInvitePopup();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(GroupDeleteEvent event) {
        getDynamicView().hide();
        if (event.isSuccess()) {
            finish();
        } else {
            Toast.makeText(this, getString(R.string.general_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    // FUNCTIONS

    @SuppressWarnings("ConstantConditions")
    @SuppressLint("InflateParams")
    private void updateTab(int position, boolean isCreate, long numNotification, int numItems) {
        View view;
        if (isCreate) {
            view = LayoutInflater.from(this).inflate(R.layout.system_tab, null);
            view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

            TextView text = view.findViewById(R.id.tab_text);
            ImageView image = view.findViewById(R.id.tab_image);
            ViewGroup container = view.findViewById(R.id.container);
            switch (position) {
                case 0:
                    text.setText(getString(R.string.main_gruppo_chat));
                    image.setImageResource(R.drawable.tab_ico_chat);
                    container.setAlpha(1f);
                    break;
                case 1:
                    text.setText(String.format(Locale.getDefault(), "%d %s", numItems, getResources().getQuantityString(R.plurals.main_gruppo_eventi, numItems)));
                    image.setImageResource(R.drawable.tab_ico_proposte);
                    container.setAlpha(0.6f);
                    break;
                default:
                    text.setText(String.format(Locale.getDefault(), "%d %s", numItems, getResources().getQuantityString(R.plurals.main_gruppo_invitati, numItems)));
                    image.setImageResource(R.drawable.ico_invitati);
                    container.setAlpha(0.6f);
                    break;
            }
        } else {
            view = mTabLayout.getTabAt(position).getCustomView();
            TextView text = view.findViewById(R.id.tab_text);
            switch (position) {
                case 1:
                    text.setText(String.format(Locale.getDefault(), "%d %s", numItems, getResources().getQuantityString(R.plurals.main_gruppo_eventi, numItems)));
                    break;
                default:
                    text.setText(String.format(Locale.getDefault(), "%d %s", numItems, getResources().getQuantityString(R.plurals.main_gruppo_invitati, numItems)));
                    break;
            }
        }

        AnimatedTextView badge = view.findViewById(R.id.tab_badge);
        if (isCreate) {
//            badge.setIgnoreMeasureExactly(false);
            badge.setTextColor(ContextCompat.getColor(this, R.color.textLightBlue));
            badge.setTextSize(11);
        }
        badge.setVisibility(numNotification == 0 ? View.GONE : View.VISIBLE);
        badge.setValue(numNotification);

        if (isCreate) {
            mTabLayout.getTabAt(position).setCustomView(view);
        }
    }

    /**
     * Gestisco lo scroll del viewpager a seconda della tastiera
     *
     * @param oldBottom oldBottom
     * @param bottom    bottom
     */
    public void onLayoutChange(int oldBottom, int bottom) {
        if (bottom > oldBottom) {
            onLayoutChange(true);
        } else if (bottom < oldBottom) {
            onLayoutChange(false);
        }
    }

    /**
     * Gestisco lo scroll del viewpager
     *
     * @param enable abilita scroll
     */
    public void onLayoutChange(boolean enable) {
        if (enable) {
            mViewPager.setSwipe(true);
        } else {
            mViewPager.setSwipe(false);
        }
    }

    /**
     * Aggiorna titolo e l'avatar della toolbar
     *
     * @param gruppo gruppo
     */
    private void updateToolbarTitleAndAvatar(Group gruppo) {
        if (mToolbar != null && gruppo != null && gruppo.getTitle() != null) {
            setToolbarTitle(gruppo.getTitle());
        }
        Glide.with(this).load(mGruppo.getAvatarThumbUrl()).asBitmap().placeholder(R.drawable.avatar_gruppo).centerCrop().into(new BitmapImageViewTarget(getToolbarIcon()) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                getToolbarIcon().setImageDrawable(circularBitmapDrawable);
            }
        });
    }

    /**
     * Aggiorno badge tablayout
     *
     * @param gruppo gruppo
     */
    private void updateTabLayout(Group gruppo) {
        updateTab(0, false, gruppo.getNumNewMessages(), 0);
        updateTab(1, false, 0, mEventoDao.getNumEventsFromGroup(gruppo.getUuid()));
        updateTab(2, false, gruppo.getNumUserUpdates(), gruppo.getGuests().size());

        initFabLayout();
    }

    /**
     * Imposto il FabLayout
     */
    private void initFabLayout() {
        SparseArray<String> texts = new SparseArray<>();
        texts.put(1, getString(R.string.system_fab_activity));
        texts.put(2, getString(R.string.system_fab_participant));
        mFabLayout.setTextResources(texts);
        if (mGruppo.isMyselfPresent()) {
            mFabLayout.setPositionTypes(Arrays.asList(BorderFabLayout.POSITION_TYPE.HIDE, BorderFabLayout.POSITION_TYPE.DEFAULT,
                    mCanAddGuests ? BorderFabLayout.POSITION_TYPE.DEFAULT : BorderFabLayout.POSITION_TYPE.DISABLE));
        } else {
            mFabLayout.setPositionTypes(Arrays.asList(BorderFabLayout.POSITION_TYPE.HIDE, BorderFabLayout.POSITION_TYPE.DISABLE,
                    BorderFabLayout.POSITION_TYPE.DISABLE));
        }

        mFabLayout.updateAnimationValue(mViewPager.getCurrentItem(), 0);
    }

    /**
     * Esegue l'animazione di reveal al click del crea proposta/sondaggio
     *
     * @param view view
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void animateReveal(final View view, final Intent intent) {
        if (mAnimationInProgress) {
            return;
        }

        if (!Utils.isLollipop()) {
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_bottom_slow, R.anim.fade_out);
            return;
        }

        mAnimationInProgress = true;

        // Calcolo posizione da cui far partire l'animazione di reveal
        float cx = (int) (view.getX() + view.getWidth() / 2);
        float cy = (int) (view.getY() + view.getHeight() / 2);
        // Radius finale
        float finalRadius = (float) Math.hypot(cx, cy);
        float initialRadius = view.getWidth() / 2;
        if (view instanceof BorderFabLayout) {
            initialRadius = ((BorderFabLayout) view).getFabButton().getWidth() / 2;
            cy += ((BorderFabLayout) view).getFabButton().getY();
        }

        //Creo l'animazione
        AnimatorSet animatorSet = new AnimatorSet();
        Animator animator = ViewAnimationUtils.createCircularReveal(mRevealLayout, (int) cx, (int) cy, initialRadius, finalRadius);
        ValueAnimator valueAnimator = ObjectAnimator.ofInt(mRevealLayout, "backgroundColor", Color.WHITE, Color.WHITE, mColorGradientEnd);
        valueAnimator.setEvaluator(new ArgbEvaluator());
        animatorSet.playTogether(animator, valueAnimator);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mRevealLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mAnimationInProgress = false;
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });
        animatorSet.start();
    }

    /**
     * Mostro il popup per l'invito degli utenti non ancora in yeap
     */
    private void showInvitePopup() {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                new MaterialDialog.Builder(Act_GruppoMain.this)
                        .title(getString(R.string.system_invite_partecipanti_in_yeap_title))
                        .content(getString(R.string.system_invite_partecipanti_in_yeap_content))
                        .positiveText(getString(R.string.system_invite_partecipanti_in_yeap_conferma))
                        .negativeText(getString(R.string.system_invite_partecipanti_in_yeap_annulla))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                Intent smsIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + Prefs.getString(Constants.SHOW_INVITE_POPUP_GRUPPO + mId)));
                                smsIntent.putExtra("sms_body", String.format(getString(R.string.system_invita_testo_gruppo), mGruppo.getTitle()));
                                startActivity(smsIntent);
                                Prefs.remove(Constants.SHOW_INVITE_POPUP_GRUPPO + mId);
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                Prefs.remove(Constants.SHOW_INVITE_POPUP_GRUPPO + mId);
                            }
                        })
                        .cancelable(false)
                        .show();
            }
        });
    }
}
