package it.mozart.yeap.ui.support.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderAdapter;
import io.realm.RealmResults;
import it.mozart.yeap.R;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.ui.support.Act_Contacts;
import it.mozart.yeap.ui.support.events.ContactSelectedEvent;
import it.mozart.yeap.ui.support.events.MandaInvitoEvent;
import it.mozart.yeap.ui.support.events.UserSelectedEvent;
import it.mozart.yeap.ui.views.AutoRefreshImageView;
import it.mozart.yeap.ui.views.RecyclerViewIndex;
import it.mozart.yeap.utility.Constants;

public class ContactsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
        implements StickyHeaderAdapter<ContactsAdapter.HeaderViewHolder>, RecyclerViewIndex.AdapterItemIndex {

    private RealmResults<User> mList;
    private List<Act_Contacts.HeaderItem> mHeaderList;

    public ContactsAdapter(RealmResults<User> list, List<Act_Contacts.HeaderItem> headers) {
        this.mList = list;
        this.mHeaderList = headers;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == Constants.ADAPTER_TYPE_SYSTEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contacts_invita, parent, false);
            return new InvitaViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contacts, parent, false);
            return new ContactsViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) != Constants.ADAPTER_TYPE_SYSTEM) {
            final ContactsViewHolder realHolder = (ContactsViewHolder) holder;
            User utente = mList.get(position);
            if (utente == null) {
                return;
            }

            realHolder.name.setText(utente.getChatName());
            realHolder.phone.setText(utente.getPhone());
            realHolder.avatar.bindThumb(utente.getUuid());
            realHolder.yeap.setVisibility(!utente.isInYeap() ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position < getItemCount() - 1)
            return super.getItemViewType(position);
        return Constants.ADAPTER_TYPE_SYSTEM;
    }

    @Override
    public long getHeaderId(int position) {
        return getId(position);
    }

    @Override
    public HeaderViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contacts_header, parent, false);
        return new HeaderViewHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(HeaderViewHolder viewholder, int position) {
        try {
            char c = mList.get(position).getChatName(true).charAt(0);
            if ((c < 65 || c > 90) && (c < 97 || c > 122)) {
                c = '#';
            }
            viewholder.header.setText(String.valueOf(c).toUpperCase());
        } catch (Exception ignored) {
        }
    }

    private int getId(int position) {
        for (int i = mHeaderList.size() - 1; i >= 0; i--) {
            if (position >= mHeaderList.get(i).getPosition())
                return mHeaderList.get(i).getId();
        }
        return mHeaderList.get(0).getId();
    }

    @Override
    public int getWordPosition(String word) {
        for (int i = 0; i < mList.size(); i++) {
            char c = mList.get(i).getChatName(true).charAt(0);
            if ((c < 65 || c > 90) && (c < 97 || c > 122)) {
                c = '#';
            }
            if (word.equalsIgnoreCase(String.valueOf(c)))
                return i;
        }
        return -1;
    }

    public void updateAdapter(RealmResults<User> list, List<Act_Contacts.HeaderItem> headers) {
        this.mList = list;
        this.mHeaderList = headers;
        notifyDataSetChanged();
    }

    public RealmResults<User> getList() {
        return mList;
    }

    class ContactsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.avatar)
        AutoRefreshImageView avatar;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.phone)
        TextView phone;
        @BindView(R.id.yeap)
        TextView yeap;

        ContactsViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getLayoutPosition();
            User utente = mList.get(position);
            if (utente != null) {
                if (utente.isInYeap()) {
                    EventBus.getDefault().post(new UserSelectedEvent(utente.getUuid(), v));
                } else {
                    EventBus.getDefault().post(new ContactSelectedEvent(utente.getContactId(), utente.getPhone(), v));
                }
            }
        }
    }

    private class InvitaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        InvitaViewHolder(View v) {
            super(v);

            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            EventBus.getDefault().post(new MandaInvitoEvent());
        }
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.header)
        TextView header;

        HeaderViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
