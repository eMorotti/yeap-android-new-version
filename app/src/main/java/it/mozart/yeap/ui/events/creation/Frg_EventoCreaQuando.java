package it.mozart.yeap.ui.events.creation;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.parceler.Parcels;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import it.mozart.yeap.R;
import it.mozart.yeap.common.QuandoInfo;
import it.mozart.yeap.helpers.AnalitycsProvider;
import it.mozart.yeap.helpers.CreationModelHelper;
import it.mozart.yeap.realm.PollItem;
import it.mozart.yeap.ui.base.BaseFragment;
import it.mozart.yeap.ui.main.MainCreationControl;
import it.mozart.yeap.ui.support.Act_Quando;
import it.mozart.yeap.ui.views.AnimCheckBox;
import it.mozart.yeap.ui.views.InterceptLinearLayout;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.DateUtils;
import it.mozart.yeap.utility.Utils;

public class Frg_EventoCreaQuando extends BaseFragment implements MainCreationControl {

    @BindView(R.id.quando_layout)
    InterceptLinearLayout mQuandoLayout;
    @BindView(R.id.quando)
    TextView mQuando;
    @BindView(R.id.delete)
    ImageButton mQuandoDelete;
    @BindView(R.id.quando_switch)
    ImageButton mQuandoSwitch;
    @BindView(R.id.quando_sondaggio_layout)
    InterceptLinearLayout mQuandoSondaggioLayout;
    @BindView(R.id.poll_layout)
    ViewGroup mPollLayout;
    @BindView(R.id.perm_items_check)
    AnimCheckBox mPermItems;

    @OnClick(R.id.quando)
    void quandoClicked() {
        mSelectedView = mQuando;
        quandoSelected(creationModelHelper.getEvent().getEventInfo().isQuandoPresent());
    }

    @OnClick(R.id.delete)
    void quandoDeleteClicked() {
        mQuando.setText(null);
        mQuandoDelete.setVisibility(View.GONE);
        creationModelHelper.getEvent().getEventInfo().updateQuandoInfo(null);
    }

    @OnClick(R.id.quando_switch)
    void quandoSwitchClicked() {
        switchClicked(!mIsSondaggio);
    }

    @OnClick(R.id.quando_layout)
    void quandoLayoutClicked() {
        switchClicked(false);
    }

    @OnClick(R.id.quando_sondaggio_layout)
    void quandoSondaggioLayoutClicked() {
        switchClicked(true);
    }

    @OnClick({R.id.perm_items, R.id.perm_items_check})
    void permInvClicked() {
        mPermItems.setChecked(!mPermItems.isChecked());
        creationModelHelper.getWhenPoll().setUsersCanAddItems(mPermItems.isChecked());
    }

    @OnClick(R.id.layout)
    void layoutClicked() {
        Utils.hideKeyboard(getActivity());
    }

    // SUPPORT

    @Inject
    CreationModelHelper creationModelHelper;

    private TextView mSelectedView;
    private boolean mIsSondaggio;

    private static final int QUANDO_CODE = 3001;

    // SYSTEM

    @Override
    protected int getLayoutId() {
        return R.layout.frg_evento_crea_quando;
    }

    @Override
    protected void initValues() {

    }

    @Override
    protected void loadParameters(Bundle arguments) {

    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    protected void initialize() {
        mIsSondaggio = creationModelHelper.isWhenPollSelected();

        QuandoInfo quandoInfo = creationModelHelper.getEvent().getEventInfo().generateQuandoModel();
        mQuando.setText(quandoInfo != null ? DateUtils.createQuandoText(quandoInfo) : null);
        if (mQuando.getText().length() != 0) {
            mQuandoDelete.setVisibility(View.VISIBLE);
        } else {
            mQuandoDelete.setVisibility(View.GONE);
        }

        mPermItems.setChecked(creationModelHelper.getWhenPoll().isUsersCanAddItems(), false);

        switchClicked(mIsSondaggio);
        initPoll();
    }

    @Override
    public void onStart() {
        super.onStart();
        // Aggiorno il titolo, il sottotitolo e l'icona della toolbar dell'activity
        if (getActivity() instanceof Act_EventoCrea) {
            ((Act_EventoCrea) getActivity()).setToolbarTitleAndIconFromCrea(creationModelHelper.getEvent().getTitle(), R.drawable.ic_arrow_back_white_24dp);
            ((Act_EventoCrea) getActivity()).setToolbarSubtitleFromCrea(getString(R.string.nuova_evento_crea_info_quando_subtitle));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case QUANDO_CODE:
                    quandoManage((QuandoInfo) Parcels.unwrap(data.getParcelableExtra(Constants.INTENT_QUANDO)));
                    break;
            }
        }
    }

    @Override
    public boolean canGoOn() {
        if (mIsSondaggio) {
            return true;
        } else {
            if (creationModelHelper.getEvent().getEventInfo().isQuandoPresent()) {
                return true;
            }
        }

        if (!mIsSondaggio) {
            Toast.makeText(getContext(), getString(R.string.evento_crea_quando_mancante), Toast.LENGTH_SHORT).show();
        }

        return false;
    }

    // FUNCTIONS

    /**
     * Crea il titolo per l'activity Quando
     *
     * @return titolo
     */
    private String createQuandoTitle() {
        return getString(R.string.nuova_evento_quando_title);
    }

    /**
     * Crea il sottotitolo per l'activity Quando
     *
     * @return sottotitolo
     */
    private String createQuandoSubtitle() {
        return getString(R.string.nuova_evento_quando_subtitle);
    }

    private void quandoSelected(boolean alreadyPresent) {
        // Apro subito l'activity se il model è null
        if (!alreadyPresent) {
            AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "setActivityWhen", "creation");
            startActivityForResult(Act_Quando.newIntent(createQuandoTitle(), createQuandoSubtitle(), null), QUANDO_CODE);
        }
    }

    private void quandoManage(QuandoInfo quandoInfo) {
        AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "setActivityWhenConfirm", "creation");
        // Aggiorno layout
        if (quandoInfo != null && quandoInfo.isSomeInfoPresent()) {
            mSelectedView.setText(DateUtils.createQuandoText(quandoInfo));
        } else {
            mSelectedView.setText(null);
            quandoInfo = null;
        }

        if (mIsSondaggio) {
            int position = Integer.parseInt(mSelectedView.getTag().toString());
            List<PollItem> list = creationModelHelper.getWhenPoll().getPollItems();
            if (list.size() >= position) {
                list.get(position - 1).updateQuandoInfo(quandoInfo);
            } else {
                PollItem pollItem = new PollItem();
                pollItem.updateQuandoInfo(quandoInfo);
                creationModelHelper.getWhenPoll().getPollItems().add(pollItem);
                updateVoceView(mPollLayout.getChildAt(position - 1), pollItem, position);

                View view = LayoutInflater.from(getContext()).inflate(R.layout.item_evento_crea_voce_sondaggio, mPollLayout, false);
                updateVoceView(view, null, mPollLayout.getChildCount() + 1);
                mPollLayout.addView(view);
            }
        } else {
            creationModelHelper.getEvent().getEventInfo().updateQuandoInfo(quandoInfo);
            mQuandoDelete.setVisibility(View.VISIBLE);
        }
    }

    private void switchClicked(boolean isSondaggio) {
        if (isSondaggio) {
            mQuandoLayout.setInterceptAll(true);
            mQuandoLayout.setAlpha(0.3f);
            mQuandoSondaggioLayout.setInterceptAll(false);
            mQuandoSondaggioLayout.setAlpha(1f);

            mQuandoSwitch.setBackgroundResource(R.drawable.circle_white);
            mQuandoSwitch.setColorFilter(Color.BLACK);
        } else {
            mQuandoLayout.setInterceptAll(false);
            mQuandoLayout.setAlpha(1f);
            mQuandoSondaggioLayout.setInterceptAll(true);
            mQuandoSondaggioLayout.setAlpha(0.3f);

            mQuandoSwitch.setBackgroundResource(R.drawable.circle_transparent_white);
            mQuandoSwitch.setColorFilter(Color.WHITE);
        }
        mIsSondaggio = isSondaggio;
        creationModelHelper.setWhenPollSelected(mIsSondaggio);
    }

    private void initPoll() {
        mPollLayout.removeAllViews();
        int i = 1;
        for (PollItem item : creationModelHelper.getWhenPoll().getPollItems()) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.item_evento_crea_voce_sondaggio, mPollLayout, false);
            updateVoceView(view, item, i);
            mPollLayout.addView(view);
            i++;
        }

        View view = LayoutInflater.from(getContext()).inflate(R.layout.item_evento_crea_voce_sondaggio, mPollLayout, false);
        updateVoceView(view, null, i);
        mPollLayout.addView(view);
    }

    private void updateVoceView(@NonNull View view, final PollItem voce, final int position) {
        final TextView quando = view.findViewById(R.id.item);
        ImageButton delete = view.findViewById(R.id.delete);

        quando.setHint(getString(R.string.nuova_evento_crea_dettaglio_quando_hint));
        if (voce != null) {
            quando.setText(DateUtils.createQuandoText(voce.generateQuandoModel()));
            delete.setVisibility(View.VISIBLE);
        } else {
            delete.setVisibility(View.GONE);
        }

        quando.setTag(String.valueOf(position));
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPollLayout.getChildCount() == 1) {
                    updateVoceView(mPollLayout.getChildAt(0), null, 1);
                } else {
                    mPollLayout.removeViewAt(position - 1);
                    creationModelHelper.getWhenPoll().getPollItems().remove(position - 1);
                    for (int i = position - 1; i < mPollLayout.getChildCount(); i++) {
                        if (creationModelHelper.getWhenPoll().getPollItems().size() > i) {
                            updateVoceView(mPollLayout.getChildAt(i), creationModelHelper.getWhenPoll().getPollItems().get(i), i + 1);
                        } else {
                            updateVoceView(mPollLayout.getChildAt(i), null, i + 1);
                        }
                    }
                }
            }
        });

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectedView = quando;
                quandoSelected(voce != null);
            }
        });
    }
}
