package it.mozart.yeap.ui.groups.main.events;

import android.view.View;

public class GroupGuestSelectedEvent {

    private String id;
    private View view;

    public GroupGuestSelectedEvent(String id, View view) {
        this.id = id;
        this.view = view;
    }

    public String getId() {
        return id;
    }

    public View getView() {
        return view;
    }
}
