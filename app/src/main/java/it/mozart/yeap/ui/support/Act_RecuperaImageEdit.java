package it.mozart.yeap.ui.support;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.io.File;
import java.io.FileNotFoundException;

import butterknife.BindView;
import butterknife.OnClick;
import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.ui.base.BaseActivityWithSyncService;
import it.mozart.yeap.ui.views.TouchImageView;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.PhotoGenerator;
import it.mozart.yeap.utility.Utils;

public class Act_RecuperaImageEdit extends BaseActivityWithSyncService {

    @BindView(R.id.input)
    EditText mInput;
    @BindView(R.id.image)
    TouchImageView mImage;

    @OnClick(R.id.fab)
    void fabClicked() {
        // Ritorno le informazioni all'activity che ha richiamato la selezione di un'immagine
        Intent intent = new Intent();
        if (mIsEdited) {
            if (mImagePath != null)
                intent.putExtra(Constants.IMAGE_PATH, mImagePath);
        } else {
            if (mImageFile == null) {
                if (mIsGif) {
                    if (mGifBytes != null) {
                        mImageFile = mPhotoGenerator.generateFileFrom(mGifBytes);
                    }
                } else {
                    mImageFile = mPhotoGenerator.generateFileFrom(mBitmap);
                }
            }
            if (mImageFile != null) {
                intent.putExtra(Constants.IMAGE_PATH, mImageFile.getAbsolutePath());
            }
        }
        if (mInput.getText() != null && mInput.getText().toString().length() != 0)
            intent.putExtra(Constants.CAPTION, mInput.getText().toString());
        setResult(RESULT_OK, intent);
        finish();
    }

    // SUPPORT

    private PhotoGenerator mPhotoGenerator;
    private File mImageFile;
    private Bitmap mBitmap;
    private String mImagePath;
    private Uri mImageUri;
    private byte[] mGifBytes;
    private boolean mIsGif;
    private boolean mIsEdited;

    private static final String PARAM_IMAGE = "paramImage";
    private static final String PARAM_URI = "paramUri";
    private static final String PARAM_GIF = "paramGif";

    private static final int CODE_EDIT = 1022;

    // SYSTEM

    public static Intent newIntent(String image) {
        Intent intent = new Intent(App.getContext(), Act_RecuperaImageEdit.class);
        intent.putExtra(PARAM_IMAGE, image);
        return intent;
    }

    public static Intent newIntent(Uri image) {
        Intent intent = new Intent(App.getContext(), Act_RecuperaImageEdit.class);
        intent.putExtra(PARAM_URI, image.toString());
        return intent;
    }

    public static Intent newIntentGif(Uri image) {
        Intent intent = new Intent(App.getContext(), Act_RecuperaImageEdit.class);
        intent.putExtra(PARAM_URI, image.toString());
        intent.putExtra(PARAM_GIF, true);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);

            if (Utils.isLollipop()) {
                ((ViewGroup.MarginLayoutParams) mToolbar.getLayoutParams()).topMargin = Utils.getStatusBarHeight(this);
            }
        }

        invalidateOptionsMenu();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_recupera_image_edit;
    }

    @Override
    protected void initVariables() {
        mIsEdited = false;
    }

    @Override
    protected void loadParameters(Bundle extras) {
        mImagePath = extras.getString(PARAM_IMAGE);
        if (extras.containsKey(PARAM_URI))
            mImageUri = Uri.parse(extras.getString(PARAM_URI));
        mIsGif = extras.getBoolean(PARAM_GIF, false);
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (mIsGif) {
            menu.clear();
        } else {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_recupera_image_edit, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_edit:
                if (mImageFile == null) {
                    mImageFile = mPhotoGenerator.generateFileFrom(mBitmap);
                }
                if (mImageFile != null) {
                    startActivityForResult(Act_ImageCrop.newIntentFreeRatio(Uri.fromFile(mImageFile)), CODE_EDIT);
                } else {
                    Toast.makeText(this, getString(R.string.general_error_message), Toast.LENGTH_SHORT).show();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void initialize() {
        mPhotoGenerator = new PhotoGenerator(this);
        if (mImageFile != null) {
            Glide.with(this).load(mImageFile).into(mImage);
        } else if (mIsGif) {
            Glide.with(this).load(mImageUri).asGif().listener(new RequestListener<Uri, GifDrawable>() {
                @Override
                public boolean onException(Exception e, Uri model, Target<GifDrawable> target, boolean isFirstResource) {
                    return false;
                }

                @Override
                public boolean onResourceReady(GifDrawable resource, Uri model, Target<GifDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                    mGifBytes = resource.getData();
                    return false;
                }
            }).into(mImage);
        } else {
            try {
                if (mImagePath != null) {
                    mBitmap = mPhotoGenerator.generatePhotoWithValue(mImagePath);
                } else {
                    mBitmap = mPhotoGenerator.generatePhotoWithValue(mImageUri);
                }
                mImage.setImageBitmap(mBitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == CODE_EDIT) {
            // Aggiorno l'immagine con quella modificata
            mImagePath = data.getStringExtra(Constants.IMAGE_CROP);
            if (mImagePath != null) {
                mIsEdited = true;
                Glide.with(this).load(mImagePath).into(mImage);
            }
        }
    }
}
