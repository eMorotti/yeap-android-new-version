package it.mozart.yeap.ui.main;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.common.MessaggioInfo;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.Group;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.GroupDao;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.services.messaggi.MessaggiService;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.ui.base.BaseActivityWithSyncService;
import it.mozart.yeap.ui.events.main.Act_EventoMain;
import it.mozart.yeap.ui.groups.main.Act_GruppoMain;
import it.mozart.yeap.ui.main.adapters.MainMessaggiAdapter;
import it.mozart.yeap.utility.Constants;

public class Act_MainMessageReply extends BaseActivityWithSyncService {

    @BindView(R.id.avatar)
    ImageView mAvatar;
    @BindView(R.id.title)
    TextView mTitle;
    @BindView(R.id.recyclerview)
    RecyclerView mRecyclerView;
    @BindView(R.id.input)
    EditText mInput;

    @OnClick(R.id.send)
    void sendClicked() {
        if (mIgnore) {
            return;
        }

        if (mInput.getText().length() == 0) {
            return;
        } else if (mInput.getText().toString().trim().length() == 0) {
            return;
        }

        mIgnore = true;

        mMessageDao.markAllAsReadAsync(mIsAttivita, mId);
        // Creo messaggio
        MessaggioInfo messaggioInfo = new MessaggioInfo(mId,
                mIsAttivita ? Constants.MESSAGGIO_TYPOLOGY_CHAT_EVENTO : Constants.MESSAGGIO_TYPOLOGY_CHAT_GRUPPO,
                AccountProvider.getInstance().getAccountId(),
                mInput.getText().toString(),
                null,
                new Date().getTime(),
                true,
                false,
                true);
        MessaggiService.createMessage(this, messaggioInfo);

        finish();
    }

    @OnClick(R.id.close)
    void closeClicked() {
        finish();
    }

    @OnClick(R.id.show)
    void showClicked() {
        if (mIsAttivita) {
            Intent intent = Act_EventoMain.newIntent(mId);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            Intent intent = Act_GruppoMain.newIntent(mId);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        finish();
    }

    // SUPPORT

    @Inject
    Realm realm;
    @Inject
    NotificationsHelper notifications;

    private MessageDao mMessageDao;
    private List<String> mMessaggiList;
    private String mId;
    private boolean mIsAttivita;
    private boolean mIgnore;

    private static final String PARAM_ID = "paramId";
    private static final String PARAM_IS_ATTIVITA = "paramIsAttivita";
    private static final String PARAM_LIST = "paramList";

    // SYSTEM

    public static Intent newIntent(String id, boolean isAttivita, List<String> messaggiList) {
        Intent intent = new Intent(App.getContext(), Act_MainMessageReply.class);
        intent.putExtra(PARAM_ID, id);
        intent.putExtra(PARAM_IS_ATTIVITA, isAttivita);
        intent.putStringArrayListExtra(PARAM_LIST, (ArrayList<String>) messaggiList);
        return intent;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_main_message_reply;
    }

    @Override
    protected void initVariables() {
        mIgnore = false;
    }

    @Override
    protected void loadParameters(Bundle extras) {
        mId = extras.getString(PARAM_ID);
        mIsAttivita = extras.getBoolean(PARAM_IS_ATTIVITA);
        mMessaggiList = extras.getStringArrayList(PARAM_LIST);
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    protected void initialize() {
        mMessageDao = new MessageDao(realm);
        if (mIsAttivita) {
            Event evento = new EventDao(realm).getEvent(mId);
            if (evento == null) {
                Toast.makeText(this, getString(R.string.general_error_evento_non_presente), Toast.LENGTH_LONG).show();
                finish();
                return;
            }

            mTitle.setText(evento.getTitle());

            Glide.with(this).load(evento.getAvatarThumbUrl()).asBitmap().placeholder(R.drawable.avatar_attivita).centerCrop().into(new BitmapImageViewTarget(mAvatar) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    mAvatar.setImageDrawable(circularBitmapDrawable);
                }
            });
        } else {
            Group gruppo = new GroupDao(realm).getGroup(mId);
            if (gruppo == null) {
                Toast.makeText(this, getString(R.string.general_error_gruppo_non_presente), Toast.LENGTH_LONG).show();
                finish();
                return;
            }

            mTitle.setText(gruppo.getTitle());

            Glide.with(this).load(gruppo.getAvatarThumbUrl()).asBitmap().placeholder(R.drawable.avatar_gruppo).centerCrop().into(new BitmapImageViewTarget(mAvatar) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    mAvatar.setImageDrawable(circularBitmapDrawable);
                }
            });
        }

        notifications.cancelAllNotification(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(new MainMessaggiAdapter(mMessaggiList));

        mInput.requestFocus();
    }
}
