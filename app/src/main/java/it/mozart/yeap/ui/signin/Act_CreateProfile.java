package it.mozart.yeap.ui.signin;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import org.parceler.Parcels;

import java.io.File;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.realm.RealmConfiguration;
import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.api.models.APIError;
import it.mozart.yeap.api.models.ImageApiModel;
import it.mozart.yeap.api.services.ProfileApi;
import it.mozart.yeap.api.services.UploadApi;
import it.mozart.yeap.helpers.AndroidPermissionHelper;
import it.mozart.yeap.helpers.ImageInputHelper;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.ui.base.BaseActivity;
import it.mozart.yeap.ui.support.Act_ImageCrop;
import it.mozart.yeap.ui.views.AutoRefreshImageView;
import it.mozart.yeap.utility.APIErrorUtils;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.Prefs;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class Act_CreateProfile extends BaseActivity {

    @BindView(R.id.nickname)
    EditText mNickname;
    @BindView(R.id.avatar)
    AutoRefreshImageView mAvatar;
    @BindView(R.id.continua)
    Button mContinua;

    @OnClick(R.id.avatar)
    void avatarClicked() {
        avatarClickedMethod(false);
    }

    @OnClick(R.id.continua)
    void continuaClicked() {
        if (mRetry) {
            initialize();
            return;
        }

        if (mNickname.getText().length() == 0) {
            Toast.makeText(this, getString(R.string.profilo_nickname_error), Toast.LENGTH_SHORT).show();
            return;
        }

        if (!AndroidPermissionHelper.isPermissionGranted(this, Manifest.permission.READ_CONTACTS)) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.READ_CONTACTS)) {
                new MaterialDialog.Builder(this)
                        .title(getString(R.string.system_login_contact_permission))
                        .content(getString(R.string.system_login_contact_permission_message))
                        .positiveText(getString(R.string.general_procedi))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                AndroidPermissionHelper.requestPermissions(Act_CreateProfile.this,
                                        new String[]{Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.CONTACTS_PERMISSION_CODE);
                            }
                        })
                        .show();
            } else {
                AndroidPermissionHelper.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.CONTACTS_PERMISSION_CODE);
            }
            return;
        }

        getDynamicView().showLoading();

        if (mAvatarFile != null) {
            uploadAvatar();
        } else if (!mNickname.getText().toString().equals(mUser.getNickname())) {
            updateUser();
        } else {
            continua(mUser.getNickname());
        }
    }

    // SUPPORT

    @Inject
    ProfileApi profileApi;
    @Inject
    UploadApi uploadApi;

    private User mUser;
    private File mAvatarFile;
    private boolean mRetry;
    private boolean mIgnoreRestore;

    private static final String PARAM_UTENTE = "paramUser";

    private static final int IMG_CODE = 701;
    private static final int IMG_CROP_CODE = 702;

    // SYSTEM

    public static Intent newIntent() {
        return new Intent(App.getContext(), Act_CreateProfile.class);
    }

    public static Intent newIntent(User utente) {
        Intent intent = new Intent(App.getContext(), Act_CreateProfile.class);
        intent.putExtra(PARAM_UTENTE, Parcels.wrap(utente));
        return intent;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_create_profile;
    }

    @Override
    protected void initVariables() {
        mRetry = false;
        mIgnoreRestore = false;
    }

    @Override
    protected void loadParameters(Bundle extras) {
        mUser = Parcels.unwrap(extras.getParcelable(PARAM_UTENTE));
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    protected void initialize() {
        if (mUser != null) {
            updateButtonText(false);

            if (mUser.getNickname() == null || mUser.getNickname().isEmpty()) {
                mIgnoreRestore = true;
            }
            mNickname.setText(mUser.getNickname());
            mNickname.setSelection(mNickname.getText().length());

            mAvatar.bindThumb(mUser.getUuid());

            Prefs.putString(Constants.PREF_PROFILE_ID, mUser.getUuid());
            Prefs.putString(Constants.PREF_PROFILE_PHONE, mUser.getPhone());

            if (!mIgnoreRestore) {
                RealmConfiguration configuration = new RealmConfiguration.Builder()
                        .name(String.format(getString(R.string.database_name), mUser.getUuid()))
                        .build();
                if (new File(configuration.getPath()).exists()) {
                    mIgnoreRestore = true;
                }
            }
        } else {
            getDynamicView().showLoading();
            registerSubscription(profileApi.getMyProfile()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<User>() {
                        @Override
                        public void call(User user) {
                            getDynamicView().hide();
                            mUser = user;

                            initialize();
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            getDynamicView().hide();
                            updateButtonText(true);

                            APIError error = APIErrorUtils.parseError(Act_CreateProfile.this, throwable);
                            Toast.makeText(Act_CreateProfile.this, error.message(), Toast.LENGTH_LONG).show();
                        }
                    }));
        }

        Prefs.putBoolean(Constants.LOGIN_EFFECTUATED, true);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.IMAGE_PERMISSION_CODE:
                avatarClickedMethod(true);
                break;
            case Constants.CONTACTS_PERMISSION_CODE:
                if (grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (grantResults[1] != PackageManager.PERMISSION_GRANTED) {
                        Prefs.putInt(Constants.SETTINGS_AUTODOWNLOAD, 2);
                    }
                    continuaClicked();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case IMG_CODE:
                    // Chiamo l'activity per il crop dell'immagine selezionata
                    Uri copertinaUri = ImageInputHelper.getPickImageResultUri(this, data);
                    startActivityForResult(Act_ImageCrop.newIntent(copertinaUri, 1, 1), IMG_CROP_CODE);
                    break;
                case IMG_CROP_CODE:
                    String image = data.getStringExtra(Constants.IMAGE_CROP);
                    if (image != null) {
                        // Salvo l'immagine come File per per poi poterla usare alla creazione dell'attività
                        mAvatarFile = new File(image);
                        Glide.with(this).load(mAvatarFile).asBitmap().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(new BitmapImageViewTarget(mAvatar) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                mAvatar.setImageDrawable(circularBitmapDrawable);
                            }
                        });
                    } else {
                        // Mostro messaggio di errore se non è stato restituito niente dall'activity di crop
                        mAvatarFile = null;
                        Toast.makeText(this, getString(R.string.general_error_ritaglio_immagine), Toast.LENGTH_LONG).show();
                    }
                    break;
            }
        }
    }

    // FUNCTIONS

    /**
     * Eseguo l'upload dell'avatar
     */
    private void uploadAvatar() {
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), mAvatarFile);
        final MultipartBody.Part image = MultipartBody.Part.createFormData("image", mAvatarFile.getName(), requestFile);
        registerSubscription(uploadApi.uploadAvatar(image)
                .subscribeOn(Schedulers.newThread())
                .flatMap(new Func1<ImageApiModel, Observable<User>>() {
                    @Override
                    public Observable<User> call(ImageApiModel imageApiModel) {
                        Glide.get(Act_CreateProfile.this).clearDiskCache();
                        User utente = new User();
                        utente.setAvatar(imageApiModel.getUploadref());
                        utente.setNickname(mNickname.getText().length() != 0 ? mNickname.getText().toString() : null);
                        return profileApi.updateProfile(utente);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<User>() {
                    @Override
                    public void call(User utenteResponse) {
                        continua(utenteResponse.getNickname());
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        getDynamicView().hide();
                        throwable.printStackTrace();
                    }
                }));
    }

    /**
     * Update dell'utente
     */
    private void updateUser() {
        User utente = new User();
        utente.setNickname(mNickname.getText().length() != 0 ? mNickname.getText().toString() : null);
        registerSubscription(profileApi.updateProfile(utente)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<User>() {
                    @Override
                    public void call(User utenteResponse) {
                        continua(utenteResponse.getNickname());
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        getDynamicView().hide();
                        throwable.printStackTrace();
                    }
                }));
    }

    /**
     * Passo alla schermata successiva
     *
     * @param nickname nickname
     */
    private void continua(String nickname) {
        mUser.setNickname(nickname);
        startActivity(Act_Sync.newIntent(mUser, mIgnoreRestore));
        finish();
    }

    private void updateButtonText(boolean retry) {
        mRetry = retry;
        if (mRetry) {
            mContinua.setText(getString(R.string.crea_profilo_button_riprova));
        } else {
            mContinua.setText(getString(R.string.crea_profilo_button_continua));
        }
    }

    private void avatarClickedMethod(boolean ignoreCheck) {
        ImageInputHelper.openChooserForAvatarSelect(this, IMG_CODE, ignoreCheck);
    }
}
