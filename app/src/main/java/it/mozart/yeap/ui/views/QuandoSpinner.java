package it.mozart.yeap.ui.views;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.AppCompatSpinner;
import android.util.AttributeSet;

public class QuandoSpinner extends AppCompatSpinner {

    OnItemSelectedListener listener;

    public QuandoSpinner(Context context) {
        super(context);
    }

    public QuandoSpinner(Context context, int mode) {
        super(context, mode);
    }

    public QuandoSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public QuandoSpinner(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public QuandoSpinner(Context context, AttributeSet attrs, int defStyleAttr, int mode) {
        super(context, attrs, defStyleAttr, mode);
    }

    public QuandoSpinner(Context context, AttributeSet attrs, int defStyleAttr, int mode, Resources.Theme popupTheme) {
        super(context, attrs, defStyleAttr, mode, popupTheme);
    }

    @Override
    public void setSelection(int position) {
        super.setSelection(position);
        if (listener != null)
            listener.onItemSelected(null, null, position, 0);
    }

    public void setOnItemSelectedEvenIfUnchangedListener(OnItemSelectedListener listener) {
        this.listener = listener;
    }
}
