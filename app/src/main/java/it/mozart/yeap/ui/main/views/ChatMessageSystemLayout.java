package it.mozart.yeap.ui.main.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.util.Locale;

import it.mozart.yeap.R;
import it.mozart.yeap.common.SystemMessage;
import it.mozart.yeap.events.EventSelectedEvent;
import it.mozart.yeap.helpers.AnalitycsProvider;
import it.mozart.yeap.realm.Message;
import it.mozart.yeap.ui.events.main.events.PollSelectedEvent;
import it.mozart.yeap.ui.main.ChatMessageBinder;
import it.mozart.yeap.ui.main.ChatMessageListener;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.DateUtils;

public class ChatMessageSystemLayout extends FrameLayout implements ChatMessageBinder {

    View mNonLettoLayout;
    TextView mNonLetto;
    TextView mMessaggio;
    TextView mDate;
    ImageView mIcon;

    public ChatMessageSystemLayout(Context context) {
        super(context);
        init();
    }

    public ChatMessageSystemLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ChatMessageSystemLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @SuppressWarnings("unused")
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ChatMessageSystemLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        inflate(getContext(), R.layout.item_chat_system, this);
        mNonLettoLayout = findViewById(R.id.non_letto_layout);
        mNonLetto = findViewById(R.id.non_letto);
        mDate = findViewById(R.id.date);
        mIcon = findViewById(R.id.icon);
        mMessaggio = findViewById(R.id.messaggio);
        mMessaggio.setMovementMethod(LinkMovementMethod.getInstance());
    }

    /**
     * Esegue il bind tra i dati del messaggio e le view
     *
     * @param messaggio messaggio
     * @param adapter   adapter interface
     * @param position  posizione
     */
    public void bind(final Message messaggio, int position, ChatMessageListener adapter) {
        if (messaggio != null) {
            showMessageText(messaggio, adapter);

            // Visualizzo il numero di messaggi ancora da leggere
            if (messaggio.isFirstNotRead()) {
                mNonLettoLayout.setVisibility(View.VISIBLE);
                if (adapter.getNumNewMessages() == 0) {
                    mNonLetto.setText(String.format("1 %s", getContext().getString(R.string.chat_messaggio_non_letto)));
                } else {
                    mNonLetto.setText(String.format(Locale.getDefault(), "%d %s", adapter.getNumNewMessages(), getContext().getString(R.string.chat_messaggi_non_letti)));
                }
            } else {
                mNonLettoLayout.setVisibility(View.GONE);
            }

            showMessageDate(messaggio, adapter, position);
        }
    }

    /**
     * Visualizzo il messaggio corretto e assegno un'azione al click, se deve essere presente
     *
     * @param messaggio messaggio
     * @param adapter   adapter interface
     */
    @SuppressWarnings("ConfusingArgumentToVarargsMethod")
    private void showMessageText(final Message messaggio, final ChatMessageListener adapter) {
        setOnClickListener(null);

        final SystemMessage systemMessage = new Gson().fromJson(messaggio.getSystemTextJson(), SystemMessage.class);
        mMessaggio.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (systemMessage.getClickId() != null && adapter.getSelectedMessageIds().size() == 0) {
                    messageClicked(messaggio.getSystemType(), systemMessage);
                }
            }
        });

        switch (messaggio.getSystemType()) {
            case Constants.SYSTEM_MESSAGE_CREAZIONE_SONDAGGIO:
            case Constants.SYSTEM_MESSAGE_CREAZIONE_SONDAGGIO_SPECIAL:
            case Constants.SYSTEM_MESSAGE_CREAZIONE_VOCE_SONDAGGIO:
            case Constants.SYSTEM_MESSAGE_CREAZIONE_VOCE_SONDAGGIO_SPECIAL:
            case Constants.SYSTEM_MESSAGE_ELIMINAZIONE_SONDAGGIO:
            case Constants.SYSTEM_MESSAGE_ELIMINAZIONE_VOCE_SONDAGGIO:
                mIcon.setVisibility(VISIBLE);
                mIcon.setImageResource(R.drawable.ico_sondaggi);
                break;
            case Constants.SYSTEM_MESSAGE_CREAZIONE_PROPOSTA:
            case Constants.SYSTEM_MESSAGE_ELIMINAZIONE_PROPOSTA:
                mIcon.setVisibility(VISIBLE);
                mIcon.setImageResource(R.drawable.ico_megafono_azure);
                break;
            default:
                mIcon.setVisibility(GONE);
                break;
        }

        Spanned testo = messaggio.getFormattedMessage(getContext());
        mMessaggio.setText(testo);
    }

    /**
     * @param systemType    tipo messaggio
     * @param systemMessage messaggio di sistema
     */
    private void messageClicked(int systemType, SystemMessage systemMessage) {
        switch (systemType) {
            case Constants.SYSTEM_MESSAGE_CREAZIONE_VOCE_SONDAGGIO:
            case Constants.SYSTEM_MESSAGE_CREAZIONE_VOCE_SONDAGGIO_SPECIAL:
                AnalitycsProvider.getInstance().logCustomOrigin(null, "viewPoll", "chat");
                EventBus.getDefault().post(new PollSelectedEvent(systemMessage.getClickIdParent()));
                break;
            case Constants.SYSTEM_MESSAGE_CREAZIONE_SONDAGGIO:
            case Constants.SYSTEM_MESSAGE_CREAZIONE_SONDAGGIO_SPECIAL:
                AnalitycsProvider.getInstance().logCustomOrigin(null, "viewPoll", "chat");
                EventBus.getDefault().post(new PollSelectedEvent(systemMessage.getClickId()));
                break;
            case Constants.SYSTEM_MESSAGE_GRUPPO_CREAZIONE_EVENTO:
                AnalitycsProvider.getInstance().logCustomOrigin(null, "viewEvent", "chat");
                EventBus.getDefault().post(new EventSelectedEvent(systemMessage.getClickId()));
                break;
        }
    }

    /**
     * Mostro la data (giorno) dei messaggi (solo sul primo messaggio della giornata)
     *
     * @param messaggio messaggio
     * @param adapter   adapter interface
     * @param position  position
     */
    private void showMessageDate(Message messaggio, ChatMessageListener adapter, int position) {
        if (position != adapter.getNumItems() - 1) {
            Message after = adapter.getMessaggi().get(position + 1);
            if (!adapter.getFormatter().format(messaggio.getCreatedAt()).equals(adapter.getFormatter().format(after.getCreatedAt()))) {
                mDate.setText(DateUtils.smartFormatDateOnly(getContext(), messaggio.getCreatedAt(), "dd MMMM yyyy"));
                mDate.setVisibility(View.VISIBLE);
            } else {
                mDate.setText(null);
                mDate.setVisibility(View.GONE);
            }
        } else {
            mDate.setText(DateUtils.smartFormatDateOnly(getContext(), messaggio.getCreatedAt(), "dd MMMM yyyy"));
            mDate.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void unbind() {

    }
}
