package it.mozart.yeap.ui.sondaggi.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;
import it.mozart.yeap.R;
import it.mozart.yeap.realm.Vote;
import it.mozart.yeap.ui.views.AutoRefreshImageView;
import it.mozart.yeap.utility.DateUtils;

public class SondaggioVoceVotiAdapter extends RealmRecyclerViewAdapter<Vote, RecyclerView.ViewHolder> {

    private Context mContext;
    private String mFormat;

    public SondaggioVoceVotiAdapter(Context context, RealmResults<Vote> voti) {
        super(voti, true);
        mContext = context;
        mFormat = context.getString(R.string.evento_voto_voce_data_semplificato);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_utenti_voto_voci, parent, false);
        return new VotiViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final VotiViewHolder realHolder = (VotiViewHolder) holder;

        Vote voto = getItem(position);
        if (voto == null) {
            return;
        }

        realHolder.name.setText(voto.getCreator().getChatName());
        realHolder.desc.setText(DateUtils.smartFormatDateOnly(mContext, voto.getCreatedAt(), mFormat));

        realHolder.avatar.bindThumb(voto.getCreatorUuid());
    }

    class VotiViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.avatar)
        AutoRefreshImageView avatar;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.desc)
        TextView desc;

        VotiViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
