package it.mozart.yeap.ui.home.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;
import it.mozart.yeap.events.GroupSelectedEvent;
import it.mozart.yeap.realm.Group;
import it.mozart.yeap.ui.home.views.GroupLayout;

public class HomeGroupsAdapter extends RealmRecyclerViewAdapter<Group, RecyclerView.ViewHolder> {

    private Context mContext;

    public HomeGroupsAdapter(Context context, OrderedRealmCollection<Group> data) {
        super(data, true);
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MainGroupsViewHolder(new GroupLayout(mContext));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final MainGroupsViewHolder realHolder = (MainGroupsViewHolder) holder;
        Group gruppo = getItem(position);

        ((GroupLayout) realHolder.itemView).bind(gruppo, false);
    }

    class MainGroupsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        MainGroupsViewHolder(View v) {
            super(v);

            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getLayoutPosition();
            Group gruppo = getItem(position);
            if (gruppo != null) {
                EventBus.getDefault().post(new GroupSelectedEvent(gruppo.getUuid()));
            }
        }
    }
}
