package it.mozart.yeap.ui.profile;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import it.mozart.yeap.R;
import it.mozart.yeap.events.ImageSelectedEvent;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.ui.base.BaseFragment;
import it.mozart.yeap.ui.views.AutoRefreshImageView;

public class Frg_ProfiloAltrui extends BaseFragment {

    @BindView(R.id.avatar)
    AutoRefreshImageView mAvatar;
    @BindView(R.id.nome)
    TextView mNome;
    @BindView(R.id.num_eventi)
    TextView mNumEventi;
    @BindView(R.id.telefono)
    TextView mTelefono;
    @BindView(R.id.add_layout)
    View mAddLayout;
    @BindView(R.id.show_layout)
    View mShowLayout;

    @OnClick(R.id.avatar)
    void avatarClicked() {
        EventBus.getDefault().post(new ImageSelectedEvent(mUtente.getAvatarLarge(), mAvatar, true));
    }

    @OnClick(R.id.attivita_layout)
    void eventoClicked() {
        startActivity(Act_ProfiloAltruiEventi.newIntent(mEventiIds.toArray(new String[mEventiIds.size()])));
    }

    @OnClick(R.id.telefono_layout)
    void telefonoClicked() {
        if (mTelefono.getText().length() > 0) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + mTelefono.getText()));
            if (intent.resolveActivity(getContext().getPackageManager()) != null) {
                startActivity(intent);
            } else {
                Toast.makeText(getContext(), getString(R.string.general_error_no_app_found), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @OnClick(R.id.sms_layout)
    void smsClicked() {
        if (mTelefono.getText().length() > 0) {
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("smsto:" + mTelefono.getText()));
            if (intent.resolveActivity(getContext().getPackageManager()) != null) {
                startActivity(intent);
            } else {
                Toast.makeText(getContext(), getString(R.string.general_error_no_app_found), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @OnClick(R.id.show_layout)
    void showClicked() {
        if (mUtente != null) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, mUtente.getContactId());
            intent.setData(uri);
            if (intent.resolveActivity(getContext().getPackageManager()) != null) {
                startActivity(intent);
            } else {
                Toast.makeText(getContext(), getString(R.string.general_error_no_app_found), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @OnClick(R.id.add_layout)
    void addClicked() {
        if (mUtente != null) {
            Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
            intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
            if (mUtente.getPhone() != null) {
                intent.putExtra(ContactsContract.Intents.Insert.PHONE, mUtente.getPhone());
            }
            if (mUtente.getNickname() != null) {
                intent.putExtra(ContactsContract.Intents.Insert.NAME, mUtente.getNickname());
            }
            intent.putExtra(ContactsContract.Intents.Insert.PHONE_TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);
            if (intent.resolveActivity(getContext().getPackageManager()) != null) {
                startActivity(intent);
            } else {
                Toast.makeText(getContext(), getString(R.string.general_error_no_app_found), Toast.LENGTH_SHORT).show();
            }
        }
    }

    // SUPPORT

    @Inject
    Realm realm;

    private User mUtente;
    private RealmChangeListener<User> mListener;
    private List<String> mEventiIds;
    private String mId;

    private static final String PARAM_ID = "paramId";

    // SYSTEM

    public static Frg_ProfiloAltrui newInstance(String utenteId) {
        Bundle args = new Bundle();
        args.putString(PARAM_ID, utenteId);
        Frg_ProfiloAltrui fragment = new Frg_ProfiloAltrui();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_profilo_altrui;
    }

    @Override
    protected void initValues() {
        mEventiIds = new ArrayList<>();
    }

    @Override
    protected void loadParameters(Bundle arguments) {
        mId = arguments.getString(PARAM_ID);
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mUtente != null)
            mUtente.removeChangeListener(mListener);
    }

    @Override
    protected void initialize() {
        mUtente = realm.where(User.class).equalTo("uuid", mId).findFirst();
        if (mUtente == null) {

        } else {
            updateInfo(mUtente);

            mListener = new RealmChangeListener<User>() {
                @Override
                public void onChange(@NonNull User element) {
                    updateInfo(element);
                }
            };
            mUtente.addChangeListener(mListener);
        }
    }

    // FUNCTIONS

    /**
     * Aggiorna le view con le informazioni dell'utente
     *
     * @param utente utente
     */
    private void updateInfo(User utente) {
        if (utente != null) {
            mEventiIds = new ArrayList<>();

            mNome.setText(utente.getChatName());
            mTelefono.setText(utente.getPhone());

            // Numero di attività in comune
            int numAttivita = 0;
            RealmResults<Event> eventi = realm.where(Event.class).findAll();
            for (Event evento : eventi) {
                if (evento.getGuests().contains(utente)) {
                    numAttivita++;
                    mEventiIds.add(evento.getUuid());
                }
            }
            mNumEventi.setText(String.format(Locale.getDefault(), "%d", numAttivita));

            // Avatar dell'utente
            mAvatar.bindThumb(utente.getUuid());

            mAddLayout.setVisibility(utente.isInContacts() ? View.GONE : View.VISIBLE);
            mShowLayout.setVisibility(utente.isInContacts() ? View.VISIBLE : View.GONE);
        } else {
            mAddLayout.setVisibility(View.GONE);
            mShowLayout.setVisibility(View.VISIBLE);
        }
    }
}
