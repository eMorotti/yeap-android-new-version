package it.mozart.yeap.ui.views;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.util.ArrayList;
import java.util.List;

import it.mozart.yeap.R;
import it.mozart.yeap.realm.Vote;
import it.mozart.yeap.utility.Prefs;
import it.mozart.yeap.utility.Reversed;
import it.mozart.yeap.utility.Utils;

public class VotantiWidget extends View {

    private Paint paint;
    private ValueAnimator animator;

    private OnWidthChangeListener onWidthChangeListener;
    private List<Vote> voti;
    private String tag;
    private int totale;

    private List<VotoInfo> votiInfo;
    private List<VotoInfo> votiInfovotoInfo;
    private List<VotoInfo> votiInfoAfter;
    private Line line;
    private float lineOffset;
    private float rightOffset;
    private float scaleStep;
    private float xStep;
    private float xStepDiff;
    private float xStepOffset;
    private float radiusOffset;
    private int foregroundColor = Color.TRANSPARENT;
    private int sizeDiff;

    public VotantiWidget(Context context) {
        super(context);
        initialize(null);
    }

    public VotantiWidget(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialize(attrs);
    }

    public VotantiWidget(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(attrs);
    }

    @SuppressWarnings("unused")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public VotantiWidget(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initialize(attrs);
    }

    private void initialize(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.VotantiWidget, 0, 0);
            foregroundColor = a.getColor(R.styleable.VotantiWidget_vw_color, foregroundColor);
            a.recycle();
        }

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStrokeWidth(Utils.dpToPx(1.5f, getContext()));
        paint.setTextAlign(Paint.Align.CENTER);

        setTag("");

        votiInfo = new ArrayList<>();
        votiInfovotoInfo = new ArrayList<>();
        votiInfoAfter = new ArrayList<>();
        line = new Line();
        lineOffset = Utils.dpToPx(10, getContext());
        rightOffset = Utils.dpToPx(10, getContext());
        radiusOffset = Utils.dpToPx(1, getContext());
        scaleStep = 0.05f;//.05f;
        xStepDiff = 0;
        xStepOffset = 0;
        tag = null;
    }

    public void setVoti(String tag, List<Vote> voti, int totale) {
        if (totale != 0) {
            if (voti.size() > 9) {
                this.voti = voti.subList(0, 9);
                Vote vote = new Vote();
                vote.setCreatorUuid("");
                this.voti.add(vote);
                this.sizeDiff = voti.size() - this.voti.size();
            } else {
                this.voti = voti;
            }
            this.tag = tag;
            if (totale > 9) {
                totale = 10;
            }
        } else {
            this.voti = voti;
            this.tag = tag;
        }
        this.totale = totale;
        updateState();
    }

    public boolean isTagPresent() {
        return this.tag != null;
    }

    public void setOnWidthChangeListener(OnWidthChangeListener onWidthChangeListener) {
        this.onWidthChangeListener = onWidthChangeListener;
    }

    private void updateState() {
        if (tag.equals(getTag())) {
            if (animator != null && animator.isRunning()) {
                animator.cancel();
            }

            int realWidth = (int) (getWidth() - getHeight() - rightOffset);
            float xStepTemp = realWidth / (float) totale;

            List<VotoInfo> newVotiInfo = new ArrayList<>();
            votiInfovotoInfo.clear();
            votiInfoAfter.clear();
            for (int i = 0; i < voti.size(); i++) {
                Vote voto = voti.get(voti.size() - i - 1);
                int index = votiInfo.indexOf(new VotoInfo(voto.getCreatorUuid()));
                VotoInfo votoInfo;
                if (index == -1) {
                    votoInfo = new VotoInfo();
                    votoInfo.userUuid = voto.getCreatorUuid();
                    votoInfo.position = i;
                    votoInfo.posX = xStepTemp * voti.size() * 1
                            - xStepTemp * i * 1;
                    votoInfo.posY = getHeight() / 2f;
                    votoInfo.scale = 0f;
                    votoInfo.scaleDiff = 1f - scaleStep * i;
                    votoInfo.alpha = 0f;
                    votoInfo.alphaDiff = 1f;
                    votoInfo.radius = 0f;
                    votoInfo.radiusDiff = Math.min(xStepTemp * 1, getHeight() / 2f);

                    loadImage(votoInfo);
                } else {
                    votoInfo = votiInfo.remove(index);
                    votoInfo.posY = getHeight() / 2f;

                    votoInfo.posXDiff = votoInfo.posX - (xStepTemp * voti.size() * 1
                            - xStepTemp * i * 1);
                    votoInfo.scaleDiff = votoInfo.scale - (1f - scaleStep * i);
                    votoInfo.radiusDiff = votoInfo.radius - getHeight() / 2f;

                    votoInfo.userUuid = voto.getCreatorUuid();
                    votoInfo.position = i;
                    votoInfo.alpha = 1f;
                }

                if (votoInfo.userUuid == null || votoInfo.userUuid.equals("")) {
                    votoInfo.userUuid = "";
                    votoInfo.isSpecial = true;
//                    votoInfo.posX = votoInfo.posX - xStepTemp + Math.min(xStepTemp * 1, getHeight() / 2f);

                    if (index == -1) {
                        votiInfoAfter.add(0, votoInfo);
                    } else {
                        newVotiInfo.add(0, votoInfo);
                    }
                } else {
                    if (index == -1) {
                        votiInfoAfter.add(votoInfo);
                    } else {
                        newVotiInfo.add(votoInfo);
                    }
                }
            }

            for (VotoInfo votoInfo : votiInfo) {
                votoInfo.alphaDiff = -votoInfo.alpha;
                votoInfo.scaleDiff = -votoInfo.scale;
            }
            votiInfovotoInfo = votiInfo;
            votiInfo = newVotiInfo;

            line.posEndY = getHeight() / 2f;
            line.posEndXRealDiff = line.posEndXReal - (xStepTemp * voti.size() * 1);
            float newXStep = voti.size() != 0 ? xStepTemp * (voti.size() - 1) + getHeight() * 3 / 2 : 0;
            line.posEndXDiff = line.posEndX - newXStep;

            xStepDiff = xStep - xStepTemp;

            animateUpdate();
        } else {
            int realWidth = (int) (getWidth() - getHeight() - rightOffset);
            xStep = realWidth / (float) totale;

            votiInfo.clear();
            for (int i = 0; i < voti.size(); i++) {
                String voto = voti.get(voti.size() - i - 1).getCreatorUuid();
                VotoInfo votoInfo = new VotoInfo();
                votoInfo.userUuid = voto;
                votoInfo.position = i;
                votoInfo.posX = xStep * voti.size() * 1
                        - xStep * i * 1;
                votoInfo.posY = getHeight() / 2f;
                votoInfo.scale = 1f - scaleStep * i;
                votoInfo.alpha = 1f;
//                votoInfo.radius = Math.min(xStep * 1, getHeight() / 2f);
                votoInfo.radius = getHeight() / 2f;


                if (votoInfo.userUuid == null || votoInfo.userUuid.equals("")) {
                    votoInfo.userUuid = "";
                    votoInfo.isSpecial = true;

                    votiInfo.add(0, votoInfo);
                } else {
                    votiInfo.add(votoInfo);

                    loadImage(votoInfo);
                }
            }

            line.posEndY = getHeight() / 2f;
            line.posEndXReal = voti.size() != 0 ? xStep * voti.size() * 1 : 0;
            line.posEndX = voti.size() != 0 ? xStep * (voti.size() - 1) + getHeight() * 3 / 2 : 0;

//            if (onWidthChangeListener != null) {
//                onWidthChangeListener.widthChanged(line.posEndXReal + line.posEndXRealOffset + getHeight() / 2);
//            }

            invalidate();
        }

        setTag(tag);
    }

    private void animateUpdate() {
        animator = ValueAnimator.ofFloat(0, 1);
        animator.setDuration(250);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                for (VotoInfo votoInfo : votiInfovotoInfo) {
                    votoInfo.alphaOffset = animation.getAnimatedFraction() * votoInfo.alphaDiff;
                    votoInfo.scaleOffset = animation.getAnimatedFraction() * votoInfo.scaleDiff;
                    votoInfo.radiusOffset = animation.getAnimatedFraction() * votoInfo.radiusDiff;
                }
                for (VotoInfo votoInfo : votiInfo) {
                    votoInfo.posXOffset = -animation.getAnimatedFraction() * votoInfo.posXDiff;
                    votoInfo.scaleOffset = -animation.getAnimatedFraction() * votoInfo.scaleDiff;
                    votoInfo.radiusOffset = -animation.getAnimatedFraction() * votoInfo.radiusDiff;
                }
                for (VotoInfo votoInfo : votiInfoAfter) {
                    votoInfo.alphaOffset = animation.getAnimatedFraction() * votoInfo.alphaDiff;
                    votoInfo.scaleOffset = animation.getAnimatedFraction() * votoInfo.scaleDiff;
                    votoInfo.radiusOffset = animation.getAnimatedFraction() * votoInfo.radiusDiff;
                }
                line.posEndXOffset = -animation.getAnimatedFraction() * line.posEndXDiff;
                line.posEndXRealOffset = -animation.getAnimatedFraction() * line.posEndXRealDiff;
                xStepOffset = -animation.getAnimatedFraction() * xStepDiff;
                if (onWidthChangeListener != null) {
                    onWidthChangeListener.widthChanged(line.posEndXReal + line.posEndXRealOffset + getHeight() / 2);
                }
                invalidate();
            }
        });
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationCancel(Animator animation) {
                super.onAnimationCancel(animation);
                animation.end();
                animation.removeAllListeners();
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                votiInfo.addAll(votiInfoAfter);
                for (VotoInfo votoInfo : votiInfo) {
                    votoInfo.reset();
                }
                votiInfovotoInfo.clear();
                votiInfoAfter.clear();
                line.reset();
                xStep = xStep + xStepOffset;
                xStepDiff = 0;
                xStepOffset = 0;
                invalidate();
            }
        });
        animator.start();
    }

    private void loadImage(final VotoInfo votoInfo) {
        if (votoInfo.userUuid != null && !votoInfo.isSpecial) {
            votoInfo.resetBitmap();
            // Recupero l'avatar
            try {
                final String url = Utils.getApiURL() + "/users/" + votoInfo.userUuid + "/avatar?type=thumb&v=" + Prefs.getString(votoInfo.userUuid, "0");
                final float radius = votoInfo.radius + votoInfo.radiusDiff;
                Glide.with(getContext()).load(url).asBitmap().centerCrop().placeholder(R.drawable.avatar_utente).into(new SimpleTarget<Bitmap>((int) (radius * 2f), (int) (radius * 2f)) {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        votoInfo.setBitmap(resource);
                        postInvalidate();
                    }

                    @Override
                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                        super.onLoadFailed(e, errorDrawable);
                        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.avatar_utente);
                        Bitmap bitmapResized = Bitmap.createScaledBitmap(icon, (int) (radius * 2f), (int) (radius * 2f), false);
                        votoInfo.setBitmap(bitmapResized);
                        postInvalidate();
                    }
                });
            } catch (Exception ignore) {
                // risolvere problema quando l'activity si sta distruggendo
            }
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (line.posEndXReal + line.posEndXRealOffset > 0) {
            LinearGradient linearGradient = new LinearGradient(-lineOffset, 0, line.posEndXReal + line.posEndXRealOffset, 0, Color.TRANSPARENT, foregroundColor, Shader.TileMode.CLAMP);
            paint.setShader(linearGradient);
            canvas.drawLine(-lineOffset, getHeight() / 2, line.posEndXReal + line.posEndXRealOffset, getHeight() / 2, paint);
            paint.setShader(null);
        }

        for (VotoInfo votoInfo : Reversed.reversed(votiInfovotoInfo)) {
            drawVote(canvas, votoInfo);
        }
        for (VotoInfo votoInfo : Reversed.reversed(votiInfoAfter)) {
            drawVote(canvas, votoInfo);
        }
        for (VotoInfo votoInfo : Reversed.reversed(votiInfo)) {
            drawVote(canvas, votoInfo);
        }
    }

    private void drawVote(Canvas canvas, VotoInfo votoInfo) {
        paint.setStyle(Paint.Style.FILL);
        float radius = votoInfo.radius + votoInfo.radiusOffset;
        float scale = votoInfo.scale + votoInfo.scaleOffset;
        float posX = votoInfo.posX + votoInfo.posXOffset;
        if (votoInfo.isSpecial) {
            paint.setColor(ContextCompat.getColor(getContext(), R.color.colorLightBlueEventoTransparent));
            paint.setAlpha(255);
            canvas.drawCircle(posX, votoInfo.posY, radius * scale, paint);

            paint.setColor(Color.WHITE);
            paint.setTextSize(45 * scale);
            paint.setAlpha((int) (255f * (votoInfo.alpha + votoInfo.alphaOffset)));
            canvas.drawText("+" + sizeDiff, posX, votoInfo.posY - ((paint.descent() + paint.ascent()) / 2f), paint);
        } else {
            paint.setColor(Color.LTGRAY);
            canvas.drawCircle(posX, votoInfo.posY, radius * scale - radiusOffset, paint);

            // Disegno icona
            if (votoInfo.bitmap != null) {
                votoInfo.rectF.set(posX - radius * scale,
                        votoInfo.posY - radius * scale,
                        posX + radius * scale,
                        votoInfo.posY + radius * scale);

                paint.setAlpha((int) (255f * (votoInfo.alpha + votoInfo.alphaOffset)));
                canvas.drawBitmap(votoInfo.bitmap, null, votoInfo.rectF, paint);
            } else {
                paint.setColor(Color.WHITE);
                paint.setAlpha((int) (255f * (votoInfo.alpha + votoInfo.alphaOffset) * 0.7f));
                canvas.drawCircle(posX, votoInfo.posY, radius * scale, paint);
            }

//            paint.setColor(foregroundColor);
//            paint.setAlpha((int) (100f * (votoInfo.alpha + votoInfo.alphaOffset)));
//            canvas.drawCircle(posX, votoInfo.posY, radius * scale, paint);
//            paint.setStyle(Paint.Style.STROKE);
//            canvas.drawCircle(posX, votoInfo.posY, (radius * scale + strokeOffset * (2f * scale - 1f)), paint);
        }
    }

    public int getCount() {
        return votiInfo.size();
    }

    private class VotoInfo {
        String userUuid;
        boolean isSpecial;
        int position;
        float posX;
        float posY;
        float posXDiff;
        float posXOffset;
        float scale;
        float scaleDiff;
        float scaleOffset;
        float alpha;
        float alphaDiff;
        float alphaOffset;
        float radius;
        float radiusDiff;
        float radiusOffset;
        RectF rectF;
        Bitmap bitmap;

        VotoInfo() {
            this.posXDiff = 0;
            this.posXOffset = 0;
            this.scaleDiff = 0;
            this.scaleOffset = 0;
            this.radiusDiff = 0;
            this.radiusOffset = 0;
            this.rectF = new RectF();
        }

        VotoInfo(String userUuid) {
            this.userUuid = userUuid;
            this.posXDiff = 0;
            this.posXOffset = 0;
            this.scaleDiff = 0;
            this.scaleOffset = 0;
            this.radiusDiff = 0;
            this.radiusOffset = 0;
            this.rectF = new RectF();
        }

        void reset() {
            this.posX = this.posX + this.posXOffset;
            this.posXDiff = 0;
            this.posXOffset = 0;
            this.scale = this.scale + this.scaleOffset;
            this.scaleDiff = 0;
            this.scaleOffset = 0;
            this.alpha = this.alpha + this.alphaOffset;
            this.alphaDiff = 0;
            this.alphaOffset = 0;
            this.radius = this.radius + this.radiusOffset;
            this.radiusDiff = 0;
            this.radiusOffset = 0;
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof VotoInfo) {
                return this.isSpecial == ((VotoInfo) o).isSpecial || this.userUuid.equals(((VotoInfo) o).userUuid);
            }
            return super.equals(o);
        }

        void resetBitmap() {
            if (this.bitmap != null)
                this.bitmap.recycle();
            this.bitmap = null;
        }

        void setBitmap(Bitmap bitmap) {
            // Copia la bitmap
            Bitmap targetBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);

            // Imposto il canvas
            Canvas canvas = new Canvas(targetBitmap);
            final Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setFilterBitmap(true);
            paint.setDither(true);
            paint.setAntiAlias(true);

            // Coloro il canvas
            canvas.drawARGB(0, 0, 0, 0);

            // Disegno un cerchio sui cui filtro la bitmap
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
            canvas.drawCircle(((float) bitmap.getWidth() - 1) / 2, ((float) bitmap.getHeight() - 1) / 2, (Math.min(((float) bitmap.getWidth()), ((float) bitmap.getHeight())) / 2), paint);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);

            this.bitmap = targetBitmap;
        }
    }

    private class Line {
        float posEndX;
        float posEndXReal;
        float posEndY;
        float posEndXDiff;
        float posEndXOffset;
        float posEndXRealDiff;
        float posEndXRealOffset;

        Line() {
            posEndXDiff = 0;
            posEndXOffset = 0;
            posEndXRealDiff = 0;
            posEndXRealOffset = 0;
        }

        void reset() {
            posEndX = posEndX + posEndXOffset;
            posEndXDiff = 0;
            posEndXOffset = 0;
            posEndXReal = posEndXReal + posEndXRealOffset;
            posEndXRealDiff = 0;
            posEndXRealOffset = 0;
        }
    }

    public interface OnWidthChangeListener {
        void widthChanged(float posX);
    }
}
