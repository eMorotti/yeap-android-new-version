package it.mozart.yeap.ui.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.os.Build;
import android.os.Handler;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.util.AttributeSet;
import android.view.View;

import java.util.List;

public class LoadingView extends View {

    private Paint paint;
    private Runnable runnable;
    private ViewPropertyAnimatorCompat anim;

    private int height;
    private float slice;
    private float increment;

    private boolean start = false;

    private int[] mColors = new int[]{Color.parseColor("#00FFFFFF"), Color.parseColor("#35FFFFFF"), Color.parseColor("#00FFFFFF")/*, Color.parseColor("#30FFFFFF")*/};

    public LoadingView(Context context) {
        super(context);
    }

    public LoadingView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LoadingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public LoadingView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        init();
    }

    private void init() {
        int width = getMeasuredWidth();
        height = getMeasuredHeight();
        slice = (float) width / 50f + 0.5f;
        increment = 0;
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(slice);
        paint.setShader(new LinearGradient(0f, 0f, 0f, (float) height, mColors, null, Shader.TileMode.CLAMP));

        runnable = new Runnable() {
            @Override
            public void run() {
                if (!start)
                    return;
                increment = (increment + slice / 24f + 0.5f) % (slice * 4f);
                invalidate();
                postDelayed(this, 16);
            }
        };
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        for (int i = 0; i < 35; i++) {
            canvas.drawLine(i + i * slice * 2f - increment, -50, i + i * 2f * slice - height / 2f - increment, height, paint);
        }
    }

    public void setColors(List<Integer> colors) {
        if (colors != null && colors.size() != 0) {
            mColors = new int[colors.size()];
            for (int i = 0; i < colors.size(); i++) {
                mColors[i] = colors.get(i);
            }
            requestLayout();
        }
    }

    public void startLoading() {
        if (!start) {
            start = true;
            init();
            new Handler().post(runnable);
        }
    }

    public void stopLoading() {
        if (start) {
            start = false;
        }
    }

    public boolean isRunning() {
        return start;
    }
}
