package it.mozart.yeap.ui.main.events;

import it.mozart.yeap.realm.User;

public class MainPartecipantiUserRemovedEvent {

    private User utente;

    public MainPartecipantiUserRemovedEvent(User utente) {
        this.utente = utente;
    }

    public User getUtente() {
        return utente;
    }
}
