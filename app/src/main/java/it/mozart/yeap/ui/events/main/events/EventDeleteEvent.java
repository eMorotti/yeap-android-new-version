package it.mozart.yeap.ui.events.main.events;

public class EventDeleteEvent {

    private boolean success;

    public EventDeleteEvent(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }
}
