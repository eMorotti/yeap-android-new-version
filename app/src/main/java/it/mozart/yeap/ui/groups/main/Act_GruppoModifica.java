package it.mozart.yeap.ui.groups.main;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.UUID;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.api.models.APIError;
import it.mozart.yeap.api.models.GruppoApiModel;
import it.mozart.yeap.api.models.ImageApiModel;
import it.mozart.yeap.api.services.GroupApi;
import it.mozart.yeap.api.services.UploadApi;
import it.mozart.yeap.events.ApiCallResultEvent;
import it.mozart.yeap.helpers.ImageInputHelper;
import it.mozart.yeap.realm.Group;
import it.mozart.yeap.realm.dao.GroupDao;
import it.mozart.yeap.ui.base.BaseActivityWithSyncService;
import it.mozart.yeap.ui.support.Act_ImageCrop;
import it.mozart.yeap.ui.views.AnimCheckBox;
import it.mozart.yeap.utility.APIErrorUtils;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.Utils;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class Act_GruppoModifica extends BaseActivityWithSyncService {

    @BindView(R.id.avatar)
    ImageView mAvatar;
    @BindView(R.id.modifica_nome)
    EditText mModificaNome;
    @BindView(R.id.perm_inv_check)
    AnimCheckBox mModificaPermesso;

    @OnClick(R.id.layout)
    void layoutClicked() {
        Utils.hideKeyboard(this);
    }

    @OnClick(R.id.avatar)
    void avatarClicked() {
        avatarClickedMethod(false);
    }

    @OnClick({R.id.perm_inv_check, R.id.modifica_perm_inv})
    void permInvClicked() {
        mModificaPermesso.setChecked(!mModificaPermesso.isChecked());
    }

    // SUPPORT

    @Inject
    Realm realm;
    @Inject
    GroupApi groupApi;
    @Inject
    UploadApi uploadApi;

    private Group mGruppo;
    private String mId;
    private File mAvatarFile;
    private boolean mTitleChanged;

    private static final String PARAM_ID = "paramId";

    private static final int IMG_CODE = 101;
    private static final int IMG_CROP_CODE = 102;

    // SYSTEM

    public static Intent newIntent(String gruppoId) {
        Intent intent = new Intent(App.getContext(), Act_GruppoModifica.class);
        intent.putExtra(PARAM_ID, gruppoId);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUseBus(true);

        setToolbarSubtitle(getString(R.string.dettaglio_gruppo_sottotitolo_modifica));
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_gruppo_modifica;
    }

    @Override
    protected void initVariables() {

    }

    @Override
    protected void loadParameters(Bundle extras) {
        mId = extras.getString(PARAM_ID);
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem salva = menu.findItem(R.id.menu_salva);
        if (salva != null) {
            salva.setEnabled(mTitleChanged || mAvatarFile != null || mGruppo.isUsersCanInvite() != mModificaPermesso.isChecked());
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_gruppo_modifica, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_salva:
                getDynamicView().showLoading();

                setRequestId(UUID.randomUUID().toString());
                GruppoApiModel model = new GruppoApiModel();
                model.setTitle(mModificaNome.getText().toString());
                model.setUsersCanInvite(mModificaPermesso.isChecked());
                if (mAvatarFile == null) {
                    model.setAvatarLargeUrl(mGruppo.getAvatarLargeUrl());
                    model.setAvatarThumbUrl(mGruppo.getAvatarThumbUrl());
                    updateGroup(model);
                } else {
                    uploadAvatar(model);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void initialize() {
        // Init gruppo
        mGruppo = new GroupDao(realm).getGroup(mId);
        if (mGruppo == null) {
            Toast.makeText(this, getString(R.string.general_error_gruppo_non_presente), Toast.LENGTH_LONG).show();
            finish();
        } else {
            updateLayout(mGruppo);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.IMAGE_PERMISSION_CODE:
                avatarClickedMethod(true);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case IMG_CODE:
                    // Chiamo l'activity per il crop dell'immagine selezionata
                    Uri copertinaUri = ImageInputHelper.getPickImageResultUri(this, data);
                    startActivityForResult(Act_ImageCrop.newIntent(copertinaUri, 1, 1), IMG_CROP_CODE);
                    break;
                case IMG_CROP_CODE:
                    String image = data.getStringExtra(Constants.IMAGE_CROP);
                    if (image != null) {
                        mAvatarFile = new File(image);
                        Glide.with(this).load(mAvatarFile).asBitmap().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).centerCrop().into(new BitmapImageViewTarget(mAvatar) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                mAvatar.setImageDrawable(circularBitmapDrawable);
                            }
                        });
                        invalidateOptionsMenu();
                    } else {
                        // Mostro messaggio di errore se non è stato restituito niente dall'activity di crop
                        Toast.makeText(this, getString(R.string.general_error_ritaglio_immagine), Toast.LENGTH_LONG).show();
                    }
                    break;
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ApiCallResultEvent event) {
        if (event.getRequestId().equals(getRequestId())) {
            Toast.makeText(this, getString(R.string.main_gruppo_modificato), Toast.LENGTH_SHORT).show();
            cancelTimer();
            finish();
        }
    }

    // FUNCTIONS

    /**
     * Aggiorno layout
     *
     * @param gruppo gruppo
     */
    private void updateLayout(final Group gruppo) {
        if (gruppo != null) {
            // Toolbar
            setToolbarTitle(gruppo.getTitle());

            // Info
            mModificaNome.setText(gruppo.getTitle());
            mModificaNome.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    boolean changed = !mGruppo.getTitle().equals(s.toString());
                    if (mTitleChanged != changed) {
                        if (changed) {
                            mModificaNome.setBackgroundResource(R.drawable.background_transparent_white_changed);
                        } else {
                            mModificaNome.setBackgroundResource(R.drawable.background_transparent_white);
                        }
                        mTitleChanged = changed;
                        invalidateOptionsMenu();
                    }
                }
            });

            // Permessi
            mModificaPermesso.setChecked(gruppo.isUsersCanInvite(), false);
            mModificaPermesso.setOnCheckedChangeListener(new AnimCheckBox.OnCheckedChangeListener() {
                @Override
                public void onChange(boolean checked) {
                    invalidateOptionsMenu();
                }
            });

            // Avatar
            Glide.with(this).load(gruppo.getAvatarThumbUrl()).asBitmap().placeholder(R.drawable.avatar_gruppo).centerCrop().into(new BitmapImageViewTarget(mAvatar) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    mAvatar.setImageDrawable(circularBitmapDrawable);
                }
            });
        }
    }

    /**
     * Eseguo l'upload dell'avatar e il seguente update del gruppo
     *
     * @param model modello per le api
     */
    private void uploadAvatar(final GruppoApiModel model) {
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), mAvatarFile);
        final MultipartBody.Part image = MultipartBody.Part.createFormData("image", mAvatarFile.getName(), requestFile);
        registerSubscription(uploadApi.uploadAvatar(image)
                .subscribeOn(Schedulers.newThread())
                .flatMap(new Func1<ImageApiModel, Observable<Group>>() {
                    @Override
                    public Observable<Group> call(ImageApiModel imageApiModel) {
                        setRequestId(UUID.randomUUID().toString());
                        model.setAvatar(imageApiModel.getUploadref());
                        return groupApi.update(getRequestId(), mId, model);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Group>() {
                    @Override
                    public void call(Group gruppo) {
                        startTimer();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        getDynamicView().hide();

                        APIError error = APIErrorUtils.parseError(Act_GruppoModifica.this, throwable);
                        Toast.makeText(Act_GruppoModifica.this, error.message(), Toast.LENGTH_LONG).show();
                    }
                }));
    }

    /**
     * Eseguo l'update del gruppo
     *
     * @param model modello per le api
     */
    private void updateGroup(GruppoApiModel model) {
        registerSubscription(groupApi.update(getRequestId(), mId, model)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Group>() {
                    @Override
                    public void call(Group gruppo) {
                        startTimer();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        getDynamicView().hide();

                        APIError error = APIErrorUtils.parseError(Act_GruppoModifica.this, throwable);
                        Toast.makeText(Act_GruppoModifica.this, error.message(), Toast.LENGTH_SHORT).show();
                    }
                }));
    }

    private void avatarClickedMethod(boolean ignoreCheck) {
        ImageInputHelper.openChooserForAvatarSelect(this, IMG_CODE, ignoreCheck);
    }
}
