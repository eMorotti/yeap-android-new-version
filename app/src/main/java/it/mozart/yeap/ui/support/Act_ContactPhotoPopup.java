package it.mozart.yeap.ui.support;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.ui.base.BaseActivityWithSyncService;
import it.mozart.yeap.ui.profile.Act_Profilo;
import it.mozart.yeap.utility.Utils;

public class Act_ContactPhotoPopup extends BaseActivityWithSyncService {

    @BindView(R.id.content)
    View mContent;
    @BindView(R.id.utente)
    TextView mUtente;
    @BindView(R.id.avatar)
    ImageView mAvatar;
    @BindView(R.id.bg)
    View mBg;

    @OnClick(R.id.bg)
    void bgClicked() {
        onBackPressed();
    }

    @OnClick(R.id.profilo_button)
    void profiloButtonClicked() {
        startActivity(Act_Profilo.newIntent(mUtenteId));
    }

    // SUPPORT

    @Inject
    Realm realm;

    private String mUtenteId;
    private boolean mAnimationInProgress;
    private int mPosX, mPosY, mRadius;
    private float mInitialScale, mInitialTransX, mInitialTransY;

    private static final String PARAM_ID = "paramId";
    private static final String PARAM_POS_X = "paramPosX";
    private static final String PARAM_POS_Y = "paramPosY";
    private static final String PARAM_RADIUS = "paramRadius";

    private static final int ANIM_REVEAL_IN_DURATION = 384;
    private static final int ANIM_REVEAL_OUT_DURATION = 250;
    private static final float ANIM_OFFSET = 110;

    // SYSTEM

    public static Intent newIntent(String utenteId, int posX, int posY, int radius) {
        Intent intent = new Intent(App.getContext(), Act_ContactPhotoPopup.class);
        intent.putExtra(PARAM_ID, utenteId);
        intent.putExtra(PARAM_POS_X, posX);
        intent.putExtra(PARAM_POS_Y, posY);
        intent.putExtra(PARAM_RADIUS, radius);
        return intent;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_contact_photo_popup;
    }

    @Override
    protected void initVariables() {
        mAnimationInProgress = false;
    }

    @Override
    protected void loadParameters(Bundle extras) {
        mUtenteId = extras.getString(PARAM_ID);
        mPosX = extras.getInt(PARAM_POS_X);
        mPosY = extras.getInt(PARAM_POS_Y);
        mRadius = extras.getInt(PARAM_RADIUS);
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    public void onBackPressed() {
        if (Utils.isLollipop()) {
            hideAnimation();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void initialize() {
        final User utente = new UserDao(realm).getUser(mUtenteId);
        if (utente == null) {
            Toast.makeText(this, getString(R.string.general_error_utente_non_presente), Toast.LENGTH_LONG).show();
            finish();
        } else {
            mUtente.setText(utente.getChatName());

            Glide.with(this).load(utente.getAvatarThumb()).asBitmap().centerCrop().into(new BitmapImageViewTarget(mAvatar) {
                @Override
                protected void setResource(Bitmap resource) {
                    mAvatar.setImageBitmap(resource);
                }
            });
            Glide.with(this).load(utente.getAvatarLarge()).asBitmap().placeholder(R.drawable.avatar_utente_square).into(new BitmapImageViewTarget(mAvatar) {
                @Override
                protected void setResource(Bitmap resource) {
                    mAvatar.setImageBitmap(resource);
                }
            });
            mAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showImage(utente.getAvatarLarge());
                }
            });

            if (Utils.isLollipop()) {
                mContent.post(new Runnable() {
                    @Override
                    public void run() {
                        showAnimation();
                    }
                });
            }
        }
    }

    // FUNCTIONS

    /**
     * Apro il dettaglio immagine
     *
     * @param url url
     */
    private void showImage(String url) {
        Intent intent = Act_ImageDetail.newIntent(url, R.drawable.avatar_utente_square);
        if (Utils.isLollipop()) {
            Pair<View, String> p1 = Pair.create((View) mAvatar, getString(R.string.transition_image));
            //noinspection unchecked
            ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(this, p1);
            startActivity(intent, optionsCompat.toBundle());
        } else {
            startActivity(intent);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void showAnimation() {
        if (mAnimationInProgress) {
            return;
        }

        mAnimationInProgress = true;

        mInitialScale = ((float) mRadius) / ((float) mContent.getWidth() / 2);
        mContent.setScaleX(mInitialScale);
        mContent.setScaleY(mInitialScale);
        mContent.animate().scaleX(1).scaleY(1).setDuration(ANIM_REVEAL_IN_DURATION).start();

        int[] location = new int[2];
        mContent.getLocationInWindow(location);
        mInitialTransX = mPosX - location[0];
        mInitialTransY = mPosY - location[1];

        ValueAnimator animatorTranslate = ValueAnimator.ofFloat(0, 1); // values from 0 to 1
        animatorTranslate.setDuration(ANIM_REVEAL_IN_DURATION);
        animatorTranslate.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            float[] point = new float[2];

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // Gets the animated float fraction
                float val = animation.getAnimatedFraction();

                float tx = mInitialTransX + val * (-mInitialTransX);
                float ty = mInitialTransY + val * (-mInitialTransY);
                float yoffset = (float) (Math.sin(val * Math.PI) * ANIM_OFFSET);

                ty += yoffset;

                mContent.setTranslationX(tx);
                mContent.setTranslationY(ty);
            }
        });
        animatorTranslate.start();

        final float finalRadius = (float) Math.hypot(mContent.getWidth(), mContent.getHeight()) / 2f;
        Animator animator = ViewAnimationUtils.createCircularReveal(mContent, mContent.getWidth() / 2, mContent.getHeight() / 2, mRadius / mInitialScale, finalRadius);
        animator.setDuration(ANIM_REVEAL_IN_DURATION);
        animator.start();
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mAnimationInProgress = false;
            }
        });

        mBg.setAlpha(0f);
        mBg.animate().alpha(1f).setDuration(ANIM_REVEAL_IN_DURATION).start();

        mContent.setAlpha(0f);
        mContent.animate().alpha(1).setDuration(ANIM_REVEAL_IN_DURATION).start();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void hideAnimation() {
        if (mAnimationInProgress) {
            return;
        }

        mAnimationInProgress = true;

        mContent.animate().scaleX(mInitialScale).scaleY(mInitialScale).setDuration(ANIM_REVEAL_OUT_DURATION).start();

        ValueAnimator animatorTranslate = ValueAnimator.ofFloat(0, 1); // values from 0 to 1
        animatorTranslate.setDuration(ANIM_REVEAL_OUT_DURATION);
        animatorTranslate.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            float[] point = new float[2];

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // Gets the animated float fraction
                float val = animation.getAnimatedFraction();

                float tx = val * (mInitialTransX);
                float ty = val * (mInitialTransY);
                float yoffset = (float) (Math.sin(Math.PI * val) * ANIM_OFFSET);

                ty += yoffset;

                mContent.setTranslationX(tx);
                mContent.setTranslationY(ty);
            }
        });
        animatorTranslate.start();

        float finalRadius = (float) Math.hypot(mContent.getWidth(), mContent.getHeight()) / 2f;
        final float initialRadius = mRadius / mInitialScale;
        Animator animator = ViewAnimationUtils.createCircularReveal(mContent, mContent.getWidth() / 2, mContent.getHeight() / 2, finalRadius, initialRadius);
        animator.setDuration(ANIM_REVEAL_OUT_DURATION);
        animator.start();
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mAnimationInProgress = false;

                finish();
                overridePendingTransition(0, 0);
            }
        });

        mBg.animate().alpha(0f).setDuration(ANIM_REVEAL_OUT_DURATION).start();
        mContent.animate().alpha(0f).setDuration(ANIM_REVEAL_OUT_DURATION).start();
    }
}
