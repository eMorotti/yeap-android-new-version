package it.mozart.yeap.ui.home.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import org.greenrobot.eventbus.EventBus;

import java.util.Locale;

import it.mozart.yeap.R;
import it.mozart.yeap.events.ImageSelectedEvent;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.Vote;
import it.mozart.yeap.ui.events.main.events.VoteEventClickedEvent;
import it.mozart.yeap.ui.events.main.events.VoteInterestEventClickedEvent;
import it.mozart.yeap.ui.views.AnimatedTextView;
import it.mozart.yeap.ui.views.LoadingView;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.DateUtils;
import it.mozart.yeap.utility.EasySpan;

public class EventLayout extends FrameLayout {

    private ImageView mAvatar;
    private TextView mUser;
    private TextView mStatus;
    private TextView mTitle;
    private View mMessageDivider;
    private View mMessageLayout;
    private TextView mMessageUser;
    private TextView mMessageTime;
    private TextView mMessage;
    private View mDoveLayout;
    private TextView mDove;
    private View mQuandoLayout;
    private TextView mQuando;
    private View mMioVotoLayout;
    private ImageView mMioVotoIcon;
    private TextView mMioVoto;
    private ImageView mNumYesIcon;
    private TextView mNumYes;
    private View mVotoLayout;
    private View mPartecipoButton;
    private View mNonPartecipoButton;
    private LoadingView mLoadingView;
    private View mVotoInteresseLayout;
    private TextView mVotoInteresseTesto;
    private AnimatedTextView mNumNewMessages;
    private TextView mInteressaButton;
    private TextView mNonInteressaButton;
    private LoadingView mInteresseLoadingView;

    public EventLayout(Context context) {
        super(context);
        init();
    }

    public EventLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EventLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @SuppressWarnings("unused")
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public EventLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.item_event, this);
        mAvatar = findViewById(R.id.avatar);
        mUser = findViewById(R.id.user);
        mStatus = findViewById(R.id.status);
        mTitle = findViewById(R.id.title);
        mMessageDivider = findViewById(R.id.divider);
        mMessageLayout = findViewById(R.id.message_layout);
        mMessageUser = findViewById(R.id.message_user);
        mMessageTime = findViewById(R.id.message_time);
        mMessage = findViewById(R.id.message);
        mDoveLayout = findViewById(R.id.dove_layout);
        mDove = findViewById(R.id.dove);
        mQuandoLayout = findViewById(R.id.quando_layout);
        mQuando = findViewById(R.id.quando);
        mMioVotoLayout = findViewById(R.id.mio_voto_layout);
        mMioVotoIcon = findViewById(R.id.mio_voto_icon);
        mMioVoto = findViewById(R.id.mio_voto);

        mNumYesIcon = findViewById(R.id.num_yes_icon);
        mNumYes = findViewById(R.id.num_yes);

        mVotoLayout = findViewById(R.id.voto_layout);
        mPartecipoButton = findViewById(R.id.partecipo_button);
        mNonPartecipoButton = findViewById(R.id.non_partecipo_button);
        mLoadingView = findViewById(R.id.loading_view);
        mLoadingView.startLoading();

        mVotoInteresseLayout = findViewById(R.id.voto_interesse_layout);
        mVotoInteresseTesto = findViewById(R.id.voto_interesse_testo);
        mInteressaButton = findViewById(R.id.interessa_button);
        mNonInteressaButton = findViewById(R.id.non_interessa_button);
        mInteresseLoadingView = findViewById(R.id.loading_view_interest);
        mInteresseLoadingView.startLoading();

        mNumNewMessages = findViewById(R.id.new_messages);
        mNumNewMessages.setTextSize(8);
        mNumNewMessages.setTextColor(Color.WHITE);
    }

    /**
     * Esegui il bind tra i dati dell'evento e le view
     *
     * @param event evento
     */
    public void bind(final Event event) {
        if (event != null) {
            // Info
            if (event.getCreatorUuid().equals(AccountProvider.getInstance().getAccountId())) {
                mUser.setText(getContext().getString(R.string.evento_proponi_me));
            } else {
                mUser.setText(String.format(getContext().getString(R.string.evento_proponi), event.getCreator().getChatName()));
            }
            mTitle.setText(event.getEventInfo().getTitle());
            if (event.getEventInfo().getWhere() != null && !event.isWherePollEnabled()) {
                mDove.setText(event.getEventInfo().getWhere());
            } else {
                mDove.setText(null);
            }
            String quando = DateUtils.createQuandoText(event.getEventInfo().generateQuandoModel());
            if (quando != null && !event.isWhenPollEnabled()) {
                mQuando.setText(quando);
            } else {
                mQuando.setText(null);
            }

            // Avatar
            Glide.with(getContext()).load(event.getAvatarThumbUrl()).asBitmap().centerCrop().into(new BitmapImageViewTarget(mAvatar) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    mAvatar.setImageDrawable(circularBitmapDrawable);
                }

                @Override
                public void onLoadFailed(Exception e, Drawable errorDrawable) {
                    Glide.with(getContext()).load(R.drawable.avatar_attivita).asBitmap().centerCrop().into(new BitmapImageViewTarget(mAvatar) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            mAvatar.setImageDrawable(circularBitmapDrawable);
                        }
                    });
                }
            });
            mAvatar.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    EventBus.getDefault().post(new ImageSelectedEvent(event.getAvatarLargeUrl(), mAvatar, true, true));
                }
            });

            // Nuovi messaggi
            long numUpdates = event.getNumNewMessages() + event.getNumPollUpdates() + event.getNumInfoUpdates() + event.getNumUserUpdates();
            if (numUpdates != 0) {
                mNumNewMessages.setVisibility(VISIBLE);
                mNumNewMessages.setValue(numUpdates);
            } else {
                mNumNewMessages.setVisibility(GONE);
            }

            boolean showMessaggi = manageStatus(event, numUpdates);
            // Stato
            if (!event.isMyselfPresent()) {
                setAlpha(0.9f);
                mVotoLayout.setVisibility(GONE);
                mMioVotoLayout.setVisibility(GONE);
                mVotoInteresseLayout.setVisibility(GONE);
                mQuandoLayout.setVisibility(GONE);
                mDoveLayout.setVisibility(GONE);
                mStatus.setText(getContext().getString(R.string.evento_hai_abbandonato));
                mStatus.setBackgroundResource(R.drawable.status_annullata);
            } else {
                setAlpha(1f);
            }

            if (showMessaggi) {
                // Ultimo messaggio
                manageLastMessage(event);
            }
        }
    }

    /**
     * Gestisco la visulalizzazione dell'ultimo messaggio presente nella chat dell'evento
     *
     * @param event evento
     */
    private void manageLastMessage(Event event) {
        mMessageDivider.setVisibility(VISIBLE);
        mMessageLayout.setVisibility(VISIBLE);

        String who;
        if (event.getMessage() != null && event.getMessage().isValid() && event.getMessage().getType() == Constants.MESSAGGIO_TYPE_SYSTEM) {
            if (event.getMessage().getCreator().getUuid().equals(AccountProvider.getInstance().getAccountId())) {
                who = getContext().getString(R.string.general_tu);
            } else {
                who = event.getMessage().getCreator().getChatName();
            }

            int img = 0;
            switch (event.getMessage().getSystemType()) {
                case Constants.SYSTEM_MESSAGE_CREAZIONE_SONDAGGIO:
                case Constants.SYSTEM_MESSAGE_CREAZIONE_SONDAGGIO_SPECIAL:
                    img = R.drawable.nav_ico_sondaggi_graph;
                    break;
                case Constants.SYSTEM_MESSAGE_CREAZIONE_VOCE_SONDAGGIO:
                case Constants.SYSTEM_MESSAGE_CREAZIONE_VOCE_SONDAGGIO_SPECIAL:
                    img = R.drawable.ico_check_azure;
                    break;
                case Constants.SYSTEM_MESSAGE_CREAZIONE_PROPOSTA:
                    img = R.drawable.ico_megafono_azure;
                    break;
            }
            if (img != 0) {
                Drawable drawable1 = DrawableCompat.wrap(ContextCompat.getDrawable(getContext(), img));
                DrawableCompat.setTint(drawable1, Color.BLACK);
                drawable1.setBounds(0, 0, mMessage.getLineHeight() / 3 * 2, mMessage.getLineHeight() / 3 * 2);
                ImageSpan imageSpan1 = new ImageSpan(drawable1, ImageSpan.ALIGN_BASELINE);

                EasySpan easySpan = new EasySpan.Builder()
                        .appendSpans("---", imageSpan1)
                        .appendText("  ")
                        .build();

                Spanned spanned = (Spanned) TextUtils.concat(easySpan.getSpannedText(), event.getMessage().getSystemMessageWhat());
                mMessage.setText(spanned);
            } else {
                mMessage.setText(String.format("%s", event.getMessage().getFormattedMessageWithoutCreator(getContext())));
            }
            mMessageUser.setText(who);
            mMessageTime.setText(DateUtils.smartFormatDateForLastMessage(getContext(), event.getMessage().getCreatedAt(), "dd MMM"));
        } else if (event.getMessage() != null && event.getMessage().isValid() && event.getMessage().getCreator() != null) {
            // Ultimo messaggio utente
            if (event.getMessage().getCreator().getUuid() != null && event.getMessage().getCreator().getUuid().equals(AccountProvider.getInstance().getAccountId())) {
                who = getContext().getString(R.string.general_tu);
            } else {
                who = event.getMessage().getCreator().getChatName();
            }

            // Ultimo messaggio
            if (event.getMessage().getMediaUrl() != null || event.getMessage().getMediaLocalUrl() != null) {
                Drawable drawable1 = DrawableCompat.wrap(ContextCompat.getDrawable(getContext(), R.drawable.ic_photo_white_24dp));
                DrawableCompat.setTint(drawable1, ContextCompat.getColor(getContext(), R.color.textWhiteSecondary));
                drawable1.setBounds(0, 0, mMessage.getLineHeight(), mMessage.getLineHeight());
                ImageSpan imageSpan1 = new ImageSpan(drawable1, ImageSpan.ALIGN_BOTTOM);

                EasySpan easySpan = new EasySpan.Builder()
                        .appendSpans("---", imageSpan1)
                        .appendText(" ")
                        .build();

                Spanned spanned = easySpan.getSpannedText();
                if (event.getMessage().getFormattedMessageWithoutCreator(getContext()).length() == 0) {
                    spanned = (Spanned) TextUtils.concat(spanned, getContext().getString(R.string.general_foto));
                } else {
                    spanned = (Spanned) TextUtils.concat(spanned, event.getMessage().getFormattedMessage(getContext()));
                }

                mMessage.setText(spanned);
            } else {
                mMessage.setText(String.format("%s", event.getMessage().getFormattedMessageWithoutCreator(getContext())));
            }
            mMessageUser.setText(who);
            mMessageTime.setText(DateUtils.smartFormatDateForLastMessage(getContext(), event.getMessage().getCreatedAt(), "dd MMM"));
        } else {
            mMessage.setText(getContext().getString(R.string.general_error_last_message_not_found));
            mMessageUser.setText(null);
            mMessageTime.setText(null);
        }
    }

    /**
     * Modifiche layout a seconda dello stato dell'attività
     *
     * @param event evento
     */
    private boolean manageStatus(final Event event, long numUpdates) {
        switch (event.getStatus()) {
            case Constants.EVENTO_IN_PIANIFICAZIONE:
                return statusInPianificazione(event, numUpdates);
            case Constants.EVENTO_CONFERMATO:
                return statusConfermata(event, numUpdates);
            case Constants.EVENTO_ANNULLATO:
                return statusAnnullata(numUpdates);
        }

        return false;
    }

    private boolean statusAnnullata(long numUpdates) {
        mAvatar.setAlpha(0.5f);
        mTitle.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        mMioVotoLayout.setVisibility(GONE);
        mVotoLayout.setVisibility(GONE);
        mVotoInteresseLayout.setVisibility(GONE);
        mMessageDivider.setVisibility(GONE);
        mMessageLayout.setVisibility(GONE);

        mStatus.setText(getContext().getString(R.string.evento_status_annullata));
        mStatus.setBackgroundResource(R.drawable.status_annullata);

        mQuandoLayout.setVisibility(GONE);
        mDoveLayout.setVisibility(GONE);

        mNumYesIcon.setVisibility(GONE);
        mNumYes.setText(null);

        return numUpdates != 0;
    }

    private boolean statusConfermata(final Event event, long numUpdates) {
        boolean showMessaggi = false;

        mAvatar.setAlpha(1f);
        mTitle.setPaintFlags(0);
        mVotoLayout.setVisibility(GONE);
        mVotoInteresseLayout.setVisibility(GONE);
        mMessageDivider.setVisibility(GONE);
        mMessageLayout.setVisibility(GONE);

        mQuandoLayout.setVisibility(VISIBLE);
        mDoveLayout.setVisibility(VISIBLE);

        mStatus.setText(getContext().getString(R.string.evento_status_confermata));
        mStatus.setBackgroundResource(R.drawable.status_confermata);

        if (event.getEventInfo().getMyVote() == null || event.getEventInfo().getMyVote().isLoading()) {
            mMioVotoLayout.setVisibility(GONE);
            if (event.getEventInfo().getMyVote() == null) {
                mLoadingView.setVisibility(INVISIBLE);
            } else {
                mLoadingView.setVisibility(VISIBLE);
            }
            mVotoLayout.setVisibility(VISIBLE);
        } else {
            mMioVotoLayout.setVisibility(VISIBLE);
            if (event.getEventInfo().getMyVote().getType() == Constants.VOTO_SI) {
                mMioVotoIcon.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorVotoSi));
                mMioVoto.setTextColor(ContextCompat.getColor(getContext(), R.color.colorVotoSi));
                mMioVoto.setText(getContext().getString(R.string.evento_partecipo));
            } else {
                mMioVotoIcon.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorVotoNo));
                mMioVoto.setTextColor(ContextCompat.getColor(getContext(), R.color.colorVotoNo));
                mMioVoto.setText(getContext().getString(R.string.evento_non_partecipo));
            }

            if (numUpdates != 0) {
                showMessaggi = true;
            }
        }

        EasySpan easySpan = new EasySpan.Builder()
                .appendSpans(String.valueOf(event.getNumVotesYes()), new StyleSpan(Typeface.BOLD), new RelativeSizeSpan(1.2f))
                .appendSpans(String.format(Locale.getDefault(), "/%d", event.getGuests().size()), new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorGrayPrimary)))
                .build();

        mNumYes.setText(easySpan.getSpannedText(), TextView.BufferType.SPANNABLE);
        mNumYesIcon.setVisibility(VISIBLE);

        mPartecipoButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Vote voto = event.getEventInfo().getMyVote();
                if (voto != null && voto.isLoading()) {
                    return;
                }

                if (!event.isMyselfPresent() || event.getStatus() == Constants.EVENTO_ANNULLATO) {
                    return;
                }

                if (voto == null || voto.getType() != Constants.VOTO_SI) {
                    mLoadingView.setVisibility(VISIBLE);
                    EventBus.getDefault().post(new VoteEventClickedEvent(event.getUuid(), Constants.VOTO_SI));
                }
            }
        });

        mNonPartecipoButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Vote voto = event.getEventInfo().getMyVote();
                if (voto != null && voto.isLoading()) {
                    return;
                }

                if (!event.isMyselfPresent() || event.getStatus() == Constants.EVENTO_ANNULLATO) {
                    return;
                }

                if (voto == null || voto.getType() != Constants.VOTO_NO) {
                    mLoadingView.setVisibility(VISIBLE);
                    EventBus.getDefault().post(new VoteEventClickedEvent(event.getUuid(), Constants.VOTO_NO));
                }
            }
        });

        return showMessaggi;
    }

    private boolean statusInPianificazione(final Event event, long numUpdates) {
        boolean showMessaggi = false;

        mAvatar.setAlpha(1f);
        mTitle.setPaintFlags(0);
        mMioVotoLayout.setVisibility(GONE);
        mVotoLayout.setVisibility(GONE);
        mVotoInteresseLayout.setVisibility(GONE);
        mMessageDivider.setVisibility(GONE);
        mMessageLayout.setVisibility(GONE);

        mQuandoLayout.setVisibility(VISIBLE);
        mDoveLayout.setVisibility(VISIBLE);

        mStatus.setText(getContext().getString(R.string.evento_status_in_pianificatione));
        mStatus.setBackgroundResource(R.drawable.status_in_pianificazione);

        if (event.getEventInfo().getMyVoteInterest() == null || event.getEventInfo().getMyVoteInterest().isLoading()) {
            if (event.getEventInfo().getMyVoteInterest() == null) {
                mInteresseLoadingView.setVisibility(INVISIBLE);
            } else {
                mInteresseLoadingView.setVisibility(VISIBLE);
            }
            mVotoInteresseLayout.setVisibility(VISIBLE);

            EasySpan easySpan = new EasySpan.Builder()
                    .appendSpans(event.getCreator().getChatName(), new StyleSpan(Typeface.BOLD))
                    .appendText(" ")
                    .appendText(getContext().getString(R.string.evento_interesse_testo))
                    .build();
            mVotoInteresseTesto.setText(easySpan.getSpannedText());
        } else {
            if (numUpdates != 0) {
                showMessaggi = true;
            }
        }

        EasySpan easySpan = new EasySpan.Builder()
                .appendSpans(String.valueOf(event.getNumInterestYes()), new StyleSpan(Typeface.BOLD), new RelativeSizeSpan(1.2f))
                .appendText(String.format(Locale.getDefault(), " %s", getContext().getString(R.string.evento_interessati_text).toUpperCase()))
                .build();

        mNumYes.setText(easySpan.getSpannedText(), TextView.BufferType.SPANNABLE);
        mNumYesIcon.setVisibility(GONE);

        mInteressaButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Vote voto = event.getEventInfo().getMyVoteInterest();
                if (voto != null && voto.isLoading()) {
                    return;
                }

                if (!event.isMyselfPresent() || event.getStatus() == Constants.EVENTO_ANNULLATO) {
                    return;
                }

                if (voto == null || voto.getType() != Constants.VOTO_INTERESSE) {
                    mInteresseLoadingView.setVisibility(VISIBLE);
                    EventBus.getDefault().post(new VoteInterestEventClickedEvent(EventLayout.this, event.getTitle(), event.getUuid(), Constants.VOTO_INTERESSE));
                }
            }
        });

        mNonInteressaButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Vote voto = event.getEventInfo().getMyVoteInterest();
                if (voto != null && voto.isLoading()) {
                    return;
                }

                if (!event.isMyselfPresent() || event.getStatus() == Constants.EVENTO_ANNULLATO) {
                    return;
                }

                if (voto == null || voto.getType() != Constants.VOTO_NO_INTERESSE) {
                    EventBus.getDefault().post(new VoteInterestEventClickedEvent(EventLayout.this, event.getTitle(), event.getUuid(), Constants.VOTO_NO_INTERESSE));
                }
            }
        });

        return showMessaggi;
    }
}
