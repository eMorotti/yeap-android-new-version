package it.mozart.yeap.ui.groups.main;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import butterknife.BindView;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import it.mozart.yeap.R;
import it.mozart.yeap.api.models.APIError;
import it.mozart.yeap.api.services.GroupApi;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.helpers.AnalitycsProvider;
import it.mozart.yeap.realm.Group;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.dao.GroupDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.services.syncAdapter.contacts.ContactsManager;
import it.mozart.yeap.ui.base.BaseFragment;
import it.mozart.yeap.ui.groups.main.adapters.GruppoMainPartecipantiAdapter;
import it.mozart.yeap.ui.groups.main.events.GroupGuestSelectedEvent;
import it.mozart.yeap.ui.groups.main.events.GroupInvitedSelectedEvent;
import it.mozart.yeap.ui.support.events.UserSelectedEvent;
import it.mozart.yeap.utility.APIErrorUtils;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class Frg_GruppoMainInvitati extends BaseFragment {

    @BindView(R.id.recyclerview)
    RecyclerView mRecyclerView;

    // SUPPORT

    @Inject
    Realm realm;
    @Inject
    GroupApi groupApi;

    private UserDao mUtenteDao;
    private GruppoMainPartecipantiAdapter mAdapter;
    private Group mGruppo;
    private String mGruppoId;
    private List<List<String>> mContactInfo;
    private String mContactId;
    private String mContactIdInContacts;
    private String mContactPhone;

    private static final String PARAM_ID = "paramId";

    // SYSTEM

    public static Frg_GruppoMainInvitati newInstance(String gruppoId) {
        Bundle args = new Bundle();
        args.putString(PARAM_ID, gruppoId);
        Frg_GruppoMainInvitati fragment = new Frg_GruppoMainInvitati();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUseBus(true);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_main_gruppo_invitati;
    }

    @Override
    protected void initValues() {

    }

    @Override
    protected void loadParameters(Bundle arguments) {
        mGruppoId = arguments.getString(PARAM_ID);
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    public void onDestroyView() {
        // Fix problema leak listener realm
        mRecyclerView.setAdapter(null);
        if (mGruppo != null) {
            mGruppo.removeAllChangeListeners();
        }
        super.onDestroyView();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (mContactId != null) {
            User contatto = mUtenteDao.getUser(mContactId);
            if (contatto != null) {
                menu.clear();
                menu.add(Menu.NONE, 1, Menu.NONE, String.format(getString(R.string.main_visualizza_partecipante_dettaglio), contatto.getChatName()));
                MenuItem item = menu.add(Menu.NONE, 2, Menu.NONE, String.format(getString(R.string.main_rimuovi_partecipante), contatto.getChatName()));
                if (!AccountProvider.getInstance().getAccountId().equals(mGruppo.getAdminUuid()) || mGruppo.getAdminUuid().equals(mContactId) || !mGruppo.isMyselfPresent()) {
                    item.setEnabled(false);
                }
            }
        } else if (mContactPhone != null) {
            User contatto = mUtenteDao.getUserLocal(mContactPhone);
            if (contatto != null) {
                menu.clear();
                menu.setHeaderTitle(contatto.getChatName());
                menu.add(0, 1, Menu.NONE, getString(R.string.system_show_contact));
                menu.add(1, 0, Menu.NONE, getString(R.string.system_invite_by_sms_user));
                int i = 0;
                for (String email : mContactInfo.get(1)) {
                    menu.add(2, i, Menu.NONE, getString(R.string.system_invite_by_mail_user));
                    i++;
                }
                menu.add(3, 0, Menu.NONE, getString(R.string.main_rimuovi_invitato));
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (mContactId != null) {
            switch (item.getItemId()) {
                case 1:
                    EventBus.getDefault().post(new UserSelectedEvent(mContactId, null));
                    break;
                case 2:
                    AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "removeParticipant", "group");
                    User utente = mUtenteDao.getUser(mContactId);
                    new MaterialDialog.Builder(getContext())
                            .content(String.format(getString(R.string.main_gruppo_rimuovi_invitato_messaggio), utente.getChatName()))
                            .positiveText(getString(R.string.general_rimuovi))
                            .negativeText(getString(R.string.general_annulla))
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    getDynamicView().showLoading();
                                    setRequestId(UUID.randomUUID().toString());
                                    groupApi.removePartecipant(getRequestId(), mGruppoId, mContactId, null)
                                            .subscribeOn(Schedulers.newThread())
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribe(new Action1<Object>() {
                                                @Override
                                                public void call(Object obj) {
                                                    startTimer();
                                                }
                                            }, new Action1<Throwable>() {
                                                @Override
                                                public void call(Throwable throwable) {
                                                    getDynamicView().hide();

                                                    APIError error = APIErrorUtils.parseError(getActivity(), throwable);
                                                    Toast.makeText(getContext(), error.message(), Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                }
                            }).show();
                    break;
            }
        } else if (mContactPhone != null) {
            switch (item.getGroupId()) {
                case 0:
                    // Visualizzo in rubrica
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, mContactIdInContacts);
                    intent.setData(uri);
                    if (intent.resolveActivity(getContext().getPackageManager()) != null) {
                        startActivity(intent);
                    } else {
                        Toast.makeText(getContext(), getString(R.string.general_error_no_app_found), Toast.LENGTH_SHORT).show();
                    }
                    break;
                case 1:
                    // Invito nell'app tramite sms
                    User contatto = mUtenteDao.getUserLocal(mContactPhone);
                    if (contatto != null) {
                        mandaInvitoToNumber(contatto.getPhone());
                    }
                    break;
                case 2:
                    // Invito nell'app tramite mail
                    mandaInvitoToMail(mContactInfo.get(1).get(item.getOrder()));
                    break;
                case 3:
                    User utente = mUtenteDao.getUserLocal(mContactPhone);
                    new MaterialDialog.Builder(getContext())
                            .content(String.format(getString(R.string.main_gruppo_rimuovi_invitato_messaggio), utente.getChatName()))
                            .positiveText(getString(R.string.general_rimuovi))
                            .negativeText(getString(R.string.general_annulla))
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    getDynamicView().showLoading();
                                    setRequestId(UUID.randomUUID().toString());
                                    groupApi.removePartecipant(getRequestId(), mGruppoId, null, mContactPhone)
                                            .subscribeOn(Schedulers.newThread())
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribe(new Action1<Object>() {
                                                @Override
                                                public void call(Object obj) {
                                                    startTimer();
                                                }
                                            }, new Action1<Throwable>() {
                                                @Override
                                                public void call(Throwable throwable) {
                                                    getDynamicView().hide();

                                                    APIError error = APIErrorUtils.parseError(getActivity(), throwable);
                                                    Toast.makeText(getContext(), error.message(), Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                }
                            }).show();
                    break;
            }
        }
        return super.onContextItemSelected(item);
    }

    @Override
    protected void initialize() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isAdded()) {
                    return;
                }

                mUtenteDao = new UserDao(realm);

                // Gruppo
                mGruppo = new GroupDao(realm).getGroup(mGruppoId);

                // Lista Invitati
                mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                mAdapter = new GruppoMainPartecipantiAdapter(getContext(), mGruppo);
                mRecyclerView.setAdapter(mAdapter);

                // Listener
                mGruppo.addChangeListener(new RealmChangeListener<Group>() {
                    @Override
                    public void onChange(@NonNull Group element) {
                        if (element.isValid()) {
                            mAdapter.updateGruppo(element);
                        }
                    }
                });
            }
        }, 400);
    }

    @Subscribe
    public void onEvent(GroupGuestSelectedEvent event) {
        mContactPhone = null;
        mContactId = event.getId();
        if (!mContactId.equals(AccountProvider.getInstance().getAccountId())) {
            AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "tapParticipant", "group");
            registerForContextMenu(event.getView());
            getActivity().openContextMenu(event.getView());
            unregisterForContextMenu(event.getView());
        } else {
            AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "tapParticipantMe", "group");
        }
    }

    @Subscribe
    public void onEvent(GroupInvitedSelectedEvent event) {
        mContactId = null;
        mContactIdInContacts = event.getId();
        mContactPhone = event.getPhone();
        mContactInfo = new ContactsManager().getDetailInfoFromPhone(getContext(), mContactPhone);
        registerForContextMenu(event.getView());
        getActivity().openContextMenu(event.getView());
        unregisterForContextMenu(event.getView());
    }


    // FUNCTIONS

    /**
     * Faccio partire l'intent per mandare un messaggio
     *
     * @param phoneNumber telefono
     */
    private void mandaInvitoToNumber(String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + phoneNumber));
        intent.putExtra("sms_body", getString(R.string.system_invita_testo));
        if (intent.resolveActivity(getContext().getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Toast.makeText(getContext(), getString(R.string.general_error_no_app_found), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Faccio partire l'intent per mandare una mail
     *
     * @param email email
     */
    private void mandaInvitoToMail(String email) {
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", email, null));
        intent.putExtra(Intent.EXTRA_EMAIL, email);
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.system_invite_by_mail_subject));
        intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.system_invita_testo));
        if (intent.resolveActivity(getContext().getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Toast.makeText(getContext(), getString(R.string.general_error_no_app_found), Toast.LENGTH_SHORT).show();
        }
    }
}
