package it.mozart.yeap.ui.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.jakewharton.rxbinding.widget.RxTextView;
import com.jakewharton.rxbinding.widget.TextViewTextChangeEvent;
import com.lapism.searchview.SearchView;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderAdapter;
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderDecoration;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import it.mozart.yeap.R;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.helpers.CreationModelHelper;
import it.mozart.yeap.realm.Group;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.dao.GroupDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.ui.base.BaseActivityWithSyncService;
import it.mozart.yeap.ui.base.BaseFragment;
import it.mozart.yeap.ui.events.creation.Act_EventoCrea;
import it.mozart.yeap.ui.groups.creation.Act_GruppoCrea;
import it.mozart.yeap.ui.main.adapters.MainPartecipantiAdapter;
import it.mozart.yeap.ui.main.adapters.MainPartecipantiSelectedAdapter;
import it.mozart.yeap.ui.main.events.MainPartecipantiUserRemovedEvent;
import it.mozart.yeap.ui.main.events.MainPartecipantiUserSelectedEvent;
import it.mozart.yeap.ui.views.RecyclerViewIndex;
import it.mozart.yeap.utility.Utils;
import jp.wasabeef.recyclerview.animators.ScaleInAnimator;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;

public class Frg_MainPartecipanti extends BaseFragment implements MainCreationControl {

    @BindView(R.id.recyclerview_selected)
    RecyclerView mRecyclerViewSelected;
    @BindView(R.id.recyclerview)
    RecyclerView mRecyclerView;
    @BindView(R.id.index)
    RecyclerViewIndex mIndex;
    @BindView(R.id.index_text)
    TextView mIndexText;
    @BindView(R.id.searchview)
    SearchView mSearchView;

    // SUPPORT

    @Inject
    Realm realm;
    @Inject
    CreationModelHelper creatioModelHelper;

    private UserDao mUserDao;
    private MainPartecipantiAdapter mAdapter;
    private StickyHeaderDecoration mDecorator;
    private MainPartecipantiSelectedAdapter mAdapterSelected;
    private List<User> mUtentiSelected;
    private List<String> mCheckList;
    private List<String> mCheckInvitedList;
    private String mGruppoId;
    private String mQuery;
    private boolean mFromCreazione;
    private boolean mIsAttivita;
    private float mOffset;

    private static final String PARAM_GRUPPO_ID = "paramGruppoId";
    private static final String PARAM_FROM_CREAZIONE = "paramFromCreazione";
    private static final String PARAM_IS_ATTIVITA = "paramIsAttivita";

    // SYSTEM

    public static Frg_MainPartecipanti newInstance(boolean isAttivita) {
        Bundle args = new Bundle();
        args.putBoolean(PARAM_FROM_CREAZIONE, true);
        args.putBoolean(PARAM_IS_ATTIVITA, isAttivita);
        Frg_MainPartecipanti fragment = new Frg_MainPartecipanti();
        fragment.setArguments(args);
        return fragment;
    }

    public static Frg_MainPartecipanti newInstance(boolean isAttivita, String gruppoId) {
        Bundle args = new Bundle();
        args.putString(PARAM_GRUPPO_ID, gruppoId);
        args.putBoolean(PARAM_FROM_CREAZIONE, true);
        args.putBoolean(PARAM_IS_ATTIVITA, isAttivita);
        Frg_MainPartecipanti fragment = new Frg_MainPartecipanti();
        fragment.setArguments(args);
        return fragment;
    }

    public static Frg_MainPartecipanti newInstanceFromMain(boolean isAttivita) {
        Bundle args = new Bundle();
        args.putBoolean(PARAM_FROM_CREAZIONE, false);
        args.putBoolean(PARAM_IS_ATTIVITA, isAttivita);
        Frg_MainPartecipanti fragment = new Frg_MainPartecipanti();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUseBus(true);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_main_partecipanti;
    }

    @Override
    protected void initValues() {
        mOffset = Utils.dpToPx(74, getContext());
    }

    @Override
    protected void loadParameters(Bundle arguments) {
        mGruppoId = arguments.getString(PARAM_GRUPPO_ID);
        mFromCreazione = arguments.getBoolean(PARAM_FROM_CREAZIONE);
        mIsAttivita = arguments.getBoolean(PARAM_IS_ATTIVITA);
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {
    }

    @Override
    protected void saveInfos(Bundle outState) {
    }

    @Override
    protected void initialize() {
        mUserDao = new UserDao(realm);

        // Init
        mIndex.setRecyclerView(mRecyclerView, mIndexText);
        if (mCheckList == null) {
            mCheckList = new ArrayList<>();
        }
        if (mCheckInvitedList == null) {
            mCheckInvitedList = new ArrayList<>();
        }
        if (mUtentiSelected == null) {
            mUtentiSelected = new ArrayList<>();
            if (mGruppoId != null) {
                User myself = mUserDao.getUser(AccountProvider.getInstance().getAccountId());
                Group group = new GroupDao(realm).getGroup(mGruppoId);
                for (User utente : group.getGuests()) {
                    if (!utente.getUuid().equals(myself.getUuid())) {
                        mUtentiSelected.add(utente);
                        mCheckList.add(utente.getUuid());
                    }
                }
            }
        }

        // Init Searchview
        mSearchView.getEditText().clearFocus();
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                Utils.hideKeyboard(getActivity());
                return false;
            }
        });
        // Gestisco il debounce sulla modifica del testo della searchView
        registerSubscription(RxTextView.textChangeEvents(mSearchView.getEditText())
                .debounce(300, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getSearchObserver()));
        mSearchView.setShadow(false);
        mSearchView.setVoice(true, this);

        // RecyclerView e Filtro
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        mRecyclerViewSelected.setLayoutManager(linearLayoutManager);
        mAdapterSelected = new MainPartecipantiSelectedAdapter(mUtentiSelected);
        mRecyclerViewSelected.setAdapter(mAdapterSelected);
        mRecyclerViewSelected.setItemAnimator(new ScaleInAnimator());

        if (mUtentiSelected.size() != 0) {
            // Riposiziono nel caso ci siano già utenti selezionati
            mSearchView.setTranslationY(mOffset);
            mRecyclerView.setTranslationY(mOffset);
            mRecyclerView.setPadding(0, 0, 0, (int) mOffset);
            mIndex.setTranslationY(mOffset);
            mIndex.setPadding(0, 0, 0, (int) mOffset);
        }

        filterList("");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SearchView.SPEECH_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (matches != null && matches.size() != 0) {
                mSearchView.setText(matches.get(0));
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        // Aggiorno il titolo e l'icona della toolbar dell'activity
        if (getActivity() instanceof Act_GruppoCrea) {
            ((Act_GruppoCrea) getActivity()).setToolbarTitleAndIconFromCrea(creatioModelHelper.getGroup().getTitle(), R.drawable.ic_arrow_back_white_24dp);
            ((Act_GruppoCrea) getActivity()).setToolbarSubtitleFromCrea(getString(R.string.main_partecipanti_title));
        } else if (getActivity() instanceof Act_EventoCrea) {
            ((Act_EventoCrea) getActivity()).setToolbarTitleAndIconFromCrea(creatioModelHelper.getEvent().getTitle(), R.drawable.ic_arrow_back_white_24dp);
            ((Act_EventoCrea) getActivity()).setToolbarSubtitleFromCrea(getString(R.string.main_partecipanti_title));
        }
    }

    @Override
    public void onStop() {
        if (mFromCreazione) {
            // Salvo gli utenti selezionati
            if (mCheckList.size() > 0 || mCheckInvitedList.size() > 0) {
                RealmList<User> list = new RealmList<>();
                UserDao userDao = new UserDao(realm);
                for (String id : mCheckList) {
                    list.add(userDao.getUser(id));
                }
                for (String phone : mCheckInvitedList) {
                    list.add(userDao.getUserLocal(phone));
                }
                if (mIsAttivita) {
                    creatioModelHelper.getEvent().setGuestsTemp(list);
                } else {
                    creatioModelHelper.getGroup().setGuestsTemp(list);
                }
            } else {
                if (mIsAttivita) {
                    creatioModelHelper.getEvent().setGuestsTemp(new ArrayList<User>());
                } else {
                    creatioModelHelper.getGroup().setGuestsTemp(new ArrayList<User>());
                }
            }
        }
        super.onStop();
    }

    @Subscribe
    public void onEvent(final MainPartecipantiUserSelectedEvent event) {
        if (!event.isRemove()) {
            mAdapterSelected.addUtente(event.getUtente());
            mRecyclerViewSelected.smoothScrollToPosition(0);
        } else {
            mAdapterSelected.removeUtente(event.getUtente());
        }
        resizingViewOnUtenteSelected(mUtentiSelected.size() == 0);
        if (getActivity() instanceof Act_MainAggiungiPartecipante) {
            ((Act_MainAggiungiPartecipante) getActivity()).partecipanteAggiunto();
        }
    }

    @Subscribe
    public void onEvent(MainPartecipantiUserRemovedEvent event) {
        mAdapterSelected.removeUtente(event.getUtente());
        if (event.getUtente().getUuid() != null) {
            int index = mCheckList.indexOf(event.getUtente().getUuid());
            if (index != -1) {
                mCheckList.remove(event.getUtente().getUuid());
                mRecyclerView.getAdapter().notifyDataSetChanged();
            }
        } else {
            int index = mCheckInvitedList.indexOf(event.getUtente().getPhone());
            if (index != -1) {
                mCheckInvitedList.remove(event.getUtente().getPhone());
                mRecyclerView.getAdapter().notifyDataSetChanged();
            }
        }
        resizingViewOnUtenteSelected(mUtentiSelected.size() == 0);
        if (getActivity() instanceof Act_MainAggiungiPartecipante) {
            ((Act_MainAggiungiPartecipante) getActivity()).partecipanteRimosso();
        }
    }

    @Override
    public boolean canGoOn() {
        if (getActivity() instanceof BaseActivityWithSyncService) {
            ((BaseActivityWithSyncService) getActivity()).showToolbarSpecial(true);
        }
        return true;
    }

    // FUNCTIONS

    /**
     * Recupero la lista degli utenti selezionati
     *
     * @return selezionati
     */
    public List<String> getCheckList() {
        if (mCheckList == null)
            return new ArrayList<>();
        return mCheckList;
    }

    /**
     * Recupero la lista degli utenti selezionati (non in yeap)
     *
     * @return selezionati
     */
    public List<String> getCheckInvitedList() {
        if (mCheckInvitedList == null)
            return new ArrayList<>();
        return mCheckInvitedList;
    }

    /**
     * Eseguo il resize e il riposizionamento della recyclerview e dell'indice
     *
     * @param isClose true se si deve espandere
     */
    private void resizingViewOnUtenteSelected(boolean isClose) {
        // Traslo e assegno il padding bottom alle view
        if (!isClose) {
            mSearchView.animate().translationY(mOffset).setDuration(150).start();
            mRecyclerView.animate().translationY(mOffset).setDuration(150).start();
            mIndex.animate().translationY(mOffset).setDuration(150).start();
            mRecyclerView.setPadding(0, 0, 0, (int) mOffset);
            mIndex.setPadding(0, 0, 0, (int) mOffset);
        } else {
            mSearchView.animate().translationY(0).setDuration(150).start();
            mRecyclerView.animate().translationY(0).setDuration(150).start();
            mIndex.animate().translationY(0).setDuration(150).start();
            mRecyclerView.setPadding(0, 0, 0, 0);
            mIndex.setPadding(0, 0, 0, 0);
        }
    }

    /**
     * Filtro gli utenti a seconda del testo immesso
     *
     * @param text testo
     */
    protected void filterList(final String text) {
        mQuery = text;

        RealmResults<User> mDataArray = mUserDao.getListUserForInviting(text);

        // Gestisco la creazione degli header
        // Ogni header ha un campo che indica la sua posizione rispetto alla lista degli utenti
        List<HeaderItem> list = new ArrayList<>();
        int id = 0;
        int position = 0;
        String current = null;
        for (int i = 0; i < mDataArray.size(); i++) {
            char c = mDataArray.get(i).getChatName(true).charAt(0);
            if ((c < 65 || c > 90) && (c < 97 || c > 122)) {
                c = '#';
            }
            if (current == null) {
                list.add(new HeaderItem(id, position, c));
                current = String.valueOf(c);
                position = i;
            } else if (!current.equalsIgnoreCase(String.valueOf(c))) {
                id++;
                list.add(new HeaderItem(id, i, c));
                current = String.valueOf(c);
                position = i;
            }
        }
        id++;
        list.add(new HeaderItem(id, position, current != null ? current.charAt(0) : ' '));

        // Aggiorno l'adapter
        if (mRecyclerView.getAdapter() == null) {
            if (mIsAttivita) {
                mAdapter = new MainPartecipantiAdapter(getContext(), mDataArray, list, mCheckList, mCheckInvitedList, creatioModelHelper.getEvent().getGuestsHashSet(), creatioModelHelper.getEvent().getGuestsNotYeapHashSet());
            } else {
                mAdapter = new MainPartecipantiAdapter(getContext(), mDataArray, list, mCheckList, mCheckInvitedList, creatioModelHelper.getGroup().getGuestsHashSet(), creatioModelHelper.getGroup().getGuestsNotYeapHashSet());
            }
            mRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.updateAdapter(mDataArray, list, mCheckList, mCheckInvitedList);
        }
        // Aggiorno il listener
        mRecyclerView.removeOnScrollListener(null);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (mRecyclerView == null)
                    return;

                if (recyclerView.getScrollState() == RecyclerView.SCROLL_STATE_IDLE)
                    return;
                // Cambio l'indice selezionato
                int position = ((LinearLayoutManager) mRecyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                char c = mAdapter.getList().get(position).getChatName(true).charAt(0);
                if ((c < 65 || c > 90) && (c < 97 || c > 122)) {
                    c = '#';
                }
                mIndex.changeSelected(String.valueOf(c));
            }
        });

        if (mDecorator == null) {
            mDecorator = new StickyHeaderDecoration((StickyHeaderAdapter) mRecyclerView.getAdapter());
            mRecyclerView.addItemDecoration(mDecorator);
        } else {
            mDecorator.clearHeaderCache();
        }
    }

    /**
     * Osservabile sul cambiamento di testo della searchView
     *
     * @return observabile
     */
    private Observer<TextViewTextChangeEvent> getSearchObserver() {
        return new Observer<TextViewTextChangeEvent>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(TextViewTextChangeEvent textViewTextChangeEvent) {
                // Ignoro se il testo è uguale e vuoto
                if ((mQuery == null || mQuery.isEmpty()) && textViewTextChangeEvent.text().toString().isEmpty()) {
                    return;
                }
                // Ignoro se il testo è uguale
                if (mQuery != null && mQuery.equalsIgnoreCase(textViewTextChangeEvent.text().toString())) {
                    return;
                }
                filterList(textViewTextChangeEvent.text().toString());
            }
        };
    }

    // CLASSI

    /**
     * Classe per gestire gli header che vengono visualizzati nella recyclerview dei contatti
     */
    public class HeaderItem {

        // Id dell header
        private int id;
        // Posizione iniziale dalla quale si passa a una nuova lettera dell'alfabeto
        private int position;
        // Carattere
        private char character;

        HeaderItem(int id, int position, char character) {
            this.id = id;
            this.position = position;
            this.character = character;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }

        public char getCharacter() {
            return character;
        }

        public void setCharacter(char character) {
            this.character = character;
        }
    }
}
