package it.mozart.yeap.ui.groups.creation;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import org.greenrobot.eventbus.Subscribe;

import java.util.UUID;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import it.mozart.yeap.R;
import it.mozart.yeap.api.models.APIError;
import it.mozart.yeap.api.models.GruppoApiModel;
import it.mozart.yeap.api.models.ImageApiModel;
import it.mozart.yeap.api.services.GroupApi;
import it.mozart.yeap.api.services.UploadApi;
import it.mozart.yeap.events.ApiCallResultEvent;
import it.mozart.yeap.helpers.CreationModelHelper;
import it.mozart.yeap.realm.Group;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.ui.base.BaseFragment;
import it.mozart.yeap.ui.groups.main.Act_GruppoMain;
import it.mozart.yeap.ui.views.AnimCheckBox;
import it.mozart.yeap.utility.APIErrorUtils;
import it.mozart.yeap.utility.Utils;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class Frg_GruppoCreaRiepilogo extends BaseFragment {

    @BindView(R.id.avatar)
    ImageView mAvatar;
    @BindView(R.id.partecipanti)
    TextView mPartecipanti;

    @BindView(R.id.perm_inv_check)
    AnimCheckBox mPermInv;
    @BindView(R.id.crea)
    Button mCrea;

    @OnClick(R.id.crea)
    void creaClicked() {
        if (creationModelHelper.getGroup().getTitle() == null)
            return;

        getDynamicView().showLoading();

        // Evito la duplicazione del gruppo rimettendoci l'id
        if (mGruppoId != null) {
            creationModelHelper.getGroup().setUuid(mGruppoId);
        } else {
            creationModelHelper.getGroup().setUuid(null);
        }

        // Gestisco gli invitati
        creationModelHelper.getGroup().getPartecipants().clear();
        creationModelHelper.getGroup().getInvited().clear();
        for (User utente : creationModelHelper.getGroup().getGuestsTemp()) {
            if (utente.getUuid() != null) {
                creationModelHelper.getGroup().getPartecipants().add(utente.getUuid());
            }
            if (!utente.isInYeap()) {
                creationModelHelper.getGroup().getInvited().add(utente.getPhone());
            }
        }

        if (creationModelHelper.getGroup().getAvatarFile() != null) {
            uploadAvatar();
        } else {
            creaGruppo();
        }
    }

    @OnClick({R.id.perm_inv, R.id.perm_inv_check})
    void permInvClicked() {
        mPermInv.setChecked(!mPermInv.isChecked());
        creationModelHelper.getGroup().setUsersCanInvite(mPermInv.isChecked());
    }

    // SUPPORT

    @Inject
    CreationModelHelper creationModelHelper;
    @Inject
    GroupApi groupApi;
    @Inject
    UploadApi uploadApi;

    private String mGruppoId;

    // SYSTEM

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUseBus(true);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_gruppo_crea_riepilogo;
    }

    @Override
    protected void initValues() {
    }

    @Override
    protected void loadParameters(Bundle arguments) {

    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void initialize() {
        if (!Utils.isLollipop()) {
            mCrea.getLayoutParams().height = (int) Utils.dpToPx(45, getContext());
        }

        // Info
        if (creationModelHelper.getGroup().getGuestsTemp().size() == 1) {
            mPartecipanti.setText(getString(R.string.nuovo_gruppo_riepilogo_partecipante));
        } else {
            mPartecipanti.setText(String.format(getString(R.string.nuovo_gruppo_riepilogo_partecipanti), creationModelHelper.getGroup().getGuestsTemp().size()));
        }

        // Permessi
        mPermInv.setChecked(creationModelHelper.getGroup().isUsersCanInvite(), false);

        // Avatar
        if (creationModelHelper.getGroup().getAvatarFile() != null) {
            Glide.with(this).load(creationModelHelper.getGroup().getAvatarFile()).asBitmap().placeholder(R.drawable.avatar_gruppo)
                    .diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(new BitmapImageViewTarget(mAvatar) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    mAvatar.setImageDrawable(circularBitmapDrawable);
                }
            });
        } else {
            Glide.with(this).load(creationModelHelper.getGroup().getAvatarThumbUrl()).asBitmap().placeholder(R.drawable.avatar_gruppo)
                    .diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(new BitmapImageViewTarget(mAvatar) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    mAvatar.setImageDrawable(circularBitmapDrawable);
                }
            });
        }

        // Partecipanti
//        mPartecipanti.setGallery(creationModelHelper.getGruppo().getInvitatiTemp(), creationModelHelper.getGruppo().getUuid());

        setRequestId(UUID.randomUUID().toString());
    }

    @Override
    public void onStart() {
        super.onStart();
        // Aggiorno il titolo, il sottotitolo e l'icona della toolbar dell'activity
        if (getActivity() instanceof Act_GruppoCrea) {
            ((Act_GruppoCrea) getActivity()).setToolbarTitleAndIconFromCrea(creationModelHelper.getGroup().getTitle(), R.drawable.ic_arrow_back_white_24dp);
            ((Act_GruppoCrea) getActivity()).setToolbarSubtitleFromCrea(getString(R.string.nuovo_gruppo_riepilogo_title));
        }
    }

    @Subscribe
    public void onEvent(ApiCallResultEvent event) {
        if (event.getRequestId().equals(getRequestId())) {
            if (mGruppoId == null) {
                Toast.makeText(getContext(), getString(R.string.general_error_message), Toast.LENGTH_SHORT).show();
                return;
            }
            cancelTimer();
            Intent intent = Act_GruppoMain.newIntent(mGruppoId);
            getActivity().setResult(Activity.RESULT_OK, new Intent());
            getActivity().finish();
            startActivity(intent);
        }
    }

    // FUNCTIONS

    /**
     * Eseguo l'upload dell'avatar
     */
    private void uploadAvatar() {
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), creationModelHelper.getGroup().getAvatarFile());
        MultipartBody.Part image = MultipartBody.Part.createFormData("image", creationModelHelper.getGroup().getAvatarFile().getName(), requestFile);
        registerSubscription(uploadApi.uploadAvatar(image)
                .subscribeOn(Schedulers.newThread())
                .flatMap(new Func1<ImageApiModel, Observable<Group>>() {
                    @Override
                    public Observable<Group> call(ImageApiModel imageApiModel) {
                        setRequestId(UUID.randomUUID().toString());
                        creationModelHelper.getGroup().setAvatar(imageApiModel.getUploadref());
                        return groupApi.createGroup(getRequestId(), createAttivitaModel(creationModelHelper.getGroup()));
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Group>() {
                    @Override
                    public void call(Group gruppo) {
                        mGruppoId = gruppo.getUuid();
                        startTimer();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        getDynamicView().hide();

                        APIError error = APIErrorUtils.parseError(getContext(), throwable);
                        Toast.makeText(getContext(), error.message(), Toast.LENGTH_LONG).show();
                    }
                }));
    }

    /**
     * Creo il gruppo
     */
    private void creaGruppo() {
        registerSubscription(groupApi.createGroup(getRequestId(), createAttivitaModel(creationModelHelper.getGroup()))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Group>() {
                    @Override
                    public void call(Group gruppo) {
                        mGruppoId = gruppo.getUuid();
                        startTimer();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        getDynamicView().hide();

                        APIError error = APIErrorUtils.parseError(getContext(), throwable);
                        Toast.makeText(getContext(), error.message(), Toast.LENGTH_LONG).show();
                    }
                }));
    }

    /**
     * Creo modello per l'api
     *
     * @param gruppo gruppo
     * @return modello
     */
    private GruppoApiModel createAttivitaModel(Group gruppo) {
        GruppoApiModel model = new GruppoApiModel();
        model.setUuid(gruppo.getUuid());
        model.setAvatar(gruppo.getAvatar());
        model.setTitle(gruppo.getTitle());
        model.setPartecipants(gruppo.getPartecipants());
        model.setInvited(gruppo.getInvited());
        model.setUsersCanInvite(gruppo.isUsersCanInvite());
        return model;
    }

    /**
     * Annullo la chiamata api (se possibile)
     *
     * @return se l'azione è stata eseguita
     */
    public boolean cancelApiCall() {
        if (!getDynamicView().isContentShown()) {
            unsubscribeSubscriptions();
            cancelTimer();
            return true;
        }
        return false;
    }
}
