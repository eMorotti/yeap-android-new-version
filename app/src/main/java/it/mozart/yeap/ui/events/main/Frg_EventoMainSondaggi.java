package it.mozart.yeap.ui.events.main;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import javax.inject.Inject;

import butterknife.BindView;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;
import it.mozart.yeap.R;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.Poll;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.PollDao;
import it.mozart.yeap.ui.base.BaseFragment;
import it.mozart.yeap.ui.events.main.adapters.EventoMainSondaggiAdapter;

public class Frg_EventoMainSondaggi extends BaseFragment {

    @BindView(R.id.recyclerview)
    RecyclerView mRecyclerView;
    @BindView(R.id.no_items)
    View mNoItems;

    // SUPPORT

    @Inject
    Realm realm;

    private EventoMainSondaggiAdapter mAdapter;
    private Event mEvent;
    private String mId;

    private static final String PARAM_ID = "paramId";

    // SYSTEM

    public static Frg_EventoMainSondaggi newInstance(String attivitaId) {
        Bundle args = new Bundle();
        args.putString(PARAM_ID, attivitaId);
        Frg_EventoMainSondaggi fragment = new Frg_EventoMainSondaggi();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_main_evento_sondaggi;
    }

    @Override
    protected void initValues() {

    }

    @Override
    protected void loadParameters(Bundle arguments) {
        mId = arguments.getString(PARAM_ID);
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mEvent != null) {
            mEvent.removeAllChangeListeners();
        }
    }

    @Override
    protected void initialize() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isAdded()) {
                    return;
                }
                // Init attività
                mEvent = new EventDao(realm).getEvent(mId);

                RealmResults<Poll> sondaggi = new PollDao(realm).getListPollsFromEvent(mEvent.getUuid());
                mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                mAdapter = new EventoMainSondaggiAdapter(getContext(), mEvent, sondaggi);
                mRecyclerView.setAdapter(mAdapter);

                mEvent.addChangeListener(new RealmChangeListener<Event>() {
                    @Override
                    public void onChange(@NonNull Event element) {
                        if (element.isValid()) {
                            mAdapter.updateEvent(element);
                        }
                    }
                });

                mNoItems.setVisibility(mAdapter.getItemCount() != 0 || !mEvent.isMyselfPresent() ? View.GONE : View.VISIBLE);
                mAdapter.setOnChangeListener(new RealmRecyclerViewAdapter.OnChangeListener() {
                    @Override
                    public void changed() {
                        if (mNoItems != null) {
                            mNoItems.setVisibility(mAdapter.getItemCount() != 0 || (mEvent.isValid() && !mEvent.isMyselfPresent()) ? View.GONE : View.VISIBLE);
                        }
                    }

                    @Override
                    public void inserted(int position) {
                        if (mRecyclerView != null) {
                            mRecyclerView.smoothScrollToPosition(0);
                        }
                    }
                });
            }
        }, 400);
    }
}
