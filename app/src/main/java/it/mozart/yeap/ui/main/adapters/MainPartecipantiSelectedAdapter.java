package it.mozart.yeap.ui.main.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import it.mozart.yeap.R;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.ui.main.events.MainPartecipantiUserRemovedEvent;
import it.mozart.yeap.ui.views.AutoRefreshImageView;

public class MainPartecipantiSelectedAdapter extends RecyclerView.Adapter<MainPartecipantiSelectedAdapter.SelectedViewHolder> {

    private List<User> mList;

    public MainPartecipantiSelectedAdapter(List<User> list) {
        this.mList = list;
    }

    @Override
    public SelectedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_partecipanti_utente_selected, parent, false);
        return new SelectedViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SelectedViewHolder holder, int position) {
        final User utente = mList.get(position);

        holder.name.setText(utente.getChatName());
        holder.avatar.bindThumb(utente.getUuid());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void addUtente(User utente) {
        mList.add(0, utente);
        notifyItemInserted(0);
    }

    public void removeUtente(User utente) {
        int index = mList.indexOf(utente);
        if (index != -1) {
            mList.remove(utente);
            if (mList.size() == 0)
                notifyDataSetChanged();
            else
                notifyItemRemoved(index);
        }
    }

    class SelectedViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.avatar)
        AutoRefreshImageView avatar;
        @BindView(R.id.name)
        TextView name;

        SelectedViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getLayoutPosition();
            User utente = mList.get(position);
            EventBus.getDefault().post(new MainPartecipantiUserRemovedEvent(utente));
        }
    }
}
