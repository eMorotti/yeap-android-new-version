package it.mozart.yeap.ui.support;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.ContactsContract;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.lapism.searchview.SearchView;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderAdapter;
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderDecoration;
import io.realm.Realm;
import io.realm.RealmResults;
import it.mozart.yeap.R;
import it.mozart.yeap.helpers.AnalitycsProvider;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.services.syncAdapter.contacts.ContactsManager;
import it.mozart.yeap.ui.base.BaseActivityWithSyncService;
import it.mozart.yeap.ui.profile.Act_Profilo;
import it.mozart.yeap.ui.support.adapters.ContactsAdapter;
import it.mozart.yeap.ui.support.events.ContactSelectedEvent;
import it.mozart.yeap.ui.support.events.MandaInvitoEvent;
import it.mozart.yeap.ui.support.events.UserSelectedEvent;
import it.mozart.yeap.ui.views.RecyclerViewIndex;
import it.mozart.yeap.utility.Utils;

public class Act_Contacts extends BaseActivityWithSyncService {

    @BindView(R.id.recyclerview)
    RecyclerView mRecyclerView;
    @BindView(R.id.index)
    RecyclerViewIndex mIndex;
    @BindView(R.id.index_text)
    TextView mIndexText;
    @BindView(R.id.searchview)
    SearchView mSearchView;

    // SUPPORT

    @Inject
    Realm realm;

    private UserDao mUtenteDao;
    private ContactsAdapter mAdapter;
    private RecyclerView.ItemDecoration mDecorator;
    private String mContactId;
    private String mContactPhone;
    private List<List<String>> mContactInfo;

    // SYSTEM

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUseBus(true);

        if (getSupportActionBar() != null) {
            setToolbarTitle(getString(R.string.app_contacts));
            setToolbarSubtitle(getString(R.string.app_contacts_subtitle));
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_contacts;
    }

    @Override
    protected void initVariables() {
    }

    @Override
    protected void loadParameters(Bundle extras) {

    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        User contatto = mUtenteDao.getUserLocal(mContactPhone);
        if (contatto != null) {
            menu.clear();
            menu.setHeaderTitle(contatto.getChatName());
            menu.add(0, 1, Menu.NONE, getString(R.string.system_show_contact));
            menu.add(1, 0, Menu.NONE, getString(R.string.system_invite_by_sms_user));
            int i = 0;
            for (String email : mContactInfo.get(1)) {
                menu.add(2, i, Menu.NONE, getString(R.string.system_invite_by_mail_user));
                i++;
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getGroupId()) {
            case 0:
                // Visualizzo in rubrica
                Intent intent = new Intent(Intent.ACTION_VIEW);
                Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, mContactId);
                intent.setData(uri);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                } else {
                    Toast.makeText(this, getString(R.string.general_error_no_app_found), Toast.LENGTH_SHORT).show();
                }
                break;
            case 1:
                // Invito nell'app tramite sms
                User contatto = mUtenteDao.getUserLocal(mContactPhone);
                if (contatto != null) {
                    mandaInvitoToNumber(contatto.getPhone());
                }
                break;
            case 2:
                // Invito nell'app tramite mail
                mandaInvitoToMail(mContactInfo.get(1).get(item.getOrder()));
                break;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    protected void initialize() {
        mUtenteDao = new UserDao(realm);

        // Init
        mIndex.setRecyclerView(mRecyclerView, mIndexText);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        filterList(null);

        // Init Searchview
        mSearchView.getEditText().clearFocus();
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                filterList(newText);
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                Utils.hideKeyboard(Act_Contacts.this);
                return false;
            }
        });
        mSearchView.setShadow(false);
        mSearchView.setVoice(true, this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SearchView.SPEECH_REQUEST_CODE && resultCode == RESULT_OK) {
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (matches != null && matches.size() != 0) {
                mSearchView.setText(matches.get(0));
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Subscribe
    public void onEvent(UserSelectedEvent event) {
        AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "viewProfile", "users");
        startActivity(Act_Profilo.newIntent(event.getId()));
    }

    @Subscribe
    public void onEvent(MandaInvitoEvent event) {
        mandaInvito();
    }

    @Subscribe
    public void onEvent(ContactSelectedEvent event) {
        mContactId = event.getId();
        mContactPhone = event.getPhone();
        mContactInfo = new ContactsManager().getDetailInfo(this, mContactId);
        registerForContextMenu(event.getView());
        openContextMenu(event.getView());
        unregisterForContextMenu(event.getView());
    }

    // FUNCTIONS

    /**
     * Faccio partire l'intent per mandare un messaggio
     *
     * @param phoneNumber telefono
     */
    private void mandaInvitoToNumber(String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + phoneNumber));
        intent.putExtra("sms_body", getString(R.string.system_invita_testo));
        if (intent.resolveActivity(getPackageManager()) != null) {
            AnalitycsProvider.getInstance().logInvite(mFirebaseAnalytics, "SMS");
            startActivity(intent);
        } else {
            Toast.makeText(this, getString(R.string.general_error_no_app_found), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Faccio partire l'intent per mandare una mail
     *
     * @param email email
     */
    private void mandaInvitoToMail(String email) {
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", email, null));
        intent.putExtra(Intent.EXTRA_EMAIL, email);
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.system_invite_by_mail_subject));
        intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.system_invita_testo));
        if (intent.resolveActivity(getPackageManager()) != null) {
            AnalitycsProvider.getInstance().logInvite(mFirebaseAnalytics, "email");
            startActivity(intent);
        } else {
            Toast.makeText(this, getString(R.string.general_error_no_app_found), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Manda l'invita per scaricare l'applicazione
     */
    private void mandaInvito() {
        // Creo intent
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");

        // Recupero messaggio
        String messaggio = getString(R.string.system_invita_testo);

        // Creo lista app ignorando me stesso
        List<ResolveInfo> resInfo = getPackageManager().queryIntentActivities(intent, 0);
        List<Intent> intentList = new ArrayList<>();
        for (ResolveInfo resolveInfo : resInfo) {
            String packageName = resolveInfo.activityInfo.packageName;
            if (!TextUtils.equals(packageName, "it.mozart.yeap")) {
                // Creo i veri intent
                Intent intentInner = new Intent();
                intentInner.setComponent(new ComponentName(packageName, resolveInfo.activityInfo.name));
                intentInner.setAction(Intent.ACTION_SEND);
                intentInner.setType("text/plain");
                intentInner.setPackage(packageName);
                intentInner.putExtra(Intent.EXTRA_TEXT, messaggio);
                intentList.add(intentInner);
            }
        }

        if (!intentList.isEmpty()) {
            // Apro il dialog
            Intent chooser = Intent.createChooser(intentList.remove(0), getString(R.string.system_invita_title));
            chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentList.toArray(new Parcelable[intentList.size()]));
            startActivity(chooser);
        } else {
            Toast.makeText(this, getString(R.string.general_error_no_app_found), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Filtra la lista di contatti
     *
     * @param text filtro
     */
    private void filterList(String text) {
        RealmResults<User> mDataArray = mUtenteDao.getListUsers(text);

        // Gestisco la creazione degli header
        // Ogni header ha un campo che indica la sua posizione rispetto alla lista degli utenti
        final List<HeaderItem> list = new ArrayList<>();
        int id = 0;
        int position = 0;
        String current = null;
        for (int i = 0; i < mDataArray.size(); i++) {
            char c = mDataArray.get(i).getChatName(true).charAt(0);
            if ((c < 65 || c > 90) && (c < 97 || c > 122)) {
                c = '#';
            }
            if (current == null) {
                current = String.valueOf(c);
                position = i;
            } else if (!current.equalsIgnoreCase(String.valueOf(c))) {
                list.add(new HeaderItem(id, position));
                id++;
                current = String.valueOf(c);
                position = i;
            }
        }
        id++;
        list.add(new HeaderItem(id, position));

        // Aggiorno l'adapter
        if (mRecyclerView.getAdapter() == null) {
            mAdapter = new ContactsAdapter(mDataArray, list);
            mRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.updateAdapter(mDataArray, list);
        }
        // Aggiorno il listener
        mRecyclerView.removeOnScrollListener(null);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (recyclerView.getScrollState() == RecyclerView.SCROLL_STATE_IDLE)
                    return;
                // Cambio l'indice selezionato
                int position = ((LinearLayoutManager) mRecyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                char c = mAdapter.getList().get(position).getChatName(true).charAt(0);
                if ((c < 65 || c > 90) && (c < 97 || c > 122)) {
                    c = '#';
                }
                mIndex.changeSelected(String.valueOf(c));
            }
        });

        // Aggiorno il decorator per l'header
        if (mDecorator != null) {
            mRecyclerView.removeItemDecoration(mDecorator);
        }
        mDecorator = new StickyHeaderDecoration((StickyHeaderAdapter) mRecyclerView.getAdapter());
        mRecyclerView.addItemDecoration(mDecorator);
    }

    public class HeaderItem {

        // Id dell header
        private int id;
        // Posizione iniziale dalla quale si passa a una nuova lettera dell'alfabeto
        private int position;

        HeaderItem(int id, int position) {
            this.id = id;
            this.position = position;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }
    }
}
