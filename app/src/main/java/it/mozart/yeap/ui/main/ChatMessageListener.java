package it.mozart.yeap.ui.main;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

import it.mozart.yeap.realm.Message;

public interface ChatMessageListener {

    List<Message> getMessaggi();

    SimpleDateFormat getFormatter();

    int getNumItems();

    int getItemType(int position);

    long getNumNewMessages();

    HashMap<String, String> getSelectedMessageIds();
}