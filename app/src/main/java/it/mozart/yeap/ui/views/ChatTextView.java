package it.mozart.yeap.ui.views;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.AppCompatTextView;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.AttributeSet;
import android.view.View;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.EasySpan;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class ChatTextView extends AppCompatTextView {

    private Subscription mTextSubscription;
    private Pattern mPattern = Pattern.compile(Constants.URL_PATTERN, Pattern.CASE_INSENSITIVE);
    private boolean mAddMoreSpaces = false;
    private boolean mIgnoreSpaces = false;
    private int mLinkColor;

    public ChatTextView(Context context) {
        super(context);
    }

    public ChatTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ChatTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setChatText(String text) {
        if (text == null) {
            return;
        }
        setChatText(text, false);
    }

    public void setChatText(String text, boolean ignoreUrl) {
        String newText;
        if (mIgnoreSpaces) {
            newText = text;
        } else {
            // Aggiungo spazi per lasciare lo spazio all'orario
            newText = text + "\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0"
                    + (mAddMoreSpaces ? "\u00A0" : "");
        }
        if (!ignoreUrl) {
            setText(newText);
            createLinkSpanIfPresent(newText);
        } else {
            setText(newText);
        }
    }

    public void setAddMoreSpaces(boolean addMoreSpaces) {
        this.mAddMoreSpaces = addMoreSpaces;
    }

    public void setIgnoreSpaces(boolean ignoreSpaces) {
        this.mIgnoreSpaces = ignoreSpaces;
    }

    public void setLinkColor(int color) {
        mLinkColor = color;
    }

    /**
     * Creo lo span per l'indirizzo web
     *
     * @param text testo
     */
    private void createLinkSpanIfPresent(final String text) {
        mTextSubscription = Observable.just(text)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(new Func1<String, Observable<Spanned>>() {
                    @Override
                    public Observable<Spanned> call(String s) {
                        EasySpan.Builder builder = new EasySpan.Builder();
                        final Matcher matcher = mPattern.matcher(text);
                        boolean trovato = false;
                        while (matcher.find()) {
                            trovato = true;
                            int start = matcher.start(0);
                            int end = matcher.end(0);
                            final String url = matcher.group(0);
                            ClickableSpan clickable = new ClickableSpan() {
                                public void onClick(View view) {
                                    Intent intent = new Intent(Intent.ACTION_VIEW);
                                    intent.setData(Uri.parse(url.toLowerCase()));
                                    getContext().startActivity(intent);
                                }

                                @Override
                                public void updateDrawState(TextPaint ds) {
                                    super.updateDrawState(ds);
                                    ds.setUnderlineText(false);
                                }
                            };

                            builder = builder.appendSpansWithPositions(text, start, end, clickable, new ForegroundColorSpan(mLinkColor), new UnderlineSpan());
                        }
                        if (!trovato)
                            return Observable.just(null);
                        return Observable.just(builder.build().getSpannedText());
                    }
                })
                .subscribe(new Action1<Spanned>() {
                    @Override
                    public void call(Spanned spanned) {
                        if (spanned != null) {
                            setText(spanned);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                    }
                });
    }

    public void clear() {
        if (mTextSubscription != null) {
            mTextSubscription.unsubscribe();
            mTextSubscription = null;
        }
    }
}
