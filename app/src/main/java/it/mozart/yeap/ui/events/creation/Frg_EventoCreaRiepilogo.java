package it.mozart.yeap.ui.events.creation;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.UUID;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.realm.Realm;
import it.mozart.yeap.R;
import it.mozart.yeap.api.models.APIError;
import it.mozart.yeap.api.models.EventoApiModel;
import it.mozart.yeap.api.models.ImageApiModel;
import it.mozart.yeap.api.services.EventApi;
import it.mozart.yeap.api.services.UploadApi;
import it.mozart.yeap.events.ApiCallResultEvent;
import it.mozart.yeap.helpers.AnalitycsProvider;
import it.mozart.yeap.helpers.CreationModelHelper;
import it.mozart.yeap.helpers.ImageInputHelper;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.Group;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.dao.GroupDao;
import it.mozart.yeap.ui.base.BaseFragment;
import it.mozart.yeap.ui.events.main.Act_EventoMain;
import it.mozart.yeap.ui.support.Act_ImageCrop;
import it.mozart.yeap.ui.views.AnimCheckBox;
import it.mozart.yeap.utility.APIErrorUtils;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.Utils;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class Frg_EventoCreaRiepilogo extends BaseFragment {

    @BindView(R.id.info_layout)
    View mInfoLayout;
    @BindView(R.id.avatar)
    ImageView mAvatar;
    @BindView(R.id.partecipanti)
    TextView mPartecipanti;

    @BindView(R.id.perm_inv_check)
    AnimCheckBox mPermInv;

    @BindView(R.id.crea)
    Button mCrea;

    @OnClick(R.id.avatar)
    void avatarClicked() {
        avatarClickedMethod(false);
    }

    @OnClick(R.id.crea)
    void creaClicked() {
        if (creationModelHelper.getEvent().getTitle() == null)
            return;

        getDynamicView().showLoading();

        if (!creationModelHelper.getEvent().isUsersCanAddItems() || !creationModelHelper.getEvent().isUsersCanInvite()) {
            AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "setActivitySettings", "creation");
        }
        AnalitycsProvider.getInstance().logCustom(mFirebaseAnalytics, "createActivityConfirm");

        // Evito la duplicazione dell'attività rimettendoci l'id
        if (mEventoId != null) {
            creationModelHelper.getEvent().setUuid(mEventoId);
        } else {
            creationModelHelper.getEvent().setUuid(null);
        }

        // Gestisco gli invitati
        creationModelHelper.getEvent().getPartecipants().clear();
        creationModelHelper.getEvent().getInvited().clear();
        for (User utente : creationModelHelper.getEvent().getGuestsTemp()) {
            if (utente.getUuid() != null) {
                creationModelHelper.getEvent().getPartecipants().add(utente.getUuid());
            }
            if (!utente.isInYeap()) {
                creationModelHelper.getEvent().getInvited().add(utente.getPhone());
            }
        }
        creationModelHelper.getEvent().setGroup(mGruppo);

        creationModelHelper.getEvent().setAvatar(mImageRef);
        if (creationModelHelper.getEvent().getAvatarFile() != null && mImageRef == null) {
            uploadAvatar();
        } else {
            creaEvento();
        }
    }

    @OnClick({R.id.perm_inv, R.id.perm_inv_check})
    void permInvClicked() {
        mPermInv.setChecked(!mPermInv.isChecked());
        creationModelHelper.getEvent().setUsersCanInvite(mPermInv.isChecked());
    }

    // SUPPORT

    @Inject
    Realm realm;
    @Inject
    CreationModelHelper creationModelHelper;
    @Inject
    EventApi eventApi;
    @Inject
    UploadApi uploadApi;

    private Group mGruppo;
    private String mEventoId;
    private String mGruppoId;
    private String mImageRef;

    private static final String PARAM_GRUPPO_ID = "paramGruppoId";

    private static final int IMG_CODE = 101;
    private static final int IMG_CROP_CODE = 102;

    // SYSTEM

    public static Frg_EventoCreaRiepilogo newInstance(String gruppoId) {
        Bundle args = new Bundle();
        args.putString(PARAM_GRUPPO_ID, gruppoId);
        Frg_EventoCreaRiepilogo fragment = new Frg_EventoCreaRiepilogo();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUseBus(true);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_evento_crea_riepilogo;
    }

    @Override
    protected void initValues() {
    }

    @Override
    protected void loadParameters(Bundle arguments) {
        mGruppoId = arguments.getString(PARAM_GRUPPO_ID);
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    protected void initialize() {
        if (!Utils.isLollipop()) {
            mCrea.getLayoutParams().height = (int) Utils.dpToPx(45, getContext());
        }

        if (mGruppoId != null) {
            mGruppo = new GroupDao(realm).getGroup(mGruppoId);
        }

        // Info
        if (creationModelHelper.getEvent().getGuestsTemp().size() == 1) {
            mPartecipanti.setText(getString(R.string.nuovo_gruppo_riepilogo_partecipante));
        } else {
            mPartecipanti.setText(String.format(getString(R.string.nuovo_gruppo_riepilogo_partecipanti), creationModelHelper.getEvent().getGuestsTemp().size()));
        }

        // Permessi
        mPermInv.setChecked(creationModelHelper.getEvent().isUsersCanInvite(), false);

        // Avatar
        if (creationModelHelper.getEvent().getAvatarFile() != null) {
            Glide.with(this).load(creationModelHelper.getEvent().getAvatarFile()).asBitmap().placeholder(R.drawable.avatar_attivita)
                    .diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(new BitmapImageViewTarget(mAvatar) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    mAvatar.setImageDrawable(circularBitmapDrawable);
                }
            });
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        // Aggiorno il titolo, il sottotitolo e l'icona della toolbar dell'activity
        if (getActivity() instanceof Act_EventoCrea) {
            ((Act_EventoCrea) getActivity()).setToolbarTitleAndIconFromCrea(creationModelHelper.getEvent().getTitle(), R.drawable.ic_arrow_back_white_24dp);
            ((Act_EventoCrea) getActivity()).setToolbarSubtitleFromCrea(getString(R.string.nuova_evento_riepilogo_title));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.IMAGE_PERMISSION_CODE:
                avatarClickedMethod(true);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case IMG_CODE:
                    // Chiamo l'activity per il crop dell'immagine selezionata
                    Uri copertinaUri = ImageInputHelper.getPickImageResultUri(getContext(), data);
                    startActivityForResult(Act_ImageCrop.newIntentFreeRatio(copertinaUri), IMG_CROP_CODE);
                    break;
                case IMG_CROP_CODE:
                    String image = data.getStringExtra(Constants.IMAGE_CROP);
                    if (image != null) {
                        // Salvo l'immagine come File per per poi poterla usare alla creazione dell'attività
                        creationModelHelper.getEvent().setAvatarFile(new File(image));
                        Glide.with(this).load(creationModelHelper.getEvent().getAvatarFile()).asBitmap().centerCrop().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(new BitmapImageViewTarget(mAvatar) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                mAvatar.setImageDrawable(circularBitmapDrawable);
                            }
                        });
                    } else {
                        // Mostro messaggio di errore se non è stato restituito niente dall'activity di crop
                        creationModelHelper.getEvent().setAvatarFile(null);
                        Toast.makeText(getContext(), getString(R.string.general_error_ritaglio_immagine), Toast.LENGTH_LONG).show();
                    }
                    break;
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ApiCallResultEvent event) {
        if (event.getRequestId().equals(getRequestId())) {
            cancelTimer();
            if (mEventoId == null) {
                Toast.makeText(getContext(), getString(R.string.general_error_message), Toast.LENGTH_SHORT).show();
                return;
            }
            Intent intent = Act_EventoMain.newIntent(mEventoId);
            getActivity().setResult(Activity.RESULT_OK, new Intent());
            getActivity().finish();
            startActivity(intent);
        }
    }

    // FUNCTIONS

    private void avatarClickedMethod(boolean ignoreCheck) {
        ImageInputHelper.openChooserForAvatarSelect(this, IMG_CODE, ignoreCheck);
    }

    /**
     * Eseguo l'upload dell'avatar
     */
    private void uploadAvatar() {
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), creationModelHelper.getEvent().getAvatarFile());
        MultipartBody.Part image = MultipartBody.Part.createFormData("image", creationModelHelper.getEvent().getAvatarFile().getName(), requestFile);
        registerSubscription(uploadApi.uploadAvatar(image)
                .subscribeOn(Schedulers.newThread())
                .flatMap(new Func1<ImageApiModel, Observable<Event>>() {
                    @Override
                    public Observable<Event> call(ImageApiModel imageApiModel) {
                        setRequestId(UUID.randomUUID().toString());
                        mImageRef = imageApiModel.getUploadref();
                        creationModelHelper.getEvent().setAvatar(mImageRef);
                        creationModelHelper.getEvent().setGroup(mGruppo);
                        return eventApi.createEvent(getRequestId(), createAttivitaModel());
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Event>() {
                    @Override
                    public void call(Event event) {
                        mEventoId = event.getUuid();
                        startTimer();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        getDynamicView().hide();

                        APIError error = APIErrorUtils.parseError(getContext(), throwable);
                        Toast.makeText(getContext(), error.message(), Toast.LENGTH_LONG).show();

                        throwable.printStackTrace();
                    }
                }));
    }

    /**
     * Creo l'attività
     */
    private void creaEvento() {
        setRequestId(UUID.randomUUID().toString());
        creationModelHelper.getEvent().setGroup(mGruppo);
        registerSubscription(eventApi.createEvent(getRequestId(), createAttivitaModel())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Event>() {
                    @Override
                    public void call(Event evento) {
                        mEventoId = evento.getUuid();
                        startTimer();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        getDynamicView().hide();

                        APIError error = APIErrorUtils.parseError(getContext(), throwable);
                        Toast.makeText(getContext(), error.message(), Toast.LENGTH_LONG).show();

                        throwable.printStackTrace();
                    }
                }));
    }

    /**
     * Creo modello per l'api
     *
     * @return modello
     */
    private EventoApiModel createAttivitaModel() {
        Event evento = creationModelHelper.getEvent();

        EventoApiModel model = new EventoApiModel();
        model.setUuid(evento.getUuid());
        model.setGroupUuid(evento.getGroupUuid());
        model.setAvatar(evento.getAvatar());
        model.setDesc(evento.getDesc());
        model.setTitle(evento.getTitle());
        model.setPartecipants(evento.getPartecipants());
        model.setInvited(evento.getInvited());
        model.setUsersCanInvite(evento.isUsersCanInvite());
        model.setUsersCanAddItems(evento.isUsersCanAddItems());
        model.setEventoInfo(evento.getEventInfo());

        if (creationModelHelper.isWherePollSelected()) {
            model.setWherePoll(creationModelHelper.getWherePoll());
        }
        if (creationModelHelper.isWhenPollSelected()) {
            model.setWhenPoll(creationModelHelper.getWhenPoll());
        }
        return model;
    }

    /**
     * Annullo la chiamata api (se possibile)
     *
     * @return se l'azione è stata eseguita
     */
    public boolean cancelApiCall() {
        if (!getDynamicView().isContentShown()) {
            unsubscribeSubscriptions();
            cancelTimer();
            return true;
        }
        return false;
    }
}
