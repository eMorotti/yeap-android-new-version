package it.mozart.yeap.ui.events.main.events;

import android.view.View;

public class VoteInterestEventClickedEvent {

    private View view;
    private String eventTitle;
    private String eventId;
    private int type;

    public VoteInterestEventClickedEvent(int type) {
        this.type = type;
    }

    public VoteInterestEventClickedEvent(String eventId, int type) {
        this.eventId = eventId;
        this.type = type;
    }

    public VoteInterestEventClickedEvent(View view, String eventTitle, String eventId, int type) {
        this.view = view;
        this.eventTitle = eventTitle;
        this.eventId = eventId;
        this.type = type;
    }

    public View getView() {
        return view;
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public String getEventId() {
        return eventId;
    }

    public int getType() {
        return type;
    }
}
