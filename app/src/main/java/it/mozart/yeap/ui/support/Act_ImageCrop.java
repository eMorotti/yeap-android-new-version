package it.mozart.yeap.ui.support;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.isseiaoki.simplecropview.CropImageView;

import java.io.File;
import java.io.FileNotFoundException;

import butterknife.BindView;
import butterknife.OnClick;
import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.ui.base.BaseActivityWithSyncService;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.PhotoGenerator;
import it.mozart.yeap.utility.Utils;

public class Act_ImageCrop extends BaseActivityWithSyncService {

    @BindView(R.id.crop_image)
    CropImageView mCropImage;
    @BindView(R.id.confirm_layout)
    View mConfirmLayout;

    @OnClick(R.id.confirm)
    void confirmClicked() {
        Bitmap bitmap;
        try {
            bitmap = mCropImage.getCroppedBitmap();
        } catch (Exception ignored) {
            Toast.makeText(this, getString(R.string.general_error_ritaglio_immagine), Toast.LENGTH_SHORT).show();
            return;
        }

        File imageFile = mPhotoGenerator.generateFileFrom(bitmap);
        if (imageFile == null) {
            Toast.makeText(this, getString(R.string.general_error_salvataggio_immagine), Toast.LENGTH_LONG).show();
            return;
        }

        Intent resultData = new Intent();
        resultData.putExtra(Constants.IMAGE_CROP, imageFile.getAbsolutePath());
        setResult(RESULT_OK, resultData);
        finish();
    }

    // SUPPORT

    private PhotoGenerator mPhotoGenerator;
    private Uri mUri;
    private boolean mRatio;
    private int mRatioA;
    private int mRatioB;

    private static final String PARAM_URI = "paramUri";
    private static final String PARAM_RATIO_A = "paramRatioA";
    private static final String PARAM_RATIO_B = "paramRatioB";
    private static final String PARAM_RATIO = "paramRatio";

    // SYSTEM

    public static Intent newIntent(Uri image, int ratioA, int ratioB) {
        Intent intent = new Intent(App.getContext(), Act_ImageCrop.class);
        intent.putExtra(PARAM_URI, image.toString());
        intent.putExtra(PARAM_RATIO_A, ratioA);
        intent.putExtra(PARAM_RATIO_B, ratioB);
        intent.putExtra(PARAM_RATIO, true);
        return intent;
    }

    public static Intent newIntentFreeRatio(Uri image) {
        Intent intent = new Intent(App.getContext(), Act_ImageCrop.class);
        intent.putExtra(PARAM_URI, image.toString());
        intent.putExtra(PARAM_RATIO, false);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            mToolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);

            if (Utils.isLollipop()) {
                ((ViewGroup.MarginLayoutParams) mToolbar.getLayoutParams()).topMargin = Utils.getStatusBarHeight(this);
            }
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_image_crop;
    }

    @Override
    protected void initVariables() {
    }

    @Override
    protected void loadParameters(Bundle extras) {
        if (extras.getString(PARAM_URI) != null)
            mUri = Uri.parse(extras.getString(PARAM_URI));
        mRatioA = extras.getInt(PARAM_RATIO_A);
        mRatioB = extras.getInt(PARAM_RATIO_B);
        mRatio = extras.getBoolean(PARAM_RATIO);
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {
    }

    @Override
    protected void saveInfos(Bundle outState) {
    }

    @Override
    protected void initialize() {
        if (mRatio) {
            mCropImage.setCustomRatio(mRatioA, mRatioB);
        } else {
            mCropImage.setCropMode(CropImageView.CropMode.RATIO_FREE);
        }

        mPhotoGenerator = new PhotoGenerator(this);
        try {
            Bitmap bitmap = mPhotoGenerator.generatePhotoWithValue(mUri);
            if (bitmap != null) {
                mCropImage.setImageBitmap(bitmap);
            } else {
                mCropImage.setImageURI(mUri);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            mCropImage.setImageURI(mUri);
        }
    }
}
