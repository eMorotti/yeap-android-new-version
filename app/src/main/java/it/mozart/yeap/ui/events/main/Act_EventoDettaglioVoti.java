package it.mozart.yeap.ui.events.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.api.services.PollApi;
import it.mozart.yeap.realm.dao.VoteDao;
import it.mozart.yeap.ui.base.BaseActivityWithSyncService;
import it.mozart.yeap.ui.events.main.adapters.VotantiListAdapter;
import it.mozart.yeap.utility.Utils;

public class Act_EventoDettaglioVoti extends BaseActivityWithSyncService {

    @BindView(R.id.layout)
    View mLayout;
    @BindView(R.id.opaque)
    View mOpaqueView;
    @BindView(R.id.recyclerview)
    RecyclerView mRecyclerView;

    @OnClick(R.id.opaque)
    void opaqueClicked() {
        animateOut();
    }

    // SUPPORT

    @Inject
    Realm realm;
    @Inject
    PollApi pollApi;

    private GestureDetector mGestureDetector;
    private String mId;
    private float mOffset;
    private boolean mAnimating;
    private boolean mIsInteressati;

    private static final String PARAM_ID = "paramId";
    private static final String PARAM_IS_INTERESSATI = "paramIsInteressati";

    // SYSTEM

    public static Intent newIntent(String eventoId) {
        Intent intent = new Intent(App.getContext(), Act_EventoDettaglioVoti.class);
        intent.putExtra(PARAM_ID, eventoId);
        intent.putExtra(PARAM_IS_INTERESSATI, false);
        return intent;
    }

    public static Intent newIntentInteressati(String eventoId) {
        Intent intent = new Intent(App.getContext(), Act_EventoDettaglioVoti.class);
        intent.putExtra(PARAM_ID, eventoId);
        intent.putExtra(PARAM_IS_INTERESSATI, true);
        return intent;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_evento_dettaglio_voti;
    }

    @Override
    protected void initVariables() {
        mAnimating = false;
    }

    @Override
    protected void loadParameters(Bundle extras) {
        mId = extras.getString(PARAM_ID);
        mIsInteressati = extras.getBoolean(PARAM_IS_INTERESSATI);
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    public void onBackPressed() {
        animateOut();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {
            if (mLayout.getTranslationY() > mOffset) {
                animateOut();
            } else {
                mLayout.animate().translationY(0).start();
            }
            return true;
        }
        return mGestureDetector.onTouchEvent(event);
    }

    @Override
    protected void initialize() {
        VoteDao voteDao = new VoteDao(realm);
        mOffset = Utils.dpToPx(32, this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        if (mIsInteressati) {
            mRecyclerView.setAdapter(new VotantiListAdapter(this, voteDao.getListVoteInterestEventYes(mId), voteDao.getListVoteInterestEventNo(mId), true));
        } else {
            mRecyclerView.setAdapter(new VotantiListAdapter(this, voteDao.getListVoteEventYes(mId), voteDao.getListVoteEventNo(mId), false));
        }

        mLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                onTouchEvent(event);
                return true;
            }
        });

        animateIn();
        mGestureDetector = new GestureDetector(this, new GestureListener());
    }

    // FUNCTIONS

    private void animateIn() {
        if (mAnimating) {
            return;
        }

        mAnimating = true;

        Animation fadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        fadeIn.setDuration(200);
        mOpaqueView.startAnimation(fadeIn);

        Animation slideIn = AnimationUtils.loadAnimation(this, R.anim.slide_in_bottom);
        if (Utils.isLollipop()) {
            slideIn.setDuration(200);
        } else {
            slideIn.setDuration(300);
        }
        slideIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mAnimating = false;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mLayout.startAnimation(slideIn);
    }

    private void animateOut() {
        if (mAnimating) {
            return;
        }

        mAnimating = true;

        Animation fadeOut = AnimationUtils.loadAnimation(this, R.anim.fade_out);
        fadeOut.setDuration(200);
        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mOpaqueView.setAlpha(0.001f);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mOpaqueView.startAnimation(fadeOut);

        Animation slideOut = AnimationUtils.loadAnimation(this, R.anim.slide_out_bottom);
        slideOut.setDuration(200);
        slideOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayout.setAlpha(0.001f);
                finish();
                overridePendingTransition(0, 0);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mLayout.startAnimation(slideOut);
    }

    // DETECTOR

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            mLayout.setTranslationY(mLayout.getTranslationY() + (e2.getY() - e1.getY()));
            if (mLayout.getTranslationY() < 0) {
                mLayout.setTranslationY(0);
            }
            return super.onScroll(e1, e2, distanceX, distanceY);
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if (velocityY > 0 && velocityY > velocityX) {
                animateOut();
            }
            return super.onFling(e1, e2, velocityX, velocityY);
        }
    }
}
