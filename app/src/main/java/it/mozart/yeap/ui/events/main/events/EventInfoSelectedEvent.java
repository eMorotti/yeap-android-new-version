package it.mozart.yeap.ui.events.main.events;

import android.view.View;

public class EventInfoSelectedEvent {

    private boolean where;
    private View view;

    public EventInfoSelectedEvent(boolean where, View view) {
        this.where = where;
        this.view = view;
    }

    public boolean isWhere() {
        return where;
    }

    public View getView() {
        return view;
    }
}
