package it.mozart.yeap.ui.sondaggi;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.PopupMenu;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.UUID;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.api.models.APIError;
import it.mozart.yeap.api.services.PollApi;
import it.mozart.yeap.events.AddItemToPollEvent;
import it.mozart.yeap.events.ApiCallResultEvent;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.helpers.AnalitycsProvider;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.Poll;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.PollDao;
import it.mozart.yeap.ui.base.BaseActivityWithSyncService;
import it.mozart.yeap.ui.events.main.events.PollItemMoreClickedEvent;
import it.mozart.yeap.ui.events.main.events.PollMoreSelectedEvent;
import it.mozart.yeap.ui.events.main.events.VotePollItemClickedEvent;
import it.mozart.yeap.ui.sondaggi.views.SondaggioLayout;
import it.mozart.yeap.utility.APIErrorUtils;
import it.mozart.yeap.utility.Utils;
import it.mozart.yeap.utility.VoteUtils;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class Act_DettaglioSondaggio extends BaseActivityWithSyncService {

    @BindView(R.id.layout)
    View mLayout;
    @BindView(R.id.opaque)
    View mOpaqueView;
    @BindView(R.id.sondaggio)
    SondaggioLayout mSondaggioLayout;

    @OnClick(R.id.opaque)
    void opaqueClicked() {
        animateOut();
    }

    // SUPPORT

    @Inject
    Realm realm;
    @Inject
    PollApi pollApi;

    private GestureDetector mGestureDetector;
    private PollDao mPollDao;
    private Event mEvento;
    private Poll mSondaggio;
    private String mId;
    private float mOffset;
    private boolean mAnimating;

    private static final String PARAM_ID = "paramId";

    // SYSTEM

    public static Intent newIntent(String sondaggioId) {
        Intent intent = new Intent(App.getContext(), Act_DettaglioSondaggio.class);
        intent.putExtra(PARAM_ID, sondaggioId);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUseBus(true);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_sondaggio_dettaglio;
    }

    @Override
    protected void initVariables() {
        mAnimating = false;
    }

    @Override
    protected void loadParameters(Bundle extras) {
        mId = extras.getString(PARAM_ID);
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mSondaggio != null) {
            mSondaggio.removeAllChangeListeners();
        }
        if (mEvento != null) {
            mEvento.removeAllChangeListeners();
        }
    }

    @Override
    public void onBackPressed() {
        animateOut();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {
            if (mLayout.getTranslationY() > mOffset) {
                animateOut();
            } else {
                mLayout.animate().translationY(0).start();
            }
            return true;
        }
        return mGestureDetector.onTouchEvent(event);
    }

    @Override
    protected void initialize() {
        mPollDao = new PollDao(realm);
        final EventDao eventDao = new EventDao(realm);
        mSondaggio = mPollDao.getPoll(mId);
        if (mSondaggio == null) {
            Toast.makeText(this, getString(R.string.general_error_sondaggio_non_presente), Toast.LENGTH_LONG).show();
            finish();
        } else {
            mOffset = Utils.dpToPx(32, this);

            mEvento = eventDao.getEvent(mSondaggio.getEventUuid());
            if (mEvento == null) {
                Toast.makeText(this, getString(R.string.general_error_evento_non_presente), Toast.LENGTH_LONG).show();
                finish();
            } else {
                setToolbarTitle(mEvento.getTitle());
                setToolbarSubtitle(getString(R.string.sondaggio_dettaglio_subtitle));

                mSondaggioLayout.bind(mSondaggio, mEvento);

                mSondaggio.addChangeListener(new RealmChangeListener<Poll>() {
                    @Override
                    public void onChange(@NonNull Poll element) {
                        if (element.isValid() && mEvento != null && mEvento.isValid()) {
                            mSondaggioLayout.bind(element, mEvento);
                        }
                    }
                });
                mEvento.addChangeListener(new RealmChangeListener<Event>() {
                    @Override
                    public void onChange(@NonNull Event element) {
                        if (element.isValid() && mSondaggio != null && mSondaggio.isValid()) {
                            mSondaggioLayout.bind(mSondaggio, element);
                        }
                    }
                });
                mSondaggioLayout.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        onTouchEvent(event);
                        return true;
                    }
                });

                animateIn();
                mGestureDetector = new GestureDetector(this, new GestureListener());
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(VotePollItemClickedEvent event) {
        AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "votePollItem", "poll");
        VoteUtils.votePollItem(this, realm, event.getSondaggioVoceId(), event.isChecked());
    }

    @Subscribe
    public void onEvent(PollItemMoreClickedEvent event) {
        AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "viewPollItem", "poll");
        startActivity(Act_DettaglioSondaggioVoci.newIntent(event.getSondaggioId(), event.getSondaggioVoceId()));
        overridePendingTransition(0, 0);
    }

    @Subscribe
    public void onEvent(AddItemToPollEvent event) {
        AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "createPollItem", "poll");
        startActivity(Act_AggiungiVoceSondaggio.newIntent(event.getSondaggioId()));
        overridePendingTransition(R.anim.slide_in_bottom_slow, R.anim.fade_out);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ApiCallResultEvent event) {
        if (event.getRequestId().equals(getRequestId())) {
            cancelTimer();
            getDynamicView().hide();
            finish();
        }
    }

    @Subscribe
    public void onEvent(PollMoreSelectedEvent event) {
        final Poll sondaggio = mPollDao.getPoll(event.getId());
        if (sondaggio == null) {
            return;
        }

        AnalitycsProvider.getInstance().logCustom(mFirebaseAnalytics, "dotsPoll");

        PopupMenu popupMenu = new PopupMenu(this, event.getView());
        popupMenu.getMenuInflater().inflate(R.menu.menu_sondaggio_dettaglio, popupMenu.getMenu());
        if (!sondaggio.getCreatorUuid().equals(AccountProvider.getInstance().getAccountId()) && !mEvento.getAdminUuid().equals(AccountProvider.getInstance().getAccountId())) {
            MenuItem eliminaSondaggio = popupMenu.getMenu().findItem(R.id.menu_delete);
            eliminaSondaggio.setEnabled(false);
        }
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_delete:
                        AnalitycsProvider.getInstance().logCustomType(mFirebaseAnalytics, "dotsPollAction", "delete");
                        new MaterialDialog.Builder(Act_DettaglioSondaggio.this)
                                .title(getString(R.string.sondaggio_popup_elimina_titolo))
                                .content(getString(R.string.sondaggio_popup_elimina_messaggio))
                                .positiveText(getString(R.string.general_elimina))
                                .negativeText(getString(R.string.general_annulla))
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        setRequestId(UUID.randomUUID().toString());
                                        getDynamicView().showLoading();
                                        registerSubscription(pollApi.deletePoll(getRequestId(), mSondaggio.getEventUuid(), mSondaggio.getUuid())
                                                .subscribeOn(Schedulers.newThread())
                                                .observeOn(AndroidSchedulers.mainThread())
                                                .subscribe(new Action1<Object>() {
                                                    @Override
                                                    public void call(Object obj) {
                                                        startTimer();
                                                    }
                                                }, new Action1<Throwable>() {
                                                    @Override
                                                    public void call(Throwable throwable) {
                                                        getDynamicView().hide();

                                                        APIError error = APIErrorUtils.parseError(Act_DettaglioSondaggio.this, throwable);
                                                        Toast.makeText(Act_DettaglioSondaggio.this, error.message(), Toast.LENGTH_SHORT).show();
                                                    }
                                                }));
                                    }
                                })
                                .show();
                        break;
                }
                return true;
            }
        });

        popupMenu.show();
    }

    // FUNCTIONS

    private void animateIn() {
        if (mAnimating) {
            return;
        }

        mAnimating = true;

        Animation fadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        fadeIn.setDuration(200);
        mOpaqueView.startAnimation(fadeIn);

        Animation slideIn = AnimationUtils.loadAnimation(this, R.anim.slide_in_bottom);
        if (Utils.isLollipop()) {
            slideIn.setDuration(200);
        } else {
            slideIn.setDuration(300);
        }
        slideIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mAnimating = false;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mLayout.startAnimation(slideIn);
    }

    private void animateOut() {
        if (mAnimating) {
            return;
        }

        mAnimating = true;

        Animation fadeOut = AnimationUtils.loadAnimation(this, R.anim.fade_out);
        fadeOut.setDuration(200);
        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mOpaqueView.setAlpha(0.001f);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mOpaqueView.startAnimation(fadeOut);

        Animation slideOut = AnimationUtils.loadAnimation(this, R.anim.slide_out_bottom);
        slideOut.setDuration(200);
        slideOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayout.setAlpha(0.001f);
                finish();
                overridePendingTransition(0, 0);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mLayout.startAnimation(slideOut);
    }

    // DETECTOR

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            mLayout.setTranslationY(mLayout.getTranslationY() + (e2.getY() - e1.getY()));
            if (mLayout.getTranslationY() < 0) {
                mLayout.setTranslationY(0);
            }
            return super.onScroll(e1, e2, distanceX, distanceY);
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if (velocityY > 0 && velocityY > velocityX) {
                animateOut();
            }
            return super.onFling(e1, e2, velocityX, velocityY);
        }
    }
}
