package it.mozart.yeap.ui.events.main.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import it.mozart.yeap.ui.events.main.Frg_EventoMainChat;
import it.mozart.yeap.ui.events.main.Frg_EventoMainInfo;
import it.mozart.yeap.ui.events.main.Frg_EventoMainInvitati;
import it.mozart.yeap.ui.events.main.Frg_EventoMainSondaggi;
import it.mozart.yeap.ui.views.SmartPagerAdapter;

public class EventoMainPagerAdapter extends SmartPagerAdapter {

    private Context mContext;
    private String mAttivitaId;

    public EventoMainPagerAdapter(Context context, FragmentManager fragmentManager, String id) {
        super(fragmentManager);
        mContext = context;
        mAttivitaId = id;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return Frg_EventoMainInfo.newInstance(mAttivitaId);
            case 1:
                return Frg_EventoMainChat.newInstance(mAttivitaId);
            case 2:
                return Frg_EventoMainSondaggi.newInstance(mAttivitaId);
            default:
                return Frg_EventoMainInvitati.newInstance(mAttivitaId);
        }
    }

    @Override
    public int getCount() {
        return 4;
    }
}
