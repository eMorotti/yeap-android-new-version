package it.mozart.yeap.ui.home;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Arrays;
import java.util.Date;
import java.util.UUID;

import javax.inject.Inject;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.api.models.APIError;
import it.mozart.yeap.api.services.EventApi;
import it.mozart.yeap.events.ApiCallResultEvent;
import it.mozart.yeap.events.EventLongSelectedEvent;
import it.mozart.yeap.events.EventSelectedEvent;
import it.mozart.yeap.events.GroupSelectedEvent;
import it.mozart.yeap.events.ImageSelectedEvent;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.helpers.AnalitycsProvider;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.Vote;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.GroupDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.realm.dao.VoteDao;
import it.mozart.yeap.services.notifications.RegistrationIntentService;
import it.mozart.yeap.services.realmJob.EventDeleteJob;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.services.voti.VotiService;
import it.mozart.yeap.ui.base.BaseActivityWithSyncService;
import it.mozart.yeap.ui.events.creation.Act_EventoCrea;
import it.mozart.yeap.ui.events.main.Act_EventoMain;
import it.mozart.yeap.ui.events.main.events.EventDeleteEvent;
import it.mozart.yeap.ui.events.main.events.VoteEventClickedEvent;
import it.mozart.yeap.ui.events.main.events.VoteInterestEventClickedEvent;
import it.mozart.yeap.ui.groups.creation.Act_GruppoCrea;
import it.mozart.yeap.ui.groups.main.Act_GruppoMain;
import it.mozart.yeap.ui.home.adapters.HomePagerAdapter;
import it.mozart.yeap.ui.home.events.EventsUpdatedEvent;
import it.mozart.yeap.ui.home.events.GroupsUpdatedEvent;
import it.mozart.yeap.ui.profile.Act_Profilo;
import it.mozart.yeap.ui.settings.Act_Settings;
import it.mozart.yeap.ui.support.Act_Contacts;
import it.mozart.yeap.ui.support.Act_ImageDetail;
import it.mozart.yeap.ui.views.AutoRefreshImageView;
import it.mozart.yeap.ui.views.BorderFabLayout;
import it.mozart.yeap.utility.APIErrorUtils;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.Utils;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

@SuppressWarnings("ALL")
public class Act_Home extends BaseActivityWithSyncService {

    @BindView(R.id.smooth_app_bar_layout)
    AppBarLayout mAppBarLayout;
    @BindView(R.id.viewpager)
    ViewPager mViewPager;
    //    @BindView(R.id.tablayout)
//    MyTabLayout mTabLayout;
    @BindView(R.id.fab_layout)
    BorderFabLayout mFabLayout;

    @BindView(R.id.contacts_bg)
    View mContactsBg;
    @BindView(R.id.contacts)
    View mContacts;
    @BindView(R.id.settings_bg)
    View mSettingsBg;
    @BindView(R.id.settings)
    View mSettings;
    @BindView(R.id.avatar)
    AutoRefreshImageView mAvatar;
    @BindView(R.id.logo)
    ImageView mLogo;
    @BindView(R.id.name)
    TextView mName;

    @BindView(R.id.reveal_layout)
    View mRevealLayout;

    @OnClick(R.id.avatar)
    void avatarClicked() {
        if (mAvatar.getAlpha() > 0.5f) {
            AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "viewMyProfile", "home");
            startActivity(Act_Profilo.newIntent(mUser.getUuid()));
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @OnClick(R.id.fab_layout)
    void fabLayoutClicked() {
        animateReveal();
    }

    @OnClick(R.id.contacts_btn)
    void contactsClicked() {
        AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "viewUsers", "home");
        startActivity(new Intent(this, Act_Contacts.class));
    }

    @OnClick(R.id.settings_btn)
    void settingsClicked() {
        AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "viewSettings", "home");
        startActivity(new Intent(this, Act_Settings.class));
    }

    // SUPPORT

    @Inject
    Realm realm;
    @Inject
    NotificationsHelper notifications;
    @Inject
    EventApi eventApi;

    @BindColor(R.color.colorGradientEnd)
    int mColorGradientEnd;

    private User mUser;
    private UserDao mUserDao;
    private EventDao mEventDao;
    private GroupDao mGroupDao;
    private HomePagerAdapter mAdapter;
    private String mPropostaConfermataId;
    private View mEventView;
    private String mEventTitle;
    private String mEventId;
    private boolean mIgnoreOffsetChange;
    private boolean mIgnoreFabAnimation;
    private boolean mAnimationInProgress;
    private boolean mAnimated;
    private boolean mShowContextForLongClick;
    private int mVoteType;
    private int mPage;
    private float mStatusBarHeight;
    private float mCX;
    private float mCY;
    private float mContactsX;
    private float mSettingsX;
    private float mNomeY;
    private float mDiff;

    private float mWidth;
    private float mOpenY;
    private float mCloseY;
    private float mCloseMiddleY;
    private float mLogoOffsetY;

    private static final String PARAM_PAGE = "paramPage";

    private static final String SAVED_PAGE = "savedPage";

    private static final int ACTIVITY_CODE = 6005;

    // SYSTEM

    public static Intent newIntent(int page) {
        Intent intent = new Intent(App.getContext(), Act_Home.class);
        intent.putExtra(PARAM_PAGE, page);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setIgnoreToolbar(true);
        super.onCreate(savedInstanceState);
        setUseBus(true);

        if (!isSavedInfoPresent()) {
            startService(new Intent(this, RegistrationIntentService.class));
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_home;
    }

    @Override
    protected void initVariables() {
        mShowContextForLongClick = false;
        mStatusBarHeight = (Utils.isLollipop() ? Utils.getStatusBarHeight(this) : 0);
        mAnimationInProgress = false;
        mAnimated = false;
        mIgnoreOffsetChange = true;
        mIgnoreFabAnimation = true;
        mWidth = Utils.screenWidth(this);
        mOpenY = Utils.dpToPx(48, this);
        mCloseY = Utils.dpToPx(10, this);
        mCloseMiddleY = Utils.dpToPx(6f, this) + mStatusBarHeight;
        mLogoOffsetY = Utils.dpToPx(24f, this);
        mPage = 0;
    }

    @Override
    protected void loadParameters(Bundle extras) {
//        mPage = extras.getInt(PARAM_PAGE, 0);
        mPage = 0;
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {
        mPage = savedInstanceState.getInt(SAVED_PAGE, mPage);
    }

    @Override
    protected void saveInfos(Bundle outState) {
        mAdapter.onSaveInstanceState(outState);
        if (mViewPager != null)
            outState.putInt(SAVED_PAGE, mViewPager.getCurrentItem());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Bundle extras = intent.getExtras();
        if (extras != null) {
            mPage = extras.getInt(PARAM_PAGE, 0);
            if (mViewPager != null)
                mViewPager.setCurrentItem(mPage, true);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mUser != null) {
            mUser.removeAllChangeListeners();
        }
    }

    @Override
    public void onBackPressed() {
        if (mViewPager.getCurrentItem() == 1) {
            mViewPager.setCurrentItem(0, true);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.clear();
        menu.setHeaderTitle(mEventTitle);
        if (mShowContextForLongClick) {
            Event evento = mEventDao.getEvent(mEventId);
            if (evento.isMyselfPresent()) {
                menu.add(Menu.NONE, 3, Menu.NONE, getString(R.string.menu_esci_da_evento));
            }
            menu.add(Menu.NONE, 4, Menu.NONE, getString(R.string.menu_elimina_attivita));
        } else {
            menu.add(Menu.NONE, 0, Menu.NONE, getString(R.string.home_event_non_interessato));
            menu.add(Menu.NONE, 1, Menu.NONE, getString(R.string.home_event_abbandona));
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                if (mEventView != null) {
                    View loadingView = mEventView.findViewById(R.id.loading_view_interest);
                    if (loadingView != null) {
                        loadingView.setVisibility(View.VISIBLE);
                    }
                }
                voteInterestEvent(mEventId, mVoteType);
                break;
            case 1:
                getDynamicView().showLoading();
                setRequestId(UUID.randomUUID().toString());
                eventApi.exit(getRequestId(), mEventId)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<Object>() {
                            @Override
                            public void call(Object obj) {

                            }
                        }, new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                getDynamicView().hide();

                                APIError error = APIErrorUtils.parseError(Act_Home.this, throwable);
                                Toast.makeText(Act_Home.this, error.message(), Toast.LENGTH_SHORT).show();
                            }
                        });
                break;
            case 3:
                AnalitycsProvider.getInstance().logCustomType(mFirebaseAnalytics, "dotsActivityAction", "abandon");
                new MaterialDialog.Builder(this)
                        .title(getString(R.string.main_evento_esci_title))
                        .content(getString(R.string.main_evento_esci_content))
                        .positiveText(getString(R.string.general_abbandona))
                        .negativeText(getString(R.string.general_annulla))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                getDynamicView().showLoading();
                                setRequestId(UUID.randomUUID().toString());
                                eventApi.exit(getRequestId(), mEventId)
                                        .subscribeOn(Schedulers.newThread())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new Action1<Object>() {
                                            @Override
                                            public void call(Object obj) {
                                            }
                                        }, new Action1<Throwable>() {
                                            @Override
                                            public void call(Throwable throwable) {
                                                getDynamicView().hide();

                                                APIError error = APIErrorUtils.parseError(Act_Home.this, throwable);
                                                Toast.makeText(Act_Home.this, error.message(), Toast.LENGTH_SHORT).show();
                                            }
                                        });
                            }
                        }).show();
                break;
            case 4:
                AnalitycsProvider.getInstance().logCustomType(mFirebaseAnalytics, "dotsActivityAction", "delete");
                new MaterialDialog.Builder(this)
                        .title(getString(R.string.main_evento_elimina_title))
                        .content(getString(R.string.main_evento_elimina_content))
                        .positiveText(getString(R.string.general_elimina))
                        .negativeText(getString(R.string.general_annulla))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                getDynamicView().showLoading();
                                App.jobManager().addJobInBackground(new EventDeleteJob(mEventId));
                            }
                        })
                        .show();
                break;
        }

        return super.onContextItemSelected(item);
    }

    @Override
    protected void initialize() {
        mEventDao = new EventDao(realm);
        mGroupDao = new GroupDao(realm);
        mUserDao = new UserDao(realm);

        mUser = mUserDao.getUser(AccountProvider.getInstance().getAccountId());

        // Init ViewPager
        mAdapter = new HomePagerAdapter(getSupportFragmentManager());
        mAdapter.onRestoreInstanceState(mSavedInstanceState);
        mAdapter.addFragment(getString(R.string.home_evento), new Frg_HomeEvents());
//        mAdapter.addFragment(getString(R.string.home_gruppi), new Frg_HomeGroups());
        mViewPager.setAdapter(mAdapter);
//        mTabLayout.setWithRoundBorder(true);
//        mTabLayout.setupWithViewPager(mViewPager);
//        mTabLayout.addOnTabSelectedListener(new MyTabLayout.OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(MyTabLayout.Tab tab) {
//                if (tab.getCustomView() != null) {
//                    tab.getCustomView().findViewById(R.id.tab_text).setAlpha(1f);
//                }
//            }
//
//            @Override
//            public void onTabUnselected(MyTabLayout.Tab tab) {
//                if (tab.getCustomView() != null) {
//                    tab.getCustomView().findViewById(R.id.tab_text).setAlpha(0.6f);
//                }
//            }
//
//            @Override
//            public void onTabReselected(MyTabLayout.Tab tab) {
//
//            }
//        });
        mViewPager.setCurrentItem(mPage);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (mIgnoreFabAnimation) {
                    mIgnoreFabAnimation = false;
                    return;
                }

                mFabLayout.updateAnimationValue(position, positionOffset);
            }

            @Override
            public void onPageSelected(final int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

//        updateTab(0, true, mEventDao.getNumberEventsWithUpdates());
//        updateTab(1, true, mGroupDao.getNumberGroupsWithUpdates());

        // Init
        SparseArray<String> textResources = new SparseArray<>();
        textResources.put(0, getString(R.string.system_fab_activity));
        textResources.put(1, getString(R.string.system_fab_group));
        mFabLayout.setTextResources(textResources);
        mFabLayout.setPositionTypes(Arrays.asList(BorderFabLayout.POSITION_TYPE.DEFAULT, BorderFabLayout.POSITION_TYPE.DEFAULT));

        mFabLayout.updateAnimationValue(mViewPager.getCurrentItem(), 0);
        mFabLayout.forceHide();
        mFabLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                mFabLayout.show();
            }
        }, 500);

        if (mUser != null) {
            mAvatar.bindThumb(mUser.getUuid());
        }
        // Reset Offset
        mAppBarLayout.post(new Runnable() {
            @Override
            public void run() {
                mAppBarLayout.setExpanded(true);
//                mAppBarLayout.syncOffset(0);
                mIgnoreOffsetChange = false;
            }
        });

        // Init Transition
        mContacts.setX(Utils.screenWidth(Act_Home.this) / 4 - Utils.dpToPx(45, Act_Home.this) - 0.5f);
        mSettings.setX(Utils.screenWidth(Act_Home.this) / 4 * 3 + 0.5f);
        mName.setY(mAvatar.getY() + Utils.dpToPx(117, this) + mStatusBarHeight);
        mLogo.setY(mName.getY() + Utils.dpToPx(4, this));
        mLogo.setVisibility(View.VISIBLE);
        mDiff = Utils.dpToPx(136, this);

        mContactsX = mContacts.getX();
        mSettingsX = mSettings.getX();
        mNomeY = mName.getY();

        // Listener
        mAppBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                // Se non ha ancora sincronizzato l'appBarLayout ignoro
                if (!mIgnoreOffsetChange) {
                    manageOffsetChange(mContactsX, mSettingsX, mSettings.getWidth(), mNomeY, verticalOffset, mDiff);
                }
            }
        });

        if (mUser != null) {
            mName.setText(String.format("~%s", mUser.getNickname()));

            mUser.addChangeListener(new RealmChangeListener<User>() {
                @Override
                public void onChange(User element) {
                    if (element != null && element.getNickname() != null) {
                        mName.setText(element.getNickname());
                    }
                }
            });
        }

//        if (mPage == 0) {
//            if (mTabLayout.getTabAt(1).getCustomView() != null) {
//                mTabLayout.getTabAt(1).getCustomView().findViewById(R.id.tab_text).setAlpha(0.6f);
//            }
//        } else {
//            if (mTabLayout.getTabAt(0).getCustomView() != null) {
//                mTabLayout.getTabAt(0).getCustomView().findViewById(R.id.tab_text).setAlpha(0.6f);
//            }
//        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ACTIVITY_CODE) {
            if (resultCode != Activity.RESULT_CANCELED)
                mAnimated = false;
            animateHide();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ApiCallResultEvent event) {
        if (event.getRequestId().equals(getRequestId())) {
            cancelTimer();
            getDynamicView().hide();
        }
    }

    @Subscribe
    public void onEvent(EventSelectedEvent event) {
        if (!mAnimationInProgress) {
            notifications.cancelAllNotification(this);
            startActivity(Act_EventoMain.newIntent(event.getId()));
        }
    }

    @Subscribe
    public void onEvent(EventLongSelectedEvent event) {
        mShowContextForLongClick = true;
        mEventId = event.getId();
        mEventTitle = event.getTitle();
        registerForContextMenu(event.getView());
        openContextMenu(event.getView());
        unregisterForContextMenu(event.getView());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventDeleteEvent event) {
        getDynamicView().hide();
        if (!event.isSuccess()) {
            Toast.makeText(this, getString(R.string.general_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onEvent(GroupSelectedEvent event) {
        if (!mAnimationInProgress) {
            notifications.cancelAllNotification(this);
            startActivity(Act_GruppoMain.newIntent(event.getId()));
        }
    }

    // Aggiorno il margine superiore e sinistro per allinearlo nel modo giusto (il superiore nen caso non sia lollipop)
    private void updateMarginForVotaSelected(View view, float posY, float posX) {
        ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).topMargin = (int) posY - (Utils.isLollipop() ? 0 : Utils.getStatusBarHeight(this));
        ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).leftMargin = (int) posX;
    }

    @Subscribe(sticky = true)
    public void onEvent(EventsUpdatedEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
//        updateTab(0, false, mEventDao.getNumberEventsWithUpdates());
    }

    @Subscribe(sticky = true)
    public void onEvent(GroupsUpdatedEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
//        updateTab(1, false, mGroupDao.getNumberGroupsWithUpdates());
    }

    @Subscribe
    public void onEvent(ImageSelectedEvent event) {
        int placeholder = event.isEvent() ? R.drawable.avatar_attivita : R.drawable.avatar_gruppo;
        Intent intent = Act_ImageDetail.newIntentSharedAnimation(event.getPath(), event.getView().getWidth() / 2, placeholder);
        if (Utils.isLollipop()) {
            Pair<View, String> p1 = Pair.create(event.getView(), getString(R.string.transition_image));
            //noinspection unchecked
            ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(this, p1);
            startActivity(intent, optionsCompat.toBundle());
        } else {
            startActivity(intent);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(VoteInterestEventClickedEvent event) {
        if (event.getType() == Constants.VOTO_NO_INTERESSE) {
            showPopupNonInterested(event.getView(), event.getEventTitle(), event.getEventId(), event.getType());
        } else {
            voteInterestEvent(event.getEventId(), event.getType());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(VoteEventClickedEvent event) {
        voteEvent(event.getEventId(), event.getType());
    }

    // FUNCTIONS

    /**
     * Gestisco l'aggiornamento delle view causato dal cambiamento dell'offset dell'appBarLayout
     *
     * @param notificationsX posizione X delle notifiche
     * @param settingsX      posizione X delle impostazioni
     * @param settingsWidth  larghezza icona delle impostazioni
     * @param nomeY          posizione Y del nome
     * @param offset         offset dell'appBarLayout
     * @param diff           differanza di dimensione tra lo stato aperto e chiuso dell'appBarLayout
     */
    private void manageOffsetChange(float notificationsX, float settingsX, int settingsWidth, float nomeY, int offset, float diff) {
        updateAvatar(offset, diff);
        updateContacts(notificationsX, offset, diff);
        updateSettings(settingsX, settingsWidth, offset, diff);
        updateLogoAndName(nomeY, offset, diff);
    }

    /**
     * Aggiornamento layout notifiche
     *
     * @param contactsX nuova posizione X dei contatti
     * @param offset    offset dell'appBarLayout
     * @param diff      differanza di dimensione tra lo stato aperto e chiuso dell'appBarLayout
     */
    private void updateContacts(float contactsX, int offset, float diff) {
        float multiplier = 1.2f;
        float alpha = Math.max(0f, 1f - Math.abs(offset / diff) * multiplier);
        float x = Utils.toRange(-offset, 0f, diff, contactsX, getResources().getDimension(R.dimen.standard_dimension_16dp));
        float y = Utils.toRange(-offset, 0f, diff, mOpenY, mCloseY);
        mContacts.setX(x);
        mContacts.setY(y);
        mContactsBg.setAlpha(alpha);
    }

    /**
     * Aggiornamento layout impostazioni
     *
     * @param settingsX nuova posizione X delle impostazioni
     * @param offset    offset dell'appBarLayout
     * @param diff      differanza di dimensione tra lo stato aperto e chiuso dell'appBarLayout
     */
    private void updateSettings(float settingsX, int width, int offset, float diff) {
        float multiplier = 1.2f;
        float alpha = Math.max(0f, 1f - Math.abs(offset / diff) * multiplier);
        float x = Utils.toRange(-offset, 0f, diff, settingsX, mWidth - getResources().getDimension(R.dimen.standard_dimension_16dp) - width);
        float y = Utils.toRange(-offset, 0f, diff, mOpenY, mCloseY);
        mSettings.setX(x);
        mSettings.setY(y);
        mSettingsBg.setAlpha(alpha);
    }

    /**
     * Aggiornamento layout avatar
     *
     * @param offset offset dell'appBarLayout
     * @param diff   differanza di dimensione tra lo stato aperto e chiuso dell'appBarLayout
     */
    private void updateAvatar(int offset, float diff) {
        float multiplier = 1.5f;
        float alpha = Math.max(0f, 1f - Math.abs(offset / diff) * multiplier);
        mAvatar.setAlpha(alpha);
        mAvatar.setTranslationY(offset / multiplier);
    }

    /**
     * Aggiornamento layout logo e nome
     *
     * @param logoY  nuova posizione Y del nome
     * @param offset offset dell'appBarLayout
     * @param diff   differanza di dimensione tra lo stato aperto e chiuso dell'appBarLayout
     */
    private void updateLogoAndName(float logoY, int offset, float diff) {
        float y = Utils.toRange(-offset, 0f, diff, logoY, mCloseMiddleY);
        float scale = Utils.toRange(-offset, 0f, diff, 1f, 0.8f);
        float translateScale = Utils.toRange(-offset, 0f, diff, 1f, 0f);
        float alpha = Math.max(0f, 1f - Math.abs(offset / diff) * 1.5f);
        mLogo.setY(y + mLogoOffsetY * translateScale);
        mLogo.setScaleX(scale);
        mLogo.setScaleY(scale);
        mName.setY(y);
        mName.setAlpha(alpha);
    }

    /**
     * Esegue l'animazione di reveal al click del fab
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void animateReveal() {
        if (mAnimationInProgress) {
            return;
        }

        if (!Utils.isLollipop()) {
            if (mViewPager.getCurrentItem() == 0) {
                startActivityForResult(new Intent(Act_Home.this, Act_EventoCrea.class), ACTIVITY_CODE);
            } else {
                startActivityForResult(new Intent(Act_Home.this, Act_GruppoCrea.class), ACTIVITY_CODE);
            }
            overridePendingTransition(R.anim.slide_in_bottom_slow, R.anim.fade_out);
            return;
        }

        if (mViewPager.getCurrentItem() == 0) {
            AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "createActivity", "home");
        } else {
            AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "createGroup", "home");
        }

        mAnimationInProgress = true;
        mAnimated = true;
        // Animo la traslazione del fab
        mFabLayout.animate().translationY(-200).setDuration(250).withEndAction(new Runnable() {
            @Override
            public void run() {
                mRevealLayout.setVisibility(View.VISIBLE);
            }
        }).start();

        // Calcolo posizione da cui far partire l'animazione di reveal
        mCX = (int) (mFabLayout.getX() + mFabLayout.getWidth() / 2);
        mCY = (int) (mFabLayout.getY() + mFabLayout.getHeight() / 2) - 200;
        // Radius finale
        float finalRadius = (float) Math.hypot(mCX, mCY);

        //Creo l'animazione
        AnimatorSet animatorSet = new AnimatorSet();
        Animator animator = ViewAnimationUtils.createCircularReveal(mRevealLayout, (int) mCX, (int) mCY, mFabLayout.getFabButton().getWidth() / 2, finalRadius);
        ValueAnimator valueAnimator = ObjectAnimator.ofInt(mRevealLayout, "backgroundColor", Color.WHITE, Color.WHITE, mColorGradientEnd);
        valueAnimator.setEvaluator(new ArgbEvaluator());
        animatorSet.playTogether(animator, valueAnimator);
        animatorSet.setStartDelay(200);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mFabLayout.hide(new BorderFabLayout.OnVisibilityChangeListener() {
                    @Override
                    public void onHidden() {
                        mFabLayout.setClickAnimationOffset(0f);
                    }

                    @Override
                    public void onShown() {

                    }
                });
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mAnimationInProgress = false;
                if (mViewPager.getCurrentItem() == 0) {
                    startActivityForResult(new Intent(Act_Home.this, Act_EventoCrea.class), ACTIVITY_CODE);
                } else {
                    startActivityForResult(new Intent(Act_Home.this, Act_GruppoCrea.class), ACTIVITY_CODE);
                }
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });
        animatorSet.start();
    }

    /**
     * Inverte l'animazione di reveal al ritorno all'activity
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void animateHide() {
        if (!Utils.isLollipop())
            return;

        if (mAnimated) {
            mAnimated = false;
            // Radius iniziale
            float initialRadius = (float) Math.hypot(mCX, mCY);

            //Creo l'animazione
            AnimatorSet animatorSet = new AnimatorSet();
            Animator animator = ViewAnimationUtils.createCircularReveal(mRevealLayout, (int) mCX, (int) mCY, initialRadius, mFabLayout.getFabButton().getWidth() / 2);
            ValueAnimator valueAnimator = ObjectAnimator.ofInt(mRevealLayout, "backgroundColor", mColorGradientEnd, mColorGradientEnd, mColorGradientEnd, mColorGradientEnd, Color.WHITE);
            valueAnimator.setEvaluator(new ArgbEvaluator());
            animatorSet.playTogether(animator, valueAnimator);
            animatorSet.setStartDelay(100);
            animatorSet.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    mRevealLayout.setVisibility(View.INVISIBLE);
                    mFabLayout.show();
                    // Animo la traslazione del fab
                    mFabLayout.animate().translationY(0).setDuration(250).start();
                }
            });
            animatorSet.start();
        } else {
            mRevealLayout.setVisibility(View.INVISIBLE);
            mFabLayout.setTranslationY(0);
            mFabLayout.show();
        }
    }

    /**
     * Aggiorno il tab
     *
     * @param position        posizione del tab
     * @param isCreate        creo il tab
     * @param numNotification numero notifiche
     */
//    @SuppressWarnings("ConstantConditions")
//    @SuppressLint("InflateParams")
//    private void updateTab(int position, boolean isCreate, long numNotification) {
//        View view;
//        if (isCreate) {
//            view = LayoutInflater.from(this).inflate(R.layout.system_tab_home, null);
//            view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//
//            TextView text = view.findViewById(R.id.tab_text);
//            switch (position) {
//                case 0:
//                    text.setText(getString(R.string.home_evento));
//                    break;
//                default:
//                    text.setText(getString(R.string.home_gruppi));
//                    break;
//            }
//        } else {
//            view = mTabLayout.getTabAt(position).getCustomView();
//        }
//
//        AnimatedTextView badge = view.findViewById(R.id.tab_badge);
//        if (isCreate) {
////            badge.setIgnoreMeasureExactly(false);
//            badge.setTextColor(ContextCompat.getColor(this, R.color.textWhiteSecondary));
//            badge.setTextSize(10);
//        }
//        badge.setVisibility(numNotification == 0 ? View.GONE : View.VISIBLE);
//        badge.setValue(numNotification);
//
//        mTabLayout.getTabAt(position).setCustomView(view);
//    }

    /**
     * Voto l'interesse per la proposta
     *
     * @param type tipo del voto
     */
    private void voteInterestEvent(final String eventId, final int type) {
        final VoteDao voteDao = new VoteDao(realm);
        final UserDao userDao = new UserDao(realm);
        final EventDao eventDao = new EventDao(realm);

        try {
            Vote voto = voteDao.getVoteInterestEventFromUser(eventId, AccountProvider.getInstance().getAccountId());
            if (voto == null) {
                voto = new Vote();
                voto.setUuid(UUID.randomUUID().toString());
                voto.setCreatedAt(new Date().getTime());
                voto.setCreator(userDao.getUser(AccountProvider.getInstance().getAccountId()));
                voto.setLoading(true);
                voto.setEventUuid(eventId);
                voto.setType(type);
                voto.setVoteType(Constants.VOTE_TYPE_INTEREST);

                final Vote finalVoto = voto;
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(@NonNull Realm realm) {
                        Vote newVoto = voteDao.createMyVoteInterestEvent(finalVoto, eventDao.getEvent(eventId));
                        VotiService.sendVote(Act_Home.this, newVoto.getUuid(), eventId);
                    }
                });

                AnalitycsProvider.getInstance().logCustom(mFirebaseAnalytics, "interestActivity");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(this, getString(R.string.general_error_realm), Toast.LENGTH_LONG).show();
        }
    }

    private void showPopupNonInterested(View view, String title, String eventId, int voteType) {
        mEventView = view;
        mEventTitle = title;
        mEventId = eventId;
        mVoteType = voteType;
        mShowContextForLongClick = false;
        registerForContextMenu(view);
        openContextMenu(view);
        unregisterForContextMenu(view);
    }

    /**
     * Voto la proposta
     *
     * @param type tipo del voto
     */
    private void voteEvent(final String eventId, final int type) {
        final VoteDao voteDao = new VoteDao(realm);
        final UserDao userDao = new UserDao(realm);
        final EventDao eventDao = new EventDao(realm);

        try {
            Vote voto = voteDao.getVoteEventFromUser(eventId, AccountProvider.getInstance().getAccountId());
            if (voto == null) {
                voto = new Vote();
                voto.setUuid(UUID.randomUUID().toString());
                voto.setCreatedAt(new Date().getTime());
                voto.setCreator(userDao.getUser(AccountProvider.getInstance().getAccountId()));
                voto.setLoading(true);
                voto.setEventUuid(eventId);
                voto.setType(type);
                voto.setVoteType(Constants.VOTE_TYPE_EVENT);

                final Vote finalVoto = voto;
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(@NonNull Realm realm) {
                        Vote newVoto = voteDao.createMyVoteEvent(finalVoto, eventDao.getEvent(eventId));
                        VotiService.sendVote(Act_Home.this, newVoto.getUuid(), eventId);
                    }
                });

                AnalitycsProvider.getInstance().logCustom(mFirebaseAnalytics, "voteActivity");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(this, getString(R.string.general_error_realm), Toast.LENGTH_LONG).show();
        }
    }
}