package it.mozart.yeap.ui.events.main.events;

import android.view.View;

public class PollMoreSelectedEvent {

    private String id;
    private View view;

    public PollMoreSelectedEvent(String id, View view) {
        this.id = id;
        this.view = view;
    }

    public String getId() {
        return id;
    }

    public View getView() {
        return view;
    }
}
