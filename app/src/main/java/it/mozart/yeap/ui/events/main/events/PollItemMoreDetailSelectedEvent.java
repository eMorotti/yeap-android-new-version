package it.mozart.yeap.ui.events.main.events;

import android.view.View;

public class PollItemMoreDetailSelectedEvent {

    private String id;
    private View view;

    public PollItemMoreDetailSelectedEvent(String id, View view) {
        this.id = id;
        this.view = view;
    }

    public String getId() {
        return id;
    }

    public View getView() {
        return view;
    }
}
