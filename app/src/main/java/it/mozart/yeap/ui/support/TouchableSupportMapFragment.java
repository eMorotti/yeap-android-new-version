package it.mozart.yeap.ui.support;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.SupportMapFragment;

import it.mozart.yeap.ui.views.TouchableWrapper;

public class TouchableSupportMapFragment extends SupportMapFragment {

    public View mOriginalContentView;
    public TouchableWrapper mTouchView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        mOriginalContentView = super.onCreateView(inflater, parent, savedInstanceState);
        mTouchView = new TouchableWrapper(getActivity());
        if (getActivity() instanceof TouchableWrapper.OnMapTouchedListener)
            mTouchView.setMapTouchListener((TouchableWrapper.OnMapTouchedListener) getActivity());
        mTouchView.addView(mOriginalContentView);
        return mTouchView;
    }

    @Override
    public View getView() {
        return mOriginalContentView;
    }
}
