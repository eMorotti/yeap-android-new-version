package it.mozart.yeap.ui.support;

import android.content.Intent;
import android.os.Bundle;

import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.services.messaggi.MessaggiService;
import it.mozart.yeap.services.sync.SyncUnitService;
import it.mozart.yeap.services.syncAdapter.SyncInit;
import it.mozart.yeap.services.syncAdapter.contacts.ContactSyncJob;
import it.mozart.yeap.services.voti.VotiService;
import it.mozart.yeap.ui.base.BaseActivity;
import it.mozart.yeap.ui.home.Act_Home;
import it.mozart.yeap.ui.signin.Act_CreateProfile;
import it.mozart.yeap.ui.signin.Act_Login;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.Prefs;

public class Act_Splash extends BaseActivity {

    // SYSTEM

    @Override
    protected int getLayoutId() {
        return R.layout.act_splash;
    }

    @Override
    protected void initVariables() {
    }

    @Override
    protected void loadParameters(Bundle extras) {
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    protected void initialize() {
        if (!Prefs.getBoolean(Constants.LOGIN_EFFECTUATED, false)) {
            startActivity(new Intent(this, Act_Login.class));
            finish();
        } else if (!Prefs.getBoolean(Constants.LOGIN_COMPLETED, false)) {
            startActivity(Act_CreateProfile.newIntent());
            finish();
        } else {
            SyncUnitService.initService(this);
            MessaggiService.initService(this);
            VotiService.initService(this);

            SyncInit syncInit = new SyncInit(Act_Splash.this);
            syncInit.execute();

            App.jobManager().addJobInBackground(new ContactSyncJob(true));

            startActivity(new Intent(this, Act_Home.class));
            finish();
        }
    }
}
