package it.mozart.yeap.ui.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;

import it.mozart.yeap.R;

public class LineView extends View {

    private Paint mPaint;

    private float mPercent;
    private float mAlpha = 0.5f;
    private int mColor = Color.RED;

    public LineView(Context context) {
        super(context);
        init(null);
    }

    public LineView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public LineView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public LineView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.LineView, 0, 0);
            mAlpha = a.getFloat(R.styleable.LineView_lv_alpha, mAlpha);
            mColor = a.getColor(R.styleable.LineView_lv_color, mColor);
            a.recycle();
        }

        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setColor(mColor);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int width = getWidth();
        if (width != 0) {
            float mid = getWidth() * mPercent;
            mPaint.setAlpha(255);
            canvas.drawRect(0, 0, mid, getHeight(), mPaint);
            mPaint.setAlpha((int) (mAlpha * 255));
            canvas.drawRect(mid, 0, getWidth(), getHeight(), mPaint);
        }

        super.onDraw(canvas);
    }

    public void setPercent(float percent) {
        this.mPercent = percent;
    }

    public void setPercent(float current, float total) {
        this.mPercent = current / total;
    }

    public void setAlpha(float alpha) {
        this.mAlpha = alpha;
    }

    public void setColor(int color) {
        this.mColor = color;
    }
}
