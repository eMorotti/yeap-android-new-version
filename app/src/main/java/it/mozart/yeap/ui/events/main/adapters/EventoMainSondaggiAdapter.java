package it.mozart.yeap.ui.events.main.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.Poll;
import it.mozart.yeap.ui.sondaggi.views.SondaggioLayout;

public class EventoMainSondaggiAdapter extends RealmRecyclerViewAdapter<Poll, RecyclerView.ViewHolder> {

    private Context mContext;
    private Event mEvento;

    public EventoMainSondaggiAdapter(Context context, Event evento, RealmResults<Poll> sondaggi) {
        super(sondaggi, true, 0);
        mContext = context;
        mEvento = evento;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MainSondaggioViewHolder(new SondaggioLayout(mContext));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MainSondaggioViewHolder sondaggioViewHolder = (MainSondaggioViewHolder) holder;
        Poll sondaggio = getItem(position);
        ((SondaggioLayout) sondaggioViewHolder.itemView).bind(sondaggio, mEvento);
    }

    public void updateEvent(Event evento) {
        mEvento = evento;
        notifyDataSetChanged();
    }

    private class MainSondaggioViewHolder extends RecyclerView.ViewHolder {

        MainSondaggioViewHolder(View v) {
            super(v);
        }
    }
}