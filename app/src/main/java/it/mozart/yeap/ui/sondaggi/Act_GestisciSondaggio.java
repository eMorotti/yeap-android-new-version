package it.mozart.yeap.ui.sondaggi;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.UUID;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.api.models.APIError;
import it.mozart.yeap.api.models.SondaggioApiModel;
import it.mozart.yeap.api.services.PollApi;
import it.mozart.yeap.events.ApiCallResultEvent;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.Poll;
import it.mozart.yeap.realm.PollItem;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.ui.base.BaseActivityWithSyncService;
import it.mozart.yeap.utility.APIErrorUtils;
import it.mozart.yeap.utility.Utils;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class Act_GestisciSondaggio extends BaseActivityWithSyncService {

    @BindView(R.id.scrollView)
    ScrollView mScrollView;
    @BindView(R.id.domanda)
    EditText mDomanda;
    @BindView(R.id.layout)
    LinearLayout mLayout;

    @OnClick(R.id.layout)
    void layoutClicked() {
        Utils.hideKeyboard(this);
    }

    // SUPPORT

    @Inject
    Realm realm;
    @Inject
    PollApi pollApi;

    private String mDomandaModel;
    private String mId;
    private int mOffset;

    private static final String PARAM_DOMANDA = "paramDomanda";
    private static final String PARAM_ID = "paramId";

    // SYSYEM

    public static Intent newIntentCrea(String attivitaId, String domanda) {
        Intent intent = new Intent(App.getContext(), Act_GestisciSondaggio.class);
        intent.putExtra(PARAM_ID, attivitaId);
        intent.putExtra(PARAM_DOMANDA, domanda);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUseBus(true);

        if (mToolbar != null) {
            mToolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_sondaggio_gestisci;
    }

    @Override
    protected void initVariables() {
    }

    @Override
    protected void loadParameters(Bundle extras) {
        mDomandaModel = extras.getString(PARAM_DOMANDA, "");
        mId = extras.getString(PARAM_ID);
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_sondaggio_nuovo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_crea:
                crea();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        cancelTimer();
        Utils.hideKeyboard(this);
        super.onBackPressed();
    }

    @Override
    protected void initialize() {
        Event evento = new EventDao(realm).getEvent(mId);
        if (evento == null) {
            Toast.makeText(this, getString(R.string.general_error_evento_non_presente), Toast.LENGTH_LONG).show();
            finish();
            return;
        } else {
            setToolbarTitle(evento.getTitle());
            setToolbarSubtitle(getString(R.string.app_nuovo_sondaggio));

            mDomanda.setText(mDomandaModel);
            mDomanda.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    mDomandaModel = s.toString();
                    if (s.length() != 0) {
                        showResponse(1);
                    } else {
                        hideResponse(1);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            setRequestId(UUID.randomUUID().toString());
        }

        mScrollView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (bottom > oldBottom) {
                    mLayout.setPadding(mLayout.getPaddingLeft(), mLayout.getPaddingTop(), mLayout.getPaddingRight(), mOffset);
                }
            }
        });

        mOffset = (int) Utils.dpToPx(16, this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ApiCallResultEvent event) {
        if (event.getRequestId().equals(getRequestId())) {
            cancelTimer();
            finish();
        }
    }

    // FUNCTIONS

    /**
     * Salvo il sondaggio
     */
    private void crea() {
        SondaggioApiModel model = new SondaggioApiModel();
        if (mDomandaModel != null && mDomandaModel.length() > 0) {
            // Imposto informazione sulla domanda
            model.setQuestion(mDomandaModel);
        } else {
            Toast.makeText(this, getString(R.string.sondaggio_error_inserisci_domanda), Toast.LENGTH_SHORT).show();
            return;
        }

        model.setPollItems(new ArrayList<PollItem>());
        for (int i = 1; i < mLayout.getChildCount(); i++) {
            TextView textView = mLayout.getChildAt(i).findViewById(R.id.risposta);
            if (textView.getText().length() != 0) {
                PollItem pollItem = new PollItem();
                pollItem.setWhat(textView.getText().toString());

                model.getPollItems().add(pollItem);
            }
        }

        getDynamicView().showLoading();
        registerSubscription(pollApi.createPoll(getRequestId(), mId, model)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Poll>() {
                    @Override
                    public void call(Poll sondaggio) {
                        startTimer();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        getDynamicView().hide();

                        APIError error = APIErrorUtils.parseError(Act_GestisciSondaggio.this, throwable);
                        Toast.makeText(Act_GestisciSondaggio.this, error.message(), Toast.LENGTH_SHORT).show();
                    }
                })
        );
    }

    private void showResponse(final int position) {
        if (!(mLayout.getChildAt(position) instanceof ViewGroup)) {
            final ViewGroup view = (ViewGroup) LayoutInflater.from(this).inflate(R.layout.item_sondaggio_voce_gestione, mLayout, false);

            final EditText risposta = view.findViewById(R.id.risposta);
            risposta.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.length() != 0) {
                        showResponse(position + 1);
                    } else {
                        hideResponse(position);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            risposta.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        int padding = mScrollView.getHeight() - view.getBottom() + view.getTop();
                        mLayout.setPadding(mLayout.getPaddingLeft(), mLayout.getPaddingTop(), mLayout.getPaddingRight(), padding);
                        mScrollView.post(new Runnable() {
                            @Override
                            public void run() {
                                mScrollView.smoothScrollTo(0, view.getTop() - mOffset / 2);
                            }
                        });
                    }
                }
            });

            mLayout.addView(view, position);
        }
    }

    private void hideResponse(final int position) {
        if (mLayout.getChildAt(position) instanceof ViewGroup) {
            if (position == mLayout.getChildCount() - 1) {
                mLayout.removeViewAt(position);
            }
        }
    }
}
