package it.mozart.yeap.ui.events.main;

import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ActionMode;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.jakewharton.rxbinding.widget.RxTextView;
import com.jakewharton.rxbinding.widget.TextViewTextChangeEvent;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import it.mozart.yeap.R;
import it.mozart.yeap.common.MediaInfo;
import it.mozart.yeap.common.MessaggioInfo;
import it.mozart.yeap.common.UrlPreviewData;
import it.mozart.yeap.events.MessaggioChatActionEvent;
import it.mozart.yeap.events.RequestImageDownloadPermissionEvent;
import it.mozart.yeap.events.UrlFoundEvent;
import it.mozart.yeap.events.UrlPreviewUpdatedEvent;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.helpers.AndroidPermissionHelper;
import it.mozart.yeap.helpers.ImageInputHelper;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.Message;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.services.messaggi.MessaggiService;
import it.mozart.yeap.services.messaggi.UrlPreviewJob;
import it.mozart.yeap.ui.base.BaseFragment;
import it.mozart.yeap.ui.events.main.adapters.EventoMainChatAdapter;
import it.mozart.yeap.ui.support.Act_Inoltra;
import it.mozart.yeap.ui.support.Act_RecuperaImageEdit;
import it.mozart.yeap.ui.support.ContextualToolbar;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.DateUtils;
import it.mozart.yeap.utility.Utils;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;

public class Frg_EventoMainChat extends BaseFragment {

    @BindView(R.id.content)
    View mInputContainer;
    @BindView(R.id.exit_text)
    View mExitText;
    @BindView(R.id.recyclerview)
    RecyclerView mRecyclerView;
    @BindView(R.id.date)
    TextView mDate;
    @BindView(R.id.input)
    EditText mInput;
    @BindView(R.id.add_image)
    ImageButton mAddImage;
    @BindView(R.id.send)
    ImageButton mSend;
    @BindView(R.id.down)
    ImageButton mGoDown;

    @BindView(R.id.preview_layout)
    View mPreviewLayout;
    @BindView(R.id.preview_inner_layout)
    View mPreviewInnerLayout;
    @BindView(R.id.preview_image)
    ImageView mPreviewImage;
    @BindView(R.id.preview_title)
    TextView mPreviewTitle;
    @BindView(R.id.preview_description)
    TextView mPreviewDescription;
    @BindView(R.id.preview_url)
    TextView mPreviewUrl;
    @BindView(R.id.preview_url_inner)
    TextView mPreviewUrlInner;

    @OnClick(R.id.input)
    void inputClicked() {
        if (mActionMode != null) {
            mAdapter.resetSelectedMessageID();
            closeActionMode();
        }
    }

    @OnClick(R.id.send)
    void sendClicked() {
        if (mInput.getText().length() == 0) {
            return;
        } else if (mInput.getText().toString().trim().length() == 0) {
            return;
        }

        mMessaggioDao.markChatAsReadAsync(true, mEventoId);
        // Creo messaggio
        MessaggioInfo messaggioInfo = new MessaggioInfo(mEventoId,
                Constants.MESSAGGIO_TYPOLOGY_CHAT_EVENTO,
                AccountProvider.getInstance().getAccountId(),
                mInput.getText().toString(),
                null,
                new Date().getTime(),
                true,
                false,
                true);
        messaggioInfo.setUrl(mUrlFound);
        if (mPreviewData != null) {
            messaggioInfo.setUrlChecked(true);
            messaggioInfo.setPreviewData(mPreviewData);
        } else if (mUrlFound == null) {
            messaggioInfo.setUrlChecked(true);
        }
        MessaggiService.createMessage(getContext(), messaggioInfo);

        mUrlFound = null;
        mPreviewLayout.setVisibility(View.GONE);
        mInput.setText("");
        mRecyclerView.scrollToPosition(0);
        mGoDown.setVisibility(View.GONE);
        mPosition = 0;
    }

    @OnClick(R.id.add_image)
    void addImageClicked() {
        addImageClickedMethod(false);
    }

    @OnClick(R.id.down)
    void goDownClicked() {
        mRecyclerView.scrollToPosition(0);
        mGoDown.setVisibility(View.GONE);
    }

    // SUPPORT

    @Inject
    Realm realm;

    private MessageDao mMessaggioDao;
    private Handler mHandler;
    private ActionMode mActionMode;
    private EventoMainChatAdapter mAdapter;
    private View.OnLayoutChangeListener mLayoutListener;
    private Event mEvento;
    private UrlPreviewJob mPreviewAsyncTask;
    private UrlPreviewData mPreviewData;
    private String mEventoId;
    private String mUrlFound;
    private boolean mIgnorePermissionRequest;
    private boolean mDateShow;
    private boolean mGoDownVisible;
    private int mScrollY;
    private int mPosition;

    private static final String PARAM_ID = "paramId";

    private Runnable mDateRunnable = new Runnable() {
        @Override
        public void run() {
            animateStickyDate(false);
        }
    };

    // SYSTEM

    public static Frg_EventoMainChat newInstance(String attivitaId) {
        Bundle args = new Bundle();
        args.putString(PARAM_ID, attivitaId);
        Frg_EventoMainChat fragment = new Frg_EventoMainChat();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUseBus(true);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_main_evento_chat;
    }

    @Override
    protected void initValues() {
        mDateShow = false;
        mPosition = 0;
        mScrollY = 0;
        mGoDownVisible = false;
        mIgnorePermissionRequest = false;
    }

    @Override
    protected void loadParameters(Bundle arguments) {
        mEventoId = arguments.getString(PARAM_ID);
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {
    }

    @Override
    protected void saveInfos(Bundle outState) {
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mActionMode != null) {
            mAdapter.resetSelectedMessageID();
            closeActionMode();
        }
    }

    @Override
    public void onDestroyView() {
        mRecyclerView.clearOnScrollListeners();
        // Fix problema leak listener realm
        mRecyclerView.setAdapter(null);
        if (mLayoutListener != null) {
            ((View) mRecyclerView.getParent()).removeOnLayoutChangeListener(mLayoutListener);
        }
        if (mEvento != null) {
            mEvento.removeAllChangeListeners();
        }
        super.onDestroyView();
    }

    @Override
    protected void initialize() {
        if (!Utils.isLollipop()) {
            int size = (int) Utils.dpToPx(40, getContext());
            int margin = (int) Utils.dpToPx(8, getContext());
            mSend.getLayoutParams().height = size;
            mSend.getLayoutParams().width = size;
            ((ViewGroup.MarginLayoutParams) mSend.getLayoutParams()).topMargin = margin;
            ((ViewGroup.MarginLayoutParams) mSend.getLayoutParams()).rightMargin = margin;
        }

        mDate.clearAnimation();
        mHandler = new Handler();
        mMessaggioDao = new MessageDao(realm);

        // Init attività
        mEvento = new EventDao(realm).getEvent(mEventoId);
        mEvento.addChangeListener(new RealmChangeListener<Event>() {
            @Override
            public void onChange(@NonNull Event evento) {
                if (evento.isValid()) {
                    showOrHideInput(evento);
                    mAdapter.updateNumberNewMessages(evento.getNumNewMessages());
                }
            }
        });

        if (mEvento.getNumNewMessages() != 0) {
            mPosition = (int) mEvento.getNumNewMessages() - 3;
            if(mPosition < 0) {
                mPosition = 0;
            }
        }

        showOrHideInput(mEvento);

        // Init Chat
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        manager.setStackFromEnd(true);
        manager.setReverseLayout(true);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setItemAnimator(null);
        mRecyclerView.getRecycledViewPool().setMaxRecycledViews(Constants.ADAPTER_TYPE_STANDARD_LEFT, 30);
        mRecyclerView.getRecycledViewPool().setMaxRecycledViews(Constants.ADAPTER_TYPE_STANDARD_RIGHT, 30);
        mRecyclerView.getRecycledViewPool().setMaxRecycledViews(Constants.ADAPTER_TYPE_SYSTEM, 30);
        mAdapter = new EventoMainChatAdapter(getContext(), mMessaggioDao.getListMessagesEventChat(mEventoId), mEvento.getNumNewMessages());
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (mRecyclerView != null && mRecyclerView.getLayoutManager() != null)
                    mPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                // Visualizzo il tasto per tornare alla fine della chat
                if (mGoDown != null) {
                    if (mPosition > 5 && !mGoDownVisible) {
                        mGoDownVisible = true;
                        mGoDown.setVisibility(View.VISIBLE);
                    } else if (mPosition <= 5 && mGoDownVisible) {
                        mGoDownVisible = false;
                        mGoDown.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                mScrollY += dy;
                // Animo la comparsa della data (se ho scrollato almeno di un certo offset)
                if (Math.abs(mScrollY) > 80)
                    animateStickyDate(true);
                // Aggiorno il testo della data
                if (mRecyclerView != null && mRecyclerView.getLayoutManager() != null) {
                    int position = ((LinearLayoutManager) mRecyclerView.getLayoutManager()).findLastVisibleItemPosition();
                    long value = mAdapter.updateStickyDate(position);
                    if (mDate.getTag() != null && value == Long.parseLong(mDate.getTag().toString()))
                        return;
                    mDate.setTag(String.valueOf(value));
                    mDate.setText(DateUtils.smartFormatDateOnly(getContext(), value * 86400000, "dd MMMM yyyy"));
                    mDate.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (mDate != null)
                                mDate.requestLayout();
                        }
                    }, 10);
                }
            }
        });
        mRecyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                // Sincronizzo la posizione ad ogni apertura/chiusura della tastiera
                if (oldLeft != left || oldRight != right || oldTop != top || oldBottom != bottom) {
                    if (mPosition == 0) {
                        mRecyclerView.post(new Runnable() {
                            @Override
                            public void run() {
                                if (mRecyclerView != null) {
                                    ((LinearLayoutManager) mRecyclerView.getLayoutManager()).scrollToPositionWithOffset(mPosition, 0);
                                }
                            }
                        });
                    }
                }
            }
        });
        // Sincronizzo la posizione ad ogni orientation change
        ((LinearLayoutManager) mRecyclerView.getLayoutManager()).scrollToPositionWithOffset(mPosition, 0);

        ((View) mRecyclerView.getParent()).addOnLayoutChangeListener(mLayoutListener = new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (oldBottom == 0)
                    return;

                if (getActivity() instanceof Act_EventoMain) {
                    ((Act_EventoMain) getActivity()).onLayoutChange(oldBottom, bottom);
                }
            }
        });

        // Init
        mInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() > 0) {
                    if (mAddImage.getVisibility() == View.VISIBLE) {
                        mSend.setVisibility(View.VISIBLE);
                    }

                    mAddImage.setVisibility(View.GONE);

                    String url = Utils.findUrlInText(s.toString());
                    if (url == null) {
                        mPreviewLayout.setVisibility(View.GONE);
                        mPreviewData = null;
                        mUrlFound = null;
                    } else if (!url.equalsIgnoreCase(mUrlFound)) {
                        mPreviewLayout.setVisibility(View.GONE);
                        mPreviewData = null;
                        mUrlFound = url;
                    }
                } else {
                    if (mAddImage.getVisibility() == View.GONE) {
                        mSend.setVisibility(View.GONE);
                    }

                    mAddImage.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        if (Constants.URL_PREVIEW_ENABLED) {
            registerSubscription(RxTextView.textChangeEvents(mInput)
                    .debounce(300, TimeUnit.MILLISECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(getInputObserver()));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Constants.IMAGE_ACTIVITY_CODE:
                    Utils.hideKeyboard(getActivity());
                    String imagePath = data.getStringExtra(Constants.IMAGE_PATH);
                    String caption = data.getStringExtra(Constants.CAPTION);
                    if (imagePath != null) {
                        try {
                            // Calcolo l'altezza dell'immagine nella chat
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inJustDecodeBounds = true;
                            BitmapFactory.decodeFile(new File(imagePath).getAbsolutePath(), options);

                            // Crea model immagine
                            MediaInfo mediaInfo = new MediaInfo(imagePath);
                            mediaInfo.setWidth(options.outWidth);
                            mediaInfo.setHeight(options.outHeight);

                            mMessaggioDao.markChatAsReadAsync(true, mEventoId);
                            // Creo messaggio
                            MessaggioInfo messaggioInfo = new MessaggioInfo(mEventoId,
                                    Constants.MESSAGGIO_TYPOLOGY_CHAT_EVENTO,
                                    AccountProvider.getInstance().getAccountId(),
                                    caption != null ? caption : "",
                                    mediaInfo,
                                    new Date().getTime(),
                                    true,
                                    false,
                                    true);
                            MessaggiService.createMessage(getContext(), messaggioInfo);
                        } catch (Exception ex) {
                            Toast.makeText(getContext(), getString(R.string.general_error_realm), Toast.LENGTH_LONG).show();
                        }
                    }
                    break;
                case Constants.IMAGE_GALLERY_ACTIVITY_CODE:
                    Uri imageUri = data.getData();
                    startActivityForResult(Act_RecuperaImageEdit.newIntent(imageUri), Constants.IMAGE_ACTIVITY_CODE);
                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.IMAGE_PERMISSION_CODE:
                addImageClickedMethod(true);
                break;
            case Constants.SD_PERMISSION_CODE:
                for (Integer permission : grantResults) {
                    if (permission == PackageManager.PERMISSION_GRANTED) {
                        mAdapter.notifyDataSetChanged();
                        break;
                    }
                }
                mIgnorePermissionRequest = false;
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(UrlPreviewUpdatedEvent event) {
        if (mPosition == 0) {
            mRecyclerView.post(new Runnable() {
                @Override
                public void run() {
                    if (mRecyclerView != null) {
                        ((LinearLayoutManager) mRecyclerView.getLayoutManager()).scrollToPositionWithOffset(mPosition, 0);
                    }
                }
            });
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(UrlFoundEvent event) {
        if (event.getId().equals(mEventoId) && event.getUrl().equals(mUrlFound) && mInput.getText().length() != 0) {
            mPreviewData = new UrlPreviewData().fromData(event.getPreviewData());
            mPreviewLayout.setVisibility(View.VISIBLE);
            mPreviewTitle.setText(event.getPreviewData().getTitle());
            mPreviewDescription.setText(event.getPreviewData().getDescription());

            String url = event.getPreviewData().getUrl() == null ? mUrlFound : event.getPreviewData().getUrl();

            if (event.getPreviewData().getDescription() == null || event.getPreviewData().getDescription().isEmpty()) {
                mPreviewDescription.setVisibility(View.GONE);
            } else {
                mPreviewDescription.setVisibility(View.VISIBLE);
            }

            if (mPreviewData.getImage() != null) {
                Glide.with(getContext()).load(mPreviewData.getImage()).centerCrop().diskCacheStrategy(DiskCacheStrategy.SOURCE).into(mPreviewImage);
                mPreviewInnerLayout.getLayoutParams().height = (int) Utils.dpToPx(60, getContext());
                mPreviewImage.setVisibility(View.VISIBLE);

                mPreviewUrl.setVisibility(View.VISIBLE);
                mPreviewUrlInner.setVisibility(View.GONE);
                mPreviewUrl.setText(url);
            } else {
                Glide.clear(mPreviewImage);
                mPreviewInnerLayout.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                mPreviewImage.setVisibility(View.GONE);

                mPreviewUrl.setVisibility(View.GONE);
                mPreviewUrlInner.setVisibility(View.VISIBLE);
                mPreviewUrlInner.setText(url);
            }
        }

        mUrlFound = null;
    }

    @Subscribe
    public void onEvent(RequestImageDownloadPermissionEvent event) {
        if (mIgnorePermissionRequest) {
            return;
        }

        mIgnorePermissionRequest = true;
        AndroidPermissionHelper.requestPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE, Constants.SD_PERMISSION_CODE);
    }

    @Subscribe
    public void onEvent(MessaggioChatActionEvent event) {
        if (event.isOpen()) {
            if (mActionMode != null) {
                updateActionMode();
                return;
            }
            mMessaggioDao.markChatAsReadAsync(true, mEventoId);
            mActionMode = getActivity().startActionMode(new ContextualToolbar(R.menu.menu_contextual_toolbar_chat, new ContextualToolbar.onCallbackListener() {
                @Override
                public void onItemSelected(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.menu_delete:
                            // Elimino i messaggi
                            new MaterialDialog.Builder(getContext())
                                    .title(getString(R.string.chat_elimina_title))
                                    .content(mAdapter.getSelectedMessageIds().size() > 1 ?
                                            String.format(getString(R.string.chat_elimina_messaggi), mAdapter.getSelectedMessageIds().size()) :
                                            String.format(getString(R.string.chat_elimina_messaggio), mAdapter.getSelectedMessageIds().entrySet().iterator().next().getValue()))
                                    .positiveText(getString(R.string.general_rimuovi))
                                    .negativeText(getString(R.string.general_annulla))
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            deleteMessages();
                                        }
                                    })
                                    .show();
                            break;
                        case R.id.menu_forward:
                            // Inoltro i messaggi
                            startActivity(Act_Inoltra.newIntent(mAdapter.getSelectedMessageIds()));
                            break;
                        case R.id.menu_copy:
                            // Copio i messaggi
                            copyMessages();
                            break;
                    }
                }

                @Override
                public void onDestroy() {
                    mAdapter.resetSelectedMessageID();
                    closeActionMode();
                    manageViewpagerScroll(true);
                }
            }));
            manageViewpagerScroll(false);
            updateActionMode();
        } else if (mActionMode != null) {
            closeActionMode();
            manageViewpagerScroll(true);
        }
    }

    // FUNCTIONS

    /**
     * Osservabile sul cambiamento di testo dell'input
     *
     * @return observabile
     */
    private Observer<TextViewTextChangeEvent> getInputObserver() {
        return new Observer<TextViewTextChangeEvent>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(TextViewTextChangeEvent textViewTextChangeEvent) {
                if (mUrlFound != null) {
                    if (mPreviewAsyncTask != null && mPreviewAsyncTask.getStatus() != AsyncTask.Status.FINISHED) {
                        mPreviewAsyncTask.cancel(true);
                    }
                    mPreviewAsyncTask = new UrlPreviewJob(null, mUrlFound, mEventoId);
                    mPreviewAsyncTask.execute();
                }
            }
        };
    }

    /**
     * Visualizzo o meno l'input della chat
     *
     * @param evento evento
     */
    private void showOrHideInput(Event evento) {
        if (evento != null && evento.isMyselfPresent()) {
            mInputContainer.setVisibility(View.VISIBLE);
            mExitText.setVisibility(View.GONE);
        } else {
            mInputContainer.setVisibility(View.INVISIBLE);
            mExitText.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Gestisco l'abilitazione dello scroll nel viewpager
     *
     * @param enable abilita
     */
    private void manageViewpagerScroll(boolean enable) {
        if (getActivity() instanceof Act_EventoMain) {
            ((Act_EventoMain) getActivity()).onLayoutChange(enable);
        }
    }

    /**
     * Chiudo l'action mode
     */
    private void closeActionMode() {
        mActionMode.finish();
        mActionMode = null;
        manageViewpagerScroll(false);
    }

    /**
     * Aggiorno la barra contestuale
     */
    private void updateActionMode() {
        // Titolo
        if (mAdapter.getSelectedMessageIds().size() == 1) {
            mActionMode.setTitle(getString(R.string.chat_action_mode_elemento));
        } else {
            mActionMode.setTitle(String.format(getString(R.string.chat_action_mode_elementi), mAdapter.getSelectedMessageIds().size()));
        }
        // Items
        boolean showCopy = true;
        boolean showForward = true;
        for (Object o : mAdapter.getSelectedMessageIds().entrySet()) {
            Map.Entry pair = (Map.Entry) o;
            Message messaggio = mMessaggioDao.getMessage((String) pair.getKey());
            if (!showCopy && !showForward)
                break;

            // Decido quale item visualizzare
            if (messaggio.getType() == Constants.MESSAGGIO_TYPE_SYSTEM) {
                showForward = false;
                showCopy = false;
            } else if (messaggio.getMediaLocalUrl() != null || messaggio.getMediaUrl() != null) {
//                showForward = false;
                showCopy = false;
            }
        }
        // Aggiorno la visibilità
        MenuItem copy = mActionMode.getMenu().findItem(R.id.menu_copy);
        MenuItem forward = mActionMode.getMenu().findItem(R.id.menu_forward);
        if (copy != null) {
            copy.setVisible(showCopy);
        }
        if (forward != null) {
            forward.setVisible(showForward);
        }
    }

    /**
     * Elimino i messaggi selezionati
     */
    private void deleteMessages() {
        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(@NonNull Realm realm) {
                    for (Object o : mAdapter.getSelectedMessageIds().entrySet()) {
                        Map.Entry pair = (Map.Entry) o;
                        mMessaggioDao.deleteMessage((String) pair.getKey());
                    }
                }
            });
            mAdapter.resetSelectedMessageID();
            Toast.makeText(getContext(), getString(R.string.system_messaggi_cancellati), Toast.LENGTH_SHORT).show();
            closeActionMode();
        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(getContext(), getString(R.string.general_error_realm), Toast.LENGTH_SHORT).show();
        }

        if (getActivity() instanceof Act_EventoMain) {
            ((Act_EventoMain) getActivity()).onLayoutChange(true);
        }
    }

    /**
     * Copia i messaggi
     */
    private void copyMessages() {
        ClipboardManager clipboard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
        String text = "";
        for (Object o : mAdapter.getSelectedMessageIds().entrySet()) {
            Map.Entry pair = (Map.Entry) o;
            Message messaggio = mMessaggioDao.getMessage((String) pair.getKey());
            if (messaggio == null)
                continue;

            if (!text.isEmpty()) {
                text += "\n";
            }
            text += DateUtils.formatDate(messaggio.getCreatedAt(), "'['dd/MM HH:mm']' ");
            text += messaggio.getCreator().getChatName() + ": " + messaggio.getFormattedMessage(getContext());
        }
        ClipData clipData = ClipData.newPlainText(getString(R.string.system_messaggi_copiati_titolo), text);
        clipboard.setPrimaryClip(clipData);
        mAdapter.resetSelectedMessageID();
        Toast.makeText(getContext(), getString(R.string.system_messaggi_copiati), Toast.LENGTH_SHORT).show();
        closeActionMode();
    }

    /**
     * Anima la visualizzazione della data corrispondente al primo messaggio in vista
     *
     * @param show visualizzo o meno la data
     */
    private void animateStickyDate(boolean show) {
        if (mDate == null)
            return;
        if (show) {
            // Se la data è non visibile la visualizzo
            if (!mDateShow)
                mDate.animate().translationY(0).setDuration(250).start();
            // Reinizializzo il postdelayed
            mHandler.removeCallbacks(mDateRunnable);
            mHandler.postDelayed(mDateRunnable, 2000);
        } else {
            // Nascondo la data
            mDate.animate().translationY(-mDate.getHeight() - ((ViewGroup.MarginLayoutParams) mDate.getLayoutParams()).topMargin)
                    .setDuration(250).withEndAction(new Runnable() {
                @Override
                public void run() {
                    mScrollY = 0;
                }
            }).start();
        }
        mDateShow = show;
    }

    private void addImageClickedMethod(boolean ignoreCheck) {
        ImageInputHelper.openChooserForChatImageSelect(this, ignoreCheck);
    }
}
