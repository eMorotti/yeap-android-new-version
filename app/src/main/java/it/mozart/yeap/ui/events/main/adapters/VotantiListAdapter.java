package it.mozart.yeap.ui.events.main.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import it.mozart.yeap.R;
import it.mozart.yeap.realm.Vote;
import it.mozart.yeap.ui.views.AutoRefreshImageView;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.DateUtils;

public class VotantiListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<Vote> mVotiSi;
    private List<Vote> mVotiNo;
    private String mFormat;
    private String mFormatTime;
    private int mSiColor;
    private int mNoColor;
    private boolean mIsInteressati;

    public VotantiListAdapter(final Context context, List<Vote> yes, List<Vote> no, boolean isInteressati) {
        mContext = context;
        mVotiSi = yes;
        mVotiNo = no;
        mFormat = context.getString(R.string.evento_voto_voce_data_semplificato);
        mFormatTime = String.format(" '%s' HH:mm", context.getString(R.string.general_at));
        mSiColor = ContextCompat.getColor(context, R.color.colorLightBlueEvento);
        mNoColor = ContextCompat.getColor(context, R.color.colorGrayPrimary);
        mIsInteressati = isInteressati;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case Constants.ADAPTER_HEADER_VOTI_SI:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_utenti_voto_header, parent, false);
                return new UtentiSiHeaderViewHolder(view);
            case Constants.ADAPTER_ITEM_VOTI_SI:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_utenti_voto, parent, false);
                return new UtentiSiViewHolder(view);
            case Constants.ADAPTER_HEADER_VOTI_NO:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_utenti_voto_header, parent, false);
                return new UtentiNoHeaderViewHolder(view);
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_utenti_voto, parent, false);
                return new UtentiNoViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case Constants.ADAPTER_HEADER_VOTI_SI:
                bindUtentiSiHeader((UtentiSiHeaderViewHolder) holder);
                break;
            case Constants.ADAPTER_ITEM_VOTI_SI:
                bindUtentiSi((UtentiSiViewHolder) holder, position);
                break;
            case Constants.ADAPTER_HEADER_VOTI_NO:
                bindUtentiNoHeader((UtentiNoHeaderViewHolder) holder);
                break;
            default:
                bindUtentiNo((UtentiNoViewHolder) holder, position);
                break;
        }
    }

    private void bindUtentiSiHeader(UtentiSiHeaderViewHolder realHolder) {
        if (mVotiSi.size() == 1) {
            realHolder.header.setText(mIsInteressati ? mContext.getString(R.string.evento_interessato) : mContext.getString(R.string.evento_partecipa));
        } else {
            realHolder.header.setText(mIsInteressati ? String.format(Locale.getDefault(), "%d %s", mVotiSi.size(), mContext.getString(R.string.evento_interessati)) :
                    String.format(Locale.getDefault(), "%d %s", mVotiSi.size(), mContext.getString(R.string.evento_partecipano)));
        }
    }

    private void bindUtentiSi(final UtentiSiViewHolder realHolder, int position) {
        Vote voto = mVotiSi.get(position - 1);
        if (voto == null || voto.getCreator() == null) {
            return;
        }

        realHolder.name.setText(voto.getCreator().getChatName());
        realHolder.desc.setText(DateUtils.smartFormatDate(mContext, voto.getCreatedAt(), mFormat, mFormatTime));
        realHolder.avatar.bindThumb(voto.getCreatorUuid());

        realHolder.icon.setImageResource(R.drawable.ic_thumb_up);
        realHolder.icon.setColorFilter(mSiColor);
    }

    private void bindUtentiNoHeader(UtentiNoHeaderViewHolder realHolder) {
        if (mVotiNo.size() == 1) {
            realHolder.header.setText(mIsInteressati ? mContext.getString(R.string.evento_non_interessato) : mContext.getString(R.string.evento_non_partecipa));
        } else {
            realHolder.header.setText(mIsInteressati ? String.format(Locale.getDefault(), "%d %s", mVotiNo.size(), mContext.getString(R.string.evento_non_interessati)) :
                    String.format(Locale.getDefault(), "%d %s", mVotiNo.size(), mContext.getString(R.string.evento_non_partecipano)));
        }
    }

    private void bindUtentiNo(final UtentiNoViewHolder realHolder, int position) {
        Vote voto = mVotiNo.get(position - mVotiSi.size() - 1 - (mVotiSi.size() != 0 ? 1 : 0));
        if (voto == null || voto.getCreator() == null) {
            return;
        }

        realHolder.name.setText(voto.getCreator().getChatName());
        realHolder.desc.setText(DateUtils.smartFormatDate(mContext, voto.getCreatedAt(), mFormat, mFormatTime));
        realHolder.avatar.bindThumb(voto.getCreatorUuid());

        realHolder.icon.setImageResource(R.drawable.ic_thumb_down);
        realHolder.icon.setColorFilter(mNoColor);
    }

    @Override
    public int getItemCount() {
        return mVotiSi.size() + mVotiNo.size() + (mVotiSi.size() != 0 ? 1 : 0) + (mVotiNo.size() != 0 ? 1 : 0);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            if (mVotiSi.size() != 0) {
                return Constants.ADAPTER_HEADER_VOTI_SI;
            } else {
                return Constants.ADAPTER_HEADER_VOTI_NO;
            }
        } else {
            if (mVotiSi.size() != 0) {
                if (position <= mVotiSi.size()) {
                    return Constants.ADAPTER_ITEM_VOTI_SI;
                } else {
                    if (position == mVotiSi.size() + 1) {
                        return Constants.ADAPTER_HEADER_VOTI_NO;
                    } else {
                        return Constants.ADAPTER_ITEM_VOTI_NO;
                    }
                }
            } else {
                return Constants.ADAPTER_ITEM_VOTI_NO;
            }
        }
    }

    class UtentiSiViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.avatar)
        AutoRefreshImageView avatar;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.desc)
        TextView desc;
        @BindView(R.id.icon)
        ImageView icon;

        UtentiSiViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    class UtentiSiHeaderViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.header)
        TextView header;

        UtentiSiHeaderViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    class UtentiNoViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.avatar)
        AutoRefreshImageView avatar;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.desc)
        TextView desc;
        @BindView(R.id.icon)
        ImageView icon;

        UtentiNoViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    class UtentiNoHeaderViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.header)
        TextView header;

        UtentiNoHeaderViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
