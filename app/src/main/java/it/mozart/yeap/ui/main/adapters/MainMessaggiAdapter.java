package it.mozart.yeap.ui.main.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import it.mozart.yeap.R;

public class MainMessaggiAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<String> mMessaggi;

    public MainMessaggiAdapter(List<String> messaggi) {
        this.mMessaggi = messaggi;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_message_reply, parent, false);
        return new MessaggiViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MessaggiViewHolder realHolder = (MessaggiViewHolder) holder;
        String messaggio = mMessaggi.get(position);

        realHolder.text.setText(messaggio);
    }

    @Override
    public int getItemCount() {
        return mMessaggi.size();
    }

    class MessaggiViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text)
        TextView text;

        MessaggiViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
