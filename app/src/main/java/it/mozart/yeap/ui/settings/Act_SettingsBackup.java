package it.mozart.yeap.ui.settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;

import butterknife.BindView;
import butterknife.OnClick;
import it.mozart.yeap.R;
import it.mozart.yeap.helpers.AnalitycsProvider;
import it.mozart.yeap.ui.base.BaseActivityWithSyncService;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.DateUtils;
import it.mozart.yeap.utility.Prefs;
import it.mozart.yeap.utility.backup.GoogleDriveBackup;

public class Act_SettingsBackup extends BaseActivityWithSyncService {

    @BindView(R.id.last_backup)
    TextView mLastBackup;
    @BindView(R.id.backup_progress)
    ProgressBar mBackupProgress;

    @OnClick(R.id.backup)
    void b() {
        if (!mPlayAvailable) {
            return;
        }
        if (mGoogleDriveBackup.isConnected()) {
            mBackupProgress.setVisibility(View.VISIBLE);
            AnalitycsProvider.getInstance().logCustom(mFirebaseAnalytics, "performBackup");
            mGoogleDriveBackup.backupRealm();
        } else {
            mGoogleDriveBackup.start();
        }
    }

    // SUPPORT

    private GoogleDriveBackup mGoogleDriveBackup;
    private boolean mPlayAvailable;

    // SYSTEM


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setToolbarTitle(getString(R.string.app_settings));
        setToolbarSubtitle(getString(R.string.app_settings_backup));
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_settings_backup;
    }

    @Override
    protected void initVariables() {
        mPlayAvailable = true;
    }

    @Override
    protected void loadParameters(Bundle extras) {

    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleDriveBackup.stop();
    }

    @Override
    protected void initialize() {
        mLastBackup.setText(Prefs.getString(Constants.BACKUP_DATE, "-"));

        if (mGoogleDriveBackup == null) {
            mGoogleDriveBackup = new GoogleDriveBackup();
        }
        if (mGoogleDriveBackup.isGooglePlayServicesAvailable(this, false)) {
            mGoogleDriveBackup.init(this);
            mGoogleDriveBackup.setListener(new GoogleDriveBackup.DriveConnectionListener() {
                @Override
                public void connected() {

                }

                @Override
                public void error() {

                }

                @Override
                public void onBackupSuccess(String testo) {
                    mBackupProgress.setVisibility(View.INVISIBLE);
                    Toast.makeText(Act_SettingsBackup.this, testo, Toast.LENGTH_LONG).show();
                    Prefs.putString(Constants.BACKUP_DATE, DateUtils.formatDate(new Date(), "dd/MM/yyyy, HH:mm"));
                    mLastBackup.setText(Prefs.getString(Constants.BACKUP_DATE, "-"));
                }

                @Override
                public void onBackupError(String error) {
                    mBackupProgress.setVisibility(View.INVISIBLE);
                    Toast.makeText(Act_SettingsBackup.this, error, Toast.LENGTH_LONG).show();
                }

                @Override
                public void onRestoreSuccess(String testo, long timestamp) {

                }

                @Override
                public void onRestoreError(String error) {

                }

                @Override
                public void onRestoreNoBackup() {

                }
            });
            mGoogleDriveBackup.start();
        } else {
            mPlayAvailable = false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case GoogleDriveBackup.REQUEST_RESOLVE_ERROR:
                if (resultCode == RESULT_OK) {
                    mGoogleDriveBackup.start();
                }
                break;
            case GoogleDriveBackup.REQUEST_PLAY_SERVICES:
                mPlayAvailable = true;
                initialize();
                break;
        }
    }
}
