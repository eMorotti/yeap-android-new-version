package it.mozart.yeap.ui.base;

import it.mozart.yeap.services.sync.SyncUnitService;

public abstract class BaseActivityWithSyncService extends BaseActivity {

    @Override
    protected void onResume() {
        super.onResume();
        SyncUnitService.bindService(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        SyncUnitService.unbindService(this);
    }
}