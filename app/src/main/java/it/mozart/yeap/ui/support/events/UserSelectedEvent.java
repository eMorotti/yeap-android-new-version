package it.mozart.yeap.ui.support.events;

import android.view.View;

public class UserSelectedEvent {

    private String id;
    private View view;

    public UserSelectedEvent(String id, View view) {
        this.id = id;
        this.view = view;
    }

    public String getId() {
        return id;
    }

    public View getView() {
        return view;
    }
}
