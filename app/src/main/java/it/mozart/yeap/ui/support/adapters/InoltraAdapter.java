package it.mozart.yeap.ui.support.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmResults;
import it.mozart.yeap.R;
import it.mozart.yeap.events.InoltraChangeSelectedEvent;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.Group;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.ui.views.AnimCheckBox;
import it.mozart.yeap.utility.Constants;

public class InoltraAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private RealmResults<Event> mAttivita;
    private RealmResults<Group> mGruppi;
    private HashMap<String, Boolean> mSelected;
    private HashMap<String, String> mSelectedTitle;

    public InoltraAdapter(Context context, RealmResults<Event> attivita, RealmResults<Group> gruppi,
                          HashMap<String, Boolean> selected, HashMap<String, String> selectedTitle) {
        this.mContext = context;
        this.mAttivita = attivita;
        this.mGruppi = gruppi;
        this.mSelected = selected;
        this.mSelectedTitle = selectedTitle;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == Constants.ADAPTER_HEADER) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_inoltra_header, parent, false);
            return new InoltraHeaderViewHolder(itemView);
        } else {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_inoltra, parent, false);
            return new InoltraViewHolder(itemView);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == Constants.ADAPTER_HEADER) {
            InoltraHeaderViewHolder realHolder = (InoltraHeaderViewHolder) holder;
            if (position == 0) {
                realHolder.header.setText(mContext.getString(R.string.general_evento));
            } else {
                realHolder.header.setText(mContext.getString(R.string.general_gruppi));
            }
        } else {
            final InoltraViewHolder realHolder = (InoltraViewHolder) holder;
            if (position <= mAttivita.size()) {
                Event attivita = mAttivita.get(position - 1);
                realHolder.nome.setText(attivita.getTitle());

                String invitati = null;
                for (User utente : attivita.getGuests()) {
                    if (invitati == null)
                        invitati = utente.getChatName();
                    else
                        invitati += ", " + utente.getChatName();
                }
                if (invitati != null)
                    realHolder.invitati.setText(invitati);

                if (mSelected.containsKey(attivita.getUuid()) && mSelected.get(attivita.getUuid())) {
                    realHolder.check.setVisibility(View.VISIBLE);
                    realHolder.check.setChecked(true, false);
                } else {
                    realHolder.check.setVisibility(View.GONE);
                }

                if (attivita.getAvatarThumbUrl() != null) {
                    Glide.with(mContext).load(attivita.getAvatarThumbUrl()).asBitmap().centerCrop().into(new BitmapImageViewTarget(realHolder.avatar) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(mContext.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            realHolder.avatar.setImageDrawable(circularBitmapDrawable);
                        }
                    });
                } else {
                    Glide.with(mContext).load(R.drawable.avatar_attivita).asBitmap().centerCrop().into(new BitmapImageViewTarget(realHolder.avatar) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(mContext.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            realHolder.avatar.setImageDrawable(circularBitmapDrawable);
                        }
                    });
                }
            } else {
                Group gruppo = mGruppi.get(position - 2 - mAttivita.size());
                realHolder.nome.setText(gruppo.getTitle());

                String invitati = null;
                for (User utente : gruppo.getGuests()) {
                    if (invitati == null)
                        invitati = utente.getChatName();
                    else
                        invitati += ", " + utente.getChatName();
                }
                if (invitati != null)
                    realHolder.invitati.setText(invitati);

                if (mSelected.containsKey(gruppo.getUuid()) && !mSelected.get(gruppo.getUuid())) {
                    realHolder.check.setVisibility(View.VISIBLE);
                    realHolder.check.setChecked(true, false);
                } else {
                    realHolder.check.setVisibility(View.GONE);
                }

                if (gruppo.getAvatarThumbUrl() != null) {
                    Glide.with(mContext).load(gruppo.getAvatarThumbUrl()).asBitmap().centerCrop().into(new BitmapImageViewTarget(realHolder.avatar) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(mContext.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            realHolder.avatar.setImageDrawable(circularBitmapDrawable);
                        }
                    });
                } else {
                    Glide.with(mContext).load(R.drawable.avatar_gruppo).asBitmap().centerCrop().into(new BitmapImageViewTarget(realHolder.avatar) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(mContext.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            realHolder.avatar.setImageDrawable(circularBitmapDrawable);
                        }
                    });
                }
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 || position == mAttivita.size() + 1)
            return Constants.ADAPTER_HEADER;
        return Constants.ADAPTER_ITEM;
    }

    @Override
    public int getItemCount() {
        return mAttivita.size() + mGruppi.size() + 2;
    }

    public HashMap<String, Boolean> getSelected() {
        return mSelected;
    }

    public HashMap<String, String> getSelectedTitle() {
        return mSelectedTitle;
    }

    class InoltraViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.avatar)
        ImageView avatar;
        @BindView(R.id.nome)
        TextView nome;
        @BindView(R.id.invitati)
        TextView invitati;
        @BindView(R.id.check)
        AnimCheckBox check;

        InoltraViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getLayoutPosition();
            if (position <= mAttivita.size()) {
                Event attivita = mAttivita.get(position - 1);
                if (mSelected.containsKey(attivita.getUuid())) {
                    mSelected.remove(attivita.getUuid());
                    mSelectedTitle.remove(attivita.getUuid());
                } else {
                    mSelected.put(attivita.getUuid(), true);
                    mSelectedTitle.put(attivita.getUuid(), attivita.getTitle());
                }
                notifyItemChanged(position);
            } else {
                Group gruppo = mGruppi.get(position - 2 - mAttivita.size());
                if (mSelected.containsKey(gruppo.getUuid())) {
                    mSelected.remove(gruppo.getUuid());
                    mSelectedTitle.remove(gruppo.getUuid());
                } else {
                    mSelected.put(gruppo.getUuid(), false);
                    mSelectedTitle.put(gruppo.getUuid(), gruppo.getTitle());
                }
                notifyItemChanged(position);
            }
            EventBus.getDefault().post(new InoltraChangeSelectedEvent());
        }
    }

    class InoltraHeaderViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.header)
        TextView header;

        InoltraHeaderViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
