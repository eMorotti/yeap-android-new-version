package it.mozart.yeap.ui.events.main.events;

public class EventoUpdateStatus {

    private int status;

    public EventoUpdateStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }
}
