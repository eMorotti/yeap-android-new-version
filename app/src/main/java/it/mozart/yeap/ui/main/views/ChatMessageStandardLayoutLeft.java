package it.mozart.yeap.ui.main.views;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;

import it.mozart.yeap.R;

public class ChatMessageStandardLayoutLeft extends ChatMessageStandardLayout {

    public ChatMessageStandardLayoutLeft(Context context) {
        super(context);
    }

    public ChatMessageStandardLayoutLeft(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ChatMessageStandardLayoutLeft(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @SuppressWarnings("unused")
    public ChatMessageStandardLayoutLeft(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.item_chat_left;
    }

    @Override
    protected boolean isOtherMessage() {
        return true;
    }

    @Override
    protected int getLinkColor() {
        return ContextCompat.getColor(getContext(), R.color.textLinkLeft);
    }
}
