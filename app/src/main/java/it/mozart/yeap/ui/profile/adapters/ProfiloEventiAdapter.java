package it.mozart.yeap.ui.profile.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;
import it.mozart.yeap.events.EventSelectedEvent;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.ui.home.views.EventLayout;

public class ProfiloEventiAdapter extends RealmRecyclerViewAdapter<Event, RecyclerView.ViewHolder> {

    private Context mContext;

    public ProfiloEventiAdapter(Context context, OrderedRealmCollection<Event> data) {
        super(data, true);
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProfiloAttivitaViewHolder(new EventLayout(mContext));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ProfiloAttivitaViewHolder realHolder = (ProfiloAttivitaViewHolder) holder;
        Event evento = getItem(position);

        ((EventLayout) realHolder.itemView).bind(evento);
    }

    private class ProfiloAttivitaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ProfiloAttivitaViewHolder(View v) {
            super(v);

            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getLayoutPosition();
            Event evento = getItem(position);
            if (evento != null) {
                EventBus.getDefault().post(new EventSelectedEvent(evento.getUuid()));
            }
        }
    }
}
