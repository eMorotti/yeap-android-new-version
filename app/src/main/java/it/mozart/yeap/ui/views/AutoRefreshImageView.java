package it.mozart.yeap.ui.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.AttributeSet;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import it.mozart.yeap.R;
import it.mozart.yeap.events.UserAvatarChangedEvent;
import it.mozart.yeap.utility.Prefs;
import it.mozart.yeap.utility.Utils;

public class AutoRefreshImageView extends android.support.v7.widget.AppCompatImageView {

    private String mUuid;
    private boolean mIgnoreBind;

    public AutoRefreshImageView(Context context) {
        super(context);
    }

    public AutoRefreshImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public AutoRefreshImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void bindThumb(String uuid) {
        if (mIgnoreBind) {
            return;
        }

        mUuid = uuid;
        if (mUuid == null) {
            Glide.with(getContext()).load(R.drawable.avatar_utente).into(this);
            return;
        }

        String url = Utils.getApiURL() + "/users/" + mUuid + "/avatar?type=thumb&v=" + Prefs.getString(mUuid, "0");
        try {
            Glide.with(getContext()).load(url).asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE).placeholder(R.drawable.avatar_utente)
                    .error(R.drawable.avatar_utente).centerCrop().into(new BitmapImageViewTarget(this) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getContext().getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    setImageDrawable(circularBitmapDrawable);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace(); // Ignoro visto che mi dice "You cannot start a load for a destroyed activity"
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(UserAvatarChangedEvent event) {
        if (mUuid == null) {
            bindThumb(event.getUuid());
        } else if (mUuid.equals(event.getUuid())) {
            bindThumb(mUuid);
        }
    }

    public void setIgnoreBind(boolean ignoreBind) {
        this.mIgnoreBind = ignoreBind;
    }
}
