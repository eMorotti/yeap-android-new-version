package it.mozart.yeap.ui.views;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Keep;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import java.util.ArrayList;
import java.util.List;

import it.mozart.yeap.R;
import it.mozart.yeap.utility.Utils;

public class BorderFabLayout extends FrameLayout {

    public enum POSITION_TYPE {
        DEFAULT, HIDE, DISABLE
    }

    private Paint mPulsePaint;
    private ObjectAnimator animator;
    private CircleTextImageButton mFabButton;

    private float mAnimationProgress;
    private float mClickAnimationOffset;

    private SparseArray<String> mTextResources;
    private List<POSITION_TYPE> mPositionTypes;

    // Parameters
    private float mPropStrokeWidth = 7;
    private float mPropStartOffset = 0;
    private float mPropOffsetIncrement = 3;
    private float mStartScale = 1f;
    private float mStartAlpha = 1f;
    private float mEndScale = 1f;
    private float mEndAlpha = 1f;
    private int mPropColor = Color.RED;
    private int mPropDuration = 200;
    private int mPropOffsetDuration = 100;

    //System

    public BorderFabLayout(Context context) {
        super(context);
        initialize(null);
    }

    public BorderFabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(attrs);
    }

    public BorderFabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(attrs);
    }

    // Inizializzazione interna
    private void initialize(AttributeSet attrs) {
        setWillNotDraw(false);
        setClipChildren(false);
        setClipToPadding(false);

        if (attrs != null) {
            TypedArray a = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.BorderView, 0, 0);
            // Larghezza bordo
            mPropStrokeWidth = a.getDimension(R.styleable.BorderView_bv_stroke_width, Utils.dpToPx(mPropStrokeWidth, getContext()));
            // Distanziamento bordo dall'immagine
            mPropStartOffset = a.getDimension(R.styleable.BorderView_bv_offset, Utils.dpToPx(mPropStartOffset, getContext()));
            // incremento distanza bordo al click
            mPropOffsetIncrement = a.getDimension(R.styleable.BorderView_bv_offset_increment, Utils.dpToPx(mPropOffsetIncrement, getContext()));
            // Colore del bordo
            mPropColor = a.getColor(R.styleable.BorderView_bv_color, mPropColor);
            // dimensione
            a.recycle();
        }

        inflate(getContext(), R.layout.system_border_fab, this);
        mFabButton = findViewById(R.id.fabButton);
        mFabButton.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (getAlpha() > 0.9f) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            if (isEnabled()) {
                                incrementOffset();
                            }
                            break;
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_CANCEL:
                            if (isEnabled()) {
                                decrementOffset();
                            }
                            break;
                    }
                }
                return false;
            }
        });

        setAnimationProgress(1f);
        mClickAnimationOffset = 0f;

        mPulsePaint = new Paint();
        mPulsePaint.setStyle(Paint.Style.STROKE);
        mPulsePaint.setStrokeWidth(mPropStrokeWidth);
        mPulsePaint.setAntiAlias(true);

        mTextResources = new SparseArray<>();
        mPositionTypes = new ArrayList<>();

        postInvalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        mPulsePaint.setColor(mPropColor);
        mPulsePaint.setStrokeWidth(mPropStrokeWidth * mAnimationProgress);

        float radius = (mFabButton.getWidth() / 2 * mFabButton.getScaleX())
                + (mPropStrokeWidth * mAnimationProgress / 2f)
                + mPropStartOffset + mClickAnimationOffset * mPropOffsetIncrement;
        canvas.drawCircle(getWidth() / 2, getHeight() / 2, radius, mPulsePaint);
    }

    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        mFabButton.setOnClickListener(l);
    }

    // ANIMATIONS

    public void forceShow() {
        setAnimationProgress(1f);
    }

    public void show() {
        show(null);
    }

    public void show(final OnVisibilityChangeListener listener) {
        if (animator != null) {
            animator.cancel();
            animator = null;
        }

        animator = ObjectAnimator.ofFloat(this, "animationProgress", mAnimationProgress, 1f);
        animator.setDuration((long) (mPropDuration * (1f)));
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationCancel(Animator animation) {
                animation.removeAllListeners();
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (listener != null)
                    listener.onShown();
            }
        });
        animator.start();
    }

    public void forceHide() {
        setAnimationProgress(0.001f);
    }

    public void hide() {
        hide(null);
    }

    public void hide(final OnVisibilityChangeListener listener) {
        if (animator != null) {
            animator.cancel();
            animator = null;
        }

        animator = ObjectAnimator.ofFloat(this, "animationProgress", mAnimationProgress, 0f);
        animator.setDuration((long) (mPropDuration * 1f));
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationCancel(Animator animation) {
                animation.removeAllListeners();
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (listener != null)
                    listener.onHidden();
            }
        });
        animator.start();
    }

    public void incrementOffset() {
        if (animator != null) {
            animator.cancel();
            animator = null;
        }

        animator = ObjectAnimator.ofFloat(this, "clickAnimationOffset", mClickAnimationOffset, 1f);
        animator.setDuration((long) (mPropOffsetDuration * 1f));
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationCancel(Animator animation) {
                animation.removeAllListeners();
            }
        });
        animator.start();
    }

    public void decrementOffset() {
        if (animator != null) {
            animator.cancel();
            animator = null;
        }

        animator = ObjectAnimator.ofFloat(this, "clickAnimationOffset", mClickAnimationOffset, 0f);
        animator.setDuration((long) (mPropOffsetDuration * 1f));
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationCancel(Animator animation) {
                animation.removeAllListeners();
            }
        });
        animator.start();
    }

    // SETTER E GETTER

    @Keep
    public void setClickAnimationOffset(float percent) {
        if (percent == 0) {
            percent = 0.001f;
        }
        mClickAnimationOffset = percent;

        float scale = 1f + percent / 15f;
        mFabButton.setScaleX(scale);
        mFabButton.setScaleY(scale);

        invalidate();
    }

    @Keep
    private void setAnimationProgress(float value) {
        if (value == 0) {
            value = 0.001f;
        }
        mAnimationProgress = value;

        setAlpha(value);
        mFabButton.setScaleX(value);
        mFabButton.setScaleY(value);

        invalidate();
    }

    private void updateEndValues(POSITION_TYPE type) {
        switch (type) {
            case DEFAULT:
                mEndAlpha = 1f;
                mEndScale = 1f;
                break;
            case HIDE:
                mEndAlpha = 0f;
                mEndScale = 0f;
                break;
            case DISABLE:
                mEndAlpha = 0.5f;
                mEndScale = 1f;
                break;
        }
    }

    private void updateStartValues(POSITION_TYPE type) {
        switch (type) {
            case DEFAULT:
                mStartAlpha = 1f;
                mStartScale = 1f;
                break;
            case HIDE:
                mStartAlpha = 0f;
                mStartScale = 0f;
                break;
            case DISABLE:
                mStartAlpha = 0.5f;
                mStartScale = 1f;
                break;
        }
    }

    public void updateAnimationValue(int position, float positionOffset) {
        float value;
        if (positionOffset >= 0.5f) {
            value = Utils.toRange(positionOffset, 0.5f, 1f, 0f, 1f);
        } else {
            value = Utils.toRange(positionOffset, 0f, 0.5f, 1f, 0f);
        }

        if (positionOffset >= 0.5f) {
            updateStartValues(mPositionTypes.get(position));
            updateEndValues(mPositionTypes.get(position + 1));
        } else {
            updateStartValues(mPositionTypes.get(position));
            updateEndValues(mPositionTypes.get(position));
        }

        float alpha = Utils.toRange(positionOffset, positionOffset >= 0.5f ? 0.5f : 0f, positionOffset >= 0.5f ? 1f : 0.5f, mStartAlpha, mEndAlpha);
        float scale = Utils.toRange(positionOffset, positionOffset >= 0.5f ? 0.5f : 0f, positionOffset >= 0.5f ? 1f : 0.5f, mStartScale, mEndScale);
        setAnimationValue(value, alpha, scale);

        if (mTextResources.get(position) != null) {
            mFabButton.setText1(mTextResources.get(position));
        } else {
            mFabButton.setText1("");
        }
        if (mTextResources.get(position + 1) != null) {
            mFabButton.setText2(mTextResources.get(position + 1));
        } else {
            mFabButton.setText2("");
        }
        mFabButton.update(position, positionOffset);
    }

    private void setAnimationValue(float value, float alphaValue, float scaleValue) {
        mAnimationProgress = value;

        float alpha;
        if (alphaValue < 0.25f) {
            alpha = 0.001f;
        } else {
            alpha = Utils.toRange(alphaValue, 0.25f, 1, 0, 1);
        }
        setAlpha(alpha);
        float scale;
        if (scaleValue < 0.25f) {
            scale = 0.4f;
        } else {
            scale = Utils.toRange(scaleValue, 0.25f, 1, 0.4f, 1);
        }
        mFabButton.setScaleX(scale);
        mFabButton.setScaleY(scale);

        invalidate();
    }

    public void setPositionTypes(List<POSITION_TYPE> positionTypes) {
        this.mPositionTypes = positionTypes;
    }

    public ImageButton getFabButton() {
        return mFabButton;
    }

    public void setTextResources(SparseArray<String> resources) {
        this.mTextResources = resources;
    }

    // LISTENER

    public interface OnVisibilityChangeListener {
        void onHidden();

        void onShown();
    }
}
