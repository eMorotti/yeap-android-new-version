package it.mozart.yeap.ui.views;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;

import it.mozart.yeap.R;

public class DynamicView extends CoordinatorLayout {

    private LayoutInflater mLayoutInflater;
    private boolean mContentShown;

    public DynamicView(Context context) {
        super(context);
        initialize();
    }

    public DynamicView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public DynamicView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    private void initialize() {
        mLayoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContentShown = true;
        setFitsSystemWindows(true);
    }

    public void showLoading() {
        removeAllViews();
        mLayoutInflater.inflate(R.layout.system_loading, this);
        mContentShown = false;
    }

    public void hide() {
        removeAllViews();
        mContentShown = true;
    }

    public boolean isContentShown() {
        return mContentShown;
    }
}
