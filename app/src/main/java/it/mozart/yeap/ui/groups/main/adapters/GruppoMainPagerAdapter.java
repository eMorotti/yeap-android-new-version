package it.mozart.yeap.ui.groups.main.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import it.mozart.yeap.ui.groups.main.Frg_GruppoMainChat;
import it.mozart.yeap.ui.groups.main.Frg_GruppoMainEventi;
import it.mozart.yeap.ui.groups.main.Frg_GruppoMainInvitati;
import it.mozart.yeap.ui.views.SmartPagerAdapter;

public class GruppoMainPagerAdapter extends SmartPagerAdapter {

    private Context mContext;
    private String mGruppoId;

    public GruppoMainPagerAdapter(Context context, FragmentManager fragmentManager, String id) {
        super(fragmentManager);
        mContext = context;
        mGruppoId = id;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return Frg_GruppoMainChat.newInstance(mGruppoId);
            case 1:
                return Frg_GruppoMainEventi.newInstance(mGruppoId);
            case 2:
                return Frg_GruppoMainInvitati.newInstance(mGruppoId);
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
