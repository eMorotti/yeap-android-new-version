package it.mozart.yeap.ui.home.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;
import it.mozart.yeap.events.EventLongSelectedEvent;
import it.mozart.yeap.events.EventSelectedEvent;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.ui.home.views.EventLayout;

public class HomeEventsAdapter extends RealmRecyclerViewAdapter<Event, RecyclerView.ViewHolder> {

    private Context mContext;

    public HomeEventsAdapter(Context context, OrderedRealmCollection<Event> data) {
        super(data, true);
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MainEventsViewHolder(new EventLayout(mContext));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final MainEventsViewHolder realHolder = (MainEventsViewHolder) holder;
        Event event = getItem(position);

        ((EventLayout) realHolder.itemView).bind(event);
    }

    private class MainEventsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        MainEventsViewHolder(View v) {
            super(v);

            v.setOnClickListener(this);
            v.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getLayoutPosition();
            Event event = getItem(position);
            if (event != null) {
                EventBus.getDefault().post(new EventSelectedEvent(event.getUuid()));
            }
        }

        @Override
        public boolean onLongClick(View v) {
            int position = getLayoutPosition();
            Event event = getItem(position);
            if (event != null) {
                EventBus.getDefault().post(new EventLongSelectedEvent(event.getUuid(), event.getTitle(), v));
                return true;
            }
            return false;
        }
    }
}
