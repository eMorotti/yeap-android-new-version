package it.mozart.yeap.ui.support;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.flurgle.camerakit.CameraKit;
import com.flurgle.camerakit.CameraListener;
import com.flurgle.camerakit.CameraView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import butterknife.BindView;
import butterknife.OnClick;
import it.mozart.yeap.R;
import it.mozart.yeap.helpers.ImageInputHelper;
import it.mozart.yeap.ui.base.BaseActivityWithSyncService;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.Prefs;
import it.mozart.yeap.utility.Utils;

public class Act_RecuperaImage extends BaseActivityWithSyncService {

    @BindView(R.id.camera_view)
    CameraView mCameraView;
    @BindView(R.id.recyclerview)
    RecyclerView mRecyclerView;
    @BindView(R.id.flash)
    ImageButton mFlash;

    @OnClick(R.id.flash)
    void flashClicked() {
        mCurrentFlash = (mCurrentFlash + 1) % FLASH_OPTIONS.length;
        mCameraView.setFlash(FLASH_OPTIONS[mCurrentFlash]);
        mFlash.setImageResource(FLASH_ICONS[mCurrentFlash]);
    }

    @OnClick(R.id.take_photo)
    void takePhotoClicked() {
        if (!mIsTakingPicture) {
            mCameraView.captureImage();
        }
    }

    @OnClick(R.id.gallery)
    void galleryClicked() {
        ImageInputHelper.openGalleryIntent(this, IMG_GALLERY_CODE);
    }

    @OnClick(R.id.camera)
    void cameraSwitchClicked() {
        mCameraView.toggleFacing();
    }

    // SUPPORT

    private Handler mBackgroundHandler;
    private boolean mIsTakingPicture;
    private int mCurrentFlash;

    private static final int[] FLASH_OPTIONS = {
            CameraKit.Constants.FLASH_AUTO,
            CameraKit.Constants.FLASH_OFF,
            CameraKit.Constants.FLASH_ON,
    };
    private static final int[] FLASH_ICONS = {
            R.drawable.ic_flash_auto_white_24dp,
            R.drawable.ic_flash_off_white_24dp,
            R.drawable.ic_flash_on_white_24dp,
    };
    private static final int RETURN_CODE = 1234;
    private static final int IMG_GALLERY_CODE = 12345;

    // SYSTEM

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            mToolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
            if (Utils.isLollipop()) {
                ((ViewGroup.MarginLayoutParams) mToolbar.getLayoutParams()).topMargin = Utils.getStatusBarHeight(this);
                ((ViewGroup.MarginLayoutParams) ((View) mFlash.getParent()).getLayoutParams()).topMargin = Utils.getStatusBarHeight(this);
            }
        }

        mCameraView.setPermissions(CameraKit.Constants.PERMISSIONS_PICTURE);
//        mCameraView.setFocus(CameraKit.Constants.FOCUS_TAP);
        mCameraView.setCameraListener(new CameraListener() {
            @Override
            public void onPictureTaken(final byte[] picture) {
                super.onPictureTaken(picture);
                getBackgroundHandler().post(new Runnable() {
                    @Override
                    public void run() {
                        File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "picture.jpg");
                        OutputStream os = null;
                        try {
                            os = new FileOutputStream(file);
                            os.write(picture);
                            os.close();
                            mIsTakingPicture = false;
                            startActivityForResult(Act_RecuperaImageEdit.newIntent(file.getAbsolutePath()), RETURN_CODE);
                        } catch (IOException e) {
                            Toast.makeText(Act_RecuperaImage.this, getString(R.string.general_error_message), Toast.LENGTH_SHORT).show();
                        } finally {
                            if (os != null) {
                                try {
                                    os.close();
                                } catch (IOException ignored) {
                                }
                            }
                        }
                    }
                });
            }
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_recupera_image;
    }

    @Override
    protected void initVariables() {
        mIsTakingPicture = false;
        mCurrentFlash = Prefs.getInt(Constants.FLASH_PARAM, 0);
    }

    @Override
    protected void loadParameters(Bundle extras) {

    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        mCameraView.start();
    }

    @Override
    protected void onStop() {
        mCameraView.stop();
        super.onStop();
    }

    @Override
    protected void initialize() {
        mFlash.setImageResource(FLASH_ICONS[mCurrentFlash]);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case RETURN_CODE:
                    // Ritorno le informazioni all'activity che ha richiamato la selezione di un'immagine
                    String imagePath = data.getStringExtra(Constants.IMAGE_PATH);
                    String caption = data.getStringExtra(Constants.CAPTION);
                    Intent intent = new Intent();
                    if (imagePath != null)
                        intent.putExtra(Constants.IMAGE_PATH, imagePath);
                    if (caption != null)
                        intent.putExtra(Constants.CAPTION, caption);
                    setResult(RESULT_OK, intent);
                    finish();
                    break;
                case IMG_GALLERY_CODE:
                    Uri imageUri = data.getData();
                    startActivityForResult(Act_RecuperaImageEdit.newIntent(imageUri), RETURN_CODE);
                    break;
            }
        }
    }

    // FUNCTIONS

    /**
     * Gestico il thread per la persistenza dell'immagine acquisita da fotocamera
     *
     * @return handler
     */

    private Handler getBackgroundHandler() {
        if (mBackgroundHandler == null) {
            HandlerThread thread = new HandlerThread("background");
            thread.start();
            mBackgroundHandler = new Handler(thread.getLooper());
        }
        return mBackgroundHandler;
    }
}
