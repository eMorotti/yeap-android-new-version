package it.mozart.yeap.ui.events.main.events;

public class VotePollItemClickedEvent {

    private String sondaggioVoceId;
    private boolean checked;

    public VotePollItemClickedEvent(String sondaggioVoceId, boolean checked) {
        this.sondaggioVoceId = sondaggioVoceId;
        this.checked = checked;
    }

    public String getSondaggioVoceId() {
        return sondaggioVoceId;
    }

    public boolean isChecked() {
        return checked;
    }
}
