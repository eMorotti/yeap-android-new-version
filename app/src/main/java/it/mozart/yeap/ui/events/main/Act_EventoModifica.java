package it.mozart.yeap.ui.events.main;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.parceler.Parcels;

import java.io.File;
import java.util.UUID;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.api.models.APIError;
import it.mozart.yeap.api.models.EventoApiModel;
import it.mozart.yeap.api.models.ImageApiModel;
import it.mozart.yeap.api.services.EventApi;
import it.mozart.yeap.api.services.UploadApi;
import it.mozart.yeap.common.DoveInfo;
import it.mozart.yeap.common.QuandoInfo;
import it.mozart.yeap.events.ApiCallResultEvent;
import it.mozart.yeap.helpers.AnalitycsProvider;
import it.mozart.yeap.helpers.ImageInputHelper;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.EventInfo;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.ui.base.BaseActivityWithSyncService;
import it.mozart.yeap.ui.support.Act_Dove;
import it.mozart.yeap.ui.support.Act_ImageCrop;
import it.mozart.yeap.ui.support.Act_Quando;
import it.mozart.yeap.ui.views.AnimCheckBox;
import it.mozart.yeap.utility.APIErrorUtils;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.DateUtils;
import it.mozart.yeap.utility.Utils;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class Act_EventoModifica extends BaseActivityWithSyncService {

    @BindView(R.id.scrollview)
    View mScrollView;

    @BindView(R.id.avatar)
    ImageView mAvatar;
    @BindView(R.id.modifica_desc)
    EditText mModificaDesc;
    @BindView(R.id.perm_inv_check)
    AnimCheckBox mModificaPermessoInvitati;

    @BindView(R.id.cosa)
    EditText mCosa;
    @BindView(R.id.dove)
    TextView mDove;
    @BindView(R.id.dove_delete)
    ImageButton mDoveDelete;
    @BindView(R.id.quando)
    TextView mQuando;
    @BindView(R.id.quando_delete)
    ImageButton mQuandoDelete;

    @OnClick(R.id.layout)
    void layoutClicked() {
        Utils.hideKeyboard(this);
    }

    @OnClick(R.id.avatar)
    void avatarClicked() {
        avatarClickedMethod(false);
    }

    @OnClick({R.id.perm_inv_check, R.id.modifica_perm_inv})
    void permInvClicked() {
        mModificaPermessoInvitati.setChecked(!mModificaPermessoInvitati.isChecked());
    }

    @OnClick(R.id.dove)
    void doveClicked() {
        AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "changeActivityWhere", "modify");
        startActivityForResult(Act_Dove.newIntent(createDoveTitle(), createDoveSubtitle(), null), DOVE_CODE);
    }

    @OnClick(R.id.dove_delete)
    void doveDeleteClicked() {
        mDoveInfoModel = mEvento.getEventInfo().generateDoveModel();
        mDove.setText(mDoveInfoModel.getDove());
    }

    @OnClick(R.id.quando)
    void quandoClicked() {
        AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "changeActivityWhen", "modify");
        startActivityForResult(Act_Quando.newIntent(createQuandoTitle(), createQuandoSubtitle(), mQuandoInfoModel), QUANDO_CODE);
    }

    @OnClick(R.id.quando_delete)
    void quandoDeleteClicked() {
        mQuandoInfoModel = mEvento.getEventInfo().generateQuandoModel();
        mQuando.setText(DateUtils.createQuandoText(mQuandoInfoModel));
    }

    // SUPPORT

    @Inject
    Realm realm;
    @Inject
    EventApi eventApi;
    @Inject
    UploadApi uploadApi;

    private Event mEvento;
    private DoveInfo mDoveInfoModel;
    private QuandoInfo mQuandoInfoModel;
    private File mAvatarFile;
    private String mId;
    private boolean mTitleChanged;
    private boolean mDoveChanged;
    private boolean mQuandoChanged;
    private boolean mDescChanged;

    private static final String PARAM_ID = "paramId";

    private static final int IMG_CODE = 101;
    private static final int IMG_CROP_CODE = 102;
    private static final int QUANDO_CODE = 3001;
    private static final int DOVE_CODE = 3002;

    // SYSTEM

    public static Intent newIntent(String eventoId) {
        Intent intent = new Intent(App.getContext(), Act_EventoModifica.class);
        intent.putExtra(PARAM_ID, eventoId);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUseBus(true);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_evento_modifica;
    }

    @Override
    protected void initVariables() {
        mDoveInfoModel = null;
        mQuandoInfoModel = null;
        mTitleChanged = false;
        mDoveChanged = false;
        mQuandoChanged = false;
        mDescChanged = false;
    }

    @Override
    protected void loadParameters(Bundle extras) {
        mId = extras.getString(PARAM_ID);
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem salva = menu.findItem(R.id.menu_salva);
        if (salva != null) {
            salva.setEnabled(mTitleChanged || mDoveChanged || mQuandoChanged || mDescChanged || mAvatarFile != null
                    || mEvento.isUsersCanInvite() != mModificaPermessoInvitati.isChecked());
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_evento_modifica, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        EventoApiModel model = new EventoApiModel();
        model.setStatus(mEvento.getStatus());
        switch (item.getItemId()) {
            case R.id.menu_ripristina:
                model.setStatus(Constants.EVENTO_CONFERMATO);
            case R.id.menu_salva:
                if (mCosa.getText().length() == 0) {
                    Toast.makeText(this, getString(R.string.dettaglio_evento_errore_inserire_qualcosa), Toast.LENGTH_SHORT).show();
                    break;
                }

                AnalitycsProvider.getInstance().logCustom(mFirebaseAnalytics, "modifyActivityConfirm");
                getDynamicView().showLoading();

                setRequestId(UUID.randomUUID().toString());
                model.setDesc(mModificaDesc.getText().toString());
                if (mEvento.isUsersCanInvite() != mModificaPermessoInvitati.isChecked()) {
                    AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "changeActivitySettings", "modify");
                }
                model.setUsersCanInvite(mModificaPermessoInvitati.isChecked());
                if (mAvatarFile == null) {
                    model.setAvatarLargeUrl(mEvento.getAvatarLargeUrl());
                    model.setAvatarThumbUrl(mEvento.getAvatarThumbUrl());
                }
                model.setEventoInfo(new EventInfo());
                if (mEvento.getStatus() == Constants.EVENTO_CONFERMATO) {
                    model.getEventoInfo().updateDoveInfo(mDoveInfoModel);
                    model.getEventoInfo().updateQuandoInfo(mQuandoInfoModel);
                } else {
                    if (!mEvento.isWherePollEnabled()) {
                        model.getEventoInfo().updateDoveInfo(mDoveInfoModel);
                    }
                    if (!mEvento.isWhenPollEnabled()) {
                        model.getEventoInfo().updateQuandoInfo(mQuandoInfoModel);
                    }
                }
                if (!mEvento.getEventInfo().getWhat().equals(mCosa.getText().toString())) {
                    AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "changeActivityWhat", "modify");
                }
                model.getEventoInfo().setWhat(mCosa.getText().toString());
                if (mAvatarFile == null) {
                    updateEvento(model);
                } else {
                    uploadAvatar(model);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void initialize() {
        // Init attività
        mEvento = new EventDao(realm).getEvent(mId);
        if (mEvento == null) {
            Toast.makeText(this, getString(R.string.general_error_evento_non_presente), Toast.LENGTH_LONG).show();
            finish();
        } else {
            updateLayout(mEvento);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.IMAGE_PERMISSION_CODE:
                avatarClickedMethod(true);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case IMG_CODE:
                    // Chiamo l'activity per il crop dell'immagine selezionata
                    Uri copertinaUri = ImageInputHelper.getPickImageResultUri(this, data);
                    startActivityForResult(Act_ImageCrop.newIntentFreeRatio(copertinaUri), IMG_CROP_CODE);
                    break;
                case IMG_CROP_CODE:
                    final String image = data.getStringExtra(Constants.IMAGE_CROP);
                    if (image != null) {
                        mAvatarFile = new File(image);
                        Glide.with(this).load(mAvatarFile).asBitmap().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).centerCrop().into(new BitmapImageViewTarget(mAvatar) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                mAvatar.setImageDrawable(circularBitmapDrawable);
                            }
                        });
                    } else {
                        // Mostro messaggio di errore se non è stato restituito niente dall'activity di crop
                        Toast.makeText(this, getString(R.string.general_error_ritaglio_immagine), Toast.LENGTH_LONG).show();
                    }
                    invalidateOptionsMenu();
                    break;
                case DOVE_CODE:
                    AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "changeActivityWhereConfirm", "modify");
                    // Recupero informazioni sul dove
                    mDoveInfoModel = Parcels.unwrap(data.getParcelableExtra(Constants.INTENT_DOVE));
                    // Aggiorno layout
                    if (mDoveInfoModel != null && mDoveInfoModel.getDove() != null) {
                        mDove.setText(mDoveInfoModel.getDove());
                    } else {
                        mDove.setText(null);
                        mDoveInfoModel = null;
                    }
                    break;
                case QUANDO_CODE:
                    AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "changeActivityWhenConfirm", "modify");
                    // Recupero informazioni sul quando
                    mQuandoInfoModel = Parcels.unwrap(data.getParcelableExtra(Constants.INTENT_QUANDO));
                    // Aggiorno layout
                    if (mQuandoInfoModel != null) {
                        if (mQuandoInfoModel.getDataDa() == null && mQuandoInfoModel.getOraDa() == null
                                && mQuandoInfoModel.getDataA() == null && mQuandoInfoModel.getOraA() == null) {
                            mQuando.setText(null);
                            mQuandoInfoModel = null;
                        } else {
                            mQuando.setText(DateUtils.createQuandoText(mQuandoInfoModel));
                        }
                    } else {
                        mQuando.setText(null);
                    }
                    break;
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ApiCallResultEvent event) {
        if (event.getRequestId().equals(getRequestId())) {
            cancelTimer();
            Toast.makeText(this, getString(R.string.main_evento_attivita_modificata), Toast.LENGTH_SHORT).show();
            setResult(Activity.RESULT_OK);
            finish();
        }
    }

    // FUNCTIONS

    /**
     * Aggiorno layout
     *
     * @param evento attività
     */
    private void updateLayout(final Event evento) {
        // Toolbar
        setToolbarTitle(evento.getTitle());
        setToolbarSubtitle(getString(R.string.dettaglio_evento_sottotitolo_modifica));

        // Info
        mModificaDesc.setText(evento.getDesc());
        mModificaDesc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean changed = (mEvento.getDesc() != null && !mEvento.getDesc().equals(s.toString())
                        || (mEvento.getDesc() == null && !s.toString().isEmpty()));
                if (mDescChanged != changed) {
                    if (changed) {
                        mModificaDesc.setBackgroundResource(R.drawable.background_transparent_white_changed_for_differences);
                    } else {
                        mModificaDesc.setBackgroundResource(R.drawable.background_transparent_white_for_differences);
                    }
                    mDescChanged = changed;
                    invalidateOptionsMenu();
                }
            }
        });

        // Proposta
        mCosa.setText(evento.getEventInfo().getWhat());
        mCosa.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean changed = !mEvento.getTitle().equals(s.toString());
                if (mTitleChanged != changed) {
                    if (changed) {
                        mCosa.setBackgroundResource(R.drawable.background_transparent_white_changed_for_differences_small);
                    } else {
                        mCosa.setBackgroundResource(R.drawable.background_transparent_white_for_differences_small);
                    }
                    mTitleChanged = changed;
                    invalidateOptionsMenu();
                }
            }
        });

        if (mEvento.getStatus() != Constants.EVENTO_CONFERMATO) {
            if (mEvento.isWherePollEnabled()) {
                mDove.setText(getString(R.string.evento_dove_non_definito));
                mDove.setEnabled(false);
            }

            if (mEvento.isWhenPollEnabled()) {
                mQuando.setText(getString(R.string.evento_quando_non_definito));
                mQuando.setEnabled(false);
            }
        }

        mDoveInfoModel = evento.getEventInfo().generateDoveModel();
        if (evento.getEventInfo().getWhere() != null) {
            mDove.setText(evento.getEventInfo().getWhere());
        }
        mDove.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean changed = (mEvento.getEventInfo().getWhere() != null && !mEvento.getEventInfo().getWhere().equals(s.toString())
                        || (mEvento.getEventInfo().getWhere() == null && !s.toString().isEmpty()));
                if (mDoveChanged != changed) {
                    if (changed) {
                        mDoveDelete.setVisibility(View.VISIBLE);
                        mDove.setBackgroundResource(R.drawable.background_transparent_white_changed_for_differences_small);
                    } else {
                        mDoveDelete.setVisibility(View.GONE);
                        mDove.setBackgroundResource(R.drawable.background_transparent_white_for_differences_small);
                    }
                    mDoveChanged = changed;
                    invalidateOptionsMenu();
                }
            }
        });

        mQuandoInfoModel = evento.getEventInfo().generateQuandoModel();
        final String quando = DateUtils.createQuandoText(mQuandoInfoModel);
        if (quando != null) {
            mQuando.setText(quando);
        }
        mQuando.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean changed = (quando != null && !quando.equals(s.toString())
                        || (quando == null && !s.toString().isEmpty()));
                if (mQuandoChanged != changed) {
                    if (changed) {
                        mQuandoDelete.setVisibility(View.VISIBLE);
                        mQuando.setBackgroundResource(R.drawable.background_transparent_white_changed_for_differences_small);
                    } else {
                        mQuandoDelete.setVisibility(View.GONE);
                        mQuando.setBackgroundResource(R.drawable.background_transparent_white_for_differences_small);
                    }
                    mQuandoChanged = changed;
                    invalidateOptionsMenu();
                }
            }
        });

        // Permessi
        mModificaPermessoInvitati.setChecked(evento.isUsersCanInvite(), false);
        mModificaPermessoInvitati.setOnCheckedChangeListener(new AnimCheckBox.OnCheckedChangeListener() {
            @Override
            public void onChange(boolean checked) {
                invalidateOptionsMenu();
            }
        });

        // Avatar
        Glide.with(this).load(evento.getAvatarThumbUrl()).asBitmap().centerCrop().into(new BitmapImageViewTarget(mAvatar) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                mAvatar.setImageDrawable(circularBitmapDrawable);
            }

            @Override
            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                Glide.with(Act_EventoModifica.this).load(R.drawable.avatar_attivita).asBitmap().centerCrop().into(new BitmapImageViewTarget(mAvatar) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        mAvatar.setImageDrawable(circularBitmapDrawable);
                    }
                });
            }
        });
    }

    /**
     * Eseguo l'upload dell'avatar e il seguente update dell'evento
     *
     * @param model modello per le api
     */
    private void uploadAvatar(final EventoApiModel model) {
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), mAvatarFile);
        final MultipartBody.Part image = MultipartBody.Part.createFormData("image", mAvatarFile.getName(), requestFile);
        registerSubscription(uploadApi.uploadAvatar(image)
                .subscribeOn(Schedulers.newThread())
                .flatMap(new Func1<ImageApiModel, Observable<Event>>() {
                    @Override
                    public Observable<Event> call(ImageApiModel imageApiModel) {
                        setRequestId(UUID.randomUUID().toString());
                        model.setAvatar(imageApiModel.getUploadref());
                        return eventApi.update(getRequestId(), mId, model);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Event>() {
                    @Override
                    public void call(Event evento) {
                        startTimer();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        getDynamicView().hide();

                        APIError error = APIErrorUtils.parseError(Act_EventoModifica.this, throwable);
                        Toast.makeText(Act_EventoModifica.this, error.message(), Toast.LENGTH_LONG).show();
                    }
                }));
    }

    /**
     * Eseguo l'update dell'evento
     *
     * @param model modello per le api
     */
    private void updateEvento(EventoApiModel model) {
        registerSubscription(eventApi.update(getRequestId(), mId, model)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Event>() {
                    @Override
                    public void call(Event evento) {
                        startTimer();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        getDynamicView().hide();

                        APIError error = APIErrorUtils.parseError(Act_EventoModifica.this, throwable);
                        Toast.makeText(Act_EventoModifica.this, error.message(), Toast.LENGTH_SHORT).show();
                    }
                }));
    }

    /**
     * Crea il titolo per l'activity Dove
     *
     * @return titolo
     */
    private String createDoveTitle() {
        return getString(R.string.nuova_evento_dove_title);
    }

    /**
     * Crea il sottotitolo per l'activity Dove
     *
     * @return sottotitolo
     */
    private String createDoveSubtitle() {
        return getString(R.string.nuova_evento_dove_subtitle);
    }

    /**
     * Crea il titolo per l'activity Quando
     *
     * @return titolo
     */
    private String createQuandoTitle() {
        return getString(R.string.nuova_evento_quando_title);
    }

    /**
     * Crea il sottotitolo per l'activity Quando
     *
     * @return sottotitolo
     */
    private String createQuandoSubtitle() {
        return getString(R.string.nuova_evento_quando_subtitle);
    }

    private void avatarClickedMethod(boolean ignoreCheck) {
        ImageInputHelper.openChooserForAvatarSelect(this, IMG_CODE, ignoreCheck);
    }
}
