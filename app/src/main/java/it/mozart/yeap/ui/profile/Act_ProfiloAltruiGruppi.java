package it.mozart.yeap.ui.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import org.greenrobot.eventbus.Subscribe;

import javax.inject.Inject;

import butterknife.BindView;
import io.realm.Realm;
import io.realm.RealmResults;
import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.events.GroupSelectedEvent;
import it.mozart.yeap.realm.Group;
import it.mozart.yeap.realm.dao.GroupDao;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.ui.base.BaseActivityWithSyncService;
import it.mozart.yeap.ui.groups.main.Act_GruppoMain;
import it.mozart.yeap.ui.profile.adapters.ProfiloGruppiAdapter;

public class Act_ProfiloAltruiGruppi extends BaseActivityWithSyncService {

    @BindView(R.id.recyclerview)
    RecyclerView mRecyclerView;
    @BindView(R.id.no_items)
    View mNoItems;

    // SUPPORT

    @Inject
    Realm realm;
    @Inject
    NotificationsHelper notifications;

    private String[] mIds;

    private static final String PARAM_IDS = "paramIds";

    // SYSTEM

    public static Intent newIntent(String[] attivitaIds) {
        Intent intent = new Intent(App.getContext(), Act_ProfiloAltruiGruppi.class);
        intent.putExtra(PARAM_IDS, attivitaIds);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUseBus(true);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_profilo_gruppi;
    }

    @Override
    protected void initVariables() {

    }

    @Override
    protected void loadParameters(Bundle extras) {
        mIds = extras.getStringArray(PARAM_IDS);
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    protected void initialize() {
        if (mIds != null && mIds.length > 0) {
            // Init recyclerView
            mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            RealmResults<Group> list = new GroupDao(realm).getListGroupsFromIdList(mIds);
            mRecyclerView.setAdapter(new ProfiloGruppiAdapter(this, list));

            if (list == null || list.size() == 0) {
                mRecyclerView.setVisibility(View.GONE);
                mNoItems.setVisibility(View.VISIBLE);
            }
        } else {
            mRecyclerView.setVisibility(View.GONE);
            mNoItems.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe
    public void onEvent(GroupSelectedEvent event) {
        notifications.cancelAllNotification(this);
        Intent intent = Act_GruppoMain.newIntent(event.getId());
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
