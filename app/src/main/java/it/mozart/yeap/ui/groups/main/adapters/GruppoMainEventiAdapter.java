package it.mozart.yeap.ui.groups.main.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;
import it.mozart.yeap.events.EventSelectedEvent;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.ui.home.views.EventLayout;

public class GruppoMainEventiAdapter extends RealmRecyclerViewAdapter<Event, RecyclerView.ViewHolder> {

    private Context mContext;

    public GruppoMainEventiAdapter(Context context, OrderedRealmCollection<Event> data) {
        super(data, true);
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MainAttivitaViewHolder(new EventLayout(mContext));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final MainAttivitaViewHolder realHolder = (MainAttivitaViewHolder) holder;
        Event evento = getItem(position);

        ((EventLayout) realHolder.itemView).bind(evento);
    }

    private class MainAttivitaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        MainAttivitaViewHolder(View v) {
            super(v);

            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getLayoutPosition();
            Event evento = getData().get(position);
            EventBus.getDefault().post(new EventSelectedEvent(evento.getUuid()));
        }
    }
}
