package it.mozart.yeap.ui.events.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.NestedScrollView;

import org.greenrobot.eventbus.Subscribe;

import javax.inject.Inject;

import butterknife.BindView;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import it.mozart.yeap.R;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.Poll;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.PollDao;
import it.mozart.yeap.realm.dao.VoteDao;
import it.mozart.yeap.ui.base.BaseFragment;
import it.mozart.yeap.ui.events.main.events.GoToPollEvent;
import it.mozart.yeap.ui.events.main.events.VotePollItemEventClickedEvent;
import it.mozart.yeap.ui.events.main.views.EventoInfoLayout;
import it.mozart.yeap.ui.events.main.views.VoteLayout;

public class Frg_EventoMainInfo extends BaseFragment {

    @BindView(R.id.scrollview)
    NestedScrollView mScrollView;
    @BindView(R.id.evento_info_layout)
    EventoInfoLayout mEventoInfoLayout;
    @BindView(R.id.main_vote_layout)
    VoteLayout mVoteLayout;

    // SUPPORT

    @Inject
    Realm realm;

    private PollDao mPollDao;
    private Event mEvento;
    private Poll mWherePoll;
    private Poll mWhenPoll;
    private String mId;

    private static final String PARAM_ID = "paramId";

    // SYSTEM

    public static Frg_EventoMainInfo newInstance(String attivitaId) {
        Bundle args = new Bundle();
        args.putString(PARAM_ID, attivitaId);
        Frg_EventoMainInfo fragment = new Frg_EventoMainInfo();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUseBus(true);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_main_evento_info;
    }

    @Override
    protected void initValues() {

    }

    @Override
    protected void loadParameters(Bundle arguments) {
        mId = arguments.getString(PARAM_ID);
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mEvento != null) {
            mEvento.removeAllChangeListeners();
        }
        if (mWherePoll != null) {
            mWherePoll.removeAllChangeListeners();
        }
        if (mWhenPoll != null) {
            mWhenPoll.removeAllChangeListeners();
        }
    }

    @Override
    protected void initialize() {
        final VoteDao voteDao = new VoteDao(realm);
        mPollDao = new PollDao(realm);
        // Init attività
        mEvento = new EventDao(realm).getEvent(mId);
        mEventoInfoLayout.bindEvento(mEvento, voteDao, mPollDao);
        mVoteLayout.bindEvento(mEvento);

        mEvento.addChangeListener(new RealmChangeListener<Event>() {
            @Override
            public void onChange(@NonNull Event element) {
                if (element.isValid()) {
                    mEventoInfoLayout.bindEvento(element, voteDao, mPollDao);
                    mVoteLayout.bindEvento(element);
                    bindPolls(element, mPollDao);
                }
            }
        });

        if (mEvento != null) {
            bindPolls(mEvento, mPollDao);
        }
    }

    @Subscribe
    public void onEvent(GoToPollEvent event) {
        mScrollView.scrollTo(0, event.getPos());
    }

    @Subscribe
    public void onEvent(VotePollItemEventClickedEvent event) {
        mEventoInfoLayout.managePolls(mEvento, mPollDao);
    }

    // FUNCTIONS

    private void bindPolls(final Event evento, PollDao pollDao) {
        if (mWherePoll != null) {
            mWherePoll.removeAllChangeListeners();
        }
        if (mWhenPoll != null) {
            mWhenPoll.removeAllChangeListeners();
        }

        if (evento.getWherePollUuid() != null) {
            mWherePoll = pollDao.getPoll(evento.getWherePollUuid());
            mWherePoll.addChangeListener(new RealmChangeListener<Poll>() {
                @Override
                public void onChange(@NonNull Poll poll) {
                    mEventoInfoLayout.bindPoll(evento, poll, "where");
                }
            });
        }
        if (evento.getWhenPollUuid() != null) {
            mWhenPoll = pollDao.getPoll(evento.getWhenPollUuid());
            mWhenPoll.addChangeListener(new RealmChangeListener<Poll>() {
                @Override
                public void onChange(@NonNull Poll poll) {
                    mEventoInfoLayout.bindPoll(evento, poll, "when");
                }
            });
        }
    }
}
