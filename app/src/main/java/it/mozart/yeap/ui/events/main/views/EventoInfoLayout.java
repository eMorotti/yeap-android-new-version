package it.mozart.yeap.ui.events.main.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.CardView;
import android.text.style.RelativeSizeSpan;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.events.AvatarSelectedEvent;
import it.mozart.yeap.events.UserImageSelectedEvent;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.EventInfo;
import it.mozart.yeap.realm.Poll;
import it.mozart.yeap.realm.Vote;
import it.mozart.yeap.realm.dao.PollDao;
import it.mozart.yeap.realm.dao.VoteDao;
import it.mozart.yeap.ui.events.main.events.EventInfoSelectedEvent;
import it.mozart.yeap.ui.events.main.events.GoToPollEvent;
import it.mozart.yeap.ui.events.main.events.ShowVotiEvent;
import it.mozart.yeap.ui.events.main.events.ShowVotiInteresseEvent;
import it.mozart.yeap.ui.sondaggi.views.SondaggioEventLayout;
import it.mozart.yeap.ui.views.VotantiWidget;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.DateUtils;
import it.mozart.yeap.utility.EasySpan;
import it.mozart.yeap.utility.Utils;

public class EventoInfoLayout extends FrameLayout {

    private CardView mCardView;
    private ImageView mPoster;
    private TextView mTitle;
    private TextView mDove;
    private ImageView mDoveMore;
    private View mDoveLayout;
    private TextView mQuando;
    private ImageView mQuandoMore;
    private View mQuandoLayout;
    private TextView mStatus;

    private ViewGroup mPollLayout;

    private ImageView mCreatoDaAvatar;
    private TextView mCreatoDa;
    private TextView mDettaglio;

    private View mVoteDetailLayout;

    private TextView mNumInteressatiYes;
    private VotantiWidget mInteressati;
    private View mInteressatiLayout;

    private TextView mNumYes;
    private VotantiWidget mConfermati;
    private TextView mNumNo;
    private VotantiWidget mNonConfermati;
    private View mVoteLayout;

    private TextView mVoteTitle;
    private TextView mNonAncoraVotato;

    public EventoInfoLayout(Context context) {
        super(context);
        init();
    }

    public EventoInfoLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EventoInfoLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @SuppressWarnings("unused")
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public EventoInfoLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        inflate(getContext(), R.layout.item_evento_info, this);
        mCardView = findViewById(R.id.cardView);
        mPoster = findViewById(R.id.poster);
        mTitle = findViewById(R.id.title);
        mDove = findViewById(R.id.dove);
        mDoveMore = findViewById(R.id.dove_more);
        mDoveLayout = findViewById(R.id.dove_layout);
        mQuando = findViewById(R.id.quando);
        mQuandoMore = findViewById(R.id.quando_more);
        mQuandoLayout = findViewById(R.id.quando_layout);
        mStatus = findViewById(R.id.status);

        mPollLayout = findViewById(R.id.poll_layout);

        mCreatoDaAvatar = findViewById(R.id.creato_da_avatar);
        mCreatoDa = findViewById(R.id.creato_da);
        mDettaglio = findViewById(R.id.dettaglio);

        mVoteDetailLayout = findViewById(R.id.vote_detail_layout);

        mNumInteressatiYes = findViewById(R.id.num_interessati_yes);
        mInteressati = findViewById(R.id.interessati);
        mInteressatiLayout = findViewById(R.id.interessati_layout);

        mNumYes = findViewById(R.id.num_yes);
        mConfermati = findViewById(R.id.confermati);
        mNumNo = findViewById(R.id.num_no);
        mNonConfermati = findViewById(R.id.non_confermati);
        mVoteLayout = findViewById(R.id.vote_layout);

        mVoteTitle = findViewById(R.id.vote_title);
        mNonAncoraVotato = findViewById(R.id.non_ancora_votato);
    }

    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        super.setOnClickListener(l);
    }

    /**
     * Esegui il bind tra i dati dell'evento e le view
     *
     * @param evento evento
     */
    public void bindEvento(Event evento, VoteDao voteDao, PollDao pollDao) {
        ((MarginLayoutParams) getLayoutParams()).bottomMargin = (int) Utils.dpToPx(16, getContext());

        if (evento != null) {
            showEventoInfo(evento, pollDao, voteDao);
        }
    }

    /**
     * Mostro i dati dell'evento
     *
     * @param evento  evento
     * @param pollDao pollDao
     * @param voteDao voteDao
     */
    private void showEventoInfo(final Event evento, PollDao pollDao, VoteDao voteDao) {
        if (evento == null || evento.getEventInfo() == null) {
            return;
        }

        if (!Utils.isLollipop()) {
            mCardView.setPreventCornerOverlap(false);
        }

        final EventInfo eventInfo = evento.getEventInfo();
        mTitle.setText(eventInfo.getWhat());
        if (evento.getDesc() != null && evento.getDesc().length() != 0) {
            mDettaglio.setText(evento.getDesc());
            mDettaglio.setVisibility(VISIBLE);
        } else {
            mDettaglio.setVisibility(GONE);
        }

        switch (evento.getStatus()) {
            case Constants.EVENTO_CONFERMATO:
                statusConfermata(evento);
                showVotoInfo(evento, voteDao);
                break;
            case Constants.EVENTO_ANNULLATO:
                statusAnnullata(evento);
                break;
            default:
                statusInPianificazione(evento);
                showVotoInteresseInfo(evento, voteDao);
                break;
        }

        managePolls(evento, pollDao);

        if (evento.getAvatarLargeUrl() != null) {
            Glide.with(getContext()).load(evento.getAvatarLargeUrl()).asBitmap().centerCrop().listener(new RequestListener<String, Bitmap>() {
                @Override
                public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                    Glide.with(getContext()).load(R.drawable.avatar_attivita).centerCrop().into(mPoster);
                    mPoster.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            EventBus.getDefault().post(new AvatarSelectedEvent(null, mPoster, true));
                        }
                    });
                    return true;
                }

                @Override
                public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                    mPoster.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            EventBus.getDefault().post(new AvatarSelectedEvent(evento.getAvatarLargeUrl(), mPoster, true));
                        }
                    });
                    return false;
                }
            }).into(mPoster);
        } else {
            Glide.with(getContext()).load(R.drawable.avatar_attivita).centerCrop().into(mPoster);
            mPoster.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    EventBus.getDefault().post(new AvatarSelectedEvent(null, mPoster, true));
                }
            });
        }

        // Avatar
        Glide.with(getContext()).load(evento.getCreator().getAvatarThumb()).asBitmap().centerCrop().placeholder(R.drawable.avatar_utente).into(new BitmapImageViewTarget(mCreatoDaAvatar) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                mCreatoDaAvatar.setImageDrawable(circularBitmapDrawable);
            }
        });
        if (evento.getCreator() != null) {
            mCreatoDaAvatar.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post(new UserImageSelectedEvent(evento.getCreator().getUuid(), mCreatoDaAvatar));
                }
            });
            mCreatoDa.setText(String.format("%s %s %s", evento.getCreator().getChatName(), getContext().getString(R.string.main_evento_creata_il_da), DateUtils.smartFormatDateOnly(getContext(), evento.getCreatedAt(), "EEE, dd MMM, yyyy")));
        }
    }

    private void statusAnnullata(Event evento) {
        mStatus.setText(getContext().getString(R.string.evento_status_annullata));
        mStatus.setBackgroundResource(R.drawable.status_annullata);

        mInteressatiLayout.setVisibility(GONE);
        mVoteDetailLayout.setVisibility(GONE);
        mVoteLayout.setVisibility(GONE);
    }

    private void statusConfermata(Event evento) {
        mStatus.setText(getContext().getString(R.string.evento_status_confermata));
        mStatus.setBackgroundResource(R.drawable.status_confermata);

        mInteressatiLayout.setVisibility(GONE);
        mVoteDetailLayout.setVisibility(VISIBLE);
        mVoteLayout.setVisibility(VISIBLE);
    }

    private void statusInPianificazione(Event evento) {
        mStatus.setText(getContext().getString(R.string.evento_status_in_pianificatione));
        mStatus.setBackgroundResource(R.drawable.status_in_pianificazione);

        mInteressatiLayout.setVisibility(VISIBLE);
        mVoteDetailLayout.setVisibility(VISIBLE);
        mVoteLayout.setVisibility(GONE);
    }

    public void managePolls(Event evento, PollDao pollDao) {
        EventInfo info = evento.getEventInfo();

        if (evento.getStatus() == Constants.EVENTO_ANNULLATO) {
            mPollLayout.setVisibility(GONE);
            mDoveMore.setVisibility(GONE);
            mQuandoMore.setVisibility(GONE);
            if (evento.isWherePollEnabled()) {
                mDove.setHint(getContext().getString(R.string.evento_non_ancora_specificato));
                mDove.setText(null);
            } else {
                mDove.setText(info.getWhere());
            }
            if (evento.isWhenPollEnabled()) {
                mQuando.setHint(getContext().getString(R.string.evento_non_ancora_specificato));
                ;
                mQuando.setText(null);
            } else {
                String quando = DateUtils.createQuandoText(info.generateQuandoModel());
                mQuando.setText(quando);
            }
            return;
        }

        mPollLayout.setVisibility(VISIBLE);
        mDove.setHint(getContext().getString(R.string.evento_dove_non_definito));
        mQuando.setHint(getContext().getString(R.string.evento_quando_non_definito));

        if (evento.isWherePollEnabled()) {
            mDove.setText(null);
            mDoveMore.setVisibility(GONE);
            mDoveLayout.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post(new GoToPollEvent(mPollLayout.getChildAt(mPollLayout.getChildCount() - 1).getBottom()));
                }
            });
            createOrUpdatePoll(evento, pollDao, evento.getWherePollUuid(), "where");
        } else {
            mDove.setText(info.getWhere());
            mDoveMore.setVisibility(VISIBLE);
            mDoveLayout.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post(new EventInfoSelectedEvent(true, mDoveLayout));
                }
            });

            removePoll("where");
        }

        if (evento.isWhenPollEnabled()) {
            mQuando.setText(null);
            mQuandoMore.setVisibility(GONE);
            mQuandoLayout.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post(new GoToPollEvent(mPollLayout.getChildAt(0).getBottom()));
                }
            });
            createOrUpdatePoll(evento, pollDao, evento.getWhenPollUuid(), "when");
        } else {
            String quando = DateUtils.createQuandoText(info.generateQuandoModel());
            mQuando.setText(quando);
            mQuandoMore.setVisibility(VISIBLE);
            mQuandoLayout.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post(new EventInfoSelectedEvent(false, mQuandoLayout));
                }
            });

            removePoll("when");
        }
    }

    private void showVotoInfo(final Event evento, final VoteDao voteDao) {
        if (evento == null || evento.getEventInfo() == null) {
            return;
        }

        mVoteTitle.setText(getContext().getString(R.string.main_evento_title_confermati));

        if (!mConfermati.isTagPresent()) {
            mConfermati.setOnWidthChangeListener(new VotantiWidget.OnWidthChangeListener() {
                @Override
                public void widthChanged(float posX) {
                    mNumYes.setTranslationX(posX);
                }
            });
            mConfermati.setVoti(evento.getUuid(), new ArrayList<Vote>(), evento.getGuests().size());
        }
        final List<Vote> votiSi = App.getRealm().copyFromRealm(voteDao.getListVoteEventYes(evento.getUuid()));
        mNumYes.setText(new EasySpan.Builder()
                .appendText(String.valueOf(votiSi.size()))
                .appendSpans("/" + evento.getGuests().size(), new RelativeSizeSpan(0.5f))
                .build()
                .getSpannedText());
        mConfermati.postDelayed(new Runnable() {
            @Override
            public void run() {
                mConfermati.setVoti(evento.getUuid(), votiSi, evento.getGuests().size());
            }
        }, 50);

        if (!mNonConfermati.isTagPresent()) {
            mNonConfermati.setOnWidthChangeListener(new VotantiWidget.OnWidthChangeListener() {
                @Override
                public void widthChanged(float posX) {
                    mNumNo.setTranslationX(posX);
                }
            });
            mNonConfermati.setVoti(evento.getUuid(), new ArrayList<Vote>(), evento.getGuests().size());
        }
        final List<Vote> votiNo = App.getRealm().copyFromRealm(voteDao.getListVoteEventNo(evento.getUuid()));
        mNumNo.setText(new EasySpan.Builder()
                .appendText(String.valueOf(votiNo.size()))
                .appendSpans("/" + evento.getGuests().size(), new RelativeSizeSpan(0.5f))
                .build()
                .getSpannedText());
        mNonConfermati.postDelayed(new Runnable() {
            @Override
            public void run() {
                mNonConfermati.setVoti(evento.getUuid(), votiNo, evento.getGuests().size());
            }
        }, 50);

        updateNonAncoraVotatoText(evento.getGuests().size() - votiSi.size() - votiNo.size());

        if (votiSi.size() != 0 || votiNo.size() != 0) {
            mVoteLayout.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mNonConfermati.getCount() != 0 || mConfermati.getCount() != 0) {
                        EventBus.getDefault().post(new ShowVotiEvent());
                    }
                }
            });
        }
    }

    private void showVotoInteresseInfo(final Event evento, final VoteDao voteDao) {
        if (evento == null || evento.getEventInfo() == null) {
            return;
        }

        mVoteTitle.setText(getContext().getString(R.string.main_evento_title_interessati));

        if (!mInteressati.isTagPresent()) {
            mInteressati.setOnWidthChangeListener(new VotantiWidget.OnWidthChangeListener() {
                @Override
                public void widthChanged(float posX) {
                    mNumInteressatiYes.setTranslationX(posX);
                }
            });
            mInteressati.setVoti(evento.getUuid(), new ArrayList<Vote>(), evento.getGuests().size());
        }
        final List<Vote> votiSi = App.getRealm().copyFromRealm(voteDao.getListVoteInterestEventYes(evento.getUuid()));
        mNumInteressatiYes.setText(new EasySpan.Builder()
                .appendText(String.valueOf(votiSi.size()))
                .appendSpans("/" + evento.getGuests().size(), new RelativeSizeSpan(0.5f))
                .build()
                .getSpannedText());
        mInteressati.postDelayed(new Runnable() {
            @Override
            public void run() {
                mInteressati.setVoti(evento.getUuid(), votiSi, evento.getGuests().size());
            }
        }, 50);

        final int numVotiNo = voteDao.getCountVoteInterestEventNo(evento.getUuid());
        updateNonAncoraVotatoText(evento.getGuests().size() - votiSi.size() - numVotiNo);

        if (votiSi.size() != 0) {
            mInteressatiLayout.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mInteressati.getCount() != 0) {
                        EventBus.getDefault().post(new ShowVotiInteresseEvent());
                    }
                }
            });
        }
    }

    private void updateNonAncoraVotatoText(int numNonVotato) {
        if (numNonVotato == 0) {
            mNonAncoraVotato.setText(getContext().getString(R.string.main_evento_non_ancora_votato_nessuno));
        } else if (numNonVotato == 1) {
            mNonAncoraVotato.setText(String.format(Locale.getDefault(), "%d %s", numNonVotato, getContext().getString(R.string.main_evento_non_ancora_votato_uno)));
        } else {
            mNonAncoraVotato.setText(String.format(Locale.getDefault(), "%d %s", numNonVotato, getContext().getString(R.string.main_evento_non_ancora_votato_molti)));
        }
    }

    public void bindPoll(Event evento, Poll poll, String tag) {
        if (poll != null) {
            boolean trovato = false;
            for (int i = 0; i < mPollLayout.getChildCount(); i++) {
                if (mPollLayout.getChildAt(i).getTag().toString().equals(tag)) {
                    trovato = true;
                    SondaggioEventLayout sondaggio = (SondaggioEventLayout) mPollLayout.getChildAt(i);
                    sondaggio.bind(poll, evento);
                    break;
                }
            }
            if (!trovato) {
                SondaggioEventLayout sondaggio = new SondaggioEventLayout(getContext());
                sondaggio.bind(poll, evento);
                sondaggio.setTag(tag);
                if (poll.getType() == Constants.POLL_TYPE_WHERE) {
                    mPollLayout.addView(sondaggio);
                } else {
                    mPollLayout.addView(sondaggio, 0);
                }
            }
        }
    }

    private void createOrUpdatePoll(Event evento, PollDao pollDao, String pollId, String tag) {
        Poll poll = pollDao.getPoll(pollId);
        if (poll != null) {
            boolean trovato = false;
            for (int i = 0; i < mPollLayout.getChildCount(); i++) {
                if (mPollLayout.getChildAt(i).getTag().toString().equals(tag)) {
                    trovato = true;
                    SondaggioEventLayout sondaggio = (SondaggioEventLayout) mPollLayout.getChildAt(i);
                    sondaggio.bind(poll, evento);
                    break;
                }
            }
            if (!trovato) {
                SondaggioEventLayout sondaggio = new SondaggioEventLayout(getContext());
                sondaggio.bind(poll, evento);
                sondaggio.setTag(tag);
                if (poll.getType() == Constants.POLL_TYPE_WHERE) {
                    mPollLayout.addView(sondaggio);
                } else {
                    mPollLayout.addView(sondaggio, 0);
                }
            }
        }
    }

    private void removePoll(String tag) {
        for (int i = 0; i < mPollLayout.getChildCount(); i++) {
            View view = mPollLayout.getChildAt(i);
            if (view.getTag().toString().equals(tag)) {
                mPollLayout.removeView(view);
                break;
            }
        }
    }
}
