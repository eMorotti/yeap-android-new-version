package it.mozart.yeap.ui.home.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class HomePagerAdapter extends FragmentPagerAdapter {

    private static String makeTagName(int position) {
        return HomePagerAdapter.class.getName() + ":" + position;
    }

    private final FragmentManager mFragmentManager;

    private final List<Fragment> mFragments = new ArrayList<>();

    private final SparseArray<String> mTags = new SparseArray<>();

    private final List<CharSequence> mTitles = new ArrayList<>();

    private Bundle mSavedInstanceState = new Bundle();

    public HomePagerAdapter(FragmentManager fm) {
        super(fm);
        mFragmentManager = fm;
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public Fragment getItem(int position) {
        // Recupero il fragment dalla lista di fragment o dal manager
        String tagName = mSavedInstanceState.getString(makeTagName(position));
        if (!TextUtils.isEmpty(tagName)) {
            Fragment fragment = mFragmentManager.findFragmentByTag(tagName);
            return fragment != null ? fragment : mFragments.get(position);
        }
        return mFragments.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles.get(position);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Object object = super.instantiateItem(container, position);
        mTags.put(position, ((Fragment) object).getTag());
        return object;
    }

    public void addFragment(CharSequence title, Fragment fragment) {
        mTitles.add(title);
        mFragments.add(fragment);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        mSavedInstanceState = savedInstanceState != null ? savedInstanceState : new Bundle();
    }

    public void onSaveInstanceState(Bundle outState) {
        for (int i = 0; i < mTags.size(); i++) {
            int key = mTags.keyAt(i);
            outState.putString(makeTagName(key), mTags.get(key));
        }
    }
}
