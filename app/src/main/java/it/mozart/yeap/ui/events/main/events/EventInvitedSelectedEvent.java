package it.mozart.yeap.ui.events.main.events;

import android.view.View;

public class EventInvitedSelectedEvent {

    private String id;
    private String phone;
    private View view;

    public EventInvitedSelectedEvent(String id, String phone, View view) {
        this.id = id;
        this.phone = phone;
        this.view = view;
    }

    public String getId() {
        return id;
    }

    public String getPhone() {
        return phone;
    }

    public View getView() {
        return view;
    }
}
