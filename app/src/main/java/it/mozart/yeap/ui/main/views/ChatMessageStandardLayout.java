package it.mozart.yeap.ui.main.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.Locale;

import it.mozart.yeap.R;
import it.mozart.yeap.common.UrlPreviewData;
import it.mozart.yeap.events.ImageSelectedEvent;
import it.mozart.yeap.events.RequestImageDownloadPermissionEvent;
import it.mozart.yeap.events.StartImageDownloadEvent;
import it.mozart.yeap.events.StartUrlFindEvent;
import it.mozart.yeap.events.UserImageSelectedEvent;
import it.mozart.yeap.helpers.AndroidPermissionHelper;
import it.mozart.yeap.realm.Message;
import it.mozart.yeap.ui.main.ChatMessageBinder;
import it.mozart.yeap.ui.main.ChatMessageListener;
import it.mozart.yeap.ui.views.AutoRefreshImageView;
import it.mozart.yeap.ui.views.ChatTextView;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.DateUtils;
import it.mozart.yeap.utility.SettingUtils;
import it.mozart.yeap.utility.Utils;

public abstract class ChatMessageStandardLayout extends FrameLayout implements ChatMessageBinder {

    private View mBg;
    private FrameLayout mMessaggioLayout;
    private View mNonLettoLayout;
    private TextView mNonLetto;
    private AutoRefreshImageView mAvatar;
    private TextView mDate;
    private TextView mUtente;
    private ChatTextView mMessaggio;
    private View mPreviewLayout;
    private View mPreviewInnerLayout;
    private ImageView mPreviewImage;
    private TextView mPreviewTitle;
    private TextView mPreviewDescription;
    private TextView mPreviewUrl;
    private TextView mPreviewUrlInner;
    private ImageView mImage;
    private ImageButton mImageDownload;
    private View mImageDownloading;
    private View mImageLoading;
    private View mTopSpace;
    private View mSpace;
    private View mSpaceTopForRight;
    private TextView mTime;
    private ImageView mTimeIcon;
//    private ImageView mErrorView;

    private int mImageWidth;

    public ChatMessageStandardLayout(Context context) {
        super(context);
        init();
    }

    public ChatMessageStandardLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ChatMessageStandardLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @SuppressWarnings("unused")
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ChatMessageStandardLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        inflate(getContext(), getLayoutId(), this);
        mBg = findViewById(R.id.bg);
        mMessaggioLayout = findViewById(R.id.messaggio_layout);
        mNonLettoLayout = findViewById(R.id.non_letto_layout);
        mNonLetto = findViewById(R.id.non_letto);
        mAvatar = findViewById(R.id.avatar);
        mDate = findViewById(R.id.date);
        mUtente = findViewById(R.id.utente);
        mMessaggio = findViewById(R.id.messaggio);
        mMessaggio.setMovementMethod(LinkMovementMethod.getInstance());
        mPreviewLayout = findViewById(R.id.preview_layout);
        mPreviewInnerLayout = findViewById(R.id.preview_inner_layout);
        mPreviewImage = findViewById(R.id.preview_image);
        mPreviewTitle = findViewById(R.id.preview_title);
        mPreviewDescription = findViewById(R.id.preview_description);
        mPreviewUrl = findViewById(R.id.preview_url);
        mPreviewUrlInner = findViewById(R.id.preview_url_inner);
        mImage = findViewById(R.id.image);
        mImageDownload = findViewById(R.id.download);
        mImageDownloading = findViewById(R.id.downloading);
        mImageLoading = findViewById(R.id.loading);
        mTopSpace = findViewById(R.id.top_space);
        mSpace = findViewById(R.id.space);
        mSpaceTopForRight = findViewById(R.id.space_top_for_right);
        mTime = findViewById(R.id.time);
        mTimeIcon = findViewById(R.id.time_icon);
//        mErrorView = findViewById(R.id.error_view);

        // Aggiungo spazi aggiuntivi al testo
        if (!isOtherMessage()) {
            mMessaggio.setAddMoreSpaces(true);
        }
        mMessaggio.setLinkColor(getLinkColor());
        // Mappo il click al parent (per adapter)
        mMessaggio.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ChatMessageStandardLayout.this.performClick();
            }
        });
        mMessaggio.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ChatMessageStandardLayout.this.performLongClick();
                return true;
            }
        });
        mImage.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ChatMessageStandardLayout.this.performLongClick();
                return true;
            }
        });

        mImageWidth = (int) getContext().getResources().getDimension(R.dimen.chat_image_height_dimension);
    }

    /**
     * Recupera il giusto layout
     *
     * @return id layout
     */
    protected abstract int getLayoutId();

    /**
     * Check se il messaggio è proprio o di un altro utente
     *
     * @return true se il layout left
     */
    protected abstract boolean isOtherMessage();

    /**
     * Recupera il colore per i link
     *
     * @return colore link
     */
    protected abstract int getLinkColor();

    /**
     * Esegui il bind tra i dati del messaggio e le view
     *
     * @param messaggio messaggio
     * @param position  posizione
     * @param adapter   adapter interface
     */
    public void bind(final Message messaggio, int position, ChatMessageListener adapter) {
        if (messaggio != null) {
            // Controllo se è presente un messaggio di testo
            // 'all' è usato nel caso sia presente sia testo che immagine
            boolean messagePresent = true;
            if (messaggio.getFormattedMessage(getContext()) != null && messaggio.getFormattedMessage(getContext()).length() > 0) {
                mMessaggio.setVisibility(View.VISIBLE);
                // Gestisce anche la presenza di url
                mMessaggio.setChatText(messaggio.getFormattedMessage(getContext()).toString());
            } else {
                mMessaggio.setVisibility(View.GONE);
                messagePresent = false;
            }

            mPreviewLayout.setVisibility(GONE);
            if (Constants.URL_PREVIEW_ENABLED) {
                if (messaggio.getUrlInfo() != null) {
                    showPreviewData(messaggio.getUrlInfo());
                } else if (!messaggio.isUrlChecked()) {
                    EventBus.getDefault().post(new StartUrlFindEvent(messaggio.getUuid(), messaggio.getUrl()));
                }
            }

            // Gestisco il messaggio selezionato
            if (adapter.getSelectedMessageIds().containsKey(messaggio.getUuid())) {
                mMessaggioLayout.setForeground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.colorOpaqueBlackLight)));
            } else {
                mMessaggioLayout.setForeground(null);
            }

            // Visualizzazione immagine
            boolean imagePresent = showMessageImage(messaggio, adapter);

            // Gestione spazi
            mSpace.setVisibility(imagePresent && !messagePresent ? View.GONE : View.VISIBLE);

            // Visualizzo il numero di messaggi ancora da leggere
            if (messaggio.isFirstNotRead()) {
                mNonLettoLayout.setVisibility(View.VISIBLE);
                if (adapter.getNumNewMessages() == 1) {
                    mNonLetto.setText(String.format("1 %s", getContext().getString(R.string.chat_messaggio_non_letto)));
                } else {
                    mNonLetto.setText(String.format(Locale.getDefault(), "%d %s", adapter.getNumNewMessages(), getContext().getString(R.string.chat_messaggi_non_letti)));
                }
            } else {
                mNonLettoLayout.setVisibility(View.GONE);
            }

            manageLayoutDiff(messaggio, adapter, position, imagePresent, messagePresent);
            showMessageDate(messaggio, adapter, position);
            showMessageTime(messaggio);
        }
    }

    private void showPreviewData(String info) {
        final UrlPreviewData previewData = new Gson().fromJson(info, UrlPreviewData.class);
        mPreviewLayout.setVisibility(VISIBLE);

        if (previewData.getTitle() != null && !previewData.getTitle().isEmpty()) {
            mPreviewTitle.setVisibility(VISIBLE);
        } else {
            mPreviewTitle.setVisibility(GONE);
        }
        mPreviewTitle.setText(previewData.getTitle());

        if (previewData.getDescription() != null && !previewData.getDescription().isEmpty()) {
            mPreviewDescription.setVisibility(VISIBLE);
        } else {
            mPreviewDescription.setVisibility(GONE);
        }
        mPreviewDescription.setText(previewData.getDescription());

        if (previewData.getImage() != null) {
            Glide.with(getContext()).load(previewData.getImage()).centerCrop().diskCacheStrategy(DiskCacheStrategy.SOURCE).into(mPreviewImage);
            mPreviewInnerLayout.getLayoutParams().height = (int) Utils.dpToPx(60, getContext());
            mPreviewImage.setVisibility(View.VISIBLE);

            mPreviewUrl.setVisibility(VISIBLE);
            mPreviewUrlInner.setVisibility(GONE);
            mPreviewUrl.setText(previewData.getUrl());
        } else {
            Glide.clear(mPreviewImage);
            mPreviewInnerLayout.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            mPreviewImage.setVisibility(View.GONE);

            mPreviewUrl.setVisibility(GONE);
            mPreviewUrlInner.setVisibility(VISIBLE);
            mPreviewUrlInner.setText(previewData.getUrl());
        }

        mPreviewLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(previewData.getUrl()));
                getContext().startActivity(intent);
            }
        });
    }

    /**
     * Visualizzazione immagine
     *
     * @param messaggio messaggio
     * @param adapter   adapter interface
     * @return imagePresent
     */
    private boolean showMessageImage(final Message messaggio, final ChatMessageListener adapter) {
        mImageDownload.setVisibility(GONE);
        mImageLoading.setVisibility(GONE);
        mImageDownloading.setVisibility(GONE);
        String image = null;

        if (messaggio.getMediaLocalUrl() != null) {
            // Controllo se l'immagine esiste
            image = checkIfLocalImageExist(messaggio);

            switch (messaggio.getImageState()) {
                case Constants.IMAGE_STATE_UPLOADING:
//                    mImageLoading.setVisibility(VISIBLE);
                    mImageLoading.setVisibility(VISIBLE);
                    break;
            }
        } else if (messaggio.getMediaUrl() != null) {
            switch (messaggio.getImageState()) {
                case Constants.IMAGE_STATE_NOT_DOWNLOADED:
                    if (SettingUtils.checkIfAutodownloadEnabled()) {
                        if (!AndroidPermissionHelper.isPermissionGranted(getContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                            mImageDownload.setVisibility(VISIBLE);
                            mImageDownload.setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (adapter.getSelectedMessageIds().size() == 0) {
                                        EventBus.getDefault().post(new RequestImageDownloadPermissionEvent());
                                    } else {
                                        ChatMessageStandardLayout.this.performClick();
                                    }
                                }
                            });
                        } else {
                            if (Utils.isNetworkAvailable(getContext())) {
                                EventBus.getDefault().post(new StartImageDownloadEvent(messaggio.getUuid(), false));
                            }
                        }
                    } else {
                        mImageDownload.setVisibility(VISIBLE);
                        mImageDownload.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (adapter.getSelectedMessageIds().size() == 0) {
                                    if (!AndroidPermissionHelper.isPermissionGranted(getContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                                        EventBus.getDefault().post(new RequestImageDownloadPermissionEvent());
                                    } else if (Utils.isNetworkAvailable(getContext())) {
                                        EventBus.getDefault().post(new StartImageDownloadEvent(messaggio.getUuid(), true));
                                    } else {
                                        Toast.makeText(getContext(), getContext().getString(R.string.general_error_internet), Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    ChatMessageStandardLayout.this.performClick();
                                }
                            }
                        });
                    }
                    break;
                case Constants.IMAGE_STATE_DOWNLOADING:
//                    mImageLoading.setVisibility(VISIBLE);
                    mImageDownloading.setVisibility(VISIBLE);
                    break;
                case Constants.IMAGE_STATE_DOWNLOAD_COMPLETE:
//                    mImageLoading.setVisibility(GONE);
                    break;
                case Constants.IMAGE_STATE_DOWNLOAD_FAILED:
                    mImageDownload.setVisibility(VISIBLE);
                    mImageDownload.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (adapter.getSelectedMessageIds().size() == 0) {
                                if (Utils.isNetworkAvailable(getContext())) {
                                    EventBus.getDefault().post(new StartImageDownloadEvent(messaggio.getUuid(), true));
                                    mImageDownload.setOnClickListener(null);
                                } else {
                                    Toast.makeText(getContext(), getContext().getString(R.string.general_error_internet), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                ChatMessageStandardLayout.this.performClick();
                            }
                        }
                    });
                    break;
            }
        }

        int[] imageDimensions = measureImage(messaggio);
        if (image == null) {
            // Mostro thumb
            if (messaggio.getThumbData() != null) {
                mImage.setVisibility(View.VISIBLE);
                showThumbImage(messaggio, imageDimensions[0], imageDimensions[1]);
                mImage.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (adapter.getSelectedMessageIds().size() == 0) {
                            if (messaggio.getMediaLocalUrl() == null) {
                                mImageDownload.performClick();
                            } else {
                                EventBus.getDefault().post(new ImageSelectedEvent(null, messaggio.getText(), mImage));
                            }
                        } else {
                            ChatMessageStandardLayout.this.performClick();
                        }
                    }
                });
            } else {
                mImage.setVisibility(View.GONE);
            }
        } else {
            mImage.getLayoutParams().width = imageDimensions[0];
            mImage.getLayoutParams().height = imageDimensions[1];
            mImage.setVisibility(View.VISIBLE);

            // Visualizzo l'immagine
            Glide.with(getContext()).load(image).asBitmap().centerCrop().override(imageDimensions[0], imageDimensions[1]).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(new BitmapImageViewTarget(mImage) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getContext().getResources(), resource);
                    circularBitmapDrawable.setCornerRadius(Utils.dpToPx(8, getContext()));
                    mImage.setImageDrawable(circularBitmapDrawable);
                }
            });
            mImage.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (adapter.getSelectedMessageIds().size() == 0) {
                        EventBus.getDefault().post(new ImageSelectedEvent(messaggio.getMediaLocalUrl(), messaggio.getText(), mImage));
                    } else {
                        ChatMessageStandardLayout.this.performClick();
                    }
                }
            });
        }

        return image != null || messaggio.getThumbData() != null;
    }

    /**
     * Recupero la dimensione dell'immagine riadattandola alla mia view
     *
     * @param messaggio messaggio
     * @return larghezza e altezza
     */
    private int[] measureImage(Message messaggio) {
        // Controllo che l'immagine mantenga una dimensione appropriata
        double ratio = messaggio.getMediaWidth() / mImageWidth;
        int messaggioHeight = (int) (messaggio.getMediaHeight() / ratio);
        if (messaggioHeight > mImage.getMaxHeight())
            messaggioHeight = mImage.getMaxHeight();
        else if (messaggioHeight < mImage.getMinimumHeight())
            messaggioHeight = mImage.getMinimumHeight();

        return new int[]{mImageWidth, messaggioHeight};
    }

    /**
     * Mostro la thumb
     *
     * @param messaggio       messaggio
     * @param messaggioWidth  larghezza immagine
     * @param messaggioHeight altezza immagine
     */
    private void showThumbImage(Message messaggio, int messaggioWidth, int messaggioHeight) {
        mImage.getLayoutParams().width = messaggioWidth;
        mImage.getLayoutParams().height = messaggioHeight;

        // Salvo l'immagine creata in cache
//        if (messaggio.getThumbTempUrl() == null) {
//            File file = Utils.saveImageInCache(getContext(), messaggio.getThumbData());
//            if (file != null)
//                messaggio.setThumbTempUrl(file.getPath());
//        }
        byte[] imgBytesData = android.util.Base64.decode(messaggio.getThumbData(), android.util.Base64.DEFAULT);

        // Carico l'immagine
        Glide.with(getContext()).load(imgBytesData).asBitmap().centerCrop().override(mImageWidth, messaggioHeight).into(new BitmapImageViewTarget(mImage) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getContext().getResources(), resource);
                circularBitmapDrawable.setCornerRadius(Utils.dpToPx(isOtherMessage() ? 8 : 8, getContext()));
                mImage.setImageDrawable(circularBitmapDrawable);
            }
        });
    }

    /**
     * Gestisco la visualizzazione delle informazioni dell'utente e dei differenti layout del messaggio
     * nel caso siano consecutivi o meno
     *
     * @param messaggio      messaggio
     * @param adapter        adapter interface
     * @param position       position
     * @param imagePresent   immagine presente
     * @param messagePresent messaggio presente
     */
    private void manageLayoutDiff(final Message messaggio, final ChatMessageListener adapter, int position, boolean imagePresent, boolean messagePresent) {
        boolean hideUtente = false;
        boolean rightHideUtente = false;
        // Controllo le condizioni
        if (position != adapter.getNumItems() - 1) {
            Message lastMessaggio = adapter.getMessaggi().get(position + 1);
            if (lastMessaggio.getCreator() != null) {
                if ((lastMessaggio.getCreator().getUuid() == null || messaggio.getCreator().getUuid() == null ||
                        lastMessaggio.getCreator().getUuid().equals(messaggio.getCreator().getUuid())) && adapter.getItemType(position + 1) != Constants.ADAPTER_TYPE_SYSTEM
                       /* && lastMessaggio.getProposta() == null && lastMessaggio.getSondaggio() == null && lastMessaggio.getEvento() == null*/
                        && messaggio.getCreatedAt() / 86400000 == lastMessaggio.getCreatedAt() / 86400000) {
                    if (isOtherMessage()) {
                        hideUtente = true;
                    } else {
                        rightHideUtente = true;
                    }
                }
            }
        }

        // Modifico il layout
        if (isOtherMessage()) {
            mSpaceTopForRight.setVisibility(View.GONE);
            if (hideUtente) {
                mUtente.setVisibility(View.GONE);
                mAvatar.setVisibility(View.GONE);
                mTopSpace.setVisibility(View.GONE);
                mBg.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.chat_bg_left_multiple));
            } else {
                mUtente.setVisibility(View.VISIBLE);
                mAvatar.setVisibility(View.VISIBLE);
                mTopSpace.setVisibility(View.VISIBLE);
                mSpaceTopForRight.setVisibility(View.VISIBLE);
                mBg.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.chat_bg_left));

                if (!imagePresent && messagePresent)
                    mSpace.setVisibility(GONE);

                mUtente.setText(messaggio.getCreator().getChatName());

                mUtente.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (adapter.getSelectedMessageIds().size() == 0) {
                            EventBus.getDefault().post(new UserImageSelectedEvent(messaggio.getCreator().getUuid(), mUtente));
                        } else {
                            ChatMessageStandardLayout.this.performClick();
                        }
                    }
                });
                mAvatar.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (adapter.getSelectedMessageIds().size() == 0) {
                            EventBus.getDefault().post(new UserImageSelectedEvent(messaggio.getCreator().getUuid(), mAvatar));
                        } else {
                            ChatMessageStandardLayout.this.performClick();
                        }
                    }
                });
            }
        } else {
            mUtente.setVisibility(View.GONE);
            mAvatar.setVisibility(View.GONE);
            if (rightHideUtente) {
                mSpaceTopForRight.setVisibility(View.GONE);
                mTopSpace.setVisibility(View.GONE);
                mBg.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.chat_bg_right_multiple));
            } else {
                mSpaceTopForRight.setVisibility(View.VISIBLE);
                mTopSpace.setVisibility(View.VISIBLE);
                mBg.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.chat_bg_right));
            }
        }

        // Gestisco l'avatar dell'utente
        if (messaggio.getCreator() != null && isOtherMessage()) {
            if (!hideUtente) {
                mAvatar.bindThumb(messaggio.getCreatorUuid());
            }
        }
    }

    /**
     * Mostro la data (giorno) dei messaggi (solo sul primo messaggio della giornata)
     *
     * @param messaggio messaggio
     * @param adapter   adapter interface
     * @param position  position
     */
    private void showMessageDate(Message messaggio, ChatMessageListener adapter, int position) {
        if (position != adapter.getNumItems() - 1) {
            Message after = adapter.getMessaggi().get(position + 1);
            if (!adapter.getFormatter().format(messaggio.getCreatedAt()).equals(adapter.getFormatter().format(after.getCreatedAt()))) {
                mDate.setText(DateUtils.smartFormatDateOnly(getContext(), messaggio.getCreatedAt(), "dd MMMM yyyy"));
                mDate.setVisibility(View.VISIBLE);
            } else {
                mDate.setText(null);
                mDate.setVisibility(View.GONE);
            }
        } else {
            mDate.setText(DateUtils.smartFormatDateOnly(getContext(), messaggio.getCreatedAt(), "dd MMMM yyyy"));
            mDate.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Visualizzo l'orario del messaggio
     *
     * @param messaggio messaggio
     */
    private void showMessageTime(Message messaggio) {
//        if (messaggio.getCreatedAtForVisualization() != 0) {
//            String time = DateUtils.formatDate(messaggio.getCreatedAtForVisualization(), "HH:mm");
//            mTime.setText(time);
//        } else {
            String time = DateUtils.formatDate(messaggio.getCreatedAt(), "HH:mm");
            mTime.setText(time);
//        }

        if (!isOtherMessage()) {
            Drawable drawable;
            if (messaggio.isSent()) {
                drawable = DrawableCompat.wrap(ContextCompat.getDrawable(getContext(), R.drawable.ic_done_white_18dp));
                DrawableCompat.setTint(drawable, ContextCompat.getColor(getContext(), R.color.textWhiteSecondary));
            } else {
                drawable = DrawableCompat.wrap(ContextCompat.getDrawable(getContext(), R.drawable.ic_access_time_white_18dp));
                DrawableCompat.setTint(drawable, ContextCompat.getColor(getContext(), R.color.textWhiteLessTransparent));
            }
            mTimeIcon.setImageDrawable(drawable);
        }
    }

    /**
     * Recupero il path locale dell'immagine
     *
     * @param messaggio messaggio
     * @return imagepath
     */
    private String checkIfLocalImageExist(Message messaggio) {
        String directoryBase = Environment.getExternalStorageDirectory() + "/Yeap/YeapImages";
        String filename = messaggio.getMediaLocalUrl().substring(messaggio.getMediaLocalUrl().lastIndexOf("/"));
        File file = new File(directoryBase + filename);
        if (file.exists()) {
            return messaggio.getMediaLocalUrl();
        }
        file = new File(directoryBase + "/Sent" + filename);
        if (file.exists()) {
            return messaggio.getMediaLocalUrl();
        }
        file = new File(messaggio.getMediaLocalUrl());
        if (file.exists()) {
            return messaggio.getMediaLocalUrl();
        }
        return null;
    }

    @Override
    public void unbind() {
        mMessaggio.clear();
    }
}
