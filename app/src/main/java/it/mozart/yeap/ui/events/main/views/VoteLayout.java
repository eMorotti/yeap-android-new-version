package it.mozart.yeap.ui.events.main.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.varunest.sparkbutton.SparkButton;

import org.greenrobot.eventbus.EventBus;

import it.mozart.yeap.R;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.EventInfo;
import it.mozart.yeap.realm.Vote;
import it.mozart.yeap.ui.events.main.events.EventoUpdateStatus;
import it.mozart.yeap.ui.events.main.events.VoteEventClickedEvent;
import it.mozart.yeap.ui.events.main.events.VoteInterestEventClickedEvent;
import it.mozart.yeap.ui.views.LoadingView;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.EasySpan;
import uk.co.chrisjenx.calligraphy.CalligraphyTypefaceSpan;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

public class VoteLayout extends FrameLayout {

    private Typeface mTypeface;

    private View mVoteLayout;
    private View mInterestLayout;
    private View mAdminLayout;

    private SparkButton mPartecipo;
    private View mPartecipoButton;
    private SparkButton mNonPartecipo;
    private View mNonPartecipoButton;
    private LoadingView mLoadingView;

    private TextView mInteressaText;
    private TextView mInteressato;
    private TextView mNonInteressato;

    private TextView mConferma;
    private TextView mAnnulla;

    private int mVoteBefore;

    public VoteLayout(Context context) {
        super(context);
        init();
    }

    public VoteLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public VoteLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @SuppressWarnings("unused")
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public VoteLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        inflate(getContext(), R.layout.item_vote_layout, this);
        mTypeface = TypefaceUtils.load(getContext().getAssets(), "fonts/OpenSans-Bold.ttf");

        mVoteLayout = findViewById(R.id.vote_layout);
        mInterestLayout = findViewById(R.id.interest_layout);
        mAdminLayout = findViewById(R.id.admin_layout);

        mPartecipo = findViewById(R.id.partecipo);
        mPartecipoButton = findViewById(R.id.partecipo_button);
        mNonPartecipo = findViewById(R.id.non_partecipo);
        mNonPartecipoButton = findViewById(R.id.non_partecipo_button);

        mInteressaText = findViewById(R.id.interessa_text);
        mInteressato = findViewById(R.id.interessa);
        mNonInteressato = findViewById(R.id.non_interessa);

        mConferma = findViewById(R.id.conferma);
        mAnnulla = findViewById(R.id.annulla);

        mLoadingView = findViewById(R.id.loading_view);
        mLoadingView.startLoading();

        mVoteBefore = -1;
    }

    /**
     * Esegui il bind tra i voti dell'evento e le view
     *
     * @param evento evento
     */
    public void bindEvento(Event evento) {
        if (evento != null) {
            if (evento.getStatus() == Constants.EVENTO_CONFERMATO) {
                mInterestLayout.setVisibility(GONE);
                mVoteLayout.setVisibility(VISIBLE);
                mAdminLayout.setVisibility(GONE);
                showVoteButtons(evento);
            } else if (evento.getStatus() == Constants.EVENTO_ANNULLATO) {
                mInterestLayout.setVisibility(GONE);
                mVoteLayout.setVisibility(INVISIBLE);
                mAdminLayout.setVisibility(GONE);
            } else {
                mVoteLayout.setVisibility(INVISIBLE);
                if (evento.getAdminUuid().equals(AccountProvider.getInstance().getAccountId())) {
                    mAdminLayout.setVisibility(VISIBLE);
                    mInterestLayout.setVisibility(GONE);
                    showAdminButtons();
                } else {
                    mAdminLayout.setVisibility(GONE);
                    mInterestLayout.setVisibility(VISIBLE);
                    showInterestButtons(evento);
                }
            }

            if (!evento.isMyselfPresent()) {
                mInterestLayout.setVisibility(GONE);
                mVoteLayout.setVisibility(GONE);
                mAdminLayout.setVisibility(GONE);
            }
        }
    }

    private void showVoteButtons(final Event evento) {
        if (evento == null || evento.getEventInfo() == null) {
            return;
        }

        final EventInfo eventInfo = evento.getEventInfo();

        mLoadingView.setVisibility(INVISIBLE);
        if (eventInfo.getMyVote() != null) {
            if (eventInfo.getMyVote().isLoading()) {
                mLoadingView.setVisibility(VISIBLE);
                if (eventInfo.getMyVote().getType() == Constants.VOTO_NO) {
                    mPartecipoButton.setBackgroundResource(R.drawable.rect_gray_voto);
                    mPartecipo.setAlpha(0.5f);
                    mNonPartecipoButton.setBackgroundResource(R.drawable.rect_main_blue);
                    mNonPartecipo.setAlpha(1f);
                } else {
                    mPartecipoButton.setBackgroundResource(R.drawable.rect_main_blue);
                    mPartecipo.setAlpha(1f);
                    mNonPartecipoButton.setBackgroundResource(R.drawable.rect_gray_voto);
                    mNonPartecipo.setAlpha(0.5f);
                }
            } else if (eventInfo.getMyVote().getType() == Constants.VOTO_SI) {
                mPartecipo.setChecked(true);
                mNonPartecipo.setChecked(false);
                mPartecipoButton.setBackgroundResource(R.drawable.rect_main_blue);
                mPartecipo.setAlpha(1f);
                mNonPartecipoButton.setBackgroundResource(R.drawable.rect_gray_voto);
                mNonPartecipo.setAlpha(0.5f);
            } else {
                mNonPartecipo.setChecked(true);
                mPartecipo.setChecked(false);
                mPartecipoButton.setBackgroundResource(R.drawable.rect_gray_voto);
                mPartecipo.setAlpha(0.5f);
                mNonPartecipoButton.setBackgroundResource(R.drawable.rect_main_blue);
                mNonPartecipo.setAlpha(1f);
            }

            if (mVoteBefore != -1) {
                if (mVoteBefore == 0) {
                    if (eventInfo.getMyVote().getType() == Constants.VOTO_NO) {
                        mNonPartecipo.playAnimation();
                    } else if (eventInfo.getMyVote().getType() == Constants.VOTO_SI) {
                        mPartecipo.playAnimation();
                    }
                } else if (mVoteBefore == Constants.VOTO_NO && eventInfo.getMyVote().getType() == Constants.VOTO_SI) {
                    mPartecipo.playAnimation();
                } else if (mVoteBefore == Constants.VOTO_SI && eventInfo.getMyVote().getType() == Constants.VOTO_NO) {
                    mNonPartecipo.playAnimation();
                }
            }
            mVoteBefore = eventInfo.getMyVote().getType();
        } else {
            mPartecipo.setChecked(false);
            mNonPartecipo.setChecked(false);
            mPartecipoButton.setBackgroundResource(R.drawable.rect_gray_voto);
            mPartecipo.setAlpha(0.5f);
            mNonPartecipoButton.setBackgroundResource(R.drawable.rect_gray_voto);
            mNonPartecipo.setAlpha(0.5f);

            mVoteBefore = 0;
        }

        mPartecipo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Vote voto = eventInfo.getMyVote();
                if (voto != null && voto.isLoading()) {
                    return;
                }

                if (!evento.isMyselfPresent() || evento.getStatus() == Constants.EVENTO_ANNULLATO) {
                    return;
                }

                if (voto == null || voto.getType() != Constants.VOTO_SI) {
                    mLoadingView.setVisibility(VISIBLE);
                    EventBus.getDefault().post(new VoteEventClickedEvent(Constants.VOTO_SI));
                }
            }
        });

        mNonPartecipo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Vote voto = eventInfo.getMyVote();
                if (voto != null && voto.isLoading()) {
                    return;
                }

                if (!evento.isMyselfPresent() || evento.getStatus() == Constants.EVENTO_ANNULLATO) {
                    return;
                }

                if (voto == null || voto.getType() != Constants.VOTO_NO) {
                    mLoadingView.setVisibility(VISIBLE);
                    EventBus.getDefault().post(new VoteEventClickedEvent(Constants.VOTO_NO));
                }
            }
        });
    }

    private void showAdminButtons() {
        setVisibility(VISIBLE);

        mConferma.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new EventoUpdateStatus(Constants.EVENTO_CONFERMATO));
            }
        });

        mAnnulla.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new EventoUpdateStatus(Constants.EVENTO_ANNULLATO));
            }
        });
    }

    private void showInterestButtons(final Event evento) {
        if (evento == null || evento.getEventInfo() == null) {
            return;
        }

        final EventInfo eventInfo = evento.getEventInfo();
        setVisibility(VISIBLE);

        EasySpan easySpan = new EasySpan.Builder()
                .appendSpans(evento.getCreator().getChatName(), new CalligraphyTypefaceSpan(mTypeface))
                .appendText(String.format(" %s", getContext().getString(R.string.evento_interesse_testo)))
                .build();

        mInteressaText.setText(easySpan.getSpannedText());

        mLoadingView.setVisibility(INVISIBLE);
        if (eventInfo.getMyVoteInterest() != null) {
            if (eventInfo.getMyVoteInterest().isLoading()) {
                mLoadingView.setVisibility(VISIBLE);
                if (eventInfo.getMyVoteInterest().getType() == Constants.VOTO_NO_INTERESSE) {
                    mNonInteressato.setBackgroundResource(R.drawable.rect_main_blue);
                    mNonInteressato.setAlpha(1f);
                    mInteressato.setBackgroundResource(R.drawable.rect_gray_voto);
                    mInteressato.setAlpha(0.5f);
                } else {
                    mInteressato.setBackgroundResource(R.drawable.rect_main_blue);
                    mInteressato.setAlpha(1f);
                    mNonInteressato.setBackgroundResource(R.drawable.rect_gray_voto);
                    mNonInteressato.setAlpha(0.5f);
                }
            } else if (eventInfo.getMyVoteInterest().getType() == Constants.VOTO_INTERESSE) {
                mInteressato.setBackgroundResource(R.drawable.rect_main_blue);
                mInteressato.setAlpha(1f);
                mNonInteressato.setBackgroundResource(R.drawable.rect_gray_voto);
                mNonInteressato.setAlpha(0.5f);
            } else {
                mNonInteressato.setBackgroundResource(R.drawable.rect_main_blue);
                mNonInteressato.setAlpha(1f);
                mInteressato.setBackgroundResource(R.drawable.rect_gray_voto);
                mInteressato.setAlpha(0.5f);
            }
        } else {
            mInteressato.setBackgroundResource(R.drawable.rect_gray_voto);
            mInteressato.setAlpha(0.5f);
            mNonInteressato.setBackgroundResource(R.drawable.rect_gray_voto);
            mNonInteressato.setAlpha(0.5f);
        }

        mInteressato.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Vote voto = eventInfo.getMyVoteInterest();
                if (voto != null && voto.isLoading()) {
                    return;
                }

                if (!evento.isMyselfPresent() || evento.getStatus() == Constants.EVENTO_ANNULLATO) {
                    return;
                }

                if (voto == null || voto.getType() != Constants.VOTO_INTERESSE) {
                    mLoadingView.setVisibility(VISIBLE);
                    EventBus.getDefault().post(new VoteInterestEventClickedEvent(Constants.VOTO_INTERESSE));
                }
            }
        });

        mNonInteressato.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Vote voto = eventInfo.getMyVoteInterest();
                if (voto != null && voto.isLoading()) {
                    return;
                }

                if (!evento.isMyselfPresent() || evento.getStatus() == Constants.EVENTO_ANNULLATO) {
                    return;
                }

                if (voto == null || voto.getType() != Constants.VOTO_NO_INTERESSE) {
                    mLoadingView.setVisibility(VISIBLE);
                    EventBus.getDefault().post(new VoteInterestEventClickedEvent(Constants.VOTO_NO_INTERESSE));
                }
            }
        });
    }
}
