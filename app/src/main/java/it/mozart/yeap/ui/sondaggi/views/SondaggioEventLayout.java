package it.mozart.yeap.ui.sondaggi.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.text.style.RelativeSizeSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import it.mozart.yeap.R;
import it.mozart.yeap.events.AddItemToPollEvent;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.Poll;
import it.mozart.yeap.realm.PollItem;
import it.mozart.yeap.realm.Vote;
import it.mozart.yeap.ui.events.main.events.PollItemMoreClickedEvent;
import it.mozart.yeap.ui.events.main.events.PollMoreSelectedEvent;
import it.mozart.yeap.ui.events.main.events.VotePollItemClickedEvent;
import it.mozart.yeap.ui.events.main.events.VotePollItemEventClickedEvent;
import it.mozart.yeap.ui.main.ChatMessageBinder;
import it.mozart.yeap.ui.views.AnimCheckBox;
import it.mozart.yeap.ui.views.PercentBgView;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.DateUtils;
import it.mozart.yeap.utility.EasySpan;
import it.mozart.yeap.utility.Utils;

public class SondaggioEventLayout extends FrameLayout implements ChatMessageBinder {

    private ImageView mMessaggioIconWhere;
    private ImageView mMessaggioIconWhen;
    private TextView mMessaggio;
    private ViewGroup mVociLayout;
    private TextView mNuovo;
    private TextView mUpdates;
    private ImageButton mMore;
    private Button mAggiungiRisposta;

    public SondaggioEventLayout(Context context) {
        super(context);
        init();
    }

    public SondaggioEventLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SondaggioEventLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @SuppressWarnings("unused")
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public SondaggioEventLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        inflate(getContext(), R.layout.item_sondaggio_evento, this);
        mMessaggioIconWhere = findViewById(R.id.messaggio_icon_where);
        mMessaggioIconWhen = findViewById(R.id.messaggio_icon_when);
        mMessaggio = findViewById(R.id.messaggio);
        mVociLayout = findViewById(R.id.voci_layout);
        mNuovo = findViewById(R.id.nuovo);
        mUpdates = findViewById(R.id.updates);
        mMore = findViewById(R.id.more);
        mAggiungiRisposta = findViewById(R.id.aggiungi_risposta);

        mMessaggio.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                SondaggioEventLayout.this.performClick();
            }
        });
    }

    /**
     * Esegui il bind tra i dati del sondaggio e le view
     *
     * @param sondaggio sondaggio
     * @param evento    evento
     */
    public void bind(final Poll sondaggio, Event evento) {
        if (sondaggio != null) {
            showSondaggioInfo(sondaggio, evento);

            if (sondaggio.getType() == Constants.POLL_TYPE_WHERE) {
                mMessaggioIconWhere.setVisibility(VISIBLE);
                mMessaggioIconWhen.setVisibility(GONE);
                mMessaggio.setText(getContext().getString(R.string.sondaggio_dove_question));
                mAggiungiRisposta.setText(getContext().getString(R.string.sondaggio_aggiungi_voce_where));
            } else {
                mMessaggioIconWhere.setVisibility(GONE);
                mMessaggioIconWhen.setVisibility(VISIBLE);
                mMessaggio.setText(getContext().getString(R.string.sondaggio_quando_question));
                mAggiungiRisposta.setText(getContext().getString(R.string.sondaggio_aggiungi_voce_when));
            }

            if (sondaggio.isDeleted()) {
                setOnClickListener(null);
            }

            if (evento.isMyselfPresent()) {
                mMore.setVisibility(VISIBLE);
                mMore.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EventBus.getDefault().post(new PollMoreSelectedEvent(sondaggio.getUuid(), mMore));
                    }
                });
            } else {
                mMore.setVisibility(GONE);
            }

            mNuovo.setVisibility(sondaggio.isNuovo() ? VISIBLE : GONE);

            if (evento.isMyselfPresent() && (evento.getAdminUuid().equals(AccountProvider.getInstance().getAccountId()) || sondaggio.isUsersCanAddItems())) {
                ((View) mAggiungiRisposta.getParent()).setVisibility(VISIBLE);
                mAggiungiRisposta.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EventBus.getDefault().post(new AddItemToPollEvent(sondaggio.getUuid(), true, sondaggio.getType() == Constants.POLL_TYPE_WHERE));
                    }
                });
            } else {
                ((View) mAggiungiRisposta.getParent()).setVisibility(GONE);
            }
        }
    }

    /**
     * Mostro i dati del sondaggio
     *
     * @param sondaggio sondaggio
     * @param evento    evento
     */
    private void showSondaggioInfo(final Poll sondaggio, Event evento) {
        List<PollItem> voci = sondaggio.getVoci();
        if (voci != null) {
            setVisibility(VISIBLE);
            int count = mVociLayout.getChildCount();
            int i;
            int updates = 0;

            // Modifico la view
            for (i = 0; i < voci.size(); i++) {
                if (i < count) {
                    View voceLayout = mVociLayout.getChildAt(i);
                    bindVoce(voci.get(i), voceLayout, evento);
                    if (voci.get(i).isNuovo()) {
                        updates++;
                    }
                } else {
                    break;
                }
            }

            for (int j = i; j < count; j++) {
                // Rimuovo la view
                View view = mVociLayout.getChildAt(j);
                if (view != null) {
                    view.setOnClickListener(null);
                    mVociLayout.removeViewAt(j);
                }
            }

            for (int j = i; j < voci.size(); j++) {
                // Creo la view
                View voceLayout = LayoutInflater.from(getContext()).inflate(R.layout.item_sondaggio_voce, mVociLayout, false);
                mVociLayout.addView(bindVoce(voci.get(j), voceLayout, evento));
                if (voci.get(j).isNuovo()) {
                    updates++;
                }
            }

            if (voci.size() == 0) {
                mVociLayout.setVisibility(GONE);
            } else {
                mVociLayout.setVisibility(VISIBLE);
            }

            if (updates != 0) {
                mUpdates.setText(String.valueOf(updates));
                mUpdates.setVisibility(VISIBLE);
            } else {
                mUpdates.setVisibility(GONE);
            }
        } else {
            setVisibility(GONE);
            mUpdates.setVisibility(GONE);
        }
    }

    private View bindVoce(final PollItem voce, View voceLayout, final Event evento) {
        final AnimCheckBox checkBox = voceLayout.findViewById(R.id.check);
        ProgressBar progressBar = voceLayout.findViewById(R.id.progress);
        TextView contatore = voceLayout.findViewById(R.id.count);
        TextView suggerimento = voceLayout.findViewById(R.id.suggerimento);
        View more = voceLayout.findViewById(R.id.more_layout);
        PercentBgView percentView = voceLayout.findViewById(R.id.percent_view);
        View nuovo = voceLayout.findViewById(R.id.nuovo);
        View layout = voceLayout.findViewById(R.id.layout);

        checkBox.setMyselfIn(evento.isMyselfPresent());

        if (voce.isDeleted()) {
            voceLayout.setVisibility(GONE);
        } else {
            voceLayout.setVisibility(VISIBLE);
        }

        // Reset iniziale
        checkBox.setOnCheckedChangeListener(null);
        checkBox.setIgnoreClick(true);
        percentView.setMax(evento.getGuests().size());

        String tag = (String) checkBox.getTag();

        // Gestisco selezione checkbox
        Vote voto = voce.getMyVote();
        if (voto != null) {
            if (voto.isLoading()) {
                checkBox.setIsLoading(true);
                if (Utils.isLollipop()) {
                    if (checkBox.isChecked()) {
                        progressBar.getIndeterminateDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
                    } else {
                        progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPropostaSelected), PorterDuff.Mode.SRC_IN);
                    }
                }
                progressBar.setVisibility(VISIBLE);
                if (!Utils.isLollipop()) {
                    progressBar.getIndeterminateDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);
                }
            } else {
                checkBox.setIsLoading(false);
                checkBox.setChecked(voto.getType() == Constants.VOTO_SI, tag.equals(voce.getUuid()));
                progressBar.setVisibility(GONE);
            }
        } else {
            checkBox.setIsLoading(false);
            checkBox.setChecked(false, tag.equals(voce.getUuid()));
            progressBar.setVisibility(GONE);
        }
        checkBox.setTag(voce.getUuid());

        // Voce
        if (voce.getWhere() != null) {
            suggerimento.setText(voce.getWhere());
        } else if (voce.isQuandoPresent()) {
            suggerimento.setText(DateUtils.createQuandoText(voce.generateQuandoModel()));
        } else {
            suggerimento.setText(voce.getWhat());
        }

        more.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new PollItemMoreClickedEvent(voce.getPollUuid(), voce.getUuid()));
            }
        });
        layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (evento.isMyselfPresent()) {
                    checkBox.performClick();
                }
            }
        });

        // Checkbox
        if (!checkBox.isLoading()) {
            checkBox.setOnCheckedChangeListener(new AnimCheckBox.OnCheckedChangeListener() {
                @Override
                public void onChange(boolean checked) {
                    // Aggiorno voto al cambiamento del checkbox
                    EventBus.getDefault().post(new VotePollItemClickedEvent(voce.getUuid(), checked));
                    EventBus.getDefault().post(new VotePollItemEventClickedEvent());
                }
            });
        }

        // Contatore
        long current = voce.getNumVoteYes();
        EasySpan easySpan = new EasySpan.Builder()
                .appendText(String.valueOf(current))
                .appendSpans("/" + evento.getGuests().size(), new RelativeSizeSpan(0.5f))
                .build();
        contatore.setText(easySpan.getSpannedText());

        percentView.updatePercent((int) current);

        nuovo.setVisibility(voce.isNuovo() ? View.VISIBLE : View.GONE);

        return voceLayout;
    }

    @Override
    public void unbind() {

    }
}
