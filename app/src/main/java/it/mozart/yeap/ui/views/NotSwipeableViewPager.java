package it.mozart.yeap.ui.views;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class NotSwipeableViewPager extends ViewPager {

    private boolean mSwipe = false;

    public NotSwipeableViewPager(Context context) {
        super(context);
    }

    public NotSwipeableViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return mSwipe && super.onInterceptTouchEvent(event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return mSwipe && super.onTouchEvent(event);
    }

    public void setSwipe(boolean swipe) {
        if (mSwipe != swipe) {
            mSwipe = swipe;
        }
    }
}