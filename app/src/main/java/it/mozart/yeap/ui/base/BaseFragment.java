package it.mozart.yeap.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import it.mozart.yeap.App;
import it.mozart.yeap.ui.views.DynamicView;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public abstract class BaseFragment extends Fragment {

    protected FirebaseAnalytics mFirebaseAnalytics;

    private boolean mUseBus = false;
    private boolean mMantainBus = false;
    private boolean mSavedInfoPresent = false;

    private CompositeSubscription mCompositeSubscription;
    private List<Subscription> mSubscriptions;

    private Unbinder mUnbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());

        initValues();

        if (getArguments() != null) {
            loadParameters(getArguments());
        }

        mSavedInfoPresent = false;
        if (savedInstanceState != null) {
            mSavedInfoPresent = true;
            loadInfos(savedInstanceState);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);
        mUnbinder = ButterKnife.bind(this, view);
        App.feather().injectFields(this);

        mCompositeSubscription = new CompositeSubscription();
        if (mSubscriptions == null) {
            mSubscriptions = new ArrayList<>();
        }

        initialize();

        return view;
    }

    protected abstract int getLayoutId();

    protected abstract void initValues();

    protected abstract void loadParameters(Bundle arguments);

    protected abstract void loadInfos(Bundle savedInstanceState);

    protected abstract void saveInfos(Bundle outState);

    // Il presenter è pronto per l'utilizzo
    protected abstract void initialize();

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        saveInfos(outState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mUseBus && !EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mUseBus && !mMantainBus) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mUseBus && mMantainBus) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
        unsubscribeSubscriptions();
    }

    protected View getRootView() {
        View view;
        if (getActivity() instanceof BaseActivityWithSyncService)
            view = ((BaseActivityWithSyncService) getActivity()).getRootView();
        else
            view = getView();
        return view;
    }

    // EventBus viene deregistrato sull' onPause
    protected void setUseBus(boolean useBus) {
        this.mUseBus = useBus;
    }

    // EventBus viene deregistrato sull' onDestroy
    protected void setMantainBus(boolean mantainBus) {
        this.mMantainBus = mantainBus;
        this.mUseBus = mantainBus;
    }

    protected boolean isSavedInfoPresent() {
        return mSavedInfoPresent;
    }

    protected void registerSubscription(Subscription subscription) {
        mCompositeSubscription.add(subscription);
    }

    protected void unsubscribeSubscriptions() {
        mCompositeSubscription.unsubscribe();
        for (int i = 0; i < mSubscriptions.size(); i++) {
            mSubscriptions.get(i).unsubscribe();
        }
    }

    public DynamicView getDynamicView() {
        if (getActivity() instanceof BaseActivityWithSyncService)
            return ((BaseActivityWithSyncService) getActivity()).getDynamicView();
        return null;
    }

    protected void setRequestId(String requestId) {
        if (getActivity() instanceof BaseActivityWithSyncService) {
            ((BaseActivityWithSyncService) getActivity()).setRequestId(requestId);
        }
    }

    protected String getRequestId() {
        if (getActivity() instanceof BaseActivityWithSyncService) {
            return ((BaseActivityWithSyncService) getActivity()).getRequestId();
        }

        return "";
    }

    protected void startTimer() {
        if (getActivity() instanceof BaseActivityWithSyncService) {
            ((BaseActivityWithSyncService) getActivity()).startTimer();
        }
    }

    protected void cancelTimer() {
        if (getActivity() instanceof BaseActivityWithSyncService) {
            ((BaseActivityWithSyncService) getActivity()).cancelTimer();
        }
    }
}