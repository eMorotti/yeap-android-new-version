package it.mozart.yeap.ui.settings;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.realm.Realm;
import it.mozart.yeap.R;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.helpers.AnalitycsProvider;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.ui.base.BaseActivityWithSyncService;
import it.mozart.yeap.ui.profile.Act_Profilo;
import it.mozart.yeap.ui.views.AutoRefreshImageView;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.Prefs;

public class Act_Settings extends BaseActivityWithSyncService {

    @BindView(R.id.profilo_nome)
    TextView mNomeProfilo;
    @BindView(R.id.profilo_telefono)
    TextView mTelefonoProfilo;
    @BindView(R.id.profilo_avatar)
    AutoRefreshImageView mAvatarProfilo;
    @BindView(R.id.autodownload)
    TextView mAutodownload;

    @OnClick(R.id.profilo)
    void profiloClicked() {
        AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "viewMyProfile", "settings");
        startActivity(Act_Profilo.newIntent(mUser.getUuid()));
    }

    @OnClick(R.id.backup_layout)
    void backupClicked() {
        AnalitycsProvider.getInstance().logCustomType(mFirebaseAnalytics, "changeSettings", "backup");
        startActivity(new Intent(this, Act_SettingsBackup.class));
    }

    @OnClick(R.id.autodownload_layout)
    void autodownloadClicked() {
        AnalitycsProvider.getInstance().logCustomType(mFirebaseAnalytics, "changeSettings", "automaticPhotoDownload");
        new MaterialDialog.Builder(this)
                .title(getString(R.string.settings_download_automatico_popup))
                .items(R.array.impostazioni_autodownload)
                .itemsCallbackSingleChoice(Prefs.getInt(Constants.SETTINGS_AUTODOWNLOAD, 0), new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                        mAutodownload.setText(text);
                        Prefs.putInt(Constants.SETTINGS_AUTODOWNLOAD, which);
                        return true;
                    }
                })
                .positiveText(getString(R.string.general_conferma))
                .show();
    }

    @OnClick(R.id.info_layout)
    void infoClicked() {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.yeap.cloud/")));
    }

    @OnClick(R.id.credits)
    void creditsClicked() {
        startActivity(new Intent(this, Act_SettingsCredits.class));
        overridePendingTransition(R.anim.slide_in_bottom_slow, R.anim.fade_out);
    }

    // SUPPORT

    @Inject
    Realm realm;

    private User mUser;

    // SYSTEM

    @Override
    protected int getLayoutId() {
        return R.layout.act_settings;
    }

    @Override
    protected void initVariables() {

    }

    @Override
    protected void loadParameters(Bundle extras) {

    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    protected void initialize() {
        mUser = new UserDao(realm).getUser(AccountProvider.getInstance().getAccountId());

        // Init profilo
        mNomeProfilo.setText(mUser.getNickname());
        mTelefonoProfilo.setText(mUser.getPhone());
        mAvatarProfilo.bindThumb(mUser.getUuid());

        mAutodownload.setText(getResources().getStringArray(R.array.impostazioni_autodownload)[Prefs.getInt(Constants.SETTINGS_AUTODOWNLOAD, 0)]);
    }
}
