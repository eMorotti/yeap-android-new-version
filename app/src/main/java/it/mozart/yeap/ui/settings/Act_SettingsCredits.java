package it.mozart.yeap.ui.settings;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import it.mozart.yeap.R;
import it.mozart.yeap.ui.base.BaseActivityWithSyncService;
import it.mozart.yeap.ui.settings.adapters.SettingsCreditsAdapter;
import it.mozart.yeap.ui.settings.events.CreditsSelectedEvent;
import it.mozart.yeap.ui.settings.models.Credits;
import it.mozart.yeap.ui.views.DividerDecorator;

public class Act_SettingsCredits extends BaseActivityWithSyncService {

    @BindView(R.id.recyclerview)
    RecyclerView mRecyclerView;

    // SUPPORT


    // SYSTEM


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUseBus(true);

        setToolbarTitle(getString(R.string.app_settings));
        setToolbarSubtitle(getString(R.string.app_settings_credits));
        mToolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_settings_credits;
    }

    @Override
    protected void initVariables() {
    }

    @Override
    protected void loadParameters(Bundle extras) {

    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.slide_out_bottom_slow);
    }

    @Override
    protected void initialize() {
        String json = getString(R.string.credits_json);

        List<Credits> creditsList = new Gson().fromJson(json, new TypeToken<ArrayList<Credits>>() {
        }.getType());

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(new SettingsCreditsAdapter(creditsList));
        mRecyclerView.addItemDecoration(new DividerDecorator(ContextCompat.getColor(this, R.color.colorGraySecondary)));
    }

    @Subscribe
    public void onEvent(CreditsSelectedEvent event) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(event.getUrl()));
        startActivity(intent);
    }
}
