package it.mozart.yeap.ui.profile;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.io.File;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.api.models.APIError;
import it.mozart.yeap.api.models.ImageApiModel;
import it.mozart.yeap.api.services.ProfileApi;
import it.mozart.yeap.api.services.UploadApi;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.helpers.AnalitycsProvider;
import it.mozart.yeap.helpers.ImageInputHelper;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.ui.base.BaseFragment;
import it.mozart.yeap.ui.support.Act_ImageCrop;
import it.mozart.yeap.ui.support.Act_Splash;
import it.mozart.yeap.ui.views.AutoRefreshImageView;
import it.mozart.yeap.utility.APIErrorUtils;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.Prefs;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class Frg_Profilo extends BaseFragment {

    @BindView(R.id.avatar)
    AutoRefreshImageView mAvatar;
    @BindView(R.id.nickname)
    EditText mNickname;

    @OnClick(R.id.avatar)
    void avatarClicked() {
        avatarClickedMethod(false);
    }

    @OnClick(R.id.confirm)
    void confirmClicked() {
        getDynamicView().showLoading();

        if (mNickname.getText().length() == 0) {
            Toast.makeText(getContext(), getString(R.string.profilo_nickname_error), Toast.LENGTH_SHORT).show();
            return;
        }

        if (!mUser.getNickname().equals(mNickname.getText().toString())) {
            AnalitycsProvider.getInstance().logCustomType(mFirebaseAnalytics, "modifyProfile", "nickname");
        }

        AnalitycsProvider.getInstance().logCustom(mFirebaseAnalytics, "modifyProfileConfirm");
        if (mAvatarFile != null) {
            uploadAvatar();
        } else {
            updateUtente();
        }
    }

    // SUPPORT

    @Inject
    Realm realm;
    @Inject
    UploadApi uploadApi;
    @Inject
    ProfileApi profileApi;
    @Inject
    NotificationsHelper notifications;

    private File mAvatarFile;
    private User mUser;

    private static final int IMG_CODE = 701;
    private static final int IMG_CROP_CODE = 702;

    // SYSTEM

    @Override
    protected int getLayoutId() {
        return R.layout.frg_profilo;
    }

    @Override
    protected void initValues() {

    }

    @Override
    protected void loadParameters(Bundle arguments) {

    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    protected void initialize() {
        mUser = new UserDao(realm).getUser(AccountProvider.getInstance().getAccountId());
        showInfo(mUser);

        registerSubscription(profileApi.getMyProfile()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<User>() {
                    @Override
                    public void call(User utente) {
                        showInfo(utente);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                    }
                }));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.IMAGE_PERMISSION_CODE:
                avatarClickedMethod(true);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case IMG_CODE:
                    // Chiamo l'activity per il crop dell'immagine selezionata
                    Uri copertinaUri = ImageInputHelper.getPickImageResultUri(getContext(), data);
                    startActivityForResult(Act_ImageCrop.newIntent(copertinaUri, 1, 1), IMG_CROP_CODE);
                    break;
                case IMG_CROP_CODE:
                    String image = data.getStringExtra(Constants.IMAGE_CROP);
                    if (image != null) {
                        // Salvo l'immagine come File per per poi poterla usare alla creazione dell'attività
                        mAvatarFile = new File(image);
                        Glide.with(this).load(mAvatarFile).asBitmap().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(new BitmapImageViewTarget(mAvatar) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                mAvatar.setImageDrawable(circularBitmapDrawable);
                            }
                        });
                    } else {
                        // Mostro messaggio di errore se non è stato restituito niente dall'activity di crop
                        mAvatarFile = null;
                        Toast.makeText(getContext(), getString(R.string.general_error_ritaglio_immagine), Toast.LENGTH_LONG).show();
                    }
                    break;
            }
        }
    }

    // FUNCTIONS

    private void showInfo(User utente) {
        if (utente != null) {
            mNickname.setText(utente.getNickname());
            mAvatar.bindThumb(utente.getUuid());
        }
    }

    /**
     * Eseguo l'upload dell'avatar
     */
    private void uploadAvatar() {
        AnalitycsProvider.getInstance().logCustomType(mFirebaseAnalytics, "modifyProfile", "avatar");
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), mAvatarFile);
        final MultipartBody.Part image = MultipartBody.Part.createFormData("image", mAvatarFile.getName(), requestFile);
        registerSubscription(uploadApi.uploadAvatar(image)
                .subscribeOn(Schedulers.newThread())
                .flatMap(new Func1<ImageApiModel, Observable<User>>() {
                    @Override
                    public Observable<User> call(ImageApiModel imageApiModel) {
                        Glide.get(getContext()).clearDiskCache();
                        User utente = new User();
                        utente.setAvatar(imageApiModel.getUploadref());
                        utente.setNickname(mNickname.getText().length() != 0 ? mNickname.getText().toString() : null);
                        return profileApi.updateProfile(utente);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<User>() {
                    @Override
                    public void call(User utente) {
                        aggiornaUtenteRealm(utente);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        getDynamicView().hide();
                        throwable.printStackTrace();
                    }
                }));
    }

    /**
     * Update dell'utente
     */
    private void updateUtente() {
        User utente = new User();
        utente.setNickname(mNickname.getText().length() != 0 ? mNickname.getText().toString() : null);
        registerSubscription(profileApi.updateProfile(utente)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<User>() {
                    @Override
                    public void call(User utenteResponse) {
                        aggiornaUtenteRealm(utenteResponse);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        getDynamicView().hide();
                        throwable.printStackTrace();
                    }
                }));
    }

    /**
     * Aggiorno i dati su realm
     *
     * @param utenteResponse utente
     */
    private void aggiornaUtenteRealm(final User utenteResponse) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                User utente = new UserDao(realm).getUser(AccountProvider.getInstance().getAccountId());
                if (utenteResponse.getNickname() != null) {
                    utente.setNickname(utenteResponse.getNickname());
                }
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                getDynamicView().hide();
                Toast.makeText(App.getContext(), getString(R.string.profilo_saved), Toast.LENGTH_SHORT).show();
                getActivity().finish();
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                getDynamicView().hide();
                error.printStackTrace();
            }
        });
    }

    /**
     * Eseguo il logout
     */
    private void logout() {
        Prefs.remove(Constants.LOGIN_COMPLETED);
        Prefs.remove(Constants.LOGIN_EFFECTUATED);
        Prefs.remove(Constants.PREF_ACCESS_TOKEN);
        Prefs.remove(Constants.PREF_PROFILE_ID);
        Prefs.remove(Constants.PREF_PROFILE_PHONE);
        Prefs.remove(Constants.BACKUP_DATE);
        AccountProvider.getInstance().reset();
        Realm.removeDefaultConfiguration();

        notifications.cancelAllNotification(getContext());

        Intent intent = new Intent(getContext(), Act_Splash.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    /**
     * Elimino l'account
     */
    private void deleteAccount() {
        new MaterialDialog.Builder(getContext())
                .content(getString(R.string.profilo_account_eliminato))
                .positiveText(getString(R.string.general_ok))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Prefs.remove(Constants.LOGIN_COMPLETED);
                        Prefs.remove(Constants.LOGIN_EFFECTUATED);
                        Prefs.remove(Constants.PREF_ACCESS_TOKEN);
                        Prefs.remove(Constants.PREF_PROFILE_ID);
                        Prefs.remove(Constants.PREF_PROFILE_PHONE);
                        Prefs.remove(Constants.BACKUP_DATE);
                        AccountProvider.getInstance().reset();
                        Realm.removeDefaultConfiguration();

                        notifications.cancelAllNotification(getContext());

                        Intent intent = new Intent(getContext(), Act_Splash.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                })
                .cancelable(false)
                .show();
    }

    public void menuDeleteAccount() {
        AnalitycsProvider.getInstance().logCustomType(mFirebaseAnalytics, "dotsProfileAction", "delete");
        new MaterialDialog.Builder(getContext())
                .title(getString(R.string.profilo_elimina_popup_title))
                .content(getString(R.string.profilo_elimina_popup_content))
                .positiveText(getString(R.string.general_elimina_account))
                .negativeText(getString(R.string.general_annulla))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        getDynamicView().showLoading();
                        profileApi.deleteAccount()
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new Action1<Object>() {
                                    @Override
                                    public void call(Object o) {
                                        deleteAccount();
                                    }
                                }, new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable throwable) {
                                        getDynamicView().hide();

                                        APIError error = APIErrorUtils.parseError(getContext(), throwable);
                                        Toast.makeText(getContext(), error.message(), Toast.LENGTH_LONG).show();
                                    }
                                });
                    }
                })
                .show();
    }

    public void menuLogout() {
        AnalitycsProvider.getInstance().logCustomType(mFirebaseAnalytics, "dotsProfileAction", "logout");
        new MaterialDialog.Builder(getContext())
                .title(getString(R.string.profilo_logout_popup_title))
                .positiveText(getString(R.string.profilo_logout))
                .negativeText(getString(R.string.general_annulla))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        getDynamicView().showLoading();
                        profileApi.logout()
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new Action1<Object>() {
                                    @Override
                                    public void call(Object o) {
                                        logout();
                                    }
                                }, new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable throwable) {
                                        getDynamicView().hide();

                                        APIError error = APIErrorUtils.parseError(getContext(), throwable);
                                        Toast.makeText(getContext(), error.message(), Toast.LENGTH_LONG).show();
                                    }
                                });
                    }
                })
                .show();
    }

    private void avatarClickedMethod(boolean ignoreCheck) {
        ImageInputHelper.openChooserForAvatarSelect(this, IMG_CODE, ignoreCheck);
    }
}
