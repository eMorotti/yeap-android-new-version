package it.mozart.yeap.ui.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.util.AttributeSet;

import it.mozart.yeap.utility.Utils;

public class CircleTextImageButton extends android.support.v7.widget.AppCompatImageButton {

    private Paint mFontPaint;
    private Paint mPaint;
    private Path mPath;
    private Canvas mCanvas;

    private Matrix mMatrix;
    private Bitmap mBitmap;

    private String mText1;
    private String mText2;
    private float mRotate;

    public CircleTextImageButton(Context context) {
        super(context);
        init();
    }

    public CircleTextImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CircleTextImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mFontPaint = new Paint();
        mFontPaint.setAntiAlias(true);
        mFontPaint.setTextSize(Utils.dpToPx(7f, getContext()));
        mFontPaint.setTextAlign(Paint.Align.CENTER);
        mFontPaint.setColor(Color.DKGRAY);
        mFontPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

        mPaint = new Paint();

        mPath = new Path();
        mMatrix = new Matrix();

        mText1 = "";
        mText2 = "";
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (getWidth() != 0) {
            if (mBitmap == null) {
                mBitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
            }
            if (mCanvas == null) {
                mCanvas = new Canvas(mBitmap);
            }
            mCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
            mCanvas.setMatrix(mMatrix);

            mPath.reset();
            mPath.addCircle(getWidth() / 2, getHeight() / 2, getWidth() / 2.3f, Path.Direction.CCW);

            mCanvas.rotate(mRotate, getWidth() / 2, getHeight() / 2);

            mCanvas.save();
            mCanvas.rotate(-90, getWidth() / 2, getHeight() / 2);
            mFontPaint.setAlpha((int) (255 * (1f + mRotate / 180)));
            mCanvas.drawTextOnPath(mText1, mPath, 0, 0, mFontPaint);
            mCanvas.restore();

            mCanvas.save();
            mCanvas.rotate(90, getWidth() / 2, getHeight() / 2);
            mFontPaint.setAlpha((int) (255 * (-mRotate / 180)));
            mCanvas.drawTextOnPath(mText2, mPath, 0, 0, mFontPaint);
            mCanvas.restore();

            canvas.drawBitmap(mBitmap, 0, 0, mPaint);
        }

        super.onDraw(canvas);
    }

    public void update(int position, float offset) {
        mRotate = offset * -180;
        invalidate();
    }

    public void setText1(String text) {
        mText1 = text.replace("", " ").trim();
    }

    public void setText2(String text) {
        mText2 = text.replace("", " ").trim();
    }
}
