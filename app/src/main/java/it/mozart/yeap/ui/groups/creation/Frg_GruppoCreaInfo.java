package it.mozart.yeap.ui.groups.creation;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.io.File;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import it.mozart.yeap.R;
import it.mozart.yeap.helpers.CreationModelHelper;
import it.mozart.yeap.helpers.ImageInputHelper;
import it.mozart.yeap.ui.base.BaseFragment;
import it.mozart.yeap.ui.main.MainCreationControl;
import it.mozart.yeap.ui.support.Act_ImageCrop;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.Utils;

public class Frg_GruppoCreaInfo extends BaseFragment implements MainCreationControl {

    @BindView(R.id.title)
    EditText mTitle;
    @BindView(R.id.avatar)
    ImageView mAvatar;

    @OnClick({R.id.avatar, R.id.avatar_text})
    void avatarClicked() {
        avatarClickedMethod(false);
    }

    @OnClick(R.id.layout)
    void layoutClicked() {
        Utils.hideKeyboard(getActivity());
    }

    // SUPPORT

    @Inject
    CreationModelHelper creationModelHelper;

    private static final int IMG_CODE = 101;
    private static final int IMG_CROP_CODE = 102;

    // SYSTEM

    @Override
    protected int getLayoutId() {
        return R.layout.frg_gruppo_crea_info;
    }

    @Override
    protected void initValues() {

    }

    @Override
    protected void loadParameters(Bundle arguments) {

    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    protected void initialize() {
        if (creationModelHelper.getGroup().getTitle() != null)
            mTitle.setText(creationModelHelper.getGroup().getTitle());
        if (creationModelHelper.getGroup().getAvatarFile() != null) {
            Glide.with(this).load(creationModelHelper.getGroup().getAvatarFile()).asBitmap().centerCrop().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(new BitmapImageViewTarget(mAvatar) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    mAvatar.setImageDrawable(circularBitmapDrawable);
                }
            });
        }

        mTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0)
                    creationModelHelper.getGroup().setTitle(null);
                else
                    creationModelHelper.getGroup().setTitle(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        // Aggiorno il titolo, il sottotitolo e l'icona della toolbar dell'activity
        if (getActivity() instanceof Act_GruppoCrea) {
            ((Act_GruppoCrea) getActivity()).setToolbarTitleAndIconFromCrea(getString(R.string.nuovo_gruppo_crea_info_title), R.drawable.ic_close_white_24dp);
            ((Act_GruppoCrea) getActivity()).setToolbarSubtitleFromCrea(getString(R.string.nuovo_gruppo_crea_info_subtitle));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.IMAGE_PERMISSION_CODE:
                avatarClickedMethod(true);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case IMG_CODE:
                    // Chiamo l'activity per il crop dell'immagine selezionata
                    Uri copertinaUri = ImageInputHelper.getPickImageResultUri(getContext(), data);
                    startActivityForResult(Act_ImageCrop.newIntentFreeRatio(copertinaUri), IMG_CROP_CODE);
                    break;
                case IMG_CROP_CODE:
                    String image = data.getStringExtra(Constants.IMAGE_CROP);
                    if (image != null) {
                        // Salvo l'immagine come File per per poi poterla usare alla creazione del gruppo
                        creationModelHelper.getGroup().setAvatarFile(new File(image));
                        Glide.with(this).load(creationModelHelper.getGroup().getAvatarFile()).asBitmap().centerCrop().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(new BitmapImageViewTarget(mAvatar) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                mAvatar.setImageDrawable(circularBitmapDrawable);
                            }
                        });
                    } else {
                        // Mostro messaggio di errore se non è stato restituito niente dall'activity di crop
                        creationModelHelper.getGroup().setAvatarFile(null);
                        Toast.makeText(getContext(), getString(R.string.general_error_ritaglio_immagine), Toast.LENGTH_LONG).show();
                    }
                    break;
            }
        }
    }

    @Override
    public boolean canGoOn() {
        if (!mTitle.getText().toString().isEmpty())
            return true;

        Toast.makeText(getContext(), getString(R.string.gruppo_crea_titolo_mancante), Toast.LENGTH_SHORT).show();
        return false;
    }

    // FUNCTIONS

    private void avatarClickedMethod(boolean ignoreCheck) {
        ImageInputHelper.openChooserForAvatarSelect(this, IMG_CODE, ignoreCheck);
    }
}
