package it.mozart.yeap.ui.groups.main.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import it.mozart.yeap.R;
import it.mozart.yeap.events.UserImageSelectedEvent;
import it.mozart.yeap.realm.Group;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.ui.groups.main.events.GroupGuestSelectedEvent;
import it.mozart.yeap.ui.groups.main.events.GroupInvitedSelectedEvent;
import it.mozart.yeap.ui.views.AutoRefreshImageView;
import it.mozart.yeap.utility.Constants;

public class GruppoMainPartecipantiAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<User> mPartecipanti;
    private List<User> mInvitati;
    private String mAdminId;

    public GruppoMainPartecipantiAdapter(Context context, Group group) {
        mContext = context;
        mPartecipanti = group.getGuests();
        mInvitati = group.getGuestsNotYeap();
        mAdminId = group.getAdminUuid();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case Constants.ADAPTER_HEADER_PARTECIPANTI:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_partecipanti_header, parent, false);
                return new PartecipantiHeaderViewHolder(view);
            case Constants.ADAPTER_ITEM_PARTECIPANTI:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_partecipanti, parent, false);
                return new PartecipantiViewHolder(view);
            case Constants.ADAPTER_HEADER_INVITATI:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_invitati_header, parent, false);
                return new InvitatiHeaderViewHolder(view);
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_invitati, parent, false);
                return new InvitatiViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case Constants.ADAPTER_HEADER_PARTECIPANTI:
                bindPartecipantiHeader((PartecipantiHeaderViewHolder) holder);
                break;
            case Constants.ADAPTER_ITEM_PARTECIPANTI:
                bindPartecipanti((PartecipantiViewHolder) holder, position);
                break;
            case Constants.ADAPTER_HEADER_INVITATI:
                bindInvitatiHeader((InvitatiHeaderViewHolder) holder);
                break;
            default:
                bindInvitati((InvitatiViewHolder) holder, position);
                break;
        }
    }

    private void bindPartecipantiHeader(PartecipantiHeaderViewHolder realHolder) {
        if (mPartecipanti.size() == 1) {
            realHolder.header.setText(mContext.getString(R.string.main_evento_num_partecipanti_single));
        } else {
            realHolder.header.setText(String.format(mContext.getString(R.string.main_evento_num_partecipanti), mPartecipanti.size()));
        }
    }

    private void bindPartecipanti(final PartecipantiViewHolder realHolder, int position) {
        final User utente = mPartecipanti.get(position - 1);
        if (utente == null) {
            return;
        }

        realHolder.name.setText(utente.getChatName());
        realHolder.status.setVisibility(View.GONE);

        realHolder.avatar.bindThumb(utente.getUuid());
        realHolder.avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new UserImageSelectedEvent(utente.getUuid(), realHolder.avatar));
            }
        });

        realHolder.admin.setVisibility(utente.getUuid().equals(mAdminId) ? View.VISIBLE : View.GONE);

        if (position == mPartecipanti.size()) {
            realHolder.divider.setVisibility(View.INVISIBLE);
        } else {
            realHolder.divider.setVisibility(View.VISIBLE);
        }
    }

    private void bindInvitatiHeader(InvitatiHeaderViewHolder realHolder) {
        if (mInvitati.size() == 1) {
            realHolder.header.setText(mContext.getString(R.string.main_evento_num_invitati_single));
        } else {
            realHolder.header.setText(String.format(mContext.getString(R.string.main_evento_num_invitati), mInvitati.size()));
        }
    }

    private void bindInvitati(final InvitatiViewHolder realHolder, int position) {
        final User utente = mInvitati.get(position - mPartecipanti.size() - 1 - (mPartecipanti.size() != 0 ? 1 : 0));
        if (utente == null) {
            return;
        }

        realHolder.name.setText(utente.getChatName());
        realHolder.phone.setText(utente.getPhone());

        realHolder.avatar.bindThumb(utente.getUuid());
        realHolder.avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new UserImageSelectedEvent(utente.getUuid(), realHolder.avatar));
            }
        });

        if (position == getItemCount() - 1) {
            realHolder.divider.setVisibility(View.INVISIBLE);
        } else {
            realHolder.divider.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mPartecipanti.size() + mInvitati.size() + (mPartecipanti.size() != 0 ? 1 : 0) + (mInvitati.size() != 0 ? 1 : 0);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            if (mPartecipanti.size() != 0) {
                return Constants.ADAPTER_HEADER_PARTECIPANTI;
            } else {
                return Constants.ADAPTER_HEADER_INVITATI;
            }
        } else {
            if (mPartecipanti.size() != 0) {
                if (position <= mPartecipanti.size()) {
                    return Constants.ADAPTER_ITEM_PARTECIPANTI;
                } else {
                    if (position == mPartecipanti.size() + 1) {
                        return Constants.ADAPTER_HEADER_INVITATI;
                    } else {
                        return Constants.ADAPTER_ITEM_INVITATI;
                    }
                }
            } else {
                return Constants.ADAPTER_ITEM_INVITATI;
            }
        }
    }

    public void updateGruppo(Group gruppo) {
        mPartecipanti = gruppo.getGuests();
        mInvitati = gruppo.getGuestsNotYeap();
        mAdminId = gruppo.getAdminUuid();
        notifyDataSetChanged();
    }

    class PartecipantiViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.avatar)
        AutoRefreshImageView avatar;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.status)
        TextView status;
        @BindView(R.id.admin)
        View admin;
        @BindView(R.id.divider)
        View divider;

        PartecipantiViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getLayoutPosition();
            User utente = mPartecipanti.get(position - 1);
            if (utente != null) {
                EventBus.getDefault().post(new GroupGuestSelectedEvent(utente.getUuid(), v));
            }
        }
    }

    class PartecipantiHeaderViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.header)
        TextView header;

        PartecipantiHeaderViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    class InvitatiViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.avatar)
        AutoRefreshImageView avatar;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.phone)
        TextView phone;
        @BindView(R.id.divider)
        View divider;

        InvitatiViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getLayoutPosition();
            User utente = mInvitati.get(position - mPartecipanti.size() - 1 - (mPartecipanti.size() != 0 ? 1 : 0));
            if (utente != null) {
                EventBus.getDefault().post(new GroupInvitedSelectedEvent(utente.getContactId(), utente.getPhone(), v));
            }
        }
    }

    class InvitatiHeaderViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.header)
        TextView header;

        InvitatiHeaderViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
