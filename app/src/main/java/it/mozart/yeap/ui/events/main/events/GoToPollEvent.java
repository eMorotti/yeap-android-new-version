package it.mozart.yeap.ui.events.main.events;

public class GoToPollEvent {
    
   private int pos;

    public GoToPollEvent(int pos) {
        this.pos = pos;
    }

    public int getPos() {
        return pos;
    }
}
