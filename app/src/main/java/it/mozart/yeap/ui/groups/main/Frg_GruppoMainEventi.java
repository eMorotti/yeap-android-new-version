package it.mozart.yeap.ui.groups.main;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import javax.inject.Inject;

import butterknife.BindView;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;
import it.mozart.yeap.R;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.ui.base.BaseFragment;
import it.mozart.yeap.ui.groups.main.adapters.GruppoMainEventiAdapter;

public class Frg_GruppoMainEventi extends BaseFragment {

    @BindView(R.id.recyclerview)
    RecyclerView mRecyclerView;
    @BindView(R.id.no_items)
    View mNoItems;

    // SUPPORT

    @Inject
    Realm realm;

    private GruppoMainEventiAdapter mAdapter;
    private String mGruppoId;

    private static final String PARAM_ID = "paramId";

    // SYSTEM

    public static Frg_GruppoMainEventi newInstance(String gruppoId) {
        Bundle args = new Bundle();
        args.putString(PARAM_ID, gruppoId);
        Frg_GruppoMainEventi fragment = new Frg_GruppoMainEventi();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_main_gruppo_eventi;
    }

    @Override
    protected void initValues() {

    }

    @Override
    protected void loadParameters(Bundle arguments) {
        mGruppoId = arguments.getString(PARAM_ID);
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    public void onDestroyView() {
        // Fix problema leak listener realm
        mRecyclerView.setAdapter(null);
        super.onDestroyView();
    }

    @Override
    protected void initialize() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(mAdapter = new GruppoMainEventiAdapter(getContext(), new EventDao(realm).getListEventsFromGroup(mGruppoId)));

        mNoItems.setVisibility(mAdapter.getItemCount() != 0 ? View.GONE : View.VISIBLE);
        mAdapter.setOnChangeListener(new RealmRecyclerViewAdapter.OnChangeListener() {
            @Override
            public void changed() {
                if (mNoItems != null) {
                    mNoItems.setVisibility(mAdapter.getItemCount() != 0 ? View.GONE : View.VISIBLE);
                }
            }

            @Override
            public void inserted(int position) {

            }
        });
    }
}
