package it.mozart.yeap.ui.signin;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.BaseUIManager;
import com.facebook.accountkit.ui.LoginType;
import com.facebook.accountkit.ui.UIManager;

import javax.inject.Inject;

import butterknife.OnClick;
import it.mozart.yeap.R;
import it.mozart.yeap.api.models.APIError;
import it.mozart.yeap.api.models.AccountApiModel;
import it.mozart.yeap.api.services.ProfileApi;
import it.mozart.yeap.helpers.AnalitycsProvider;
import it.mozart.yeap.services.syncAdapter.SyncInit;
import it.mozart.yeap.ui.base.BaseActivity;
import it.mozart.yeap.utility.APIErrorUtils;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.DeviceIdGenerator;
import it.mozart.yeap.utility.Prefs;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class Act_Login extends BaseActivity {

    @OnClick(R.id.register)
    void registerClicked() {
        AccountKit.initialize(getApplicationContext(), new AccountKit.InitializeCallback() {
            @Override
            public void onInitialized() {
                getDynamicView().showLoading();
                Intent intent = new Intent(Act_Login.this, AccountKitActivity.class);
                AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder = new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.PHONE,
                        AccountKitActivity.ResponseType.CODE
                );
                UIManager uiManager = new BaseUIManager(LoginType.PHONE, R.style.FacebookTheme);
                configurationBuilder.setUIManager(uiManager);
                intent.putExtra(AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION, configurationBuilder.build());
                startActivityForResult(intent, APP_REQUEST_CODE);
            }
        });
    }

    @OnClick(R.id.info)
    void infoClicked() {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.yeap.cloud/terms.html")));
    }

    // SUPPORT

    @Inject
    ProfileApi profileApi;

    public static final int APP_REQUEST_CODE = 99;

    // SYSTEM

    @Override
    protected int getLayoutId() {
        return R.layout.act_login;
    }

    @Override
    protected void initVariables() {

    }

    @Override
    protected void loadParameters(Bundle extras) {

    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    protected void initialize() {

    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == APP_REQUEST_CODE) {
            AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
            if (loginResult.getError() != null) {
                AnalitycsProvider.getInstance().logLogin(mFirebaseAnalytics, false, "facebook");
                Toast.makeText(this, loginResult.getError().getUserFacingMessage(), Toast.LENGTH_LONG).show();
                getDynamicView().hide();
            } else if (loginResult.wasCancelled()) {
                getDynamicView().hide();
            } else {
                if (loginResult.getAuthorizationCode() != null) {
                    final AccountApiModel model = new AccountApiModel();
                    model.setAuthorizationCode(loginResult.getAuthorizationCode());
                    model.setDeviceId(DeviceIdGenerator.readDeviceId(this));
                    registerSubscription(profileApi.login(model)
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Action1<AccountApiModel>() {
                                @Override
                                public void call(AccountApiModel accountApiModel) {
                                    AnalitycsProvider.getInstance().logLogin(mFirebaseAnalytics, true);

                                    Prefs.putString(Constants.PREF_DEVICE_ID, model.getDeviceId());
                                    Prefs.putString(Constants.PREF_ACCESS_TOKEN, accountApiModel.getAccessToken());

                                    SyncInit syncInit = new SyncInit(Act_Login.this);
                                    syncInit.execute();

                                    startActivity(Act_CreateProfile.newIntent(accountApiModel.getProfile()));
                                    finish();
                                }
                            }, new Action1<Throwable>() {
                                @Override
                                public void call(Throwable throwable) {
                                    AnalitycsProvider.getInstance().logLogin(mFirebaseAnalytics, false);

                                    getDynamicView().hide();
                                    APIError error = APIErrorUtils.parseError(Act_Login.this, throwable);
                                    new MaterialDialog.Builder(Act_Login.this)
                                            .content(error.message())
                                            .positiveText(getString(R.string.general_riprova))
                                            .negativeText(getString(R.string.general_esci))
                                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                @Override
                                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                    getDynamicView().showLoading();
                                                    onActivityResult(requestCode, resultCode, data);
                                                }
                                            })
                                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                                @Override
                                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                    finish();
                                                }
                                            })
                                            .cancelable(false)
                                            .show();
                                }
                            }));
                } else {
                    Toast.makeText(this, getString(R.string.general_error_fb_authorization), Toast.LENGTH_LONG).show();
                    startActivity(new Intent(this, Act_Login.class));
                    finish();
                }
            }
        }
    }
}
