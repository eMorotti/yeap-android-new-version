package it.mozart.yeap.ui.sondaggi;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.UUID;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.api.models.APIError;
import it.mozart.yeap.api.models.SondaggioVoceApiModel;
import it.mozart.yeap.api.services.PollApi;
import it.mozart.yeap.events.ApiCallResultEvent;
import it.mozart.yeap.realm.Poll;
import it.mozart.yeap.realm.PollItem;
import it.mozart.yeap.realm.dao.PollDao;
import it.mozart.yeap.ui.base.BaseActivityWithSyncService;
import it.mozart.yeap.utility.APIErrorUtils;
import it.mozart.yeap.utility.Utils;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class Act_AggiungiVoceSondaggio extends BaseActivityWithSyncService {

    @BindView(R.id.risposta)
    EditText mRisposta;

    @OnClick(R.id.layout)
    void layoutClicked() {
        Utils.hideKeyboard(this);
    }

    // SUPPORT

    @Inject
    Realm realm;
    @Inject
    PollApi pollApi;

    private Poll mSondaggio;
    private String mId;

    private static final String PARAM_ID = "paramId";

    // SYSYEM

    public static Intent newIntent(String sondaggioId) {
        Intent intent = new Intent(App.getContext(), Act_AggiungiVoceSondaggio.class);
        intent.putExtra(PARAM_ID, sondaggioId);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUseBus(true);

        if (mToolbar != null) {
            mToolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_sondaggio_aggiungi_voce;
    }

    @Override
    protected void initVariables() {
    }

    @Override
    protected void loadParameters(Bundle extras) {
        mId = extras.getString(PARAM_ID);
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_sondaggio_voce_nuovo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_crea:
                crea();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        cancelTimer();
        Utils.hideKeyboard(this);
        super.onBackPressed();
        overridePendingTransition(0, R.anim.slide_out_bottom_slow);
    }

    @Override
    protected void initialize() {
        mSondaggio = new PollDao(realm).getPoll(mId);
        if (mSondaggio == null) {
            Toast.makeText(this, getString(R.string.general_error_sondaggio_non_presente), Toast.LENGTH_LONG).show();
            finish();
            overridePendingTransition(0, R.anim.slide_out_bottom);
        } else {
            setToolbarTitle(mSondaggio.getQuestion());
            setToolbarSubtitle(getString(R.string.app_nuova_voce_sondaggio));

            mRisposta.setHint(mSondaggio.getQuestion());

            setRequestId(UUID.randomUUID().toString());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ApiCallResultEvent event) {
        if (event.getRequestId().equals(getRequestId())) {
            cancelTimer();
            finish();
            overridePendingTransition(0, R.anim.slide_out_bottom_slow);
        }
    }

    // FUNCTIONS

    /**
     * Creo la voce sondaggio
     */
    private void crea() {
        if (mRisposta.getText().length() == 0) {
            Toast.makeText(Act_AggiungiVoceSondaggio.this, getString(R.string.sondaggio_voce_risposta_errore), Toast.LENGTH_SHORT).show();
            return;
        }

        getDynamicView().showLoading();
        registerSubscription(pollApi.createPollItem(getRequestId(), mSondaggio.getEventUuid(), mId, new SondaggioVoceApiModel(mRisposta.getText().toString()))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<PollItem>() {
                    @Override
                    public void call(PollItem voce) {
                        startTimer();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        getDynamicView().hide();

                        APIError error = APIErrorUtils.parseError(Act_AggiungiVoceSondaggio.this, throwable);
                        Toast.makeText(Act_AggiungiVoceSondaggio.this, error.message(), Toast.LENGTH_SHORT).show();
                    }
                })
        );
    }
}
