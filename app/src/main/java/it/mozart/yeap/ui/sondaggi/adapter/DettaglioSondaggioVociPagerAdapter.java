package it.mozart.yeap.ui.sondaggi.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

import it.mozart.yeap.realm.PollItem;
import it.mozart.yeap.ui.sondaggi.Frg_DettaglioSondaggioVoce;

public class DettaglioSondaggioVociPagerAdapter extends FragmentStatePagerAdapter {

    private List<PollItem> mVoci;

    public DettaglioSondaggioVociPagerAdapter(FragmentManager fragmentManager, List<PollItem> voci) {
        super(fragmentManager);
        mVoci = voci;
    }

    @Override
    public Fragment getItem(int position) {
        return Frg_DettaglioSondaggioVoce.newInstance(mVoci.get(position).getUuid());
    }

    @Override
    public int getCount() {
        return mVoci.size();
    }
}
