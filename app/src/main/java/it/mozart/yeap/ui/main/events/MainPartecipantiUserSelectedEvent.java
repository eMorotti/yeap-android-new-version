package it.mozart.yeap.ui.main.events;

import it.mozart.yeap.realm.User;

public class MainPartecipantiUserSelectedEvent {

    private User utente;
    private boolean remove;

    public MainPartecipantiUserSelectedEvent(User utente, boolean remove) {
        this.utente = utente;
        this.remove = remove;
    }

    public User getUtente() {
        return utente;
    }

    public boolean isRemove() {
        return remove;
    }
}
