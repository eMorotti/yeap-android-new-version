package it.mozart.yeap.ui.views;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;

import it.mozart.yeap.R;

public class AnimCheckBox extends View {
    private final String TAG = "AnimCheckBox";
    private ValueAnimator mAnimator;
    private Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private RectF mRectF = new RectF();
    private RectF mInnerRectF = new RectF();
    private Path mPath = new Path();
    private float mSweepAngle;
    private final double mSin27 = Math.sin(Math.toRadians(27));
    private final double mSin63 = Math.sin(Math.toRadians(63));
    private float mHookStartY;
    private float mBaseLeftHookOffset;
    private float mEndLeftHookOffset;
    private int size;
    private boolean mChecked = true;
    private boolean mUseLikeRadioButton = false;
    private boolean mEnableAnimation = true;
    private boolean mEnabled = true;
    private boolean mEnabledClick = true;
    private boolean mIsLoading = false;
    private boolean mIgnoreClick = false;
    private float mHookOffset;
    private float mHookSize;
    private int mInnerCircleAlpha = 0XFF;
    private int mStrokeWidth = 1;
    private int mStrokeWidthBase = 2;
    private final int mDuration = 250;
    private int mStrokeColorBase;
    private int mStrokeColor = Color.BLUE;
    private int mHookColor = Color.BLACK;
    private int mCircleColor = Color.WHITE;
    private int mDisabledColor = Color.BLACK;
    private OnCheckedChangeListener mOnCheckedChangeListener;

    private boolean myselfIn = true;

    public AnimCheckBox(Context context) {
        this(context, null);
    }

    public AnimCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray array = getContext().obtainStyledAttributes(attrs, R.styleable.AnimCheckBox);
            mStrokeWidth = (int) array.getDimension(R.styleable.AnimCheckBox_ac_stroke_width, dip(mStrokeWidth));
            mStrokeColor = array.getColor(R.styleable.AnimCheckBox_ac_stroke_color, mStrokeColor);
            mHookColor = array.getColor(R.styleable.AnimCheckBox_ac_hook_color, mHookColor);
            mCircleColor = array.getColor(R.styleable.AnimCheckBox_ac_circle_color, mCircleColor);
            array.recycle();
        } else {
            mStrokeWidth = dip(mStrokeWidth);
        }
        mStrokeColorBase = ContextCompat.getColor(getContext(), R.color.colorTransparentWhite);
        mStrokeWidthBase = dip(2);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(mStrokeWidth);
        mPaint.setColor(mStrokeColor);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!myselfIn) {
                    return;
                }
                if (isClickable()) {
                    if (!mIgnoreClick) {
                        setChecked(!mChecked);
                    } else {
                        if (mOnCheckedChangeListener != null) {
                            mOnCheckedChangeListener.onChange(!mChecked);
                        }
                    }
                }
            }
        });
    }

    public void setMyselfIn(boolean myselfIn) {
        this.myselfIn = myselfIn;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        return super.dispatchTouchEvent(event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        if (MeasureSpec.getMode(widthMeasureSpec) == MeasureSpec.AT_MOST &&
                MeasureSpec.getMode(heightMeasureSpec) == MeasureSpec.AT_MOST) {
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) getLayoutParams();

            int defaultSize = 40;
            width = height = Math.min(dip(defaultSize) - params.leftMargin - params.rightMargin,
                    dip(defaultSize) - params.bottomMargin - params.topMargin);
        }
        int size = Math.min(width - getPaddingLeft() - getPaddingRight(),
                height - getPaddingBottom() - getPaddingTop());
        setMeasuredDimension(size, size);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        size = getWidth();
        int radius = (getWidth() - (2 * mStrokeWidth)) / 2;
        mRectF.set(mStrokeWidth, mStrokeWidth, size - mStrokeWidth, size - mStrokeWidth);
        mInnerRectF.set(mRectF);
        mInnerRectF.inset(mStrokeWidth / 2, mStrokeWidth / 2);
        mHookStartY = (float) (size / 2 - (radius * mSin27 + (radius - radius * mSin63))) * 0.8f;
        mBaseLeftHookOffset = (float) (radius * (1 - mSin63)) + mStrokeWidth / 2;
        float mBaseRightHookOffset = 0f;
        mEndLeftHookOffset = mBaseLeftHookOffset + (2 * size / 3 - mHookStartY) * 0.51f;
        float mEndRightHookOffset = mBaseRightHookOffset + (size / 3 + mHookStartY) * 0.61f;
        mHookSize = size - (mEndLeftHookOffset + mEndRightHookOffset);
        mHookOffset = mChecked ? mHookSize + mEndLeftHookOffset - mBaseLeftHookOffset : 0;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawCircle(canvas);
        if (!mIsLoading) {
            drawHook(canvas);
        }
    }

    private void drawCircle(Canvas canvas) {
        if (!mIsLoading) {
            if (mSweepAngle != 0) {
                initDrawBaseStrokeCirclePaint();
                canvas.drawArc(mRectF, 202, 360, false, mPaint);
            }
            initDrawStrokeCirclePaint();
            RectF newRectF = new RectF(mRectF);
            newRectF.set(newRectF.left + 1, newRectF.top + 1, newRectF.right - 1, newRectF.bottom - 1);
            if (!mEnabledClick) {
                canvas.drawArc(newRectF, 202, 360, false, mPaint);
            } else {
                canvas.drawArc(newRectF, 202, mSweepAngle, false, mPaint);
            }
            if (mEnabledClick) {
                initDrawAlphaStrokeCirclePaint();
                canvas.drawArc(newRectF, 202, mSweepAngle - 360, false, mPaint);
            }
        } else if (!mEnabledClick) {
            initDrawStrokeCirclePaint();
            RectF newRectF = new RectF(mRectF);
            newRectF.set(newRectF.left + 1, newRectF.top + 1, newRectF.right - 1, newRectF.bottom - 1);
            canvas.drawArc(newRectF, 202, mSweepAngle, false, mPaint);
        }
        initDrawInnerCirclePaint();
        canvas.drawArc(mRectF, 0, 360, false, mPaint);

        if (!mEnabledClick) {
            initDrawStrokeCirclePaint();
            RectF newRectF = new RectF(mRectF);
            newRectF.set(newRectF.left + 1, newRectF.top + 1, newRectF.right - 1, newRectF.bottom - 1);
            canvas.drawArc(newRectF, 202, 360, false, mPaint);
        }
    }

    private void drawHook(Canvas canvas) {
        if (mHookOffset == 0)
            return;
        initDrawHookPaint();
        mPath.reset();
        float offset = 0;
        if (mHookOffset <= (2 * size / 3 - mHookStartY - mBaseLeftHookOffset)) {
            mPath.moveTo(mBaseLeftHookOffset, mBaseLeftHookOffset + mHookStartY);
            mPath.lineTo(mBaseLeftHookOffset + mHookOffset, mBaseLeftHookOffset + mHookStartY + mHookOffset);
        } else if (mHookOffset <= mHookSize) {
            mPath.moveTo(mBaseLeftHookOffset, mBaseLeftHookOffset + mHookStartY);
            mPath.lineTo(2 * size / 3 - mHookStartY, 3 * size / 5);
            mPath.lineTo(mHookOffset + mBaseLeftHookOffset,
                    3 * size / 5 - (mHookOffset - (3 * size / 5 - mHookStartY - mBaseLeftHookOffset)));
        } else {
            offset = mHookOffset - mHookSize;
            mPath.moveTo(mBaseLeftHookOffset + offset, mBaseLeftHookOffset + mHookStartY + offset);
            mPath.lineTo(2 * size / 3 - mHookStartY, 5 * size / 8);
            mPath.lineTo(mHookSize + mBaseLeftHookOffset + offset,
                    5 * size / 8 - (mHookSize - (5 * size / 8 - mHookStartY - mBaseLeftHookOffset) + offset));
        }
        canvas.drawPath(mPath, mPaint);
    }

    private void initDrawHookPaint() {
        mPaint.setAlpha(0xFF);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(mStrokeWidth);
        if(mEnabled) {
            mPaint.setColor(mHookColor);
        } else {
            mPaint.setColor(Color.WHITE);
        }
    }

    private void initDrawStrokeCirclePaint() {
        mPaint.setAlpha(0xFF);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(mStrokeWidth);
        mPaint.setColor(mStrokeColor);
    }

    private void initDrawBaseStrokeCirclePaint() {
        mPaint.setAlpha(0xFF);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(mStrokeWidthBase);
        mPaint.setColor(mStrokeColorBase);
    }

    private void initDrawAlphaStrokeCirclePaint() {
        mPaint.setStrokeWidth(mStrokeWidth / 2);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(Color.BLACK);
        mPaint.setAlpha((int) toRange(mInnerCircleAlpha, 0, 0xFF, 0, 0x60));
    }

    private void initDrawInnerCirclePaint() {
        mPaint.setStyle(Paint.Style.FILL);
        if (mEnabled || mIsLoading) {
            mPaint.setColor(mCircleColor);
            mPaint.setAlpha(mInnerCircleAlpha);
        } else {
            mPaint.setColor(mDisabledColor);
            mPaint.setAlpha(85);
        }
    }

    private void startCheckedAnim() {
        mAnimator = new ValueAnimator();
        final float hookMaxValue = mHookSize + mEndLeftHookOffset - mBaseLeftHookOffset;
        final float circleMaxFraction = mHookSize / hookMaxValue;
        final float circleMaxValue = 360 / circleMaxFraction;
        mAnimator.setFloatValues(0, 1);
        mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float fraction = animation.getAnimatedFraction();
                mHookOffset = fraction * hookMaxValue;
                if (fraction <= circleMaxFraction) {
                    mSweepAngle = (int) ((circleMaxFraction - fraction) * circleMaxValue);
                } else {
                    mSweepAngle = 0;
                }
                mInnerCircleAlpha = (int) (fraction * 0xFF);
                invalidate();
            }
        });
        mAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        mAnimator.setDuration(mDuration).start();
    }

    private void startUnCheckedAnim() {
        mAnimator = new ValueAnimator();
        final float hookMaxValue = mHookSize + mEndLeftHookOffset - mBaseLeftHookOffset;
        final float circleMinFraction = (mEndLeftHookOffset - mBaseLeftHookOffset) / hookMaxValue;
        final float circleMaxValue = 360 / (1 - circleMinFraction);
        mAnimator.setFloatValues(0, 1);
        mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float circleFraction = animation.getAnimatedFraction();
                float fraction = 1 - circleFraction;
                mHookOffset = fraction * hookMaxValue;
                if (circleFraction >= circleMinFraction) {
                    mSweepAngle = (int) ((circleFraction - circleMinFraction) * circleMaxValue);
                } else {
                    mSweepAngle = 0;
                }
                mInnerCircleAlpha = (int) (fraction * 0xFF);
                invalidate();
            }
        });
        mAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        mAnimator.setDuration(mDuration).start();
    }

    private void startAnim() {
        clearAnimation();
        if (mAnimator != null)
            mAnimator.cancel();
        if (mChecked) {
            startCheckedAnim();
        } else {
            startUnCheckedAnim();
        }
    }


    private int getAlphaColor(int color, int alpha) {
        alpha = alpha < 0 ? 0 : alpha;
        alpha = alpha > 255 ? 255 : alpha;
        return (color & 0x00FFFFFF) | alpha << 24;
    }

    public boolean isChecked() {
        return mChecked;
    }


    /**
     * setChecked with Animation
     *
     * @param checked true if checked, false if unchecked
     */
    public void setChecked(boolean checked) {
        if (mUseLikeRadioButton) {
            if (isChecked() && !checked)
                return;
        }
        setChecked(checked, true);
    }

    /**
     * @param checked   true if checked, false if unchecked
     * @param animation true with animation,false without animation
     */
    public void setChecked(boolean checked, boolean animation) {
        if (checked == this.mChecked || isLoading()) {
            return;
        }
        this.mChecked = checked;
        clearAnimation();
        if (mAnimator != null)
            mAnimator.cancel();
        if (animation && mEnableAnimation) {
            startAnim();
        } else {
            if (mChecked) {
                mInnerCircleAlpha = 0xFF;
                mSweepAngle = 0;
                mHookOffset = mHookSize + mEndLeftHookOffset - mBaseLeftHookOffset;
            } else {
                mInnerCircleAlpha = 0x00;
                mSweepAngle = 360;
                mHookOffset = 0;
            }
            invalidate();
        }
        if (mOnCheckedChangeListener != null) {
            mOnCheckedChangeListener.onChange(mChecked);
        }
    }

    public boolean isUseLikeRadioButton() {
        return mUseLikeRadioButton;
    }

    public void setUseLikeRadioButton(boolean mUseLikeRadioButton) {
        this.mUseLikeRadioButton = mUseLikeRadioButton;
    }

    public boolean isEnableAnimation() {
        return mEnableAnimation;
    }

    public void setEnableAnimation(boolean enableAnimation) {
        this.mEnableAnimation = enableAnimation;
    }

    public boolean isEnabled() {
        return mEnabled;
    }

    public void setEnabled(boolean enabled) {
        if (!enabled) {
            setChecked(true, false);
            setClickable(false);
        } else {
            setClickable(true);
        }
        this.mEnabled = enabled;
    }

    public boolean isEnabledClick() {
        return mEnabledClick;
    }

    public void setEnabledClick(boolean enabled) {
        if (!enabled) {
            setClickable(false);
        } else {
            setClickable(true);
        }
        this.mEnabledClick = enabled;
    }

    public boolean isLoading() {
        return mIsLoading;
    }

    public void setIsLoading(boolean isLoading) {
        if (isLoading) {
//            this.setChecked(true, false);
            this.setClickable(false);
        } else {
            if (mEnabled && mEnabledClick) {
                this.setClickable(true);
            }
        }
        this.mIsLoading = isLoading;
    }

    public void setIgnoreClick(boolean ignoreClick) {
        this.mIgnoreClick = ignoreClick;
    }

    private int dip(int dip) {
        return (int) getContext().getResources().getDisplayMetrics().density * dip;
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        return super.onSaveInstanceState();
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        super.onRestoreInstanceState(state);
    }

    public float toRange(float value, float oldMin, float oldMax, float newMin, float newMax) {
        return (((value - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin;
    }

    /**
     * setOnCheckedChangeListener
     *
     * @param listener the OnCheckedChangeListener listener
     */
    public void setOnCheckedChangeListener(OnCheckedChangeListener listener) {
        this.mOnCheckedChangeListener = listener;
    }

    public interface OnCheckedChangeListener {
        void onChange(boolean checked);
    }

}