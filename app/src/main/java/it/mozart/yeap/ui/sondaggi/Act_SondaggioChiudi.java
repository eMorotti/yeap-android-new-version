package it.mozart.yeap.ui.sondaggi;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.parceler.Parcels;

import java.util.UUID;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.Sort;
import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.api.models.APIError;
import it.mozart.yeap.api.services.EventApi;
import it.mozart.yeap.common.DoveInfo;
import it.mozart.yeap.common.QuandoInfo;
import it.mozart.yeap.events.ApiCallResultEvent;
import it.mozart.yeap.helpers.AnalitycsProvider;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.EventInfo;
import it.mozart.yeap.realm.Poll;
import it.mozart.yeap.realm.PollItem;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.PollDao;
import it.mozart.yeap.ui.base.BaseActivityWithSyncService;
import it.mozart.yeap.ui.support.Act_Dove;
import it.mozart.yeap.ui.support.Act_Quando;
import it.mozart.yeap.utility.APIErrorUtils;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.DateUtils;
import it.mozart.yeap.utility.EasySpan;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class Act_SondaggioChiudi extends BaseActivityWithSyncService {

    @BindView(R.id.dove_layout)
    ViewGroup mDoveLayout;
    @BindView(R.id.dove_main_layout)
    ViewGroup mDoveMainLayout;
    @BindView(R.id.dove)
    TextView mDove;
    @BindView(R.id.dove_delete)
    ImageButton mDoveDelete;
    @BindView(R.id.quando_layout)
    ViewGroup mQuandoLayout;
    @BindView(R.id.quando_main_layout)
    ViewGroup mQuandoMainLayout;
    @BindView(R.id.quando)
    TextView mQuando;
    @BindView(R.id.quando_delete)
    ImageButton mQuandoDelete;

    @OnClick(R.id.dove)
    void doveClicked() {
        doveSelected(mDoveInfo != null);
    }

    @OnClick(R.id.dove_delete)
    void doveDeleteClicked() {
        mDove.setText(null);
        mDoveInfo = null;

        if (mWherePoll != null && mWherePoll.getVoci().size() != 0) {
            mDoveSelected = 0;
        } else {
            mDoveSelected = -1;
        }
        for (int i = 0; i < mDoveLayout.getChildCount(); i++) {
            updateColors(mDoveLayout.getChildAt(i), mDoveSelected == i);
        }
    }

    @OnClick(R.id.quando)
    void quandoClicked() {
        quandoSelected(mQuandoInfo != null);
    }

    @OnClick(R.id.quando_delete)
    void quandoDeleteClicked() {
        mQuando.setText(null);
        mQuandoInfo = null;

        if (mWhenPoll != null && mWhenPoll.getVoci().size() != 0) {
            mQuandoSelected = 0;
        } else {
            mQuandoSelected = -1;
        }
        for (int i = 0; i < mQuandoLayout.getChildCount(); i++) {
            updateColors(mQuandoLayout.getChildAt(i), mQuandoSelected == i);
        }
    }

    @OnClick(R.id.chiudi)
    void chiudiClicked() {
        if (canConfirmEvent()) {
            EventInfo info = new EventInfo();
            if (mIsDove) {
                if (mDoveSelected != -1) {
                    PollItem pollItem = mWherePoll.getVoci().sort(PollItem.NUM_VOTES_YES, Sort.DESCENDING).get(mDoveSelected);
                    info.updateDoveInfo(pollItem.generateDoveModel());
                } else {
                    info.updateDoveInfo(mDoveInfo);
                }
            } else {
                if (mQuandoSelected != -1) {
                    PollItem pollItem = mWhenPoll.getVoci().sort(PollItem.NUM_VOTES_YES, Sort.DESCENDING).get(mQuandoSelected);
                    info.updateQuandoInfo(pollItem.generateQuandoModel());
                } else {
                    info.updateQuandoInfo(mQuandoInfo);
                }
            }

            setRequestId(UUID.randomUUID().toString());
            getDynamicView().showLoading();

            Observable<Object> observable;
            if (mIsDove) {
                observable = eventApi.closeWherePoll(getRequestId(), mId, info);
            } else {
                observable = eventApi.closeWhenPoll(getRequestId(), mId, info);
            }
            registerSubscription(observable
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<Object>() {
                        @Override
                        public void call(Object object) {
                            startTimer();
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            getDynamicView().hide();

                            APIError error = APIErrorUtils.parseError(Act_SondaggioChiudi.this, throwable);
                            Toast.makeText(Act_SondaggioChiudi.this, error.message(), Toast.LENGTH_SHORT).show();
                        }
                    }));
        } else {
            Toast.makeText(this, getString(R.string.general_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    // SUPPORT

    @Inject
    Realm realm;
    @Inject
    EventApi eventApi;

    private DoveInfo mDoveInfo;
    private QuandoInfo mQuandoInfo;
    private Event mEvent;
    private Poll mWherePoll;
    private Poll mWhenPoll;
    private String mId;
    private int mDoveSelected;
    private int mQuandoSelected;
    private boolean mIsDove;

    private static final String PARAM_ID = "paramId";
    private static final String PARAM_IS_DOVE = "paramIsDove";

    private static final int QUANDO_CODE = 3001;
    private static final int DOVE_CODE = 3002;

    // SYSTEM

    public static Intent newIntent(String eventoId, boolean isDove) {
        Intent intent = new Intent(App.getContext(), Act_SondaggioChiudi.class);
        intent.putExtra(PARAM_ID, eventoId);
        intent.putExtra(PARAM_IS_DOVE, isDove);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUseBus(true);

        setToolbarTitle(getString(R.string.sondaggio_chiudi_title));
        setToolbarSubtitle(mIsDove ? getString(R.string.sondaggio_chiudi_subtitle_where) : getString(R.string.sondaggio_chiudi_subtitle_when));
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_sondaggio_chiudi;
    }

    @Override
    protected void initVariables() {
        mDoveSelected = -1;
        mQuandoSelected = -1;
    }

    @Override
    protected void loadParameters(Bundle extras) {
        mId = extras.getString(PARAM_ID);
        mIsDove = extras.getBoolean(PARAM_IS_DOVE);
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    protected void initialize() {
        EventDao eventDao = new EventDao(realm);
        PollDao pollDao = new PollDao(realm);
        mEvent = eventDao.getEvent(mId);
        if (mIsDove) {
            mQuandoMainLayout.setVisibility(View.GONE);
            if (mEvent.isWherePollEnabled()) {
                mWherePoll = pollDao.getPoll(mEvent.getWherePollUuid());
            }
        } else {
            mDoveMainLayout.setVisibility(View.GONE);
            if (mEvent.isWhenPollEnabled()) {
                mWhenPoll = pollDao.getPoll(mEvent.getWhenPollUuid());
            }
        }

        if (mIsDove) {
            if (mWherePoll != null && mWherePoll.getVoci().size() != 0) {
                mDoveSelected = 0;
                int position = 0;
                for (PollItem pollItem : mWherePoll.getVoci().sort(PollItem.NUM_VOTES_YES, Sort.DESCENDING)) {
                    View view = LayoutInflater.from(this).inflate(R.layout.item_evento_conferma, mDoveLayout, false);
                    updateVoceView(view, pollItem, position);
                    mDoveLayout.addView(view);

                    position++;
                }

                for (int i = 0; i < mDoveLayout.getChildCount(); i++) {
                    updateColors(mDoveLayout.getChildAt(i), mDoveSelected == i);
                }

                mDove.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.length() == 0) {
                            mDove.setBackgroundResource(R.drawable.background_transparent_white);
                            mDoveDelete.setVisibility(View.GONE);
                        } else {
                            mDove.setBackgroundResource(R.drawable.background_transparent_more_white);
                            mDoveDelete.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
            } else {
                mDove.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.length() == 0) {
                            mDove.setBackgroundResource(R.drawable.background_transparent_white_changed);
                            mDoveDelete.setVisibility(View.GONE);
                        } else {
                            mDove.setBackgroundResource(R.drawable.background_transparent_more_white);
                            mDoveDelete.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
                if (mEvent.getEventInfo().getWhere() != null) {
                    mDoveInfo = mEvent.getEventInfo().generateDoveModel();
                    mDove.setText(mEvent.getEventInfo().getWhere());
                    mDoveDelete.setColorFilter(Color.BLACK);
                } else {
                    mDove.setText("");
                }
            }
        } else {
            if (mWhenPoll != null && mWhenPoll.getVoci().size() != 0) {
                mQuandoSelected = 0;
                int position = 0;
                for (PollItem pollItem : mWhenPoll.getVoci().sort(PollItem.NUM_VOTES_YES, Sort.DESCENDING)) {
                    View view = LayoutInflater.from(this).inflate(R.layout.item_evento_conferma, mQuandoLayout, false);
                    updateVoceView(view, pollItem, position);
                    mQuandoLayout.addView(view);

                    position++;
                }

                for (int i = 0; i < mQuandoLayout.getChildCount(); i++) {
                    updateColors(mQuandoLayout.getChildAt(i), mQuandoSelected == i);
                }

                mQuando.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.length() == 0) {
                            mQuando.setBackgroundResource(R.drawable.background_transparent_white);
                            mQuandoDelete.setVisibility(View.GONE);
                        } else {
                            mQuando.setBackgroundResource(R.drawable.background_transparent_more_white);
                            mQuandoDelete.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
            } else {
                mQuando.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.length() == 0) {
                            mQuando.setBackgroundResource(R.drawable.background_transparent_white_changed);
                            mQuandoDelete.setVisibility(View.GONE);
                        } else {
                            mQuando.setBackgroundResource(R.drawable.background_transparent_more_white);
                            mQuandoDelete.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
                if (mEvent.getEventInfo().isQuandoPresent()) {
                    mQuandoInfo = mEvent.getEventInfo().generateQuandoModel();
                    mQuando.setText(DateUtils.createQuandoText(mQuandoInfo));
                    mQuandoDelete.setColorFilter(Color.BLACK);
                } else {
                    mQuando.setText("");
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case DOVE_CODE:
                    doveManage((DoveInfo) Parcels.unwrap(data.getParcelableExtra(Constants.INTENT_DOVE)));
                    break;
                case QUANDO_CODE:
                    quandoManage((QuandoInfo) Parcels.unwrap(data.getParcelableExtra(Constants.INTENT_QUANDO)));
                    break;
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ApiCallResultEvent event) {
        if (event.getRequestId().equals(getRequestId())) {
            cancelTimer();
            Toast.makeText(this, getString(R.string.sondaggio_chiudi_message), Toast.LENGTH_SHORT).show();
            setResult(Activity.RESULT_OK);
            finish();
        }
    }

    // FUNCTIONS

    private void updateVoceView(@NonNull View view, final PollItem voce, final int position) {
        final TextView text = view.findViewById(R.id.item);
        TextView count = view.findViewById(R.id.count);

        if (mIsDove) {
            text.setText(voce.getWhere());
        } else {
            text.setText(DateUtils.createQuandoText(voce.generateQuandoModel()));
        }

        long current = voce.getNumVoteYes();
        EasySpan easySpan = new EasySpan.Builder()
                .appendText(String.valueOf(current))
                .appendSpans("/" + mEvent.getGuests().size(), new RelativeSizeSpan(0.5f))
                .build();
        count.setText(easySpan.getSpannedText());

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIsDove) {
                    mDoveSelected = position;
                    for (int i = 0; i < mDoveLayout.getChildCount(); i++) {
                        updateColors(mDoveLayout.getChildAt(i), mDoveSelected == i);
                    }

                    mDove.setTextColor(Color.WHITE);
                    mDoveDelete.setColorFilter(Color.WHITE);
                    if (mWherePoll != null && mWherePoll.getVoci().size() != 0) {
                        mDove.setBackgroundResource(R.drawable.background_transparent_white);
                    } else {
                        mDove.setBackgroundResource(R.drawable.background_transparent_white_changed);
                    }
                } else {
                    mQuandoSelected = position;
                    for (int i = 0; i < mQuandoLayout.getChildCount(); i++) {
                        updateColors(mQuandoLayout.getChildAt(i), mQuandoSelected == i);
                    }

                    mQuando.setTextColor(Color.WHITE);
                    mQuandoDelete.setColorFilter(Color.WHITE);
                    if (mWhenPoll != null && mWhenPoll.getVoci().size() != 0) {
                        mQuando.setBackgroundResource(R.drawable.background_transparent_white);
                    } else {
                        mQuando.setBackgroundResource(R.drawable.background_transparent_white_changed);
                    }
                }
            }
        });
    }

    private void updateColors(@NonNull View view, boolean selected) {
        TextView text = view.findViewById(R.id.item);
        TextView count = view.findViewById(R.id.count);

        if (selected) {
            text.setBackgroundResource(R.drawable.background_transparent_more_white);
            text.setTextColor(Color.BLACK);
            count.setTextColor(Color.BLACK);
        } else {
            text.setBackgroundResource(R.drawable.background_transparent_white);
            text.setTextColor(Color.WHITE);
            count.setTextColor(Color.WHITE);
        }
    }

    /**
     * Crea il titolo per l'activity Dove
     *
     * @return titolo
     */
    private String createDoveTitle() {
        return getString(R.string.nuova_evento_dove_title);
    }

    /**
     * Crea il sottotitolo per l'activity Dove
     *
     * @return sottotitolo
     */
    private String createDoveSubtitle() {
        return getString(R.string.nuova_evento_dove_subtitle);
    }

    private void doveSelected(boolean alreadyPresent) {
        // Apro subito l'activity se il model è null
        if (!alreadyPresent) {
            AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "setActivityWhere", "pollClose");
            startActivityForResult(Act_Dove.newIntent(createDoveTitle(), createDoveSubtitle(), null), DOVE_CODE);
        } else {
            mDove.setBackgroundResource(R.drawable.background_transparent_more_white);
            mDove.setTextColor(Color.BLACK);
            mDoveDelete.setColorFilter(Color.BLACK);

            mDoveSelected = -1;
            for (int i = 0; i < mDoveLayout.getChildCount(); i++) {
                updateColors(mDoveLayout.getChildAt(i), mDoveSelected == i);
            }
        }
    }

    private void doveManage(DoveInfo doveInfo) {
        AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "setActivityWhereConfirm", "pollClose");
        // Aggiorno layout
        if (doveInfo != null && doveInfo.getDove() != null) {
            mDove.setText(doveInfo.getDove());
            mDove.setTextColor(Color.BLACK);
            mDoveDelete.setColorFilter(Color.BLACK);
            mDoveInfo = doveInfo;

            mDoveSelected = -1;
            for (int i = 0; i < mDoveLayout.getChildCount(); i++) {
                updateColors(mDoveLayout.getChildAt(i), mDoveSelected == i);
            }
        } else {
            mDove.setText(null);
            mDoveInfo = null;

            mDoveSelected = 0;
            for (int i = 0; i < mDoveLayout.getChildCount(); i++) {
                updateColors(mDoveLayout.getChildAt(i), mDoveSelected == i);
            }
        }
    }

    /**
     * Crea il titolo per l'activity Quando
     *
     * @return titolo
     */
    private String createQuandoTitle() {
        return getString(R.string.nuova_evento_quando_title);
    }

    /**
     * Crea il sottotitolo per l'activity Quando
     *
     * @return sottotitolo
     */
    private String createQuandoSubtitle() {
        return getString(R.string.nuova_evento_quando_subtitle);
    }

    private void quandoSelected(boolean alreadyPresent) {
        // Apro subito l'activity se il model è null
        if (!alreadyPresent) {
            AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "setActivityWhen", "pollClose");
            startActivityForResult(Act_Quando.newIntent(createQuandoTitle(), createQuandoSubtitle(), null), QUANDO_CODE);
        } else {
            mQuando.setBackgroundResource(R.drawable.background_transparent_more_white);
            mQuando.setTextColor(Color.BLACK);
            mQuandoDelete.setColorFilter(Color.BLACK);

            mQuandoSelected = -1;
            for (int i = 0; i < mQuandoLayout.getChildCount(); i++) {
                updateColors(mQuandoLayout.getChildAt(i), mQuandoSelected == i);
            }
        }
    }

    private void quandoManage(QuandoInfo quandoInfo) {
        AnalitycsProvider.getInstance().logCustomOrigin(mFirebaseAnalytics, "setActivityWhenConfirm", "pollClose");
        // Aggiorno layout
        if (quandoInfo != null && quandoInfo.isSomeInfoPresent()) {
            mQuando.setText(DateUtils.createQuandoText(quandoInfo));
            mQuando.setTextColor(Color.BLACK);
            mQuandoDelete.setColorFilter(Color.BLACK);
            mQuandoInfo = quandoInfo;

            mQuandoSelected = -1;
            for (int i = 0; i < mQuandoLayout.getChildCount(); i++) {
                updateColors(mQuandoLayout.getChildAt(i), mQuandoSelected == i);
            }
        } else {
            mQuando.setText(null);
            mQuandoInfo = null;

            mQuandoSelected = 0;
            for (int i = 0; i < mQuandoLayout.getChildCount(); i++) {
                updateColors(mQuandoLayout.getChildAt(i), mQuandoSelected == i);
            }
        }
    }

    private boolean canConfirmEvent() {
        if (mIsDove) {
            if (mDoveSelected == -1 && mDove.getText().length() == 0) {
                return false;
            }
        } else {
            if (mQuandoSelected == -1 && mQuando.getText().length() == 0) {
                return false;
            }
        }

        return true;
    }
}
