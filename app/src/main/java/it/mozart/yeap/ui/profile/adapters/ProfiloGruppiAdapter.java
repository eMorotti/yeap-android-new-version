package it.mozart.yeap.ui.profile.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;
import it.mozart.yeap.events.GroupSelectedEvent;
import it.mozart.yeap.realm.Group;
import it.mozart.yeap.ui.home.views.GroupLayout;

public class ProfiloGruppiAdapter extends RealmRecyclerViewAdapter<Group, RecyclerView.ViewHolder> {

    private Context mContext;

    public ProfiloGruppiAdapter(Context context, OrderedRealmCollection<Group> data) {
        super(data, true);
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProfiloGruppiViewHolder(new GroupLayout(mContext));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ProfiloGruppiViewHolder realHolder = (ProfiloGruppiViewHolder) holder;
        Group gruppo = getItem(position);

        ((GroupLayout) realHolder.itemView).bind(gruppo, false);
    }

    class ProfiloGruppiViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ProfiloGruppiViewHolder(View v) {
            super(v);

            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getLayoutPosition();
            Group gruppo = getItem(position);
            if (gruppo != null) {
                EventBus.getDefault().post(new GroupSelectedEvent(gruppo.getUuid()));
            }
        }
    }
}
