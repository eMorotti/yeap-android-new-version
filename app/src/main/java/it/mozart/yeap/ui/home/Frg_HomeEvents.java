package it.mozart.yeap.ui.home;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import butterknife.BindView;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;
import it.mozart.yeap.R;
import it.mozart.yeap.helpers.HomeScrollHelper;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.ui.base.BaseFragment;
import it.mozart.yeap.ui.home.adapters.HomeEventsAdapter;
import it.mozart.yeap.ui.home.events.EventsUpdatedEvent;

public class Frg_HomeEvents extends BaseFragment {

    @BindView(R.id.recyclerview)
    RecyclerView mRecyclerView;
    @BindView(R.id.no_items)
    View mNoItems;

    // SUPPORT

    @Inject
    Realm realm;
    @Inject
    HomeScrollHelper scrollHelper;

    private HomeEventsAdapter mAdapter;
    private float mScroll;

    private final String SAVED_SCROLL = "savedScroll";

    // SYSTEM

    @Override
    protected int getLayoutId() {
        return R.layout.frg_home_events;
    }

    @Override
    protected void initValues() {
        mScroll = 0;
    }

    @Override
    protected void loadParameters(Bundle arguments) {

    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {
        mScroll = savedInstanceState.getFloat(SAVED_SCROLL);
    }

    @Override
    protected void saveInfos(Bundle outState) {
        outState.putFloat(SAVED_SCROLL, mScroll);
    }

    @Override
    public void onDestroyView() {
        mRecyclerView.setAdapter(null);
        super.onDestroyView();
    }

    @Override
    protected void initialize() {
        scrollHelper.setScrollingEvents(false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        // Reset Scroll
        mScroll = 0;
        // Gestisco il problema con la differenza di altezza delle pagine nel viewpager (problema con l'appbarlayout)
        mRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                mRecyclerView.scrollToPosition(0);
            }
        });
        mRecyclerView.setAdapter(mAdapter = new HomeEventsAdapter(getContext(), new EventDao(realm).getListEventsHome()));

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                mScroll += dy;
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                scrollHelper.setScrollingEvents(newState != RecyclerView.SCROLL_STATE_IDLE);
            }
        });

        mNoItems.setVisibility(mAdapter.getItemCount() != 0 ? View.GONE : View.VISIBLE);
        mAdapter.setOnChangeListener(new RealmRecyclerViewAdapter.OnChangeListener() {
            @Override
            public void changed() {
                if (mNoItems != null) {
                    mNoItems.setVisibility(mAdapter.getItemCount() != 0 ? View.GONE : View.VISIBLE);
                    EventBus.getDefault().postSticky(new EventsUpdatedEvent());
                }
            }

            @Override
            public void inserted(int position) {
                if (mRecyclerView != null) {
                    RecyclerView.LayoutManager layoutManager = mRecyclerView.getLayoutManager();
                    if (layoutManager instanceof LinearLayoutManager) {
                        if (((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition() == 0) {
                            mRecyclerView.smoothScrollToPosition(0);
                        }
                    }
                }
            }
        });
    }
}
