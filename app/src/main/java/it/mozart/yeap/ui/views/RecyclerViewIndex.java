package it.mozart.yeap.ui.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import it.mozart.yeap.utility.Utils;

public class RecyclerViewIndex extends View {

    private Paint mPaint;
    private RecyclerView mRecyclerView;
    private TextView mTextView;
    private List<Alphabet> mAlphabet;
    private Alphabet mSelected;
    private boolean mIsAlphComplete;
    private int mPosition = -1;

    private static final String ALPHABET = "#ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public RecyclerViewIndex(Context context) {
        super(context);
    }

    public RecyclerViewIndex(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RecyclerViewIndex(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @SuppressWarnings("unused")
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public RecyclerViewIndex(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setRecyclerView(RecyclerView recyclerView, TextView textView) {
        mRecyclerView = recyclerView;
        mTextView = textView;
        setUpAlphabet();
    }

    private void setUpAlphabet() {
        mAlphabet = new ArrayList<>();
        for (char c : ALPHABET.toCharArray()) {
            mAlphabet.add(new Alphabet(String.valueOf(c)));
        }
        mIsAlphComplete = true;

        mPaint = new Paint();
        mPaint.setColor(Color.WHITE);
        mPaint.setTextAlign(Paint.Align.CENTER);
        mPaint.setStrokeWidth(Utils.dpToPx(2, getContext()));
    }

    public void updateAlphabet(List<Character> list) {
        if (list.size() > 8)
            return;
        mAlphabet.clear();
        for (Character c : list) {
            mAlphabet.add(new Alphabet(String.valueOf(c)));
        }
        mIsAlphComplete = false;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (mRecyclerView == null)
            return;

        int y = 0;
        int width = getWidth();
        int slice = (getHeight() - getPaddingBottom()) / mAlphabet.size();
        setTextSizeForWidth(mPaint, getWidth(), slice);

        for (Alphabet alphabet : mAlphabet) {
            if (!mIsAlphComplete) {
                mPaint.setAlpha(170);
                int posYCenter = (y + y + slice) / 2;
                canvas.drawLine(width / 2, y, width / 2, posYCenter - width / 2, mPaint);
                canvas.drawLine(width / 2, posYCenter + width / 2, width / 2, y + slice, mPaint);
            }
            if (!alphabet.isSet()) {
                alphabet.setPosition(y, y + slice);
            }
            if (mSelected != null && alphabet == mSelected)
                mPaint.setAlpha(250);
            else
                mPaint.setAlpha(170);
            canvas.drawText(alphabet.getWord(), width / 2, (y + slice / 2) + mPaint.descent(), mPaint);
            y += slice;
        }
    }

    private static void setTextSizeForWidth(Paint paint, float desiredWidth, float desiredHeight) {
        final float testTextSize = 48f;
        paint.setTextSize(testTextSize);
        Rect bounds = new Rect();

        paint.getTextBounds("m", 0, "m".length(), bounds);
        float desiredTextSize = testTextSize * desiredWidth / bounds.width();

        paint.getTextBounds("A", 0, "A".length(), bounds);
        float desiredTextSize2 = testTextSize * desiredHeight / bounds.height();

        paint.setTextSize(Math.min(desiredTextSize, desiredTextSize2) / 2);
    }

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {
        if (mRecyclerView != null) {
            final int action = event.getAction();
            switch (action) {
                case MotionEvent.ACTION_DOWN:
                    checkSelected(event.getY());
                    setRecyclerViewPosition();
                case MotionEvent.ACTION_MOVE:
                    checkSelected(event.getY());
                    setRecyclerViewPosition();
                    return true;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    hideTextView();
                    return true;
            }
        }
        return super.onTouchEvent(event);
    }

    public void changeSelected(String word) {
        if (mRecyclerView == null)
            return;
        if (mSelected == null)
            mSelected = mAlphabet.get(0);
        if (mSelected.getWord().equalsIgnoreCase(word))
            return;
        for (Alphabet alphabet : mAlphabet) {
            if (alphabet.getWord().equalsIgnoreCase(word)) {
                mSelected = alphabet;
                invalidate();
                break;
            }
        }
    }

    private void checkSelected(float y) {
        for (Alphabet alphabet : mAlphabet) {
            if (alphabet.isInside(y)) {
                if (mSelected == null || mSelected != alphabet) {
                    mSelected = alphabet;
                    showTextView(mSelected.getWord());
                    invalidate();
                }
                break;
            }
        }
    }

    public void setRecyclerViewPosition() {
        AdapterItemIndex adapter;
        if (mRecyclerView.getAdapter() != null && mRecyclerView.getAdapter() instanceof AdapterItemIndex)
            adapter = (AdapterItemIndex) mRecyclerView.getAdapter();
        else
            return;

        if (mSelected == null)
            return;

        int position = adapter.getWordPosition(mSelected.getWord());
        if (position == -1) {
            mSelected = null;
            return;
        }
        if (mPosition != position) {
            mPosition = position;
            if (mRecyclerView.getLayoutManager() instanceof LinearLayoutManagerWithSmoothScroller)
                mRecyclerView.smoothScrollToPosition(position);
            else if (mRecyclerView.getLayoutManager() instanceof LinearLayoutManager)
                ((LinearLayoutManager) mRecyclerView.getLayoutManager()).scrollToPositionWithOffset(position, 0);
            else if (mRecyclerView.getLayoutManager() instanceof GridLayoutManager)
                ((GridLayoutManager) mRecyclerView.getLayoutManager()).scrollToPositionWithOffset(position, 0);
            else if (mRecyclerView.getLayoutManager() instanceof StaggeredGridLayoutManager)
                ((StaggeredGridLayoutManager) mRecyclerView.getLayoutManager()).scrollToPositionWithOffset(position, 0);
            else
                mRecyclerView.scrollToPosition(position);
        }
    }

    private void showTextView(String text) {
        mTextView.setText(text);
        mTextView.setVisibility(VISIBLE);
    }

    private void hideTextView() {
        mTextView.setVisibility(INVISIBLE);
    }

    private class Alphabet {
        private String word;
        private float y;
        private float height;

        public Alphabet(String word) {
            this.word = word;
            this.y = -1;
        }

        public boolean isSet() {
            return this.y != -1;
        }

        public void setPosition(int start, int end) {
            this.y = start;
            this.height = end;
        }

        public boolean isInside(float touch) {
            return touch >= this.y && touch < this.height;
        }

        public String getWord() {
            return word;
        }
    }

    public interface AdapterItemIndex {
        int getWordPosition(String word);
    }
}
