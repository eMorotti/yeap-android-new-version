package it.mozart.yeap.ui.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;

import it.mozart.yeap.R;

public class PulseImageView extends AutoRefreshImageView {

    private Paint mPulsePaint;

    private CircleInfo[] mCircleOffsets;
    private float mEndOffset;

    // Parameters
    private float mPropStrokeWidth = 4;
    private float mPropStartOffset = 0;
    private float mPropSpacing = 0;
    private float mPropStartAlpha = 1;
    private int mPropNumCircles = 3;
    private int mPropColor = Color.RED;

    // System

    public PulseImageView(Context context) {
        super(context);
        initialize(null);
    }

    public PulseImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(attrs);
    }

    public PulseImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        for (CircleInfo circleInfo : mCircleOffsets) {
//            // Controllo che l'impulso sia visibile prima di disegnarlo
//            if (circleInfo.getOffset() <= mPropStartOffset || circleInfo.isClear()) {
//                // Ignoro solo se non è un pulse infinito
//                if (!mPropEndless || (mPropEndless && !mPulse))
//                    continue;
//            }
//            // Calcolo il colore dell'impulso basandomi sull'offset dello stesso
//            mPulsePaint.setColor(Color.argb((int) toRange(circleInfo.getOffset(), 0, mEndOffset, mPropStartAlpha * 255f, 0), Color.red(mPropColor), Color.green(mPropColor), Color.blue(mPropColor)));
//            canvas.drawCircle(canvas.getWidth() / 2, canvas.getHeight() / 2, canvas.getWidth() / 2 + circleInfo.getOffset() + mPropStartOffset, mPulsePaint);
            mPulsePaint.setColor(Color.argb((int) toRange(circleInfo.getOffset(), 0, mEndOffset, mPropStartAlpha * 255f, 0), Color.red(mPropColor), Color.green(mPropColor), Color.blue(mPropColor)));
            canvas.drawCircle(canvas.getWidth() / 2, canvas.getHeight() / 2, canvas.getWidth() / 2 + mPropStartOffset + circleInfo.getOffset(), mPulsePaint);
        }

//        mPulsePaint.setColor(Color.argb((int) toRange(mPropStartOffset + mPropStrokeWidth + mPropSpacing, 0, mEndOffset, mPropStartAlpha * 255f, 0), Color.red(mPropColor), Color.green(mPropColor), Color.blue(mPropColor)));
//        canvas.drawCircle(canvas.getWidth() / 2, canvas.getHeight() / 2, canvas.getWidth() / 2 + mPropStartOffset + mPropStrokeWidth + mPropSpacing, mPulsePaint);
//
//        mPulsePaint.setColor(Color.argb((int) toRange(mPropStartOffset + mPropStrokeWidth * 2 + mPropSpacing * 2, 0, mEndOffset, mPropStartAlpha * 255f, 0), Color.red(mPropColor), Color.green(mPropColor), Color.blue(mPropColor)));
//        canvas.drawCircle(canvas.getWidth() / 2, canvas.getHeight() / 2, canvas.getWidth() / 2 + mPropStartOffset + mPropStrokeWidth * 2 + mPropSpacing * 2, mPulsePaint);
        super.onDraw(canvas);
    }

    // Inizializzazione interna
    private void initialize(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.PulseView, 0, 0);
            // Larghezza impulso
            mPropStrokeWidth = a.getDimension(R.styleable.PulseView_pv_stroke_width, mPropStrokeWidth);
            // Spazio tra impulsi
            mPropSpacing = a.getDimension(R.styleable.PulseView_pv_spacing, mPropSpacing);
            // Distanziamento dall'immagine (non funziona bene) SCONSIGLIATO
            mPropStartOffset = a.getDimension(R.styleable.PulseView_pv_offset, mPropStartOffset);
            // Valore di opacità iniziale per il calcolo del colore degli impulsi
            mPropStartAlpha = a.getFloat(R.styleable.PulseView_pv_start_alpha, mPropStartAlpha);
            // Numero impulsi
            mPropNumCircles = a.getInt(R.styleable.PulseView_pv_num_circles, mPropNumCircles);
            // Colore degli impulsi (non usare colori con trasparenza, ma usare l'attributo corrispondente)
            mPropColor = a.getColor(R.styleable.PulseView_pv_color, mPropColor);
            a.recycle();
        }
        initVariables();
        setIgnoreBind(true);

        mPulsePaint = new Paint();
        mPulsePaint.setStyle(Paint.Style.STROKE);
        mPulsePaint.setStrokeWidth(mPropStrokeWidth);
        mPulsePaint.setColor(mPropColor);
        mPulsePaint.setAntiAlias(true);
    }

    // Inizializzazione variabili interna
    private void initVariables() {
        mEndOffset = mPropStrokeWidth * mPropNumCircles - mPropStrokeWidth / 2 + mPropSpacing * (mPropNumCircles - 1);
        mCircleOffsets = new CircleInfo[mPropNumCircles];
        for (int i = 0; i < mPropNumCircles; i++) {
            mCircleOffsets[i] = new CircleInfo(mPropStrokeWidth * i + mPropSpacing * i);
        }
    }

    // Modifica il colore
    public void setColor(int color) {
        mPropColor = color;
        invalidate();
    }

    private float toRange(float value, float oldMin, float oldMax, float newMin, float newMax) {
        return (((value - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin;
    }

    private class CircleInfo {

        // Distanza dall'immagine
        private float offset;

        CircleInfo(float offset) {
            this.offset = offset;
        }

        public float getOffset() {
            return offset;
        }

        public void setOffset(float offset) {
            this.offset = offset;
        }
    }
}
