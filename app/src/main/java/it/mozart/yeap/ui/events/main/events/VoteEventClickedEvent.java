package it.mozart.yeap.ui.events.main.events;

public class VoteEventClickedEvent {

    private String eventId;
    private int type;

    public VoteEventClickedEvent(int type) {
        this.type = type;
    }

    public VoteEventClickedEvent(String eventId, int type) {
        this.eventId = eventId;
        this.type = type;
    }

    public String getEventId() {
        return eventId;
    }

    public int getType() {
        return type;
    }
}
