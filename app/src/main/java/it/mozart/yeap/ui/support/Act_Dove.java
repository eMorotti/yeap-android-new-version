package it.mozart.yeap.ui.support;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.jakewharton.rxbinding.widget.RxTextView;
import com.jakewharton.rxbinding.widget.TextViewTextChangeEvent;
import com.lapism.searchview.SearchAdapter;
import com.lapism.searchview.SearchItem;
import com.lapism.searchview.SearchView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.api.models.APIError;
import it.mozart.yeap.api.models.SondaggioVoceApiModel;
import it.mozart.yeap.api.services.PollApi;
import it.mozart.yeap.common.DoveInfo;
import it.mozart.yeap.events.ApiCallResultEvent;
import it.mozart.yeap.helpers.AndroidPermissionHelper;
import it.mozart.yeap.realm.PollItem;
import it.mozart.yeap.realm.WhereHistory;
import it.mozart.yeap.realm.dao.WhereHistoryDao;
import it.mozart.yeap.services.realmJob.AddItemToWhereHistoryJob;
import it.mozart.yeap.ui.base.BaseActivityWithSyncService;
import it.mozart.yeap.utility.APIErrorUtils;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.Utils;
import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class Act_Dove extends BaseActivityWithSyncService implements OnMapReadyCallback, LocationListener {

    @BindView(R.id.searchview)
    SearchView mSearchView;
    @BindView(R.id.conferma)
    Button mConferma;

    @OnClick(R.id.conferma)
    void confermaClicked() {
        if (mFromPoll) {
            crea();
        } else {
            App.jobManager().addJobInBackground(new AddItemToWhereHistoryJob(mModel));
            Intent intent = new Intent();
            intent.putExtra(Constants.INTENT_DOVE, Parcels.wrap(mModel));
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    // SUPPORT

    @Inject
    Realm realm;
    @Inject
    PollApi pollApi;

    private ReactiveLocationProvider mLocationProvider;
    private GoogleApiClient mGoogleApiClient;
    private GoogleMap mMap;
    private SearchAdapter mAdapter;
    private WhereHistoryDao mHistoryDao;
    private DoveInfo mModel;
    private String mQuery;
    private String mTitle;
    private String mSubtitle;
    private String mEventId;
    private String mPollId;
    private boolean mIgnoreChange;
    private boolean mFromPoll;

    private static final String PARAM_EVENT_ID = "paramEventId";
    private static final String PARAM_POLL_ID = "paramPollId";
    private static final String PARAM_TITLE = "paramTitle";
    private static final String PARAM_SUBTITLE = "paramSubtitle";
    private static final String PARAM_DOVE = "paramDove";
    private static final String PARAM_FROM_POLL = "paramFromPoll";

    private static final int PERMISSION_LOCATION = 1001;

    // SYSTEM

    public static Intent newIntent(String title, String subtitle, DoveInfo doveInfo) {
        Intent intent = new Intent(App.getContext(), Act_Dove.class);
        intent.putExtra(PARAM_TITLE, title);
        intent.putExtra(PARAM_SUBTITLE, subtitle);
        if (doveInfo != null)
            intent.putExtra(PARAM_DOVE, Parcels.wrap(doveInfo));
        intent.putExtra(PARAM_FROM_POLL, false);
        return intent;
    }

    public static Intent newIntentFromPoll(String eventId, String pollId, String title, String subtitle) {
        Intent intent = new Intent(App.getContext(), Act_Dove.class);
        intent.putExtra(PARAM_EVENT_ID, eventId);
        intent.putExtra(PARAM_POLL_ID, pollId);
        intent.putExtra(PARAM_TITLE, title);
        intent.putExtra(PARAM_SUBTITLE, subtitle);
        intent.putExtra(PARAM_FROM_POLL, true);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUseBus(true);
        setToolbarTitle(mTitle);
        setToolbarSubtitle(mSubtitle);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_dove;
    }

    @Override
    protected void initVariables() {
        mIgnoreChange = false;
    }

    @Override
    protected void loadParameters(Bundle extras) {
        mEventId = extras.getString(PARAM_EVENT_ID);
        mPollId = extras.getString(PARAM_POLL_ID);
        mTitle = extras.getString(PARAM_TITLE, "");
        mSubtitle = extras.getString(PARAM_SUBTITLE, null);
        if (extras.containsKey(PARAM_DOVE)) {
            mModel = Parcels.unwrap(extras.getParcelable(PARAM_DOVE));
        }
        mFromPoll = extras.getBoolean(PARAM_FROM_POLL);
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    protected void initialize() {
        if (mModel == null) {
            mModel = new DoveInfo();
        }
        mHistoryDao = new WhereHistoryDao(realm);

        // Init Google e Location
        mLocationProvider = new ReactiveLocationProvider(this);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                })
                .addApi(Places.GEO_DATA_API)
                .build();

        // Init searchView
        mSearchView.getEditText().clearFocus();
        mAdapter = new SearchAdapter(this);
        mAdapter.setOnItemClickListener(new SearchAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                mConferma.setVisibility(View.VISIBLE);

                SearchItem item = mAdapter.getResultList().get(position);

                // Modifico il testo
                mIgnoreChange = true;
                mSearchView.setText(item.get_text() + " " + (item.getSecondaryText() != null ? item.getSecondaryText() : ""));
                mModel.setDove(item.getSecondaryText() == null ? mSearchView.getEditText().getText().toString() : item.get_text().toString());
                mModel.setDoveExtraText(item.getSecondaryText() == null ? null : item.getSecondaryText().toString());
                mModel.setPlaceId(item.getPlaceId());

                // Se è presente il place ID recupero il luogo
                if (item.getPlaceId() != null) {
                    PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, item.getPlaceId());
                    placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(@NonNull PlaceBuffer places) {
                            if (!places.getStatus().isSuccess()) {
                                places.release();
                                return;
                            }
                            Place place = places.get(0);
                            mModel.setLat(place.getLatLng().latitude);
                            mModel.setLng(place.getLatLng().longitude);

                            mMap.clear();
                            mMap.addMarker(new MarkerOptions()
                                    .position(place.getLatLng())
                                    .anchor(0.35f, 0.9f)
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.places_pin_big))
                                    .title(place.getName().toString()));

                            // Animo lo spostamento
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 17));
                        }
                    });
                }
                mSearchView.close(true);
            }
        });
        mSearchView.setAdapter(mAdapter);
        loadHistoryItems();
        // Gestisco il debounce sulla modifica del testo della searchView
        registerSubscription(RxTextView.textChangeEvents(mSearchView.getEditText())
                .debounce(300, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getSearchObserver()));

        // Init View
        mConferma.setVisibility(View.GONE);
    }

    @SuppressWarnings("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        final boolean locationEnabled = initializeLocation();

        if (mModel.getDove() != null && mModel.getDove().length() != 0) {
            mSearchView.getEditText().setText(mModel.getDove());
        }
        if (Utils.isNetworkAvailable(this)) {
            if (mModel.getPlaceId() != null && mModel.getPlaceId().length() != 0) {
                mLocationProvider.getPlaceById(mModel.getPlaceId())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<PlaceBuffer>() {
                            @Override
                            public void call(PlaceBuffer places) {
                                if (places.getCount() == 0)
                                    return;
                                Place place = places.get(0);
                                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 17));
                                mMap.addMarker(new MarkerOptions()
                                        .position(place.getLatLng())
                                        .anchor(0.35f, 0.9f)
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.places_pin_big))
                                        .title(place.getName().toString()));
                            }
                        }, new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                mModel.setPlaceId(null);

                                if (locationEnabled) {
                                    LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
                                    locationManager.requestSingleUpdate(new Criteria(), Act_Dove.this, null);
                                }
                            }
                        });
            } else if (mModel.getDove() != null && mModel.getDove().length() != 0) {
                mLocationProvider.getGeocodeObservable(mModel.getDove(), 1)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<List<Address>>() {
                            @Override
                            public void call(List<Address> addresses) {
                                if (addresses.size() == 0)
                                    return;
                                Address address = addresses.get(0);
                                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(address.getLatitude(), address.getLongitude()), 17));
                            }
                        }, new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {

                            }
                        });
            } else if (locationEnabled) {
                LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
                locationManager.requestSingleUpdate(new Criteria(), this, null);
            }
        } else if (locationEnabled) {
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            locationManager.requestSingleUpdate(new Criteria(), this, null);
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_LOCATION: {
                // Reinizializzo la location se viene dato il permesso alla localizzazione
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //noinspection MissingPermission
                    mMap.setMyLocationEnabled(true);
                    LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
                    //noinspection MissingPermission
                    locationManager.requestSingleUpdate(new Criteria(), this, null);
                }
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null && (mModel == null || mModel.getPlaceId() == null || mModel.getPlaceId().length() == 0))
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 17));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ApiCallResultEvent event) {
        if (event.getRequestId().equals(getRequestId())) {
            cancelTimer();
            finish();
            overridePendingTransition(0, R.anim.slide_out_bottom_slow);
        }
    }

    // FUNCTIONS

    /**
     * Richiede permessi per la localizzazione e mostra la posizione corrente
     */
    @SuppressLint("MissingPermission")
    private boolean initializeLocation() {
        // Controllo se ha la localizzazione come feature
        PackageManager packageManager = getPackageManager();
        if (!packageManager.hasSystemFeature(PackageManager.FEATURE_LOCATION))
            return false;

        // Controllo se ha dato il permesso alla localizzazione
        if (!AndroidPermissionHelper.isPermissionGranted(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_LOCATION);
            return false;
        }

        //noinspection MissingPermission
        mMap.setMyLocationEnabled(true);
        return true;
    }

    /**
     * Osservabile sul cambiamento di testo della searchView
     *
     * @return observabile
     */
    private Observer<TextViewTextChangeEvent> getSearchObserver() {
        return new Observer<TextViewTextChangeEvent>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(TextViewTextChangeEvent textViewTextChangeEvent) {
                // Ignoro se il testo è uguale
                if (mQuery != null && mQuery.equalsIgnoreCase(textViewTextChangeEvent.text().toString()))
                    return;
                updateSearchItems(textViewTextChangeEvent.text().toString());
            }
        };
    }

    /**
     * Gestisce l'esecuzione della chiamata per il recupero degli autocompletamenti sul mainThread
     *
     * @param query testo della ricerca
     */
    private void updateSearchItems(final String query) {
        // Nascondo la conferma se il testo è cambiato
        if (mQuery == null || !mQuery.equalsIgnoreCase(query)) {
            if (!mIgnoreChange)
                mConferma.setVisibility(View.GONE);
            mIgnoreChange = false;
        }

        mQuery = query;
        // Sono su mainThread
        if (Utils.isCurrentlyOnMainThread()) {
            if (query == null || query.isEmpty()) {
                showSearchItems(null);
            } else {
                showSearchItems(query);
            }
        } else {
            // Non sono su mainThread
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    if (query == null || query.isEmpty()) {
                        showSearchItems(null);
                    } else {
                        showSearchItems(query);
                    }
                }
            });
        }
    }

    /**
     * Gestisce il recupero e la visualizzazione della lista degli autocompletamenti
     *
     * @param query testo della ricerca
     */
    private void showSearchItems(final String query) {
        if (query == null) {
            // Resetto la lista
            mAdapter.setSuggestionsList(new ArrayList<SearchItem>());
            mSearchView.submitQuery();
        } else {
            // Recupero la lista da Google places
            registerSubscription(ReactiveLocationProvider.fromPendingResult(Places.GeoDataApi.getAutocompletePredictions(mGoogleApiClient, query, null, null))
                    .subscribe(new Action1<AutocompletePredictionBuffer>() {
                        @Override
                        public void call(AutocompletePredictionBuffer autocompletePredictions) {
                            List<SearchItem> items = new ArrayList<>();
                            for (AutocompletePrediction prediction : autocompletePredictions) {
                                // Trasformo in oggetti usabili dall'adapter
                                items.add(new SearchItem(prediction.getFullText(null), prediction.getPrimaryText(null), prediction.getSecondaryText(null), prediction.getPlaceId()));
                            }
                            autocompletePredictions.release();
                            mAdapter.setSuggestionsList(items);
                            mSearchView.submitQuery();
                        }
                    }));
        }
    }

    /**
     * Carica la lista di elementi dallo storico
     */
    private void loadHistoryItems() {
        List<SearchItem> items = new ArrayList<>();
        for (WhereHistory historyItem : mHistoryDao.getHistory()) {
            items.add(new SearchItem(null, historyItem.getText(), historyItem.getSecondaryText(), historyItem.getPlaceId()));
        }
        mAdapter.setHistoryList(items);
        mSearchView.setText("");
    }

    /**
     * Creo la voce sondaggio
     */
    private void crea() {
        getDynamicView().showLoading();
        setRequestId(UUID.randomUUID().toString());
        registerSubscription(pollApi.createPollItem(getRequestId(), mEventId, mPollId, new SondaggioVoceApiModel(mModel))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<PollItem>() {
                    @Override
                    public void call(PollItem voce) {
                        startTimer();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        getDynamicView().hide();

                        APIError error = APIErrorUtils.parseError(Act_Dove.this, throwable);
                        Toast.makeText(Act_Dove.this, error.message(), Toast.LENGTH_SHORT).show();
                    }
                })
        );
    }
}
