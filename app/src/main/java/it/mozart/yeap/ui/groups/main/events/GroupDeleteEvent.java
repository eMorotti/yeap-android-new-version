package it.mozart.yeap.ui.groups.main.events;

public class GroupDeleteEvent {

    private boolean success;

    public GroupDeleteEvent(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }
}
