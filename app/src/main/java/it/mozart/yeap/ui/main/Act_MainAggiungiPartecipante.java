package it.mozart.yeap.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.api.models.APIError;
import it.mozart.yeap.api.services.EventApi;
import it.mozart.yeap.api.services.GroupApi;
import it.mozart.yeap.events.ApiCallResultEvent;
import it.mozart.yeap.helpers.CreationModelHelper;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.GroupDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.ui.base.BaseActivityWithSyncService;
import it.mozart.yeap.ui.events.main.events.ShowPopupInvitedEvent;
import it.mozart.yeap.ui.groups.main.events.ShowPopupInvitedGroup;
import it.mozart.yeap.utility.APIErrorUtils;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.Prefs;
import it.mozart.yeap.utility.Utils;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class Act_MainAggiungiPartecipante extends BaseActivityWithSyncService {

    @BindView(R.id.confirm)
    Button mConfirm;

    @OnClick(R.id.confirm)
    void confirmClicked() {
        if (mFragment == null)
            return;

        if (mFragment.getCheckList().size() == 0 && mFragment.getCheckInvitedList().size() == 0)
            return;

        mFailedIds.clear();
        new MaterialDialog.Builder(this)
                .title(mIsAttivita ? getString(R.string.main_evento_aggiungi_partecipanti_title) : getString(R.string.main_gruppo_aggiungi_partecipanti_title))
                .content(mIsAttivita ? getString(R.string.main_evento_aggiungi_partecipanti_messaggio) : getString(R.string.main_gruppo_aggiungi_partecipanti_messaggio))
                .positiveText(getString(R.string.general_ok))
                .negativeText(getString(R.string.general_annulla))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        if (!Utils.isNetworkAvailable(Act_MainAggiungiPartecipante.this)) {
                            Toast.makeText(Act_MainAggiungiPartecipante.this, getString(R.string.general_error_internet), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        getDynamicView().showLoading();

                        // Concateno le chiamate per aggiungere partecipanti e invitati
                        registerSubscription(Observable.concat(Observable.from(mFragment.getCheckList())
                                .subscribeOn(Schedulers.newThread())
                                .concatMap(new Func1<String, Observable<Object>>() {
                                    @Override
                                    public Observable<Object> call(final String s) {
                                        setRequestId(UUID.randomUUID().toString());
                                        if (mIsAttivita) {
                                            return addPartecipanteAttivita(s, true);
                                        } else {
                                            return addPartecipanteGruppo(s, true);
                                        }
                                    }
                                }), Observable.from(mFragment.getCheckInvitedList())
                                .subscribeOn(Schedulers.newThread())
                                .concatMap(new Func1<String, Observable<Object>>() {
                                    @Override
                                    public Observable<Object> call(final String s) {
                                        setRequestId(UUID.randomUUID().toString());
                                        if (mIsAttivita) {
                                            return addPartecipanteAttivita(s, false);
                                        } else {
                                            return addPartecipanteGruppo(s, false);
                                        }
                                    }
                                }))
                                .toList()
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new Action1<List<Object>>() {
                                    @Override
                                    public void call(List<Object> objs) {
                                        startTimer();
                                    }
                                }, new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable throwable) {
                                        getDynamicView().hide();

                                        APIError error = APIErrorUtils.parseError(Act_MainAggiungiPartecipante.this, throwable);
                                        Toast.makeText(Act_MainAggiungiPartecipante.this, error.message(), Toast.LENGTH_SHORT).show();
                                    }
                                }));
                    }
                }).show();
    }

    // SUPPORT

    @Inject
    Realm realm;
    @Inject
    EventApi eventApi;
    @Inject
    GroupApi groupApi;
    @Inject
    CreationModelHelper creationModelHelper;

    private Frg_MainPartecipanti mFragment;
    private List<String> mFailedIds;
    private List<String> mFailedPhones;
    private String mId;
    private boolean mIsAttivita;

    private static final String PARAM_ID = "paramId";
    private static final String PARAM_IS_ATTIVITA = "paramIsAttivita";

    // SYSTEM

    public static Intent newIntent(String id, boolean isAttivita) {
        Intent intent = new Intent(App.getContext(), Act_MainAggiungiPartecipante.class);
        intent.putExtra(PARAM_ID, id);
        intent.putExtra(PARAM_IS_ATTIVITA, isAttivita);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUseBus(true);

        if (mToolbar != null) {
            mToolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_main_aggiungi_partecipante;
    }

    @Override
    protected void initVariables() {
        mFailedIds = new ArrayList<>();
        mFailedPhones = new ArrayList<>();
    }

    @Override
    protected void loadParameters(Bundle extras) {
        mId = extras.getString(PARAM_ID);
        mIsAttivita = extras.getBoolean(PARAM_IS_ATTIVITA);
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    public void onBackPressed() {
        Utils.hideKeyboard(this);
        super.onBackPressed();
    }

    @Override
    protected void initialize() {
        if (mIsAttivita) {
            creationModelHelper.setEvent(new EventDao(realm).getEvent(mId));
            setToolbarTitle(creationModelHelper.getEvent().getTitle());
        } else {
            creationModelHelper.setGroup(new GroupDao(realm).getGroup(mId));
            setToolbarTitle(creationModelHelper.getGroup().getTitle());
        }
        setToolbarSubtitle(getString(R.string.main_aggiungi_partecipanti_title));

        mFragment = Frg_MainPartecipanti.newInstanceFromMain(mIsAttivita);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content, mFragment)
                .commit();

        mConfirm.setVisibility(View.GONE);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ApiCallResultEvent event) {
        if (event.getRequestId().equals(getRequestId())) {
            cancelTimer();
            getDynamicView().hide();

            UserDao userDao = new UserDao(realm);

            String message = "";
            if (mFailedIds.size() != 0 || mFailedPhones.size() != 0) {
                for (String id : mFailedIds) {
                    User utente = userDao.getUser(id);
                    if (utente != null) {
                        if (!message.isEmpty()) {
                            message += ", ";
                        }
                        message += utente.getChatName();
                    }
                }
                for (String phone : mFailedPhones) {
                    User utente = userDao.getUserLocal(phone);
                    if (utente != null) {
                        if (!message.isEmpty()) {
                            message += ", ";
                        }
                        message += utente.getChatName();
                    }

                    mFragment.getCheckInvitedList().remove(phone);
                }
                if (mFailedIds.size() + mFailedPhones.size() == 1) {
                    message += String.format(" %s", getString(R.string.main_aggiungi_partecipanti_non_aggiunto));
                } else {
                    message += String.format(" %s", getString(R.string.main_aggiungi_partecipanti_non_aggiunti));
                }
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }

            String values = "";
            for (String phone : mFragment.getCheckInvitedList()) {
                User user = userDao.getUserLocal(phone);
                if(user != null) {
                    if (values.isEmpty()) {
                        values = user.getPhone();
                    } else {
                        values += String.format(";%s", user.getPhone());
                    }
                }
            }
            if (!values.isEmpty()) {
                if (mIsAttivita) {
                    Prefs.putString(Constants.SHOW_INVITE_POPUP_EVENTO + mId, values);
                    EventBus.getDefault().postSticky(new ShowPopupInvitedEvent());
                } else {
                    Prefs.putString(Constants.SHOW_INVITE_POPUP_GRUPPO + mId, values);
                    EventBus.getDefault().postSticky(new ShowPopupInvitedGroup());
                }
            }

            finish();
        }
    }

    // FUNCTIONS

    /**
     * Aggiunto partecipante o invitato
     */
    public void partecipanteAggiunto() {
        if (mFragment == null)
            return;

        if (mFragment.getCheckList().size() == 1 || mFragment.getCheckInvitedList().size() == 1) {
            mConfirm.setVisibility(View.VISIBLE);
            mConfirm.setText(String.format(getString(R.string.main_aggiungi_partecipanti_invita), mFragment.getCheckList().size() + mFragment.getCheckInvitedList().size()));
        } else if (mFragment.getCheckList().size() == 0 && mFragment.getCheckInvitedList().size() == 0) {
            mConfirm.setVisibility(View.GONE);
        }
    }

    /**
     * Rimosso partecipante o invitato
     */
    public void partecipanteRimosso() {
        if (mFragment == null)
            return;

        if (mFragment.getCheckList().size() == 1 || mFragment.getCheckInvitedList().size() == 1) {
            mConfirm.setVisibility(View.VISIBLE);
            mConfirm.setText(String.format(getString(R.string.main_aggiungi_partecipanti_invita), mFragment.getCheckList().size() + mFragment.getCheckInvitedList().size()));
        } else if (mFragment.getCheckList().size() == 0 && mFragment.getCheckInvitedList().size() == 0) {
            mConfirm.setVisibility(View.GONE);
        }
    }

    /**
     * Chiamata aggiunta partecipante in attività
     *
     * @param value id utente
     * @return observable
     */
    private Observable<Object> addPartecipanteAttivita(final String value, boolean isId) {
        if (isId) {
            return eventApi.addPartecipant(getRequestId(), mId, value, null).onErrorReturn(new Func1<Throwable, Object>() {
                @Override
                public Object call(Throwable throwable) {
                    mFailedIds.add(value);
                    return throwable;
                }
            });
        } else {
            return eventApi.addPartecipant(getRequestId(), mId, null, value).onErrorReturn(new Func1<Throwable, Object>() {
                @Override
                public Object call(Throwable throwable) {
                    mFailedPhones.add(value);
                    return throwable;
                }
            });
        }
    }

    /**
     * Chiamata aggiunta partecipante in gruppo
     *
     * @param value id utente
     * @return observable
     */
    private Observable<Object> addPartecipanteGruppo(final String value, boolean isId) {
        if (isId) {
            return groupApi.addPartecipant(getRequestId(), mId, value, null).onErrorReturn(new Func1<Throwable, Object>() {
                @Override
                public Object call(Throwable throwable) {
                    mFailedIds.add(value);
                    return throwable;
                }
            });
        } else {
            return groupApi.addPartecipant(getRequestId(), mId, null, value).onErrorReturn(new Func1<Throwable, Object>() {
                @Override
                public Object call(Throwable throwable) {
                    mFailedPhones.add(value);
                    return throwable;
                }
            });
        }
    }
}
