package it.mozart.yeap.ui.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import org.greenrobot.eventbus.Subscribe;

import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.events.ImageSelectedEvent;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.ui.base.BaseActivityWithSyncService;
import it.mozart.yeap.ui.support.Act_ImageDetail;
import it.mozart.yeap.utility.Utils;

public class Act_Profilo extends BaseActivityWithSyncService {

    // SUPPORT

    private String mId;

    private static final String PARAM_ID = "paramId";

    // SYSTEM

    public static Intent newIntent(String utenteId) {
        Intent intent = new Intent(App.getContext(), Act_Profilo.class);
        intent.putExtra(PARAM_ID, utenteId);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUseBus(true);

        Fragment fragment;
        if (AccountProvider.getInstance().getAccountId().equals(mId)) {
            setToolbarTitle(getString(R.string.profilo_title_you));
            setToolbarSubtitle(getString(R.string.profilo_subtitle_edit));
//            if (getSupportActionBar() != null)
//                getSupportActionBar().setTitle(getString(R.string.app_profilo));
            fragment = new Frg_Profilo();
        } else {
            if (getSupportActionBar() != null)
                getSupportActionBar().setTitle(getString(R.string.app_profilo_altrui));
            fragment = Frg_ProfiloAltrui.newInstance(mId);
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.content, fragment).commit();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_profilo;
    }

    @Override
    protected void initVariables() {

    }

    @Override
    protected void loadParameters(Bundle extras) {
        mId = extras.getString(PARAM_ID);
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {

    }

    @Override
    protected void saveInfos(Bundle outState) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (AccountProvider.getInstance().getAccountId().equals(mId)) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_my_profile, menu);
            return true;
        } else {
            return super.onCreateOptionsMenu(menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Frg_Profilo profilo = (Frg_Profilo) getSupportFragmentManager().findFragmentById(R.id.content);
        switch (item.getItemId()) {
            case R.id.menu_delete:
                profilo.menuDeleteAccount();
                break;
            case R.id.menu_logout:
                profilo.menuLogout();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void initialize() {

    }


    @Subscribe
    public void onEvent(ImageSelectedEvent event) {
        Intent intent = Act_ImageDetail.newIntentSharedAnimation(event.getPath(), event.getView().getWidth() / 2, R.drawable.avatar_utente);
        if (Utils.isLollipop()) {
            Pair<View, String> p1 = Pair.create(event.getView(), getString(R.string.transition_image));
            //noinspection unchecked
            ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(this, p1);
            startActivity(intent, optionsCompat.toBundle());
        } else {
            startActivity(intent);
        }
    }
}
