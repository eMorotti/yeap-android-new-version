package it.mozart.yeap.ui.home.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import org.greenrobot.eventbus.EventBus;

import it.mozart.yeap.R;
import it.mozart.yeap.events.ImageSelectedEvent;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Group;
import it.mozart.yeap.ui.views.AnimatedTextView;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.DateUtils;
import it.mozart.yeap.utility.EasySpan;

public class GroupLayout extends FrameLayout {

    private ImageView mAvatar;
    private TextView mTitle;
    private TextView mMessageCreated;
    private TextView mMessage;
    private AnimatedTextView mNumNewMessages;

    public GroupLayout(Context context) {
        super(context);
        init();
    }

    public GroupLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GroupLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @SuppressWarnings("unused")
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public GroupLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.item_group, this);
        mAvatar = findViewById(R.id.avatar);
        mTitle = findViewById(R.id.title);
        mMessageCreated = findViewById(R.id.message_created);
        mMessage = findViewById(R.id.message);

        mNumNewMessages = findViewById(R.id.new_messages);
        mNumNewMessages.setTextSize(8);
        mNumNewMessages.setTextColor(Color.WHITE);
    }

    /**
     * Esegui il bind tra i dati del gruppo e le view
     *
     * @param group   gruppo
     * @param animate anima l'aggiornamento
     */
    public void bind(final Group group, boolean animate) {
        if (group != null) {
            // Titolo
            mTitle.setText(group.getTitle());

            // Ultimo messaggio
            manageLastMessage(group);

            // Avatar
            mAvatar.setOnClickListener(null);
            Glide.with(getContext()).load(group.getAvatarThumbUrl()).asBitmap().placeholder(R.drawable.avatar_gruppo).centerCrop().into(new BitmapImageViewTarget(mAvatar) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    mAvatar.setImageDrawable(circularBitmapDrawable);
                }
            });
            mAvatar.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    EventBus.getDefault().post(new ImageSelectedEvent(group.getAvatarLargeUrl(), mAvatar, true, false));
                }
            });

            // Nuovi messaggi
            if (group.getNumNewMessages() != 0 || group.getNumUserUpdates() != 0) {
                mNumNewMessages.setVisibility(VISIBLE);
                mNumNewMessages.setValue(group.getNumNewMessages() + group.getNumUserUpdates());
            } else {
                mNumNewMessages.setVisibility(GONE);
            }
        }
    }

    /**
     * Gestisco la visulalizzazione dell'ultimo messaggio presente nella chat del gruppo
     *
     * @param group gruppo
     */
    private void manageLastMessage(Group group) {
        mMessageCreated.setVisibility(VISIBLE);
        if (group.getMessage() != null && group.getMessage().isValid() && group.getMessage().getCreator() != null) {
            // Ultimo messaggio utente
            String who;
            if (group.getMessage().getCreator().getUuid().equals(AccountProvider.getInstance().getAccountId())) {
                who = getContext().getString(R.string.general_tu);
            } else {
                who = group.getMessage().getCreator().getChatName();
            }
            // Ultimo messaggio data
            mMessageCreated.setText(who + " • " + DateUtils.smartFormatDateOnly(getContext(), group.getMessage().getCreatedAt(), "dd MMM"));
            // Ultimo messaggio
            if (group.getMessage().getMediaUrl() != null || group.getMessage().getMediaLocalUrl() != null) {
                Drawable drawable1 = DrawableCompat.wrap(ContextCompat.getDrawable(getContext(), R.drawable.ic_photo_white_24dp));
                DrawableCompat.setTint(drawable1, ContextCompat.getColor(getContext(), R.color.textGray));
                drawable1.setBounds(0, 0, mMessage.getLineHeight(), mMessage.getLineHeight());
                ImageSpan imageSpan1 = new ImageSpan(drawable1, ImageSpan.ALIGN_BOTTOM);

                EasySpan easySpan = new EasySpan.Builder()
                        .appendSpans("---", imageSpan1)
                        .appendText(" ")
                        .appendText(group.getMessage().getText())
                        .build();

                Spanned spanned = easySpan.getSpannedText();
                if (group.getMessage().getFormattedMessageWithoutCreator(getContext()).length() == 0) {
                    spanned = (Spanned) TextUtils.concat(spanned, getContext().getString(R.string.general_foto));
                } else {
                    spanned = (Spanned) TextUtils.concat(spanned, group.getMessage().getFormattedMessage(getContext()));
                }

                mMessage.setText(spanned);
            } else {
                mMessage.setText(group.getMessage().getFormattedMessageWithoutCreator(getContext()));
            }
        } else if (group.getMessage() != null && group.getMessage().getType() == Constants.MESSAGGIO_TYPE_SYSTEM) {
            String who;
            if (group.getMessage().getCreator().getUuid().equals(AccountProvider.getInstance().getAccountId())) {
                who = getContext().getString(R.string.general_tu);
            } else {
                who = group.getMessage().getCreator().getChatName();
            }
            mMessageCreated.setText(who + " • " + DateUtils.smartFormatDateOnly(getContext(), group.getMessage().getCreatedAt(), "dd MMM"));
            mMessage.setText(group.getMessage().getFormattedMessageWithoutCreator(getContext()));
        } else {
            mMessageCreated.setText(null);
            mMessageCreated.setVisibility(GONE);
            mMessage.setText(getContext().getString(R.string.general_error_last_message_not_found));
        }
    }
}
