package it.mozart.yeap.ui.main;

public interface MainCreationControl {

    boolean canGoOn();
}
