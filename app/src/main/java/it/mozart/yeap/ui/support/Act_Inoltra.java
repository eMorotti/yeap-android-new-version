package it.mozart.yeap.ui.support;

import android.app.Activity;
import android.app.TaskStackBuilder;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.common.MediaInfo;
import it.mozart.yeap.common.MessaggioInfo;
import it.mozart.yeap.events.InoltraChangeSelectedEvent;
import it.mozart.yeap.events.MessaggioInoltratoEvent;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Message;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.GroupDao;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.services.messaggi.InoltraMessaggioWithExternalImageJob;
import it.mozart.yeap.services.messaggi.MessaggiService;
import it.mozart.yeap.ui.base.BaseActivityWithSyncService;
import it.mozart.yeap.ui.events.main.Act_EventoMain;
import it.mozart.yeap.ui.groups.main.Act_GruppoMain;
import it.mozart.yeap.ui.home.Act_Home;
import it.mozart.yeap.ui.support.adapters.InoltraAdapter;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.Utils;

public class Act_Inoltra extends BaseActivityWithSyncService {

    @BindView(R.id.recyclerview)
    RecyclerView mRecyclerView;
    @BindView(R.id.list)
    TextView mList;
    @BindView(R.id.fab)
    FloatingActionButton mFab;

    @OnClick(R.id.fab)
    void fabClicked() {
        if (mIsInTransaction)
            return;

        mIsInTransaction = true;

        final String utenteId = AccountProvider.getInstance().getAccountId();
        int numMessaggi = 0;
        try {
            // Caso in cui arriva da app esterna
            if (handleSpecialIntentIfPresent(utenteId)) {
                return;
            }

            MessageDao messaggioDao = new MessageDao(realm);
            // Caso in cui arriva dall'interno dell'app
            for (Object o : mMessaggiIds.entrySet()) {
                Map.Entry pair = (Map.Entry) o;
                Message messaggio = messaggioDao.getMessage((String) pair.getKey());
                if (messaggio == null)
                    continue;

                // Crea il model per l'immagine
                MediaInfo mediaInfo = null;
                if (messaggio.getMediaLocalUrl() != null) {// TODO
//                    File file = new File(messaggio.getMediaLocalUrl());
//                    if (file.exists())
                    mediaInfo = new MediaInfo(messaggio.getMediaLocalUrl(), messaggio.getThumbData(), 0, (int) messaggio.getMediaWidth(),
                            (int) messaggio.getMediaHeight(), false);
                } else if (messaggio.getMediaUrl() != null) {
                    mediaInfo = new MediaInfo(messaggio.getMediaUrl(), messaggio.getThumbData(), 0, (int) messaggio.getMediaWidth(),
                            (int) messaggio.getMediaHeight(), true);
                }

                if (mediaInfo == null && messaggio.getText().isEmpty()) {
                    continue;
                }
                numMessaggi++;

                // Gestisco la creazione di tutti i messaggi
                for (Object obj : mAdapter.getSelected().entrySet()) {
                    Map.Entry pair2 = (Map.Entry) obj;
                    MessaggioInfo messaggioInfo;
                    if ((boolean) pair2.getValue()) {
                        // Caso evento
                        messaggioInfo = new MessaggioInfo((String) pair2.getKey(),
                                Constants.MESSAGGIO_TYPOLOGY_CHAT_EVENTO,
                                utenteId,
                                messaggio.getText(),
                                mediaInfo,
                                new Date().getTime(),
                                true,
                                false,
                                true);
                    } else {
                        // Caso gruppo
                        messaggioInfo = new MessaggioInfo((String) pair2.getKey(),
                                Constants.MESSAGGIO_TYPOLOGY_CHAT_GRUPPO,
                                utenteId,
                                messaggio.getText(),
                                mediaInfo,
                                new Date().getTime(),
                                true,
                                false,
                                true);
                    }
                    // Creo messaggio
                    MessaggiService.createMessage(Act_Inoltra.this, messaggioInfo);
                }
            }

            if (numMessaggi != 0) {
                if (mAdapter.getSelected().size() == 1) {
                    for (Map.Entry pair : mAdapter.getSelected().entrySet()) {
                        if ((boolean) pair.getValue())
                            startActivity(Act_EventoMain.newIntent((String) pair.getKey()));
                        else
                            startActivity(Act_GruppoMain.newIntent((String) pair.getKey()));
                    }
                }
                finish();
            } else {
                new MaterialDialog.Builder(Act_Inoltra.this)
                        .title(getString(R.string.system_inolta_error_title))
                        .content(getString(R.string.system_inolta_error_message))
                        .positiveText(getString(R.string.general_ok))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                finish();
                            }
                        })
                        .cancelable(false)
                        .show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            mIsInTransaction = false;
            Toast.makeText(this, getString(R.string.general_error_realm), Toast.LENGTH_SHORT).show();
        }
    }

    // SUPPORT

    @Inject
    Realm realm;

    private InoltraAdapter mAdapter;
    private HashMap<String, String> mMessaggiIds;
    private HashMap<String, Boolean> mSelected;
    private HashMap<String, String> mSelectedTitle;
    private boolean mIsInTransaction;
    private boolean mMultiImagesFinished;

    private static final String PARAM_MESSAGES = "paramMessages";

    private static final String SAVED_SELECTED = "savedSelected";
    private static final String SAVED_SELECTED_TITLE = "savedSelectedTitle";

    private static final int IMG_CODE = 564;

    // SYSTEM

    public static Intent newIntent(HashMap<String, String> messaggi) {
        Intent intent = new Intent(App.getContext(), Act_Inoltra.class);
        intent.putExtra(PARAM_MESSAGES, messaggi);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUseBus(true);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.app_inoltra));
        }

        // Controllo se è una condivisione di immagine
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();
        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if (type.startsWith("image/")) {
                mFab.setImageResource(R.drawable.ic_arrow_forward_white_24dp);
            }
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_inoltra;
    }

    @Override
    protected void initVariables() {
        mSelected = new HashMap<>();
        mSelectedTitle = new HashMap<>();
        mIsInTransaction = false;
        mMultiImagesFinished = false;
    }

    @Override
    protected void loadParameters(Bundle extras) {
        if (extras.containsKey(PARAM_MESSAGES)) {
            //noinspection unchecked
            mMessaggiIds = (HashMap<String, String>) extras.getSerializable(PARAM_MESSAGES);
        }
    }

    @Override
    protected void loadInfos(Bundle savedInstanceState) {
        //noinspection unchecked
        mSelected = (HashMap<String, Boolean>) savedInstanceState.getSerializable(SAVED_SELECTED);
        //noinspection unchecked
        mSelectedTitle = (HashMap<String, String>) savedInstanceState.getSerializable(SAVED_SELECTED_TITLE);
    }

    @Override
    protected void saveInfos(Bundle outState) {
        outState.putSerializable(SAVED_SELECTED, mAdapter.getSelected());
        outState.putSerializable(SAVED_SELECTED_TITLE, mAdapter.getSelectedTitle());
    }

    @Override
    protected void initialize() {
        mFab.hide();

        // Init Recyclerview
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new InoltraAdapter(this, new EventDao(realm).getListActivitiesWithMeInside(), new GroupDao(realm).getListGroupsWithMeInside(), mSelected, mSelectedTitle);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case IMG_CODE:
                    String imagePath = data.getStringExtra(Constants.IMAGE_PATH);
                    String caption = data.getStringExtra(Constants.CAPTION);
                    if (imagePath != null) {
                        try {
                            // Calcolo l'altezza dell'immagine nella chat
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inJustDecodeBounds = true;
                            BitmapFactory.decodeFile(new File(imagePath).getAbsolutePath(), options);

                            // Crea model immagine
                            MediaInfo mediaInfo = new MediaInfo(imagePath, 0, false);

                            mediaInfo.setWidth(options.outWidth);
                            mediaInfo.setHeight(options.outHeight);

                            for (Map.Entry pair : mAdapter.getSelected().entrySet()) {
                                MessaggioInfo messaggioInfo;
                                if ((boolean) pair.getValue()) {
                                    // Caso evento
                                    messaggioInfo = new MessaggioInfo((String) pair.getKey(),
                                            Constants.MESSAGGIO_TYPOLOGY_CHAT_EVENTO,
                                            AccountProvider.getInstance().getAccountId(),
                                            caption != null ? caption : "",
                                            mediaInfo,
                                            new Date().getTime(),
                                            true,
                                            false,
                                            true);
                                } else {
                                    // Caso gruppo
                                    messaggioInfo = new MessaggioInfo((String) pair.getKey(),
                                            Constants.MESSAGGIO_TYPOLOGY_CHAT_GRUPPO,
                                            AccountProvider.getInstance().getAccountId(),
                                            caption != null ? caption : "",
                                            mediaInfo,
                                            new Date().getTime(),
                                            true,
                                            false,
                                            true);
                                }
                                // Creo messaggio
                                MessaggiService.createMessage(Act_Inoltra.this, messaggioInfo);
                            }

                            // Se è condiviso in una sola evento/gruppo, lo apro
                            if (mAdapter.getSelected().size() == 1) {
                                Intent intent = null;
                                for (Map.Entry pair : mAdapter.getSelected().entrySet()) {
                                    if ((boolean) pair.getValue())
                                        intent = Act_EventoMain.newIntent((String) pair.getKey());
                                    else
                                        intent = Act_GruppoMain.newIntent((String) pair.getKey());
                                }
                                TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(this)
                                        .addNextIntent(Act_Home.newIntent(0))
                                        .addNextIntent(intent);

                                taskStackBuilder.startActivities();
                            } else {
                                startActivity(Act_Home.newIntent(0));
                            }
                            finish();
                        } catch (Exception ex) {
                            Toast.makeText(this, getString(R.string.general_error_realm), Toast.LENGTH_LONG).show();
                        }
                    }
                    break;
            }
        } else {
            mIsInTransaction = false;
        }
    }

    @Subscribe
    public void onEvent(InoltraChangeSelectedEvent event) {
        if (mAdapter.getSelectedTitle().size() == 0) {
            mList.setVisibility(View.GONE);
            mFab.hide();
        } else {
            mList.setVisibility(View.VISIBLE);
            mFab.show();
        }
        String selected = "";
        for (Object o : mAdapter.getSelectedTitle().entrySet()) {
            Map.Entry pair = (Map.Entry) o;
            if (selected.isEmpty())
                selected = (String) pair.getValue();
            else
                selected += ", " + pair.getValue();
        }
        mList.setText(selected);
    }

    @Subscribe
    public void onEvent(MessaggioInoltratoEvent event) {
        if (mMultiImagesFinished) {
            if (mAdapter.getSelected().size() == 1) {
                for (Map.Entry pair : mAdapter.getSelected().entrySet()) {
                    if ((boolean) pair.getValue()) {
                        Intent intent = Act_EventoMain.newIntent((String) pair.getKey());
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    } else {
                        Intent intent = Act_GruppoMain.newIntent((String) pair.getKey());
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                }
            }
            finish();
        }
    }

    // FUNCTIONS

    /**
     * Gestisco i casi in cui l'activity sia stata aperta da intent esterno
     */
    private boolean handleSpecialIntentIfPresent(String utenteId) {
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if (type.startsWith("image/")) {
                // Caso condivisione immagine
                Uri imageUri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
//                if(getMimeType(imageUri).equals("gif")) {
//                    startActivityForResult(Act_RecuperaImageEdit.newIntentGif(imageUri), IMG_CODE);
//                } else {
                startActivityForResult(Act_RecuperaImageEdit.newIntent(imageUri), IMG_CODE);
//                }
            } else if ("text/plain".equals(type)) {
                // Caso condivisione testo
                String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
                String url = Utils.findUrlInText(sharedText);

                for (Map.Entry pair : mAdapter.getSelected().entrySet()) {
                    MessaggioInfo messaggioInfo;
                    if ((boolean) pair.getValue()) {
                        // Caso evento
                        messaggioInfo = new MessaggioInfo((String) pair.getKey(),
                                Constants.MESSAGGIO_TYPOLOGY_CHAT_EVENTO,
                                utenteId,
                                sharedText,
                                null,
                                new Date().getTime(),
                                true,
                                false,
                                true);
                    } else {
                        // Caso gruppo
                        messaggioInfo = new MessaggioInfo((String) pair.getKey(),
                                Constants.MESSAGGIO_TYPOLOGY_CHAT_GRUPPO,
                                utenteId,
                                sharedText,
                                null,
                                new Date().getTime(),
                                true,
                                false,
                                true);
                    }
                    if (url != null) {
                        messaggioInfo.setUrl(url);
                    }
                    // Creo messaggio
                    MessaggiService.createMessage(Act_Inoltra.this, messaggioInfo);
                }

                if (mAdapter.getSelected().size() == 1) {
                    for (Map.Entry pair : mAdapter.getSelected().entrySet()) {
                        Intent newIntent;
                        if ((boolean) pair.getValue()) {
                            newIntent = Act_EventoMain.newIntent((String) pair.getKey());
                        } else {
                            newIntent = Act_GruppoMain.newIntent((String) pair.getKey());
                        }
                        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(newIntent);
                    }
                }
                finish();
            }
            return true;
        } else if (Intent.ACTION_SEND_MULTIPLE.equals(action) && type != null && type.startsWith("image/") && intent.hasExtra(Intent.EXTRA_STREAM)) {
            getDynamicView().showLoading();
            mMultiImagesFinished = false;

            // Caso condivisione immagini multiple
            ArrayList<Parcelable> list = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);

            for (Parcelable p : list) {
                Uri uri = (Uri) p;
                App.jobManager().addJobInBackground(new InoltraMessaggioWithExternalImageJob(uri, mAdapter.getSelected().entrySet(), utenteId));
            }

            mMultiImagesFinished = true;
            return true;
        }
        return false;
    }

    private String getMimeType(Uri uri) {
        String mimeType;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver contentResolver = getContentResolver();
            mimeType = MimeTypeMap.getSingleton().getExtensionFromMimeType(contentResolver.getType(uri));
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri.toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension.toLowerCase());
        }
        return mimeType;
    }
}
