package it.mozart.yeap.common;

import org.parceler.Parcel;

@Parcel(Parcel.Serialization.BEAN)
public class DoveInfo {

    private String dove;
    private String doveExtraText;
    private String placeId;
    private double lat;
    private double lng;

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getDove() {
        return dove;
    }

    public void setDove(String dove) {
        this.dove = dove;
    }

    public String getDoveExtraText() {
        return doveExtraText;
    }

    public void setDoveExtraText(String doveExtraText) {
        this.doveExtraText = doveExtraText;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
