package it.mozart.yeap.common;

import org.parceler.Parcel;

import java.util.Date;

@Parcel(Parcel.Serialization.BEAN)
public class QuandoInfo {

    private OnInfoChange listener;
    private Date dataDa;
    private Date oraDa;
    private Date dataA;
    private Date oraA;

    public Date getDataDa() {
        return dataDa;
    }

    public void setDataDa(Date dataDa) {
        this.dataDa = dataDa;
        if (listener != null) {
            listener.infoChanged(canShowConfirm());
        }
    }

    public Date getOraDa() {
        return oraDa;
    }

    public void setOraDa(Date oraDa) {
        this.oraDa = oraDa;
        if (listener != null) {
            listener.infoChanged(canShowConfirm());
        }
    }

    public Date getDataA() {
        return dataA;
    }

    public void setDataA(Date dataA) {
        this.dataA = dataA;
        if (listener != null) {
            listener.infoChanged(canShowConfirm());
        }
    }

    public Date getOraA() {
        return oraA;
    }

    public void setOraA(Date oraA) {
        this.oraA = oraA;
        if (listener != null) {
            listener.infoChanged(canShowConfirm());
        }
    }

    public void setListener(OnInfoChange listener) {
        this.listener = listener;
    }

    public boolean isSomeInfoPresent() {
        return dataDa != null || oraDa != null || dataA != null || oraA != null;
    }

    public boolean canShowConfirm() {
        return (dataDa != null && oraDa != null && dataA != null && oraA != null) ||
                (dataDa != null && oraDa != null && dataA == null && oraA == null) ||
                (dataDa == null && oraDa == null && dataA != null && oraA != null);
    }

    public interface OnInfoChange {
        void infoChanged(boolean infoPresent);
    }
}
