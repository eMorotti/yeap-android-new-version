package it.mozart.yeap.common;

import com.schinizer.rxunfurl.model.PreviewData;

import org.parceler.Parcel;

@Parcel(Parcel.Serialization.BEAN)
public class UrlPreviewData {

    private String title;
    private String description;
    private String url;
    private String image;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public UrlPreviewData fromData(PreviewData data) {
        this.title = data.getTitle();
        this.description = data.getDescription();
        this.url = data.getUrl();
        if (data.getImages() != null && data.getImages().size() != 0) {
            this.image = data.getImages().get(0).getSource();
        }
        return this;
    }
}
