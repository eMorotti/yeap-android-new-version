package it.mozart.yeap.common;

import org.parceler.Parcel;
import org.parceler.ParcelConstructor;

@Parcel(Parcel.Serialization.BEAN)
public class MediaInfo {

    private String media;
    private String thumbData;
    private int mediaType;
    private int width;
    private int height;
    private boolean external;

    public MediaInfo(String path) {
        this(path, 0, false);
    }

    public MediaInfo(String path, int type) {
        this(path, type, false);
    }

    public MediaInfo(String path, int type, boolean external) {
        this(path, type, null, external);
    }

    public MediaInfo(String path, int type, String thumb, boolean external) {
        this.media = path;
        this.mediaType = type;
        this.thumbData = thumb;
        this.external = external;
    }

    @ParcelConstructor
    public MediaInfo(String media, String thumbData, int mediaType, int width, int height, boolean external) {
        this.media = media;
        this.thumbData = thumbData;
        this.mediaType = mediaType;
        this.width = width;
        this.height = height;
        this.external = external;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getThumbData() {
        return thumbData;
    }

    public int getMediaType() {
        return mediaType;
    }

    public boolean isExternal() {
        return external;
    }
}
