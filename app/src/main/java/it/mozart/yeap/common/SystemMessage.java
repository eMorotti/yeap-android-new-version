package it.mozart.yeap.common;

public class SystemMessage {

    private int type;
    private String clickId;
    private String clickIdParent;
    private String message;
    private String lastMessage;
    private String byId;
    private String toId;
    private String what;
    private String whatOld;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getById() {
        return byId;
    }

    public void setById(String byId) {
        this.byId = byId;
    }

    public String getToId() {
        return toId;
    }

    public void setToId(String toId) {
        this.toId = toId;
    }

    public String getWhat() {
        return what;
    }

    public void setWhat(String what) {
        this.what = what;
    }

    public String getWhatOld() {
        return whatOld;
    }

    public void setWhatOld(String whatOld) {
        this.whatOld = whatOld;
    }

    public String getClickId() {
        return clickId;
    }

    public void setClickId(String clickId) {
        this.clickId = clickId;
    }

    public String getClickIdParent() {
        return clickIdParent;
    }

    public void setClickIdParent(String clickIdParent) {
        this.clickIdParent = clickIdParent;
    }
}
