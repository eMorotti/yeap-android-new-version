package it.mozart.yeap.common;

import org.parceler.Parcel;
import org.parceler.ParcelConstructor;

@Parcel(Parcel.Serialization.BEAN)
public class MessaggioInfo {

    private String parentId;
    private int parentType;
    private String utenteId;
    private String testo;
    private MediaInfo mediaInfo;
    private long createdAt;
    private UrlPreviewData previewData;
    private String url;
    private boolean urlChecked;
    private boolean letto;
    private boolean inviato;
    private boolean send;

    @ParcelConstructor
    public MessaggioInfo(String parentId, int parentType, String utenteId, String testo, MediaInfo mediaInfo, long createdAt, boolean letto, boolean inviato, boolean send) {
        this.parentId = parentId;
        this.parentType = parentType;
        this.utenteId = utenteId;
        this.testo = testo;
        this.mediaInfo = mediaInfo;
        this.createdAt = createdAt;
        this.letto = letto;
        this.inviato = inviato;
        this.send = send;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public int getParentType() {
        return parentType;
    }

    public void setParentType(int parentType) {
        this.parentType = parentType;
    }

    public String getUtenteId() {
        return utenteId;
    }

    public void setUtenteId(String utenteId) {
        this.utenteId = utenteId;
    }

    public String getTesto() {
        return testo;
    }

    public void setTesto(String testo) {
        this.testo = testo;
    }

    public boolean isLetto() {
        return letto;
    }

    public void setLetto(boolean letto) {
        this.letto = letto;
    }

    public boolean isInviato() {
        return inviato;
    }

    public void setInviato(boolean inviato) {
        this.inviato = inviato;
    }

    public MediaInfo getMediaInfo() {
        return mediaInfo;
    }

    public void setMediaInfo(MediaInfo mediaInfo) {
        this.mediaInfo = mediaInfo;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public UrlPreviewData getPreviewData() {
        return previewData;
    }

    public void setPreviewData(UrlPreviewData previewData) {
        this.previewData = previewData;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isUrlChecked() {
        return urlChecked;
    }

    public void setUrlChecked(boolean urlChecked) {
        this.urlChecked = urlChecked;
    }

    public boolean isSend() {
        return send;
    }

    public void setSend(boolean send) {
        this.send = send;
    }
}
