package it.mozart.yeap.realm;

import org.parceler.Parcel;

import io.realm.RealmObject;
import io.realm.UserRealmProxy;
import io.realm.annotations.Ignore;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.utility.Prefs;
import it.mozart.yeap.utility.Utils;

@Parcel(implementations = {UserRealmProxy.class},
        value = Parcel.Serialization.BEAN,
        analyze = {User.class})
public class User extends RealmObject {

    @PrimaryKey
    private String id;
    public static String ID = "id";

    @Index
    private String phone;
    public static String PHONE = "phone";

    @Index
    private String uuid;
    public static String UUID = "uuid";

    private String name;
    private String surname;
    private String nickname;

    private String realName;
    public static String REAL_NAME = "realName";

    @Ignore
    private String avatar;

    private String contactId;
    private boolean inYeap;
    public static String IN_YEAP = "inYeap";
    private boolean inContacts;
    public static String IN_CONTACTS = "inContacts";
    private boolean me;
    public static String ME = "me";
    private boolean deleted;
    public static String DELETED = "deleted";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        calcRealName();
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
        calcRealName();
    }

    public String getNickname() {
        if (nickname == null) {
            return App.getContext().getString(R.string.general_sconosciuto);
        }
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
        calcRealName();
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public boolean isInYeap() {
        return inYeap;
    }

    public void setInYeap(boolean inYeap) {
        this.inYeap = inYeap;
    }

    public boolean isInContacts() {
        return inContacts;
    }

    public void setInContacts(boolean inContacts) {
        this.inContacts = inContacts;
    }

    public boolean isMe() {
        return me;
    }

    public void setMe(boolean me) {
        this.me = me;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getAvatarThumb() {
        if (uuid == null) {
            return null;
        }
        return Utils.getApiURL() + "/users/" + uuid + "/avatar?type=thumb&v=" + Prefs.getString(uuid, "0");
    }

    public String getAvatarLarge() {
        if (uuid == null) {
            return null;
        }
        return Utils.getApiURL() + "/users/" + uuid + "/avatar?v=" + Prefs.getString(uuid, "0");
    }

    private void calcRealName() {
        if (deleted && !getNickname().equals(App.getContext().getString(R.string.general_sconosciuto))) {
            setRealName(getNickname());
            return;
        }

        String name;
        if (getName() != null && getSurname() != null && !deleted) {
            name = String.format("%s %s", getName(), getSurname());
        } else if (getName() != null && !deleted) {
            name = getName();
        } else if (getSurname() != null && !deleted) {
            name = getSurname();
        } else {
            name = getNickname();
        }

        setRealName(name);
    }

    public String getChatName() {
        return getChatName(false);
    }

    public String getChatName(boolean ignoreTilde) {
        if (isMe()) {
            return App.getContext().getString(R.string.general_tu);
        }

        if (deleted && !getNickname().equals(App.getContext().getString(R.string.general_sconosciuto))) {
            return String.format("~%s", getNickname());
        }

        String chatName;
        if (getName() != null && getSurname() != null) {
            chatName = String.format("%s %s", getName(), getSurname());
        } else if (getName() != null) {
            chatName = getName();
        } else if (getSurname() != null) {
            chatName = getSurname();
        } else {
            chatName = getNickname();
        }

        if (!isInContacts() && !ignoreTilde) {
            return String.format("~%s", chatName);
        } else {
            return chatName;
        }
    }

    @Override
    public int hashCode() {
        if (uuid != null) {
            return uuid.hashCode();
        } else {
            return (getRealName() + phone + inContacts).hashCode();
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof User && ((User) obj).getUuid() != null) {
            return ((User) obj).getUuid().equals(uuid);
        } else if (obj instanceof User) {
            return ((User) obj).getPhone().equals(phone);
        }
        return super.equals(obj);
    }
}
