package it.mozart.yeap.realm;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

public class Vote extends RealmObject {

    @PrimaryKey
    private String uuid;
    public static String UUID = "uuid";
    @Index
    private int type; // YES, NO
    public static String TYPE = "type";
    @Index
    private int voteType; // VOTE, INTEREST
    public static String VOTE_TYPE = "voteType";
    private long createdAt;
    public static String CREATED_AT = "createdAt";
    private String eventUuid;
    public static String EVENT_ID = "eventUuid";
    private String pollItemUuid;
    public static String POLLITEM_ID = "pollItemUuid";

    private User creator;
    private String userUuid;
    public static String CREATOR_ID = "userUuid";

    private boolean loading;
    public static String LOADING = "loading";

    private boolean deleted;
    public static String DELETED = "deleted";

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getVoteType() {
        return voteType;
    }

    public void setVoteType(int voteType) {
        this.voteType = voteType;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public String getEventUuid() {
        return eventUuid;
    }

    public void setEventUuid(String eventUuid) {
        this.eventUuid = eventUuid;
    }

    public String getPollItemUuid() {
        return pollItemUuid;
    }

    public void setPollItemUuid(String pollItemUuid) {
        this.pollItemUuid = pollItemUuid;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
        if (creator == null) {
            setCreatorUuid(null);
        } else {
            setCreatorUuid(creator.getUuid());
        }
    }

    public String getCreatorUuid() {
        return userUuid;
    }

    public void setCreatorUuid(String creatorUuid) {
        this.userUuid = creatorUuid;
    }

    public boolean isLoading() {
        return loading;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
