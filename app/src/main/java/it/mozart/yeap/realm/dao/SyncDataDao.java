package it.mozart.yeap.realm.dao;

import io.realm.Realm;
import io.realm.RealmResults;
import it.mozart.yeap.realm.SyncData;

public class SyncDataDao {

    private Realm realm;

    public SyncDataDao(Realm realm) {
        this.realm = realm;
    }

    public SyncData getSyncData(String id) {
        return realm.where(SyncData.class).equalTo(SyncData.ID, id).findFirst();
    }

    public RealmResults<SyncData> getListSyncDataNotDone() {
        return realm.where(SyncData.class).equalTo(SyncData.DONE, false).findAllSorted(SyncData.CREATED_AT);
    }

    public void deleteAllSyncDataDone() {
        realm.where(SyncData.class).equalTo(SyncData.DONE, true).findAll().deleteAllFromRealm();
    }

    public void deleteAllSyncData() {
        realm.where(SyncData.class).findAll().deleteAllFromRealm();
    }
}
