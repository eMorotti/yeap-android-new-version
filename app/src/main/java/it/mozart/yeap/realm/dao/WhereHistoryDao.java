package it.mozart.yeap.realm.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import it.mozart.yeap.common.DoveInfo;
import it.mozart.yeap.realm.WhereHistory;

public class WhereHistoryDao {

    private Realm realm;

    public WhereHistoryDao(Realm realm) {
        this.realm = realm;
    }

    public List<WhereHistory> getHistory() {
        RealmResults<WhereHistory> results = realm.where(WhereHistory.class).findAllSorted(WhereHistory.CREATED_AT, Sort.DESCENDING);
        if (results != null && results.size() != 0) {
            return results.subList(0, Math.min(results.size(), 4));
        }
        return new ArrayList<>();
    }

    public void createOrUpdate(DoveInfo doveInfo) {
        WhereHistory historyItem = realm.where(WhereHistory.class).equalTo(WhereHistory.TEXT, doveInfo.getDove()).equalTo(WhereHistory.PLACE_ID, doveInfo.getPlaceId()).findFirst();
        if (historyItem != null) {
            historyItem.setCreatedAt(new Date().getTime());
        } else {
            historyItem = realm.createObject(WhereHistory.class);
            historyItem.setText(doveInfo.getDove());
            historyItem.setSecondaryText(doveInfo.getDoveExtraText());
            historyItem.setPlaceId(doveInfo.getPlaceId());
        }
    }
}
