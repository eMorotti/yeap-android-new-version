package it.mozart.yeap.realm;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

public class Event extends RealmObject {

    @PrimaryKey
    private String uuid;
    public static String UUID = "uuid";
    private int version;
    public static String VERSION = "version";
    private int status;

    private long createdAt;
    public static String CREATED_AT = "createdAt";
    @Index
    private long updatedAt;
    public static String UPDATED_AT = "updatedAt";

    private EventInfo event;

    private String desc;
    private boolean usersCanInvite;
    private boolean usersCanAddItems;

    private String avatarThumbUrl;
    private String avatarLargeUrl;
    @Ignore
    private String avatar;
    @Ignore
    private File avatarFile;

    private Message message;
    private long numNewMessages;
    public static String NUM_NEW_MESSAGES = "numNewMessages";

    private RealmList<User> guests = new RealmList<>();
    private RealmList<User> guestsNotYeap = new RealmList<>();
    @Ignore
    private List<User> guestsTemp;
    @Ignore
    private RealmList<User> users;
    @Ignore
    private List<String> partecipants;
    @Ignore
    private List<String> invited;

    private String wherePollUuid;
    private boolean wherePollEnabled;
    private String whenPollUuid;
    private boolean whenPollEnabled;

    private Group group;
    private String groupUuid;
    public static String GROUP_ID = "groupUuid";

    private long numInfoUpdates;
    public static String NUM_INFO_UPDATES = "numInfoUpdates";
    private long numPollUpdates;
    public static String NUM_POLL_UPDATES = "numPollUpdates";
    private long numUserUpdates;
    public static String NUM_USER_UPDATES = "numUserUpdates";

    private long numVotesYes;
    private long numVotesNo;
    private long numInterestYes;
    private long numInterestNo;

    private User admin;
    private String adminUuid;
    private User creator;
    private String creatorUuid;

    private boolean myselfPresent;
    public static String MYSELF_PRESENT = "myselfPresent";

    @Ignore
    private RealmList<Poll> polls;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public EventInfo getEventInfo() {
        return event;
    }

    public void setEventInfo(EventInfo eventInfo) {
        this.event = eventInfo;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public boolean isUsersCanInvite() {
        return usersCanInvite;
    }

    public void setUsersCanInvite(boolean usersCanInvite) {
        this.usersCanInvite = usersCanInvite;
    }

    public boolean isUsersCanAddItems() {
        return usersCanAddItems;
    }

    public void setUsersCanAddItems(boolean usersCanAddItems) {
        this.usersCanAddItems = usersCanAddItems;
    }

    public String getAvatarThumbUrl() {
        return avatarThumbUrl;
    }

    public void setAvatarThumbUrl(String avatarThumbUrl) {
        this.avatarThumbUrl = avatarThumbUrl;
    }

    public String getAvatarLargeUrl() {
        return avatarLargeUrl;
    }

    public void setAvatarLargeUrl(String avatarLargeUrl) {
        this.avatarLargeUrl = avatarLargeUrl;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public File getAvatarFile() {
        return avatarFile;
    }

    public void setAvatarFile(File avatarFile) {
        this.avatarFile = avatarFile;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public long getNumNewMessages() {
        return numNewMessages;
    }

    public void setNumNewMessages(long numNewMessages) {
        this.numNewMessages = numNewMessages;
    }

    public RealmList<User> getGuests() {
        return guests;
    }

    public HashSet<User> getGuestsHashSet() {
        if(guests != null) {
            return new HashSet<>(guests);
        }
        return new HashSet<>();
    }

    public void setGuests(RealmList<User> guests) {
        this.guests = guests;
    }

    public RealmList<User> getGuestsNotYeap() {
        return guestsNotYeap;
    }

    public HashSet<User> getGuestsNotYeapHashSet() {
        if(guestsNotYeap != null) {
            return new HashSet<>(guestsNotYeap);
        }
        return new HashSet<>();
    }

    public void setGuestsNotYeap(RealmList<User> guestsNotYeap) {
        this.guestsNotYeap = guestsNotYeap;
    }

    public List<User> getGuestsTemp() {
        if (guestsTemp == null)
            guestsTemp = new ArrayList<>();
        return guestsTemp;
    }

    public void setGuestsTemp(List<User> guestsTemp) {
        this.guestsTemp = guestsTemp;
    }

    public RealmList<User> getUsers() {
        return users;
    }

    public void setUsers(RealmList<User> users) {
        this.users = users;
    }

    public List<String> getPartecipants() {
        if (partecipants == null)
            partecipants = new ArrayList<>();
        return partecipants;
    }

    public void setPartecipants(List<String> partecipants) {
        this.partecipants = partecipants;
    }

    public List<String> getInvited() {
        if (invited == null)
            invited = new ArrayList<>();
        return invited;
    }

    public void setInvited(List<String> invited) {
        this.invited = invited;
    }

    public String getWherePollUuid() {
        return wherePollUuid;
    }

    public void setWherePollUuid(String wherePollUuid) {
        this.wherePollUuid = wherePollUuid;
    }

    public boolean isWherePollEnabled() {
        return wherePollEnabled;
    }

    public void setWherePollEnabled(boolean wherePollEnabled) {
        this.wherePollEnabled = wherePollEnabled;
    }

    public String getWhenPollUuid() {
        return whenPollUuid;
    }

    public void setWhenPollUuid(String whenPollUuid) {
        this.whenPollUuid = whenPollUuid;
    }

    public boolean isWhenPollEnabled() {
        return whenPollEnabled;
    }

    public void setWhenPollEnabled(boolean whenPollEnabled) {
        this.whenPollEnabled = whenPollEnabled;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
        if (group == null) {
            setGroupUuid(null);
        } else {
            setGroupUuid(group.getUuid());
        }
    }

    public String getGroupUuid() {
        return groupUuid;
    }

    public void setGroupUuid(String groupUuid) {
        this.groupUuid = groupUuid;
    }

    public long getNumInfoUpdates() {
        return numInfoUpdates;
    }

    public void setNumInfoUpdates(long numInfoUpdates) {
        this.numInfoUpdates = numInfoUpdates;
    }

    public long getNumPollUpdates() {
        return numPollUpdates;
    }

    public void setNumPollUpdates(long numPollUpdates) {
        this.numPollUpdates = numPollUpdates;
    }

    public long getNumUserUpdates() {
        return numUserUpdates;
    }

    public void setNumUserUpdates(long numUserUpdates) {
        this.numUserUpdates = numUserUpdates;
    }

    public long getNumVotesYes() {
        return numVotesYes;
    }

    public void setNumVotesYes(long numVotesYes) {
        this.numVotesYes = numVotesYes;
    }

    public long getNumVotesNo() {
        return numVotesNo;
    }

    public void setNumVotesNo(long numVotesNo) {
        this.numVotesNo = numVotesNo;
    }

    public long getNumInterestYes() {
        return numInterestYes;
    }

    public void setNumInterestYes(long numInterestYes) {
        this.numInterestYes = numInterestYes;
    }

    public long getNumInterestNo() {
        return numInterestNo;
    }

    public void setNumInterestNo(long numInterestNo) {
        this.numInterestNo = numInterestNo;
    }

    public User getAdmin() {
        return admin;
    }

    public void setAdmin(User admin) {
        this.admin = admin;
        if (admin == null) {
            setAdminUuid(null);
        } else {
            setAdminUuid(admin.getUuid());
        }
    }

    public String getAdminUuid() {
        return adminUuid;
    }

    public void setAdminUuid(String adminUuid) {
        this.adminUuid = adminUuid;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
        if (creator == null) {
            setCreatorUuid(null);
        } else {
            setCreatorUuid(creator.getUuid());
        }
    }

    public String getCreatorUuid() {
        return creatorUuid;
    }

    public void setCreatorUuid(String creatorUuid) {
        this.creatorUuid = creatorUuid;
    }

    public boolean isMyselfPresent() {
        return myselfPresent;
    }

    public void setMyselfPresent(boolean myselfPresent) {
        this.myselfPresent = myselfPresent;
    }

    public RealmList<Poll> getPolls() {
        return polls;
    }

    public void setPolls(RealmList<Poll> polls) {
        this.polls = polls;
    }

    public String getTitle() {
        return getEventInfo().getTitle();
    }
}
