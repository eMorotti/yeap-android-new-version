package it.mozart.yeap.realm;

import android.content.Context;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.UnderlineSpan;

import com.google.gson.Gson;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
import it.mozart.yeap.common.SystemMessage;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.EasySpan;
import uk.co.chrisjenx.calligraphy.CalligraphyTypefaceSpan;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

public class Message extends RealmObject {

    @PrimaryKey
    private String uuid;
    public static String UUID = "uuid";
    private long createdAt;
    public static String CREATED_AT = "createdAt";
    private long createdAtDate;
    public static String CREATED_AT_DATE = "createdAtDate";
    private long createdAtForVisualization;
    public static String CREATED_AT_DATE_VISUALIZATION = "createdAtForVisualization";
    @Index
    private int type; // Normale, Sistema
    public static String TYPE = "type";
    @Index
    private int typology; // Evento, Gruppo
    public static String TYPOLOGY = "typology";
    @Index
    private int systemType;
    public static String SYSTEM_TYPE = "systemType";

    private String text;
    private String systemTextJson;
    private String thumbData;
    private String mediaUrl;
    private String mediaLocalUrl;
    private String mediaType;
    private double mediaWidth;
    private double mediaHeight;
    private int imageState;

    private User creator;
    public static String CREATOR = "creator";
    private String creatorUuid;

    @Index
    private String eventParentUuid;
    public static String EVENT_PARENT_ID = "eventParentUuid";
    @Index
    private String groupParentUuid;
    public static String GROUP_PARENT_ID = "groupParentUuid";

    private Poll poll;
    private PollItem pollItem;

    @Index
    private boolean ignoreNotification;
    public static String IGNORE_NOTIFICATION = "ignoreNotification";
    @Index
    private boolean sent;
    public static String SENT = "sent";
    @Index
    private boolean read;
    public static String READ = "read";
    @Index
    private boolean ignoreRead;
    public static String IGNORE_READ = "ignoreRead";
    @Index
    private boolean firstNotRead;
    public static String FIRST_NOT_READ = "firstNotRead";

    private boolean urlChecked;
    private String urlInfo;
    private String url;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
        setCreatedAtDate(createdAt / 86400000);
    }

    public long getCreatedAtDate() {
        return createdAtDate;
    }

    public void setCreatedAtDate(long createdAtDate) {
        this.createdAtDate = createdAtDate;
    }

    public long getCreatedAtForVisualization() {
        return createdAtForVisualization;
    }

    public void setCreatedAtForVisualization(long createdAtForVisualization) {
        this.createdAtForVisualization = createdAtForVisualization;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getTypology() {
        return typology;
    }

    public void setTypology(int typology) {
        this.typology = typology;
    }

    public int getSystemType() {
        return systemType;
    }

    public void setSystemType(int systemType) {
        this.systemType = systemType;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSystemTextJson() {
        return systemTextJson;
    }

    public void setSystemTextJson(String systemTextJson) {
        this.systemTextJson = systemTextJson;
    }

    public String getThumbData() {
        return thumbData;
    }

    public void setThumbData(String thumbData) {
        this.thumbData = thumbData;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    public String getMediaLocalUrl() {
        return mediaLocalUrl;
    }

    public void setMediaLocalUrl(String mediaLocalUrl) {
        this.mediaLocalUrl = mediaLocalUrl;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public double getMediaWidth() {
        return mediaWidth;
    }

    public void setMediaWidth(double mediaWidth) {
        this.mediaWidth = mediaWidth;
    }

    public double getMediaHeight() {
        return mediaHeight;
    }

    public void setMediaHeight(double mediaHeight) {
        this.mediaHeight = mediaHeight;
    }

    public int getImageState() {
        return imageState;
    }

    public void setImageState(int imageState) {
        this.imageState = imageState;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
        if (creator == null) {
            setCreatorUuid(null);
        } else {
            setCreatorUuid(creator.getUuid());
        }
    }

    public String getCreatorUuid() {
        return creatorUuid;
    }

    public void setCreatorUuid(String creatorUuid) {
        this.creatorUuid = creatorUuid;
    }

    public String getEventParentUuid() {
        return eventParentUuid;
    }

    public void setEventParentUuid(String eventParentUuid) {
        this.eventParentUuid = eventParentUuid;
    }

    public String getGroupParentUuid() {
        return groupParentUuid;
    }

    public void setGroupParentUuid(String groupParentUuid) {
        this.groupParentUuid = groupParentUuid;
    }

    public Poll getPoll() {
        return poll;
    }

    public void setPoll(Poll poll) {
        this.poll = poll;
    }

    public PollItem getPollItem() {
        return pollItem;
    }

    public void setPollItem(PollItem pollItem) {
        this.pollItem = pollItem;
    }

    public boolean isIgnoreNotification() {
        return ignoreNotification;
    }

    public void setIgnoreNotification(boolean ignoreNotification) {
        this.ignoreNotification = ignoreNotification;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public boolean isIgnoreRead() {
        return ignoreRead;
    }

    public void setIgnoreRead(boolean ignoreRead) {
        this.ignoreRead = ignoreRead;
    }

    public boolean isFirstNotRead() {
        return firstNotRead;
    }

    public void setFirstNotRead(boolean firstNotRead) {
        this.firstNotRead = firstNotRead;
    }

    public boolean isUrlChecked() {
        return urlChecked;
    }

    public void setUrlChecked(boolean urlChecked) {
        this.urlChecked = urlChecked;
    }

    public String getUrlInfo() {
        return urlInfo;
    }

    public void setUrlInfo(String urlInfo) {
        this.urlInfo = urlInfo;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    // GESTIONE MESSAGGI

    public Spanned getFormattedMessageWithoutCreator(Context context) {
        return getFormattedMessage(context, false);
    }

    public Spanned getFormattedMessage(Context context) {
        return getFormattedMessage(context, true);
    }

    public Spanned getSystemMessageWhat() {
        try {
            SystemMessage systemMessage = new Gson().fromJson(getSystemTextJson(), SystemMessage.class);
            if (systemMessage.getWhat() != null) {
                return new SpannableString(systemMessage.getWhat());
            } else {
                return new SpannableString("");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return new SpannableString("");
        }
    }

    private Spanned getFormattedMessage(Context context, boolean showCreator) {
        if (getType() == Constants.MESSAGGIO_TYPE_SYSTEM) {
            return getSystemMessageText(context, showCreator);
        } else {
            return new SpannableString(getText());
        }
    }

    private Spanned getSystemMessageText(Context context, boolean showCreator) {
        try {
            SystemMessage systemMessage = new Gson().fromJson(getSystemTextJson(), SystemMessage.class);
            if (!showCreator && systemMessage.getLastMessage() != null) {
                return crateSystemMessage(context, systemMessage, true, true);
            } else {
                return crateSystemMessage(context, systemMessage, false, false);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return new SpannableString("");
        }
    }

    private Spanned crateSystemMessage(Context context, SystemMessage systemMessage, boolean useLastMessage, boolean ignoreStyle) {
        String text;
        if (useLastMessage) {
            text = systemMessage.getLastMessage();
        } else {
            text = systemMessage.getMessage();
        }

        EasySpan.Builder easySpanBuilder = new EasySpan.Builder();
        if (!ignoreStyle) {
            Typeface typeface = TypefaceUtils.load(context.getAssets(), "fonts/OpenSans-Bold.ttf");
            String[] split = text.split("\\|#b\\|");
            for (int i = 0; i < split.length; i++) {
                if (i % 2 == 1) {
                    CalligraphyTypefaceSpan typefaceSpan = new CalligraphyTypefaceSpan(typeface);
                    if (split[i].startsWith("|#u|")) {
                        split[i] = split[i].replaceFirst("\\|#u\\|", "");
                        easySpanBuilder.appendSpans(split[i], typefaceSpan, new UnderlineSpan());
                    } else {
                        easySpanBuilder.appendSpans(split[i], typefaceSpan);
                    }
                } else {
                    easySpanBuilder.appendText(split[i]);
                }
            }
        } else {
            easySpanBuilder.appendText(text);
        }

        return easySpanBuilder.build().getSpannedText();
    }
}
