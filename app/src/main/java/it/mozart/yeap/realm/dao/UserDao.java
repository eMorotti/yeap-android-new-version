package it.mozart.yeap.realm.dao;

import java.util.HashSet;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.User;

public class UserDao {

    private Realm realm;

    public UserDao(Realm realm) {
        this.realm = realm;
    }

    public User getUser(String uuid) {
        return realm.where(User.class).equalTo(User.UUID, uuid).findFirst();
    }

    public User getUserNotDeleted(String uuid) {
        return realm.where(User.class).equalTo(User.UUID, uuid).equalTo(User.DELETED, false).findFirst();
    }

    public User getUserDeleted(String uuid) {
        return realm.where(User.class).equalTo(User.UUID, uuid).equalTo(User.DELETED, true).findFirst();
    }

    public User getUserLocal(String phone) {
        return realm.where(User.class).equalTo(User.PHONE, phone).isNull(User.UUID).equalTo(User.DELETED, false).findFirst();
    }

    public User getUserButLocal(String phone) {
        return realm.where(User.class).equalTo(User.PHONE, phone).isNotNull(User.UUID).equalTo(User.DELETED, false).findFirst();
    }

    public RealmResults<User> getUsersLocalNotIn(HashSet<String> phones) {
        RealmQuery<User> query = realm.where(User.class).isNull(User.UUID).equalTo(User.DELETED, false);
        if (phones != null && phones.size() > 0) {
            for (String phone : phones) {
                query = query.notEqualTo(User.PHONE, phone);
            }
        }
        return query.findAll();
    }

    public RealmResults<User> getUsersNotIn(HashSet<String> phones) {
        RealmQuery<User> query = realm.where(User.class).isNotNull(User.UUID).equalTo(User.DELETED, false);
        if (phones != null && phones.size() > 0) {
            for (String phone : phones) {
                query = query.notEqualTo(User.PHONE, phone);
            }
        }
        return query.findAll();
    }

    public User getUserByPhone(String phone) {
        return realm.where(User.class).equalTo(User.PHONE, phone).equalTo(User.DELETED, false).findFirst();
    }

    public RealmResults<User> getUsersByPhone(String phone) {
        return realm.where(User.class).equalTo(User.PHONE, phone).equalTo(User.DELETED, false).findAll();
    }

    public RealmResults<User> getUsersByPhoneIgnoreDelete(String phone) {
        return realm.where(User.class).equalTo(User.PHONE, phone).findAll();
    }

    public RealmResults<User> getUsersByPhoneWithDelete(String phone, boolean delete) {
        return realm.where(User.class).equalTo(User.PHONE, phone).equalTo(User.DELETED, delete).findAll();
    }

    public RealmResults<User> getListUsers(String filter) {
        RealmQuery<User> query = realm.where(User.class).equalTo(User.ME, false).notEqualTo(User.UUID, AccountProvider.getInstance().getAccountId()).equalTo(User.IN_CONTACTS, true);
        if (filter != null && filter.length() != 0) {
            query = query.contains(User.REAL_NAME, filter, Case.INSENSITIVE);
        }
        return query.equalTo(User.DELETED, false).findAllSorted(User.REAL_NAME, Sort.ASCENDING);
    }

    public RealmResults<User> getListUserForInviting(String filter) {
        RealmQuery<User> query = realm.where(User.class).equalTo(User.DELETED, false).equalTo(User.ME, false)
                .notEqualTo(User.UUID, AccountProvider.getInstance().getAccountId()).equalTo(User.IN_CONTACTS, true);
        if (filter != null && filter.length() != 0) {
            query = query.contains(User.REAL_NAME, filter, Case.INSENSITIVE);
        }
        return query.findAllSorted(User.REAL_NAME, Sort.ASCENDING);
    }

    public RealmResults<User> getAllListUserExceptMe() {
        return realm.where(User.class).notEqualTo(User.UUID, AccountProvider.getInstance().getAccessToken())//.equalTo(User.IN_CONTACTS, true)
                .findAllSorted(new String[]{User.IN_YEAP, User.REAL_NAME}, new Sort[]{Sort.DESCENDING, Sort.ASCENDING});
    }

    public User createUser(User utente) {
        return realm.copyToRealm(utente);
    }
}
