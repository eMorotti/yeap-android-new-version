package it.mozart.yeap.realm;

import io.realm.RealmObject;

public class WhereHistory extends RealmObject {

    private String text;
    public static String TEXT = "text";
    private String secondaryText;
    private String placeId;
    public static String PLACE_ID = "placeId";

    private long createdAt;
    public static String CREATED_AT = "createdAt";

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSecondaryText() {
        return secondaryText;
    }

    public void setSecondaryText(String secondaryText) {
        this.secondaryText = secondaryText;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }
}
