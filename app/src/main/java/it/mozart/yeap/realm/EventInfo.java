package it.mozart.yeap.realm;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import it.mozart.yeap.common.DoveInfo;
import it.mozart.yeap.common.QuandoInfo;
import it.mozart.yeap.utility.DateUtils;

public class EventInfo extends RealmObject {

    @PrimaryKey
    private String uuid;
    public static String UUID = "uuid";

    private String what;
    private String where;
    private String placeId;
    private double lat;
    private double lng;
    private String fromDate;
    private String fromHour;
    private String toDate;
    private String toHour;

    private Vote myVote;
    private Vote myVoteInterest;

    @Ignore
    private RealmList<Vote> votes;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getWhat() {
        return what;
    }

    public void setWhat(String what) {
        this.what = what;
    }

    public String getWhere() {
        return where;
    }

    public void setWhere(String where) {
        this.where = where;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getFromHour() {
        return fromHour;
    }

    public void setFromHour(String fromHour) {
        this.fromHour = fromHour;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getToHour() {
        return toHour;
    }

    public void setToHour(String toHour) {
        this.toHour = toHour;
    }

    public Vote getMyVote() {
        return myVote;
    }

    public void setMyVote(Vote myVote) {
        this.myVote = myVote;
    }

    public Vote getMyVoteInterest() {
        return myVoteInterest;
    }

    public void setMyVoteInterest(Vote myVoteInterest) {
        this.myVoteInterest = myVoteInterest;
    }

    public RealmList<Vote> getVotes() {
        return votes;
    }

    public void setVotes(RealmList<Vote> votes) {
        this.votes = votes;
    }

    public String getTitle() {
        if (what != null) {
            return what;
        }
        if (where != null) {
            return where;
        }
        String quando = DateUtils.createQuandoText(generateQuandoModel());
        if (quando != null) {
            return quando;
        }

        return "";
    }

    public DoveInfo generateDoveModel() {
        if (getWhere() != null) {
            DoveInfo doveInfo = new DoveInfo();
            doveInfo.setPlaceId(getPlaceId());
            doveInfo.setLat(getLat());
            doveInfo.setLng(getLng());
            doveInfo.setDove(getWhere());
            return doveInfo;
        }
        return null;
    }

    public QuandoInfo generateQuandoModel() {
        Date startDate = null;
        if (fromDate != null && fromDate.length() > 0) {
            startDate = DateTime.parse(fromDate, DateTimeFormat.forPattern("yyyy-MM-dd")).toDate();
        }
        Date startTime = null;
        if (fromHour != null && fromHour.length() > 0) {
            startTime = DateTime.parse(fromHour, DateTimeFormat.forPattern("HH:mm")).toDate();
        }
        Date endDate = null;
        if (toDate != null && toDate.length() > 0) {
            endDate = DateTime.parse(toDate, DateTimeFormat.forPattern("yyyy-MM-dd")).toDate();
        }
        Date endTime = null;
        if (toHour != null && toHour.length() > 0) {
            endTime = DateTime.parse(toHour, DateTimeFormat.forPattern("HH:mm")).toDate();
        }
        if (startDate != null || startTime != null || endDate != null || endTime != null) {
            QuandoInfo quandoInfo = new QuandoInfo();
            quandoInfo.setDataDa(startDate);
            quandoInfo.setOraDa(startTime);
            quandoInfo.setDataA(endDate);
            quandoInfo.setOraA(endTime);
            return quandoInfo;
        }
        return null;
    }

    public DateTime generaleDateTimeFrom() {
        DateTime date = null;
        if (fromDate != null && fromDate.length() > 0) {
            date = DateTime.parse(fromDate, DateTimeFormat.forPattern("yyyy-MM-dd"));
        }
        DateTime time = null;
        if (fromHour != null && fromHour.length() > 0) {
            time = DateTime.parse(fromHour, DateTimeFormat.forPattern("HH:mm"));
        }
        if (date != null && time != null) {
            return date.withTime(time.toLocalTime());
        }

        return null;
    }

    public DateTime generaleDateTimeTo() {
        DateTime date = null;
        if (toDate != null && toDate.length() > 0) {
            date = DateTime.parse(toDate, DateTimeFormat.forPattern("yyyy-MM-dd"));
        }
        DateTime time = null;
        if (toHour != null && toHour.length() > 0) {
            time = DateTime.parse(toHour, DateTimeFormat.forPattern("HH:mm"));
        }
        if (date != null && time != null) {
            return date.withTime(time.toLocalTime());
        }

        return null;
    }

    public boolean isQuandoPresent() {
        return fromDate != null && fromDate.length() > 0 || fromHour != null && fromHour.length() > 0 || toDate != null && toDate.length() > 0 || toHour != null && toHour.length() > 0;
    }

    public Date getFirstDate() {
        if (fromDate != null && fromDate.length() > 0) {
            return DateTime.parse(fromDate, DateTimeFormat.forPattern("yyyy-MM-dd")).toDate();
        }
        if (toDate != null && toDate.length() > 0) {
            return DateTime.parse(toDate, DateTimeFormat.forPattern("yyyy-MM-dd")).toDate();
        }
        return null;
    }

    public void updateDoveInfo(DoveInfo doveModel) {
        if (doveModel == null) {
            where = null;
            placeId = null;
            lat = 0;
            lng = 0;
        } else {
            where = doveModel.getDove();
            placeId = doveModel.getPlaceId();
            lat = doveModel.getLat();
            lng = doveModel.getLng();
        }
    }

    public void updateQuandoInfo(QuandoInfo quandoModel) {
        if (quandoModel == null) {
            fromDate = null;
            fromHour = null;
            toDate = null;
            toHour = null;
        } else {
            if (quandoModel.getDataDa() != null) {
                fromDate = DateUtils.formatDate(quandoModel.getDataDa(), "yyyy-MM-dd");
            }
            if (quandoModel.getOraDa() != null) {
                fromHour = DateUtils.formatDate(quandoModel.getOraDa(), "HH:mm");
            }
            if (quandoModel.getDataA() != null) {
                toDate = DateUtils.formatDate(quandoModel.getDataA(), "yyyy-MM-dd");
            }
            if (quandoModel.getOraA() != null) {
                toHour = DateUtils.formatDate(quandoModel.getOraA(), "HH:mm");
            }
        }
    }
}
