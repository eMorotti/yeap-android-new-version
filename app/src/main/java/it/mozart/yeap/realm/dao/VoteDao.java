package it.mozart.yeap.realm.dao;

import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.PollItem;
import it.mozart.yeap.realm.Vote;
import it.mozart.yeap.utility.Constants;

public class VoteDao {

    private Realm realm;

    public VoteDao(Realm realm) {
        this.realm = realm;
    }

    public Vote getVote(String uuid) {
        return realm.where(Vote.class).equalTo(Vote.UUID, uuid).findFirst();
    }

    public Vote getVoteEventFromUser(String eventoId, String userId) {
        return realm.where(Vote.class).equalTo(Vote.EVENT_ID, eventoId).equalTo(Vote.VOTE_TYPE, Constants.VOTE_TYPE_EVENT).equalTo(Vote.CREATOR_ID, userId).findFirst();
    }

    public Vote getVoteInterestEventFromUser(String eventoId, String userId) {
        return realm.where(Vote.class).equalTo(Vote.EVENT_ID, eventoId).equalTo(Vote.VOTE_TYPE, Constants.VOTE_TYPE_INTEREST).equalTo(Vote.CREATOR_ID, userId).findFirst();
    }

    public long getVoteEventYesCount(String eventoId) {
        return realm.where(Vote.class).equalTo(Vote.EVENT_ID, eventoId).equalTo(Vote.VOTE_TYPE, Constants.VOTE_TYPE_EVENT).equalTo(Vote.LOADING, false).equalTo(Vote.TYPE, Constants.VOTO_SI).equalTo(Vote.DELETED, false).count();
    }

    public long getVoteEventNoCount(String eventoId) {
        return realm.where(Vote.class).equalTo(Vote.EVENT_ID, eventoId).equalTo(Vote.VOTE_TYPE, Constants.VOTE_TYPE_EVENT).equalTo(Vote.LOADING, false).equalTo(Vote.TYPE, Constants.VOTO_NO).equalTo(Vote.DELETED, false).count();
    }

    public long getInterestEventYesCount(String eventoId) {
        return realm.where(Vote.class).equalTo(Vote.EVENT_ID, eventoId).equalTo(Vote.VOTE_TYPE, Constants.VOTE_TYPE_INTEREST).equalTo(Vote.LOADING, false).equalTo(Vote.TYPE, Constants.VOTO_INTERESSE).equalTo(Vote.DELETED, false).count();
    }

    public long getInterestEventNoCount(String eventoId) {
        return realm.where(Vote.class).equalTo(Vote.EVENT_ID, eventoId).equalTo(Vote.VOTE_TYPE, Constants.VOTE_TYPE_INTEREST).equalTo(Vote.LOADING, false).equalTo(Vote.TYPE, Constants.VOTO_NO_INTERESSE).equalTo(Vote.DELETED, false).count();
    }

    public Vote getVotePollItemFromUser(String voceId, String userId) {
        return realm.where(Vote.class).equalTo(Vote.POLLITEM_ID, voceId).equalTo(Vote.VOTE_TYPE, Constants.VOTE_TYPE_EVENT).equalTo(Vote.CREATOR_ID, userId).findFirst();
    }

    public long getVotePollItemYesCount(String voceId) {
        return realm.where(Vote.class).equalTo(Vote.POLLITEM_ID, voceId).equalTo(Vote.VOTE_TYPE, Constants.VOTE_TYPE_EVENT).equalTo(Vote.LOADING, false).equalTo(Vote.TYPE, Constants.VOTO_SI).equalTo(Vote.DELETED, false).count();
    }

    public RealmResults<Vote> getListVoteEventYes(String eventoId) {
        return realm.where(Vote.class).equalTo(Vote.EVENT_ID, eventoId).equalTo(Vote.VOTE_TYPE, Constants.VOTE_TYPE_EVENT).equalTo(Vote.LOADING, false).equalTo(Vote.TYPE, Constants.VOTO_SI).equalTo(Vote.DELETED, false).findAllSorted(Vote.CREATED_AT, Sort.DESCENDING);
    }

    public RealmResults<Vote> getListVoteEventNo(String eventoId) {
        return realm.where(Vote.class).equalTo(Vote.EVENT_ID, eventoId).equalTo(Vote.VOTE_TYPE, Constants.VOTE_TYPE_EVENT).equalTo(Vote.LOADING, false).equalTo(Vote.TYPE, Constants.VOTO_NO).equalTo(Vote.DELETED, false).findAllSorted(Vote.CREATED_AT, Sort.DESCENDING);
    }

    public RealmResults<Vote> getListVoteInterestEventYes(String eventoId) {
        return realm.where(Vote.class).equalTo(Vote.EVENT_ID, eventoId).equalTo(Vote.VOTE_TYPE, Constants.VOTE_TYPE_INTEREST).equalTo(Vote.LOADING, false).equalTo(Vote.TYPE, Constants.VOTO_INTERESSE).equalTo(Vote.DELETED, false).findAllSorted(Vote.CREATED_AT, Sort.DESCENDING);
    }

    public RealmResults<Vote> getListVoteInterestEventNo(String eventoId) {
        return realm.where(Vote.class).equalTo(Vote.EVENT_ID, eventoId).equalTo(Vote.VOTE_TYPE, Constants.VOTE_TYPE_INTEREST).equalTo(Vote.LOADING, false).equalTo(Vote.TYPE, Constants.VOTO_NO_INTERESSE).equalTo(Vote.DELETED, false).findAllSorted(Vote.CREATED_AT, Sort.DESCENDING);
    }

    public int getCountVoteInterestEventNo(String eventoId) {
        return (int) realm.where(Vote.class).equalTo(Vote.EVENT_ID, eventoId).equalTo(Vote.VOTE_TYPE, Constants.VOTE_TYPE_INTEREST).equalTo(Vote.LOADING, false).equalTo(Vote.TYPE, Constants.VOTO_NO_INTERESSE).equalTo(Vote.DELETED, false).count();
    }

    public RealmResults<Vote> getListVotePollItemYes(String voceId) {
        return realm.where(Vote.class).equalTo(Vote.POLLITEM_ID, voceId).equalTo(Vote.VOTE_TYPE, Constants.VOTE_TYPE_EVENT).equalTo(Vote.LOADING, false).equalTo(Vote.TYPE, Constants.VOTO_SI).equalTo(Vote.DELETED, false).findAll();
    }

    public RealmResults<Vote> getVoteNotSent() {
        return realm.where(Vote.class).equalTo(Vote.LOADING, true).equalTo(Vote.VOTE_TYPE, Constants.VOTE_TYPE_EVENT).findAll();
    }

    public RealmResults<Vote> getVoteInterestNotSent() {
        return realm.where(Vote.class).equalTo(Vote.LOADING, true).equalTo(Vote.VOTE_TYPE, Constants.VOTE_TYPE_INTEREST).findAll();
    }

    public Vote createVote() {
        return realm.createObject(Vote.class, UUID.randomUUID().toString());
    }

    public Vote createMyVoteEvent(Vote voto, Event event) {
        Vote newVoto = realm.copyToRealm(voto);
        if (event.getEventInfo() != null) {
            event.getEventInfo().setMyVote(newVoto);
        }
        return newVoto;
    }

    public Vote createMyVoteInterestEvent(Vote voto, Event event) {
        Vote newVoto = realm.copyToRealm(voto);
        if (event.getEventInfo() != null) {
            event.getEventInfo().setMyVoteInterest(newVoto);
        }
        return newVoto;
    }

    public Vote createMyVotePollItem(Vote voto, PollItem sondaggioVoce) {
        Vote newVoto = realm.copyToRealm(voto);
        sondaggioVoce.setMyVote(newVoto);
        return newVoto;
    }

    public void deleteAllVoteForEvent(String eventoId) {
        RealmResults<Vote> voti = realm.where(Vote.class).equalTo(Vote.EVENT_ID, eventoId).findAll();
        voti.deleteAllFromRealm();
    }

    public void deleteAllDeletedVoteForEvent(String eventoId) {
        RealmResults<Vote> voti = realm.where(Vote.class).equalTo(Vote.EVENT_ID, eventoId).equalTo(Vote.DELETED, true).findAll();
        voti.deleteAllFromRealm();
    }

    public void deleteAllVoteForPollItem(String pollItemId) {
        RealmResults<Vote> voti = realm.where(Vote.class).equalTo(Vote.POLLITEM_ID, pollItemId).findAll();
        voti.deleteAllFromRealm();
    }

    public void deleteAllDeletedVoteForPollItem(String pollItemId) {
        RealmResults<Vote> voti = realm.where(Vote.class).equalTo(Vote.POLLITEM_ID, pollItemId).equalTo(Vote.DELETED, true).findAll();
        voti.deleteAllFromRealm();
    }
}
