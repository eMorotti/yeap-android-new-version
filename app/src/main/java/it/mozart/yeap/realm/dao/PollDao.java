package it.mozart.yeap.realm.dao;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import it.mozart.yeap.realm.Poll;
import it.mozart.yeap.realm.PollItem;
import it.mozart.yeap.utility.Constants;

public class PollDao {

    private Realm realm;

    public PollDao(Realm realm) {
        this.realm = realm;
    }

    public Poll getPoll(String uuid) {
        return realm.where(Poll.class).equalTo(Poll.UUID, uuid).findFirst();
    }

    public PollItem getPollItem(String uuid) {
        return realm.where(PollItem.class).equalTo(PollItem.UUID, uuid).findFirst();
    }

    public RealmResults<Poll> getListPollsFromEvent(String eventoId) {
        return realm.where(Poll.class).equalTo(Poll.EVENT_ID, eventoId).equalTo(Poll.DELETED, false)
                .notEqualTo(Poll.TYPE, Constants.POLL_TYPE_WHERE).notEqualTo(Poll.TYPE, Constants.POLL_TYPE_WHEN).findAllSorted(Poll.CREATED_AT, Sort.DESCENDING);
    }

    public int getNumberOfPolls(String eventoId) {
        return (int) realm.where(Poll.class).equalTo(Poll.EVENT_ID, eventoId).notEqualTo(Poll.TYPE, Constants.POLL_TYPE_WHERE).notEqualTo(Poll.TYPE, Constants.POLL_TYPE_WHEN)
                .equalTo(Poll.DELETED, false).count();
    }

    public Poll createPoll(Poll sondaggio, boolean isSpecial, boolean letto, boolean isSyncTotal) {
        Poll newSondaggio = realm.copyToRealm(sondaggio);
        newSondaggio.setNuovo(!letto && !isSyncTotal);

        if (!isSyncTotal) {
            EventDao eventDao = new EventDao(realm);
            MessageDao messaggioDao = new MessageDao(realm);
            if (isSpecial) {
                messaggioDao.createSystemMessageNewPollSpecial(eventDao.getEvent(newSondaggio.getEventUuid()), newSondaggio, letto);
            } else {
                messaggioDao.createSystemMessageNewPoll(eventDao.getEvent(newSondaggio.getEventUuid()), newSondaggio, letto);
            }
        }

        return newSondaggio;
    }

    public PollItem createPollItem(PollItem voce, boolean isSpecial, boolean letto, boolean isSyncTotal) {
        PollItem newVoce = realm.copyToRealm(voce);
        newVoce.setNuovo(!letto && !isSyncTotal);

        Poll sondaggio = getPoll(newVoce.getPollUuid());
        sondaggio.getVoci().add(newVoce);

        if (!isSyncTotal) {
            EventDao eventDao = new EventDao(realm);
            MessageDao messaggioDao = new MessageDao(realm);
            if (isSpecial) {
                messaggioDao.createSystemMessageNewPollItemSpecial(eventDao.getEvent(sondaggio.getEventUuid()), newVoce, letto);
            } else {
                messaggioDao.createSystemMessageNewPollItem(eventDao.getEvent(sondaggio.getEventUuid()), newVoce, letto);
            }
        }

        return newVoce;
    }

    public void deletePoll(String pollId) {
        VoteDao voteDao = new VoteDao(realm);

        Poll sondaggio = getPoll(pollId);
        for (PollItem pollItem : sondaggio.getVoci()) {
            voteDao.deleteAllVoteForPollItem(pollItem.getUuid());
        }
        sondaggio.getVoci().deleteAllFromRealm();
        sondaggio.deleteFromRealm();
    }

    public void deletePollItem(String pollItemId) {
        VoteDao voteDao = new VoteDao(realm);

        PollItem sondaggioVoce = getPollItem(pollItemId);
        voteDao.deleteAllVoteForPollItem(sondaggioVoce.getUuid());
        sondaggioVoce.deleteFromRealm();
    }

    public void deleteAllPolls(String eventoId) {
        VoteDao voteDao = new VoteDao(realm);

        RealmResults<Poll> sondaggi = getListPollsFromEvent(eventoId);
        for (Poll poll : sondaggi) {
            for (PollItem pollItem : poll.getVoci()) {
                voteDao.deleteAllVoteForPollItem(pollItem.getUuid());
            }
            poll.getVoci().deleteAllFromRealm();
        }
        sondaggi.deleteAllFromRealm();
    }
}
