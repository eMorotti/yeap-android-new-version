package it.mozart.yeap.realm;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class Poll extends RealmObject {

    @PrimaryKey
    private String uuid;
    public static String UUID = "uuid";
    private long createdAt;
    public static String CREATED_AT = "createdAt";
    private boolean deleted;
    public static String DELETED = "deleted";
    private String eventUuid;
    public static String EVENT_ID = "eventUuid";
    @Index
    private int type;
    public static String TYPE = "type";
    private boolean usersCanAddItems;

    @Required
    private String question;
    private RealmList<PollItem> voci = new RealmList<>();

    private User creator;
    private String creatorUuid;

    @Ignore
    private List<PollItem> pollItems;

    private boolean nuovo;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getEventUuid() {
        return eventUuid;
    }

    public void setEventUuid(String eventUuid) {
        this.eventUuid = eventUuid;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isUsersCanAddItems() {
        return usersCanAddItems;
    }

    public void setUsersCanAddItems(boolean usersCanAddItems) {
        this.usersCanAddItems = usersCanAddItems;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public RealmList<PollItem> getVoci() {
        return voci;
    }

    public void setVoci(RealmList<PollItem> voci) {
        this.voci = voci;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
        if (creator == null) {
            setCreatorUuid(null);
        } else {
            setCreatorUuid(creator.getUuid());
        }
    }

    public String getCreatorUuid() {
        return creatorUuid;
    }

    public void setCreatorUuid(String creatorUuid) {
        this.creatorUuid = creatorUuid;
    }

    public List<PollItem> getPollItems() {
        return pollItems;
    }

    public void setPollItems(List<PollItem> pollItems) {
        this.pollItems = pollItems;
    }

    public boolean isNuovo() {
        return nuovo;
    }

    public void setNuovo(boolean nuovo) {
        this.nuovo = nuovo;
    }
}
