package it.mozart.yeap.realm.dao;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.Group;
import it.mozart.yeap.realm.User;

public class EventDao {

    private Realm realm;

    public EventDao(Realm realm) {
        this.realm = realm;
    }

    public Event getEvent(String uuid) {
        return realm.where(Event.class).equalTo(Event.UUID, uuid).findFirst();
    }

    public RealmResults<Event> getListEventsHome() {
        return realm.where(Event.class).findAllSorted(Event.UPDATED_AT, Sort.DESCENDING);
    }

    public RealmResults<Event> getListActivitiesWithMeInside() {
        return realm.where(Event.class).equalTo(Event.MYSELF_PRESENT, true).findAllSorted(Event.UPDATED_AT, Sort.DESCENDING);
    }

    public RealmResults<Event> getListEventsFromIdList(String[] ids) {
        RealmQuery<Event> query = realm.where(Event.class);
        // Creo la query
        if (ids.length > 1) {
            for (String id : ids) {
                query = query.or().equalTo(Event.UUID, id);
            }
        } else {
            query = query.equalTo(Event.UUID, ids[0]);
        }
        return query.findAll();
    }

    public RealmResults<Event> getListEventsFromGroup(String gruppoId) {
        return realm.where(Event.class).equalTo(Event.GROUP_ID, gruppoId).findAllSorted(Event.CREATED_AT);
    }

    public int getNumEventsFromGroup(String gruppoId) {
        return (int) realm.where(Event.class).equalTo(Event.GROUP_ID, gruppoId).count();
    }

    public long getNumberEventsWithUpdates() {
        return realm.where(Event.class)
                .notEqualTo(Event.NUM_NEW_MESSAGES, 0)
                .or()
                .notEqualTo(Event.NUM_INFO_UPDATES, 0)
                .or()
                .notEqualTo(Event.NUM_POLL_UPDATES, 0)
                .or()
                .notEqualTo(Event.NUM_USER_UPDATES, 0)
                .count();
    }

    public Event createEvent(Event evento, User creator, boolean isSyncTotal) {
        Group gruppo = new GroupDao(realm).getGroup(evento.getGroupUuid());
        if (gruppo != null) {
            evento.setGroup(gruppo);
        }

        Event newEvento = realm.copyToRealm(evento);
        MessageDao messageDao = new MessageDao(realm);
        messageDao.createSystemMessageNewEvent(newEvento, creator, isSyncTotal);

        if (newEvento.getGroup() != null) {
            messageDao.createSystemMessageNewEventInGroup(newEvento, isSyncTotal);
        }

        return newEvento;
    }

    void updateNumberMessages(String eventoId) {
        Event evento = getEvent(eventoId);
        if (evento == null)
            return;
        // Aggiorno numero nuovi messaggi
        long numNuoviMessaggi = new MessageDao(realm).getNumberNewMessagesEvent(eventoId);
        if (numNuoviMessaggi != evento.getNumNewMessages()) {
            evento.setNumNewMessages(numNuoviMessaggi);
        }
    }

    void updateNumberPollUpdates(String eventoId) {
        Event evento = getEvent(eventoId);
        if (evento == null)
            return;
        // Aggiorno numero aggiornamenti
        long numPollUpdates = new MessageDao(realm).getNumberPollUpdatesEvent(eventoId);
        if (numPollUpdates != evento.getNumPollUpdates()) {
            evento.setNumPollUpdates(numPollUpdates);
        }
    }

    void updateNumberUserUpdates(String eventoId) {
        Event evento = getEvent(eventoId);
        if (evento == null)
            return;
        // Aggiorno numero aggiornamenti
        long numUserUpdates = new MessageDao(realm).getNumberUserUpdatesEvent(eventoId);
        if (numUserUpdates != evento.getNumUserUpdates()) {
            evento.setNumUserUpdates(numUserUpdates);
        }
    }

    void updateNumberInfoUpdates(String eventoId) {
        Event evento = getEvent(eventoId);
        if (evento == null)
            return;
        // Aggiorno numero aggiornamenti
        long numNuoviMessaggi = new MessageDao(realm).getNumberInfoUpdatesEvent(eventoId);
        if (numNuoviMessaggi != evento.getNumInfoUpdates()) {
            evento.setNumInfoUpdates(numNuoviMessaggi);
        }
    }

    public void deleteEvent(String eventoId) {
        Event evento = getEvent(eventoId);
        if (evento == null)
            return;

        PollDao pollDao = new PollDao(realm);
        pollDao.deleteAllPolls(eventoId);

        VoteDao voteDao = new VoteDao(realm);
        voteDao.deleteAllVoteForEvent(eventoId);

        MessageDao messageDao = new MessageDao(realm);
        messageDao.deleteAllMessagesEvent(eventoId);

        evento.getEventInfo().deleteFromRealm();
        evento.deleteFromRealm();
    }
}
