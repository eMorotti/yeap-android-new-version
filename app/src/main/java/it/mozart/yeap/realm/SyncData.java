package it.mozart.yeap.realm;

import com.google.gson.JsonElement;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

public class SyncData extends RealmObject {

    @PrimaryKey
    private String id;
    public static String ID = "id";
    private int v;
    private int type;
    private int typology;
    private long timestamp;
    public static String CREATED_AT = "timestamp";

    private String requestId;
    @Ignore
    private JsonElement data;
    private String content;


    private boolean inElaborazione;
    private boolean elaborato;
    public static String DONE = "elaborato";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getV() {
        return v;
    }

    public void setV(int v) {
        this.v = v;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getTypology() {
        return typology;
    }

    public void setTypology(int typology) {
        this.typology = typology;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public JsonElement getData() {
        return data;
    }

    public void setData(JsonElement data) {
        this.data = data;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isInElaborazione() {
        return inElaborazione;
    }

    public void setInElaborazione(boolean inElaborazione) {
        this.inElaborazione = inElaborazione;
    }

    public boolean isElaborato() {
        return elaborato;
    }

    public void setElaborato(boolean elaborato) {
        this.elaborato = elaborato;
    }
}
