package it.mozart.yeap.realm.dao;

import com.google.gson.Gson;

import java.util.Date;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.common.MediaInfo;
import it.mozart.yeap.common.SystemMessage;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.Group;
import it.mozart.yeap.realm.Message;
import it.mozart.yeap.realm.Poll;
import it.mozart.yeap.realm.PollItem;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.Vote;
import it.mozart.yeap.services.realmJob.MarkAllAsReadJob;
import it.mozart.yeap.services.realmJob.MarkChatAsReadJob;
import it.mozart.yeap.services.realmJob.MarkInfoAsReadJob;
import it.mozart.yeap.services.realmJob.MarkPollAsReadJob;
import it.mozart.yeap.services.realmJob.MarkUsersAsReadJob;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.DateUtils;
import me.leolin.shortcutbadger.ShortcutBadger;

public class MessageDao {

    private Realm realm;

    public MessageDao(Realm realm) {
        this.realm = realm;
    }

    public Message getMessage(String id) {
        return realm.where(Message.class).equalTo(Message.UUID, id).findFirst();
    }

    public RealmResults<Message> getListMessagesEventChat(String eventoId) {
        RealmQuery<Message> query = realm.where(Message.class).equalTo(Message.TYPOLOGY, Constants.MESSAGGIO_TYPOLOGY_CHAT_EVENTO).equalTo(Message.EVENT_PARENT_ID, eventoId)
                .beginGroup()
                .isNotNull(Message.CREATOR)
                .or()
                .beginGroup()
                .isNull(Message.CREATOR)
                .equalTo(Message.TYPE, Constants.MESSAGGIO_TYPE_SYSTEM)
                .endGroup()
                .endGroup();
        query = filterMessages(query, true);

        return query.findAllSorted(Message.CREATED_AT_DATE_VISUALIZATION, Sort.DESCENDING);
    }

    public RealmResults<Message> getListMessagesGroupChat(String gruppoId) {
        RealmQuery<Message> query = realm.where(Message.class).equalTo(Message.TYPOLOGY, Constants.MESSAGGIO_TYPOLOGY_CHAT_GRUPPO).equalTo(Message.GROUP_PARENT_ID, gruppoId)
                .beginGroup()
                .isNotNull(Message.CREATOR)
                .or()
                .beginGroup()
                .isNull(Message.CREATOR)
                .equalTo(Message.TYPE, Constants.MESSAGGIO_TYPE_SYSTEM)
                .endGroup()
                .endGroup();
        query = filterMessages(query, false);

        return query.findAllSorted(Message.CREATED_AT, Sort.DESCENDING);
    }

    public RealmResults<Message> getListMessagesNotSent() {
        return realm.where(Message.class).equalTo(Message.TYPE, Constants.MESSAGGIO_TYPE_STANDARD)
                .beginGroup().isNotNull(Message.EVENT_PARENT_ID).or().isNotNull(Message.GROUP_PARENT_ID).endGroup()
                .equalTo(Message.SENT, false)
                .findAllSorted(Message.CREATED_AT, Sort.DESCENDING);
    }

    public int getDifferentEventsWithNotReadMessages() {
        return realm.where(Message.class).isNotNull(Message.EVENT_PARENT_ID).equalTo(Message.READ, false)
                .equalTo(Message.IGNORE_NOTIFICATION, false).distinct(Message.EVENT_PARENT_ID).size();
    }

    public RealmResults<Message> getNotReadMessagesForEvent() {
        return realm.where(Message.class).isNotNull(Message.EVENT_PARENT_ID).equalTo(Message.READ, false)
                .equalTo(Message.IGNORE_NOTIFICATION, false).findAllSorted(Message.CREATED_AT, Sort.DESCENDING);
    }

    public int getNumNotReadMessagedForEvent() {
        return (int) realm.where(Message.class).isNotNull(Message.EVENT_PARENT_ID).equalTo(Message.READ, false)
                .equalTo(Message.IGNORE_NOTIFICATION, false).count();
    }

    public int getDifferentGroupsWithNotReadMessages() {
        return realm.where(Message.class).isNotNull(Message.GROUP_PARENT_ID).equalTo(Message.READ, false)
                .equalTo(Message.IGNORE_NOTIFICATION, false).distinct(Message.GROUP_PARENT_ID).size();
    }

    public RealmResults<Message> getNotReadMessagesForGroup() {
        return realm.where(Message.class).isNotNull(Message.GROUP_PARENT_ID).equalTo(Message.READ, false)
                .equalTo(Message.IGNORE_NOTIFICATION, false).findAllSorted(Message.CREATED_AT, Sort.DESCENDING);
    }

    public int getNumNotReadMessagedForGroup() {
        return (int) realm.where(Message.class).isNotNull(Message.GROUP_PARENT_ID).equalTo(Message.READ, false)
                .equalTo(Message.IGNORE_NOTIFICATION, false).count();
    }

    long getNumberNewMessagesEvent(String eventoId) {
        return realm.where(Message.class).equalTo(Message.EVENT_PARENT_ID, eventoId).equalTo(Message.READ, false).equalTo(Message.IGNORE_READ, false)
                .beginGroup()
                .notEqualTo(Message.TYPE, Constants.MESSAGGIO_TYPE_SYSTEM)
                .or()
                .equalTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_CREAZIONE_EVENTO)
                .endGroup()
                .count();
    }

    long getNumberInfoUpdatesEvent(String eventoId) {
        RealmQuery<Message> query = realm.where(Message.class).equalTo(Message.EVENT_PARENT_ID, eventoId).equalTo(Message.READ, false);
        query = filterInfoMessages(query);

        return query.count();
    }

    long getNumberPollUpdatesEvent(String eventoId) {
        RealmQuery<Message> query = realm.where(Message.class).equalTo(Message.EVENT_PARENT_ID, eventoId).equalTo(Message.READ, false);
        query = filterPollMessages(query);

        return query.count();
    }

    long getNumberUserUpdatesEvent(String eventoId) {
        RealmQuery<Message> query = realm.where(Message.class).equalTo(Message.EVENT_PARENT_ID, eventoId).equalTo(Message.READ, false).equalTo(Message.IGNORE_READ, false);
        query = filterUserMessages(query);

        return query.count();
    }

    long getNumberNewMessagesGroup(String gruppoId) {
        return realm.where(Message.class).equalTo(Message.GROUP_PARENT_ID, gruppoId).equalTo(Message.READ, false).equalTo(Message.IGNORE_READ, false)
                .beginGroup()
                .notEqualTo(Message.TYPE, Constants.MESSAGGIO_TYPE_SYSTEM)
                .or()
                .equalTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_CREAZIONE_EVENTO)
                .or()
                .equalTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_CREAZIONE_GRUPPO)
                .endGroup()
                .count();
    }

    long getNumberUserUpdatesGroup(String gruppoId) {
        RealmQuery<Message> query = realm.where(Message.class).equalTo(Message.GROUP_PARENT_ID, gruppoId).equalTo(Message.READ, false);
        query = filterUserMessages(query);

        return query.count();
    }

    public Message createMessage(String testo, User creator, MediaInfo image, int typology, RealmObject object, boolean letto, boolean inviato) {
        return createMessage(UUID.randomUUID().toString(), testo, creator, image, typology, object, letto, inviato);
    }

    public Message createMessage(String messaggioUuid, String testo, User creator, MediaInfo image, int typology, RealmObject object, boolean letto, boolean inviato) {
        // Imposto informazioni base
        Message messaggio = realm.createObject(Message.class, messaggioUuid);
        messaggio.setText(testo != null ? testo.trim() : "");
        messaggio.setCreator(creator);
        messaggio.setCreatedAt(new Date().getTime());
        // Aggiungo immagine se presente
        if (image != null) {
            if (image.isExternal())
                messaggio.setMediaUrl(image.getMedia());
            else
                messaggio.setMediaLocalUrl(image.getMedia());
            messaggio.setThumbData(image.getThumbData());
            messaggio.setMediaWidth(image.getWidth());
            messaggio.setMediaHeight(image.getHeight());
            messaggio.setImageState(Constants.IMAGE_STATE_NOT_DOWNLOADED);
        }
        messaggio.setTypology(typology);
        messaggio.setType(Constants.MESSAGGIO_TYPE_STANDARD);
        messaggio.setSent(inviato);

        String parentId = "";
        // Imposto il parent corretto a seconda della tipologia del messaggio
        switch (typology) {
            case Constants.MESSAGGIO_TYPOLOGY_CHAT_EVENTO:
                parentId = ((Event) object).getUuid();
                messaggio.setEventParentUuid(parentId);
                ((Event) object).setUpdatedAt(new Date().getTime());
                // Aggiorno l'ultimo messaggio da visualizzare nella lista
                ((Event) object).setMessage(messaggio);
                break;
            case Constants.MESSAGGIO_TYPOLOGY_CHAT_GRUPPO:
                parentId = ((Group) object).getUuid();
                messaggio.setGroupParentUuid(parentId);
                ((Group) object).setUpdatedAt(new Date().getTime());
                // Aggiorno l'ultimo messaggio da visualizzare nella lista
                ((Group) object).setMessage(messaggio);
                break;
        }
        messaggio.setRead(letto);

        // Aggiorno il primo messaggio non letto
        updateFirstMessageNotRead(typology, parentId);
        // Aggiorno il numero di nuovi messaggi
        if (typology != Constants.MESSAGGIO_TYPOLOGY_CHAT_GRUPPO) {
            new EventDao(realm).updateNumberMessages(parentId);
        } else {
            new GroupDao(realm).updateNumberMessages(parentId);
        }

        return messaggio;
    }

    // BASE - System messages

    private Message createSystemMessageInGroup(Group gruppo, User creator, boolean letto, boolean ignoreUpdateEvent) {
        // Creo un nuovo messaggio
        Message messaggio = realm.createObject(Message.class, UUID.randomUUID().toString());
        messaggio.setCreatedAt(new Date().getTime());
        messaggio.setTypology(Constants.MESSAGGIO_TYPOLOGY_CHAT_GRUPPO);
        messaggio.setGroupParentUuid(gruppo.getUuid());
        messaggio.setType(Constants.MESSAGGIO_TYPE_SYSTEM);
        messaggio.setRead(letto);
        messaggio.setCreator(creator);
        messaggio.setIgnoreRead(true);
        messaggio.setIgnoreNotification(true);

        if (!ignoreUpdateEvent) {
            gruppo.setUpdatedAt(new Date().getTime());
            gruppo.setMessage(messaggio);
        }

        return messaggio;
    }

    private void updateNumMessagesGroup(Message messaggio, Group gruppo, boolean letto) {
        if (!letto) {
            updateFirstMessageNotRead(messaggio.getTypology(), gruppo.getUuid());
            GroupDao groupDao = new GroupDao(realm);
            groupDao.updateNumberMessages(gruppo.getUuid());
        }
    }

    private void updateNumUserUpdatesGroup(Group gruppo, boolean letto) {
        if (!letto) {
            GroupDao groupDao = new GroupDao(realm);
            groupDao.updateNumberUserUpdates(gruppo.getUuid());
        }
    }

    private Message createSystemMessageInEvent(Event evento, User creator, boolean letto, boolean ignoreUpdateEvent) {
        // Creo un nuovo messaggio
        Message messaggio = realm.createObject(Message.class, UUID.randomUUID().toString());
        messaggio.setCreatedAt(new Date().getTime());
        messaggio.setTypology(Constants.MESSAGGIO_TYPOLOGY_CHAT_EVENTO);
        messaggio.setEventParentUuid(evento.getUuid());
        messaggio.setType(Constants.MESSAGGIO_TYPE_SYSTEM);
        messaggio.setRead(letto);
        messaggio.setCreator(creator);
        messaggio.setIgnoreRead(true);
        messaggio.setIgnoreNotification(true);

        if (!ignoreUpdateEvent) {
            evento.setUpdatedAt(new Date().getTime());
            evento.setMessage(messaggio);
        }

        return messaggio;
    }

    private void updateNumMessagesEvent(Message messaggio, Event evento, boolean letto) {
        if (!letto) {
            updateFirstMessageNotRead(messaggio.getTypology(), evento.getUuid());
            EventDao eventDao = new EventDao(realm);
            eventDao.updateNumberMessages(evento.getUuid());
        }
    }

    private void updateNumUsersUpdatesEvent(Event evento, boolean letto) {
        if (!letto) {
            EventDao eventDao = new EventDao(realm);
            eventDao.updateNumberUserUpdates(evento.getUuid());
        }
    }

    private void updateNumInfoUpdatesEvent(Event evento, boolean letto) {
        if (!letto) {
            EventDao eventDao = new EventDao(realm);
            eventDao.updateNumberInfoUpdates(evento.getUuid());
        }
    }

    private void updateNumPollUpdatesEvent(Event evento, boolean letto) {
        if (!letto) {
            EventDao eventDao = new EventDao(realm);
            eventDao.updateNumberPollUpdates(evento.getUuid());
        }
    }

    // START - System messages

    void createSystemMessageNewGroup(Group gruppo, User creator, boolean isSyncTotal) {
        boolean letto = isMyself(creator) || isSyncTotal;

        Message messaggio = createSystemMessageInGroup(gruppo, creator, letto, !Constants.MESSAGGI_SHOW_CREAZIONE_GRUPPO_IN_HOME);
        messaggio.setSystemType(Constants.SYSTEM_MESSAGE_CREAZIONE_GRUPPO);
        messaggio.setIgnoreRead(false);
        messaggio.setCreatedAt(gruppo.getCreatedAt());
        messaggio.setIgnoreNotification(false);

        SystemMessage systemMessage = new SystemMessage();
        if (isMyself(creator)) {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_gruppo_creato_by_me), gruppo.getTitle()));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_gruppo_creato_by_me_system), gruppo.getTitle()));
        } else {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_gruppo_creato), creator.getChatName(), gruppo.getTitle()));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_gruppo_creato_system), gruppo.getTitle()));
        }
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        updateNumMessagesGroup(messaggio, gruppo, letto);
    }

    public Message createSystemMessageGroupUserAdded(Group gruppo, User maker, User utente, boolean letto) {
        if (!Constants.MESSAGGI_SHOW_AGGIUNTA_PARTECIPANTE) {
            letto = true;
        }
        Message messaggio = createSystemMessageInGroup(gruppo, maker, letto, !Constants.MESSAGGI_SHOW_AGGIUNTA_PARTECIPANTE_IN_HOME);
        messaggio.setSystemType(Constants.SYSTEM_MESSAGE_AGGIUNTO_PARTECIPANTE);
        messaggio.setIgnoreRead(false);
        messaggio.setIgnoreNotification(false);

        SystemMessage systemMessage = new SystemMessage();
        if (utente.getUuid().equals(AccountProvider.getInstance().getAccountId())) {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_nuovo_partecipante_me), maker.getChatName()));
            systemMessage.setLastMessage(App.getContext().getString(R.string.chat_messaggio_sistema_nuovo_partecipante_me_system));
        } else if (maker.getUuid().equals(AccountProvider.getInstance().getAccountId())) {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_nuovo_partecipante_by_me), utente.getChatName()));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_nuovo_partecipante_by_me_system), utente.getChatName()));
        } else {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_nuovo_partecipante), maker.getChatName(), utente.getChatName()));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_nuovo_partecipante_system), utente.getChatName()));
        }
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        updateNumMessagesGroup(messaggio, gruppo, letto);
        updateNumUserUpdatesGroup(gruppo, letto);

        return messaggio;
    }

    public Message createSystemMessageGroupInvitedAdded(Group gruppo, User maker, User utente, boolean letto) {
        if (!Constants.MESSAGGI_SHOW_AGGIUNTA_PARTECIPANTE) {
            letto = true;
        }
        Message messaggio = createSystemMessageInGroup(gruppo, maker, letto, !Constants.MESSAGGI_SHOW_AGGIUNTA_PARTECIPANTE_IN_HOME);
        messaggio.setSystemType(Constants.SYSTEM_MESSAGE_AGGIUNTO_PARTECIPANTE);
        messaggio.setIgnoreRead(false);
        messaggio.setIgnoreNotification(false);

        SystemMessage systemMessage = new SystemMessage();
        systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_nuovo_invitato_by_me), utente.getChatName()));
        systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_nuovo_invitato_by_me_system), utente.getChatName()));
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        updateNumMessagesGroup(messaggio, gruppo, letto);
        updateNumUserUpdatesGroup(gruppo, letto);

        return messaggio;
    }

    public Message createSystemMessageGroupUserRemoved(Group gruppo, User maker, User utente, boolean letto) {
        if (!Constants.MESSAGGI_SHOW_RIMOZIONE_PARTECIPANTE) {
            letto = true;
        }
        long currentTime = gruppo.getUpdatedAt();
        Message messaggio = createSystemMessageInGroup(gruppo, maker, letto, !Constants.MESSAGGI_SHOW_RIMOZIONE_PARTECIPANTE_IN_HOME);
        messaggio.setSystemType(Constants.SYSTEM_MESSAGE_RIMOSSO_PARTECIPANTE);

        SystemMessage systemMessage = new SystemMessage();
        if (utente.getUuid().equals(AccountProvider.getInstance().getAccountId())) {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_rimosso_partecipante_me), maker.getChatName()));
            systemMessage.setLastMessage(App.getContext().getString(R.string.chat_messaggio_sistema_rimosso_partecipante_me_system));
            messaggio.setIgnoreRead(false);
            messaggio.setIgnoreNotification(false);
        } else if (maker.getUuid().equals(AccountProvider.getInstance().getAccountId())) {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_rimosso_partecipante_by_me), utente.getChatName()));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_rimosso_partecipante_by_me_system), utente.getChatName()));
            gruppo.setUpdatedAt(currentTime);
        } else {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_rimosso_partecipante), maker.getChatName(), utente.getChatName()));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_rimosso_partecipante_system), utente.getChatName()));
            gruppo.setUpdatedAt(currentTime);
        }
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        updateNumMessagesGroup(messaggio, gruppo, letto);
        updateNumUserUpdatesGroup(gruppo, letto);

        return messaggio;
    }

    public Message createSystemMessageGroupInvitedRemoved(Group gruppo, User maker, User utente, boolean letto) {
        if (!Constants.MESSAGGI_SHOW_RIMOZIONE_PARTECIPANTE) {
            letto = true;
        }
        long currentTime = gruppo.getUpdatedAt();
        Message messaggio = createSystemMessageInGroup(gruppo, maker, letto, !Constants.MESSAGGI_SHOW_RIMOZIONE_PARTECIPANTE_IN_HOME);
        messaggio.setSystemType(Constants.SYSTEM_MESSAGE_RIMOSSO_PARTECIPANTE);

        SystemMessage systemMessage = new SystemMessage();
        systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_rimosso_partecipante_by_me), utente.getChatName()));
        systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_rimosso_partecipante_by_me_system), utente.getChatName()));
        gruppo.setUpdatedAt(currentTime);
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        updateNumMessagesGroup(messaggio, gruppo, letto);
        updateNumUserUpdatesGroup(gruppo, letto);

        return messaggio;
    }

    public Message createSystemMessageGroupUserLeft(Group gruppo, User utente, boolean letto) {
        if (!Constants.MESSAGGI_SHOW_ABBANDONA_GRUPPO) {
            letto = true;
        }
        boolean iAmCreator = isMyself(utente);

        long currenctTime = gruppo.getUpdatedAt();
        Message messaggio = createSystemMessageInGroup(gruppo, utente, letto, !Constants.MESSAGGI_SHOW_ABBANDONA_GRUPPO_IN_HOME);
        messaggio.setSystemType(Constants.SYSTEM_MESSAGE_ABBANDONATO_GRUPPO);
        gruppo.setUpdatedAt(currenctTime);

        SystemMessage systemMessage = new SystemMessage();
        if (iAmCreator) {
            systemMessage.setMessage(App.getContext().getString(R.string.chat_messaggio_sistema_uscita_partecipante_me));
        } else {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_uscita_partecipante), utente.getChatName()));
            systemMessage.setLastMessage(App.getContext().getString(R.string.chat_messaggio_sistema_uscita_partecipante_system));
        }
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        updateNumMessagesGroup(messaggio, gruppo, letto);

        return messaggio;
    }

    public Message createSystemMessageGroupNewAdmin(User admin, Group gruppo) {
        Message messaggio = createSystemMessageInGroup(gruppo, admin, App.isGroupVisible(gruppo.getUuid()), !Constants.MESSAGGI_SHOW_NUOVO_ADMIN_IN_HOME);
        messaggio.setSystemType(Constants.SYSTEM_MESSAGE_NUOVO_ADMIN);
        messaggio.setIgnoreRead(false);
        messaggio.setIgnoreNotification(false);

        SystemMessage systemMessage = new SystemMessage();
        if (admin.getUuid().equals(AccountProvider.getInstance().getAccountId())) {
            systemMessage.setMessage(App.getContext().getString(R.string.chat_messaggio_sistema_uscita_partecipante_nuovo_admin_me));
        } else {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_uscita_partecipante_nuovo_admin), admin.getChatName()));
            systemMessage.setLastMessage(App.getContext().getString(R.string.chat_messaggio_sistema_uscita_partecipante_nuovo_admin_system));
        }
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        updateNumMessagesGroup(messaggio, gruppo, App.isGroupVisible(gruppo.getUuid()));

        return messaggio;
    }

    void createSystemMessageNewEvent(Event evento, User creator, boolean isSyncTotal) {
        boolean letto = isMyself(creator) || isSyncTotal;

        Message messaggio = createSystemMessageInEvent(evento, creator, letto, !Constants.MESSAGGI_SHOW_CREAZIONE_EVENTO_IN_HOME);
        messaggio.setSystemType(Constants.SYSTEM_MESSAGE_CREAZIONE_EVENTO);
        messaggio.setIgnoreRead(false);
        messaggio.setCreatedAt(evento.getCreatedAt());
        messaggio.setIgnoreNotification(false);

        SystemMessage systemMessage = new SystemMessage();
        if (isMyself(creator)) {
            systemMessage.setMessage(App.getContext().getString(R.string.chat_messaggio_sistema_evento_creata_by_me));
            systemMessage.setLastMessage(App.getContext().getString(R.string.chat_messaggio_sistema_evento_creata_by_me_system));
        } else {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_evento_creata), creator.getChatName()));
            systemMessage.setLastMessage(App.getContext().getString(R.string.chat_messaggio_sistema_evento_creata_system));
        }
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        updateNumMessagesEvent(messaggio, evento, letto);
    }

    void createSystemMessageNewEventInGroup(Event evento, boolean isSyncTotal) {
        boolean iAmCreator = isMyself(evento.getCreator()) || isSyncTotal;

        Message messaggio = createSystemMessageInGroup(evento.getGroup(), evento.getCreator(), iAmCreator, !Constants.MESSAGGI_SHOW_GRUPPO_CREAZIONE_EVENTO_IN_HOME);
        messaggio.setCreatedAt(evento.getCreatedAt());
        messaggio.setSystemType(Constants.SYSTEM_MESSAGE_GRUPPO_CREAZIONE_EVENTO);

        SystemMessage systemMessage = new SystemMessage();
        if (isMyself(evento.getCreator())) {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_gruppo_evento_creata_by_me), evento.getTitle()));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_gruppo_evento_creata_by_me_system), evento.getTitle()));
        } else {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_gruppo_evento_creata), evento.getCreator().getChatName(), evento.getTitle()));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_gruppo_evento_creata_system), evento.getTitle()));
        }
        systemMessage.setClickId(evento.getUuid());
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        updateNumMessagesGroup(messaggio, evento.getGroup(), iAmCreator);
    }

    public Message createSystemMessageEventUserAdded(Event evento, User maker, User utente, boolean letto) {
        if (!Constants.MESSAGGI_SHOW_AGGIUNTA_PARTECIPANTE) {
            letto = true;
        }
        Message messaggio = createSystemMessageInEvent(evento, maker, letto, !Constants.MESSAGGI_SHOW_AGGIUNTA_PARTECIPANTE_IN_HOME);
        messaggio.setSystemType(Constants.SYSTEM_MESSAGE_AGGIUNTO_PARTECIPANTE);
        messaggio.setIgnoreRead(false);
        messaggio.setIgnoreNotification(false);

        SystemMessage systemMessage = new SystemMessage();
        if (utente.getUuid().equals(AccountProvider.getInstance().getAccountId())) {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_nuovo_partecipante_me), maker.getChatName()));
            systemMessage.setLastMessage(App.getContext().getString(R.string.chat_messaggio_sistema_nuovo_partecipante_me_system));
        } else if (maker.getUuid().equals(AccountProvider.getInstance().getAccountId())) {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_nuovo_partecipante_by_me), utente.getChatName()));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_nuovo_partecipante_by_me_system), utente.getChatName()));
        } else {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_nuovo_partecipante), maker.getChatName(), utente.getChatName()));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_nuovo_partecipante_system), utente.getChatName()));
        }
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        updateNumMessagesEvent(messaggio, evento, letto);
        updateNumUsersUpdatesEvent(evento, letto);

        return messaggio;
    }

    public Message createSystemMessageEventInvitedAdded(Event evento, User maker, User utente, boolean letto) {
        if (!Constants.MESSAGGI_SHOW_AGGIUNTA_PARTECIPANTE) {
            letto = true;
        }
        Message messaggio = createSystemMessageInEvent(evento, maker, letto, !Constants.MESSAGGI_SHOW_AGGIUNTA_PARTECIPANTE_IN_HOME);
        messaggio.setSystemType(Constants.SYSTEM_MESSAGE_AGGIUNTO_PARTECIPANTE);
        messaggio.setIgnoreRead(false);
        messaggio.setIgnoreNotification(false);

        SystemMessage systemMessage = new SystemMessage();
        systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_nuovo_invitato_by_me), utente.getChatName()));
        systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_nuovo_invitato_by_me_system), utente.getChatName()));
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        updateNumMessagesEvent(messaggio, evento, letto);
        updateNumUsersUpdatesEvent(evento, letto);

        return messaggio;
    }

    public Message createSystemMessageEventUserRemoved(Event evento, User maker, User utente, boolean letto) {
        if (!Constants.MESSAGGI_SHOW_RIMOZIONE_PARTECIPANTE) {
            letto = true;
        }
        long currentTime = evento.getUpdatedAt();
        Message messaggio = createSystemMessageInEvent(evento, maker, letto, !Constants.MESSAGGI_SHOW_RIMOZIONE_PARTECIPANTE_IN_HOME);
        messaggio.setSystemType(Constants.SYSTEM_MESSAGE_RIMOSSO_PARTECIPANTE);

        SystemMessage systemMessage = new SystemMessage();
        if (utente.getUuid().equals(AccountProvider.getInstance().getAccountId())) {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_rimosso_partecipante_me), maker.getChatName()));
            systemMessage.setLastMessage(App.getContext().getString(R.string.chat_messaggio_sistema_rimosso_partecipante_me_system));
            messaggio.setIgnoreRead(false);
            messaggio.setIgnoreNotification(false);
        } else if (maker.getUuid().equals(AccountProvider.getInstance().getAccountId())) {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_rimosso_partecipante_by_me), utente.getChatName()));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_rimosso_partecipante_by_me_system), utente.getChatName()));
            evento.setUpdatedAt(currentTime);
        } else {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_rimosso_partecipante), maker.getChatName(), utente.getChatName()));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_rimosso_partecipante_system), utente.getChatName()));
            evento.setUpdatedAt(currentTime);
        }
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        updateNumMessagesEvent(messaggio, evento, letto);
        updateNumUsersUpdatesEvent(evento, letto);

        return messaggio;
    }

    public Message createSystemMessageEventInvitedRemoved(Event evento, User maker, User utente, boolean letto) {
        if (!Constants.MESSAGGI_SHOW_RIMOZIONE_PARTECIPANTE) {
            letto = true;
        }
        long currentTime = evento.getUpdatedAt();
        Message messaggio = createSystemMessageInEvent(evento, maker, letto, !Constants.MESSAGGI_SHOW_RIMOZIONE_PARTECIPANTE_IN_HOME);
        messaggio.setSystemType(Constants.SYSTEM_MESSAGE_RIMOSSO_PARTECIPANTE);

        SystemMessage systemMessage = new SystemMessage();
        systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_rimosso_partecipante_by_me), utente.getChatName()));
        systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_rimosso_partecipante_by_me_system), utente.getChatName()));
        evento.setUpdatedAt(currentTime);
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        updateNumMessagesEvent(messaggio, evento, letto);
        updateNumUsersUpdatesEvent(evento, letto);

        return messaggio;
    }

    public Message createSystemMessageEventUserLeft(Event evento, User utente, boolean letto) {
        if (!Constants.MESSAGGI_SHOW_ABBANDONA_EVENTO) {
            letto = true;
        }
        boolean iAmCreator = isMyself(utente);

        long currenctTime = evento.getUpdatedAt();
        Message messaggio = createSystemMessageInEvent(evento, utente, letto, !Constants.MESSAGGI_SHOW_ABBANDONA_EVENTO_IN_HOME);
        messaggio.setSystemType(Constants.SYSTEM_MESSAGE_ABBANDONATO_EVENTO);
        evento.setUpdatedAt(currenctTime);

        SystemMessage systemMessage = new SystemMessage();
        if (iAmCreator) {
            systemMessage.setMessage(App.getContext().getString(R.string.chat_messaggio_sistema_uscita_partecipante_me));
        } else {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_uscita_partecipante), utente.getChatName()));
            systemMessage.setLastMessage(App.getContext().getString(R.string.chat_messaggio_sistema_uscita_partecipante_system));
        }
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        updateNumMessagesEvent(messaggio, evento, letto);

        return messaggio;
    }

    public Message createSystemMessageEventNewAdmin(User admin, Event evento) {
        Message messaggio = createSystemMessageInEvent(evento, admin, App.isEventVisible(evento.getUuid()), !Constants.MESSAGGI_SHOW_NUOVO_ADMIN_IN_HOME);
        messaggio.setSystemType(Constants.SYSTEM_MESSAGE_NUOVO_ADMIN);
        messaggio.setIgnoreRead(false);
        messaggio.setIgnoreNotification(false);

        SystemMessage systemMessage = new SystemMessage();
        if (admin.getUuid().equals(AccountProvider.getInstance().getAccountId())) {
            systemMessage.setMessage(App.getContext().getString(R.string.chat_messaggio_sistema_uscita_partecipante_nuovo_admin_me));
        } else {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_uscita_partecipante_nuovo_admin), admin.getChatName()));
            systemMessage.setLastMessage(App.getContext().getString(R.string.chat_messaggio_sistema_uscita_partecipante_nuovo_admin_system));
        }
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        return messaggio;
    }

    void createSystemMessageNewPoll(Event evento, Poll sondaggio, boolean letto) {
        if (!Constants.MESSAGGI_SHOW_CREAZIONE_SONDAGGIO_UPDATE) {
            letto = true;
        }
        boolean iAmCreator = isMyself(sondaggio.getCreator());

        Message messaggio = createSystemMessageInEvent(evento, sondaggio.getCreator(), letto, !Constants.MESSAGGI_SHOW_CREAZIONE_SONDAGGIO_IN_HOME);
        messaggio.setSystemType(Constants.SYSTEM_MESSAGE_CREAZIONE_SONDAGGIO);
        messaggio.setCreatedAt(sondaggio.getCreatedAt());
        messaggio.setPoll(sondaggio);
        messaggio.setIgnoreRead(false);
        messaggio.setIgnoreNotification(false);

        SystemMessage systemMessage = new SystemMessage();
        if (iAmCreator) {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_sondaggio_creazione_by_me), sondaggio.getQuestion()));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_sondaggio_creazione_by_me_system), sondaggio.getQuestion()));
        } else {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_sondaggio_creazione), sondaggio.getCreator().getChatName(), sondaggio.getQuestion()));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_sondaggio_creazione_system), sondaggio.getQuestion()));
        }
        systemMessage.setClickId(sondaggio.getUuid());
        systemMessage.setWhat(sondaggio.getQuestion());
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        updateNumPollUpdatesEvent(evento, letto);
    }

    void createSystemMessageNewPollSpecial(Event evento, Poll sondaggio, boolean letto) {
        if (!Constants.MESSAGGI_SHOW_CREAZIONE_SONDAGGIO_SPECIAL_UPDATE) {
            letto = true;
        }
        boolean iAmCreator = isMyself(sondaggio.getCreator());

        Message messaggio = createSystemMessageInEvent(evento, sondaggio.getCreator(), letto, !Constants.MESSAGGI_SHOW_CREAZIONE_SONDAGGIO_SPECIAL_IN_HOME);
        messaggio.setSystemType(Constants.SYSTEM_MESSAGE_CREAZIONE_SONDAGGIO_SPECIAL);
        messaggio.setCreatedAt(sondaggio.getCreatedAt());
        messaggio.setPoll(sondaggio);
        messaggio.setIgnoreRead(false);
        messaggio.setIgnoreNotification(false);

        SystemMessage systemMessage = new SystemMessage();
        if (iAmCreator) {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_sondaggio_creazione_by_me), sondaggio.getQuestion()));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_sondaggio_creazione_by_me_system), sondaggio.getQuestion()));
        } else {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_sondaggio_creazione), sondaggio.getCreator().getChatName(), sondaggio.getQuestion()));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_sondaggio_creazione_system), sondaggio.getQuestion()));
        }
        systemMessage.setClickId(sondaggio.getUuid());
        systemMessage.setWhat(sondaggio.getQuestion());
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        updateNumInfoUpdatesEvent(evento, letto);
    }

    void createSystemMessageNewPollItem(Event evento, PollItem sondaggioVoce, boolean letto) {
        if (!Constants.MESSAGGI_SHOW_CREAZIONE_VOCE_SONDAGGIO_UPDATE) {
            letto = true;
        }
        boolean iAmCreator = isMyself(sondaggioVoce.getCreator());

        Message messaggio = createSystemMessageInEvent(evento, sondaggioVoce.getCreator(), letto, !Constants.MESSAGGI_SHOW_CREAZIONE_VOCE_SONDAGGIO_IN_HOME);
        messaggio.setSystemType(Constants.SYSTEM_MESSAGE_CREAZIONE_VOCE_SONDAGGIO);
        messaggio.setCreatedAt(sondaggioVoce.getCreatedAt());
        messaggio.setPollItem(sondaggioVoce);
        messaggio.setIgnoreRead(false);
        messaggio.setIgnoreNotification(false);

        SystemMessage systemMessage = new SystemMessage();
        if (iAmCreator) {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_voce_sondaggio_creazione_by_me), sondaggioVoce.getWhat()));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_voce_sondaggio_creazione_by_me_system), sondaggioVoce.getWhat()));
        } else {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_voce_sondaggio_creazione), sondaggioVoce.getCreator().getChatName(), sondaggioVoce.getWhat()));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_voce_sondaggio_creazione_system), sondaggioVoce.getWhat()));
        }
        systemMessage.setClickIdParent(sondaggioVoce.getPollUuid());
        systemMessage.setClickId(sondaggioVoce.getUuid());
        if (sondaggioVoce.getWhere() != null) {
            systemMessage.setWhat(sondaggioVoce.getWhere());
        } else if (sondaggioVoce.isQuandoPresent()) {
            systemMessage.setWhat(DateUtils.createQuandoText(sondaggioVoce.generateQuandoModel()));
        } else {
            systemMessage.setWhat(sondaggioVoce.getWhat());
        }
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        updateNumPollUpdatesEvent(evento, letto);
    }

    void createSystemMessageNewPollItemSpecial(Event evento, PollItem sondaggioVoce, boolean letto) {
        if (!Constants.MESSAGGI_SHOW_CREAZIONE_VOCE_SONDAGGIO_SPECIAL_UPDATE) {
            letto = true;
        }
        boolean iAmCreator = isMyself(sondaggioVoce.getCreator());

        Message messaggio = createSystemMessageInEvent(evento, sondaggioVoce.getCreator(), letto, !Constants.MESSAGGI_SHOW_CREAZIONE_VOCE_SONDAGGIO_SPECIAL_IN_HOME);
        messaggio.setSystemType(Constants.SYSTEM_MESSAGE_CREAZIONE_VOCE_SONDAGGIO_SPECIAL);
        messaggio.setCreatedAt(sondaggioVoce.getCreatedAt());
        messaggio.setPollItem(sondaggioVoce);
        messaggio.setIgnoreRead(false);
        messaggio.setIgnoreNotification(false);

        SystemMessage systemMessage = new SystemMessage();
        if (iAmCreator) {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_voce_sondaggio_creazione_by_me), sondaggioVoce.getWhat()));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_voce_sondaggio_creazione_by_me_system), sondaggioVoce.getWhat()));
        } else {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_voce_sondaggio_creazione), sondaggioVoce.getCreator().getChatName(), sondaggioVoce.getWhat()));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_voce_sondaggio_creazione_system), sondaggioVoce.getWhat()));
        }
        systemMessage.setClickIdParent(sondaggioVoce.getPollUuid());
        systemMessage.setClickId(sondaggioVoce.getUuid());
        if (sondaggioVoce.getWhere() != null) {
            systemMessage.setWhat(sondaggioVoce.getWhere());
        } else if (sondaggioVoce.isQuandoPresent()) {
            systemMessage.setWhat(DateUtils.createQuandoText(sondaggioVoce.generateQuandoModel()));
        } else {
            systemMessage.setWhat(sondaggioVoce.getWhat());
        }
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        updateNumInfoUpdatesEvent(evento, letto);
    }

    public Message createSystemMessagePollOpened(Event evento, User maker, String what, boolean letto) {
        if (!Constants.MESSAGGI_SHOW_APERTURA_SONDAGGIO) {
            letto = true;
        }
        boolean iAmCreator = isMyself(maker);

        Message messaggio = createSystemMessageInEvent(evento, maker, letto, !Constants.MESSAGGI_SHOW_APERTURA_SONDAGGIO_IN_HOME);
        messaggio.setSystemType(Constants.SYSTEM_MESSAGE_APERTURA_SONDAGGIO);
        messaggio.setIgnoreRead(false);
        messaggio.setIgnoreNotification(false);

        SystemMessage systemMessage = new SystemMessage();
        if (iAmCreator) {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_sondaggio_aperto_by_me), what));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_sondaggio_aperto_by_me_system), what));
        } else {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_sondaggio_aperto), maker.getChatName(), what));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_sondaggio_aperto_system), what));
        }
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        updateNumInfoUpdatesEvent(evento, letto);

        return messaggio;
    }

    public Message createSystemMessagePollClosed(Event evento, User maker, String what, boolean letto) {
        if (!Constants.MESSAGGI_SHOW_CHIUSURA_SONDAGGIO) {
            letto = true;
        }
        boolean iAmCreator = isMyself(maker);

        Message messaggio = createSystemMessageInEvent(evento, maker, letto, !Constants.MESSAGGI_SHOW_CHIUSURA_SONDAGGIO_IN_HOME);
        messaggio.setSystemType(Constants.SYSTEM_MESSAGE_CHIUSURA_SONDAGGIO);
        messaggio.setIgnoreRead(false);
        messaggio.setIgnoreNotification(false);

        SystemMessage systemMessage = new SystemMessage();
        if (iAmCreator) {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_sondaggio_chiuso_by_me), what));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_sondaggio_chiuso_by_me_system), what));
        } else {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_sondaggio_chiuso), maker.getChatName(), what));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_sondaggio_chiuso_system), what));
        }
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        updateNumInfoUpdatesEvent(evento, letto);

        return messaggio;
    }

    public Message createSystemMessagePollDeleted(Event evento, Poll sondaggio, boolean letto) {
        if (!Constants.MESSAGGI_SHOW_ELIMINAZIONE_SONDAGGIO) {
            letto = true;
        }
        boolean iAmCreator = isMyself(sondaggio.getCreator());

        Message messaggio = createSystemMessageInEvent(evento, sondaggio.getCreator(), letto, !Constants.MESSAGGI_SHOW_ELIMINAZIONE_SONDAGGIO_IN_HOME);
        messaggio.setSystemType(Constants.SYSTEM_MESSAGE_ELIMINAZIONE_SONDAGGIO);
        messaggio.setIgnoreRead(false);
        messaggio.setIgnoreNotification(true);

        SystemMessage systemMessage = new SystemMessage();
        if (iAmCreator) {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_sondaggio_eliminato_by_me), sondaggio.getQuestion()));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_sondaggio_eliminato_by_me_system), sondaggio.getQuestion()));
        } else {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_sondaggio_eliminato), sondaggio.getCreator().getChatName(), sondaggio.getQuestion()));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_sondaggio_eliminato_system), sondaggio.getQuestion()));
        }
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        updateNumPollUpdatesEvent(evento, letto);

        return messaggio;
    }

    public Message createSystemMessagePollItemDeleted(Event evento, PollItem sondaggioVoce, boolean letto) {
        if (!Constants.MESSAGGI_SHOW_ELIMINAZIONE_VOCE_SONDAGGIO) {
            letto = true;
        }
        boolean iAmCreator = isMyself(sondaggioVoce.getCreator());

        Message messaggio = createSystemMessageInEvent(evento, sondaggioVoce.getCreator(), letto, !Constants.MESSAGGI_SHOW_ELIMINAZIONE_VOCE_SONDAGGIO_IN_HOME);
        messaggio.setSystemType(Constants.SYSTEM_MESSAGE_ELIMINAZIONE_VOCE_SONDAGGIO);
        messaggio.setIgnoreRead(false);
        messaggio.setIgnoreNotification(true);

        SystemMessage systemMessage = new SystemMessage();
        if (iAmCreator) {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_voce_sondaggio_eliminato_by_me), sondaggioVoce.getWhat()));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_voce_sondaggio_eliminato_by_me_system), sondaggioVoce.getWhat()));
        } else {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_voce_sondaggio_eliminato), sondaggioVoce.getCreator().getChatName(), sondaggioVoce.getWhat()));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_voce_sondaggio_eliminato_system), sondaggioVoce.getWhat()));
        }
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        updateNumPollUpdatesEvent(evento, letto);

        return messaggio;
    }

    public Message createSystemMessageGroupTitleUpdated(Group gruppo, User creator, boolean letto) {
        boolean iAmCreator = isMyself(creator);

        Message messaggio = createSystemMessageInGroup(gruppo, creator, letto, false);
        messaggio.setIgnoreRead(false);
        messaggio.setIgnoreNotification(true);

        SystemMessage systemMessage = new SystemMessage();
        if (iAmCreator) {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_gruppo_modificato_titolo_by_me), gruppo.getTitle()));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_gruppo_modificato_titolo_by_me_system), gruppo.getTitle()));
        } else {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_gruppo_modificato_titolo), creator.getChatName(), gruppo.getTitle()));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_gruppo_modificato_titolo_system), gruppo.getTitle()));
        }
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        updateNumMessagesGroup(messaggio, gruppo, letto);

        return messaggio;
    }

    public Message createSystemMessageEventChangeStatus(Event evento, User utente, boolean letto) {
        boolean iAmCreator = isMyself(utente);

        boolean showInHome;
        switch (evento.getStatus()) {
            case Constants.EVENTO_CONFERMATO:
                if (!Constants.MESSAGGI_SHOW_CONFERMA_EVENTO) {
                    letto = true;
                }
                showInHome = !Constants.MESSAGGI_SHOW_CONFERMA_EVENTO_IN_HOME;
                break;
            case Constants.EVENTO_ANNULLATO:
                if (!Constants.MESSAGGI_SHOW_ANNULLA_EVENTO) {
                    letto = true;
                }
                showInHome = !Constants.MESSAGGI_SHOW_ANNULLA_EVENTO_IN_HOME;
                break;
            default:
                if (!Constants.MESSAGGI_SHOW_PIANIFICA_EVENTO) {
                    letto = true;
                }
                showInHome = !Constants.MESSAGGI_SHOW_PIANIFICA_EVENTO_IN_HOME;
                break;
        }
        Message messaggio = createSystemMessageInEvent(evento, utente, letto, showInHome);
        messaggio.setIgnoreRead(false);
        messaggio.setIgnoreNotification(false);

        SystemMessage systemMessage = new SystemMessage();
        switch (evento.getStatus()) {
            case Constants.EVENTO_CONFERMATO:
                messaggio.setSystemType(Constants.SYSTEM_MESSAGE_CONFERMATO_EVENTO);
                if (iAmCreator) {
                    systemMessage.setMessage(App.getContext().getString(R.string.chat_messaggio_sistema_cambio_stato_confermata_by_me));
                } else {
                    systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_cambio_stato_confermata), utente.getChatName()));
                    systemMessage.setLastMessage(App.getContext().getString(R.string.chat_messaggio_sistema_cambio_stato_confermata_system));
                }
                break;
            case Constants.EVENTO_ANNULLATO:
                messaggio.setSystemType(Constants.SYSTEM_MESSAGE_ANNULLATO_EVENTO);
                if (iAmCreator) {
                    systemMessage.setMessage(App.getContext().getString(R.string.chat_messaggio_sistema_cambio_stato_annullata_by_me));
                } else {
                    systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_cambio_stato_annullata), utente.getChatName()));
                    systemMessage.setLastMessage(App.getContext().getString(R.string.chat_messaggio_sistema_cambio_stato_annullata_system));
                }
                break;
            default:
                messaggio.setSystemType(Constants.SYSTEM_MESSAGE_PIANIFICATA_EVENTO);
                if (iAmCreator) {
                    systemMessage.setMessage(App.getContext().getString(R.string.chat_messaggio_sistema_cambio_stato_pianificazione_by_me));
                } else {
                    systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_cambio_stato_pianificazione), utente.getChatName()));
                    systemMessage.setLastMessage(App.getContext().getString(R.string.chat_messaggio_sistema_cambio_stato_pianificazione_system));
                }
                break;
        }
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        updateNumMessagesEvent(messaggio, evento, letto);

        return messaggio;
    }

    private Message isMessageEventModifiedAlreadyPresent(Event event) {
        return realm.where(Message.class).equalTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_MODIFICA_EVENTO).equalTo(Message.READ, false)
                .equalTo(Message.EVENT_PARENT_ID, event.getUuid()).findFirst();
    }

    public Message createSystemMessageEventInfoModified(Event evento, User utente) {
        boolean iAmCreator = isMyself(utente);

        Message messaggio = createSystemMessageInEvent(evento, utente, true, !Constants.MESSAGGI_SHOW_MODIFICA_EVENTO_IN_HOME);
        messaggio.setSystemType(Constants.SYSTEM_MESSAGE_MODIFICA_EVENTO);

        SystemMessage systemMessage = new SystemMessage();
        if (iAmCreator) {
            systemMessage.setMessage(App.getContext().getString(R.string.chat_messaggio_sistema_evento_immagine_modificata_by_me));
        } else {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_evento_immagine_modificata), utente.getChatName()));
            systemMessage.setLastMessage(App.getContext().getString(R.string.chat_messaggio_sistema_evento_immagine_modificata_system));
        }
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        return messaggio;
    }

    public Message createSystemMessageEventTwoInfoModified(Event evento, User utente, String info1, String info2, boolean letto) {
        if (!Constants.MESSAGGI_SHOW_MODIFICA_EVENTO) {
            letto = true;
        }
        Message lastMessage = isMessageEventModifiedAlreadyPresent(evento);
        if (lastMessage != null) {
            lastMessage.setRead(true);
            lastMessage.setFirstNotRead(false);
        }

        boolean iAmCreator = isMyself(utente);

        Message messaggio = createSystemMessageInEvent(evento, utente, letto, !Constants.MESSAGGI_SHOW_MODIFICA_EVENTO_IN_HOME);
        messaggio.setSystemType(Constants.SYSTEM_MESSAGE_MODIFICA_EVENTO);
        messaggio.setIgnoreNotification(false);

        SystemMessage systemMessage = new SystemMessage();
        if (iAmCreator) {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_evento_modificato_two_by_me), info1, info2));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_evento_modificato_two_by_me_system), info1, info2));
        } else {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_evento_modificato_two), utente.getChatName(), info1, info2));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_evento_modificato_two_system), info1, info2));
        }
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        updateNumInfoUpdatesEvent(evento, letto);

        return messaggio;
    }

    public Message createSystemMessageEventAllInfoModified(Event evento, User utente, String info1, String info2, String info3, boolean letto) {
        if (!Constants.MESSAGGI_SHOW_MODIFICA_EVENTO) {
            letto = true;
        }
        Message lastMessage = isMessageEventModifiedAlreadyPresent(evento);
        if (lastMessage != null) {
            lastMessage.setRead(true);
            lastMessage.setFirstNotRead(false);
        }

        boolean iAmCreator = isMyself(utente);

        Message messaggio = createSystemMessageInEvent(evento, utente, letto, !Constants.MESSAGGI_SHOW_MODIFICA_EVENTO_IN_HOME);
        messaggio.setSystemType(Constants.SYSTEM_MESSAGE_MODIFICA_EVENTO);
        messaggio.setIgnoreNotification(false);

        SystemMessage systemMessage = new SystemMessage();
        if (iAmCreator) {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_evento_modificato_all_by_me), info1, info2, info3));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_evento_modificato_all_by_me_system), info1, info2, info3));
        } else {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_evento_modificato_all), utente.getChatName(), info1, info2, info3));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_evento_modificato_all_system), info1, info2, info3));
        }
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        updateNumInfoUpdatesEvent(evento, letto);

        return messaggio;
    }

    public Message createSystemMessageEventInfoModifiedWhat(Event evento, User utente, boolean letto) {
        if (!Constants.MESSAGGI_SHOW_MODIFICA_EVENTO) {
            letto = true;
        }
        Message lastMessage = isMessageEventModifiedAlreadyPresent(evento);
        if (lastMessage != null) {
            lastMessage.setRead(true);
            lastMessage.setFirstNotRead(false);
        }

        boolean iAmCreator = isMyself(utente);

        Message messaggio = createSystemMessageInEvent(evento, utente, letto, !Constants.MESSAGGI_SHOW_MODIFICA_EVENTO_IN_HOME);
        messaggio.setSystemType(Constants.SYSTEM_MESSAGE_MODIFICA_EVENTO);
        messaggio.setIgnoreNotification(false);

        SystemMessage systemMessage = new SystemMessage();
        if (iAmCreator) {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_evento_modificato_cosa_by_me), evento.getEventInfo().getWhat()));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_evento_modificato_cosa_by_me_system), evento.getEventInfo().getWhat()));
        } else {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_evento_modificato_cosa), utente.getChatName(), evento.getEventInfo().getWhat()));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_evento_modificato_cosa_system), evento.getEventInfo().getWhat()));
        }
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        updateNumInfoUpdatesEvent(evento, letto);

        return messaggio;
    }

    public Message createSystemMessageEventInfoModifiedWhere(Event evento, User utente, boolean letto) {
        if (!Constants.MESSAGGI_SHOW_MODIFICA_EVENTO) {
            letto = true;
        }
        Message lastMessage = isMessageEventModifiedAlreadyPresent(evento);
        if (lastMessage != null) {
            lastMessage.setRead(true);
            lastMessage.setFirstNotRead(false);
        }

        boolean iAmCreator = isMyself(utente);

        Message messaggio = createSystemMessageInEvent(evento, utente, letto, !Constants.MESSAGGI_SHOW_MODIFICA_EVENTO_IN_HOME);
        messaggio.setSystemType(Constants.SYSTEM_MESSAGE_MODIFICA_EVENTO);
        messaggio.setIgnoreNotification(false);

        SystemMessage systemMessage = new SystemMessage();
        if (iAmCreator) {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_evento_modificato_dove_by_me), evento.getEventInfo().getWhere()));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_evento_modificato_dove_by_me_system), evento.getEventInfo().getWhere()));
        } else {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_evento_modificato_dove), utente.getChatName(), evento.getEventInfo().getWhere()));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_evento_modificato_dove_system), evento.getEventInfo().getWhere()));
        }
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        updateNumInfoUpdatesEvent(evento, letto);

        return messaggio;
    }

    public Message createSystemMessageEventInfoModifiedWhen(Event evento, User utente, boolean letto) {
        if (!Constants.MESSAGGI_SHOW_MODIFICA_EVENTO) {
            letto = true;
        }
        Message lastMessage = isMessageEventModifiedAlreadyPresent(evento);
        if (lastMessage != null) {
            lastMessage.setRead(true);
            lastMessage.setFirstNotRead(false);
        }

        boolean iAmCreator = isMyself(utente);

        Message messaggio = createSystemMessageInEvent(evento, utente, letto, !Constants.MESSAGGI_SHOW_MODIFICA_EVENTO_IN_HOME);
        messaggio.setSystemType(Constants.SYSTEM_MESSAGE_MODIFICA_EVENTO);
        messaggio.setIgnoreNotification(false);

        SystemMessage systemMessage = new SystemMessage();
        String quando = DateUtils.createQuandoText(evento.getEventInfo().generateQuandoModel());
        if (iAmCreator) {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_evento_modificato_quando_by_me), quando));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_evento_modificato_quando_by_me_system), quando));
        } else {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_evento_modificato_quando), utente.getChatName(), quando));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_evento_modificato_quando_system), quando));
        }
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        updateNumInfoUpdatesEvent(evento, letto);

        return messaggio;
    }

    public Message createSystemMessageEventVote(Event evento, Vote voto, User creator, boolean letto) { //TODO aggiungere interesse
        boolean iAmCreator = isMyself(creator);
        if (!Constants.MESSAGGI_SHOW_VOTO_EVENTO) {
            letto = true;
        }

        RealmResults<Message> messaggi = getListMessagesEventChat(evento.getUuid());
        Message messaggio;
        if (messaggi.size() == 0) {
            messaggio = createSystemMessageInEvent(evento, creator, letto, true);
            messaggio.setSystemType(Constants.SYSTEM_MESSAGE_VOTO_EVENTO);
        } else {
            Message lastMessage = messaggi.get(0);
            if (lastMessage.getSystemType() == Constants.SYSTEM_MESSAGE_VOTO_EVENTO && lastMessage.getCreatorUuid().equals(creator.getUuid())) {
                messaggio = lastMessage;
                messaggio.setRead(letto);
                messaggio.setCreatedAt(new Date().getTime());
            } else {
                messaggio = createSystemMessageInEvent(evento, creator, letto, true);
                messaggio.setSystemType(Constants.SYSTEM_MESSAGE_VOTO_EVENTO);
            }
        }
        messaggio.setIgnoreRead(!Constants.MESSAGGI_SHOW_VOTO_EVENTO);
        messaggio.setIgnoreNotification(!Constants.MESSAGGI_SHOW_VOTO_EVENTO);

        String votoText = voto.getType() == Constants.VOTO_SI ? App.getContext().getString(R.string.chat_voto_si) : App.getContext().getString(R.string.chat_voto_no);

        SystemMessage systemMessage = new SystemMessage();
        if (iAmCreator) {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_voto_evento_by_me), votoText));
        } else {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_voto_evento), creator.getChatName(), votoText));
            systemMessage.setLastMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_voto_evento_system), votoText));
        }
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        if (Constants.MESSAGGI_SHOW_VOTO_EVENTO) {
            updateNumMessagesEvent(messaggio, evento, letto);
        }

        return messaggio;
    }

    public Message createSystemMessageEventVoteInterest(Event evento, Vote voto, User creator, boolean letto) { //TODO aggiungere interesse
        boolean iAmCreator = isMyself(creator);
        if (!Constants.MESSAGGI_SHOW_VOTO_INTERESSE_EVENTO) {
            letto = true;
        }

        RealmResults<Message> messaggi = getListMessagesEventChat(evento.getUuid());
        Message messaggio;
        if (messaggi.size() == 0) {
            messaggio = createSystemMessageInEvent(evento, creator, letto, true);
            messaggio.setSystemType(Constants.SYSTEM_MESSAGE_VOTO_EVENTO_INTERESSE);
        } else {
            Message lastMessage = messaggi.get(0);
            if (lastMessage.getSystemType() == Constants.SYSTEM_MESSAGE_VOTO_EVENTO_INTERESSE && lastMessage.getCreatorUuid().equals(creator.getUuid())) {
                messaggio = lastMessage;
                messaggio.setRead(letto);
                messaggio.setCreatedAt(new Date().getTime());
            } else {
                messaggio = createSystemMessageInEvent(evento, creator, letto, true);
                messaggio.setSystemType(Constants.SYSTEM_MESSAGE_VOTO_EVENTO_INTERESSE);
            }
        }
        messaggio.setIgnoreRead(!Constants.MESSAGGI_SHOW_VOTO_INTERESSE_EVENTO);
        messaggio.setIgnoreNotification(!Constants.MESSAGGI_SHOW_VOTO_INTERESSE_EVENTO);

        SystemMessage systemMessage = new SystemMessage();
        if (iAmCreator) {
            systemMessage.setMessage(App.getContext().getString(R.string.chat_messaggio_sistema_voto_evento_interesse_by_me));
        } else {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_voto_evento_interesse), creator.getChatName()));
            systemMessage.setLastMessage(App.getContext().getString(R.string.chat_messaggio_sistema_voto_evento_interesse_system));
        }
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        if (Constants.MESSAGGI_SHOW_VOTO_INTERESSE_EVENTO) {
            updateNumMessagesEvent(messaggio, evento, letto);
        }

        return messaggio;
    }

    public Message createSystemMessageEventVoteNoInterest(Event evento, Vote voto, User creator, boolean letto) { //TODO aggiungere interesse
        boolean iAmCreator = isMyself(creator);
        if (!Constants.MESSAGGI_SHOW_VOTO_INTERESSE_EVENTO) {
            letto = true;
        }

        RealmResults<Message> messaggi = getListMessagesEventChat(evento.getUuid());
        Message messaggio;
        if (messaggi.size() == 0) {
            messaggio = createSystemMessageInEvent(evento, creator, letto, true);
            messaggio.setSystemType(Constants.SYSTEM_MESSAGE_VOTO_EVENTO_INTERESSE);
        } else {
            Message lastMessage = messaggi.get(0);
            if (lastMessage.getSystemType() == Constants.SYSTEM_MESSAGE_VOTO_EVENTO_INTERESSE && lastMessage.getCreatorUuid().equals(creator.getUuid())) {
                messaggio = lastMessage;
                messaggio.setRead(letto);
                messaggio.setCreatedAt(new Date().getTime());
            } else {
                messaggio = createSystemMessageInEvent(evento, creator, letto, true);
                messaggio.setSystemType(Constants.SYSTEM_MESSAGE_VOTO_EVENTO_INTERESSE);
            }
        }
        messaggio.setIgnoreRead(!Constants.MESSAGGI_SHOW_VOTO_INTERESSE_EVENTO);
        messaggio.setIgnoreNotification(!Constants.MESSAGGI_SHOW_VOTO_INTERESSE_EVENTO);

        SystemMessage systemMessage = new SystemMessage();
        if (iAmCreator) {
            systemMessage.setMessage(App.getContext().getString(R.string.chat_messaggio_sistema_voto_evento_no_interesse_by_me));
        } else {
            systemMessage.setMessage(String.format(App.getContext().getString(R.string.chat_messaggio_sistema_voto_evento_no_interesse), creator.getChatName()));
            systemMessage.setLastMessage(App.getContext().getString(R.string.chat_messaggio_sistema_voto_evento_no_interesse_system));
        }
        messaggio.setSystemTextJson(new Gson().toJson(systemMessage, SystemMessage.class));

        if (Constants.MESSAGGI_SHOW_VOTO_INTERESSE_EVENTO) {
            updateNumMessagesEvent(messaggio, evento, letto);
        }

        return messaggio;
    }

    // END - System messages

    public void markAllAsReadAsync(boolean isEvent, String uuid) {
        App.jobManager().addJobInBackground(new MarkAllAsReadJob(isEvent, uuid));
    }

    public void markAllAsRead(boolean isEvent, String uuid) {
        if (isEvent) {
            markChatAsRead(true, uuid);
            markInfoAsRead(uuid);
            markPollAsRead(uuid);
            markUsersAsRead(true, uuid);
        } else {
            markChatAsRead(false, uuid);
            markUsersAsRead(false, uuid);
        }

        if (ShortcutBadger.isBadgeCounterSupported(App.getContext())) {
            //noinspection TryFinallyCanBeTryWithResources
            MessageDao messageDao = new MessageDao(realm);
            ShortcutBadger.applyCount(App.getContext(), messageDao.getNumNotReadMessagedForEvent() + messageDao.getNumNotReadMessagedForGroup());
        }
    }

    public void markChatAsReadAsync(boolean isEvent, String uuid) {
        App.jobManager().addJobInBackground(new MarkChatAsReadJob(isEvent, uuid));
    }

    public void markChatAsRead(boolean isEvent, String uuid) {
        String field;
        // Definisco il parent corretto
        if (isEvent) {
            field = Message.EVENT_PARENT_ID;
        } else {
            field = Message.GROUP_PARENT_ID;
        }

        // Segno tutti i messaggi come letti
        RealmQuery<Message> query = realm.where(Message.class).equalTo(Message.IGNORE_READ, false).equalTo(field, uuid).equalTo(Message.READ, false);
        query = filterMessages(query, isEvent);
        RealmResults<Message> messaggi = query.findAll();
        for (Message messaggio : messaggi) {
            messaggio.setRead(true);
            messaggio.setFirstNotRead(false);
        }

        // Aggiorno il numero dei nuovi messaggi
        if (isEvent) {
            new EventDao(realm).updateNumberMessages(uuid);
        } else {
            new GroupDao(realm).updateNumberMessages(uuid);
        }
    }

    public void markPollAsReadAsync(String uuid) {
        App.jobManager().addJobInBackground(new MarkPollAsReadJob(uuid));
    }

    public void markPollAsRead(String uuid) {
        // Segno tutti i messaggi come letti
        RealmQuery<Message> query = realm.where(Message.class).equalTo(Message.EVENT_PARENT_ID, uuid).equalTo(Message.READ, false);
        query = filterPollMessages(query);
        RealmResults<Message> messaggi = query.findAll();
        for (Message messaggio : messaggi) {
            if (messaggio.getPoll() != null && messaggio.getPoll().isNuovo()) {
                messaggio.getPoll().setNuovo(false);
            }
            if (messaggio.getPollItem() != null && messaggio.getPollItem().isNuovo()) {
                messaggio.getPollItem().setNuovo(false);
            }
            messaggio.setRead(true);
            messaggio.setFirstNotRead(false);
        }

        RealmResults<Poll> polls = new PollDao(realm).getListPollsFromEvent(uuid);
        for (Poll poll : polls) {
            if (poll != null && poll.isNuovo()) {
                poll.setNuovo(false);
            }
        }

        // Aggiorno il numero dei nuovi messaggi
        new EventDao(realm).updateNumberPollUpdates(uuid);
    }

    public void markUsersAsReadAsync(boolean isEvento, String uuid) {
        App.jobManager().addJobInBackground(new MarkUsersAsReadJob(isEvento, uuid));
    }

    public void markUsersAsRead(boolean isEvent, String uuid) {
        String field;
        // Definisco il parent corretto
        if (isEvent) {
            field = Message.EVENT_PARENT_ID;
        } else {
            field = Message.GROUP_PARENT_ID;
        }

        // Segno tutti i messaggi come letti
        RealmQuery<Message> query = realm.where(Message.class).equalTo(field, uuid).equalTo(Message.READ, false);
        query = filterUserMessages(query);
        RealmResults<Message> messaggi = query.findAll();
        for (Message messaggio : messaggi) {
            messaggio.setRead(true);
            messaggio.setFirstNotRead(false);
        }

        // Aggiorno il numero dei nuovi messaggi
        if (isEvent) {
            new EventDao(realm).updateNumberUserUpdates(uuid);
        } else {
            new GroupDao(realm).updateNumberUserUpdates(uuid);
        }
    }

    public void markInfoAsReadAsync(String uuid) {
        App.jobManager().addJobInBackground(new MarkInfoAsReadJob(uuid));
    }

    public void markInfoAsRead(String uuid) {
        // Segno tutti i messaggi come letti
        RealmQuery<Message> query = realm.where(Message.class).equalTo(Message.EVENT_PARENT_ID, uuid).equalTo(Message.READ, false);
        query = filterInfoMessages(query);
        RealmResults<Message> messaggi = query.findAll();
        for (Message messaggio : messaggi) {
            if (messaggio.getPoll() != null && messaggio.getPoll().isNuovo()) {
                messaggio.getPoll().setNuovo(false);
            }
            if (messaggio.getPollItem() != null && messaggio.getPollItem().isNuovo()) {
                messaggio.getPollItem().setNuovo(false);
            }
            messaggio.setRead(true);
            messaggio.setFirstNotRead(false);
        }

        // Aggiorno il numero dei nuovi messaggi
        new EventDao(realm).updateNumberInfoUpdates(uuid);
    }

    private void updateFirstMessageNotRead(int typology, String uuid) {
        String field = "";
        String id = "";
        // Creo la condizione della query a seconda della chat in cui si ricade
        switch (typology) {
            case Constants.MESSAGGIO_TYPOLOGY_CHAT_EVENTO:
                field = Message.EVENT_PARENT_ID;
                id = uuid;
                break;
            case Constants.MESSAGGIO_TYPOLOGY_CHAT_GRUPPO:
                field = Message.GROUP_PARENT_ID;
                id = uuid;
                break;
        }
        // Recupero il primo messaggio non letto del momento
        Message oldMessaggio = realm.where(Message.class).equalTo(Message.TYPOLOGY, typology)
                .equalTo(Message.FIRST_NOT_READ, true).equalTo(Message.IGNORE_READ, false).equalTo(field, id).findFirst();
        // Recupero il nuovo primo messaggio non letto
        RealmResults<Message> messaggi = realm.where(Message.class).equalTo(Message.TYPOLOGY, typology).equalTo(Message.IGNORE_READ, false)
                .beginGroup()
                .notEqualTo(Message.TYPE, Constants.MESSAGGIO_TYPE_SYSTEM)
                .or()
                .equalTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_CREAZIONE_GRUPPO)
                .or()
                .equalTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_CREAZIONE_EVENTO)
                .endGroup()
                .equalTo(Message.READ, false).equalTo(field, id).findAllSorted(Message.CREATED_AT, Sort.ASCENDING);
        if (messaggi == null || messaggi.size() == 0) {
            if (oldMessaggio != null) {
                oldMessaggio.setFirstNotRead(false);
            }
            return;
        }
        Message messaggio = messaggi.first();
        // Controllo se non è lo stesso messaggio, altrimenti ignoro
        if (messaggio != oldMessaggio) {
            if (oldMessaggio != null) {
                oldMessaggio.setFirstNotRead(false);
            }
            if (messaggio != null) {
                messaggio.setFirstNotRead(true);
            }
        }
    }

    public void deleteMessage(String uuid) {
        Message messaggio = getMessage(uuid);
        if (messaggio != null) {
            messaggio.deleteFromRealm();
        }
    }

    public void deleteAllMessagesGroup(String groupId) {
        RealmResults<Message> messaggi = realm.where(Message.class).equalTo(Message.GROUP_PARENT_ID, groupId).equalTo(Message.TYPOLOGY, Constants.MESSAGGIO_TYPOLOGY_CHAT_GRUPPO).findAll();
        messaggi.deleteAllFromRealm();
    }

    public void deleteAllMessagesEvent(String eventoId) {
        RealmResults<Message> messaggi = realm.where(Message.class).equalTo(Message.EVENT_PARENT_ID, eventoId).equalTo(Message.TYPOLOGY, Constants.MESSAGGIO_TYPOLOGY_CHAT_EVENTO).findAll();
        messaggi.deleteAllFromRealm();
    }

    private boolean isMyself(User utente) {
        return utente.getUuid() != null && utente.getUuid().equals(AccountProvider.getInstance().getAccountId());
    }


    private boolean showInHomepage(int type) {
        switch (type) {
            case Constants.SYSTEM_MESSAGE_VOTO_EVENTO:
                return Constants.MESSAGGI_SHOW_VOTO_EVENTO_IN_HOME;
            case Constants.SYSTEM_MESSAGE_VOTO_EVENTO_INTERESSE:
                return Constants.MESSAGGI_SHOW_VOTO_INTERESSE_EVENTO_IN_HOME;
            case Constants.SYSTEM_MESSAGE_CREAZIONE_EVENTO:
                return Constants.MESSAGGI_SHOW_CREAZIONE_EVENTO_IN_HOME;
            case Constants.SYSTEM_MESSAGE_MODIFICA_EVENTO:
                return Constants.MESSAGGI_SHOW_MODIFICA_EVENTO_IN_HOME;
            case Constants.SYSTEM_MESSAGE_ANNULLATO_EVENTO:
                return Constants.MESSAGGI_SHOW_ANNULLA_EVENTO_IN_HOME;
            case Constants.SYSTEM_MESSAGE_CONFERMATO_EVENTO:
                return Constants.MESSAGGI_SHOW_CONFERMA_EVENTO_IN_HOME;
            case Constants.SYSTEM_MESSAGE_PIANIFICATA_EVENTO:
                return Constants.MESSAGGI_SHOW_PIANIFICA_EVENTO_IN_HOME;
            case Constants.SYSTEM_MESSAGE_ABBANDONATO_EVENTO:
                return Constants.MESSAGGI_SHOW_ABBANDONA_EVENTO_IN_HOME;
            case Constants.SYSTEM_MESSAGE_CREAZIONE_SONDAGGIO:
                return Constants.MESSAGGI_SHOW_CREAZIONE_SONDAGGIO_IN_HOME;
            case Constants.SYSTEM_MESSAGE_CREAZIONE_SONDAGGIO_SPECIAL:
                return Constants.MESSAGGI_SHOW_CREAZIONE_SONDAGGIO_SPECIAL_IN_HOME;
            case Constants.SYSTEM_MESSAGE_ELIMINAZIONE_SONDAGGIO:
                return Constants.MESSAGGI_SHOW_ELIMINAZIONE_SONDAGGIO_IN_HOME;
            case Constants.SYSTEM_MESSAGE_CREAZIONE_VOCE_SONDAGGIO:
                return Constants.MESSAGGI_SHOW_CREAZIONE_VOCE_SONDAGGIO_IN_HOME;
            case Constants.SYSTEM_MESSAGE_CREAZIONE_VOCE_SONDAGGIO_SPECIAL:
                return Constants.MESSAGGI_SHOW_CREAZIONE_VOCE_SONDAGGIO_SPECIAL_IN_HOME;
            case Constants.SYSTEM_MESSAGE_ELIMINAZIONE_VOCE_SONDAGGIO:
                return Constants.MESSAGGI_SHOW_ELIMINAZIONE_VOCE_SONDAGGIO_IN_HOME;
            case Constants.SYSTEM_MESSAGE_APERTURA_SONDAGGIO:
                return Constants.MESSAGGI_SHOW_APERTURA_SONDAGGIO_IN_HOME;
            case Constants.SYSTEM_MESSAGE_CHIUSURA_SONDAGGIO:
                return Constants.MESSAGGI_SHOW_CHIUSURA_SONDAGGIO_IN_HOME;
            case Constants.SYSTEM_MESSAGE_CREAZIONE_GRUPPO:
                return Constants.MESSAGGI_SHOW_CREAZIONE_GRUPPO_IN_HOME;
            case Constants.SYSTEM_MESSAGE_ABBANDONATO_GRUPPO:
                return Constants.MESSAGGI_SHOW_ABBANDONA_GRUPPO_IN_HOME;
            case Constants.SYSTEM_MESSAGE_GRUPPO_CREAZIONE_EVENTO:
                return Constants.MESSAGGI_SHOW_GRUPPO_CREAZIONE_EVENTO_IN_HOME;
            case Constants.SYSTEM_MESSAGE_AGGIUNTO_PARTECIPANTE:
                return Constants.MESSAGGI_SHOW_AGGIUNTA_PARTECIPANTE_IN_HOME;
            case Constants.SYSTEM_MESSAGE_RIMOSSO_PARTECIPANTE:
                return Constants.MESSAGGI_SHOW_RIMOZIONE_PARTECIPANTE_IN_HOME;
            case Constants.SYSTEM_MESSAGE_NUOVO_ADMIN:
                return Constants.MESSAGGI_SHOW_NUOVO_ADMIN_IN_HOME;
        }
        return false;
    }

    private RealmQuery<Message> filterMessages(RealmQuery<Message> query, boolean isEvent) {
        if (isEvent) {
            if (!Constants.MESSAGGI_SHOW_VOTO_EVENTO) {
                query = query.notEqualTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_VOTO_EVENTO);
            }
            if (!Constants.MESSAGGI_SHOW_VOTO_INTERESSE_EVENTO) {
                query = query.notEqualTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_VOTO_EVENTO_INTERESSE);
            }
            if (!Constants.MESSAGGI_SHOW_CREAZIONE_EVENTO) {
                query = query.notEqualTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_CREAZIONE_EVENTO);
            }
            if (!Constants.MESSAGGI_SHOW_MODIFICA_EVENTO) {
                query = query.notEqualTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_MODIFICA_EVENTO);
            }
            if (!Constants.MESSAGGI_SHOW_ANNULLA_EVENTO) {
                query = query.notEqualTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_ANNULLATO_EVENTO);
            }
            if (!Constants.MESSAGGI_SHOW_CONFERMA_EVENTO) {
                query = query.notEqualTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_CONFERMATO_EVENTO);
            }
            if (!Constants.MESSAGGI_SHOW_PIANIFICA_EVENTO) {
                query = query.notEqualTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_PIANIFICATA_EVENTO);
            }
            if (!Constants.MESSAGGI_SHOW_ABBANDONA_EVENTO) {
                query = query.notEqualTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_ABBANDONATO_EVENTO);
            }
            if (!Constants.MESSAGGI_SHOW_CREAZIONE_SONDAGGIO) {
                query = query.notEqualTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_CREAZIONE_SONDAGGIO);
            }
            if (!Constants.MESSAGGI_SHOW_CREAZIONE_SONDAGGIO_SPECIAL) {
                query = query.notEqualTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_CREAZIONE_SONDAGGIO_SPECIAL);
            }
            if (!Constants.MESSAGGI_SHOW_ELIMINAZIONE_SONDAGGIO) {
                query = query.notEqualTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_ELIMINAZIONE_SONDAGGIO);
            }
            if (!Constants.MESSAGGI_SHOW_CREAZIONE_VOCE_SONDAGGIO) {
                query = query.notEqualTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_CREAZIONE_VOCE_SONDAGGIO);
            }
            if (!Constants.MESSAGGI_SHOW_CREAZIONE_VOCE_SONDAGGIO_SPECIAL) {
                query = query.notEqualTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_CREAZIONE_VOCE_SONDAGGIO_SPECIAL);
            }
            if (!Constants.MESSAGGI_SHOW_ELIMINAZIONE_VOCE_SONDAGGIO) {
                query = query.notEqualTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_ELIMINAZIONE_VOCE_SONDAGGIO);
            }
            if (!Constants.MESSAGGI_SHOW_APERTURA_SONDAGGIO) {
                query = query.notEqualTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_APERTURA_SONDAGGIO);
            }
            if (!Constants.MESSAGGI_SHOW_CHIUSURA_SONDAGGIO) {
                query = query.notEqualTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_CHIUSURA_SONDAGGIO);
            }
        } else {
            if (!Constants.MESSAGGI_SHOW_CREAZIONE_GRUPPO) {
                query = query.notEqualTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_CREAZIONE_GRUPPO);
            }
            if (!Constants.MESSAGGI_SHOW_ABBANDONA_GRUPPO) {
                query = query.notEqualTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_ABBANDONATO_GRUPPO);
            }
            if (!Constants.MESSAGGI_SHOW_GRUPPO_CREAZIONE_EVENTO) {
                query = query.notEqualTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_GRUPPO_CREAZIONE_EVENTO);
            }
        }
        if (!Constants.MESSAGGI_SHOW_AGGIUNTA_PARTECIPANTE) {
            query = query.notEqualTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_AGGIUNTO_PARTECIPANTE);
        }
        if (!Constants.MESSAGGI_SHOW_RIMOZIONE_PARTECIPANTE) {
            query = query.notEqualTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_RIMOSSO_PARTECIPANTE);
        }
        if (!Constants.MESSAGGI_SHOW_NUOVO_ADMIN) {
            query = query.notEqualTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_NUOVO_ADMIN);
        }

        return query;
    }

    private RealmQuery<Message> filterInfoMessages(RealmQuery<Message> query) {
        boolean needOr = false;
        query = query.beginGroup();
        if (Constants.MESSAGGI_SHOW_MODIFICA_EVENTO_UPDATE) {
            if (needOr) {
                query = query.or();
            }
            query = query.equalTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_MODIFICA_EVENTO);
            needOr = true;
        }
        if (Constants.MESSAGGI_SHOW_ANNULLA_EVENTO_UPDATE) {
            if (needOr) {
                query = query.or();
            }
            query = query.equalTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_ANNULLATO_EVENTO);
            needOr = true;
        }
        if (Constants.MESSAGGI_SHOW_CONFERMA_EVENTO_UPDATE) {
            if (needOr) {
                query = query.or();
            }
            query = query.equalTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_CONFERMATO_EVENTO);
            needOr = true;
        }
        if (Constants.MESSAGGI_SHOW_PIANIFICA_EVENTO_UPDATE) {
            if (needOr) {
                query = query.or();
            }
            query = query.equalTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_PIANIFICATA_EVENTO);
            needOr = true;
        }
        if (Constants.MESSAGGI_SHOW_CREAZIONE_VOCE_SONDAGGIO_SPECIAL_UPDATE) {
            if (needOr) {
                query = query.or();
            }
            query = query.equalTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_CREAZIONE_VOCE_SONDAGGIO_SPECIAL);
            needOr = true;
        }
        if (Constants.MESSAGGI_SHOW_CREAZIONE_SONDAGGIO_SPECIAL_UPDATE) {
            if (needOr) {
                query = query.or();
            }
            query = query.equalTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_CREAZIONE_SONDAGGIO_SPECIAL);
        }
        query = query.endGroup();

        return query;
    }

    private RealmQuery<Message> filterPollMessages(RealmQuery<Message> query) {
        boolean needOr = false;
        query = query.beginGroup();
        if (Constants.MESSAGGI_SHOW_CREAZIONE_SONDAGGIO_UPDATE) {
            if (needOr) {
                query = query.or();
            }
            query = query.equalTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_CREAZIONE_SONDAGGIO);
            needOr = true;
        }
        if (Constants.MESSAGGI_SHOW_CREAZIONE_VOCE_SONDAGGIO_UPDATE) {
            if (needOr) {
                query = query.or();
            }
            query = query.equalTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_CREAZIONE_VOCE_SONDAGGIO);
            needOr = true;
        }
        if (Constants.MESSAGGI_SHOW_APERTURA_SONDAGGIO_UPDATE) {
            if (needOr) {
                query = query.or();
            }
            query = query.equalTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_APERTURA_SONDAGGIO);
            needOr = true;
        }
        if (Constants.MESSAGGI_SHOW_CHIUSURA_SONDAGGIO_UPDATE) {
            if (needOr) {
                query = query.or();
            }
            query = query.equalTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_CHIUSURA_SONDAGGIO);
        }
        query = query.endGroup();

        return query;
    }

    private RealmQuery<Message> filterUserMessages(RealmQuery<Message> query) {
        boolean needOr = false;
        query = query.beginGroup();
        if (Constants.MESSAGGI_SHOW_AGGIUNTA_PARTECIPANTE_UPDATE) {
            if (needOr) {
                query = query.or();
            }
            query = query.equalTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_AGGIUNTO_PARTECIPANTE);
            needOr = true;
        }
        if (Constants.MESSAGGI_SHOW_RIMOZIONE_PARTECIPANTE_UPDATE) {
            if (needOr) {
                query = query.or();
            }
            query = query.equalTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_RIMOSSO_PARTECIPANTE);
            needOr = true;
        }
        if (Constants.MESSAGGI_SHOW_CONFERMA_EVENTO_UPDATE) {
            if (needOr) {
                query = query.or();
            }
            query = query.equalTo(Message.SYSTEM_TYPE, Constants.SYSTEM_MESSAGE_NUOVO_ADMIN);
        }
        query = query.endGroup();

        return query;
    }
}
