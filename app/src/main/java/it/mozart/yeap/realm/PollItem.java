package it.mozart.yeap.realm;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import it.mozart.yeap.common.DoveInfo;
import it.mozart.yeap.common.QuandoInfo;
import it.mozart.yeap.utility.DateUtils;

public class PollItem extends RealmObject {

    @PrimaryKey
    private String uuid;
    public static String UUID = "uuid";
    private long createdAt;
    public static String CREATED_AT = "createdAt";
    private String pollUuid;
    private boolean deleted;

    private String what;
    private String where;
    private String placeId;
    private double lat;
    private double lng;
    private String fromDate;
    private String fromHour;
    private String toDate;
    private String toHour;

    private User creator;
    private String creatorUuid;

    private Vote myVote;
    private int numVoteYes;
    public static String NUM_VOTES_YES = "numVoteYes";
    private int numVoteNo;

    private boolean nuovo;

    @Ignore
    private RealmList<Vote> votes;

    public PollItem() {
    }

    public PollItem(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public String getPollUuid() {
        return pollUuid;
    }

    public void setPollUuid(String pollUuid) {
        this.pollUuid = pollUuid;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getWhat() {
        return what;
    }

    public void setWhat(String what) {
        this.what = what;
    }

    public String getWhere() {
        return where;
    }

    public void setWhere(String where) {
        this.where = where;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getFromHour() {
        return fromHour;
    }

    public void setFromHour(String fromHour) {
        this.fromHour = fromHour;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getToHour() {
        return toHour;
    }

    public void setToHour(String toHour) {
        this.toHour = toHour;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
        if (creator == null) {
            setCreatorUuid(null);
        } else {
            setCreatorUuid(creator.getUuid());
        }
    }

    public String getCreatorUuid() {
        return creatorUuid;
    }

    public void setCreatorUuid(String creatorUuid) {
        this.creatorUuid = creatorUuid;
    }

    public Vote getMyVote() {
        return myVote;
    }

    public void setMyVote(Vote myVote) {
        this.myVote = myVote;
    }

    public int getNumVoteYes() {
        return numVoteYes;
    }

    public void setNumVoteYes(int numVoteYes) {
        if (numVoteYes < 0) {
            numVoteYes = 0;
        }
        this.numVoteYes = numVoteYes;
    }

    public int getNumVoteNo() {
        return numVoteNo;
    }

    public void setNumVoteNo(int numVoteNo) {
        this.numVoteNo = numVoteNo;
    }

    public boolean isNuovo() {
        return nuovo;
    }

    public void setNuovo(boolean nuovo) {
        this.nuovo = nuovo;
    }

    public RealmList<Vote> getVotes() {
        return votes;
    }

    public void setVotes(RealmList<Vote> votes) {
        this.votes = votes;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PollItem) {
            return getUuid().equals(((PollItem) obj).getUuid());
        }
        return super.equals(obj);
    }

    public DoveInfo generateDoveModel() {
        if (getWhere() != null) {
            DoveInfo doveInfo = new DoveInfo();
            doveInfo.setPlaceId(getPlaceId());
            doveInfo.setLat(getLat());
            doveInfo.setLng(getLng());
            doveInfo.setDove(getWhere());
            return doveInfo;
        }
        return null;
    }

    public QuandoInfo generateQuandoModel() {
        Date startDate = null;
        if (fromDate != null && fromDate.length() > 0) {
            startDate = DateTime.parse(fromDate, DateTimeFormat.forPattern("yyyy-MM-dd")).toDate();
        }
        Date startTime = null;
        if (fromHour != null && fromHour.length() > 0) {
            startTime = DateTime.parse(fromHour, DateTimeFormat.forPattern("HH:mm")).toDate();
        }
        Date endDate = null;
        if (toDate != null && toDate.length() > 0) {
            endDate = DateTime.parse(toDate, DateTimeFormat.forPattern("yyyy-MM-dd")).toDate();
        }
        Date endTime = null;
        if (toHour != null && toHour.length() > 0) {
            endTime = DateTime.parse(toHour, DateTimeFormat.forPattern("HH:mm")).toDate();
        }
        if (startDate != null || startTime != null || endDate != null || endTime != null) {
            QuandoInfo quandoInfo = new QuandoInfo();
            quandoInfo.setDataDa(startDate);
            quandoInfo.setOraDa(startTime);
            quandoInfo.setDataA(endDate);
            quandoInfo.setOraA(endTime);
            return quandoInfo;
        }
        return null;
    }

    public boolean isQuandoPresent() {
        return fromDate != null && fromDate.length() > 0 || fromHour != null && fromHour.length() > 0 || toDate != null && toDate.length() > 0 || toHour != null && toHour.length() > 0;
    }

    public void updateDoveInfo(DoveInfo doveModel) {
        if (doveModel == null) {
            where = null;
            placeId = null;
            lat = 0;
            lng = 0;
        } else {
            where = doveModel.getDove();
            placeId = doveModel.getPlaceId();
            lat = doveModel.getLat();
            lng = doveModel.getLng();
        }
    }

    public void updateQuandoInfo(QuandoInfo quandoModel) {
        if (quandoModel == null) {
            fromDate = null;
            fromHour = null;
            toDate = null;
            toHour = null;
        } else {
            if (quandoModel.getDataDa() != null) {
                fromDate = DateUtils.formatDate(quandoModel.getDataDa(), "yyyy-MM-dd");
            }
            if (quandoModel.getOraDa() != null) {
                fromHour = DateUtils.formatDate(quandoModel.getOraDa(), "HH:mm");
            }
            if (quandoModel.getDataA() != null) {
                toDate = DateUtils.formatDate(quandoModel.getDataA(), "yyyy-MM-dd");
            }
            if (quandoModel.getOraA() != null) {
                toHour = DateUtils.formatDate(quandoModel.getOraA(), "HH:mm");
            }
        }
    }
}
