package it.mozart.yeap.realm.dao;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import it.mozart.yeap.realm.Group;
import it.mozart.yeap.realm.User;

public class GroupDao {

    private Realm realm;

    public GroupDao(Realm realm) {
        this.realm = realm;
    }

    public Group getGroup(String uuid) {
        return realm.where(Group.class).equalTo(Group.UUID, uuid).findFirst();
    }

    public RealmResults<Group> getListGroupsHome() {
        return realm.where(Group.class).findAllSorted(Group.UPDATED_AT, Sort.DESCENDING);
    }

    public RealmResults<Group> getListGroupsWithMeInside() {
        return realm.where(Group.class).equalTo(Group.MYSELF_PRESENT, true).findAllSorted(Group.UPDATED_AT, Sort.DESCENDING);
    }

    public RealmResults<Group> getListGroupsFromIdList(String[] ids) {
        RealmQuery<Group> query = realm.where(Group.class);
        // Creo la query
        if (ids.length > 1) {
            for (String id : ids) {
                query = query.or().equalTo(Group.UUID, id);
            }
        } else {
            query = query.equalTo(Group.UUID, ids[0]);
        }
        return query.findAll();
    }

    public long getNumberGroupsWithUpdates() {
        return realm.where(Group.class)
                .notEqualTo(Group.NUM_NEW_MESSAGES, 0)
                .or()
                .notEqualTo(Group.NUM_USER_UPDATES, 0)
                .count();
    }

    public Group creaGruppo(Group gruppo, User creator, boolean isSyncTotal) {
        Group newGruppo = realm.copyToRealm(gruppo);
        new MessageDao(realm).createSystemMessageNewGroup(newGruppo, creator, isSyncTotal);

        return newGruppo;
    }

    void updateNumberMessages(String gruppoId) {
        Group gruppo = getGroup(gruppoId);
        if (gruppo == null)
            return;
        // Aggiorno numero nuovi messaggi
        long numNuoviMessaggi = new MessageDao(realm).getNumberNewMessagesGroup(gruppoId);
        if (numNuoviMessaggi != gruppo.getNumNewMessages()) {
            gruppo.setNumNewMessages(numNuoviMessaggi);
        }
    }

    void updateNumberUserUpdates(String gruppoId) {
        Group gruppo = getGroup(gruppoId);
        if (gruppo == null)
            return;
        // Aggiorno numero nuovi messaggi
        long numUserUpdates = new MessageDao(realm).getNumberUserUpdatesGroup(gruppoId);
        if (numUserUpdates != gruppo.getNumUserUpdates()) {
            gruppo.setNumUserUpdates(numUserUpdates);
        }
    }

    public void deleteGroup(String gruppoId) {
        Group gruppo = getGroup(gruppoId);
        if (gruppo == null)
            return;

        MessageDao messageDao = new MessageDao(realm);
        messageDao.deleteAllMessagesGroup(gruppoId);

        gruppo.deleteFromRealm();
    }
}
