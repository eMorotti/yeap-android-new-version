package it.mozart.yeap.utility.debounce;

public interface NotificationListener {

    void startNotification(NotificationInfo info);
}
