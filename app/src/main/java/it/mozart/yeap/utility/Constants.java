package it.mozart.yeap.utility;

public class Constants {

    static final String URL_DEV = "api.yeap.testmozart.com";
    static final String URL_PROD = "api.yeap.cloud";
    static final String URL_DEV_WS = "ws.yeap.testmozart.com";
    static final String URL_PROD_WS = "ws.yeap.cloud";

    /**
     * Settings
     */

    // VOTI
    public static final boolean MESSAGGI_SHOW_VOTO_EVENTO = false;
    public static final boolean MESSAGGI_SHOW_VOTO_EVENTO_IN_HOME = true;

    public static final boolean MESSAGGI_SHOW_VOTO_INTERESSE_EVENTO = false;
    public static final boolean MESSAGGI_SHOW_VOTO_INTERESSE_EVENTO_IN_HOME = true;

    // EVENTO
    public static final boolean MESSAGGI_SHOW_CREAZIONE_EVENTO = true;
    public static final boolean MESSAGGI_SHOW_CREAZIONE_EVENTO_IN_HOME = true;

    public static final boolean MESSAGGI_SHOW_MODIFICA_EVENTO = true;
    public static final boolean MESSAGGI_SHOW_MODIFICA_EVENTO_IN_HOME = true;
    public static final boolean MESSAGGI_SHOW_MODIFICA_EVENTO_UPDATE = true;

    public static final boolean MESSAGGI_SHOW_ANNULLA_EVENTO = true;
    public static final boolean MESSAGGI_SHOW_ANNULLA_EVENTO_IN_HOME = true;
    public static final boolean MESSAGGI_SHOW_ANNULLA_EVENTO_UPDATE = true;

    public static final boolean MESSAGGI_SHOW_CONFERMA_EVENTO = true;
    public static final boolean MESSAGGI_SHOW_CONFERMA_EVENTO_IN_HOME = true;
    public static final boolean MESSAGGI_SHOW_CONFERMA_EVENTO_UPDATE = true;

    public static final boolean MESSAGGI_SHOW_PIANIFICA_EVENTO = true;
    public static final boolean MESSAGGI_SHOW_PIANIFICA_EVENTO_IN_HOME = true;
    public static final boolean MESSAGGI_SHOW_PIANIFICA_EVENTO_UPDATE = true;

    public static final boolean MESSAGGI_SHOW_ABBANDONA_EVENTO = true;
    public static final boolean MESSAGGI_SHOW_ABBANDONA_EVENTO_IN_HOME = true;

    // GRUPPO
    public static final boolean MESSAGGI_SHOW_CREAZIONE_GRUPPO = true;
    public static final boolean MESSAGGI_SHOW_CREAZIONE_GRUPPO_IN_HOME = true;

    public static final boolean MESSAGGI_SHOW_ABBANDONA_GRUPPO = true;
    public static final boolean MESSAGGI_SHOW_ABBANDONA_GRUPPO_IN_HOME = true;

    public static final boolean MESSAGGI_SHOW_GRUPPO_CREAZIONE_EVENTO = true;
    public static final boolean MESSAGGI_SHOW_GRUPPO_CREAZIONE_EVENTO_IN_HOME = true;

    // SONDAGGI
    public static final boolean MESSAGGI_SHOW_CREAZIONE_SONDAGGIO = false;
    public static final boolean MESSAGGI_SHOW_CREAZIONE_SONDAGGIO_IN_HOME = true;
    public static final boolean MESSAGGI_SHOW_CREAZIONE_SONDAGGIO_UPDATE = true;

    public static final boolean MESSAGGI_SHOW_CREAZIONE_SONDAGGIO_SPECIAL = false;
    public static final boolean MESSAGGI_SHOW_CREAZIONE_SONDAGGIO_SPECIAL_IN_HOME = true;
    public static final boolean MESSAGGI_SHOW_CREAZIONE_SONDAGGIO_SPECIAL_UPDATE = true;

    public static final boolean MESSAGGI_SHOW_ELIMINAZIONE_SONDAGGIO = false;
    public static final boolean MESSAGGI_SHOW_ELIMINAZIONE_SONDAGGIO_IN_HOME = true;

    public static final boolean MESSAGGI_SHOW_CREAZIONE_VOCE_SONDAGGIO = false;
    public static final boolean MESSAGGI_SHOW_CREAZIONE_VOCE_SONDAGGIO_IN_HOME = true;
    public static final boolean MESSAGGI_SHOW_CREAZIONE_VOCE_SONDAGGIO_UPDATE = true;

    public static final boolean MESSAGGI_SHOW_CREAZIONE_VOCE_SONDAGGIO_SPECIAL = false;
    public static final boolean MESSAGGI_SHOW_CREAZIONE_VOCE_SONDAGGIO_SPECIAL_IN_HOME = true;
    public static final boolean MESSAGGI_SHOW_CREAZIONE_VOCE_SONDAGGIO_SPECIAL_UPDATE = true;

    public static final boolean MESSAGGI_SHOW_ELIMINAZIONE_VOCE_SONDAGGIO = false;
    public static final boolean MESSAGGI_SHOW_ELIMINAZIONE_VOCE_SONDAGGIO_IN_HOME = true;

    public static final boolean MESSAGGI_SHOW_APERTURA_SONDAGGIO = true;
    public static final boolean MESSAGGI_SHOW_APERTURA_SONDAGGIO_IN_HOME = true;
    public static final boolean MESSAGGI_SHOW_APERTURA_SONDAGGIO_UPDATE = true;

    public static final boolean MESSAGGI_SHOW_CHIUSURA_SONDAGGIO = true;
    public static final boolean MESSAGGI_SHOW_CHIUSURA_SONDAGGIO_IN_HOME = true;
    public static final boolean MESSAGGI_SHOW_CHIUSURA_SONDAGGIO_UPDATE = true;

    // PARTECIPANTI
    public static final boolean MESSAGGI_SHOW_AGGIUNTA_PARTECIPANTE = true;
    public static final boolean MESSAGGI_SHOW_AGGIUNTA_PARTECIPANTE_IN_HOME = true;
    public static final boolean MESSAGGI_SHOW_AGGIUNTA_PARTECIPANTE_UPDATE = true;

    public static final boolean MESSAGGI_SHOW_RIMOZIONE_PARTECIPANTE = true;
    public static final boolean MESSAGGI_SHOW_RIMOZIONE_PARTECIPANTE_IN_HOME = true;
    public static final boolean MESSAGGI_SHOW_RIMOZIONE_PARTECIPANTE_UPDATE = true;

    public static final boolean MESSAGGI_SHOW_NUOVO_ADMIN = true;
    public static final boolean MESSAGGI_SHOW_NUOVO_ADMIN_IN_HOME = true;
    public static final boolean MESSAGGI_SHOW_NUOVO_ADMIN_UPDATE = true;

    // PREVIEW
    public static final boolean URL_PREVIEW_ENABLED = true;

    /**
     * Support
     */
    public static final String UPDATE_APP = "updateApp";

    public static final String LOGIN_EFFECTUATED = "loginEffettuated";
    public static final String LOGIN_COMPLETED = "loginCompleted";

    public static final String IMAGE_CROP = "imageCrop";
    public static final String IMAGE_PATH = "imagePath";

    public static final String CAPTION = "caption";

    public static final String INTENT_DOVE = "intentDove";
    public static final String INTENT_QUANDO = "intentQuando";

    public static final String URL_PATTERN = "((https?:\\/\\/)[\\w-]+(\\.[\\w-]+)+\\.?(:\\d+)?(\\/\\S*)?)";

    public static final String DOWNLOAD_PREFIX = "RealmMessageImage_";

    public static final int TYPE_EVENTO = 1;
    public static final int TYPE_GRUPPO = 2;
    public static final int TYPE_PROPOSTA = 3;
    public static final int TYPE_SONDAGGIO = 4;

    public static final String PREF_ACCESS_TOKEN = "prefAccessToken";
    public static final String PREF_DEVICE_ID = "prefDeviceId";
    public static final String PREF_PROFILE_ID = "prefProfileId";
    public static final String PREF_PROFILE_PHONE = "prefProfilePhone";

    public static final int IMAGE_PERMISSION_CODE = 5243;
    public static final int CONTACTS_PERMISSION_CODE = 3243;
    public static final int SD_PERMISSION_CODE = 1539;

    public static final int IMAGE_ACTIVITY_CODE = 191;
    public static final int IMAGE_GALLERY_ACTIVITY_CODE = 192;

    public static final String DEFAULT_JOB_QUEUE = "DEFAULT";

    public static final String SHOW_INVITE_POPUP_GRUPPO = "showInvitePopupGruppo";
    public static final String SHOW_INVITE_POPUP_EVENTO = "showInvitePopupEvento";

    public static final String REPLY_ACTION = "it.mozart.yeap.services.sync.REPLY_ACTION";
    public static final String REPLY_KEY = "replayKey";
    public static final String REPLY_ID = "replayId";
    public static final String REPLY_IS_EVENTO = "replayIsAttivita";
    public static final String REPLY_NOTIFICATION_ID = "replayNotificationId";

    public static final String BACKUP_PATH = "backupPath";
    public static final String BACKUP_DATE = "backupDate";
    public static final String BACKUP_TIMESTAMP = "backupTimestamp";

    public static final String URL_JOB_TAG = "urlJobTag";

    public static final int EVENT_PAGE_DETAILS = 0;
    public static final int EVENT_PAGE_CHAT = 1;
    public static final int EVENT_PAGE_POLLS = 2;
    public static final int EVENT_PAGE_USERS = 3;

    public static final int GROUP_PAGE_CHAT = 0;
    public static final int GROUP_PAGE_USERS = 2;

    /**
     * Camera
     */
    public static final String FLASH_PARAM = "flashParam";

    /**
     * Adapter types
     */
    public static final int ADAPTER_TYPE_STANDARD_LEFT = 1;
    public static final int ADAPTER_TYPE_STANDARD_RIGHT = 10;
    public static final int ADAPTER_TYPE_SYSTEM = 11;
    public static final int ADAPTER_TYPE_PROPOSTA = 12;
    public static final int ADAPTER_TYPE_PROPOSTA_LEFT = 120;
    public static final int ADAPTER_TYPE_PROPOSTA_RIGHT = 121;
    public static final int ADAPTER_TYPE_SONDAGGIO = 13;
    public static final int ADAPTER_TYPE_SONDAGGIO_LEFT = 130;
    public static final int ADAPTER_TYPE_SONDAGGIO_RIGHT = 131;
    public static final int ADAPTER_TYPE_EVENTO = 14;
    public static final int ADAPTER_TYPE_OTHER = 100;

    public static final int ADAPTER_HEADER = 1;
    public static final int ADAPTER_ITEM = 2;

    public static final int ADAPTER_HEADER_PARTECIPANTI = 1;
    public static final int ADAPTER_ITEM_PARTECIPANTI = 2;
    public static final int ADAPTER_HEADER_INVITATI = 3;
    public static final int ADAPTER_ITEM_INVITATI = 4;

    public static final int ADAPTER_HEADER_VOTI_SI = 1;
    public static final int ADAPTER_ITEM_VOTI_SI = 2;
    public static final int ADAPTER_HEADER_VOTI_NO = 3;
    public static final int ADAPTER_ITEM_VOTI_NO = 4;

    /**
     * Models
     */
    public static final int IMAGE_STATE_NOT_DOWNLOADED = 0;
    public static final int IMAGE_STATE_DOWNLOAD_COMPLETE = 1;
    public static final int IMAGE_STATE_DOWNLOAD_FAILED = 2;
    public static final int IMAGE_STATE_DOWNLOADING = 3;
    public static final int IMAGE_STATE_UPLOADING = 4;
    public static final int IMAGE_STATE_UPLOAD_COMPLETE = 5;

    public static final int EVENTO_IN_PIANIFICAZIONE = 1;
    public static final int EVENTO_ANNULLATO = 3;
    public static final int EVENTO_CONFERMATO = 2;

    public static final int VOTO_SI = 1;
    public static final int VOTO_NO = 2;
    public static final int VOTO_INTERESSE = 3;
    public static final int VOTO_NO_INTERESSE = 4;
    public static final int VOTE_TYPE_EVENT = 0;
    public static final int VOTE_TYPE_INTEREST = 1;

    public static final int MESSAGGIO_TYPOLOGY_CHAT_EVENTO = 1;
    public static final int MESSAGGIO_TYPOLOGY_CHAT_GRUPPO = 11;
    public static final int MESSAGGIO_TYPOLOGY_PROPOSTA = 2;
    public static final int MESSAGGIO_TYPOLOGY_SONDAGGIO = 3;
    public static final int MESSAGGIO_TYPOLOGY_PROPOSTA_CONFERMATA = 4;

    public static final int MESSAGGIO_TYPE_STANDARD = 1;
    public static final int MESSAGGIO_TYPE_SYSTEM = 2;

    public static final int POLL_TYPE_GENERAL = 1;
    public static final int POLL_TYPE_WHERE = 2;
    public static final int POLL_TYPE_WHEN = 3;

    public static final int MESSAGGIO_SISTEMA_ATTIVITA_CREATA = 4;
    public static final int MESSAGGIO_SISTEMA_GENERAL = 1;

    public static final int SYNC_UNIT_EVENTO_CREA = 1;
    public static final int SYNC_UNIT_EVENTO_AGGIUNTO_PARTECIPANTE = 2;
    public static final int SYNC_UNIT_EVENTO_PARTECIPANTE_USCITO = 3;
    public static final int SYNC_UNIT_EVENTO_RIMOSSO_PARTECIPANTE = 4;
    public static final int SYNC_UNIT_EVENTO_MODIFICA = 5;
    public static final int SYNC_UNIT_PROPOSTA_AGGIUNTA = 6;
    public static final int SYNC_UNIT_PROPOSTA_ELIMINATA = 7;
    public static final int SYNC_UNIT_PROPOSTA_VOTO = 8;
    public static final int SYNC_UNIT_SONDAGGIO_AGGIUNTO = 9;
    public static final int SYNC_UNIT_SONDAGGIO_ELIMINATO = 10;
    public static final int SYNC_UNIT_SONDAGGIO_VOCE_AGGIUNTA = 11;
    public static final int SYNC_UNIT_SONDAGGIO_VOCE_ELIMINATA = 12;
    public static final int SYNC_UNIT_SONDAGGIO_VOCE_VOTO = 13;
    public static final int SYNC_UNIT_EVENTO_CAMBIO_STATO = 14;
    public static final int SYNC_UNIT_EVENTO_MODIFICA_INFO = 15;
    public static final int SYNC_UNIT_EVENTO_MESSAGGIO = 16;
    public static final int SYNC_UNIT_EVENTO_VOTO = 17;
    public static final int SYNC_UNIT_EVENTO_AGGIUNTO_ME = 18;
    public static final int SYNC_UNIT_EVENTO_AGGIUNTO_INVITATO = 19;
    public static final int SYNC_UNIT_EVENTO_RIMOSSO_INVITATO = 20;
    public static final int SYNC_UNIT_SONDAGGIO_APERTO = 21;
    public static final int SYNC_UNIT_SONDAGGIO_CHIUSO = 22;
    public static final int SYNC_UNIT_GRUPPO_CREA = 31;
    public static final int SYNC_UNIT_GRUPPO_AGGIUNTO_PARTECIPANTE = 32;
    public static final int SYNC_UNIT_GRUPPO_PARTECIPANTE_USCITO = 33;
    public static final int SYNC_UNIT_GRUPPO_RIMOSSO_PARTECIPANTE = 34;
    public static final int SYNC_UNIT_GRUPPO_MODIFICATO = 35;
    public static final int SYNC_UNIT_GRUPPO_MESSAGGIO = 36;
    public static final int SYNC_UNIT_GRUPPO_AGGIUNTO_ME = 37;
    public static final int SYNC_UNIT_GRUPPO_AGGIUNTO_INVITATO = 38;
    public static final int SYNC_UNIT_GRUPPO_RIMOSSO_INVITATO = 39;
    public static final int SYNC_UNIT_USER_AVATAR_CHANGE = 50;

    public static final int SYSTEM_MESSAGE_CREAZIONE_EVENTO = 1;
    public static final int SYSTEM_MESSAGE_EVENTO_CAMBIO_STATO = 2;
    public static final int SYSTEM_MESSAGE_MODIFICA_EVENTO = 2;
    public static final int SYSTEM_MESSAGE_ANNULLATO_EVENTO = 3;
    public static final int SYSTEM_MESSAGE_CONFERMATO_EVENTO = 4;
    public static final int SYSTEM_MESSAGE_PIANIFICATA_EVENTO = 5;
    public static final int SYSTEM_MESSAGE_VOTO_EVENTO = 6;
    public static final int SYSTEM_MESSAGE_VOTO_EVENTO_INTERESSE = 61;
    public static final int SYSTEM_MESSAGE_ABBANDONATO_EVENTO = 7;
    public static final int SYSTEM_MESSAGE_CREAZIONE_GRUPPO = 8;
    public static final int SYSTEM_MESSAGE_MODIFICA_GRUPPO = 9;
    public static final int SYSTEM_MESSAGE_ABBANDONATO_GRUPPO = 10;
    public static final int SYSTEM_MESSAGE_ELIMINAZIONE_PROPOSTA = 11;
    public static final int SYSTEM_MESSAGE_VOTO_PROPOSTA = 12;
    public static final int SYSTEM_MESSAGE_ELIMINAZIONE_SONDAGGIO = 13;
    public static final int SYSTEM_MESSAGE_AGGIUNTO_PARTECIPANTE = 15;
    public static final int SYSTEM_MESSAGE_RIMOSSO_PARTECIPANTE = 16;
    public static final int SYSTEM_MESSAGE_NUOVO_ADMIN = 17;
    public static final int SYSTEM_MESSAGE_GRUPPO_CREAZIONE_EVENTO = 18;

    public static final int SYSTEM_MESSAGE_CREAZIONE_SONDAGGIO = 19;
    public static final int SYSTEM_MESSAGE_CREAZIONE_SONDAGGIO_SPECIAL = 191;
    public static final int SYSTEM_MESSAGE_CREAZIONE_VOCE_SONDAGGIO = 20;
    public static final int SYSTEM_MESSAGE_CREAZIONE_VOCE_SONDAGGIO_SPECIAL = 201;
    public static final int SYSTEM_MESSAGE_ELIMINAZIONE_VOCE_SONDAGGIO = 21;
    public static final int SYSTEM_MESSAGE_CREAZIONE_PROPOSTA = 22;
    public static final int SYSTEM_MESSAGE_APERTURA_SONDAGGIO = 23;
    public static final int SYSTEM_MESSAGE_CHIUSURA_SONDAGGIO = 24;

    /**
     * Impostazioni
     */
    public static final String SETTINGS_AUTODOWNLOAD = "settingsAutodownload";
    public static final String SETTINGS_AUTOBACKUP = "settingsAutobackup";
}
