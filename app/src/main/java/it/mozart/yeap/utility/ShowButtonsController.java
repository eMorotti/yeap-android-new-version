package it.mozart.yeap.utility;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;

import java.util.ArrayList;
import java.util.List;

import it.mozart.yeap.R;

public class ShowButtonsController {

    private Context mContext;
    private List<ViewContainer> mList;
    private View mOpaque;
    private onToggleListener mListener;

    private Type mType;
    private double mAngle;
    private List<Double> mAngles;
    private boolean mOpen;

    private float mDistance;
    private double mStartAngleOffset;
    private boolean mRotation;
    private boolean mCaptionPositionIsLeft;

    public enum Type {
        CIRCLE, CIRCLE_RIGHT, CIRCLE_LEFT, LINE_HOR, LINE_HOR_INV, LINE_VER, LINE_VER_INV
    }

    // SYSTEM

    public ShowButtonsController(Context context, ViewGroup container) {
        this.mContext = context;
        this.mList = new ArrayList<>();
        // Offset per il recupero delle view
        int startOffset = 0;
        // Cerco di prendere la view opaca
        this.mOpaque = container.findViewById(R.id.opaque);
        if (this.mOpaque != null) {
            startOffset = 1;
            this.mOpaque.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ShowButtonsController.this.hide(true);
                }
            });
        }

        // Prendo tutte le altre view
        for (int i = startOffset; i < container.getChildCount(); i++) {
            ViewContainer viewContainer;
            if (container.getChildAt(i) instanceof ViewGroup) {
                viewContainer = new ViewContainer(((ViewGroup) container.getChildAt(i)).getChildAt(0),
                        ((ViewGroup) container.getChildAt(i)).getChildAt(1));
            } else {
                viewContainer = new ViewContainer(container.getChildAt(i));
            }
            mList.add(viewContainer);
        }

        // Inizializzazione
        double angle = 80f / (mList.size() * 2);
        this.mType = Type.CIRCLE_RIGHT;
        this.mAngle = Math.toRadians(angle);
        this.mOpen = false;
        this.mDistance = Utils.dpToPx(75, mContext);
        this.mAngles = null;
        this.mRotation = false;
        this.mCaptionPositionIsLeft = true;
    }

    // Prende il riferimento della view opaca
    public void setOpaqueView(View view) {
        this.mOpaque = view;
        this.mOpaque.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowButtonsController.this.hide(true);
            }
        });
    }

    // Imposta la distanza della traslazione degli elementi
    public void setDistance(int distance) {
        this.mDistance = Utils.dpToPx(distance, mContext);
    }

    // Imposto l'offset del primo elemento
    public void setStartAngleOffset(float offset) {
        this.mStartAngleOffset = Math.toRadians(offset);
    }

    // Abilita o disabilita la rotazione
    public void setRotation(boolean rotate) {
        this.mRotation = rotate;
    }

    public void setCaptionPositionIsLeft(boolean captionPositionLeft) {
        this.mCaptionPositionIsLeft = captionPositionLeft;
    }

    // Imposta il tipo di animazione
    public void setType(Type type) {
        this.mType = type;
    }

    // Imposta gli angoli per la modalità circolare
    public void setAngles(float... angles) {
        if (angles.length < mList.size())
            throw new IllegalStateException("Error: too many angles");
        if (angles.length > 0) {
            mAngles = new ArrayList<>();
            for (float angle : angles) {
                mAngles.add(Math.toRadians(angle));
            }
        }
    }

    // Inverto l'animazione e gestisco la traslazione delle view
    public void toggle() {
        mOpen = !mOpen;
        float distance = mDistance;
        double angle = mAngle / 2f + mStartAngleOffset;
        // Gestisco le varie tipologie di animazione
        for (int i = 0; i < mList.size(); i++) {
            float translationX;
            float translationY;
            if (mType == Type.CIRCLE) {
                if (mAngles.size() != 0) {
                    translationX = (float) (distance * Math.cos(mAngles.get(i)));
                    translationY = (float) (distance * Math.sin(mAngles.get(i)));
                } else {
                    translationX = (float) (distance * Math.cos(angle));
                    translationY = (float) (distance * Math.sin(angle));
                }
            } else if (mType == Type.CIRCLE_RIGHT) {
                translationX = (float) (distance * Math.cos(angle));
                translationY = (float) (distance * Math.sin(angle));
            } else if (mType == Type.CIRCLE_LEFT) {
                translationX = -(float) (distance * Math.cos(angle));
                translationY = (float) (distance * Math.sin(angle));
            } else if (mType == Type.LINE_VER_INV) {
                translationX = 0f;
                translationY = -distance;
                distance += mDistance;
            } else if (mType == Type.LINE_VER) {
                translationX = 0f;
                translationY = distance;
                distance += mDistance;
            } else if (mType == Type.LINE_HOR_INV) {
                translationX = -distance;
                translationY = 0f;
                distance += mDistance;
            } else {
                translationX = distance;
                translationY = 0f;
                distance += mDistance;
            }
            // Animo le view
            if (mList.get(i).getCaptionView() != null) {
                animateCaption(mList.get(i), translationX, -translationY, mOpen);
            } else {
                animateSubAction(mList.get(i), translationX, -translationY, mOpen);
            }
            angle += mAngle + mAngle + mAngle / 2f;
        }

        // Animo la view opaca
        animateOpaque(mOpen);

        if (mListener != null) {
            mListener.onToggle(mOpen);
        }
    }

    // Anima la view opaca
    private void animateOpaque(final boolean show) {
        if (mOpaque == null)
            return;
        mOpaque.animate().alpha(show ? 1f : 0f).withStartAction(new Runnable() {
            @Override
            public void run() {
                mOpaque.setVisibility(View.VISIBLE);
            }
        }).withEndAction(new Runnable() {
            @Override
            public void run() {
                if (!show)
                    mOpaque.setVisibility(View.GONE);
            }
        }).setDuration(200).start();
    }

    // Anima le view
    private void animateSubAction(final ViewContainer viewContainer, float translationX, float translationY, final boolean show) {
        viewContainer.getMainView().animate().translationX(show ? translationX : 0).translationY(show ? translationY : 0)
                .rotation(mRotation ? (show ? -1090 : 0) : 0).alpha(show ? 1f : 0f).withStartAction(new Runnable() {
            @Override
            public void run() {
                viewContainer.getMainView().setVisibility(View.VISIBLE);
                viewContainer.getMainView().setClickable(false);
            }
        }).withEndAction(new Runnable() {
            @Override
            public void run() {
                viewContainer.getMainView().setClickable(true);
                if (!show) {
                    viewContainer.getMainView().setVisibility(View.GONE);
                } else if (viewContainer.getCaptionView() != null) {
                    animateCamptionOnEnd(viewContainer);
                }
            }
        }).setDuration(show ? 350 : 250).setInterpolator(show ? new MyBounceInterpolator() : new AccelerateDecelerateInterpolator()).start();
    }

    // Animo la via aggiuntiva
    private void animateCaption(final ViewContainer viewContainer, final float translationX, final float translationY, final boolean show) {
        if (show) {
            animateSubAction(viewContainer, translationX, translationY, true);
        } else {
            if (viewContainer.getCaptionView().getVisibility() == View.INVISIBLE) {
                animateSubAction(viewContainer, translationX, translationY, false);
                return;
            }
            viewContainer.getCaptionView().setPivotX(mCaptionPositionIsLeft ? viewContainer.getCaptionView().getWidth() : 0);
            viewContainer.getCaptionView().setPivotY(viewContainer.getCaptionView().getHeight() / 2);
            viewContainer.getCaptionView().animate().scaleX(0f).scaleY(0f).alpha(0f).withEndAction(new Runnable() {
                @Override
                public void run() {
                    viewContainer.getCaptionView().setVisibility(View.INVISIBLE);
                    animateSubAction(viewContainer, translationX, translationY, false);
                }
            }).setDuration(100).start();
        }
    }

    private void animateCamptionOnEnd(final ViewContainer viewContainer) {
        if (mCaptionPositionIsLeft) {
            viewContainer.getCaptionView().setX(viewContainer.getMainView().getX() - viewContainer.getCaptionView().getWidth());
        } else {
            viewContainer.getCaptionView().setX(viewContainer.getMainView().getX() + viewContainer.getMainView().getWidth());
        }
        viewContainer.getCaptionView().setY(viewContainer.getMainView().getY());
        viewContainer.getCaptionView().animate().scaleX(1f).scaleY(1f).alpha(1f).withStartAction(new Runnable() {
            @Override
            public void run() {
                viewContainer.getCaptionView().setVisibility(View.VISIBLE);
            }
        }).setDuration(150).start();
    }

    // Nasconde le view
    public void hide() {
        hide(false);
    }

    // Nasconde le view con animazione
    public void hide(boolean animate) {
        if (animate) {
            if (mOpen) {
                toggle();
            }
        } else {
            mOpen = false;
            for (int i = 0; i < mList.size(); i++) {
                // Resetto i valori
                ViewContainer viewContainer = mList.get(i);
                if (viewContainer.getMainView() != null) {
                    viewContainer.getMainView().setTranslationX(0);
                    viewContainer.getMainView().setTranslationY(0);
                    viewContainer.getMainView().setAlpha(0);
                    viewContainer.getMainView().setVisibility(View.GONE);
                }
                if (viewContainer.getCaptionView() != null) {
                    viewContainer.getCaptionView().setX(0);
                    viewContainer.getCaptionView().setY(0);
                    viewContainer.getCaptionView().setAlpha(0);
                    viewContainer.getCaptionView().setVisibility(View.INVISIBLE);
                }
            }
            if (mOpaque != null) {
                mOpaque.setAlpha(0);
                mOpaque.setVisibility(View.GONE);
            }
            if (mListener != null) {
                mListener.onToggle(mOpen);
            }
        }
    }

    public boolean isOpen() {
        return mOpen;
    }

    public List<ViewContainer> getList() {
        return mList;
    }

    public void setOnToggleListener(onToggleListener listener) {
        mListener = listener;
    }

    // CLASSES

    public class ViewContainer {
        private View mainView;
        private View captionView;

        ViewContainer(View mainView) {
            this.mainView = mainView;
        }

        ViewContainer(View mainView, View captionView) {
            this.mainView = mainView;
            this.captionView = captionView;
        }

        public View getMainView() {
            return mainView;
        }

        public View getCaptionView() {
            return captionView;
        }
    }

    // INTERFACE

    public interface onToggleListener {
        void onToggle(boolean isOpen);
    }

    // CLASSES

    private class MyBounceInterpolator implements Interpolator {

        double amplitude = 0.3f;
        double frequency = 5;

        @Override
        public float getInterpolation(float time) {
            return (float) (-1 * Math.pow(Math.E, -time / amplitude) * Math.cos(frequency * time) + 1);
        }
    }
}
