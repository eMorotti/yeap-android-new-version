package it.mozart.yeap.utility;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;

import java.util.Date;
import java.util.UUID;

import io.realm.Realm;
import it.mozart.yeap.R;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.PollItem;
import it.mozart.yeap.realm.Vote;
import it.mozart.yeap.realm.dao.PollDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.realm.dao.VoteDao;
import it.mozart.yeap.services.voti.VotiService;

public class VoteUtils {

    /**
     * Voto la voce sondaggio
     *
     * @param context         contesto
     * @param realm           realm
     * @param sondaggioVoceId id voce sondaggio
     * @param checked         checked
     */
    public static void votePollItem(final Context context, Realm realm, String sondaggioVoceId, final boolean checked) {
        final PollItem sondaggioVoce = new PollDao(realm).getPollItem(sondaggioVoceId);
        if (sondaggioVoce == null) {
            Toast.makeText(context, context.getString(R.string.general_error_sondaggio_voce_non_presente), Toast.LENGTH_SHORT).show();
            return;
        }

        final VoteDao voteDao = new VoteDao(realm);
        final UserDao userDao = new UserDao(realm);

        try {
            Vote voto = voteDao.getVotePollItemFromUser(sondaggioVoceId, AccountProvider.getInstance().getAccountId());
            if (voto == null && checked) {
                voto = new Vote();
                voto.setUuid(UUID.randomUUID().toString());
                voto.setCreatedAt(new Date().getTime());
                voto.setCreator(userDao.getUser(AccountProvider.getInstance().getAccountId()));
                voto.setLoading(true);
                voto.setPollItemUuid(sondaggioVoce.getUuid());
                voto.setType(Constants.VOTO_SI);

                final Vote finalVoto = voto;
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(@NonNull Realm realm) {
                        Vote newVoto = voteDao.createMyVotePollItem(finalVoto, sondaggioVoce);
                        VotiService.sendVote(context, newVoto.getUuid(), sondaggioVoce.getPollUuid());
                    }
                });
            } else if (voto != null && !checked) {
                final Vote finalVoto1 = voto;
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(@NonNull Realm realm) {
                        finalVoto1.setType(Constants.VOTO_NO);
                        finalVoto1.setLoading(true);
                        VotiService.sendVote(context, finalVoto1.getUuid(), sondaggioVoce.getPollUuid());
                    }
                });
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(context, context.getString(R.string.general_error_realm), Toast.LENGTH_LONG).show();
        }
    }
}
