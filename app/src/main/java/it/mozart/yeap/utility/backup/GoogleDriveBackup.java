package it.mozart.yeap.utility.backup;

import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallbacks;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.DriveStatusCodes;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.metadata.CustomPropertyKey;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.util.Date;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.utility.Constants;

public class GoogleDriveBackup implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient mGoogleApiClient;
    private DriveConnectionListener mListener;
    private WeakReference<Activity> mActivityRef;
    private File mExportRealmPath;
    private String mRealmFilePath;
    private long mFileTimestamp;
    private boolean mRunning;

    public static final int REQUEST_RESOLVE_ERROR = 1001;
    public static final int REQUEST_PLAY_SERVICES = 9000;

    public void init(Activity activity) {
        mActivityRef = new WeakReference<>(activity);

        mGoogleApiClient = new GoogleApiClient.Builder(activity)
                .addApi(Drive.API)
                .addScope(Drive.SCOPE_APPFOLDER)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mExportRealmPath = activity.getCacheDir();
        mRunning = false;
    }

    public boolean isGooglePlayServicesAvailable(Context context, boolean ignoreResolution) {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int resultCode = api.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (!ignoreResolution) {
                if (api.isUserResolvableError(resultCode)) {
                    api.getErrorDialog((Activity) context, resultCode, REQUEST_PLAY_SERVICES).show();
                } else {
                    Toast.makeText(context, context.getString(R.string.general_error_no_play_services), Toast.LENGTH_SHORT).show();
                }
            }
            return false;
        }
        return true;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (mListener != null) {
            mListener.connected();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        mRunning = false;
        if (connectionResult.hasResolution() && mActivityRef != null && mActivityRef.get() != null) {
            Activity activity = mActivityRef.get();
            try {
                connectionResult.startResolutionForResult(activity, REQUEST_RESOLVE_ERROR);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
                GoogleApiAvailability.getInstance().getErrorDialog(activity, connectionResult.getErrorCode(), 0).show();
            }
        } else if (connectionResult.getErrorCode() != 4) {
            if (mListener != null) {
                mListener.error();
            }
        }
    }

    public GoogleApiClient getClient() {
        return mGoogleApiClient;
    }

    public boolean isConnected() {
        return mGoogleApiClient != null && mGoogleApiClient.isConnected();
    }

    public void start() {
        if (mGoogleApiClient != null && !mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
    }

    public void stop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private DriveId findLastBackup() {
        DriveApi.MetadataBufferResult results = Drive.DriveApi.getAppFolder(mGoogleApiClient).listChildren(mGoogleApiClient).await();
        for (Metadata metadata : results.getMetadataBuffer()) {
            if (metadata.getMimeType() != null && metadata.getMimeType().equalsIgnoreCase("realmdatabase/json") && metadata.getDescription().equals(AccountProvider.getInstance().getAccountId())) {
                mRealmFilePath = metadata.getCustomProperties().get(new CustomPropertyKey(Constants.BACKUP_PATH, CustomPropertyKey.PRIVATE));
                String timestamp = metadata.getCustomProperties().get(new CustomPropertyKey(Constants.BACKUP_TIMESTAMP, CustomPropertyKey.PRIVATE));
                if (timestamp != null) {
                    mFileTimestamp = Long.parseLong(timestamp);
                }
                return metadata.getDriveId();
            }
        }

        return null;
    }

    public void backupRealm() {
        if (mRunning) {
            return;
        }

        Drive.DriveApi.newDriveContents(mGoogleApiClient).setResultCallback(new ResultCallbacks<DriveApi.DriveContentsResult>() {
            @Override
            public void onSuccess(@NonNull DriveApi.DriveContentsResult driveContentsResult) {
                if (driveContentsResult.getStatus().isSuccess()) {
                    createFile(driveContentsResult);
                }
            }

            @Override
            public void onFailure(@NonNull Status status) {
                mRunning = false;
                if (mListener != null) {
                    mListener.onBackupError(App.getContext().getString(R.string.backup_failed));
                }
            }
        });
    }

    private void createFile(DriveApi.DriveContentsResult driveContentsResult) {
        final DriveContents driveContents = driveContentsResult.getDriveContents();

        new Thread() {
            @Override
            public void run() {
                Status status = Drive.DriveApi.requestSync(mGoogleApiClient).await();
                if (status.getStatus().isSuccess() || status.getStatus().getStatus().getStatusCode() == DriveStatusCodes.DRIVE_RATE_LIMIT_EXCEEDED) {
                    final DriveId driveId = findLastBackup();
                    Realm realm = App.getRealm();
                    String realmFileName;
                    String realmPath;

                    File exportedFile;
                    //noinspection TryFinallyCanBeTryWithResources
                    try {
                        realmFileName = realm.getConfiguration().getRealmFileName();
                        realmPath = realm.getPath();

                        exportedFile = new File(mExportRealmPath, realmFileName);
                        //noinspection ResultOfMethodCallIgnored
                        exportedFile.delete();
                        realm.writeCopyTo(exportedFile);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        mRunning = false;
                        if (mListener != null) {
                            mListener.onBackupError(App.getContext().getString(R.string.backup_failed));
                        }
                        return;
                    } finally {
                        realm.close();
                    }

                    OutputStream outputStream = driveContents.getOutputStream();
                    FileInputStream fileInputStream = null;
                    try {
                        fileInputStream = new FileInputStream(exportedFile);
                        byte[] buffer = new byte[1024];
                        int bytesRead;
                        while ((bytesRead = fileInputStream.read(buffer)) != -1) {
                            outputStream.write(buffer, 0, bytesRead);
                        }
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } finally {
                        try {
                            outputStream.close();
                            if (fileInputStream != null) {
                                fileInputStream.close();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
                            .setTitle(realmFileName)
                            .setDescription(AccountProvider.getInstance().getAccountId())
                            .setMimeType("realmdatabase/json")
                            .setCustomProperty(new CustomPropertyKey(Constants.BACKUP_PATH, CustomPropertyKey.PRIVATE), realmPath)
                            .setCustomProperty(new CustomPropertyKey(Constants.BACKUP_TIMESTAMP, CustomPropertyKey.PRIVATE), String.valueOf(new Date().getTime()))
                            .build();

                    Drive.DriveApi.getAppFolder(mGoogleApiClient).createFile(mGoogleApiClient, changeSet, driveContents).setResultCallback(new ResultCallbacks<DriveFolder.DriveFileResult>() {
                        @Override
                        public void onSuccess(@NonNull DriveFolder.DriveFileResult driveFileResult) {
                            if (driveFileResult.getStatus().isSuccess()) {
                                mRunning = false;
                                if (mListener != null) {
                                    mListener.onBackupSuccess(App.getContext().getString(R.string.backup_done));
                                }
                                if (driveId != null) {
                                    driveId.asDriveFile().delete(mGoogleApiClient);
                                }
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Status status) {
                            mRunning = false;
                            if (mListener != null) {
                                mListener.onBackupError(App.getContext().getString(R.string.backup_failed));
                            }
                        }
                    });
                } else {
                    if (mListener != null) {
                        mListener.onBackupError(App.getContext().getString(R.string.backup_failed));
                    }
                }
            }
        }.start();
    }

    public void restoreRealm() {
        if (mRunning) {
            return;
        }
        readFile();
    }

    private void readFile() {
        new Thread() {
            @Override
            public void run() {
                Status status = Drive.DriveApi.requestSync(mGoogleApiClient).await();
                if (status.getStatus().isSuccess() || status.getStatus().getStatus().getStatusCode() == DriveStatusCodes.DRIVE_RATE_LIMIT_EXCEEDED) {
                    DriveId driveId = findLastBackup();
                    if (driveId != null) {
                        DriveApi.DriveContentsResult driveContentsResult = driveId.asDriveFile().open(mGoogleApiClient, DriveFile.MODE_READ_ONLY, null).await();
                        if (driveContentsResult.getStatus().isSuccess()) {
                            DriveContents driveContents = driveContentsResult.getDriveContents();

                            InputStream inputStream = driveContents.getInputStream();
                            File file = new File(mRealmFilePath);
                            OutputStream outputStream = null;
                            try {
                                outputStream = new FileOutputStream(file);
                                byte[] buffer = new byte[4 * 1024];
                                int read;

                                while ((read = inputStream.read(buffer)) != -1) {
                                    outputStream.write(buffer, 0, read);
                                }
                                outputStream.flush();
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            } finally {
                                try {
                                    if (outputStream != null) {
                                        outputStream.close();
                                    }
                                    inputStream.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }

                            mRunning = false;
                            if (mListener != null) {
                                mListener.onRestoreSuccess(App.getContext().getString(R.string.backup_restore_done), mFileTimestamp);
                            }
                        } else {
                            mRunning = false;
                            if (mListener != null) {
                                mListener.onRestoreError(App.getContext().getString(R.string.backup_restore_failed));
                            }
                        }
                    } else {
                        mRunning = false;
                        if (mListener != null) {
                            mListener.onRestoreNoBackup();
                        }
                    }
                } else {
                    if (mListener != null) {
                        mListener.onRestoreError(App.getContext().getString(R.string.backup_restore_failed));
                    }
                }
            }
        }.start();
    }

    public void setListener(DriveConnectionListener listener) {
        this.mListener = listener;
    }

    public interface DriveConnectionListener {
        void connected();

        void error();

        void onBackupSuccess(String testo);

        void onBackupError(String error);

        void onRestoreSuccess(String testo, long timestamp);

        void onRestoreError(String error);

        void onRestoreNoBackup();
    }
}
