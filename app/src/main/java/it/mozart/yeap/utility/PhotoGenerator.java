package it.mozart.yeap.utility;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import it.mozart.yeap.App;

public class PhotoGenerator {

    private Context mContext;

    public PhotoGenerator(Context context) {
        this.mContext = context;
    }

    private int getCapturedExifOrientation(String path) {
        try {
            ExifInterface exif = new ExifInterface(path);
            int result = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            if (result == ExifInterface.ORIENTATION_UNDEFINED) {
                String realmPhotoPath = getRealPathFromUri(Uri.parse("content://media" + path));
                exif = new ExifInterface(realmPhotoPath);
                return exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            }
            return result;
        } catch (IOException e) {
            return 0;
        }
    }

    private int getImageOrientation(int exifOrientation) {
        int rotate = 0;
        switch (exifOrientation) {
            case ExifInterface.ORIENTATION_ROTATE_270:
                rotate = 270;
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                rotate = 180;
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                rotate = 90;
                break;
            default:
                break;
        }
        return rotate;
    }

    private int getImageOrientation(String path) {
        int exif = getCapturedExifOrientation(path);
        return getImageOrientation(exif);
    }

    private Bitmap generatePhotoByPathWithMaxSize(String photoPath, int maxSize) throws FileNotFoundException {
        InputStream is = new FileInputStream(photoPath);
        BitmapFactory.Options options = generateBitmapFactoryOption(maxSize, is);
        is = new FileInputStream(photoPath);
        return BitmapFactory.decodeStream(is, null, options);
    }

    private Bitmap generatePhotoByPathWithMaxSize(Uri photoPath, int maxSize) throws FileNotFoundException {
        InputStream is = mContext.getContentResolver().openInputStream(photoPath);
        BitmapFactory.Options options = generateBitmapFactoryOption(maxSize, is);
        is = mContext.getContentResolver().openInputStream(photoPath);
        return BitmapFactory.decodeStream(is, null, options);
    }

    private String getRealPathFromUri(Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = App.getContext().getContentResolver().query(contentUri, proj, null, null, null);
            if (cursor != null) {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            }
            return null;
        } finally {
            if (cursor != null)
                cursor.close();
        }
    }

    @NonNull
    private BitmapFactory.Options generateBitmapFactoryOption(int maxSize, InputStream is) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        if (maxSize > 0) {
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(is, null, options);
            int photoWidth = options.outWidth;
            int photoHeight = options.outHeight;
            options.inSampleSize = Math.max(photoWidth / maxSize, photoHeight / maxSize);
        }
        options.inJustDecodeBounds = false;
        return options;
    }

    public Bitmap generatePhotoWithValue(String photoPath) throws FileNotFoundException {
        return generatePhotoWithValue(photoPath, 2000);
    }

    public Bitmap generatePhotoWithValue(String photoPath, int maxSize) throws FileNotFoundException {
        Bitmap generatingBitmap = generatePhotoByPathWithMaxSize(photoPath, maxSize);
        generatingBitmap = scalePhotoByMaxSize(maxSize, generatingBitmap);
        generatingBitmap = rotatePhotoIfNeed(generatingBitmap, photoPath);
        return generatingBitmap;
    }


    public Bitmap generatePhotoWithValue(Uri photoPath) throws FileNotFoundException {
        try {
            return generatePhotoWithValue(photoPath, 2000);
        } catch (Exception ex) {
            ex.printStackTrace();
            return generatePhotoWithValue(photoPath.getPath(), 2000);
        }
    }

    public Bitmap generatePhotoWithValue(Uri photoPath, int maxSize) throws FileNotFoundException {
        Bitmap generatingBitmap = generatePhotoByPathWithMaxSize(photoPath, maxSize);
        generatingBitmap = scalePhotoByMaxSize(maxSize, generatingBitmap);
        generatingBitmap = rotatePhotoIfNeed(generatingBitmap, photoPath.getPath());
        return generatingBitmap;
    }

    private Bitmap scalePhotoByMaxSize(int maxSize, Bitmap generatingBitmap) {
        if (maxSize <= 0) {
            return generatingBitmap;
        }

        int photoWidth = generatingBitmap.getWidth();
        int photoHeight = generatingBitmap.getHeight();

        if (photoWidth <= maxSize && photoHeight <= maxSize) {
            return generatingBitmap;
        }
        float scaleSize = Math.max((float) photoWidth / maxSize, (float) photoHeight / maxSize);
        generatingBitmap = Bitmap.createScaledBitmap(generatingBitmap, (int) (photoWidth / scaleSize), (int) (photoHeight / scaleSize), true);
        return generatingBitmap;
    }

    private Bitmap rotatePhotoIfNeed(Bitmap generatingBitmap, String photoPath) {
        try {
            int rotate = getImageOrientation(photoPath);
            if (rotate == 0) {
                return generatingBitmap;
            }

            Matrix matrix = new Matrix();
            matrix.postRotate(rotate);
            generatingBitmap = Bitmap.createBitmap(generatingBitmap, 0, 0, generatingBitmap.getWidth(), generatingBitmap.getHeight(), matrix, true);
        } catch (Exception ex) {
            Log.d("PHOTO_GENERATOR", "No need to rotate");
        }
        return generatingBitmap;
    }

    public File generateFileFrom(String photoPath) {
        return generateFileFrom(photoPath, 2000);
    }

    public File generateFileFrom(String photoPath, int maxSize) {
        try {
            Bitmap bitmap = generatePhotoWithValue(photoPath, maxSize);
            if (bitmap == null) {
                return null;
            }

            return generateFileFrom(bitmap);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public File generateFileFrom(Uri photoPath) {
        return generateFileFrom(photoPath, 2000);
    }

    public File generateFileFrom(Uri photoPath, int maxSize) {
        try {
            Bitmap bitmap = generatePhotoWithValue(photoPath, maxSize);
            if (bitmap == null) {
                return null;
            }

            return generateFileFrom(bitmap);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public File generateFileFrom(Bitmap bitmap) {
        try {
            File photoFile = File.createTempFile("IMAGE_" + System.currentTimeMillis() + ".jpg", null, mContext.getCacheDir());

            OutputStream os;
            os = new FileOutputStream(photoFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
            return photoFile;
        } catch (Exception e) {
            return null;
        }
    }

    public File generateFileFrom(byte[] bytes) {
        try {
            File photoFile = File.createTempFile("IMAGE_" + System.currentTimeMillis() + ".gif", null, mContext.getCacheDir());

            OutputStream os;
            os = new FileOutputStream(photoFile);
            os.write(bytes);
            os.flush();
            os.close();
            return photoFile;
        } catch (Exception e) {
            return null;
        }
    }
}
