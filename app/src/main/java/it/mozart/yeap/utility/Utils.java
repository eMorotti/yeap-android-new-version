package it.mozart.yeap.utility;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Looper;
import android.text.Html;
import android.text.Spanned;
import android.view.Display;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import it.mozart.yeap.BuildConfig;
import it.mozart.yeap.events.ImageSelectedEvent;

public class Utils {

    private static Pattern mPattern = Pattern.compile(Constants.URL_PATTERN, Pattern.CASE_INSENSITIVE);

    public static String getApiURL() {
        if (BuildConfig.DEBUG) {
            return "http://" + Constants.URL_DEV;
        } else {
            return "http://" + Constants.URL_PROD;
        }
    }

    public static String getApiURLSecure() {
        if (BuildConfig.DEBUG) {
            return "https://" + Constants.URL_DEV;
        } else {
            return "https://" + Constants.URL_PROD;
        }
    }

    public static String getWsURL() {
        if (BuildConfig.DEBUG) {
            return "http://" + Constants.URL_DEV_WS;
        } else {
            return "http://" + Constants.URL_PROD_WS;
        }
    }

    // Casting sicuro
    public static <T> T safeCast(Object o, Class<T> c) {
        if (c.isInstance(o)) {
            return c.cast(o);
        }
        return null;
    }

    // Controlla che la versione del dispositivo sia Lollipop o superiore
    public static boolean isLollipop() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    // Controlla che la versione del dispositivo sia Nougat o superiore
    public static boolean isNougat() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.N;
    }

    // Controlla che la versione del dispositivo sia Oreo o superiore
    public static boolean isOreo() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.O;
    }

    // Controlla che la versione del dispositivo sia Nougat o superiore
    public static boolean isNougatMR1() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1;
    }

    // Nasconde tastiera
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
    }

    // Controlla se il dispositivo è attualmente connesso a internet
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    // Controlla se su main Thread
    public static boolean isCurrentlyOnMainThread() {
        return Looper.myLooper() == Looper.getMainLooper();
    }

    // Recupera la dimensione in altezza della statusbar
    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    // Recupera la dimensione in larghezza dello schermo
    public static int screenWidth(Context context) {
        Display display;
        if (context instanceof Activity)
            display = ((Activity) context).getWindowManager().getDefaultDisplay();
        else
            display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }

    // Recupera la dimensione in altezza dello schermo
    public static int screenHeight(Context context) {
        Display display;
        if (context instanceof Activity)
            display = ((Activity) context).getWindowManager().getDefaultDisplay();
        else
            display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.y;
    }

    // Trasforma un valore da dp a pixel
    public static float dpToPx(float dp, Context context) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (dp * scale);
    }

    // Trasforma un valore da un range a un altro
    public static float toRange(float value, float oldMin, float oldMax, float newMin, float newMax) {
        return (((value - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin;
    }

    // Ritorna il valore compreso tra 2 estremi
    public static float clamp(float value, float start, float end) {
        if (value < start) {
            return start;
        } else if (value > end) {
            return end;
        } else {
            return value;
        }
    }

    public static Spanned fromHtml(String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(text);
        }
    }

    public static boolean checkIfImageExists(ImageSelectedEvent event) { // TODO
        if (event.isUrl()) {
            return true;
        } else {
//            File file = new File(event.getPath());
//            return file.exists();
            return true;
        }
    }

    public static String findUrlInText(String text) {
        Matcher matcher = mPattern.matcher(text);
        if (matcher.find()) {
            return matcher.group(0);
        }
        return null;
    }
}
