package it.mozart.yeap.utility;

import android.content.Context;

import java.net.SocketTimeoutException;

import it.mozart.yeap.R;
import it.mozart.yeap.api.models.APIError;
import retrofit2.HttpException;

public class APIErrorUtils {

    // Parsing errore chiamata API
    public static APIError parseError(Context context, Throwable e) {
        APIError error = new APIError(context.getString(R.string.general_error_realm));
        if (e == null) {
            return error;
        }
        e.printStackTrace();
        if (!Utils.isNetworkAvailable(context)) {
            error.setMessage(context.getString(R.string.general_error_internet));
        } else if (e instanceof SocketTimeoutException) {
            error.setMessage(context.getString(R.string.general_error_socket_timeout));
        } else if (e instanceof HttpException) {
            HttpException exception = (HttpException) e;
            error.setStatusCode(exception.code());
            if (exception.code() == 401) {
                error.setMessage(context.getString(R.string.general_error_api_401));
            } else if (exception.code() == 403) {
                error.setMessage(context.getString(R.string.general_error_api_403));
            } else if (exception.code() == 404) {
                error.setMessage(context.getString(R.string.general_error_api_404));
            } else if (exception.code() == 500) {
                error.setMessage(context.getString(R.string.general_error_api_500));
            }
        }

        return error;
    }
}
