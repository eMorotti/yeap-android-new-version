package it.mozart.yeap.utility;

import android.content.Context;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.common.QuandoInfo;

public class DateUtils {

    // Formatta una data
    public static String formatDate(Date date, String format) {
        return new SimpleDateFormat(format, Locale.getDefault()).format(date);
    }

    // Formatta una data
    public static String formatDate(DateTime date, String format) {
        return DateTimeFormat.forPattern(format).print(date);
    }

    // Formatta una data
    public static String formatDate(long date, String format) {
        return DateTimeFormat.forPattern(format).print(date);
    }

    // Formatta solo la data
    public static String formatDate(LocalDate date, String format) {
        return DateTimeFormat.forPattern(format).print(date);
    }

    // Formatta solo la data in maniera intelligiente
    public static String smartFormatDateOnly(Context context, long date, String format) {
        return smartFormatDate(context, date, format, null);
    }

    // Formatta solo la data in maniera intelligiente
    public static String smartFormatDateForLastMessage(Context context, long date, String format) {
        LocalDate now = DateTime.now().toLocalDate();
        LocalDate dateTime = new DateTime(date).toLocalDate();
        if (now.isEqual(dateTime)) {
            return formatDate(date, "HH:mm");
        } else if (now.minusDays(1).isEqual(dateTime)) {
            return context.getString(R.string.general_ieri);
        } else {
            if (now.getYear() == dateTime.getYear()) {
                return formatDate(date, format);
            } else {
                return formatDate(date, format + " yyyy");
            }
        }
    }

    // Formatta solo la data in maniera intelligiente (includere l'orario anche nel campo format)
    public static String smartFormatDate(Context context, long date, String format, String formatTime) {
        LocalDate now = DateTime.now().toLocalDate();
        LocalDate dateTime = new DateTime(date).toLocalDate();
        if (now.isEqual(dateTime)) {
            if (formatTime != null) {
                return context.getString(R.string.general_oggi) + formatDate(date, formatTime);
            } else {
                return context.getString(R.string.general_oggi);
            }
        } else if (now.minusDays(1).isEqual(dateTime)) {
            if (formatTime != null) {
                return context.getString(R.string.general_ieri) + formatDate(date, formatTime);
            } else {
                return context.getString(R.string.general_ieri);
            }
        } else if (now.plusDays(1).isEqual(dateTime)) {
            if (formatTime != null) {
                return context.getString(R.string.general_domani) + formatDate(date, formatTime);
            } else {
                return context.getString(R.string.general_domani);
            }
        } else {
            return formatDate(date, format);
        }
    }

    // Creo il testo per il Quando
    public static String createQuandoText(QuandoInfo quandoInfo) {
        if (quandoInfo == null) {
            return null;
        }

        int value = 0;
        if (quandoInfo.getDataDa() != null) {
            value += 1;
        }
        if (quandoInfo.getOraDa() != null) {
            value += 2;
        }
        if (quandoInfo.getDataA() != null) {
            value += 4;
        }
        if (quandoInfo.getOraA() != null) {
            value += 8;
        }
        String text = null;
        switch (value) {
            case 1:
                text = String.format(App.getContext().getString(R.string.system_quando_1_text_dda),
                        smartFormatDateOnly(App.getContext(), quandoInfo.getDataDa().getTime(), "dd MMMM yyyy"));
                break;
            case 2:
                text = String.format(App.getContext().getString(R.string.system_quando_2_text_hda), formatDate(quandoInfo.getOraDa(), "HH:mm"));
                break;
            case 3:
                text = String.format(App.getContext().getString(R.string.system_quando_3_text_dda_hda),
                        smartFormatDateOnly(App.getContext(), quandoInfo.getDataDa().getTime(), "dd MMMM yyyy"), formatDate(quandoInfo.getOraDa(), "HH:mm"));
                break;
            case 4:
                text = String.format(App.getContext().getString(R.string.system_quando_4_text_da),
                        smartFormatDateOnly(App.getContext(), quandoInfo.getDataA().getTime(), "dd MMMM yyyy"));
                break;
            case 5:
                text = String.format(App.getContext().getString(R.string.system_quando_5_text_dda_da),
                        smartFormatDateOnly(App.getContext(), quandoInfo.getDataDa().getTime(), "dd MMMM yyyy"),
                        smartFormatDateOnly(App.getContext(), quandoInfo.getDataA().getTime(), "dd MMMM yyyy"));
                break;
            case 6:
                text = String.format(App.getContext().getString(R.string.system_quando_6_text_hda_da),
                        formatDate(quandoInfo.getOraDa(), "HH:mm"),
                        smartFormatDateOnly(App.getContext(), quandoInfo.getDataA().getTime(), "dd MMMM yyyy"));
                break;
            case 7:
                text = String.format(App.getContext().getString(R.string.system_quando_7_text_dda_hda_da),
                        smartFormatDateOnly(App.getContext(), quandoInfo.getDataDa().getTime(), "dd MMMM yyyy"),
                        formatDate(quandoInfo.getOraDa(), "HH:mm"),
                        smartFormatDateOnly(App.getContext(), quandoInfo.getDataA().getTime(), "dd MMMM yyyy"));
                break;
            case 8:
                text = String.format(App.getContext().getString(R.string.system_quando_8_text_ha),
                        formatDate(quandoInfo.getOraA(), "HH:mm"));
                break;
            case 9:
                text = String.format(App.getContext().getString(R.string.system_quando_9_text_dda_ha),
                        smartFormatDateOnly(App.getContext(), quandoInfo.getDataDa().getTime(), "dd MMMM yyyy"),
                        formatDate(quandoInfo.getOraA(), "HH:mm"));
                break;
            case 10:
                text = String.format(App.getContext().getString(R.string.system_quando_10_text_hda_ha),
                        formatDate(quandoInfo.getOraDa(), "HH:mm"), formatDate(quandoInfo.getOraA(), "HH:mm"));
                break;
            case 11:
                text = String.format(App.getContext().getString(R.string.system_quando_11_text_dda_hda_ha),
                        smartFormatDateOnly(App.getContext(), quandoInfo.getDataDa().getTime(), "dd MMMM yyyy"),
                        formatDate(quandoInfo.getOraDa(), "HH:mm"), formatDate(quandoInfo.getOraA(), "HH:mm"));
                break;
            case 12:
                text = String.format(App.getContext().getString(R.string.system_quando_12_text_da_ha),
                        smartFormatDateOnly(App.getContext(), quandoInfo.getDataA().getTime(), "dd MMMM yyyy"),
                        formatDate(quandoInfo.getOraA(), "HH:mm"));
                break;
            case 13:
                text = String.format(App.getContext().getString(R.string.system_quando_13_text_dda_da_ha),
                        smartFormatDateOnly(App.getContext(), quandoInfo.getDataDa().getTime(), "dd MMMM yyyy"),
                        smartFormatDateOnly(App.getContext(), quandoInfo.getDataA().getTime(), "dd MMMM yyyy"),
                        formatDate(quandoInfo.getOraA(), "HH:mm"));
                break;
            case 14:
                text = String.format(App.getContext().getString(R.string.system_quando_14_text_hda_da_ha),
                        formatDate(quandoInfo.getOraDa(), "HH:mm"), smartFormatDateOnly(App.getContext(), quandoInfo.getDataA().getTime(), "dd MMMM yyyy"),
                        formatDate(quandoInfo.getOraA(), "HH:mm"));
                break;
            case 15:
                text = String.format(App.getContext().getString(R.string.system_quando_15_text_dda_hda_da_ha),
                        smartFormatDateOnly(App.getContext(), quandoInfo.getDataDa().getTime(), "dd MMMM yyyy"),
                        formatDate(quandoInfo.getOraDa(), "HH:mm"),
                        smartFormatDateOnly(App.getContext(), quandoInfo.getDataA().getTime(), "dd MMMM yyyy"),
                        formatDate(quandoInfo.getOraA(), "HH:mm"));
                break;
        }
        return text;
    }
}
