package it.mozart.yeap.utility.debounce;

public class NotificationInfo {

    private int type;
    private String id;

    public NotificationInfo(int type, String id) {
        this.type = type;
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public String getId() {
        return id;
    }
}
