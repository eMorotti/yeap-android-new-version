package it.mozart.yeap.utility.debounce;

import java.util.List;
import java.util.concurrent.TimeUnit;

import it.mozart.yeap.services.sync.NotificationsHelper;
import rx.BackpressureOverflow;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Action0;
import rx.functions.Action1;

public class NotificationUpdater {

    public void setNotificationObservable(final NotificationsHelper notifications, int bufferSize, int timeout) {
        Observable.create(new Observable.OnSubscribe<NotificationInfo>() {

            @Override
            public void call(final Subscriber<? super NotificationInfo> subscriber) {
                notifications.setNotificationListener(new NotificationListener() {
                    @Override
                    public void startNotification(NotificationInfo info) {
                        subscriber.onNext(info);
                    }
                });
            }
        }).onBackpressureBuffer(5, new Action0() {
            @Override
            public void call() {

            }
        }, BackpressureOverflow.ON_OVERFLOW_DROP_OLDEST).lift(new BufferTimeoutOp<NotificationInfo>(timeout, TimeUnit.MILLISECONDS, bufferSize))
                .subscribe(new Action1<List<NotificationInfo>>() {
                    @Override
                    public void call(List<NotificationInfo> notificationInfos) {
                        NotificationInfo info = notificationInfos.get(0);
                        notifications.createNotifications(info);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                    }
                });
    }
}
