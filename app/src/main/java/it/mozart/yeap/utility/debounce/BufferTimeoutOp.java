package it.mozart.yeap.utility.debounce;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable.Operator;
import rx.Producer;
import rx.Scheduler;
import rx.Subscriber;
import rx.exceptions.Exceptions;
import rx.functions.Action0;
import rx.observers.SerializedSubscriber;
import rx.schedulers.Schedulers;
import rx.subscriptions.SerialSubscription;

import static com.jakewharton.rxbinding.internal.Preconditions.checkArgument;

public class BufferTimeoutOp<T> implements Operator<List<T>, T> {
    final long timeout;
    final TimeUnit unit;
    private final int bufferSize;
    final Scheduler scheduler;

    /**
     * @param timeout    How long the oldest event could be buffered until the whole buffer gets published, regardless of the size.
     * @param unit       The unit of time for the specified timeout.
     * @param scheduler  The {@link Scheduler} to use internally to manage the timers which handle timeout for each event.
     * @param bufferSize Maximum size of the internal buffer, when the limit is reached it will published even before timeout.
     */
    public BufferTimeoutOp(long timeout, TimeUnit unit, int bufferSize, Scheduler scheduler) {
        this.timeout = timeout;
        this.unit = unit;
        this.bufferSize = bufferSize;
        this.scheduler = scheduler;
    }

    public BufferTimeoutOp(long timeout, TimeUnit unit, int bufferSize) {
        this(timeout, unit, bufferSize, Schedulers.newThread());
    }

    @Override
    public Subscriber<? super T> call(final Subscriber<? super List<T>> child) {
        final BufLimitWithTimeout<T> parent
                = new BufLimitWithTimeout<>(child, bufferSize, scheduler.createWorker(), timeout, unit);

        child.add(parent);
        child.setProducer(parent.createProducer());

        return parent;
    }

    static final class BufLimitWithTimeout<T> extends Subscriber<T> {
        private final SerializedSubscriber<? super List<T>> s;
        private final int bufferSize;
        private final rx.Scheduler.Worker worker;
        private final long timeout;
        private final TimeUnit unit;
        private boolean emitting = false;
        private boolean terminate = false;
        /**
         * Items requested by downstream
         */
        private long requested = 0L;


        /**
         * Guarded by this
         */
        private SerialSubscription sSub = new SerialSubscription();
        /**
         * Guarded by this.
         */
        private List<T> buffer = null;

        public BufLimitWithTimeout(
                final Subscriber<? super List<T>> child, int bufferSize, rx.Scheduler.Worker worker, long timeout, TimeUnit unit) {
            this.timeout = timeout;
            this.unit = unit;
            checkArgument(bufferSize > 0, "Buffer size must be greater than 0.");
            s = new SerializedSubscriber<>(child);
            s.add(worker);
            s.add(sSub);
            this.bufferSize = bufferSize;
            this.worker = worker;
        }

        @Override
        public void onCompleted() {
            flush();
        }

        @Override
        public void onError(Throwable e) {
            s.onError(e);
            unsubscribe();
            clear();
        }

        @Override
        public void onNext(T t) {
            List<T> localBuffer = buffer;
            boolean emit = false;

            synchronized (this) {
                if (buffer == null) {
                    localBuffer = (buffer = new ArrayList<>(bufferSize));
                    sSub.set(worker.schedule(new Action0() {
                        @Override
                        public void call() {
                            emit();
                        }
                    }, timeout, unit));
                }
                localBuffer.add(t);

                if (localBuffer.size() == bufferSize) {
                    buffer = null;
                    emit = true;
                }
            }

            if (emit) emit(localBuffer);
        }

        void emit(List<T> buffer) {
            boolean requestMore;
            synchronized (this) {
                if (emitting) return;
                emitting = true;
            }

            if (buffer != null && buffer.size() > 0) {
                try {
                    s.onNext(buffer);
                    synchronized (this) {
                        requested = Math.max(0, requested - 1);
                        requestMore = requested > 0L;
                    }
                    if (requestMore) request(bufferSize);
                } catch (Throwable e) {
                    Exceptions.throwOrReport(e, this, buffer);
                    return;
                }
            }

            synchronized (this) {
                if (!terminate) {
                    emitting = false;
                    return;
                }
            }
            s.onCompleted();
        }

        void emit() {
            List<T> localBuffer;
            synchronized (this) {
                localBuffer = buffer;
                buffer = null;
            }

            emit(localBuffer);
        }

        void flush() {
            synchronized (this) {
                terminate = true;
                if (emitting) return;
            }
            emit();
        }

        void clear() {
            synchronized (this) {
                buffer = null;
            }
        }

        Producer createProducer() {
            return new Producer() {
                @Override
                public void request(long n) {
                    checkArgument(n >= 0L, "n >= required but it was {}");
                    if (n != 0L) {
                        boolean requestInitial;
                        synchronized (BufLimitWithTimeout.this) {
                            requestInitial = requested == 0L;
                            requested = +n;
                        }
                        if (requestInitial) BufLimitWithTimeout.this.request(bufferSize);
                    }
                }
            };
        }
    }
}
