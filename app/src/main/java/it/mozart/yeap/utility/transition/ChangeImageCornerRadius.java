package it.mozart.yeap.utility.transition;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.transition.Transition;
import android.transition.TransitionValues;
import android.util.AttributeSet;
import android.view.ViewGroup;

import it.mozart.yeap.ui.views.TouchImageView;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class ChangeImageCornerRadius extends Transition {

    private static final String PROPNAME_CORNER = "it.mozart.yeap:ChangeImageCornerRadius:cornerRadius";

    public ChangeImageCornerRadius() {
    }

    public ChangeImageCornerRadius(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private void captureValues(TransitionValues values) {
        if (values.view.getTransitionName() != null && values.view instanceof TouchImageView) {
            values.values.put(PROPNAME_CORNER, ((TouchImageView) values.view).getCornerRadius());
        }
    }

    @Override
    public void captureStartValues(TransitionValues transitionValues) {
        captureValues(transitionValues);
    }

    @Override
    public void captureEndValues(TransitionValues transitionValues) {
        captureValues(transitionValues);
    }

    @Override
    public Animator createAnimator(ViewGroup sceneRoot, TransitionValues startValues, TransitionValues endValues) {
        if (startValues == null || endValues == null) {
            return null;
        }

        if (startValues.view.getTransitionName() == null || !(startValues.view instanceof TouchImageView)) {
            return null;
        }

        float start = (float) startValues.values.get(PROPNAME_CORNER);
        float end = (float) endValues.values.get(PROPNAME_CORNER);
        if (start == end) {
            return null;
        }

        return ObjectAnimator.ofFloat((TouchImageView) startValues.view, "cornerRadius", start, end);
    }
}
