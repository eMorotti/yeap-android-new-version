package it.mozart.yeap.utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;

public class SettingUtils {

    public static boolean checkIfAutodownloadEnabled() {
        return Prefs.getInt(Constants.SETTINGS_AUTODOWNLOAD, 0) != 2;
    }

    // Controllo se posso scaricare i media
    public static boolean checkIfCanDownload(Context context) {
        int setting = Prefs.getInt(Constants.SETTINGS_AUTODOWNLOAD, 0);
        ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        switch (setting) {
            case 1:
                // Controllo se si è connessi a un wifi
                if (Utils.isLollipop()) {
                    boolean isWifiConnected = false;
                    Network[] networks = connectivityManager.getAllNetworks();
                    if (networks != null) {
                        for (Network network : networks) {
                            NetworkInfo info = connectivityManager.getNetworkInfo(network);
                            if (info != null && info.getType() == ConnectivityManager.TYPE_WIFI) {
                                if (info.isAvailable() && info.isConnected()) {
                                    isWifiConnected = true;
                                    break;
                                }
                            }
                        }
                    }
                    return isWifiConnected;
                } else {
                    NetworkInfo info = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                    return info != null && info.isAvailable() && info.isConnected();
                }
            case 2:
                return false;
        }
        return true;
    }
}
