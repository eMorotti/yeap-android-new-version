package it.mozart.yeap.api.services;

import java.util.List;

import it.mozart.yeap.api.models.ContactApiModel;
import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;

public interface ContactApi {

    @POST("/contacts/sync")
    Observable<List<ContactApiModel>> sync(@Body() List<ContactApiModel> list);
}