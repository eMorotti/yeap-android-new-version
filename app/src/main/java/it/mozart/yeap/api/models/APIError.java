package it.mozart.yeap.api.models;

public class APIError {

    private int statusCode;
    private String message;

    public APIError() {
    }

    public APIError(String message) {
        this.message = message;
    }

    public int status() {
        return statusCode;
    }

    public String message() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
}
