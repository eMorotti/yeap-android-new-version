package it.mozart.yeap.api.services;

import java.util.List;

import it.mozart.yeap.api.models.GruppoApiModel;
import it.mozart.yeap.api.models.MessaggioApiModel;
import it.mozart.yeap.realm.Group;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

public interface GroupApi {

    @GET("/groups")
    Observable<List<Group>> getGroups();

    @GET("/group/{id}")
    Observable<Group> getGroup(@Path("id") String uuid);

    @POST("/group")
    Observable<Group> createGroup(@Header("request-id") String reqId, @Body() GruppoApiModel gruppo);

    @POST("/group/{id}/msg")
    Observable<Object> sendMessage(@Path("id") String groupUuid, @Body() MessaggioApiModel messaggio);

    @FormUrlEncoded
    @POST("/group/{id}/add-partecipant")
    Observable<Object> addPartecipant(@Header("request-id") String reqId, @Path("id") String groupUuid, @Field("userUuid") String userUuid, @Field("phone") String phone);

    @FormUrlEncoded
    @POST("/group/{id}/remove-partecipant")
    Observable<Object> removePartecipant(@Header("request-id") String reqId, @Path("id") String groupUuid, @Field("userUuid") String userUuid, @Field("phone") String phone);

    @POST("/group/{id}/exit")
    Observable<Object> exit(@Header("request-id") String reqId, @Path("id") String groupUuid);

    @POST("/group/{id}/update")
    Observable<Group> update(@Header("request-id") String reqId, @Path("id") String groupUuid, @Body() GruppoApiModel gruppo);
}
