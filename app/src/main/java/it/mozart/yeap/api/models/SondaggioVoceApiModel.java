package it.mozart.yeap.api.models;

import it.mozart.yeap.common.DoveInfo;
import it.mozart.yeap.common.QuandoInfo;
import it.mozart.yeap.utility.DateUtils;

public class SondaggioVoceApiModel {

    private String uuid;
    private String what;
    private String where;
    private String placeId;
    private double lat;
    private double lng;
    private String fromDate;
    private String fromHour;
    private String toDate;
    private String toHour;

    public SondaggioVoceApiModel(String what) {
        this.what = what;
    }


    public SondaggioVoceApiModel(DoveInfo doveInfo) {
        updateDoveInfo(doveInfo);
    }

    public SondaggioVoceApiModel(QuandoInfo quandoInfo) {
        updateQuandoInfo(quandoInfo);
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getWhat() {
        return what;
    }

    public void setWhat(String what) {
        this.what = what;
    }

    public String getWhere() {
        return where;
    }

    public void setWhere(String where) {
        this.where = where;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getFromHour() {
        return fromHour;
    }

    public void setFromHour(String fromHour) {
        this.fromHour = fromHour;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getToHour() {
        return toHour;
    }

    public void setToHour(String toHour) {
        this.toHour = toHour;
    }

    public void updateDoveInfo(DoveInfo doveModel) {
        if (doveModel == null) {
            where = null;
            placeId = null;
            lat = 0;
            lng = 0;
        } else {
            where = doveModel.getDove();
            placeId = doveModel.getPlaceId();
            lat = doveModel.getLat();
            lng = doveModel.getLng();
        }
    }

    public void updateQuandoInfo(QuandoInfo quandoModel) {
        if (quandoModel == null) {
            fromDate = null;
            fromHour = null;
            toDate = null;
            toHour = null;
        } else {
            if (quandoModel.getDataDa() != null) {
                fromDate = DateUtils.formatDate(quandoModel.getDataDa(), "yyyy-MM-dd");
            }
            if (quandoModel.getOraDa() != null) {
                fromHour = DateUtils.formatDate(quandoModel.getOraDa(), "HH:mm");
            }
            if (quandoModel.getDataA() != null) {
                toDate = DateUtils.formatDate(quandoModel.getDataA(), "yyyy-MM-dd");
            }
            if (quandoModel.getOraA() != null) {
                toHour = DateUtils.formatDate(quandoModel.getOraA(), "HH:mm");
            }
        }
    }
}
