package it.mozart.yeap.api.models;

public class ImageApiModel {

    private String uploadref;

    public String getUploadref() {
        return uploadref;
    }

    public void setUploadref(String uploadref) {
        this.uploadref = uploadref;
    }
}
