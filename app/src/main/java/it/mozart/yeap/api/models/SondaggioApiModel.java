package it.mozart.yeap.api.models;

import java.util.List;

import it.mozart.yeap.realm.PollItem;

public class SondaggioApiModel {

    private String uuid;
    private String question;
    private boolean usersCanAddItems;
    private List<PollItem> pollItems;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public boolean isUsersCanAddItems() {
        return usersCanAddItems;
    }

    public void setUsersCanAddItems(boolean usersCanAddItems) {
        this.usersCanAddItems = usersCanAddItems;
    }

    public List<PollItem> getPollItems() {
        return pollItems;
    }

    public void setPollItems(List<PollItem> pollItems) {
        this.pollItems = pollItems;
    }
}
