package it.mozart.yeap.api.services;

import it.mozart.yeap.api.models.SondaggioApiModel;
import it.mozart.yeap.api.models.SondaggioVoceApiModel;
import it.mozart.yeap.realm.Poll;
import it.mozart.yeap.realm.PollItem;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

public interface PollApi {

    //    @GET("/activity/{id}/polll/{id2}")
//    Observable<Sondaggio> getPoll(@Path("id") String eventUuid, @Path("id2") String uuid);
//
    @POST("/activity/{id}/poll")
    Observable<Poll> createPoll(@Header("request-id") String reqId, @Path("id") String eventUuid, @Body() SondaggioApiModel sondaggio);

    @DELETE("/activity/{id}/polls/{id2}")
    Observable<Object> deletePoll(@Header("request-id") String reqId, @Path("id") String eventUuid, @Path("id2") String uuid);

    @POST("/activity/{id}/polls/{id2}/item")
    Observable<PollItem> createPollItem(@Header("request-id") String reqId, @Path("id") String eventUuid, @Path("id2") String uuid, @Body() SondaggioVoceApiModel sondaggioVoce);

    @FormUrlEncoded
    @POST("/activity/{id}/polls/{id2}/items/{id3}/vote")
    Observable<Object> votePollItem(@Path("id") String eventUuid, @Path("id2") String pollUuid, @Path("id3") String uuid, @Field("type") int type);

    @DELETE("/activity/{id}/polls/{id2}/items/{id3}")
    Observable<Object> deletePollItem(@Header("request-id") String reqId, @Path("id") String eventUuid, @Path("id2") String pollUuid, @Path("id3") String uuid);
}
