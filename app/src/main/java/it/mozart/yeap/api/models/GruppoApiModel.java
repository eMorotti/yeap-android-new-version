package it.mozart.yeap.api.models;

import java.util.List;

public class GruppoApiModel {

    private String uuid;
    private String title;
    private String avatar;
    private String avatarThumbUrl;
    private String avatarLargeUrl;
    private Boolean usersCanInvite;
    private List<String> partecipants;
    private List<String> invited;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAvatarThumbUrl() {
        return avatarThumbUrl;
    }

    public void setAvatarThumbUrl(String avatarThumbUrl) {
        this.avatarThumbUrl = avatarThumbUrl;
    }

    public String getAvatarLargeUrl() {
        return avatarLargeUrl;
    }

    public void setAvatarLargeUrl(String avatarLargeUrl) {
        this.avatarLargeUrl = avatarLargeUrl;
    }

    public Boolean getUsersCanInvite() {
        return usersCanInvite;
    }

    public void setUsersCanInvite(Boolean usersCanInvite) {
        this.usersCanInvite = usersCanInvite;
    }

    public List<String> getPartecipants() {
        return partecipants;
    }

    public void setPartecipants(List<String> partecipants) {
        this.partecipants = partecipants;
    }

    public List<String> getInvited() {
        return invited;
    }

    public void setInvited(List<String> invited) {
        this.invited = invited;
    }
}
