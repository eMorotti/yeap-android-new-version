package it.mozart.yeap.api.models;

import java.util.List;

import it.mozart.yeap.realm.EventInfo;

public class EventoApiModel {

    private String uuid;
    private String title;
    private String desc;
    private String groupUuid;
    private String avatar;
    private String avatarThumbUrl;
    private String avatarLargeUrl;
    private Boolean usersCanInvite;
    private Boolean usersCanAddItems;
    private List<String> partecipants;
    private List<String> invited;
    private EventInfo event;
    private int status;
    private SondaggioApiModel wherePoll;
    private SondaggioApiModel whenPoll;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getGroupUuid() {
        return groupUuid;
    }

    public void setGroupUuid(String groupUuid) {
        this.groupUuid = groupUuid;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAvatarThumbUrl() {
        return avatarThumbUrl;
    }

    public void setAvatarThumbUrl(String avatarThumbUrl) {
        this.avatarThumbUrl = avatarThumbUrl;
    }

    public String getAvatarLargeUrl() {
        return avatarLargeUrl;
    }

    public void setAvatarLargeUrl(String avatarLargeUrl) {
        this.avatarLargeUrl = avatarLargeUrl;
    }

    public Boolean getUsersCanInvite() {
        return usersCanInvite;
    }

    public void setUsersCanInvite(Boolean usersCanInvite) {
        this.usersCanInvite = usersCanInvite;
    }

    public Boolean getUsersCanAddItems() {
        return usersCanAddItems;
    }

    public void setUsersCanAddItems(Boolean usersCanAddItems) {
        this.usersCanAddItems = usersCanAddItems;
    }

    public List<String> getPartecipants() {
        return partecipants;
    }

    public void setPartecipants(List<String> partecipants) {
        this.partecipants = partecipants;
    }

    public List<String> getInvited() {
        return invited;
    }

    public void setInvited(List<String> invited) {
        this.invited = invited;
    }

    public EventInfo getEventoInfo() {
        return event;
    }

    public void setEventoInfo(EventInfo evento) {
        this.event = evento;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public SondaggioApiModel getWherePoll() {
        return wherePoll;
    }

    public void setWherePoll(SondaggioApiModel wherePoll) {
        this.wherePoll = wherePoll;
    }

    public SondaggioApiModel getWhenPoll() {
        return whenPoll;
    }

    public void setWhenPoll(SondaggioApiModel whenPoll) {
        this.whenPoll = whenPoll;
    }
}
