package it.mozart.yeap.api.services;

import it.mozart.yeap.api.models.AccountApiModel;
import it.mozart.yeap.realm.User;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import rx.Observable;

public interface ProfileApi {

    @GET("/account/profile")
    Observable<User> getMyProfile();

    @POST("/account/profile")
    Observable<User> updateProfile(@Body() User utente);

    @FormUrlEncoded
    @POST("/account/pushtoken")
    Observable<Void> sendPushToken(@Field("pushtoken") String pushToken, @Field("device") String device);

    @POST("/account/login")
    Observable<AccountApiModel> login(@Body() AccountApiModel info);

    @POST("/account/logout")
    Observable<Object> logout();

    @POST("/account/delete")
    Observable<Object> deleteAccount();


    // TEST

//    @FormUrlEncoded
//    @POST("/test/login")
//    Observable<AccountApiModel> loginTest(@Field("phone") String phone, @Field("deviceId") String deviceId);
}
