package it.mozart.yeap.api.services;

import it.mozart.yeap.api.models.ImageApiModel;
import okhttp3.MultipartBody;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import rx.Observable;

public interface UploadApi {

    @Multipart
    @POST("/upload/avatar")
    Observable<ImageApiModel> uploadAvatar(@Part MultipartBody.Part image);

    @Multipart
    @POST("/upload/media-image")
    Observable<ImageApiModel> uploadMediaImage(@Part MultipartBody.Part image);
}
