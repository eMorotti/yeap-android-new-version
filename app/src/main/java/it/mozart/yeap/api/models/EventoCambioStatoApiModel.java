package it.mozart.yeap.api.models;

import it.mozart.yeap.realm.EventInfo;

public class EventoCambioStatoApiModel {

    private int status;
    private EventInfo event;

    public EventoCambioStatoApiModel() {
    }

    public EventoCambioStatoApiModel(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public EventInfo getEvent() {
        return event;
    }

    public void setEvent(EventInfo event) {
        this.event = event;
    }
}