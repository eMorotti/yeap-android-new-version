package it.mozart.yeap.api.services;

import java.util.List;

import it.mozart.yeap.api.models.EventoApiModel;
import it.mozart.yeap.api.models.EventoCambioStatoApiModel;
import it.mozart.yeap.api.models.MessaggioApiModel;
import it.mozart.yeap.api.models.SondaggioApiModel;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.EventInfo;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

public interface EventApi {

    @GET("/activities")
    Observable<List<Event>> getEvents();

    @GET("/activity/{id}")
    Observable<Event> getEvent(@Path("id") String uuid);

    @POST("/activity")
    Observable<Event> createEvent(@Header("request-id") String reqId, @Body() EventoApiModel evento);

    @POST("/activity/{id}/msg")
    Observable<Object> sendMessage(@Path("id") String eventUuid, @Body() MessaggioApiModel messaggio);

    @FormUrlEncoded
    @POST("/activity/{id}/add-partecipant")
    Observable<Object> addPartecipant(@Header("request-id") String reqId, @Path("id") String eventUuid, @Field("userUuid") String userUuid, @Field("phone") String phone);

    @FormUrlEncoded
    @POST("/activity/{id}/remove-partecipant")
    Observable<Object> removePartecipant(@Header("request-id") String reqId, @Path("id") String eventUuid, @Field("userUuid") String userUuid, @Field("phone") String phone);

    @POST("/activity/{id}/exit")
    Observable<Object> exit(@Header("request-id") String reqId, @Path("id") String eventUuid);

    @POST("/activity/{id}/update")
    Observable<Event> update(@Header("request-id") String reqId, @Path("id") String eventUuid, @Body() EventoApiModel evento);

    @POST("/activity/{id}/status")
    Observable<Event> changeStatus(@Header("request-id") String reqId, @Path("id") String eventUuid, @Body() EventoCambioStatoApiModel stato);

    @FormUrlEncoded
    @POST("/activity/{id}/event/vote")
    Observable<Object> voteEvent(@Path("id") String eventUuid, @Field("type") int voteType);

    @POST("/activity/{id}/where-poll/open")
    Observable<Object> openWherePoll(@Header("request-id") String reqId, @Path("id") String eventUuid, @Body() SondaggioApiModel sondaggio);

    @POST("/activity/{id}/where-poll/close")
    Observable<Object> closeWherePoll(@Header("request-id") String reqId, @Path("id") String eventUuid, @Body() EventInfo info);

    @POST("/activity/{id}/when-poll/open")
    Observable<Object> openWhenPoll(@Header("request-id") String reqId, @Path("id") String eventUuid, @Body() SondaggioApiModel sondaggio);

    @POST("/activity/{id}/when-poll/close")
    Observable<Object> closeWhenPoll(@Header("request-id") String reqId, @Path("id") String eventUuid, @Body() EventInfo info);
}
