package it.mozart.yeap.api.models;

import it.mozart.yeap.realm.EventInfo;

public class PropostaApiModel {

    private String uuid;
    private String what;
    private String where;
    private String placeId;
    private double lat;
    private double lng;
    private String fromHour;
    private String toHour;
    private String fromDate;
    private String toDate;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getWhat() {
        return what;
    }

    public void setWhat(String what) {
        this.what = what;
    }

    public String getWhere() {
        return where;
    }

    public void setWhere(String where) {
        this.where = where;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getFromHour() {
        return fromHour;
    }

    public void setFromHour(String fromHour) {
        this.fromHour = fromHour;
    }

    public String getToHour() {
        return toHour;
    }

    public void setToHour(String toHour) {
        this.toHour = toHour;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public boolean checkIfValid(EventInfo eventInfo) {
        boolean valid = false;
        if (what != null && !what.isEmpty() && !what.equalsIgnoreCase(eventInfo.getWhat())) {
            valid = true;
        }
        if (where != null && !where.isEmpty() && !where.equalsIgnoreCase(eventInfo.getWhere())) {
            valid = true;
        }
        if (fromHour != null && !fromHour.isEmpty() && !fromHour.equalsIgnoreCase(eventInfo.getFromHour())) {
            valid = true;
        }
        if (toHour != null && !toHour.isEmpty() && !toHour.equalsIgnoreCase(eventInfo.getToHour())) {
            valid = true;
        }
        if (fromDate != null && !fromDate.isEmpty() && !fromDate.equalsIgnoreCase(eventInfo.getFromDate())) {
            valid = true;
        }
        if (toDate != null && !toDate.isEmpty() && !toDate.equalsIgnoreCase(eventInfo.getToDate())) {
            valid = true;
        }
        return valid;
    }
}
