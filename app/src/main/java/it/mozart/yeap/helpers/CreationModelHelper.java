package it.mozart.yeap.helpers;

import java.util.ArrayList;
import java.util.UUID;

import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.api.models.SondaggioApiModel;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.EventInfo;
import it.mozart.yeap.realm.Group;
import it.mozart.yeap.realm.PollItem;
import it.mozart.yeap.utility.Constants;

public class CreationModelHelper {

    private Event event;
    private SondaggioApiModel wherePoll;
    private boolean wherePollSelected;
    private SondaggioApiModel whenPoll;
    private boolean whenPollSelected;
    private Group group;

    public void createEvent() {
        this.event = new Event();
        this.event.setUuid(UUID.randomUUID().toString());
        this.event.setEventInfo(new EventInfo());
        this.event.setUsersCanInvite(true);
        this.event.setUsersCanAddItems(true);
        this.event.setStatus(Constants.EVENTO_IN_PIANIFICAZIONE);

        this.wherePoll = new SondaggioApiModel();
        this.wherePoll.setPollItems(new ArrayList<PollItem>());
        this.wherePoll.setUsersCanAddItems(true);
        this.wherePoll.setQuestion(App.getContext().getString(R.string.sondaggio_dove_question));
        this.whenPoll = new SondaggioApiModel();
        this.whenPoll.setPollItems(new ArrayList<PollItem>());
        this.whenPoll.setUsersCanAddItems(true);
        this.whenPoll.setQuestion(App.getContext().getString(R.string.sondaggio_quando_question));

        this.wherePollSelected = false;
        this.whenPollSelected = false;
    }

    public void createGroup() {
        this.group = new Group();
        this.group.setUuid(UUID.randomUUID().toString());
        this.group.setUsersCanInvite(true);
    }

    public Event getEvent() {
        return event;
    }

    public SondaggioApiModel getWherePoll() {
        return wherePoll;
    }

    public boolean isWherePollSelected() {
        return wherePollSelected;
    }

    public SondaggioApiModel getWhenPoll() {
        return whenPoll;
    }

    public boolean isWhenPollSelected() {
        return whenPollSelected;
    }

    public Group getGroup() {
        return group;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public void setWherePollSelected(boolean wherePollSelected) {
        this.wherePollSelected = wherePollSelected;
    }

    public void setWhenPollSelected(boolean whenPollSelected) {
        this.whenPollSelected = whenPollSelected;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}
