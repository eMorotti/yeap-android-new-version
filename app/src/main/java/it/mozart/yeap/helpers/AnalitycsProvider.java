package it.mozart.yeap.helpers;

import android.os.Bundle;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.crashlytics.android.answers.InviteEvent;
import com.crashlytics.android.answers.LoginEvent;
import com.google.firebase.analytics.FirebaseAnalytics;

import it.mozart.yeap.BuildConfig;

public class AnalitycsProvider {

    private static AnalitycsProvider instance;

    private boolean enabled;

    private AnalitycsProvider() {
        enabled = !BuildConfig.DEBUG;
    }

    public static AnalitycsProvider getInstance() {
        if (instance == null) {
            instance = new AnalitycsProvider();
        }

        return instance;
    }

    public void logInvite(FirebaseAnalytics firebaseAnalytics, String method) {
        if (enabled) {
            Answers.getInstance().logInvite(new InviteEvent().putMethod(method));

            if (firebaseAnalytics != null) {
                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.ORIGIN, method);
                firebaseAnalytics.logEvent("invite", bundle);
            }
        }
    }

    public void logLogin(FirebaseAnalytics firebaseAnalytics, boolean success) {
        if (enabled) {
            Answers.getInstance().logLogin(new LoginEvent().putSuccess(success));

            if (firebaseAnalytics != null) {
                Bundle bundle = new Bundle();
                bundle.putBoolean(FirebaseAnalytics.Param.VALUE, success);
                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, bundle);
            }
        }
    }

    public void logLogin(FirebaseAnalytics firebaseAnalytics, boolean success, String method) {
        if (enabled) {
            Answers.getInstance().logLogin(new LoginEvent().putSuccess(success).putMethod(method));

            if (firebaseAnalytics != null) {
                Bundle bundle = new Bundle();
                bundle.putBoolean(FirebaseAnalytics.Param.VALUE, success);
                bundle.putString(FirebaseAnalytics.Param.ORIGIN, method);
                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, bundle);
            }
        }
    }

    public void logCustom(FirebaseAnalytics firebaseAnalytics, String name) {
        if (enabled) {
            Answers.getInstance().logCustom(new CustomEvent(name));

            if (firebaseAnalytics != null) {
                firebaseAnalytics.logEvent(name, new Bundle());
            }
        }
    }

    public void logCustomOrigin(FirebaseAnalytics firebaseAnalytics, String name, String origin) {
        if (enabled) {
            Answers.getInstance().logCustom(new CustomEvent(name).putCustomAttribute("origin", origin));

            if (firebaseAnalytics != null) {
                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.ORIGIN, origin);
                firebaseAnalytics.logEvent(name, bundle);
            }
        }
    }

    public void logCustomType(FirebaseAnalytics firebaseAnalytics, String name, String type) {
        if (enabled) {
            Answers.getInstance().logCustom(new CustomEvent(name).putCustomAttribute("type", type));

            if (firebaseAnalytics != null) {
                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, type);
                firebaseAnalytics.logEvent(name, bundle);
            }
        }
    }
}
