package it.mozart.yeap.helpers;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import it.mozart.yeap.R;
import it.mozart.yeap.ui.support.Act_RecuperaImage;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.Prefs;

public class ImageInputHelper {

    public static void openChooserForAvatarSelect(final Activity activity, final int CODE, boolean ignoreCheck) {
        boolean ignoreCamera = AndroidPermissionHelper.isCameraPresent(activity);
        if (!ignoreCamera && !ignoreCheck) {
            if (!AndroidPermissionHelper.alreadyAskedForPermission(Manifest.permission.CAMERA)) {
                Prefs.putBoolean(Manifest.permission.CAMERA, true);
                AndroidPermissionHelper.requestPermission(activity, Manifest.permission.CAMERA, Constants.IMAGE_PERMISSION_CODE);
                return;
            }
            if (!AndroidPermissionHelper.isPermissionGranted(activity, Manifest.permission.CAMERA)) {
                if (!AndroidPermissionHelper.shouldRequestPermission(activity, Manifest.permission.CAMERA)) {
                    activity.startActivityForResult(Intent.createChooser(openGallery(), activity.getString(R.string.system_image_picker_title)), CODE);
                    return;
                } else {
                    AndroidPermissionHelper.requestPermission(activity, Manifest.permission.CAMERA, Constants.IMAGE_PERMISSION_CODE);
                    return;
                }
            }
        }
        if (!ignoreCamera && AndroidPermissionHelper.isPermissionGranted(activity, Manifest.permission.CAMERA)) {
            activity.startActivityForResult(ImageInputHelper.getPickImageChooserIntent(activity), CODE);
        } else {
            activity.startActivityForResult(Intent.createChooser(openGallery(), activity.getString(R.string.system_image_picker_title)), CODE);
        }
    }

    public static void openChooserForAvatarSelect(final Fragment fragment, final int CODE, boolean ignoreCheck) {
        boolean ignoreCamera = AndroidPermissionHelper.isCameraPresent(fragment.getContext());
        if (!ignoreCamera && !ignoreCheck) {
            if (!AndroidPermissionHelper.alreadyAskedForPermission(Manifest.permission.CAMERA)) {
                Prefs.putBoolean(Manifest.permission.CAMERA, true);
                AndroidPermissionHelper.requestPermission(fragment, Manifest.permission.CAMERA, Constants.IMAGE_PERMISSION_CODE);
                return;
            }
            if (!AndroidPermissionHelper.isPermissionGranted(fragment.getContext(), Manifest.permission.CAMERA)) {
                if (!AndroidPermissionHelper.shouldRequestPermission(fragment, Manifest.permission.CAMERA)) {
                    fragment.startActivityForResult(Intent.createChooser(openGallery(), fragment.getString(R.string.system_image_picker_title)), CODE);
                    return;
                } else {
                    AndroidPermissionHelper.requestPermission(fragment, Manifest.permission.CAMERA, Constants.IMAGE_PERMISSION_CODE);
                    return;
                }
            }
        }
        if (!ignoreCamera && AndroidPermissionHelper.isPermissionGranted(fragment.getContext(), Manifest.permission.CAMERA)) {
            fragment.startActivityForResult(ImageInputHelper.getPickImageChooserIntent(fragment.getContext()), CODE);
        } else {
            fragment.startActivityForResult(Intent.createChooser(openGallery(), fragment.getString(R.string.system_image_picker_title)), CODE);
        }
    }

    public static void openChooserForChatImageSelect(Fragment fragment, boolean ignoreCheck) {
        boolean ignoreCamera = AndroidPermissionHelper.isCameraPresent(fragment.getContext());
        if (!ignoreCamera && !ignoreCheck) {
            if (!AndroidPermissionHelper.alreadyAskedForPermission(Manifest.permission.CAMERA)) {
                Prefs.putBoolean(Manifest.permission.CAMERA, true);
                AndroidPermissionHelper.requestPermission(fragment, Manifest.permission.CAMERA, Constants.IMAGE_PERMISSION_CODE);
                return;
            }
            if (!AndroidPermissionHelper.isPermissionGranted(fragment.getContext(), Manifest.permission.CAMERA)) {
                if (!AndroidPermissionHelper.shouldRequestPermission(fragment, Manifest.permission.CAMERA)) {
                    fragment.startActivityForResult(Intent.createChooser(openGallery(), fragment.getString(R.string.system_image_picker_title)), Constants.IMAGE_GALLERY_ACTIVITY_CODE);
                    return;
                } else {
                    AndroidPermissionHelper.requestPermission(fragment, Manifest.permission.CAMERA, Constants.IMAGE_PERMISSION_CODE);
                    return;
                }
            }
        }
        if (!ignoreCamera && AndroidPermissionHelper.isPermissionGranted(fragment.getContext(), Manifest.permission.CAMERA)) {
            fragment.startActivityForResult(new Intent(fragment.getContext(), Act_RecuperaImage.class), Constants.IMAGE_ACTIVITY_CODE);
        } else {
            fragment.startActivityForResult(Intent.createChooser(openGallery(), fragment.getString(R.string.system_image_picker_title)), Constants.IMAGE_GALLERY_ACTIVITY_CODE);
        }
    }

    public static void openGalleryIntent(Activity activity, int CODE) {
        activity.startActivityForResult(Intent.createChooser(openGallery(), activity.getString(R.string.system_image_picker_title)), CODE);
    }

    private static Intent openGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        return galleryIntent;
    }

    private static Intent getPickImageChooserIntent(Context context) {
        // Determine Uri of camera image to  save.
        Uri outputFileUri = getCaptureImageOutputUri(context);

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = context.getPackageManager();

        // collect all camera intents
        if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
            for (ResolveInfo res : listCam) {
                Intent intent = new Intent(captureIntent);
                intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                intent.setPackage(res.activityInfo.packageName);
                if (outputFileUri != null) {
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                }
                allIntents.add(intent);
            }
        }

        // collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        // the main intent is the last in the  list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main  intent
        Intent chooserIntent = Intent.createChooser(mainIntent, context.getString(R.string.system_image_picker_title));

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    private static Uri getCaptureImageOutputUri(Context context) {
        Uri outputFileUri = null;
        File getImage = context.getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    public static Uri getPickImageResultUri(Context context, Intent data) {
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri(context) : data.getData();
    }
}