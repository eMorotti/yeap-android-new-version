package it.mozart.yeap.helpers;

public class HomeScrollHelper {

    private boolean mScrollingEvents;
    private boolean mScrollingGroups;

    public HomeScrollHelper() {
    }

    public boolean isScrollingEvents() {
        return mScrollingEvents;
    }

    public void setScrollingEvents(boolean scrollingEvents) {
        this.mScrollingEvents = scrollingEvents;
    }

    public boolean isScrollingGroups() {
        return mScrollingGroups;
    }

    public void setScrollingGroups(boolean scrollingGroups) {
        this.mScrollingGroups = scrollingGroups;
    }
}
