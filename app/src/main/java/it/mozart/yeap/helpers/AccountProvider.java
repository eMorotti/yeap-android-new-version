package it.mozart.yeap.helpers;

import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.Prefs;

public class AccountProvider {

    private static AccountProvider instance;

    private String accountId;
    private String accountPhone;
    private String accessToken;
    private String deviceId;

    private AccountProvider() {
    }

    public static AccountProvider getInstance() {
        if (instance == null) {
            instance = new AccountProvider();
        }

        return instance;
    }

    public String getAccountId() {
        if (accountId == null) {
            accountId = Prefs.getString(Constants.PREF_PROFILE_ID);
        }
        return accountId;
    }

    public String getAccountPhone() {
        if (accountPhone == null) {
            accountPhone = Prefs.getString(Constants.PREF_PROFILE_PHONE);
        }
        return accountPhone;
    }

    public String getAccessToken() {
        if (accessToken == null) {
            accessToken = Prefs.getString(Constants.PREF_ACCESS_TOKEN);
        }
        return accessToken;
    }

    public String getDeviceId() {
        if (deviceId == null) {
            deviceId = Prefs.getString(Constants.PREF_DEVICE_ID);
        }
        return deviceId;
    }

    public boolean isLoggedIn() {
        return Prefs.getBoolean(Constants.LOGIN_COMPLETED);
    }

    public void reset() {
        accountId = null;
        accessToken = null;
        deviceId = null;
    }
}
