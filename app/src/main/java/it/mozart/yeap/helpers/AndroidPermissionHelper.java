package it.mozart.yeap.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import it.mozart.yeap.utility.Prefs;

public class AndroidPermissionHelper {

    public static boolean alreadyAskedForPermission(String permission) {
        return Prefs.getBoolean(permission, false);
    }

    public static boolean isCameraPresent(Context context) {
        boolean ignoreCamera = false;
        PackageManager packageManager = context.getPackageManager();
        if (!packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            ignoreCamera = true;
        }

        return ignoreCamera;
    }

    public static boolean isPermissionGranted(Activity activity, String permission) {
        return ActivityCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean isPermissionGranted(Context context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean shouldRequestPermission(Activity activity, String permission) {
        return ActivityCompat.shouldShowRequestPermissionRationale(activity, permission);
    }

    public static boolean shouldRequestPermission(Fragment fragment, String permission) {
        return fragment.shouldShowRequestPermissionRationale(permission);
    }

    public static void requestPermission(Activity activity, String permission, int code) {
        ActivityCompat.requestPermissions(activity, new String[]{permission}, code);
    }

    public static void requestPermission(Fragment fragment, String permission, int code) {
        fragment.requestPermissions(new String[]{permission}, code);
    }

    public static void requestPermissions(Activity activity, String[] permissions, int code) {
        ActivityCompat.requestPermissions(activity, permissions, code);
    }

    public static void requestPermissions(Fragment fragment, String[] permissions, int code) {
        fragment.requestPermissions(permissions, code);
    }
}
