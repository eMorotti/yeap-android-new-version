package it.mozart.yeap.services.syncAdapter;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.util.Log;

import it.mozart.yeap.R;

class SyncUtils {

    static Account isSyncAccountExists(Context ctx) {
        try {
            AccountManager accountManager = (AccountManager) ctx.getSystemService(Context.ACCOUNT_SERVICE);
            //noinspection MissingPermission
            Account a[] = accountManager.getAccountsByType(ctx.getString(R.string.sync_account_type));
            return a[0];
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    static void createSyncAccount(Context ctx, String username, String password, String email) throws Exception {
        // Create the account type and default account
        Account newAccount = new Account(ctx.getString(R.string.sync_account_name), ctx.getString(R.string.sync_account_type));

        // Get an instance of the Android account manager
        AccountManager accountManager = (AccountManager) ctx.getSystemService(Context.ACCOUNT_SERVICE);

        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
        if (accountManager.addAccountExplicitly(newAccount, null, null)) {
            String authority = ctx.getString(R.string.sync_authority);
            ContentResolver.setSyncAutomatically(newAccount, authority, true);

            Log.d("ACCOUNT", "CREATED");
        } else {
            Log.w("ACCOUNT", "ERROR");
        }
    }
}
