package it.mozart.yeap.services.sync.models;

import it.mozart.yeap.realm.SyncData;

public class SyncUnit {

    private String id;
    private SyncData body;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SyncData getBody() {
        return body;
    }

    public void setBody(SyncData body) {
        this.body = body;
    }
}
