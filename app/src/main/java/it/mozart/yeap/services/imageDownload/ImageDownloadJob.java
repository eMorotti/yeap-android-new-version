package it.mozart.yeap.services.imageDownload;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.CancelReason;
import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;

import java.io.File;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.helpers.AndroidPermissionHelper;
import it.mozart.yeap.realm.Message;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.services.Priority;
import it.mozart.yeap.utility.Constants;

public class ImageDownloadJob extends Job {

    private String mUUID;
    private String mPath;

    private static final String mDirectory = "/Yeap/YeapImages";

    public ImageDownloadJob(String uuid, String path) {
        super(new Params(Priority.LOW).requireNetwork().persist());
        mUUID = uuid;
        mPath = path;
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {
        if (!AndroidPermissionHelper.isPermissionGranted(App.getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            return;
        }

        File directory = new File(Environment.getExternalStorageDirectory(), mDirectory);
        if (!directory.isDirectory()) {
            if (!directory.mkdirs())
                throw new Exception();
        }

        String filename = "Picture_" + System.currentTimeMillis() + ".jpg";
        DownloadManager downloadManager = (DownloadManager) App.getContext().getSystemService(Context.DOWNLOAD_SERVICE);
        Uri downloadUri = Uri.parse(mPath);
        DownloadManager.Request request = new DownloadManager.Request(downloadUri);
        request.allowScanningByMediaScanner();
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false)
                .setTitle(filename)
                .setDescription(Constants.DOWNLOAD_PREFIX + mUUID)
                .setMimeType("image/jpeg")
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN)
                .setDestinationInExternalPublicDir(mDirectory, filename);

        downloadManager.enqueue(request);
    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {
        if (cancelReason == CancelReason.REACHED_RETRY_LIMIT) {
            Realm realm = App.getRealm();
            //noinspection TryFinallyCanBeTryWithResources
            try {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(@NonNull Realm realm) {
                        Message messaggio = new MessageDao(realm).getMessage(mUUID);
                        if (messaggio != null)
                            messaggio.setImageState(Constants.IMAGE_STATE_DOWNLOAD_FAILED);
                    }
                });
            } finally {
                realm.close();
            }
        }
    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        throwable.printStackTrace();
        return null;
    }

    @Override
    protected int getRetryLimit() {
        return 5;
    }
}
