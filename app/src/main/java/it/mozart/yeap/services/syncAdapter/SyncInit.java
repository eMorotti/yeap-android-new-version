package it.mozart.yeap.services.syncAdapter;

import android.accounts.Account;
import android.content.Context;
import android.os.AsyncTask;

public class SyncInit extends AsyncTask<Void, Void, Void> {

    private Context mCtx;

    public SyncInit(Context ctx) {
        this.mCtx = ctx;
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            Account account = SyncUtils.isSyncAccountExists(mCtx);
            if (account == null) {
                SyncUtils.createSyncAccount(mCtx, null, null, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
