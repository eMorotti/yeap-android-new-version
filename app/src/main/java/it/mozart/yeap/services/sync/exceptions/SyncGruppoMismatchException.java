package it.mozart.yeap.services.sync.exceptions;

public class SyncGruppoMismatchException extends Exception {

    private String gruppoId;

    public SyncGruppoMismatchException(String gruppoId) {
        super();
        this.gruppoId = gruppoId;
    }

    public String getGruppoId() {
        return gruppoId;
    }
}
