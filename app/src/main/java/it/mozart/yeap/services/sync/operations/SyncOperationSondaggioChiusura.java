package it.mozart.yeap.services.sync.operations;

import com.google.gson.Gson;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.common.DoveInfo;
import it.mozart.yeap.common.QuandoInfo;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.EventInfo;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.services.sync.exceptions.SyncEventoMismatchException;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.DateUtils;

public class SyncOperationSondaggioChiusura extends SyncOperation {

    private ChiusuraSondaggioModel mChiusuraSondaggioModel;

    public SyncOperationSondaggioChiusura(SyncData syncData) {
        super(syncData);
    }

    @Override
    public void parseModel() {
        mChiusuraSondaggioModel = new Gson().fromJson(mSyncDataContent, ChiusuraSondaggioModel.class);
    }

    @Override
    public void doJob(Realm realm) throws Exception {
        if (mChiusuraSondaggioModel == null)
            throw new Exception("Model is null");

        EventDao eventoDao = new EventDao(realm);

        Event evento = eventoDao.getEvent(mChiusuraSondaggioModel.activityUuid);
        if (evento == null) {
            throw new SyncEventoMismatchException(mChiusuraSondaggioModel.activityUuid);
        }

        if (evento.getVersion() < mChiusuraSondaggioModel.activityVersion - 1) {
            throw new SyncEventoMismatchException(mChiusuraSondaggioModel.activityUuid);
        }

        evento.setVersion(mChiusuraSondaggioModel.activityVersion);

        DoveInfo doveInfo = null;
        QuandoInfo quandoInfo = null;
        if (mChiusuraSondaggioModel.pollType.equals("where")) {
            if (mChiusuraSondaggioModel.updates != null && mChiusuraSondaggioModel.updates.getWhere() != null) {
                evento.getEventInfo().updateDoveInfo(mChiusuraSondaggioModel.updates.generateDoveModel());
                doveInfo = mChiusuraSondaggioModel.updates.generateDoveModel();
            } else {
                doveInfo = evento.getEventInfo().generateDoveModel();
            }
            evento.setWherePollEnabled(false);
        } else {
            if (mChiusuraSondaggioModel.updates != null && mChiusuraSondaggioModel.updates.isQuandoPresent()) {
                evento.getEventInfo().updateQuandoInfo(mChiusuraSondaggioModel.updates.generateQuandoModel());
                quandoInfo = mChiusuraSondaggioModel.updates.generateQuandoModel();
            } else {
                quandoInfo = evento.getEventInfo().generateQuandoModel();
            }
            evento.setWhenPollEnabled(false);
        }

        evento.setUpdatedAt(mChiusuraSondaggioModel.timestamp);

        letto = (App.isEventVisible(mChiusuraSondaggioModel.activityUuid) && App.isEventPageVisible(Constants.EVENT_PAGE_DETAILS))
                || mChiusuraSondaggioModel.makerUuid.equals(AccountProvider.getInstance().getAccountId());
        new MessageDao(realm).createSystemMessagePollClosed(evento, new UserDao(realm).getUser(mChiusuraSondaggioModel.makerUuid),
                doveInfo != null ? doveInfo.getDove() : DateUtils.createQuandoText(quandoInfo),
                letto).setCreatedAt(mChiusuraSondaggioModel.timestamp);
    }

    @Override
    public boolean createNotifications(NotificationsHelper notifications) {
        return false;
    }

    private static class ChiusuraSondaggioModel {
        String activityUuid;
        int activityVersion;
        String makerUuid;
        String pollType;
        long timestamp;
        //        Event activity;
        EventInfo updates;
    }
}
