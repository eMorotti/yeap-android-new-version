package it.mozart.yeap.services.sync.operations;

import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import io.realm.Realm;
import it.mozart.yeap.events.UserAvatarChangedEvent;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.Prefs;

public class SyncOperationUtenteAvatarCambiato extends SyncOperation {

    private UserAvatarCambiatoModel mUserAvatarCambiatoModel;

    public SyncOperationUtenteAvatarCambiato(SyncData syncData) {
        super(syncData);
    }

    @Override
    public void parseModel() {
        mUserAvatarCambiatoModel = new Gson().fromJson(mSyncDataContent, UserAvatarCambiatoModel.class);
    }

    @Override
    public void doJob(Realm realm) throws Exception {
        if (mUserAvatarCambiatoModel == null)
            throw new Exception("Model is null");

        int currentVersion = Integer.parseInt(Prefs.getString(mUserAvatarCambiatoModel.userUuid, "0"));
        Prefs.putString(mUserAvatarCambiatoModel.userUuid, String.valueOf(currentVersion + 1));

        EventBus.getDefault().post(new UserAvatarChangedEvent(mUserAvatarCambiatoModel.userUuid));
    }

    @Override
    public boolean createNotifications(NotificationsHelper notifications) {
        // Invio notifiche
        if (letto) {
            notifications.setInfo(Constants.TYPE_EVENTO, uuid);
        }
        return letto;
    }

    private static class UserAvatarCambiatoModel {
        String userUuid;
    }
}
