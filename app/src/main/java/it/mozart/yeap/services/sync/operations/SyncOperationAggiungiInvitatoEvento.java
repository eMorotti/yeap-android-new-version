package it.mozart.yeap.services.sync.operations;

import com.google.gson.Gson;

import io.realm.Realm;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.services.sync.exceptions.SyncEventoMismatchException;

public class SyncOperationAggiungiInvitatoEvento extends SyncOperation {

    private AggiungiPartecipanteEventoModel mAggiungiPartecipanteModel;

    public SyncOperationAggiungiInvitatoEvento(SyncData syncData) {
        super(syncData);
    }

    @Override
    public void parseModel() {
        mAggiungiPartecipanteModel = new Gson().fromJson(mSyncDataContent, AggiungiPartecipanteEventoModel.class);
    }

    @Override
    public void doJob(Realm realm) throws Exception {
        if (mAggiungiPartecipanteModel == null)
            throw new Exception("Model is null");

        EventDao eventDao = new EventDao(realm);
        UserDao utenteDao = new UserDao(realm);

        Event evento = eventDao.getEvent(mAggiungiPartecipanteModel.activityUuid);
        if (evento == null) {
            throw new SyncEventoMismatchException(mAggiungiPartecipanteModel.activityUuid);
        }

        if (evento.getVersion() < mAggiungiPartecipanteModel.activityVersion - 1 || mAggiungiPartecipanteModel.phone == null) {
            throw new SyncEventoMismatchException(mAggiungiPartecipanteModel.activityUuid);
        }

        if (manageUserAlreadyPresent(evento, utenteDao)) {
            return;
        }

        evento.setVersion(mAggiungiPartecipanteModel.activityVersion);
        uuid = evento.getUuid();

        // Recupero utenti
        User maker = utenteDao.getUser(mAggiungiPartecipanteModel.makerUuid);
        if (maker == null) {
            throw new SyncEventoMismatchException(mAggiungiPartecipanteModel.activityUuid);
        }
        User utente = utenteDao.getUserLocal(mAggiungiPartecipanteModel.phone);

        // Creo messaggio
        new MessageDao(realm).createSystemMessageEventInvitedAdded(evento, maker, utente, true).setCreatedAt(mAggiungiPartecipanteModel.timestamp);
    }

    @Override
    public boolean createNotifications(NotificationsHelper notifications) {
        return false;
    }

    private boolean manageUserAlreadyPresent(Event evento, UserDao utenteDao) {
        // Creo utente se non presente
        User newUtente = utenteDao.getUserLocal(mAggiungiPartecipanteModel.phone);
        if (newUtente != null) {
            newUtente.setInYeap(false);
            // Aggiungo in invitati
            if (evento.getGuestsNotYeap().contains(newUtente)) {
                return true;
            } else {
                evento.getGuestsNotYeap().add(0, newUtente);
            }
        }

        return false;
    }

    private static class AggiungiPartecipanteEventoModel {
        String activityUuid;
        int activityVersion;
        String makerUuid;
        long timestamp;
        String phone;
    }
}
