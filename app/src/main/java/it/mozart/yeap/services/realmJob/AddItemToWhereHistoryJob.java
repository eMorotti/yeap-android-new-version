package it.mozart.yeap.services.realmJob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.common.DoveInfo;
import it.mozart.yeap.realm.dao.WhereHistoryDao;
import it.mozart.yeap.services.Priority;

public class AddItemToWhereHistoryJob extends Job {

    private DoveInfo mDoveInfo;

    public AddItemToWhereHistoryJob(DoveInfo doveInfo) {
        super(new Params(Priority.HIGH));
        mDoveInfo = doveInfo;
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {
        Realm realm = App.getRealm();
        //noinspection TryFinallyCanBeTryWithResources
        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(@NonNull Realm realm) {
                    new WhereHistoryDao(realm).createOrUpdate(mDoveInfo);
                }
            });
        } finally {
            realm.close();
        }
    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {

    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        return null;
    }
}
