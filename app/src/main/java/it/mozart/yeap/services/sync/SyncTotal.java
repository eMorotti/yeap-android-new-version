package it.mozart.yeap.services.sync;

import android.support.annotation.NonNull;

import java.util.List;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import it.mozart.yeap.App;
import it.mozart.yeap.api.services.EventApi;
import it.mozart.yeap.api.services.GroupApi;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.Group;
import it.mozart.yeap.realm.Poll;
import it.mozart.yeap.realm.PollItem;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.Vote;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.GroupDao;
import it.mozart.yeap.realm.dao.PollDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.realm.dao.VoteDao;
import it.mozart.yeap.utility.Constants;
import rx.functions.Action1;

public class SyncTotal {

    static void syncEventoInJob(final Realm realm, EventApi eventApi, String uuid) {
        if (App.isSyncing())
            return;

        App.setSyncing(true);
        App.jobManager().stop();

        eventApi.getEvent(uuid)
                .toBlocking()
                .subscribe(new Action1<Event>() {
                    @Override
                    public void call(Event evento) {
                        syncEvento(realm, evento);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                    }
                });

        App.setSyncing(false);
        App.jobManager().start();
    }

    static void syncGruppiInJob(final Realm realm, GroupApi groupApi, String uuid) {
        if (App.isSyncing())
            return;

        App.setSyncing(true);
        App.jobManager().stop();

        groupApi.getGroup(uuid)
                .toBlocking()
                .subscribe(new Action1<Group>() {
                    @Override
                    public void call(Group gruppo) {
                        syncGruppo(realm, gruppo);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                    }
                });

        App.setSyncing(false);
        App.jobManager().start();
    }

    public static Group syncGruppo(Realm realm, final Group gruppoInfo) {
        final GroupDao gruppoDao = new GroupDao(realm);
        final UserDao utenteDao = new UserDao(realm);
        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(@NonNull Realm realm) {
                    syncGruppo(gruppoInfo, gruppoDao, utenteDao);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return gruppoDao.getGroup(gruppoInfo.getUuid());
    }

    public static Group syncGruppo(final Group gruppoInfo, GroupDao gruppoDao, UserDao utenteDao) {
        boolean groupAlreadyPresent = false;

        RealmList<User> partecipanti = manageGuests(utenteDao, gruppoInfo.getUsers(), gruppoInfo.getPartecipants());
        RealmList<User> invitati = null;
        if (gruppoInfo.getInvited() != null && gruppoInfo.getInvited().size() != 0) {
            invitati = manageInvited(utenteDao, gruppoInfo.getInvited());
        }
        Group gruppo = gruppoDao.getGroup(gruppoInfo.getUuid());
        if (gruppo == null) {
            gruppo = new Group();
            gruppo.setUuid(gruppoInfo.getUuid());
        } else {
            groupAlreadyPresent = true;
        }

        gruppo.setTitle(gruppoInfo.getTitle());
        gruppo.setAvatarThumbUrl(gruppoInfo.getAvatarThumbUrl());
        gruppo.setAvatarLargeUrl(gruppoInfo.getAvatarLargeUrl());
        gruppo.setUsersCanInvite(gruppoInfo.isUsersCanInvite());
        gruppo.setVersion(gruppoInfo.getVersion());
        gruppo.setCreatedAt(gruppoInfo.getCreatedAt());
        gruppo.setGuests(partecipanti);
        if (gruppoInfo.getInvited() != null && gruppoInfo.getInvited().size() != 0) {
            gruppo.setGuestsNotYeap(invitati);
        }
        gruppo.setAdmin(utenteDao.getUser(gruppoInfo.getAdminUuid()));
        gruppo.setCreator(utenteDao.getUser(gruppoInfo.getCreatorUuid()));
        gruppo.setMyselfPresent(gruppoInfo.getPartecipants().contains(AccountProvider.getInstance().getAccountId()));

        if (!groupAlreadyPresent) {
            gruppo = gruppoDao.creaGruppo(gruppo, utenteDao.getUser(gruppoInfo.getCreatorUuid()), true);
        }

        return gruppo;
    }

    public static Event syncEvento(Realm realm, final Event eventoInfo) {
        final EventDao eventDao = new EventDao(realm);
        final UserDao userDao = new UserDao(realm);
        final VoteDao voteDao = new VoteDao(realm);
        final PollDao sondaggioDao = new PollDao(realm);

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                syncEvento(realm, eventoInfo, eventDao, userDao, voteDao, sondaggioDao);
            }
        });

        return eventDao.getEvent(eventoInfo.getUuid());
    }

    public static Event syncEvento(Realm realm, final Event eventoInfo, EventDao eventDao, UserDao userDao, VoteDao voteDao, PollDao sondaggioDao) {
        boolean eventAlreadyPresent = false;

        // Creo l'evento
        RealmList<User> partecipanti = manageGuests(userDao, eventoInfo.getUsers(), eventoInfo.getPartecipants());
        RealmList<User> invitati = null;
        if (eventoInfo.getInvited() != null && eventoInfo.getInvited().size() != 0) {
            invitati = manageInvited(userDao, eventoInfo.getInvited());
        }
        Event evento = eventDao.getEvent(eventoInfo.getUuid());
        if (evento == null) {
            evento = new Event();
            evento.setUuid(eventoInfo.getUuid());
        } else {
            eventAlreadyPresent = true;
            voteDao.deleteAllVoteForEvent(evento.getEventInfo().getUuid());
            evento.getEventInfo().deleteFromRealm();
        }

        if (eventoInfo.getEventInfo() != null) {
            evento.setEventInfo(realm.copyToRealm(eventoInfo.getEventInfo()));
        }
        evento.setDesc(eventoInfo.getDesc());
        evento.setUsersCanInvite(eventoInfo.isUsersCanInvite());
        evento.setUsersCanAddItems(eventoInfo.isUsersCanAddItems());
        evento.setAvatarThumbUrl(eventoInfo.getAvatarThumbUrl());
        evento.setAvatarLargeUrl(eventoInfo.getAvatarLargeUrl());
        evento.setStatus(eventoInfo.getStatus());
        evento.setVersion(eventoInfo.getVersion());
        evento.setCreatedAt(eventoInfo.getCreatedAt());
        if (eventoInfo.getGroupUuid() != null) {
            evento.setGroupUuid(eventoInfo.getGroupUuid());
        }
        evento.setGuests(partecipanti);
        if (eventoInfo.getInvited() != null && eventoInfo.getInvited().size() != 0) {
            evento.setGuestsNotYeap(invitati);
        }
        evento.setAdmin(userDao.getUser(eventoInfo.getAdminUuid()));
        evento.setCreator(userDao.getUser(eventoInfo.getCreatorUuid()));
        evento.setMyselfPresent(eventoInfo.getPartecipants().contains(AccountProvider.getInstance().getAccountId()));

        if (!eventAlreadyPresent) {
            evento = eventDao.createEvent(evento, userDao.getUser(eventoInfo.getCreatorUuid()), true);
        }

        syncEventoVoti(userDao, voteDao, evento, eventoInfo);

        evento.setWherePollEnabled(eventoInfo.isWherePollEnabled());
        evento.setWherePollUuid(eventoInfo.getWherePollUuid());
        evento.setWhenPollEnabled(eventoInfo.isWhenPollEnabled());
        evento.setWhenPollUuid(eventoInfo.getWhenPollUuid());

        // Creo sondaggi
        for (Poll sondaggioInfo : eventoInfo.getPolls()) {
            Poll sondaggio = sondaggioDao.getPoll(sondaggioInfo.getUuid());
            boolean notPresent = false;
            if (sondaggio == null) {
                notPresent = true;
                sondaggio = new Poll();
                sondaggio.setUuid(sondaggioInfo.getUuid());
            }
            syncSondaggio(userDao, sondaggio, sondaggioInfo, evento);

            if (notPresent) {
                if (sondaggio.getType() == Constants.POLL_TYPE_WHERE || sondaggio.getType() == Constants.POLL_TYPE_WHEN) {
                    sondaggio = sondaggioDao.createPoll(sondaggio, true, false, true);
                } else {
                    sondaggio = sondaggioDao.createPoll(sondaggio, false, false, true);
                }
            }

            for (PollItem sondaggioVoceInfo : sondaggioInfo.getPollItems()) {
                PollItem sondaggioVoce = sondaggioDao.getPollItem(sondaggioVoceInfo.getUuid());
                boolean notPresentBis = false;
                if (sondaggioVoce == null) {
                    notPresentBis = true;
                    sondaggioVoce = new PollItem();
                    sondaggioVoce.setUuid(sondaggioVoceInfo.getUuid());
                }
                syncSondaggioVoce(userDao, sondaggioVoce, sondaggioVoceInfo, sondaggio);

                if (notPresentBis) {
                    if (sondaggio.getType() == Constants.POLL_TYPE_WHERE || sondaggio.getType() == Constants.POLL_TYPE_WHEN) {
                        sondaggioVoce = sondaggioDao.createPollItem(sondaggioVoce, true, false, true);
                    } else {
                        sondaggioVoce = sondaggioDao.createPollItem(sondaggioVoce, false, false, true);
                    }
                } else {
                    sondaggio.getVoci().add(sondaggioVoce);
                }

                syncSondaggioVoceVoti(userDao, voteDao, sondaggioVoce, sondaggioVoceInfo);
            }

            if (sondaggio.getCreatedAt() > evento.getUpdatedAt()) {
                evento.setUpdatedAt(sondaggio.getCreatedAt());
            }
        }

        return evento;
    }

    public static RealmList<User> manageGuests(UserDao userDao, List<User> users, final List<String> partecipants) {
        final RealmList<User> invitati = new RealmList<>();
        if (partecipants == null) {
            return invitati;
        }

        for (final User contatto : users) {
            try {
                manageRemoteUser(userDao, contatto, new AddToListListener() {
                    @Override
                    public void add(User user) {
                        if (partecipants.contains(contatto.getUuid())) {
                            invitati.add(user);
                        }
                    }
                });
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        return invitati;
    }

    public static void manageRemoteUser(UserDao userDao, User contatto, AddToListListener listener) {
        User utente = userDao.getUserNotDeleted(contatto.getUuid());
        User utenteLocal = userDao.getUserLocal(contatto.getPhone());
        if (utente != null) { // utente esiste
            if (!utente.isMe()) {
                utente.setNickname(contatto.getNickname());
                utente.setPhone(contatto.getPhone());
            }

            if (listener != null) {
                listener.add(utente);
            }
        } else { // utente non esiste
            if (utenteLocal != null) { // utente locale esiste
                User newUtente = new User();
                newUtente.setId(utenteLocal.getId());
                newUtente.setContactId(utenteLocal.getId());
                newUtente.setUuid(contatto.getUuid());
                newUtente.setName(utenteLocal.getName());
                newUtente.setSurname(utenteLocal.getSurname());
                newUtente.setNickname(contatto.getNickname());
                newUtente.setPhone(contatto.getPhone());
                newUtente.setInYeap(true);
                newUtente.setInContacts(true);

                utenteLocal.deleteFromRealm();
                newUtente = userDao.createUser(newUtente);

                if (listener != null) {
                    listener.add(newUtente);
                }
            } else { // utente locale non esiste
                User newUtente = new User();
                newUtente.setId(String.format(Locale.getDefault(), "%s%d", contatto.getPhone(), System.currentTimeMillis()));
                newUtente.setPhone(contatto.getPhone());
                newUtente.setUuid(contatto.getUuid());
                newUtente.setNickname(contatto.getNickname());
                newUtente.setInYeap(true);

                newUtente = userDao.createUser(newUtente);

                if (listener != null) {
                    listener.add(newUtente);
                }
            }
        }
    }

    public static RealmList<User> manageInvited(UserDao userDao, List<String> invited) {
        RealmList<User> invitati = new RealmList<>();
        if (invited == null) {
            return invitati;
        }

        for (String phone : invited) {
            User newUtente = userDao.getUserLocal(phone);
            if (newUtente != null) {
                newUtente.setInYeap(false);
                // Aggiungo in invitati
                if (invited.contains(phone)) {
                    invitati.add(newUtente);
                }
            }
        }

        return invitati;
    }

    public static void syncEventoVoti(UserDao userDao, VoteDao voteDao, Event evento, Event eventoInfo) {
        RealmResults<Vote> voti = voteDao.getListVoteEventYes(eventoInfo.getUuid());
        for (Vote voto : voti) {
            voto.setDeleted(true);
        }
        RealmResults<Vote> votiNo = voteDao.getListVoteEventNo(eventoInfo.getUuid());
        for (Vote voto : votiNo) {
            voto.setDeleted(true);
        }
        RealmResults<Vote> votiInteresse = voteDao.getListVoteInterestEventYes(eventoInfo.getUuid());
        for (Vote voto : votiInteresse) {
            voto.setDeleted(true);
        }
        RealmResults<Vote> votiInteresseNo = voteDao.getListVoteInterestEventNo(eventoInfo.getUuid());
        for (Vote voto : votiInteresseNo) {
            voto.setDeleted(true);
        }
        evento.getEventInfo().setMyVote(null);
        evento.getEventInfo().setMyVoteInterest(null);
        for (Vote model : eventoInfo.getEventInfo().getVotes()) {
            User utente = userDao.getUser(model.getCreatorUuid());
            Vote voto = voteDao.getVoteEventFromUser(eventoInfo.getUuid(), model.getCreatorUuid());
            Vote votoInterest = voteDao.getVoteInterestEventFromUser(eventoInfo.getUuid(), model.getCreatorUuid());

            if (model.getType() == Constants.VOTO_SI || model.getType() == Constants.VOTO_NO) {
                if (voto == null) {
                    voto = voteDao.createVote();
                    voto.setCreator(utente);
                    voto.setEventUuid(evento.getUuid());
                    voto.setCreatedAt(model.getCreatedAt());
                    voto.setLoading(false);
                    voto.setType(model.getType());
                } else {
                    voto.setDeleted(false);
                    voto.setCreatedAt(model.getCreatedAt());
                    voto.setLoading(false);
                    voto.setType(model.getType());
                }
                voto.setVoteType(Constants.VOTE_TYPE_EVENT);

                if (!voto.isDeleted() && utente.getUuid().equals(AccountProvider.getInstance().getAccountId())) {
                    evento.getEventInfo().setMyVote(voto);
                }
            } else {
                if (votoInterest == null) {
                    votoInterest = voteDao.createVote();
                    votoInterest.setCreator(utente);
                    votoInterest.setEventUuid(evento.getUuid());
                    votoInterest.setCreatedAt(model.getCreatedAt());
                    votoInterest.setLoading(false);
                    votoInterest.setType(model.getType());
                } else {
                    votoInterest.setDeleted(false);
                    votoInterest.setCreatedAt(model.getCreatedAt());
                    votoInterest.setLoading(false);
                    votoInterest.setType(model.getType());
                }
                votoInterest.setVoteType(Constants.VOTE_TYPE_INTEREST);

                if (!votoInterest.isDeleted() && utente.getUuid().equals(AccountProvider.getInstance().getAccountId())) {
                    evento.getEventInfo().setMyVoteInterest(votoInterest);
                }
            }
        }
        voteDao.deleteAllDeletedVoteForEvent(eventoInfo.getUuid());

        evento.setNumVotesYes((int) voteDao.getVoteEventYesCount(eventoInfo.getUuid()));
        evento.setNumVotesNo((int) voteDao.getVoteEventNoCount(eventoInfo.getUuid()));

        evento.setNumInterestYes((int) voteDao.getInterestEventYesCount(eventoInfo.getUuid()));
        evento.setNumInterestNo((int) voteDao.getInterestEventNoCount(eventoInfo.getUuid()));
    }

    public static void syncSondaggio(UserDao userDao, Poll sondaggio, Poll sondaggioInfo, Event evento) {
        sondaggio.setEventUuid(evento.getUuid());
        sondaggio.setQuestion(sondaggioInfo.getQuestion());
        sondaggio.setType(sondaggioInfo.getType());
        sondaggio.setVoci(new RealmList<PollItem>());
        sondaggio.setCreator(userDao.getUser(sondaggioInfo.getCreatorUuid()));
        sondaggio.setCreatedAt(sondaggioInfo.getCreatedAt());
        sondaggio.setDeleted(sondaggioInfo.isDeleted());
        sondaggio.setUsersCanAddItems(sondaggioInfo.isUsersCanAddItems());
    }

    public static void syncSondaggioVoce(UserDao userDao, PollItem sondaggioVoce, PollItem sondaggioVoceInfo, Poll sondaggio) {
        sondaggioVoce.setPollUuid(sondaggio.getUuid());
        sondaggioVoce.setWhat(sondaggioVoceInfo.getWhat());
        sondaggioVoce.setWhere(sondaggioVoceInfo.getWhere());
        sondaggioVoce.setPlaceId(sondaggioVoceInfo.getPlaceId());
        sondaggioVoce.setLat(sondaggioVoceInfo.getLat());
        sondaggioVoce.setLng(sondaggioVoceInfo.getLng());
        sondaggioVoce.setFromDate(sondaggioVoceInfo.getFromDate());
        sondaggioVoce.setToDate(sondaggioVoceInfo.getToDate());
        sondaggioVoce.setFromHour(sondaggioVoceInfo.getFromHour());
        sondaggioVoce.setToHour(sondaggioVoceInfo.getToHour());
        sondaggioVoce.setCreator(userDao.getUser(sondaggioVoceInfo.getCreatorUuid()));
        sondaggioVoce.setDeleted(sondaggioVoceInfo.isDeleted());
        sondaggioVoce.setCreatedAt(sondaggioVoceInfo.getCreatedAt());
    }

    public static void syncSondaggioVoceVoti(UserDao userDao, VoteDao voteDao, PollItem voce, PollItem voceInfo) {
        RealmResults<Vote> voti = voteDao.getListVotePollItemYes(voceInfo.getUuid());
        for (Vote voto : voti) {
            voto.setDeleted(true);
        }
        voce.setMyVote(null);
        for (Vote model : voceInfo.getVotes()) {
            User utente = userDao.getUser(model.getCreatorUuid());
            Vote voto = voteDao.getVotePollItemFromUser(voceInfo.getUuid(), model.getCreatorUuid());
            if (voto != null && model.getType() == Constants.VOTO_NO) {
                voto.deleteFromRealm();
            } else if (voto == null && model.getType() == Constants.VOTO_SI) {
                voto = voteDao.createVote();
                voto.setCreator(utente);
                voto.setPollItemUuid(voce.getUuid());
                voto.setCreatedAt(model.getCreatedAt());
                voto.setLoading(false);
                voto.setType(Constants.VOTO_SI);
            } else if (voto != null && model.getType() == Constants.VOTO_SI) {
                voto.setDeleted(false);
                voto.setCreatedAt(model.getCreatedAt());
                voto.setLoading(false);
                voto.setType(Constants.VOTO_SI);
            }

            if (voto != null && voto.isValid() && !voto.isDeleted() && model.getCreatorUuid().equals(AccountProvider.getInstance().getAccountId())) {
                voce.setMyVote(voto);
            }
        }
        voteDao.deleteAllDeletedVoteForPollItem(voceInfo.getUuid());

        voce.setNumVoteYes((int) voteDao.getVotePollItemYesCount(voce.getUuid()));
    }

    public interface AddToListListener {
        void add(User user);
    }
}
