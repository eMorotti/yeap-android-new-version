package it.mozart.yeap.services.sync.exceptions;

public class SyncEventoMismatchException extends Exception {

    private String attivitaId;

    public SyncEventoMismatchException(String attivitaId) {
        super();
        this.attivitaId = attivitaId;
    }

    public String getAttivitaId() {
        return attivitaId;
    }
}
