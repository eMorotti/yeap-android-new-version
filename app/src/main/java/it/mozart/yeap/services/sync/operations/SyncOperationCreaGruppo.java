package it.mozart.yeap.services.sync.operations;

import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import it.mozart.yeap.App;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Group;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.dao.GroupDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.services.sync.SyncTotal;
import it.mozart.yeap.ui.groups.main.events.ShowPopupInvitedGroup;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.Prefs;

public class SyncOperationCreaGruppo extends SyncOperation {

    private CreaGruppoModel mCreaGruppoModel;

    public SyncOperationCreaGruppo(SyncData syncData) {
        super(syncData);
    }

    @Override
    public void parseModel() {
        mCreaGruppoModel = new Gson().fromJson(mSyncDataContent, CreaGruppoModel.class);
    }

    @Override
    public void doJob(Realm realm) throws Exception {
        if (mCreaGruppoModel == null)
            throw new Exception("Model is null");

        GroupDao gruppoDao = new GroupDao(realm);
        UserDao utenteDao = new UserDao(realm);

        Group gruppo = gruppoDao.getGroup(mCreaGruppoModel.uuid);
        if (gruppo == null) {
            // Creo gruppo
            gruppo = mCreaGruppoModel.copyInGroup(utenteDao);

            if (gruppo.getGuestsNotYeap() != null && gruppo.getGuestsNotYeap().size() != 0) {
                String values = "";
                for (User user : gruppo.getGuestsNotYeap()) {
                    if (values.isEmpty()) {
                        values = user.getPhone();
                    } else {
                        values = String.format(",%s", user.getPhone());
                    }
                }
                Prefs.putString(Constants.SHOW_INVITE_POPUP_GRUPPO + mCreaGruppoModel.uuid, values);
                EventBus.getDefault().postSticky(new ShowPopupInvitedGroup());
            }

            gruppoDao.creaGruppo(gruppo, utenteDao.getUser(mCreaGruppoModel.creatorUuid), false);
        }
    }

    @Override
    public boolean createNotifications(NotificationsHelper notifications) {
        // Invio notifiche
        if (mCreaGruppoModel.uuid != null && !AccountProvider.getInstance().getAccountId().equals(mCreaGruppoModel.creatorUuid)) {
            notifications.setInfo(Constants.TYPE_GRUPPO, mCreaGruppoModel.uuid);
        }
        return mCreaGruppoModel.uuid != null && !AccountProvider.getInstance().getAccountId().equals(mCreaGruppoModel.creatorUuid);
    }

    private static class CreaGruppoModel {
        String uuid;
        int version;
        long createdAt;

        String title;
        String avatarThumbUrl;
        String avatarLargeUrl;
        boolean usersCanInvite;

        String adminUuid;
        String creatorUuid;
        String makerUuid;

        List<String> partecipants;
        List<String> invited;
        List<User> users;

        Group copyInGroup(UserDao utenteDao) {
            App.setSyncing(true);
            App.jobManager().stop();
            RealmList<User> partecipanti = SyncTotal.manageGuests(utenteDao, users, partecipants);
            RealmList<User> invitati = SyncTotal.manageInvited(utenteDao, invited);
            App.setSyncing(false);
            App.jobManager().start();

            // Creo gruppo
            Group gruppo = new Group();
            gruppo.setUuid(uuid);
            gruppo.setVersion(version);
            gruppo.setCreatedAt(createdAt);
            gruppo.setTitle(title);
            gruppo.setAvatarThumbUrl(avatarThumbUrl);
            gruppo.setAvatarLargeUrl(avatarLargeUrl);
            gruppo.setCreatorUuid(creatorUuid);
            gruppo.setCreator(utenteDao.getUser(creatorUuid));
            gruppo.setAdminUuid(adminUuid);
            gruppo.setAdmin(utenteDao.getUser(adminUuid));
            gruppo.setUsersCanInvite(usersCanInvite);
            gruppo.setGuests(partecipanti);
            gruppo.setGuestsNotYeap(invitati);
            gruppo.setMyselfPresent(partecipants.contains(AccountProvider.getInstance().getAccountId()));

            return gruppo;
        }
    }
}
