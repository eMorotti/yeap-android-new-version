package it.mozart.yeap.services.imageSave;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Locale;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.helpers.AndroidPermissionHelper;
import it.mozart.yeap.realm.Message;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.services.Priority;

public class ImageSaveJob extends Job {

    private String mUUID;
    private String mPath;

    private static final String mDirectory = "/Yeap/YeapImages/Sent";

    public ImageSaveJob(String uuid, String path) {
        super(new Params(Priority.LOW).requireNetwork().persist());
        mUUID = uuid;
        mPath = path;
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {
        try {
            if (!AndroidPermissionHelper.isPermissionGranted(App.getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                return;
            }

            File directory = new File(Environment.getExternalStorageDirectory(), mDirectory);
            if (!directory.isDirectory()) {
                if (!directory.mkdirs())
                    throw new Exception();
            }

            String filename = "Picture_" + System.currentTimeMillis() + ".jpg";

            InputStream in;
            OutputStream out;

            in = new FileInputStream(mPath);
            out = new FileOutputStream(directory + "/" + filename);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();

            out.flush();
            out.close();

            final File file = new File(directory, filename);

            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
            values.put(MediaStore.Images.ImageColumns.BUCKET_ID, file.toString().toLowerCase(Locale.getDefault()).hashCode());
            values.put(MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME, file.getName().toLowerCase(Locale.getDefault()));
            values.put("_data", file.getAbsolutePath());

            ContentResolver cr = App.getContext().getContentResolver();
            cr.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

            Realm realm = App.getRealm();
            //noinspection TryFinallyCanBeTryWithResources
            try {
                final Message messaggio = new MessageDao(realm).getMessage(mUUID);
                if (messaggio != null) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(@NonNull Realm realm) {
                            messaggio.setMediaLocalUrl(file.getAbsolutePath());
                        }
                    });
                }
            } finally {
                realm.close();
            }
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {

    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        throwable.printStackTrace();
        return null;
    }

    @Override
    protected int getRetryLimit() {
        return 5;
    }
}
