package it.mozart.yeap.services.sync.operations;

import com.google.gson.Gson;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.services.sync.SyncTotal;
import it.mozart.yeap.services.sync.exceptions.SyncEventoMismatchException;
import it.mozart.yeap.utility.Constants;

public class SyncOperationAggiungiPartecipanteEvento extends SyncOperation {

    private AggiungiPartecipanteEventoModel mAggiungiPartecipanteModel;

    public SyncOperationAggiungiPartecipanteEvento(SyncData syncData) {
        super(syncData);
    }

    @Override
    public void parseModel() {
        mAggiungiPartecipanteModel = new Gson().fromJson(mSyncDataContent, AggiungiPartecipanteEventoModel.class);
    }

    @Override
    public void doJob(Realm realm) throws Exception {
        if (mAggiungiPartecipanteModel == null)
            throw new Exception("Model is null");

        EventDao eventDao = new EventDao(realm);
        UserDao utenteDao = new UserDao(realm);

        Event evento = eventDao.getEvent(mAggiungiPartecipanteModel.activityUuid);
        if (evento == null) {
            throw new SyncEventoMismatchException(mAggiungiPartecipanteModel.activityUuid);
        }

        if (evento.getVersion() < mAggiungiPartecipanteModel.activityVersion - 1 || mAggiungiPartecipanteModel.user == null) {
            throw new SyncEventoMismatchException(mAggiungiPartecipanteModel.activityUuid);
        }

        if (evento.getVersion() + 1 == mAggiungiPartecipanteModel.activityVersion) {
            manageUserAlreadyPresent(evento, utenteDao);
        }

        evento.setVersion(mAggiungiPartecipanteModel.activityVersion);
        uuid = evento.getUuid();

        // Recupero utenti
        User maker = utenteDao.getUser(mAggiungiPartecipanteModel.makerUuid);
        if (maker == null) {
            throw new SyncEventoMismatchException(mAggiungiPartecipanteModel.activityUuid);
        }
        User utente = utenteDao.getUser(mAggiungiPartecipanteModel.user.getUuid());

        letto = (App.isEventVisible(mAggiungiPartecipanteModel.activityUuid) && App.isEventPageVisible(Constants.EVENT_PAGE_USERS))
                || mAggiungiPartecipanteModel.makerUuid.equals(AccountProvider.getInstance().getAccountId());

        // Creo messaggio
        new MessageDao(realm).createSystemMessageEventUserAdded(evento, maker, utente, letto).setCreatedAt(mAggiungiPartecipanteModel.timestamp);
    }

    @Override
    public boolean createNotifications(NotificationsHelper notifications) {
        // Invio notifiche
        if (!letto) {
            notifications.setInfo(Constants.TYPE_EVENTO, uuid);
        }
        return !letto;
    }

    private void manageUserAlreadyPresent(final Event evento, UserDao utenteDao) {
        User contatto = mAggiungiPartecipanteModel.user;
        SyncTotal.manageRemoteUser(utenteDao, contatto, new SyncTotal.AddToListListener() {
            @Override
            public void add(User user) {
                evento.getGuests().add(0, user);
            }
        });

//        RealmResults<User> utenti = utenteDao.getUsersByPhoneIgnoreDelete(contatto.getPhone());
//        boolean needToCreate = false;
//        boolean foundSameId = false;
//        for (User utente : utenti) {
//            if (utente.isDeleted()) { // utente cancellato
//                utente.setContactId(null);
//                utente.setInContacts(false);
//                utente.setName(null);
//                utente.setSurname(null);
//            } else if (utente.getUuid() != null) { // utente in yeap
//                if (utente.getUuid().equals(contatto.getUuid())) { // stesso id
//                    utente.setNickname(contatto.getNickname());
//
//                    foundSameId = true;
//
//                    evento.getGuests().add(0, utente);
//                } else { // id differente
//                    utente.setDeleted(true);
//                    utente.setContactId(null);
//                    utente.setInContacts(false);
//                    utente.setName(null);
//                    utente.setSurname(null);
//
//                    needToCreate = true;
//                }
//            } else { // utente locale
//                utente.setUuid(contatto.getUuid());
//                utente.setNickname(contatto.getNickname());
//                utente.setInYeap(true);
//
//                evento.getGuests().add(0, utente);
//            }
//        }
//        // nessun utente
//        if (utenti.size() == 0) {
//            User newUtente = new User();
//            newUtente.setId(String.format(Locale.getDefault(), "local_%s%s%d", contatto.getPhone(), contatto.getId(), System.currentTimeMillis()));
//            newUtente.setPhone(contatto.getPhone());
//            newUtente.setUuid(contatto.getUuid());
//            newUtente.setNickname(contatto.getNickname());
//            newUtente.setInYeap(true);
//            newUtente.setInContacts(true);
//
//            newUtente = utenteDao.createUser(newUtente);
//
//            evento.getGuests().add(0, newUtente);
//        }
//
//        if (needToCreate && !foundSameId) {
//            User newUtente = new User();
//            newUtente.setId(String.format(Locale.getDefault(), "local_%s%s%d", contatto.getPhone(), contatto.getId(), System.currentTimeMillis()));
//            newUtente.setPhone(contatto.getPhone());
//            newUtente.setUuid(contatto.getUuid());
//            newUtente.setNickname(contatto.getNickname());
//            newUtente.setInYeap(true);
//            newUtente.setInContacts(true);
//
//            newUtente = utenteDao.createUser(newUtente);
//
//            evento.getGuests().add(0, newUtente);
//        }


//        // Creo utente se non presente
//        User newUtente = utenteDao.getUser(mAggiungiPartecipanteModel.user.getUuid());
//        if (newUtente == null) {
//            RealmResults<User> utenti = utenteDao.getUsersByPhone(mAggiungiPartecipanteModel.user.getPhone());
//            if (utenti != null) {
//                boolean needToCreate = false;
//                boolean foundSameId = false;
//                for (User utente2 : utenti) {
//                    if (!utente2.isInYeap()) {
//                        utente2.setUuid(mAggiungiPartecipanteModel.user.getUuid());
//                        utente2.setNickname(mAggiungiPartecipanteModel.user.getNickname());
//                        utente2.setInYeap(true);
//                        // Aggiungo in invitati
//                        evento.getGuests().add(0, utente2);
//
//                        foundSameId = true;
//                    } else {
//                        utente2.setDeleted(true);
//                        utente2.setInYeap(false);
//
//                        needToCreate = true;
//                    }
//                }
//                if (needToCreate && !foundSameId) {
//                    User newUtente2 = utenteDao.createUserDiff(mAggiungiPartecipanteModel.user);
//                    newUtente2.setInYeap(true);
//                    // Aggiungo in invitati
//                    evento.getGuests().add(0, newUtente2);
//                }
//            }
//            if (utenti == null || utenti.size() == 0) {
//                User newUtente2 = utenteDao.createUserDiff(mAggiungiPartecipanteModel.user);
//                newUtente2.setInYeap(true);
//                // Aggiungo in invitati
//                evento.getGuests().add(0, newUtente2);
//            }
//        } else {
//            newUtente.setInYeap(true);
//            newUtente.setPhone(mAggiungiPartecipanteModel.user.getPhone());
//            newUtente.setNickname(mAggiungiPartecipanteModel.user.getNickname());
//            // Aggiungo in invitati
//            evento.getGuests().add(0, newUtente);
//        }
    }

    private static class AggiungiPartecipanteEventoModel {
        String activityUuid;
        int activityVersion;
        String makerUuid;
        long timestamp;
        User user;
    }
}
