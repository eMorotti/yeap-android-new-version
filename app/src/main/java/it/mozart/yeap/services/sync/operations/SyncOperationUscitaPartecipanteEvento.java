package it.mozart.yeap.services.sync.operations;

import com.google.gson.Gson;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.realm.dao.PollDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.realm.dao.VoteDao;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.services.sync.SyncTotal;
import it.mozart.yeap.services.sync.exceptions.SyncEventoMismatchException;
import it.mozart.yeap.utility.Constants;

public class SyncOperationUscitaPartecipanteEvento extends SyncOperation {

    private UscitaPartecipanteEventoModel mUscitaPartecipanteModel;

    public SyncOperationUscitaPartecipanteEvento(SyncData syncData) {
        super(syncData);
    }

    @Override
    public void parseModel() {
        mUscitaPartecipanteModel = new Gson().fromJson(mSyncDataContent, UscitaPartecipanteEventoModel.class);
    }

    @Override
    public void doJob(Realm realm) throws Exception {
        if (mUscitaPartecipanteModel == null)
            throw new Exception("Model is null");

        EventDao eventoDao = new EventDao(realm);
        UserDao utenteDao = new UserDao(realm);
        MessageDao messaggioDao = new MessageDao(realm);

        Event evento = eventoDao.getEvent(mUscitaPartecipanteModel.activityUuid);
        if (evento == null) {
            throw new SyncEventoMismatchException(mUscitaPartecipanteModel.activityUuid);
        }

        if (evento.getVersion() < mUscitaPartecipanteModel.activityVersion - 1 || mUscitaPartecipanteModel.userUuid == null) {
            throw new SyncEventoMismatchException(mUscitaPartecipanteModel.activityUuid);
        }

        evento.setVersion(mUscitaPartecipanteModel.activityVersion);

        User utente = utenteDao.getUser(mUscitaPartecipanteModel.userUuid);
        if (mUscitaPartecipanteModel.adminUuid != null) {
            User admin = utenteDao.getUser(mUscitaPartecipanteModel.adminUuid);
            evento.setAdmin(admin);
            messaggioDao.createSystemMessageEventNewAdmin(admin, evento).setCreatedAt(mUscitaPartecipanteModel.timestamp - 1);
        }
        if (evento.getGuests().contains(utente)) {
            evento.getGuests().remove(utente);
        } else {
            return;
        }

        evento.setWherePollEnabled(mUscitaPartecipanteModel.activity.isWherePollEnabled());
        evento.setWherePollUuid(mUscitaPartecipanteModel.activity.getWherePollUuid());
        evento.setWhenPollEnabled(mUscitaPartecipanteModel.activity.isWhenPollEnabled());
        evento.setWhenPollUuid(mUscitaPartecipanteModel.activity.getWhenPollUuid());
        uuid = evento.getUuid();

        if (utente == null) {
            throw new SyncEventoMismatchException(mUscitaPartecipanteModel.activityUuid);
        }
        letto = (App.isEventVisible(mUscitaPartecipanteModel.activityUuid) && App.isEventPageVisible(Constants.EVENT_PAGE_CHAT))
                || utente.getUuid().equals(AccountProvider.getInstance().getAccountId());

        if (utente.getUuid().equals(AccountProvider.getInstance().getAccountId())) {
            evento.setMyselfPresent(false);
        }

        // Creo messaggio
        messaggioDao.createSystemMessageEventUserLeft(evento, utente, letto).setCreatedAt(mUscitaPartecipanteModel.timestamp);

        SyncTotal.syncEvento(realm, mUscitaPartecipanteModel.activity, eventoDao, utenteDao, new VoteDao(realm), new PollDao(realm));
    }

    @Override
    public boolean createNotifications(NotificationsHelper notifications) {
        // Invio notifiche
        if (!letto) {
            notifications.setInfo(Constants.TYPE_EVENTO, uuid);
        }
        return !letto;
    }

    private static class UscitaPartecipanteEventoModel {
        String activityUuid;
        int activityVersion;
        long timestamp;
        String userUuid;
        String adminUuid;
        Event activity;
    }
}
