package it.mozart.yeap.services.sync;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.RemoteInput;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.DisplayMetrics;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import io.realm.Realm;
import io.realm.RealmResults;
import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.Group;
import it.mozart.yeap.realm.Message;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.GroupDao;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.services.Priority;
import it.mozart.yeap.ui.events.main.Act_EventoMain;
import it.mozart.yeap.ui.groups.main.Act_GruppoMain;
import it.mozart.yeap.ui.home.Act_Home;
import it.mozart.yeap.ui.main.Act_MainMessageReply;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.Utils;
import it.mozart.yeap.utility.debounce.NotificationInfo;
import me.leolin.shortcutbadger.ShortcutBadger;

public class SyncUnitNotificationJob extends Job {

    private NotificationInfo mInfo;

    protected SyncUnitNotificationJob(NotificationInfo info) {
        super(new Params(Priority.HIGH).groupBy("Notification"));
        mInfo = info;
    }

    @Override
    public void onAdded() {
    }

    @Override
    public void onRun() throws Throwable {
        createNotifications(App.getContext(), mInfo.getType(), mInfo.getId());
    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {

    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        return RetryConstraint.CANCEL;
    }

    public void createNotifications(Context context, int type, String id) {
        if (id == null) {
            return;
        }

        switch (type) {
            case Constants.TYPE_EVENTO:
                if (App.isEventVisible(id)) {
                    return;
                }
                createNotification(context, getConfigsForEvent(context));
                break;
            case Constants.TYPE_GRUPPO:
                if (App.isGroupVisible(id)) {
                    return;
                }
                createNotification(context, getConfigsForGroup(context));
                break;
        }
    }

    private void createNotification(Context context, NotificationConfig config) {
        if (config == null) {
            return;
        }

        boolean showReply = false;
        boolean isEvento = false;
        String id = null;

        NotificationCompat.Builder notificationBuilder = createNotification(context, true);
        Intent intent = null;
        if (config.getParentId() == null) {
            switch (config.getType()) {
                case Constants.TYPE_EVENTO:
                    intent = Act_Home.newIntent(0);
                    notificationBuilder.setLargeIcon(null);
                    break;
                case Constants.TYPE_GRUPPO:
                    intent = Act_Home.newIntent(1);
                    notificationBuilder.setLargeIcon(null);
                    break;
            }
        } else {
            switch (config.getType()) {
                case Constants.TYPE_EVENTO:
                    showReply = true;
                    isEvento = true;
                    id = config.getParentId();
                    intent = Act_EventoMain.newIntent(config.getParentId());
                    notificationBuilder = updateLargeIcon(context, notificationBuilder, config);
                    break;
                case Constants.TYPE_GRUPPO:
                    showReply = true;
                    isEvento = false;
                    id = config.getParentId();
                    intent = Act_GruppoMain.newIntent(config.getParentId());
                    notificationBuilder = updateLargeIcon(context, notificationBuilder, config);
                    break;
            }
        }

        if (intent == null) {
            return;
        }

        if (showReply) {
            String replyLabel = context.getString(R.string.system_reply_action);
            RemoteInput remoteInput = new RemoteInput.Builder(Constants.REPLY_KEY)
                    .setLabel(replyLabel)
                    .build();

            NotificationCompat.Action replyAction = new NotificationCompat.Action.Builder(R.drawable.chat_ico_send, replyLabel, getReplyPendingIntent(context, id, isEvento, config.getType(), config))
                    .addRemoteInput(remoteInput)
                    .setAllowGeneratedReplies(true)
                    .build();

            notificationBuilder.addAction(replyAction);
        }

        // Random per creare identificativo univoco per la notifica
        Random random = new Random();

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, random.nextInt(100000), intent, PendingIntent.FLAG_ONE_SHOT);
        notificationBuilder.setContentIntent(pendingIntent);
        notificationBuilder = createStyle(notificationBuilder, config);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(config.getType(), notificationBuilder.build());

        if (ShortcutBadger.isBadgeCounterSupported(App.getContext())) {
            ShortcutBadger.applyCount(App.getContext(), config.getNumMessaggi());
        }
    }

    @SuppressLint("InlinedApi")
    private NotificationCompat.Builder createNotification(Context context, boolean sound) {
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        return (NotificationCompat.Builder) new NotificationCompat.Builder(context, "yeap!")
                .setColor(ContextCompat.getColor(context, R.color.colorGradientEnd))
                .setSmallIcon(R.drawable.logo_white)
                .setAutoCancel(true)
                .setVisibility(Notification.VISIBILITY_PUBLIC)
                .setCategory(Notification.CATEGORY_MESSAGE)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSound(defaultSoundUri);
    }

    private NotificationCompat.Builder updateLargeIcon(Context context, NotificationCompat.Builder builder, NotificationConfig config) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        float density = metrics.density;
        int size = (int) (64 * density);

        Bitmap bitmap;
        try {
            if(config.getImage() !=  null) {
                bitmap = Glide.with(context).load(config.getImage()).asBitmap().centerCrop().into(size, size).get();
            } else if (config.getType() == Constants.TYPE_EVENTO) {
                bitmap = Glide.with(context).load(R.drawable.avatar_attivita).asBitmap().centerCrop().into(size, size).get();
            } else {
                bitmap = Glide.with(context).load(R.drawable.avatar_gruppo).asBitmap().centerCrop().into(size, size).get();
            }
            RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(context.getResources(), bitmap);
            circularBitmapDrawable.setCircular(true);

            Bitmap mutableBitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(mutableBitmap);
            circularBitmapDrawable.setBounds(0, 0, size, size);
            circularBitmapDrawable.draw(canvas);

            bitmap = mutableBitmap;
        } catch (Exception ex) {
            bitmap = null;
        }

        if (bitmap != null) {
            builder.setLargeIcon(bitmap);
        } else {
            if (config.getType() == Constants.TYPE_EVENTO) {
                builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.avatar_attivita));
            } else {
                builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.avatar_gruppo));
            }
        }

        return builder;
    }

    private NotificationCompat.Builder createStyle(NotificationCompat.Builder notificationBuilder, NotificationConfig config) {
//        if (Utils.isNougat()) {
//        NotificationCompat.MessagingStyle messagingStyle = new NotificationCompat.MessagingStyle("Me");
//        if (config.getMessaggi().length != 0) {
//            for (MessageInfo messaggio : config.getMessaggi()) {
//                messagingStyle.addMessage(messaggio.getMessaggio(), messaggio.getTimestamp(), messaggio.getUtente());
//            }
//        } else {
//            messagingStyle.addMessage(config.getMessaggio(), config.getTimestamp(), config.getUser());
//        }
//        messagingStyle.setConversationTitle(config.getTitolo());
//        notificationBuilder.setStyle(messagingStyle);
//        } else {
        notificationBuilder.setContentTitle(config.getTitolo());

        if (config.getMessaggi().length != 0) {
            notificationBuilder.setContentText(config.getMessaggio());

            NotificationCompat.InboxStyle style = new NotificationCompat.InboxStyle();
            for (MessageInfo messaggio : config.getMessaggi()) {
                style.addLine(messaggio.getUtente() + ": " + messaggio.getMessaggio());
            }
            style.setSummaryText(config.getSommario());
            style.setBigContentTitle(config.getTitolo());
            notificationBuilder.setStyle(style);
        } else {
            notificationBuilder.setContentText(config.getUser() + ": " + config.getMessaggio());
        }
//        }

        return notificationBuilder;
    }

    private PendingIntent getReplyPendingIntent(Context context, String id, boolean isEvento, int notificationId, NotificationConfig config) {
        if (Utils.isNougat()) {
            Intent intent = NotificationService.getReplyMessageIntent(context, id, isEvento, notificationId);
            return PendingIntent.getService(App.getContext(), 100, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        } else {
            Intent intent = Act_MainMessageReply.newIntent(id, isEvento, config.getMessaggiList());
            return PendingIntent.getActivity(App.getContext(), 100, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        }
    }

    private NotificationConfig getConfigsForEvent(Context context) {
        NotificationConfig config = new NotificationConfig();
        config.setType(Constants.TYPE_EVENTO);

        Realm realm = App.getRealm();
        MessageDao messageDao = new MessageDao(realm);
        EventDao eventDao = new EventDao(realm);

        try {
            int numDifferentEvents = messageDao.getDifferentEventsWithNotReadMessages();
            RealmResults<Message> messaggi = messageDao.getNotReadMessagesForEvent();
            int numMessaggi = messageDao.getNumNotReadMessagedForGroup();
            if (messaggi.size() == 0) {
                return null;
            } else if (messaggi.size() == 1) {
                Message message = messaggi.get(0);
                Event event = eventDao.getEvent(message.getEventParentUuid());
                config.setTitolo(event.getTitle());
                config.setMessaggio(getMessageText(context, message));
                config.setUser(message.getCreator().getChatName());
                config.setTimestamp(message.getCreatedAt());
                config.setParentId(event.getUuid());
                config.setImage(event.getAvatarThumbUrl());
                config.setNumMessaggi(1 + numMessaggi);
            } else {
                if (numDifferentEvents == 1) {
                    Message message = messaggi.get(0);
                    Event event = eventDao.getEvent(message.getEventParentUuid());
                    config.setTitolo(event.getTitle());
                    config.setMessaggi(createModelMessaggi(context, eventDao, messaggi, numDifferentEvents));
                    config.setSommario(String.format(context.getString(R.string.system_notifica_messaggi_evento), messaggi.size()));
                    config.setNumMessaggi(messaggi.size() + numMessaggi);
                    config.setMessaggio(config.getSommario());
                    config.setParentId(event.getUuid());
                    config.setImage(event.getAvatarThumbUrl());
                } else {
                    config.setTitolo(context.getString(R.string.general_app));
                    config.setMessaggi(createModelMessaggi(context, eventDao, messaggi, numDifferentEvents));
                    config.setSommario(String.format(context.getString(R.string.system_notifica_messaggi_evento_multiplo), messaggi.size(), numDifferentEvents));
                    config.setNumMessaggi(messaggi.size() + numMessaggi);
                    config.setMessaggio(config.getSommario());
                }
            }
        } finally {
            realm.close();
        }

        return config;
    }

    private static MessageInfo[] createModelMessaggi(Context context, EventDao eventDao, RealmResults<Message> messaggi, int numDifferentEvents) {
        String currentUuid = App.getEventUuid();
        int maxLenght = Math.min(30, messaggi.size());
        MessageInfo[] messageInfos = new MessageInfo[maxLenght];
        for (int i = maxLenght - 1; i >= 0; i--) {
            Message messaggio = messaggi.get(i);
            if (currentUuid != null && currentUuid.equals(messaggio.getEventParentUuid())) {
                continue;
            }
            Event evento = eventDao.getEvent(messaggio.getEventParentUuid());
            String nomeUtente = messaggio.getCreator().getChatName() + (numDifferentEvents != 1 ? (" @ " + evento.getTitle()) : "");
            String testo = getMessageText(context, messaggio);
            messageInfos[maxLenght - i - 1] = new MessageInfo(nomeUtente, testo, messaggio.getCreatedAt());
        }
        return messageInfos;
    }

    private NotificationConfig getConfigsForGroup(Context context) {
        NotificationConfig config = new NotificationConfig();
        config.setType(Constants.TYPE_GRUPPO);

        Realm realm = App.getRealm();
        MessageDao messageDao = new MessageDao(realm);
        GroupDao groupDao = new GroupDao(realm);

        try {
            int numDifferentGroups = messageDao.getDifferentGroupsWithNotReadMessages();
            RealmResults<Message> messaggi = messageDao.getNotReadMessagesForGroup();
            int numMessaggi = messageDao.getNumNotReadMessagedForEvent();
            if (messaggi.size() == 0) {
                return null;
            } else if (messaggi.size() == 1) {
                Message message = messaggi.get(0);
                Group gruppo = groupDao.getGroup(message.getGroupParentUuid());
                config.setTitolo(gruppo.getTitle());
                config.setMessaggio(getMessageText(context, message));
                config.setUser(message.getCreator().getChatName());
                config.setTimestamp(message.getCreatedAt());
                config.setParentId(gruppo.getUuid());
                config.setImage(gruppo.getAvatarThumbUrl());
                config.setNumMessaggi(1 + numMessaggi);
            } else {
                if (numDifferentGroups == 1) {
                    Message message = messaggi.get(0);
                    Group gruppo = groupDao.getGroup(message.getGroupParentUuid());
                    config.setTitolo(gruppo.getTitle());
                    config.setMessaggi(createModelMessaggi(context, groupDao, messaggi, numDifferentGroups));
                    config.setSommario(String.format(context.getString(R.string.system_notifica_messaggi_gruppo), messaggi.size()));
                    config.setNumMessaggi(messaggi.size() + numMessaggi);
                    config.setMessaggio(config.getSommario());
                    config.setParentId(gruppo.getUuid());
                    config.setImage(gruppo.getAvatarThumbUrl());
                } else {
                    config.setTitolo(context.getString(R.string.general_app));
                    config.setMessaggi(createModelMessaggi(context, groupDao, messaggi, numDifferentGroups));
                    config.setSommario(String.format(context.getString(R.string.system_notifica_messaggi_gruppo_multiplo), messaggi.size(), numDifferentGroups));
                    config.setNumMessaggi(messaggi.size() + numMessaggi);
                    config.setMessaggio(config.getSommario());
                }
            }
        } finally {
            realm.close();
        }

        return config;
    }

    private static MessageInfo[] createModelMessaggi(Context context, GroupDao groupDao, RealmResults<Message> messaggi, int numDifferentGroups) {
        String currentUuid = App.getGroupUuid();
        int maxLenght = Math.min(30, messaggi.size());
        MessageInfo[] messageInfos = new MessageInfo[maxLenght];
        for (int i = maxLenght - 1; i >= 0; i--) {
            Message messaggio = messaggi.get(i);
            if (currentUuid != null && currentUuid.equals(messaggio.getEventParentUuid())) {
                continue;
            }
            Group gruppo = groupDao.getGroup(messaggio.getGroupParentUuid());
            String nomeUtente = messaggio.getCreator().getChatName() + (numDifferentGroups != 1 ? (" @ " + gruppo.getTitle()) : "");
            String testo = getMessageText(context, messaggio);
            messageInfos[maxLenght - i - 1] = new MessageInfo(nomeUtente, testo, messaggio.getCreatedAt());
        }
        return messageInfos;
    }

    private static String getMessageText(Context context, Message message) {
        String testo = "";
        if (message.getMediaLocalUrl() != null || message.getMediaUrl() != null) {
            testo = "(" + context.getString(R.string.general_foto) + ") ";
        }
        testo += message.getFormattedMessageWithoutCreator(context);

        return testo;
    }

    // CLASSES

    private static class NotificationConfig {

        private String titolo;
        private String messaggio;
        private String user;
        private long timestamp;
        private MessageInfo[] messaggi;
        private String sommario;
        private int numMessaggi;
        private String parentId;
        private String image;
        private int type;
        private boolean visualizza;
        private boolean innerParent;

        NotificationConfig() {
            visualizza = false;
            messaggi = new MessageInfo[0];
        }

        public String getTitolo() {
            return titolo;
        }

        public void setTitolo(String titolo) {
            this.titolo = titolo;
        }

        public String getMessaggio() {
            return messaggio;
        }

        public void setMessaggio(String messaggio) {
            this.messaggio = messaggio;
        }

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }

        public long getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(long timestamp) {
            this.timestamp = timestamp;
        }

        public MessageInfo[] getMessaggi() {
            return messaggi;
        }

        public void setMessaggi(MessageInfo[] messaggi) {
            this.messaggi = messaggi;
        }

        public String getSommario() {
            return sommario;
        }

        public void setSommario(String sommario) {
            this.sommario = sommario;
        }

        public int getNumMessaggi() {
            return numMessaggi;
        }

        public void setNumMessaggi(int numMessaggi) {
            this.numMessaggi = numMessaggi;
        }

        public String getParentId() {
            return parentId;
        }

        public void setParentId(String parentId) {
            this.parentId = parentId;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public boolean isVisualizza() {
            return visualizza;
        }

        public void setVisualizza(boolean visualizza) {
            this.visualizza = visualizza;
        }

        public boolean isInnerParent() {
            return innerParent;
        }

        public void setInnerParent(boolean innerParent) {
            this.innerParent = innerParent;
        }

        public List<String> getMessaggiList() {
            List<String> msgs = new ArrayList<>();
            if (messaggi != null && messaggi.length != 0) {
                for (MessageInfo info : messaggi) {
                    msgs.add(info.getUtente() + ": " + info.getMessaggio());
                }
            } else {
                msgs.add(user + ": " + messaggio);
            }
            return msgs;
        }
    }

    private static class MessageInfo {

        private String utente;
        private String messaggio;
        private long timestamp;
        private boolean isEvento;
        private String avatar;

        public MessageInfo(String utente, String messaggio, long timestamp) {
            this.utente = utente;
            this.messaggio = messaggio;
            this.timestamp = timestamp;
        }

        public String getUtente() {
            return utente;
        }

        public String getMessaggio() {
            return messaggio;
        }

        public long getTimestamp() {
            return timestamp;
        }

        public boolean isEvento() {
            return isEvento;
        }

        public void setEvento(boolean evento) {
            isEvento = evento;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }
    }
}
