package it.mozart.yeap.services.sync.operations;

import com.google.gson.Gson;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.R;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.Poll;
import it.mozart.yeap.realm.PollItem;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.realm.dao.PollDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.realm.dao.VoteDao;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.services.sync.SyncTotal;
import it.mozart.yeap.services.sync.exceptions.SyncEventoMismatchException;
import it.mozart.yeap.utility.Constants;

public class SyncOperationSondaggioApertura extends SyncOperation {

    private AperturaSondaggioModel mAperturaSondaggioModel;

    public SyncOperationSondaggioApertura(SyncData syncData) {
        super(syncData);
    }

    @Override
    public void parseModel() {
        mAperturaSondaggioModel = new Gson().fromJson(mSyncDataContent, AperturaSondaggioModel.class);
    }

    @Override
    public void doJob(Realm realm) throws Exception {
        if (mAperturaSondaggioModel == null)
            throw new Exception("Model is null");

        EventDao eventoDao = new EventDao(realm);
        PollDao pollDao = new PollDao(realm);
        VoteDao votoDao = new VoteDao(realm);
        UserDao userDao = new UserDao(realm);

        Event evento = eventoDao.getEvent(mAperturaSondaggioModel.activityUuid);
        if (evento == null) {
            throw new SyncEventoMismatchException(mAperturaSondaggioModel.activityUuid);
        }

        if (evento.getVersion() < mAperturaSondaggioModel.activityVersion - 1 || mAperturaSondaggioModel.activity == null) {
            throw new SyncEventoMismatchException(mAperturaSondaggioModel.activityUuid);
        }

        evento.setVersion(mAperturaSondaggioModel.activityVersion);

        letto = (App.isEventVisible(mAperturaSondaggioModel.activityUuid) && App.isEventPageVisible(Constants.EVENT_PAGE_DETAILS))
                || mAperturaSondaggioModel.makerUuid.equals(AccountProvider.getInstance().getAccountId());

        // Creo sondaggi
        for (Poll sondaggioInfo : mAperturaSondaggioModel.activity.getPolls()) {
            Poll sondaggio = pollDao.getPoll(sondaggioInfo.getUuid());
            boolean notPresent = false;
            if (sondaggio == null) {
                notPresent = true;
                sondaggio = new Poll();
                sondaggio.setUuid(sondaggioInfo.getUuid());
            }
            SyncTotal.syncSondaggio(userDao, sondaggio, sondaggioInfo, evento);

            if (notPresent) {
                sondaggio = pollDao.createPoll(sondaggio, true, letto, true);
            }

            for (PollItem sondaggioVoceInfo : sondaggioInfo.getPollItems()) {
                PollItem sondaggioVoce = pollDao.getPollItem(sondaggioVoceInfo.getUuid());
                boolean notPresentBis = false;
                if (sondaggioVoce == null) {
                    notPresentBis = true;
                    sondaggioVoce = new PollItem();
                    sondaggioVoce.setUuid(sondaggioVoceInfo.getUuid());
                }
                SyncTotal.syncSondaggioVoce(userDao, sondaggioVoce, sondaggioVoceInfo, sondaggio);

                if (notPresentBis) {
                    sondaggioVoce = pollDao.createPollItem(sondaggioVoce, true, letto, true);
                } else {
                    sondaggio.getVoci().add(sondaggioVoce);
                }

                SyncTotal.syncSondaggioVoceVoti(userDao, votoDao, sondaggioVoce, sondaggioVoceInfo);
            }

            if (sondaggio.getCreatedAt() > evento.getUpdatedAt()) {
                evento.setUpdatedAt(sondaggio.getCreatedAt());
            }
        }

        evento.setWherePollEnabled(mAperturaSondaggioModel.activity.isWherePollEnabled());
        evento.setWherePollUuid(mAperturaSondaggioModel.activity.getWherePollUuid());
        evento.setWhenPollEnabled(mAperturaSondaggioModel.activity.isWhenPollEnabled());
        evento.setWhenPollUuid(mAperturaSondaggioModel.activity.getWhenPollUuid());

        letto = (App.isEventVisible(mAperturaSondaggioModel.activityUuid) && App.isEventPageVisible(Constants.EVENT_PAGE_DETAILS))
                || mAperturaSondaggioModel.makerUuid.equals(AccountProvider.getInstance().getAccountId());
        new MessageDao(realm).createSystemMessagePollOpened(evento, userDao.getUser(mAperturaSondaggioModel.makerUuid),
                mAperturaSondaggioModel.pollType.equalsIgnoreCase("where") ? App.getContext().getString(R.string.system_poll_open_where) : App.getContext().getString(R.string.system_poll_open_when),
                letto).setCreatedAt(mAperturaSondaggioModel.timestamp);
    }

    @Override
    public boolean createNotifications(NotificationsHelper notifications) {
        return false;
    }

    private static class AperturaSondaggioModel {
        String activityUuid;
        int activityVersion;
        String makerUuid;
        String pollType;
        long timestamp;
        Event activity;
    }
}
