package it.mozart.yeap.services.notifications;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import javax.inject.Inject;

import it.mozart.yeap.App;
import it.mozart.yeap.api.services.ProfileApi;
import rx.functions.Action1;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    @Inject
    ProfileApi profileApi;

    public MyFirebaseInstanceIDService() {
        App.feather().injectFields(this);
    }

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("FIREBASE_TOKEN", refreshedToken);
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
        profileApi.sendPushToken(token, "android").subscribe(new Action1<Void>() {
            @Override
            public void call(Void aVoid) {
            }
        }, new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                throwable.printStackTrace();
            }
        });
    }
}
