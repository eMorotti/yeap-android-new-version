package it.mozart.yeap.services.messaggi;

import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import it.mozart.yeap.common.MediaInfo;
import it.mozart.yeap.common.MessaggioInfo;
import it.mozart.yeap.events.MessaggioInoltratoEvent;
import it.mozart.yeap.services.Priority;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.PhotoGenerator;

public class InoltraMessaggioWithExternalImageJob extends Job {

    private Uri mUri;
    private Set<Map.Entry<String, Boolean>> mSelectedAttivitaAndGroups;
    private String mOwnerId;

    public InoltraMessaggioWithExternalImageJob(Uri uri, Set<Map.Entry<String, Boolean>> selected, String ownerId) {
        super(new Params(Priority.HIGH).groupBy("MultipleMessages"));
        mUri = uri;
        mSelectedAttivitaAndGroups = selected;
        mOwnerId = ownerId;
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {
        PhotoGenerator photoGenerator = new PhotoGenerator(getApplicationContext());
        File file = photoGenerator.generateFileFrom(mUri);
        if (file == null) {
            return;
        }

        // Calcolo l'altezza dell'immagine nella chat
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file.getPath(), options);

        MediaInfo mediaInfo = new MediaInfo(file.getPath(), 0, false);
        mediaInfo.setWidth(options.outWidth);
        mediaInfo.setHeight(options.outHeight);

        for (Map.Entry pair : mSelectedAttivitaAndGroups) {
            MessaggioInfo messaggioInfo;
            if ((boolean) pair.getValue()) {
                // Caso evento
                messaggioInfo = new MessaggioInfo((String) pair.getKey(),
                        Constants.MESSAGGIO_TYPOLOGY_CHAT_EVENTO,
                        mOwnerId,
                        null,
                        mediaInfo,
                        new Date().getTime(),
                        true,
                        false,
                        true);
            } else {
                // Caso gruppo
                messaggioInfo = new MessaggioInfo((String) pair.getKey(),
                        Constants.MESSAGGIO_TYPOLOGY_CHAT_GRUPPO,
                        mOwnerId,
                        null,
                        mediaInfo,
                        new Date().getTime(),
                        true,
                        false,
                        true);
            }
            // Creo messaggio
            MessaggiService.createMessage(getApplicationContext(), messaggioInfo);

            // Serve per chiudere la schermata di inoltra
            EventBus.getDefault().post(new MessaggioInoltratoEvent());
        }
    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {

    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        // Serve per chiudere la schermata di inoltra
        EventBus.getDefault().post(new MessaggioInoltratoEvent());
        return null;
    }
}
