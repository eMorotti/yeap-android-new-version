package it.mozart.yeap.services.imageDownload;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.annotation.NonNull;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.realm.Message;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.utility.Constants;

public class ImageDownloadReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
            long receivedId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1L);
            DownloadManager manager = (DownloadManager) App.getContext().getSystemService(Context.DOWNLOAD_SERVICE);

            DownloadManager.Query query = new DownloadManager.Query();
            query.setFilterById(receivedId);
            final Cursor cursor = manager.query(query);
            if (cursor.moveToFirst()) {
                int index = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
                if (cursor.getInt(index) == DownloadManager.STATUS_SUCCESSFUL) {
                    String description = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_DESCRIPTION));
                    if (description.startsWith(Constants.DOWNLOAD_PREFIX)) {
                        final String uuid = description.replace(Constants.DOWNLOAD_PREFIX, "");
                        Realm realm = App.getRealm();
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(@NonNull Realm realm) {
                                Message messaggio = new MessageDao(realm).getMessage(uuid);
                                if (messaggio != null) {
                                    messaggio.setImageState(Constants.IMAGE_STATE_DOWNLOAD_COMPLETE);
                                    messaggio.setMediaLocalUrl(cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI)));
                                }
                            }
                        });
                        realm.close();
                    }
                } else if (cursor.getInt(index) == DownloadManager.STATUS_FAILED) {
                    String description = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_DESCRIPTION));
                    if (description.startsWith(Constants.DOWNLOAD_PREFIX)) {
                        final String uuid = description.replace(Constants.DOWNLOAD_PREFIX, "");
                        Realm realm = App.getRealm();
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(@NonNull Realm realm) {
                                Message messaggio = new MessageDao(realm).getMessage(uuid);
                                if (messaggio != null) {
                                    messaggio.setImageState(Constants.IMAGE_STATE_DOWNLOAD_FAILED);
                                }
                            }
                        });
                        realm.close();
                    }
                }
            }
            cursor.close();
        }
    }
}
