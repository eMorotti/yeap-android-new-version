package it.mozart.yeap.services.sync.operations;

import com.google.gson.Gson;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.api.models.EventoApiModel;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.services.sync.exceptions.SyncEventoMismatchException;
import it.mozart.yeap.utility.Constants;

public class SyncOperationModificaEvento extends SyncOperation {

    private ModificaAttivitaModel mModificaEventoModel;

    public SyncOperationModificaEvento(SyncData syncData) {
        super(syncData);
    }

    @Override
    public void parseModel() {
        mModificaEventoModel = new Gson().fromJson(mSyncDataContent, ModificaAttivitaModel.class);
    }

    @Override
    public void doJob(Realm realm) throws Exception {
        if (mModificaEventoModel == null)
            throw new Exception("Model is null");

        EventDao eventoDao = new EventDao(realm);
        UserDao utenteDao = new UserDao(realm);
        MessageDao messaggioDao = new MessageDao(realm);

        Event evento = eventoDao.getEvent(mModificaEventoModel.activityUuid);
        if (evento == null) {
            throw new SyncEventoMismatchException(mModificaEventoModel.activityUuid);
        }

        if (evento.getVersion() < mModificaEventoModel.activityVersion - 1 || mModificaEventoModel.makerUuid == null) {
            throw new SyncEventoMismatchException(mModificaEventoModel.activityUuid);
        }

        boolean avatarChanged = false;
        // Modifico attivita
        evento.setVersion(mModificaEventoModel.activityVersion);
        evento.setUpdatedAt(mModificaEventoModel.timestamp);
        if (mModificaEventoModel.updates != null) {
            User maker = utenteDao.getUser(mModificaEventoModel.makerUuid);
            if (maker == null) {
                throw new SyncEventoMismatchException(mModificaEventoModel.activityUuid);
            }
            letto = (App.isEventVisible(mModificaEventoModel.activityUuid) && App.isEventPageVisible(Constants.EVENT_PAGE_DETAILS))
                    || mModificaEventoModel.makerUuid.equals(AccountProvider.getInstance().getAccountId());

            if (mModificaEventoModel.updates.getDesc() != null) {
                evento.setDesc(mModificaEventoModel.updates.getDesc());
            }
            if (mModificaEventoModel.updates.getAvatarThumbUrl() != null) {
                evento.setAvatarThumbUrl(mModificaEventoModel.updates.getAvatarThumbUrl());
                avatarChanged = true;
            }
            if (mModificaEventoModel.updates.getAvatarLargeUrl() != null) {
                evento.setAvatarLargeUrl(mModificaEventoModel.updates.getAvatarLargeUrl());
                avatarChanged = true;
            }
            if (mModificaEventoModel.updates.getUsersCanInvite() != null) {
                evento.setUsersCanInvite(mModificaEventoModel.updates.getUsersCanInvite());
            }
            if (mModificaEventoModel.updates.getUsersCanAddItems() != null) {
                evento.setUsersCanAddItems(mModificaEventoModel.updates.getUsersCanAddItems());
            }

            if (avatarChanged) {
                messaggioDao.createSystemMessageEventInfoModified(evento, maker).setCreatedAt(mModificaEventoModel.timestamp);
            }
        }
    }

    @Override
    public boolean createNotifications(NotificationsHelper notifications) {
        return false;
    }

    private static class ModificaAttivitaModel {
        String activityUuid;
        int activityVersion;
        String makerUuid;
        long timestamp;
        EventoApiModel updates;
    }
}
