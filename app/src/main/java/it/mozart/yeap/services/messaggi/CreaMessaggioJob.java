package it.mozart.yeap.services.messaggi;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.RetryStrategy;
import com.firebase.jobdispatcher.Trigger;
import com.google.gson.Gson;

import io.realm.Realm;
import io.realm.RealmObject;
import it.mozart.yeap.App;
import it.mozart.yeap.common.MessaggioInfo;
import it.mozart.yeap.common.UrlPreviewData;
import it.mozart.yeap.realm.Message;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.GroupDao;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.services.Priority;
import it.mozart.yeap.utility.Constants;

class CreaMessaggioJob extends Job {

    private MessaggioInfo mMessaggioInfo;

    CreaMessaggioJob(MessaggioInfo messaggioInfo) {
        super(new Params(Priority.HIGH + 100).groupBy("CreaMessaggio" + messaggioInfo.getParentId()));
        mMessaggioInfo = messaggioInfo;
    }

    @Override
    public void onAdded() {
    }

    @Override
    public void onRun() throws Throwable {
        final Realm realm = App.getRealm();
        final Message messaggio;
        final String messaggioUuid;
        //noinspection TryFinallyCanBeTryWithResources
        try {
            RealmObject parentObject = null;
            switch (mMessaggioInfo.getParentType()) {
                case Constants.MESSAGGIO_TYPOLOGY_CHAT_EVENTO:
                    parentObject = new EventDao(realm).getEvent(mMessaggioInfo.getParentId());
                    break;
                case Constants.MESSAGGIO_TYPOLOGY_CHAT_GRUPPO:
                    parentObject = new GroupDao(realm).getGroup(mMessaggioInfo.getParentId());
                    break;
            }

            realm.beginTransaction();
            // Creo il messaggio
            messaggio = new MessageDao(realm).createMessage(
                    mMessaggioInfo.getTesto(),
                    new UserDao(realm).getUser(mMessaggioInfo.getUtenteId()),
                    mMessaggioInfo.getMediaInfo(),
                    mMessaggioInfo.getParentType(),
                    parentObject,
                    mMessaggioInfo.isLetto(),
                    mMessaggioInfo.isInviato());
            messaggio.setCreatedAt(mMessaggioInfo.getCreatedAt());
            if (mMessaggioInfo.getPreviewData() != null) {
                messaggio.setUrlInfo(new Gson().toJson(mMessaggioInfo.getPreviewData(), UrlPreviewData.class));
            }
            messaggio.setUrl(mMessaggioInfo.getUrl());
            messaggio.setUrlChecked(mMessaggioInfo.isUrlChecked());
            messaggio.setCreatedAtForVisualization(messaggio.getCreatedAt());
            if (messaggio.getMediaLocalUrl() != null) {
                messaggio.setImageState(Constants.IMAGE_STATE_DOWNLOAD_COMPLETE);
            }
            messaggioUuid = messaggio.getUuid();
            realm.commitTransaction();
        } catch (Exception e) {
            if (realm.isInTransaction())
                realm.cancelTransaction();
            throw e;
        } finally {
            realm.close();
        }

        try {
            // Invio il messaggio
            if (messaggioUuid != null && mMessaggioInfo.isSend()) {
                Bundle bundle = new Bundle();
                bundle.putString(InvioMessaggioJob.PARAM_ID, messaggioUuid);

                App.jobDispatcher().mustSchedule(App.jobDispatcher().newJobBuilder()
                        .setService(InvioMessaggioJob.class)
                        .setTrigger(Trigger.executionWindow(0, 0))
                        .setTag(messaggioUuid)
                        .setLifetime(Lifetime.FOREVER)
                        .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                        .setConstraints(
                                Constraint.ON_ANY_NETWORK
                        )
                        .setExtras(bundle)
                        .build());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {

    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        return RetryConstraint.createExponentialBackoff(runCount, 1000);
    }

    @Override
    protected int getRetryLimit() {
        return 5;
    }
}
