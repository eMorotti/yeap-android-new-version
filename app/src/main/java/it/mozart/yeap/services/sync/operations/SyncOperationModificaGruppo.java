package it.mozart.yeap.services.sync.operations;

import com.google.gson.Gson;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.api.models.GruppoApiModel;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Group;
import it.mozart.yeap.realm.Message;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.dao.GroupDao;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.services.sync.exceptions.SyncGruppoMismatchException;
import it.mozart.yeap.utility.Constants;

public class SyncOperationModificaGruppo extends SyncOperation {

    private ModificaGruppoModel mModificaGruppoModel;

    public SyncOperationModificaGruppo(SyncData syncData) {
        super(syncData);
    }

    @Override
    public void parseModel() {
        mModificaGruppoModel = new Gson().fromJson(mSyncDataContent, ModificaGruppoModel.class);
    }

    @Override
    public void doJob(Realm realm) throws Exception {
        if (mModificaGruppoModel == null)
            throw new Exception("Model is null");

        GroupDao gruppoDao = new GroupDao(realm);
        UserDao utenteDao = new UserDao(realm);
        MessageDao messaggioDao = new MessageDao(realm);

        Group gruppo = gruppoDao.getGroup(mModificaGruppoModel.groupUuid);
        if (gruppo == null) {
            throw new SyncGruppoMismatchException(mModificaGruppoModel.groupUuid);
        }

        if (gruppo.getVersion() < mModificaGruppoModel.groupVersion - 1 || mModificaGruppoModel.makerUuid == null) {
            throw new SyncGruppoMismatchException(mModificaGruppoModel.groupUuid);
        }

        // Modifico gruppo
        gruppo.setVersion(mModificaGruppoModel.groupVersion);
        uuid = gruppo.getUuid();

        if (mModificaGruppoModel.updates != null) {
            User maker = utenteDao.getUser(mModificaGruppoModel.makerUuid);
            if (maker == null) {
                throw new SyncGruppoMismatchException(mModificaGruppoModel.groupUuid);
            }
            letto = (App.isGroupVisible(mModificaGruppoModel.groupUuid) && App.isGroupPageVisible(Constants.GROUP_PAGE_CHAT))
                    || mModificaGruppoModel.makerUuid.equals(AccountProvider.getInstance().getAccountId());
            boolean ignoreLetto = true;

            if (mModificaGruppoModel.updates.getTitle() != null) {
                gruppo.setTitle(mModificaGruppoModel.updates.getTitle());

                // Creo messaggio sistema
                Message messaggio = messaggioDao.createSystemMessageGroupTitleUpdated(gruppo, maker, letto);
                messaggio.setCreatedAt(mModificaGruppoModel.timestamp);

                ignoreLetto = false;
            }
            if (mModificaGruppoModel.updates.getAvatarThumbUrl() != null) {
                gruppo.setAvatarThumbUrl(mModificaGruppoModel.updates.getAvatarThumbUrl());

                if (ignoreLetto) {
                    letto = true;
                }
            }
            if (mModificaGruppoModel.updates.getAvatarLargeUrl() != null) {
                gruppo.setAvatarLargeUrl(mModificaGruppoModel.updates.getAvatarLargeUrl());

                if (ignoreLetto) {
                    letto = true;
                }
            }
            if (mModificaGruppoModel.updates.getUsersCanInvite() != null) {
                gruppo.setUsersCanInvite(mModificaGruppoModel.updates.getUsersCanInvite());

                if (ignoreLetto) {
                    letto = true;
                }
            }
        }
    }

    @Override
    public boolean createNotifications(NotificationsHelper notifications) {
        if (!letto) {
            notifications.setInfo(Constants.TYPE_GRUPPO, uuid);
        }
        return !letto;
    }

    private static class ModificaGruppoModel {
        String groupUuid;
        int groupVersion;
        String makerUuid;
        long timestamp;
        GruppoApiModel updates;
    }
}
