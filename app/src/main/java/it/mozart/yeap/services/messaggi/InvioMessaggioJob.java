package it.mozart.yeap.services.messaggi;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;
import com.firebase.jobdispatcher.SimpleJobService;

import java.io.File;

import javax.inject.Inject;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.api.models.ImageApiModel;
import it.mozart.yeap.api.models.MessaggioApiModel;
import it.mozart.yeap.api.services.EventApi;
import it.mozart.yeap.api.services.GroupApi;
import it.mozart.yeap.api.services.UploadApi;
import it.mozart.yeap.realm.Message;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.services.imageSave.ImageSaveJob;
import it.mozart.yeap.utility.Constants;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.HttpException;
import rx.functions.Func1;

public class InvioMessaggioJob extends SimpleJobService {

    @SuppressWarnings("WeakerAccess")
    @Inject
    EventApi eventApi;
    @SuppressWarnings("WeakerAccess")
    @Inject
    GroupApi groupApi;
    @SuppressWarnings("WeakerAccess")
    @Inject
    UploadApi uploadApi;

    public static final String PARAM_ID = "id";

    @Override
    public boolean onStartJob(JobParameters job) {
        App.feather().injectFields(this);
        return super.onStartJob(job);
    }

    // Eseguo upload immagine
    private String uploadMediaImage(String mediaUrl) throws Exception {
        Uri uri = Uri.parse(mediaUrl);
        if (!uri.isAbsolute()) {
            uri = Uri.fromFile(new File(mediaUrl));
        }
        File file = new File(uri.getPath());
        if (!file.canRead())
            throw new Exception("Cannot load file");
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), file);
        MultipartBody.Part image = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
        ImageApiModel imageApiModel = uploadApi.uploadMediaImage(image)
                .toBlocking()
                .single();

        return imageApiModel.getUploadref();
    }

    @Override
    public int onRunJob(JobParameters job) {
        Bundle bundle = job.getExtras();
        if (bundle == null) {
            return 0;
        }

        final String mId = bundle.getString(PARAM_ID);

        final Realm realm = App.getRealm();
        final MessageDao messaggioDao = new MessageDao(realm);
        final Message messaggio;
        //noinspection TryFinallyCanBeTryWithResources
        try {
            messaggio = messaggioDao.getMessage(mId);
            if (messaggio != null && !messaggio.isSent()) {
                MessaggioApiModel messaggioApiModel = new MessaggioApiModel();
                messaggioApiModel.setUuid(messaggio.getUuid());
                messaggioApiModel.setText(messaggio.getText());

                if (messaggio.getMediaLocalUrl() != null) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(@NonNull Realm realm) {
                            messaggio.setImageState(Constants.IMAGE_STATE_UPLOADING);
                        }
                    });
                    messaggioApiModel.setMedia(uploadMediaImage(messaggio.getMediaLocalUrl()));
                }

                // Invio messaggio tramite api
                Object obj = null;
                if (messaggio.getTypology() == Constants.MESSAGGIO_TYPOLOGY_CHAT_EVENTO) {
                    obj = eventApi.sendMessage(messaggio.getEventParentUuid(), messaggioApiModel)
                            .onErrorReturn(new Func1<Throwable, Object>() {
                                @Override
                                public Object call(Throwable throwable) {
                                    return throwable;
                                }
                            })
                            .toBlocking()
                            .single();
                } else if (messaggio.getTypology() == Constants.MESSAGGIO_TYPOLOGY_CHAT_GRUPPO) {
                    obj = groupApi.sendMessage(messaggio.getGroupParentUuid(), messaggioApiModel)
                            .onErrorReturn(new Func1<Throwable, Object>() {
                                @Override
                                public Object call(Throwable throwable) {
                                    return throwable;
                                }
                            })
                            .toBlocking()
                            .single();
                }
                if (obj != null && obj instanceof Exception) {
                    throw ((Exception) obj);
                }

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(@NonNull Realm realm) {
                        if (messaggio.getMediaLocalUrl() != null) {
                            messaggio.setImageState(Constants.IMAGE_STATE_UPLOAD_COMPLETE);
                        }
                        messaggio.setSent(true);
                    }
                });

                // Salvo immagine nella cartella dell'app
                if (messaggio.getMediaLocalUrl() != null) {
                    App.jobManager().addJobInBackground(new ImageSaveJob(messaggio.getUuid(), messaggio.getMediaLocalUrl()));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (e instanceof HttpException) {
                switch (((HttpException) e).code()) {
                    case 403:
//                        realm.executeTransaction(new Realm.Transaction() {
//                            @Override
//                            public void execute(Realm realm) {
//                                Message messaggio = messaggioDao.getMessage(mId);
//                                messaggio.setError("Sei uscito dal gruppo, non puoi inviare messaggi");
//                                messaggio.setSent(true);
//                            }
//                        });
                        break;
                    default:
                        return JobService.RESULT_FAIL_RETRY;
                }
            } else {
                return JobService.RESULT_FAIL_RETRY;
            }
        } finally {
            realm.close();
        }

        return 0;
    }
}
