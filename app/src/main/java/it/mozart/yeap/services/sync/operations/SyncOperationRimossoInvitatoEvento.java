package it.mozart.yeap.services.sync.operations;

import com.google.gson.Gson;

import io.realm.Realm;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.services.sync.exceptions.SyncEventoMismatchException;

public class SyncOperationRimossoInvitatoEvento extends SyncOperation {

    private RimossoInvitatoEventoModel mRimossoInvitatoModel;

    public SyncOperationRimossoInvitatoEvento(SyncData syncData) {
        super(syncData);
    }

    @Override
    public void parseModel() {
        mRimossoInvitatoModel = new Gson().fromJson(mSyncDataContent, RimossoInvitatoEventoModel.class);
    }

    @Override
    public void doJob(Realm realm) throws Exception {
        if (mRimossoInvitatoModel == null)
            throw new Exception("Model is null");

        EventDao eventoDao = new EventDao(realm);
        UserDao utenteDao = new UserDao(realm);

        Event evento = eventoDao.getEvent(mRimossoInvitatoModel.activityUuid);
        if (evento == null) {
            throw new SyncEventoMismatchException(mRimossoInvitatoModel.activityUuid);
        }

        if (evento.getVersion() < mRimossoInvitatoModel.activityVersion - 1 || mRimossoInvitatoModel.phone == null) {
            throw new SyncEventoMismatchException(mRimossoInvitatoModel.activityUuid);
        }

        User utente = utenteDao.getUserLocal(mRimossoInvitatoModel.phone);
        if (evento.getGuestsNotYeap().contains(utente)) {
            evento.getGuestsNotYeap().remove(utente);
        } else {
            return;
        }

        evento.setVersion(mRimossoInvitatoModel.activityVersion);
        uuid = evento.getUuid();

        // Recupero utenti
        User maker = utenteDao.getUser(mRimossoInvitatoModel.makerUuid);
        if (maker == null) {
            throw new SyncEventoMismatchException(mRimossoInvitatoModel.activityUuid);
        }

        // Creo messaggio
        new MessageDao(realm).createSystemMessageEventInvitedRemoved(evento, maker, utente, true).setCreatedAt(mRimossoInvitatoModel.timestamp);
    }

    @Override
    public boolean createNotifications(NotificationsHelper notifications) {
        return false;
    }

    private static class RimossoInvitatoEventoModel {
        String activityUuid;
        int activityVersion;
        String makerUuid;
        long timestamp;
        String phone;
    }
}
