package it.mozart.yeap.services.syncAdapter.contacts;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmResults;
import it.mozart.yeap.App;
import it.mozart.yeap.api.models.ContactApiModel;
import it.mozart.yeap.api.services.ContactApi;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.dao.UserDao;

public class ContactsManager {

    synchronized void syncContacts(Context context, ContactApi contactApi) {
        long tti = System.currentTimeMillis();
        Log.d("CONTACT", "Sync contacts start ");
        List<Contatto> contatti = loadContatti(context);
        HashSet<String> phones = new HashSet<>();
        callApi(contatti, phones, contactApi);
        saveContactsToRealm(contatti, phones);
        Log.d("CONTACT", "Sync contacts end " + (System.currentTimeMillis() - tti));
    }

    private synchronized List<Contatto> loadContatti(Context context) {
        ContentResolver resolver = context.getContentResolver();
        List<Contatto> contatti = new ArrayList<>();
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        String phoneISO;
        try {
            phoneISO = phoneUtil.getRegionCodeForNumber(phoneUtil.parse(AccountProvider.getInstance().getAccountPhone(), null));
        } catch (NumberParseException e) {
            e.printStackTrace();
            return contatti;
        }

        String[] projection = new String[]{ContactsContract.CommonDataKinds.Phone.CONTACT_ID, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER};
        Cursor c = resolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection, null, null, null);
        while (c != null && c.moveToNext()) {
            long id = c.getLong(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
            String number = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            String name = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            try {
                Phonenumber.PhoneNumber phoneNumber = phoneUtil.parse(number, phoneISO);
                if (phoneUtil.getNumberType(phoneNumber) == PhoneNumberUtil.PhoneNumberType.MOBILE
                        || phoneUtil.getNumberType(phoneNumber) == PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE) {
                    Contatto contatto = new Contatto(id, name, null, phoneUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164));
                    if (!contatti.contains(contatto)) {
                        contatti.add(contatto);
                    }
                }
            } catch (NumberParseException ignored) {
                // non è un valido phone number
                Log.d("CONTACT", "FAILED " + number);
            }
        }
        if (c != null) {
            c.close();
        }

        return contatti;
    }

    private synchronized void callApi(List<Contatto> contatti, HashSet<String> phones, ContactApi contactApi) {
        List<ContactApiModel> contattiPerApi = new ArrayList<>();
        for (Contatto contatto : contatti) {
            phones.add(contatto.getNumber());
            ContactApiModel model = new ContactApiModel();
            model.setId(contatto.getNumber());
            model.setP(contatto.getNumber());
            contattiPerApi.add(model);
        }

        List<ContactApiModel> results = contactApi.sync(contattiPerApi)
                .toBlocking()
                .single();

        for (ContactApiModel model : results) {
            int index = contatti.indexOf(new Contatto(model.getId()));
            contatti.get(index).setUuid(model.getUuid());
        }
    }

    private synchronized void saveContactsToRealm(List<Contatto> contatti, HashSet<String> phones) {
        App.setSyncing(true);
        App.jobManager().stop();

        Realm realm = App.getRealm();
        //noinspection TryFinallyCanBeTryWithResources
        try {
            UserDao utenteDao = new UserDao(realm);
            for (Contatto contatto : contatti) {
                try {
                    if (contatto.getUuid() != null) {
                        User utente = utenteDao.getUserNotDeleted(contatto.getUuid());
                        if (utente != null) { // utente esiste
                            if (!isEqual(utente.getContactId(), contatto.getId()) || !isEqual(utente.getName(), contatto.getName()) ||
                                    !isEqual(utente.getSurname(), contatto.getSurname()) || !isEqual(utente.getPhone(), contatto.getNumber())
                                    || !utente.isInContacts() || !utente.isInYeap()) {
                                realm.beginTransaction();
                                utente.setContactId(contatto.getId());
                                utente.setName(contatto.getName());
                                utente.setSurname(contatto.getSurname());
                                utente.setPhone(contatto.getNumber());
                                utente.setInContacts(true);
                                utente.setInYeap(true);
                                realm.commitTransaction();
                            }
                        } else { // utente non esiste
                            User utenteLocal = utenteDao.getUserLocal(contatto.getNumber());
                            if (utenteLocal == null) {
                                User newUtente = new User();
                                newUtente.setId(String.format(Locale.getDefault(), "%s%d", contatto.getNumber(), System.currentTimeMillis()));
                                newUtente.setPhone(contatto.getNumber());
                                newUtente.setUuid(contatto.getUuid());
                                newUtente.setContactId(contatto.getId());
                                newUtente.setName(contatto.getName());
                                newUtente.setSurname(contatto.getSurname());
                                newUtente.setInContacts(true);
                                newUtente.setInYeap(true);

                                realm.beginTransaction();
                                utenteDao.createUser(newUtente);
                                realm.commitTransaction();
                            } else {
                                User newUtente = new User();
                                newUtente.setId(utenteLocal.getId());
                                newUtente.setContactId(contatto.getId());
                                newUtente.setUuid(contatto.getUuid());
                                newUtente.setName(contatto.getName());
                                newUtente.setSurname(contatto.getSurname());
                                newUtente.setPhone(contatto.getNumber());
                                newUtente.setInYeap(true);
                                newUtente.setInContacts(true);

                                realm.beginTransaction();
                                utenteLocal.deleteFromRealm();
                                realm.copyToRealm(newUtente);
                                realm.commitTransaction();
                            }
                        }
                    } else {
                        User utente = utenteDao.getUserButLocal(contatto.getUuid());
                        User utenteLocal = utenteDao.getUserLocal(contatto.getNumber());
                        if (utente == null) {
                            if (utenteLocal != null) { // utente locale esiste
                                if (!isEqual(utenteLocal.getContactId(), contatto.getId()) || !isEqual(utenteLocal.getName(), contatto.getName()) ||
                                        !isEqual(utenteLocal.getSurname(), contatto.getSurname()) || !isEqual(utenteLocal.getPhone(), contatto.getNumber())
                                        || !utenteLocal.isInContacts()) {
                                    realm.beginTransaction();
                                    utenteLocal.setContactId(contatto.getId());
                                    utenteLocal.setName(contatto.getName());
                                    utenteLocal.setSurname(contatto.getSurname());
                                    utenteLocal.setPhone(contatto.getNumber());
                                    utenteLocal.setInContacts(true);
                                    realm.commitTransaction();
                                }
                            } else { // utente locale non esiste
                                User newUtente = new User();
                                newUtente.setId(String.format(Locale.getDefault(), "%s%d", contatto.getNumber(), System.currentTimeMillis()));
                                newUtente.setPhone(contatto.getNumber());
                                newUtente.setContactId(contatto.getId());
                                newUtente.setName(contatto.getName());
                                newUtente.setSurname(contatto.getSurname());
                                newUtente.setInContacts(true);
                                newUtente.setInYeap(false);

                                realm.beginTransaction();
                                utenteDao.createUser(newUtente);
                                realm.commitTransaction();
                            }
                        }
                    }

                    RealmResults<User> utenti = utenteDao.getUsersByPhone(contatto.getNumber());
                    for (User utente : utenti) {
                        if (contatto.getUuid() != null) { // contatto in yeap
                            if (utente.getUuid() != null && !utente.getUuid().equals(contatto.getUuid())) {
                                if (!utente.isDeleted()) {
                                    realm.beginTransaction();
                                    utente.setDeleted(true);
                                    realm.commitTransaction();
                                }
                            }
                        } else { // contatto non in yeap
                            if (utente.getUuid() != null && !utente.isDeleted()) {
                                realm.beginTransaction();
                                utente.setDeleted(true);
                                realm.commitTransaction();
                            }
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    if (realm.isInTransaction()) {
                        realm.cancelTransaction();
                    }
                }
            }

            try {
                RealmResults<User> daNascondere = utenteDao.getUsersNotIn(phones);
                realm.beginTransaction();
                for (User user : daNascondere) {
                    if (!user.isInContacts() || user.getContactId() != null) {
                        user.setInContacts(false);
                        user.setContactId(null);
                    }
                }
                realm.commitTransaction();
                RealmResults<User> daCancellare = utenteDao.getUsersLocalNotIn(phones);
                if (daCancellare != null) {
                    realm.beginTransaction();
                    daCancellare.deleteAllFromRealm();
                    realm.commitTransaction();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                if (realm.isInTransaction()) {
                    realm.cancelTransaction();
                }
            }
        } finally {
            realm.close();
        }

        App.setSyncing(false);
        App.jobManager().start();
    }

    public List<List<String>> getDetailInfo(Context context, String contactId) {
        List<List<String>> info = new ArrayList<>();
        List<String> phoneNumbers = new ArrayList<>();
        List<String> emails = new ArrayList<>();

        ContentResolver resolver = context.getContentResolver();
        Cursor cursor = resolver.query(ContactsContract.Contacts.CONTENT_URI, null, ContactsContract.Contacts._ID + "=" + contactId, null, null);
        try {
            if (cursor != null) {
                cursor.moveToNext();
                long id = cursor.getLong(cursor.getColumnIndex(ContactsContract.Contacts._ID));

                Cursor cursorPhone = resolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id, null, null);
                Cursor cursorEmail = resolver.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + id, null, null);
                try {
                    if (cursorPhone != null) {
                        while (cursorPhone.moveToNext()) {
                            String number = cursorPhone.getString(cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            phoneNumbers.add(number);
                        }
                    }
                    if (cursorPhone != null) {
                        cursorPhone.close();
                    }
                    if (cursorEmail != null) {
                        while (cursorEmail.moveToNext()) {
                            String email = cursorEmail.getString(cursorEmail.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
                            emails.add(email);
                        }
                    }
                    if (cursorEmail != null) {
                        cursorEmail.close();
                    }
                } finally {
                    if (cursorPhone != null) {
                        cursorPhone.close();
                    }
                    if (cursorEmail != null) {
                        cursorEmail.close();
                    }
                }
            }
            if (cursor != null) {
                cursor.close();
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        info.add(phoneNumbers);
        info.add(emails);

        return info;
    }

    public List<List<String>> getDetailInfoFromPhone(Context context, String contactPhone) {
        List<List<String>> info = new ArrayList<>();
        List<String> phoneNumbers = new ArrayList<>();
        List<String> emails = new ArrayList<>();

        ContentResolver resolver = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(contactPhone));
        Cursor cursor = resolver.query(uri, null, null, null, null);
        try {
            if (cursor != null) {
                cursor.moveToNext();
                long id = cursor.getLong(cursor.getColumnIndex(ContactsContract.Contacts._ID));

                Cursor cursorPhone = resolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id, null, null);
                Cursor cursorEmail = resolver.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + id, null, null);
                try {
                    if (cursorPhone != null) {
                        while (cursorPhone.moveToNext()) {
                            String number = cursorPhone.getString(cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            phoneNumbers.add(number);
                        }
                    }
                    if (cursorPhone != null) {
                        cursorPhone.close();
                    }
                    if (cursorEmail != null) {
                        while (cursorEmail.moveToNext()) {
                            String email = cursorEmail.getString(cursorEmail.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
                            emails.add(email);
                        }
                    }
                    if (cursorEmail != null) {
                        cursorEmail.close();
                    }
                } finally {
                    if (cursorPhone != null) {
                        cursorPhone.close();
                    }
                    if (cursorEmail != null) {
                        cursorEmail.close();
                    }
                }
            }
            if (cursor != null) {
                cursor.close();
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        info.add(phoneNumbers);
        info.add(emails);

        return info;
    }

    private boolean isEqual(String value1, String value2) {
        if (value1 == null && value2 == null) {
            return true;
        }
        if (value1 == null || value2 == null) {
            return false;
        }
        return value1.equals(value2);
    }

    private static class Contatto {

        private long id;
        private String name;
        private String surname;
        private String number;
        private String uuid;

        public Contatto(String number) {
            this.number = number;
        }

        public Contatto(long id, String name, String surname, String number) {
            this.id = id;
            this.name = name;
            this.surname = surname;
            this.number = number.replace(" ", "").replace("(", "").replace(")", "").replace("/", "").replace("\\", "");
        }

        public String getId() {
            return String.valueOf(id);
        }

        public String getName() {
            return name;
        }

        public String getSurname() {
            return surname;
        }

        public String getNumber() {
            return number;
        }

        public boolean isYeap() {
            return uuid != null;
        }

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof Contatto) {
                return String.valueOf(number).equals(((Contatto) obj).getNumber());
            }
            return super.equals(obj);
        }
    }

//    private static final String ALLOWED_CHARACTERS = "0123456789qwertyuiopasdfghjklzxcvbnm";
//    private static final String NUMBERS = "0123456789";
//
//    private static String getRandomString(final int sizeOfRandomString) {
//        final Random random = new Random();
//        final StringBuilder sb = new StringBuilder(sizeOfRandomString);
//        for (int i = 0; i < sizeOfRandomString; ++i)
//            sb.append(ALLOWED_CHARACTERS.charAt(random.nextInt(ALLOWED_CHARACTERS.length())));
//        return sb.toString();
//    }
//
//    private static String getRandomNumber(final int sizeOfRandomString) {
//        final Random random = new Random();
//        final StringBuilder sb = new StringBuilder(sizeOfRandomString);
//        for (int i = 0; i < sizeOfRandomString; ++i)
//            sb.append(NUMBERS.charAt(random.nextInt(NUMBERS.length())));
//        return sb.toString();
//    }
//
//    public static void insertContacts(Context context) {
//        long tti = System.currentTimeMillis();
//        System.out.println("#### " + tti);
//        for (int i = 0; i < 500; i++) {
//            ArrayList<ContentProviderOperation> op_list = new ArrayList<ContentProviderOperation>();
//            op_list.clear();
//            try {
//
//                int backRefIndex = 0;
//                System.out.println("Array List: " + backRefIndex);
//                op_list.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
//                        .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
//                        .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
//                        .build());
//                //backRefIndex = backRefIndex+1;
//
//                op_list.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
//                        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, backRefIndex)
//                        .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
//                        .withValue(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME, getRandomString(8))
//                        .build());
//                //backRefIndex = backRefIndex+1;
//
//                op_list.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
//                        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, backRefIndex)
//                        .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
//                        .withValue(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME, getRandomString(6))
//                        .build());
//                //backRefIndex = backRefIndex+1;
//
//                op_list.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
//                        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, backRefIndex)
//                        .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
//                        .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, getRandomNumber(10))
//                        .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_HOME)
//                        .withValue(ContactsContract.CommonDataKinds.Phone.LABEL, "")
//                        .build());
//                // backRefIndex = backRefIndex+1;
//
//                ContentProviderResult[] result = context.getContentResolver().applyBatch(ContactsContract.AUTHORITY, op_list);
//                Uri uri = result[0].uri;
//                System.out.println("URI: " + uri);
//                System.out.println("Thread finish");
//            } catch (OperationApplicationException exp) {
//                exp.printStackTrace();
//            } catch (RemoteException exp) {
//                exp.printStackTrace();
//            }
//        }
//        System.out.println("#### finish " + (System.currentTimeMillis() - tti));
//    }
}