package it.mozart.yeap.services.sync.operations;

import com.google.gson.Gson;

import io.realm.Realm;
import it.mozart.yeap.realm.Group;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.dao.GroupDao;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.services.sync.exceptions.SyncGruppoMismatchException;

public class SyncOperationAggiungiInvitatoGruppo extends SyncOperation {

    private AggiungiPartecipanteGruppoModel mAggiungiPartecipanteModel;

    public SyncOperationAggiungiInvitatoGruppo(SyncData syncData) {
        super(syncData);
    }

    @Override
    public void parseModel() {
        mAggiungiPartecipanteModel = new Gson().fromJson(mSyncDataContent, AggiungiPartecipanteGruppoModel.class);
    }

    @Override
    public void doJob(Realm realm) throws Exception {
        if (mAggiungiPartecipanteModel == null)
            throw new Exception("Model is null");

        GroupDao groupDao = new GroupDao(realm);
        UserDao utenteDao = new UserDao(realm);

        Group gruppo = groupDao.getGroup(mAggiungiPartecipanteModel.groupUuid);
        if (gruppo == null) {
            throw new SyncGruppoMismatchException(mAggiungiPartecipanteModel.groupUuid);
        }

        if (gruppo.getVersion() < mAggiungiPartecipanteModel.groupVersion - 1 || mAggiungiPartecipanteModel.phone == null) {
            throw new SyncGruppoMismatchException(mAggiungiPartecipanteModel.groupUuid);
        }

        if (manageUserAlreadyPresent(gruppo, utenteDao)) {
            return;
        }

        gruppo.setVersion(mAggiungiPartecipanteModel.groupVersion);
        uuid = gruppo.getUuid();

        // Recupero utenti
        User maker = utenteDao.getUser(mAggiungiPartecipanteModel.makerUuid);
        if (maker == null) {
            throw new SyncGruppoMismatchException(mAggiungiPartecipanteModel.groupUuid);
        }
        User utente = utenteDao.getUserLocal(mAggiungiPartecipanteModel.phone);

        // Creo messaggio
        new MessageDao(realm).createSystemMessageGroupInvitedAdded(gruppo, maker, utente, true).setCreatedAt(mAggiungiPartecipanteModel.timestamp);
    }

    @Override
    public boolean createNotifications(NotificationsHelper notifications) {
        return false;
    }

    private boolean manageUserAlreadyPresent(Group gruppo, UserDao utenteDao) {
        // Creo utente se non presente
        User newUtente = utenteDao.getUserLocal(mAggiungiPartecipanteModel.phone);
        if (newUtente != null) {
            newUtente.setInYeap(false);
            // Aggiungo in invitati
            if (gruppo.getGuestsNotYeap().contains(newUtente)) {
                return true;
            } else {
                gruppo.getGuestsNotYeap().add(0, newUtente);
            }
        }

        return false;
    }

    private static class AggiungiPartecipanteGruppoModel {
        String groupUuid;
        int groupVersion;
        String makerUuid;
        long timestamp;
        String phone;
    }
}
