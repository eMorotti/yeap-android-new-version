package it.mozart.yeap.services.notifications;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;

import javax.inject.Inject;

import it.mozart.yeap.App;
import it.mozart.yeap.api.services.ProfileApi;
import rx.functions.Action1;

public class RegistrationIntentService extends IntentService {

    @Inject
    ProfileApi profileApi;

    public RegistrationIntentService() {
        super("RegIntentService");
        App.feather().injectFields(this);
    }

    public RegistrationIntentService(String name) {
        super(name);
        App.feather().injectFields(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d("FIREBASE_TOKEN", token != null ? token : "null");
        profileApi.sendPushToken(token, "android").subscribe(new Action1<Void>() {
            @Override
            public void call(Void aVoid) {
            }
        }, new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                throwable.printStackTrace();
            }
        });
    }
}
