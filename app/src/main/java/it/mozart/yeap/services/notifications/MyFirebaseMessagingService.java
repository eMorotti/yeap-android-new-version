package it.mozart.yeap.services.notifications;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import it.mozart.yeap.services.sync.SyncUnitService;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d("FIREBASE_PUSH", remoteMessage.getData().toString());
        if (remoteMessage.getData().size() == 0) {
            return;
        }

        if (remoteMessage.getData().get("sync") != null) {
            SyncUnitService.newGcmMessage(getApplicationContext());
        }
    }
}