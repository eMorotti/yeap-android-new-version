package it.mozart.yeap.services.sync;

import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.services.sync.operations.SyncOperation;
import it.mozart.yeap.services.sync.operations.SyncOperationAggiungiInvitatoEvento;
import it.mozart.yeap.services.sync.operations.SyncOperationAggiungiPartecipanteEvento;
import it.mozart.yeap.services.sync.operations.SyncOperationCambioStatoEvento;
import it.mozart.yeap.services.sync.operations.SyncOperationCreaEvento;
import it.mozart.yeap.services.sync.operations.SyncOperationCreaSondaggio;
import it.mozart.yeap.services.sync.operations.SyncOperationCreaSondaggioVoce;
import it.mozart.yeap.services.sync.operations.SyncOperationEliminaSondaggio;
import it.mozart.yeap.services.sync.operations.SyncOperationEliminaSondaggioVoce;
import it.mozart.yeap.services.sync.operations.SyncOperationEventoAggiuntoMe;
import it.mozart.yeap.services.sync.operations.SyncOperationMessaggioEvento;
import it.mozart.yeap.services.sync.operations.SyncOperationModificaEvento;
import it.mozart.yeap.services.sync.operations.SyncOperationModificaEventoInfo;
import it.mozart.yeap.services.sync.operations.SyncOperationRimossoInvitatoEvento;
import it.mozart.yeap.services.sync.operations.SyncOperationRimossoPartecipanteEvento;
import it.mozart.yeap.services.sync.operations.SyncOperationSondaggioApertura;
import it.mozart.yeap.services.sync.operations.SyncOperationSondaggioChiusura;
import it.mozart.yeap.services.sync.operations.SyncOperationUscitaPartecipanteEvento;
import it.mozart.yeap.services.sync.operations.SyncOperationUtenteAvatarCambiato;
import it.mozart.yeap.services.sync.operations.SyncOperationVotoEvento;
import it.mozart.yeap.services.sync.operations.SyncOperationVotoSondaggioVoce;
import it.mozart.yeap.utility.Constants;

class SyncJobFactory {

    static SyncJob create(SyncData unit) throws Exception {
        SyncOperation syncOperation = null;
        switch (unit.getType()) {
            case Constants.SYNC_UNIT_EVENTO_CREA:
                syncOperation = new SyncOperationCreaEvento(unit);
                break;
            case Constants.SYNC_UNIT_EVENTO_MESSAGGIO:
                syncOperation = new SyncOperationMessaggioEvento(unit);
                break;
            case Constants.SYNC_UNIT_EVENTO_CAMBIO_STATO:
                syncOperation = new SyncOperationCambioStatoEvento(unit);
                break;
            case Constants.SYNC_UNIT_EVENTO_MODIFICA:
                syncOperation = new SyncOperationModificaEvento(unit);
                break;
            case Constants.SYNC_UNIT_EVENTO_AGGIUNTO_PARTECIPANTE:
                syncOperation = new SyncOperationAggiungiPartecipanteEvento(unit);
                break;
            case Constants.SYNC_UNIT_EVENTO_MODIFICA_INFO:
                syncOperation = new SyncOperationModificaEventoInfo(unit);
                break;
            case Constants.SYNC_UNIT_EVENTO_PARTECIPANTE_USCITO:
                syncOperation = new SyncOperationUscitaPartecipanteEvento(unit);
                break;
            case Constants.SYNC_UNIT_EVENTO_RIMOSSO_PARTECIPANTE:
                syncOperation = new SyncOperationRimossoPartecipanteEvento(unit);
                break;
            case Constants.SYNC_UNIT_EVENTO_VOTO:
                syncOperation = new SyncOperationVotoEvento(unit);
                break;
            case Constants.SYNC_UNIT_EVENTO_AGGIUNTO_ME:
                syncOperation = new SyncOperationEventoAggiuntoMe(unit);
                break;
            case Constants.SYNC_UNIT_EVENTO_AGGIUNTO_INVITATO:
                syncOperation = new SyncOperationAggiungiInvitatoEvento(unit);
                break;
            case Constants.SYNC_UNIT_EVENTO_RIMOSSO_INVITATO:
                syncOperation = new SyncOperationRimossoInvitatoEvento(unit);
                break;

//            case Constants.SYNC_UNIT_GRUPPO_CREA:
//                syncOperation = new SyncOperationCreaGruppo(unit);
//                break;
//            case Constants.SYNC_UNIT_GRUPPO_AGGIUNTO_PARTECIPANTE:
//                syncOperation = new SyncOperationAggiungiPartecipanteGruppo(unit);
//                break;
//            case Constants.SYNC_UNIT_GRUPPO_PARTECIPANTE_USCITO:
//                syncOperation = new SyncOperationUscitaPartecipanteGruppo(unit);
//                break;
//            case Constants.SYNC_UNIT_GRUPPO_RIMOSSO_PARTECIPANTE:
//                syncOperation = new SyncOperationRimossoPartecipanteGruppo(unit);
//                break;
//            case Constants.SYNC_UNIT_GRUPPO_MODIFICATO:
//                syncOperation = new SyncOperationModificaGruppo(unit);
//                break;
//            case Constants.SYNC_UNIT_GRUPPO_MESSAGGIO:
//                syncOperation = new SyncOperationMessaggioGruppo(unit);
//                break;
//            case Constants.SYNC_UNIT_GRUPPO_AGGIUNTO_ME:
//                syncOperation = new SyncOperationGruppoAggiuntoMe(unit);
//                break;
//            case Constants.SYNC_UNIT_GRUPPO_AGGIUNTO_INVITATO:
//                syncOperation = new SyncOperationAggiungiInvitatoGruppo(unit);
//                break;
//            case Constants.SYNC_UNIT_GRUPPO_RIMOSSO_INVITATO:
//                syncOperation = new SyncOperationRimossoInvitatoGruppo(unit);
//                break;

            case Constants.SYNC_UNIT_SONDAGGIO_AGGIUNTO:
                syncOperation = new SyncOperationCreaSondaggio(unit);
                break;
            case Constants.SYNC_UNIT_SONDAGGIO_ELIMINATO:
                syncOperation = new SyncOperationEliminaSondaggio(unit);
                break;
            case Constants.SYNC_UNIT_SONDAGGIO_VOCE_AGGIUNTA:
                syncOperation = new SyncOperationCreaSondaggioVoce(unit);
                break;
            case Constants.SYNC_UNIT_SONDAGGIO_VOCE_ELIMINATA:
                syncOperation = new SyncOperationEliminaSondaggioVoce(unit);
                break;
            case Constants.SYNC_UNIT_SONDAGGIO_VOCE_VOTO:
                syncOperation = new SyncOperationVotoSondaggioVoce(unit);
                break;
            case Constants.SYNC_UNIT_SONDAGGIO_APERTO:
                syncOperation = new SyncOperationSondaggioApertura(unit);
                break;
            case Constants.SYNC_UNIT_SONDAGGIO_CHIUSO:
                syncOperation = new SyncOperationSondaggioChiusura(unit);
                break;

            case Constants.SYNC_UNIT_USER_AVATAR_CHANGE:
                syncOperation = new SyncOperationUtenteAvatarCambiato(unit);
                break;
        }
        if (syncOperation == null) {
            throw new Exception("SynUnit type non riconosciuto: " + unit.getType());
        }
        return new SyncJob(unit.getId(), syncOperation);
    }
}