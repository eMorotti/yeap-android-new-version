package it.mozart.yeap.services.messaggi;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.schinizer.rxunfurl.RxUnfurl;
import com.schinizer.rxunfurl.model.PreviewData;

import org.greenrobot.eventbus.EventBus;

import io.reactivex.functions.Consumer;
import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.common.UrlPreviewData;
import it.mozart.yeap.events.UrlFoundEvent;
import it.mozart.yeap.events.UrlPreviewUpdatedEvent;
import it.mozart.yeap.realm.Message;
import it.mozart.yeap.realm.dao.MessageDao;
import okhttp3.OkHttpClient;

public class UrlPreviewJob extends AsyncTask<Void, Void, Void> {

    private String mMessageId;
    private String mUrl;
    private String mId;

    public UrlPreviewJob(String messageUuid, String url, String id) {
        mMessageId = messageUuid;
        mUrl = url;
        mId = id;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        if (mUrl != null) {
            OkHttpClient okHttpClient = new OkHttpClient();

            RxUnfurl rx = new RxUnfurl.Builder()
                    .client(okHttpClient)
                    .build();

            rx.generatePreview(mUrl).subscribe(new Consumer<PreviewData>() {
                @Override
                public void accept(@io.reactivex.annotations.NonNull PreviewData data) throws Exception {
                    Realm realm = App.getRealm();
                    //noinspection TryFinallyCanBeTryWithResources
                    try {
                        if (mUrl == null) {
                            if (mMessageId != null) {
                                final Message message = new MessageDao(realm).getMessage(mMessageId);
                                if (message != null) {
                                    realm.executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(@NonNull Realm realm) {
                                            message.setUrlChecked(true);
                                        }
                                    });
                                }
                            }
                        } else if (data != null && (!data.getTitle().isEmpty() || !data.getDescription().isEmpty())) {
                            if (mMessageId != null) {
                                final Message message = new MessageDao(realm).getMessage(mMessageId);
                                if (message != null) {
                                    final PreviewData finalPreviewData = data;
                                    realm.executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(@NonNull Realm realm) {
                                            message.setUrlChecked(true);
                                            message.setUrl(finalPreviewData.getUrl());
                                            message.setUrlInfo(new Gson().toJson(new UrlPreviewData().fromData(finalPreviewData), UrlPreviewData.class));
                                        }
                                    });
                                    EventBus.getDefault().post(new UrlPreviewUpdatedEvent());
                                }
                            } else {
                                EventBus.getDefault().post(new UrlFoundEvent(mId, mUrl, data));
                            }
                        } else {
                            if (mMessageId != null) {
                                final Message message = new MessageDao(realm).getMessage(mMessageId);
                                if (message != null) {
                                    realm.executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(@NonNull Realm realm) {
                                            message.setUrlChecked(true);
                                        }
                                    });
                                }
                            }
                        }
                    } finally {
                        realm.close();
                    }
                }
            }, new Consumer<Throwable>() {
                @Override
                public void accept(@io.reactivex.annotations.NonNull Throwable throwable) throws Exception {
                }
            });
        }

        return null;
    }
}
