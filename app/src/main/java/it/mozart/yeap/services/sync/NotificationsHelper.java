package it.mozart.yeap.services.sync;

import android.content.Context;
import android.support.v4.app.NotificationManagerCompat;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.utility.debounce.NotificationInfo;
import it.mozart.yeap.utility.debounce.NotificationListener;
import it.mozart.yeap.utility.debounce.NotificationUpdater;
import me.leolin.shortcutbadger.ShortcutBadger;

public class NotificationsHelper {

    private NotificationListener listener;
    private NotificationInfo info;

    public NotificationsHelper() {
        new NotificationUpdater().setNotificationObservable(this, 10, 500);
    }

    public void setInfo(int type, String id) {
        info = new NotificationInfo(type, id);
    }

    public void callNotification() {
        if (listener != null) {
            listener.startNotification(info);
        }
    }

    public void setNotificationListener(NotificationListener listener) {
        this.listener = listener;
    }

    public void createNotifications(NotificationInfo info) {
        App.jobManager().addJobInBackground(new SyncUnitNotificationJob(info));
    }

    public void cancelAllNotification(Context context) {
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.cancelAll();

        if (ShortcutBadger.isBadgeCounterSupported(context)) {
            Realm realm = App.getRealm();
            //noinspection TryFinallyCanBeTryWithResources
            try {
                MessageDao messageDao = new MessageDao(realm);
                ShortcutBadger.applyCount(context, messageDao.getNumNotReadMessagedForEvent() + messageDao.getNumNotReadMessagedForGroup());
            } finally {
                realm.close();
            }
        }
    }
}