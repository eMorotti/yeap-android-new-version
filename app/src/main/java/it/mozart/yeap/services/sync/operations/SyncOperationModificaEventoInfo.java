package it.mozart.yeap.services.sync.operations;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.Message;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.services.sync.exceptions.SyncEventoMismatchException;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.DateUtils;

public class SyncOperationModificaEventoInfo extends SyncOperation {

    private ModificaEventoInfoModel mModificaEventoInfoModel;

    public SyncOperationModificaEventoInfo(SyncData syncData) {
        super(syncData);
    }

    @Override
    public void parseModel() {
        mModificaEventoInfoModel = new Gson().fromJson(mSyncDataContent, ModificaEventoInfoModel.class);
    }

    @Override
    public void doJob(Realm realm) throws Exception {
        if (mModificaEventoInfoModel == null)
            throw new Exception("Model is null");

        EventDao eventoDao = new EventDao(realm);
        UserDao utenteDao = new UserDao(realm);
        MessageDao messaggioDao = new MessageDao(realm);

        Event evento = eventoDao.getEvent(mModificaEventoInfoModel.activityUuid);
        if (evento == null || evento.getEventInfo() == null) {
            throw new SyncEventoMismatchException(mModificaEventoInfoModel.activityUuid);
        }

        if (evento.getVersion() < mModificaEventoInfoModel.activityVersion - 1 || mModificaEventoInfoModel.makerUuid == null) {
            throw new SyncEventoMismatchException(mModificaEventoInfoModel.activityUuid);
        }

        // Modifico attivita
        evento.setVersion(mModificaEventoInfoModel.activityVersion);
        evento.setUpdatedAt(mModificaEventoInfoModel.timestamp);
        uuid = evento.getUuid();

        if (mModificaEventoInfoModel.updates != null) {
            User maker = utenteDao.getUser(mModificaEventoInfoModel.makerUuid);
            if (maker == null) {
                throw new SyncEventoMismatchException(mModificaEventoInfoModel.activityUuid);
            }
            letto = (App.isEventVisible(mModificaEventoInfoModel.activityUuid) && App.isEventPageVisible(Constants.EVENT_PAGE_DETAILS))
                    || mModificaEventoInfoModel.makerUuid.equals(AccountProvider.getInstance().getAccountId());

            int cont = 0;
            // Modifica evento info
            if (mModificaEventoInfoModel.updates.has("what")) {
                String what = mModificaEventoInfoModel.updates.get("what").getAsString();
                if (what.isEmpty()) {
                    evento.getEventInfo().setWhat(null);
                } else {
                    evento.getEventInfo().setWhat(what);
                    cont += 1;
                }
            }
            if (mModificaEventoInfoModel.updates.has("where")) {
                JsonElement where = mModificaEventoInfoModel.updates.get("where");
                if (where.isJsonNull() || where.getAsString().isEmpty()) {
                    evento.getEventInfo().setWhere(null);
                } else {
                    evento.getEventInfo().setWhere(where.getAsString());
                    cont += 2;
                }

                if (mModificaEventoInfoModel.updates.has("placeId")) {
                    JsonElement placeId = mModificaEventoInfoModel.updates.get("placeId");
                    if (placeId.isJsonNull() || placeId.getAsString().isEmpty()) {
                        evento.getEventInfo().setPlaceId(null);
                    } else {
                        evento.getEventInfo().setPlaceId(placeId.getAsString());
                    }
                }
                if (mModificaEventoInfoModel.updates.has("lat")) {
                    JsonElement lat = mModificaEventoInfoModel.updates.get("lat");
                    if (lat.isJsonNull()) {
                        evento.getEventInfo().setLat(0);
                    } else {
                        evento.getEventInfo().setLat(lat.getAsDouble());
                    }
                }
                if (mModificaEventoInfoModel.updates.has("lng")) {
                    JsonElement lng = mModificaEventoInfoModel.updates.get("lng");
                    if (lng.isJsonNull()) {
                        evento.getEventInfo().setLng(0);
                    } else {
                        evento.getEventInfo().setLng(lng.getAsDouble());
                    }
                }
            }

            if (mModificaEventoInfoModel.updates.has("fromDate")) {
                JsonElement fromDate = mModificaEventoInfoModel.updates.get("fromDate");
                if (fromDate.isJsonNull() || fromDate.getAsString().isEmpty()) {
                    evento.getEventInfo().setFromDate(null);
                } else {
                    evento.getEventInfo().setFromDate(fromDate.getAsString());
                    cont += 4;
                }
            }
            if (mModificaEventoInfoModel.updates.has("fromHour")) {
                JsonElement fromHour = mModificaEventoInfoModel.updates.get("fromHour");
                if (fromHour.isJsonNull() || fromHour.getAsString().isEmpty()) {
                    evento.getEventInfo().setFromHour(null);
                } else {
                    evento.getEventInfo().setFromHour(fromHour.getAsString());
                    cont += 4;
                }
            }
            if (mModificaEventoInfoModel.updates.has("toDate")) {
                JsonElement toDate = mModificaEventoInfoModel.updates.get("toDate");
                if (toDate.isJsonNull() || toDate.getAsString().isEmpty()) {
                    evento.getEventInfo().setToDate(null);
                } else {
                    evento.getEventInfo().setToDate(toDate.getAsString());
                    cont += 4;
                }
            }
            if (mModificaEventoInfoModel.updates.has("toHour")) {
                JsonElement toHour = mModificaEventoInfoModel.updates.get("toHour");
                if (toHour.isJsonNull() || toHour.getAsString().isEmpty()) {
                    evento.getEventInfo().setToHour(null);
                } else {
                    evento.getEventInfo().setToHour(toHour.getAsString());
                    cont += 4;
                }
            }

            Message message;
            switch (cont) {
                case 1:
                    message = messaggioDao.createSystemMessageEventInfoModifiedWhat(evento, maker, letto);
                    break;
                case 2:
                    message = messaggioDao.createSystemMessageEventInfoModifiedWhere(evento, maker, letto);
                    break;
                case 3:
                    message = messaggioDao.createSystemMessageEventTwoInfoModified(evento, maker, evento.getEventInfo().getWhat(), evento.getEventInfo().getWhere(), letto);
                    break;
                case 5:
                case 9:
                case 13:
                case 17:
                    message = messaggioDao.createSystemMessageEventTwoInfoModified(evento, maker, evento.getEventInfo().getWhat(), DateUtils.createQuandoText(evento.getEventInfo().generateQuandoModel()), letto);
                    break;
                case 6:
                case 10:
                case 14:
                case 18:
                    message = messaggioDao.createSystemMessageEventTwoInfoModified(evento, maker, evento.getEventInfo().getWhere(), DateUtils.createQuandoText(evento.getEventInfo().generateQuandoModel()), letto);
                    break;
                case 4:
                case 8:
                case 12:
                case 16:
                    message = messaggioDao.createSystemMessageEventInfoModifiedWhen(evento, maker, letto);
                    break;
                default:
                    message = messaggioDao.createSystemMessageEventAllInfoModified(evento, maker, evento.getEventInfo().getWhat(), evento.getEventInfo().getWhere(), DateUtils.createQuandoText(evento.getEventInfo().generateQuandoModel()), letto);
                    break;
            }
            message.setCreatedAt(mModificaEventoInfoModel.timestamp);
        }
    }

    @Override
    public boolean createNotifications(NotificationsHelper notifications) {
        if (!letto) {
            notifications.setInfo(Constants.TYPE_EVENTO, uuid);
        }
        return !letto;
    }

    private static class ModificaEventoInfoModel {
        String activityUuid;
        int activityVersion;
        String makerUuid;
        long timestamp;
        String eventUuid;
        JsonObject updates;
    }
}
