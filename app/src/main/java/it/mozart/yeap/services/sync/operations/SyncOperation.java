package it.mozart.yeap.services.sync.operations;

import com.google.gson.JsonSyntaxException;

import io.realm.Realm;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.services.sync.NotificationsHelper;

public abstract class SyncOperation {

    protected String mSyncDataContent;
    protected String uuid;
    protected boolean letto;

    private static final String DEFAULT_QUEUE = "SyncQueue";

    SyncOperation(SyncData syncData) {
        if (syncData != null) {
            mSyncDataContent = syncData.getContent();
        }
        letto = true;
        try {
            parseModel();
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
    }

    public abstract void parseModel();

    public abstract void doJob(Realm realm) throws Exception;

    public final String queueId() {
        return DEFAULT_QUEUE;
    }

    public abstract boolean createNotifications(NotificationsHelper notifications);
}
