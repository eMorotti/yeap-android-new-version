package it.mozart.yeap.services.sync.operations;

import com.google.gson.Gson;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.realm.dao.PollDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.realm.dao.VoteDao;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.services.sync.SyncTotal;
import it.mozart.yeap.utility.Constants;

public class SyncOperationEventoAggiuntoMe extends SyncOperation {

    private AggiungiMeEventoModel mAggiungiMeEventoModel;

    public SyncOperationEventoAggiuntoMe(SyncData syncData) {
        super(syncData);
    }

    @Override
    public void parseModel() {
        mAggiungiMeEventoModel = new Gson().fromJson(mSyncDataContent, AggiungiMeEventoModel.class);
    }

    @Override
    public void doJob(Realm realm) throws Exception {
        if (mAggiungiMeEventoModel == null)
            throw new Exception("Model is null");

        EventDao eventDao = new EventDao(realm);
        UserDao utenteDao = new UserDao(realm);
        VoteDao voteDao = new VoteDao(realm);
        PollDao pollDao = new PollDao(realm);
        MessageDao messageDao = new MessageDao(realm);

        // Sync attività
        Event evento = SyncTotal.syncEvento(realm, mAggiungiMeEventoModel.activity, eventDao, utenteDao, voteDao, pollDao);

        if (mAggiungiMeEventoModel.makerUuid != null) {
            manageMyselfAdded(evento, messageDao, utenteDao);
        }
    }

    private void manageMyselfAdded(Event evento, MessageDao messageDao, UserDao userDao) {
        User myself = userDao.getUser(AccountProvider.getInstance().getAccountId());

        evento.setMyselfPresent(true);
        if (!evento.getGuests().contains(myself)) {
            evento.getGuests().add(myself);
        }

        letto = App.isGroupVisible(mAggiungiMeEventoModel.activityUuid);

        // Creo messaggio
        messageDao.createSystemMessageEventUserAdded(evento, userDao.getUser(mAggiungiMeEventoModel.makerUuid), myself, letto).setCreatedAt(mAggiungiMeEventoModel.timestamp);
    }

    @Override
    public boolean createNotifications(NotificationsHelper notifications) {
        // Invio notifiche
        if (!letto) {
            notifications.setInfo(Constants.TYPE_EVENTO, mAggiungiMeEventoModel.activityUuid);
        }
        return !letto;
    }

    private static class AggiungiMeEventoModel {
        String activityUuid;
        int activityVersion;
        String makerUuid;
        long timestamp;
        Event activity;
    }
}
