package it.mozart.yeap.services.sync.operations;

import com.google.gson.Gson;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Group;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.dao.GroupDao;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.services.sync.SyncTotal;
import it.mozart.yeap.utility.Constants;

public class SyncOperationGruppoAggiuntoMe extends SyncOperation {

    private AggiungiMeGruppoModel mAggiungiMeGruppoModel;

    public SyncOperationGruppoAggiuntoMe(SyncData syncData) {
        super(syncData);
    }

    @Override
    public void parseModel() {
        mAggiungiMeGruppoModel = new Gson().fromJson(mSyncDataContent, AggiungiMeGruppoModel.class);
    }

    @Override
    public void doJob(Realm realm) throws Exception {
        if (mAggiungiMeGruppoModel == null)
            throw new Exception("Model is null");

        GroupDao gruppoDao = new GroupDao(realm);
        UserDao utenteDao = new UserDao(realm);
        MessageDao messageDao = new MessageDao(realm);

        // Sync gruppo
        Group gruppo = SyncTotal.syncGruppo(mAggiungiMeGruppoModel.group, gruppoDao, utenteDao);

        if (mAggiungiMeGruppoModel.makerUuid != null) {
            manageMyselfAdded(gruppo, messageDao, utenteDao);
        }
    }

    private void manageMyselfAdded(Group gruppo, MessageDao messageDao, UserDao userDao) {
        User myself = userDao.getUser(AccountProvider.getInstance().getAccountId());

        gruppo.setMyselfPresent(true);
        if (!gruppo.getGuests().contains(myself)) {
            gruppo.getGuests().add(myself);
        }

        letto = App.isGroupVisible(mAggiungiMeGruppoModel.groupUuid);

        // Creo messaggio
        messageDao.createSystemMessageGroupUserAdded(gruppo, userDao.getUser(mAggiungiMeGruppoModel.makerUuid), myself, letto).setCreatedAt(mAggiungiMeGruppoModel.timestamp);
    }

    @Override
    public boolean createNotifications(NotificationsHelper notifications) {
        // Invio notifiche
        if (!letto) {
            notifications.setInfo(Constants.TYPE_GRUPPO, mAggiungiMeGruppoModel.groupUuid);
        }
        return !letto;
    }

    private static class AggiungiMeGruppoModel {
        String groupUuid;
        int groupVersion;
        String makerUuid;
        long timestamp;
        Group group;
    }
}
