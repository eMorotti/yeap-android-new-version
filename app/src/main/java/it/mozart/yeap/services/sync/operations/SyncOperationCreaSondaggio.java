package it.mozart.yeap.services.sync.operations;

import com.google.gson.Gson;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.Poll;
import it.mozart.yeap.realm.PollItem;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.PollDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.services.sync.SyncTotal;
import it.mozart.yeap.services.sync.exceptions.SyncEventoMismatchException;
import it.mozart.yeap.utility.Constants;

public class SyncOperationCreaSondaggio extends SyncOperation {

    private CreaSondaggioModel mCreaSondaggioModel;

    public SyncOperationCreaSondaggio(SyncData syncData) {
        super(syncData);
    }

    @Override
    public void parseModel() {
        mCreaSondaggioModel = new Gson().fromJson(mSyncDataContent, CreaSondaggioModel.class);
    }

    @Override
    public void doJob(Realm realm) throws Exception {
        if (mCreaSondaggioModel == null)
            throw new Exception("Model is null");

        EventDao eventoDao = new EventDao(realm);
        PollDao sondaggioDao = new PollDao(realm);
        UserDao userDao = new UserDao(realm);

        Event evento = eventoDao.getEvent(mCreaSondaggioModel.activityUuid);
        if (evento == null) {
            throw new SyncEventoMismatchException(mCreaSondaggioModel.activityUuid);
        }

        if (evento.getVersion() < mCreaSondaggioModel.activityVersion - 1 || mCreaSondaggioModel.poll == null) {
            throw new SyncEventoMismatchException(mCreaSondaggioModel.activityUuid);
        }

        evento.setVersion(mCreaSondaggioModel.activityVersion);
        uuid = evento.getUuid();

        if (mCreaSondaggioModel.poll.isDeleted()) {
            return;
        }

        Poll sondaggio = sondaggioDao.getPoll(mCreaSondaggioModel.poll.getUuid());
        boolean notPresent = false;
        if (sondaggio == null) {
            notPresent = true;
            sondaggio = new Poll();
            sondaggio.setUuid(mCreaSondaggioModel.poll.getUuid());
        }
        SyncTotal.syncSondaggio(userDao, sondaggio, mCreaSondaggioModel.poll, evento);

        letto = (App.isEventVisible(mCreaSondaggioModel.activityUuid) && App.isEventPageVisible(Constants.EVENT_PAGE_POLLS))
                || mCreaSondaggioModel.poll.getCreatorUuid().equals(AccountProvider.getInstance().getAccountId());

        if (notPresent) {
            if (sondaggio.getType() == Constants.POLL_TYPE_WHERE || sondaggio.getType() == Constants.POLL_TYPE_WHEN) {
                sondaggioDao.createPoll(sondaggio, true, letto, false);
            } else {
                sondaggioDao.createPoll(sondaggio, false, letto, false);
            }

            if (mCreaSondaggioModel.poll.getPollItems() != null) {
                for (PollItem sondaggioVoceInfo : mCreaSondaggioModel.poll.getPollItems()) {
                    PollItem sondaggioVoce = sondaggioDao.getPollItem(sondaggioVoceInfo.getUuid());
                    boolean notPresentBis = false;
                    if (sondaggioVoce == null) {
                        notPresentBis = true;
                        sondaggioVoce = new PollItem();
                        sondaggioVoce.setUuid(sondaggioVoceInfo.getUuid());
                    }
                    SyncTotal.syncSondaggioVoce(userDao, sondaggioVoce, sondaggioVoceInfo, sondaggio);

                    if (notPresentBis) {
                        if (sondaggio.getType() == Constants.POLL_TYPE_WHERE || sondaggio.getType() == Constants.POLL_TYPE_WHEN) {
                            sondaggioDao.createPollItem(sondaggioVoce, true, letto, true);
                        } else {
                            sondaggioDao.createPollItem(sondaggioVoce, false, letto, true);
                        }
                    }
                }
            }
        }

        letto = notPresent && !letto;
    }

    @Override
    public boolean createNotifications(NotificationsHelper notifications) {
        // Invio notifiche
        if (letto) {
            notifications.setInfo(Constants.TYPE_EVENTO, uuid);
        }
        return letto;
    }

    private static class CreaSondaggioModel {
        String activityUuid;
        int activityVersion;
        Poll poll;
    }
}
