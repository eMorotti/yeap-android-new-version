package it.mozart.yeap.services.sync.operations;

import com.google.gson.Gson;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.Message;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.realm.dao.PollDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.realm.dao.VoteDao;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.services.sync.SyncTotal;
import it.mozart.yeap.services.sync.exceptions.SyncEventoMismatchException;
import it.mozart.yeap.utility.Constants;

public class SyncOperationRimossoPartecipanteEvento extends SyncOperation {

    private RimossoPartecipanteEventoModel mRimossoPartecipanteModel;

    public SyncOperationRimossoPartecipanteEvento(SyncData syncData) {
        super(syncData);
    }

    @Override
    public void parseModel() {
        mRimossoPartecipanteModel = new Gson().fromJson(mSyncDataContent, RimossoPartecipanteEventoModel.class);
    }

    @Override
    public void doJob(Realm realm) throws Exception {
        if (mRimossoPartecipanteModel == null)
            throw new Exception("Model is null");

        EventDao eventoDao = new EventDao(realm);
        UserDao utenteDao = new UserDao(realm);

        Event evento = eventoDao.getEvent(mRimossoPartecipanteModel.activityUuid);
        if (evento == null) {
            throw new SyncEventoMismatchException(mRimossoPartecipanteModel.activityUuid);
        }

        if (evento.getVersion() < mRimossoPartecipanteModel.activityVersion - 1 || mRimossoPartecipanteModel.userUuid == null) {
            throw new SyncEventoMismatchException(mRimossoPartecipanteModel.activityUuid);
        }

        if (evento.getVersion() + 1 == mRimossoPartecipanteModel.activityVersion) {
            User utente = utenteDao.getUser(mRimossoPartecipanteModel.userUuid);
            if (evento.getGuests().contains(utente)) {
                evento.getGuests().remove(utente);
                if (utente.getUuid().equals(AccountProvider.getInstance().getAccountId())) {
                    evento.setMyselfPresent(false);
                }
            } else {
                return;
            }
        }

        evento.setVersion(mRimossoPartecipanteModel.activityVersion);
        uuid = evento.getUuid();

        // Recupero utenti
        User maker = utenteDao.getUser(mRimossoPartecipanteModel.makerUuid);
        if (maker == null) {
            throw new SyncEventoMismatchException(mRimossoPartecipanteModel.activityUuid);
        }
        User utente = utenteDao.getUser(mRimossoPartecipanteModel.userUuid);
        letto = (App.isEventVisible(mRimossoPartecipanteModel.activityUuid) && App.isEventPageVisible(Constants.EVENT_PAGE_CHAT))
                || mRimossoPartecipanteModel.makerUuid.equals(AccountProvider.getInstance().getAccountId());

        // Creo messaggio
        Message message = new MessageDao(realm).createSystemMessageEventUserRemoved(evento, maker, utente, letto);
        message.setCreatedAt(mRimossoPartecipanteModel.timestamp);
        letto = !letto && !message.isIgnoreRead();

        SyncTotal.syncEvento(realm, mRimossoPartecipanteModel.activity, eventoDao, utenteDao, new VoteDao(realm), new PollDao(realm));
    }

    @Override
    public boolean createNotifications(NotificationsHelper notifications) {
        // Invio notifiche
        if (letto) {
            notifications.setInfo(Constants.TYPE_EVENTO, uuid);
        }
        return letto;
    }

    private static class RimossoPartecipanteEventoModel {
        String activityUuid;
        int activityVersion;
        String makerUuid;
        long timestamp;
        String userUuid;
        Event activity;
    }
}
