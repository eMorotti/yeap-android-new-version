package it.mozart.yeap.services.sync.operations;

import com.google.gson.Gson;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.Poll;
import it.mozart.yeap.realm.PollItem;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.realm.dao.PollDao;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.services.sync.exceptions.SyncEventoMismatchException;
import it.mozart.yeap.utility.Constants;

public class SyncOperationEliminaSondaggioVoce extends SyncOperation {

    private EliminaSondaggioVoceModel mEliminaSondaggioVoceModel;

    public SyncOperationEliminaSondaggioVoce(SyncData syncData) {
        super(syncData);
    }

    @Override
    public void parseModel() {
        mEliminaSondaggioVoceModel = new Gson().fromJson(mSyncDataContent, EliminaSondaggioVoceModel.class);
    }

    @Override
    public void doJob(Realm realm) throws Exception {
        if (mEliminaSondaggioVoceModel == null)
            throw new Exception("Model is null");

        EventDao eventDao = new EventDao(realm);
        PollDao sondaggioDao = new PollDao(realm);
        MessageDao messageDao = new MessageDao(realm);

        Event evento = eventDao.getEvent(mEliminaSondaggioVoceModel.activityUuid);
        if (evento == null) {
            throw new SyncEventoMismatchException(mEliminaSondaggioVoceModel.activityUuid);
        }

        if (evento.getVersion() < mEliminaSondaggioVoceModel.activityVersion - 1) {
            throw new SyncEventoMismatchException(mEliminaSondaggioVoceModel.activityUuid);
        }

        Poll sondaggio = sondaggioDao.getPoll(mEliminaSondaggioVoceModel.pollUuid);
        if (sondaggio == null) {
            throw new SyncEventoMismatchException(mEliminaSondaggioVoceModel.activityUuid);
        }

        evento.setVersion(mEliminaSondaggioVoceModel.activityVersion);
        uuid = evento.getUuid();

        PollItem sondaggioVoce = sondaggioDao.getPollItem(mEliminaSondaggioVoceModel.pollItemUuid);
        if (sondaggioVoce != null) {
            letto = (App.isEventVisible(mEliminaSondaggioVoceModel.activityUuid) && App.isEventPageVisible(Constants.EVENT_PAGE_CHAT))
                    || mEliminaSondaggioVoceModel.makerUuid.equals(AccountProvider.getInstance().getAccountId());

            messageDao.createSystemMessagePollItemDeleted(evento, sondaggioVoce, letto).setCreatedAt(mEliminaSondaggioVoceModel.timestamp);
            sondaggioDao.deletePollItem(sondaggioVoce.getUuid());
        }
    }

    @Override
    public boolean createNotifications(NotificationsHelper notifications) {
        // Invio notifiche
//        if (!letto) {
//            notifications.setInfo(Constants.TYPE_EVENTO, uuid);
//        }
//        return !letto;
        return false;
    }

    private static class EliminaSondaggioVoceModel {
        String activityUuid;
        int activityVersion;
        long timestamp;
        String makerUuid;
        String pollUuid;
        String pollItemUuid;
    }
}
