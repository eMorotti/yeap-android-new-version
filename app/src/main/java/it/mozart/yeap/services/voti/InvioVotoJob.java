package it.mozart.yeap.services.voti;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.CancelReason;
import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;

import javax.inject.Inject;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.api.services.EventApi;
import it.mozart.yeap.api.services.PollApi;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.Poll;
import it.mozart.yeap.realm.PollItem;
import it.mozart.yeap.realm.Vote;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.PollDao;
import it.mozart.yeap.realm.dao.VoteDao;
import it.mozart.yeap.services.Priority;
import it.mozart.yeap.utility.Constants;
import retrofit2.HttpException;
import rx.functions.Func1;

class InvioVotoJob extends Job {

    @SuppressWarnings("WeakerAccess")
    @Inject
    PollApi pollApi;
    @SuppressWarnings("WeakerAccess")
    @Inject
    EventApi eventApi;

    private String mId;

    InvioVotoJob(String id, String parentId) {
        super(new Params(Priority.HIGH).requireNetwork().groupBy("InvioVoto" + parentId));
        mId = id;
    }

    @Override
    public void onAdded() {
        App.feather().injectFields(this);
    }

    @Override
    public void onRun() throws Throwable {
        final Realm realm = App.getRealm();
        final VoteDao votoDao = new VoteDao(realm);
        try {
            final Vote voto = votoDao.getVote(mId);
            if (voto != null && voto.isLoading()) {
                // Invio messaggio tramite api
                Object obj = null;
                if (voto.getPollItemUuid() != null) {
                    PollDao pollDao = new PollDao(realm);
                    final PollItem sondaggioVoce = pollDao.getPollItem(voto.getPollItemUuid());
                    Poll sondaggio = pollDao.getPoll(sondaggioVoce.getPollUuid());
                    obj = pollApi.votePollItem(sondaggio.getEventUuid(), sondaggioVoce.getPollUuid(), voto.getPollItemUuid(), voto.getType())
                            .onErrorReturn(new Func1<Throwable, Object>() {
                                @Override
                                public Object call(Throwable throwable) {
                                    return throwable;
                                }
                            })
                            .toBlocking()
                            .single();

                    if (obj != null && !(obj instanceof Exception) && (voto.getType() == Constants.VOTO_NO || voto.getType() == Constants.VOTO_NO_INTERESSE)) {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(@NonNull Realm realm) {
                                sondaggioVoce.setMyVote(null);
                                voto.deleteFromRealm();
                            }
                        });
                    }
                } else if (voto.getEventUuid() != null) {
                    EventDao eventDao = new EventDao(realm);
                    final Event evento = eventDao.getEvent(voto.getEventUuid());
                    obj = eventApi.voteEvent(evento.getUuid(), voto.getType())
                            .onErrorReturn(new Func1<Throwable, Object>() {
                                @Override
                                public Object call(Throwable throwable) {
                                    return throwable;
                                }
                            })
                            .toBlocking()
                            .single();
                }
                if (obj != null && obj instanceof Exception) {
                    throw ((Exception) obj);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (e instanceof HttpException) {
                switch (((HttpException) e).code()) {
                    case 403:
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(@NonNull Realm realm) {
                                Vote voto = votoDao.getVote(mId);
                                if (voto != null) {
                                    if (voto.getType() == Constants.VOTO_SI) {
                                        voto.setType(Constants.VOTO_NO);
                                    } else if (voto.getType() == Constants.VOTO_NO) {
                                        voto.setType(Constants.VOTO_SI);
                                    } else if (voto.getType() == Constants.VOTO_INTERESSE) {
                                        voto.setType(Constants.VOTO_NO_INTERESSE);
                                    } else {
                                        voto.setType(Constants.VOTO_INTERESSE);
                                    }
                                    voto.setLoading(false);
                                }
                            }
                        });
                        break;
                    case 404:
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(@NonNull Realm realm) {
                                Vote voto = votoDao.getVote(mId);
                                voto.deleteFromRealm();
                            }
                        });
                        break;
                    default:
                        throw e;
                }
            } else {
                throw e;
            }
        } finally {
            realm.close();
        }
    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {
        if (cancelReason == CancelReason.REACHED_RETRY_LIMIT) {
            final Realm realm = App.getRealm();
            //noinspection TryFinallyCanBeTryWithResources
            try {
                final Vote voto = new VoteDao(realm).getVote(mId);
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(@NonNull Realm realm) {
                        voto.deleteFromRealm();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                realm.close();
            }
        }
    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        return RetryConstraint.createExponentialBackoff(runCount, 1000);
    }
}
