package it.mozart.yeap.services.sync.operations;

import com.google.gson.Gson;

import io.realm.Realm;
import it.mozart.yeap.realm.Group;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.dao.GroupDao;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.services.sync.exceptions.SyncGruppoMismatchException;

public class SyncOperationRimossoInvitatoGruppo extends SyncOperation {

    private RimossoInvitatoGruppoModel mRimossoInvitatoModel;

    public SyncOperationRimossoInvitatoGruppo(SyncData syncData) {
        super(syncData);
    }

    @Override
    public void parseModel() {
        mRimossoInvitatoModel = new Gson().fromJson(mSyncDataContent, RimossoInvitatoGruppoModel.class);
    }

    @Override
    public void doJob(Realm realm) throws Exception {
        if (mRimossoInvitatoModel == null)
            throw new Exception("Model is null");

        GroupDao groupDao = new GroupDao(realm);
        UserDao utenteDao = new UserDao(realm);

        Group gruppo = groupDao.getGroup(mRimossoInvitatoModel.groupUuid);
        if (gruppo == null) {
            throw new SyncGruppoMismatchException(mRimossoInvitatoModel.groupUuid);
        }

        if (gruppo.getVersion() < mRimossoInvitatoModel.groupVersion - 1 || mRimossoInvitatoModel.phone == null) {
            throw new SyncGruppoMismatchException(mRimossoInvitatoModel.groupUuid);
        }

        User utente = utenteDao.getUserLocal(mRimossoInvitatoModel.phone);
        if (gruppo.getGuestsNotYeap().contains(utente)) {
            gruppo.getGuestsNotYeap().remove(utente);
        } else {
            return;
        }

        gruppo.setVersion(mRimossoInvitatoModel.groupVersion);
        uuid = gruppo.getUuid();

        // Recupero utenti
        User maker = utenteDao.getUser(mRimossoInvitatoModel.makerUuid);
        if (maker == null) {
            throw new SyncGruppoMismatchException(mRimossoInvitatoModel.groupUuid);
        }

        // Creo messaggio
        new MessageDao(realm).createSystemMessageGroupInvitedRemoved(gruppo, maker, utente, true).setCreatedAt(mRimossoInvitatoModel.timestamp);
    }

    @Override
    public boolean createNotifications(NotificationsHelper notifications) {
        return false;
    }

    private static class RimossoInvitatoGruppoModel {
        String groupUuid;
        int groupVersion;
        String makerUuid;
        long timestamp;
        String phone;
    }
}
