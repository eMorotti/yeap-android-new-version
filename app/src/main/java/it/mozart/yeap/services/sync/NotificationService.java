package it.mozart.yeap.services.sync;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.RemoteInput;

import java.util.Date;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.common.MessaggioInfo;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.services.messaggi.MessaggiService;
import it.mozart.yeap.utility.Constants;

public class NotificationService extends Service {

    public static Intent getReplyMessageIntent(Context context, String id, boolean isEvento, int notificationId) {
        Intent intent = new Intent(context, NotificationService.class);
        intent.setAction(Constants.REPLY_ACTION);
        intent.putExtra(Constants.REPLY_ID, id);
        intent.putExtra(Constants.REPLY_IS_EVENTO, isEvento);
        intent.putExtra(Constants.REPLY_NOTIFICATION_ID, notificationId);
        return intent;
    }

    public NotificationService() {
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            return START_STICKY;
        }

        if (Constants.REPLY_ACTION.equals(intent.getAction())) {
            String message = getReplyMessage(intent);
            if (message == null) {
                return START_STICKY;
            }

            String id = intent.getStringExtra(Constants.REPLY_ID);
            boolean isEvento = intent.getBooleanExtra(Constants.REPLY_IS_EVENTO, true);
            int notificationId = intent.getIntExtra(Constants.REPLY_NOTIFICATION_ID, 0);

            Realm realm = App.getRealm();
            //noinspection TryFinallyCanBeTryWithResources
            try {
                MessageDao messageDao = new MessageDao(realm);
                if (isEvento) {
                    messageDao.markChatAsReadAsync(true, id);
                    MessaggioInfo messaggioInfo = new MessaggioInfo(id,
                            Constants.MESSAGGIO_TYPOLOGY_CHAT_EVENTO,
                            AccountProvider.getInstance().getAccountId(),
                            message,
                            null,
                            new Date().getTime(),
                            true,
                            false,
                            true);
                    MessaggiService.createMessage(App.getContext(), messaggioInfo);
                } else {
                    messageDao.markChatAsReadAsync(false, id);
                    MessaggioInfo messaggioInfo = new MessaggioInfo(id,
                            Constants.MESSAGGIO_TYPOLOGY_CHAT_GRUPPO,
                            AccountProvider.getInstance().getAccountId(),
                            message,
                            null,
                            new Date().getTime(),
                            true,
                            false,
                            true);
                    MessaggiService.createMessage(App.getContext(), messaggioInfo);
                }
            } finally {
                NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
                notificationManager.cancel(notificationId); //TODO non va
                realm.close();
            }
        }

        return START_STICKY;
    }

    private String getReplyMessage(Intent intent) {
        Bundle remoteInput = RemoteInput.getResultsFromIntent(intent);
        if (remoteInput != null) {
            return remoteInput.getString(Constants.REPLY_KEY);
        }
        return null;
    }
}