package it.mozart.yeap.services.sync.operations;

import com.google.gson.Gson;

import java.util.Date;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.common.MediaInfo;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Group;
import it.mozart.yeap.realm.Message;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.dao.GroupDao;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.services.sync.exceptions.SyncGruppoMismatchException;
import it.mozart.yeap.services.sync.models.SyncMessaggioData;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.Utils;

public class SyncOperationMessaggioGruppo extends SyncOperation {

    private MessaggioGruppoModel mMessaggioGruppoModel;

    public SyncOperationMessaggioGruppo(SyncData syncData) {
        super(syncData);
    }

    @Override
    public void parseModel() {
        mMessaggioGruppoModel = new Gson().fromJson(mSyncDataContent, MessaggioGruppoModel.class);
    }

    @Override
    public void doJob(Realm realm) throws Exception {
        if (mMessaggioGruppoModel == null)
            throw new Exception("Model is null");

        GroupDao gruppoDao = new GroupDao(realm);
        MessageDao messaggioDao = new MessageDao(realm);
        UserDao utenteDao = new UserDao(realm);

        // Controllo se il gruppo è presente
        final Group gruppo = gruppoDao.getGroup(mMessaggioGruppoModel.groupUuid);
        if (gruppo == null) {
            throw new SyncGruppoMismatchException(mMessaggioGruppoModel.groupUuid);
        }

        if (mMessaggioGruppoModel.msg == null || mMessaggioGruppoModel.msg.getUserUuid() == null) {
            return;
        }

        uuid = gruppo.getUuid();

        // Controllo se il messaggio è già presente
        Message messaggio = messaggioDao.getMessage(mMessaggioGruppoModel.msg.getUuid());
        if (messaggio != null) {
            messaggio.setThumbData(mMessaggioGruppoModel.msg.getThumbData());
            messaggio.setMediaWidth(mMessaggioGruppoModel.msg.getImageWidth());
            messaggio.setMediaHeight(mMessaggioGruppoModel.msg.getImageHeight());
            return;
        }

        // Media presente
        MediaInfo mediaInfo = null;
        if (mMessaggioGruppoModel.msg.getMediaUrl() != null) {
            mediaInfo = new MediaInfo(mMessaggioGruppoModel.msg.getMediaUrl(), 0, mMessaggioGruppoModel.msg.getThumbData(), true);
            mediaInfo.setWidth(mMessaggioGruppoModel.msg.getImageWidth());
            mediaInfo.setHeight(mMessaggioGruppoModel.msg.getImageHeight());
        }

        String url = Utils.findUrlInText(mMessaggioGruppoModel.msg.getText());

        User utente = utenteDao.getUser(mMessaggioGruppoModel.msg.getUserUuid());
        if (utente != null) {
            // Creo il messaggio
            messaggio = messaggioDao.createMessage(
                    mMessaggioGruppoModel.msg.getUuid(),
                    mMessaggioGruppoModel.msg.getText(),
                    utente,
                    mediaInfo,
                    Constants.MESSAGGIO_TYPOLOGY_CHAT_GRUPPO,
                    gruppo,
                    (App.isGroupVisible(mMessaggioGruppoModel.groupUuid) && App.isGroupPageVisible(Constants.GROUP_PAGE_CHAT))
                            || mMessaggioGruppoModel.msg.getUserUuid().equals(AccountProvider.getInstance().getAccountId()),
                    true);
            if (url != null) {
                messaggio.setUrl(url);
            } else {
                messaggio.setUrlChecked(true);
            }
            messaggio.setCreatedAt(new Date().getTime());
            messaggio.setCreatedAtForVisualization(mMessaggioGruppoModel.msg.getCreatedAt());

            // Invio notifiche
            if (App.isGroupVisible(mMessaggioGruppoModel.groupUuid) && App.isGroupPageVisible(Constants.GROUP_PAGE_CHAT)) {
                messaggioDao.markChatAsReadAsync(false, gruppo.getUuid());
            }
        }
    }

    @Override
    public boolean createNotifications(NotificationsHelper notifications) {
        if ((!App.isGroupVisible(mMessaggioGruppoModel.groupUuid) || !App.isGroupPageVisible(Constants.GROUP_PAGE_CHAT))
                && !mMessaggioGruppoModel.msg.getUserUuid().equals(AccountProvider.getInstance().getAccountId())) {
            notifications.setInfo(Constants.TYPE_GRUPPO, uuid);
        }
        return (!App.isGroupVisible(mMessaggioGruppoModel.groupUuid) || !App.isGroupPageVisible(Constants.GROUP_PAGE_CHAT))
                && !mMessaggioGruppoModel.msg.getUserUuid().equals(AccountProvider.getInstance().getAccountId());
    }

    private static class MessaggioGruppoModel {
        SyncMessaggioData msg;
        String groupUuid;
    }
}
