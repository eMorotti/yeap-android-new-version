package it.mozart.yeap.services.realmJob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;

import org.greenrobot.eventbus.EventBus;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.realm.dao.GroupDao;
import it.mozart.yeap.services.Priority;
import it.mozart.yeap.ui.groups.main.events.GroupDeleteEvent;
import it.mozart.yeap.utility.Constants;

public class GroupDeleteJob extends Job {

    private String mGroupId;

    public GroupDeleteJob(String gruppoId) {
        super(new Params(Priority.HIGH + 1000).groupBy(Constants.DEFAULT_JOB_QUEUE));
        mGroupId = gruppoId;
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {
        Realm realm = App.getRealm();
        //noinspection TryFinallyCanBeTryWithResources
        try {
            final GroupDao groupDao = new GroupDao(realm);
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(@NonNull Realm realm) {
                    groupDao.deleteGroup(mGroupId);
                }
            });

            EventBus.getDefault().post(new GroupDeleteEvent(true));
        } catch (Exception ex) {
            ex.printStackTrace();
            EventBus.getDefault().post(new GroupDeleteEvent(false));
        } finally {
            realm.close();
        }
    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {

    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        return null;
    }
}
