package it.mozart.yeap.services.syncAdapter.contacts;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import it.mozart.yeap.App;
import it.mozart.yeap.api.services.ContactApi;
import it.mozart.yeap.services.Priority;
import it.mozart.yeap.services.syncAdapter.events.ContactsSyncDoneEvent;
import it.mozart.yeap.services.syncAdapter.events.ContactsSyncFailedEvent;

public class ContactSyncJob extends Job {

    @Inject
    ContactsManager contactsManager;
    @Inject
    ContactApi contactApi;

    private boolean mIgnoreEvent;

    public ContactSyncJob(boolean ignoreEvent) {
        super(new Params(ignoreEvent ? Priority.LOW : Priority.HIGH).requireNetwork());
        mIgnoreEvent = ignoreEvent;
    }

    @Override
    public void onAdded() {
        App.feather().injectFields(this);
    }

    @Override
    public void onRun() throws Throwable {
        contactsManager.syncContacts(getApplicationContext(), contactApi);

        if (!mIgnoreEvent) {
            EventBus.getDefault().post(new ContactsSyncDoneEvent());
        }
    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {
        if (!mIgnoreEvent) {
            EventBus.getDefault().post(new ContactsSyncFailedEvent());
        }
    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        return RetryConstraint.CANCEL;
    }
}
