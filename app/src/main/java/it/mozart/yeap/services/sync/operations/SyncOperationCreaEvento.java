package it.mozart.yeap.services.sync.operations;

import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import it.mozart.yeap.App;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.EventInfo;
import it.mozart.yeap.realm.Poll;
import it.mozart.yeap.realm.PollItem;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.PollDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.realm.dao.VoteDao;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.services.sync.SyncTotal;
import it.mozart.yeap.ui.events.main.events.ShowPopupInvitedEvent;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.Prefs;

public class SyncOperationCreaEvento extends SyncOperation {

    private CreaEventoModel mCreaEventoModel;

    public SyncOperationCreaEvento(SyncData syncData) {
        super(syncData);
    }

    @Override
    public void parseModel() {
        mCreaEventoModel = new Gson().fromJson(mSyncDataContent, CreaEventoModel.class);
    }

    @Override
    public void doJob(Realm realm) throws Exception {
        if (mCreaEventoModel == null)
            throw new Exception("Model is null");

        UserDao utenteDao = new UserDao(realm);
        EventDao eventoDao = new EventDao(realm);
        VoteDao voteDao = new VoteDao(realm);
        PollDao sondaggioDao = new PollDao(realm);

        Event evento = eventoDao.getEvent(mCreaEventoModel.uuid);
        if (evento == null) {
            // Creo attivita
            evento = mCreaEventoModel.copyInEvent(utenteDao);
            evento.setEventInfo(realm.copyToRealm(mCreaEventoModel.event));

            if (evento.getGuestsNotYeap() != null && evento.getGuestsNotYeap().size() != 0) {
                String values = "";
                for (User user : evento.getGuestsNotYeap()) {
                    if (values.isEmpty()) {
                        values = user.getPhone();
                    } else {
                        values += String.format(";%s", user.getPhone());
                    }
                }
                Prefs.putString(Constants.SHOW_INVITE_POPUP_EVENTO + mCreaEventoModel.uuid, values);
                EventBus.getDefault().postSticky(new ShowPopupInvitedEvent());
            }

            evento = eventoDao.createEvent(evento, utenteDao.getUser(mCreaEventoModel.creatorUuid), false);
            Event tempEvent = new Event();
            tempEvent.setUuid(evento.getUuid());
            tempEvent.setEventInfo(new EventInfo());
            tempEvent.getEventInfo().setVotes(mCreaEventoModel.event.getVotes());

            SyncTotal.syncEventoVoti(utenteDao, voteDao, evento, tempEvent);

            // Creo sondaggi
            for (Poll sondaggioInfo : mCreaEventoModel.polls) {
                Poll sondaggio = sondaggioDao.getPoll(sondaggioInfo.getUuid());
                boolean notPresent = false;
                if (sondaggio == null) {
                    notPresent = true;
                    sondaggio = new Poll();
                    sondaggio.setUuid(sondaggioInfo.getUuid());
                }
                SyncTotal.syncSondaggio(utenteDao, sondaggio, sondaggioInfo, evento);

                if (notPresent) {
                    if (sondaggioInfo.getType() == Constants.POLL_TYPE_WHERE || sondaggioInfo.getType() == Constants.POLL_TYPE_WHEN) {
                        sondaggio = sondaggioDao.createPoll(sondaggio, true, false, true);
                    } else {
                        sondaggio = sondaggioDao.createPoll(sondaggio, false, false, true);
                    }
                }

                for (PollItem sondaggioVoceInfo : sondaggioInfo.getPollItems()) {
                    PollItem sondaggioVoce = sondaggioDao.getPollItem(sondaggioVoceInfo.getUuid());
                    boolean notPresentBis = false;
                    if (sondaggioVoce == null) {
                        notPresentBis = true;
                        sondaggioVoce = new PollItem();
                        sondaggioVoce.setUuid(sondaggioVoceInfo.getUuid());
                    }
                    SyncTotal.syncSondaggioVoce(utenteDao, sondaggioVoce, sondaggioVoceInfo, sondaggio);

                    if (notPresentBis) {
                        if (sondaggioInfo.getType() == Constants.POLL_TYPE_WHERE || sondaggioInfo.getType() == Constants.POLL_TYPE_WHEN) {
                            sondaggioVoce = sondaggioDao.createPollItem(sondaggioVoce, true, false, true);
                        } else {
                            sondaggioVoce = sondaggioDao.createPollItem(sondaggioVoce, false, false, true);
                        }
                    } else {
                        sondaggio.getVoci().add(sondaggioVoce);
                    }

                    SyncTotal.syncSondaggioVoceVoti(utenteDao, voteDao, sondaggioVoce, sondaggioVoceInfo);
                }

                if (sondaggio.getCreatedAt() > evento.getUpdatedAt()) {
                    evento.setUpdatedAt(sondaggio.getCreatedAt());
                }
            }

            evento.setWherePollEnabled(mCreaEventoModel.wherePollEnabled);
            evento.setWherePollUuid(mCreaEventoModel.wherePollUuid);
            evento.setWhenPollEnabled(mCreaEventoModel.whenPollEnabled);
            evento.setWhenPollUuid(mCreaEventoModel.whenPollUuid);
        }
    }

    @Override
    public boolean createNotifications(NotificationsHelper notifications) {
        // Invio notifiche
        if (mCreaEventoModel.uuid != null && !AccountProvider.getInstance().getAccountId().equals(mCreaEventoModel.creatorUuid)) {
            notifications.setInfo(Constants.TYPE_EVENTO, mCreaEventoModel.uuid);
        }
        return mCreaEventoModel.uuid != null && !AccountProvider.getInstance().getAccountId().equals(mCreaEventoModel.creatorUuid);
    }

    private class CreaEventoModel {
        String uuid;
        int version;
        int status;
        String desc;
        String avatarThumbUrl;
        String avatarLargeUrl;
        boolean usersCanInvite;
        boolean usersCanAddItems;
        long createdAt;
        String groupUuid;
        String adminUuid;
        String creatorUuid;
        String makerUuid;
        EventInfo event;
        List<String> partecipants;
        List<String> invited;
        List<User> users;
        List<Poll> polls;
        boolean wherePollEnabled;
        boolean whenPollEnabled;
        String wherePollUuid;
        String whenPollUuid;

        Event copyInEvent(UserDao utenteDao) {
            App.setSyncing(true);
            App.jobManager().stop();
            RealmList<User> partecipanti = SyncTotal.manageGuests(utenteDao, users, partecipants);
            RealmList<User> invitati = SyncTotal.manageInvited(utenteDao, invited);
            App.setSyncing(false);
            App.jobManager().start();

            Event evento = new Event();
            evento.setUuid(uuid);
            evento.setVersion(version);
            evento.setCreatedAt(createdAt);
            evento.setStatus(status);
            evento.setDesc(desc);
            evento.setAvatarThumbUrl(avatarThumbUrl);
            evento.setAvatarLargeUrl(avatarLargeUrl);
            evento.setCreator(utenteDao.getUser(creatorUuid));
            evento.setAdmin(utenteDao.getUser(adminUuid));
            evento.setGroupUuid(groupUuid);
            evento.setUsersCanInvite(usersCanInvite);
            evento.setUsersCanAddItems(usersCanAddItems);
            evento.setGuests(partecipanti);
            evento.setGuestsNotYeap(invitati);
            evento.setMyselfPresent(partecipants.contains(AccountProvider.getInstance().getAccountId()));

            return evento;
        }
    }
}
