package it.mozart.yeap.services.realmJob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.services.Priority;

public class MarkPollAsReadJob extends Job {

    private String mId;

    public MarkPollAsReadJob(String id) {
        super(new Params(Priority.HIGH));
        mId = id;
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {
        Realm realm = App.getRealm();
        //noinspection TryFinallyCanBeTryWithResources
        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(@NonNull Realm realm) {
                    new MessageDao(realm).markPollAsRead(mId);
                }
            });
        } finally {
            realm.close();
        }
    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {

    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        return null;
    }
}
