package it.mozart.yeap.services.sync.operations;

import com.google.gson.Gson;

import java.util.Date;
import java.util.UUID;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.EventInfo;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.Vote;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.realm.dao.VoteDao;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.services.sync.exceptions.SyncEventoMismatchException;
import it.mozart.yeap.utility.Constants;

public class SyncOperationVotoEvento extends SyncOperation {

    private VotoEventoModel mVotoEventoModel;

    public SyncOperationVotoEvento(SyncData syncData) {
        super(syncData);
    }

    @Override
    public void parseModel() {
        mVotoEventoModel = new Gson().fromJson(mSyncDataContent, VotoEventoModel.class);
    }

    @Override
    public void doJob(Realm realm) throws Exception {
        if (mVotoEventoModel == null)
            throw new Exception("Model is null");

        EventDao eventDao = new EventDao(realm);
        UserDao userDao = new UserDao(realm);
        VoteDao voteDao = new VoteDao(realm);

        Event evento = eventDao.getEvent(mVotoEventoModel.activityUuid);
        if (evento == null || evento.getEventInfo() == null) {
            throw new SyncEventoMismatchException(mVotoEventoModel.activityUuid);
        }

        if (evento.getVersion() < mVotoEventoModel.activityVersion - 1 || mVotoEventoModel.vote == null) {
            throw new SyncEventoMismatchException(mVotoEventoModel.activityUuid);
        }

        // Modifico o creo voto
        evento.setVersion(mVotoEventoModel.activityVersion);
        if (Constants.MESSAGGI_SHOW_VOTO_EVENTO) {
            evento.setUpdatedAt(new Date().getTime());
        }
        uuid = evento.getUuid();

        if (mVotoEventoModel.vote.getCreatorUuid() != null) {
            User utente = userDao.getUser(mVotoEventoModel.vote.getCreatorUuid());
            if (utente == null) {
                throw new SyncEventoMismatchException(mVotoEventoModel.activityUuid);
            }

            EventInfo eventoInfo = evento.getEventInfo();

            boolean isInterest = mVotoEventoModel.vote.getType() == Constants.VOTO_INTERESSE || mVotoEventoModel.vote.getType() == Constants.VOTO_NO_INTERESSE;

            Vote voto = isInterest ?
                    voteDao.getVoteInterestEventFromUser(evento.getUuid(), mVotoEventoModel.vote.getCreatorUuid())
                    :
                    voteDao.getVoteEventFromUser(evento.getUuid(), mVotoEventoModel.vote.getCreatorUuid());
            if (voto == null) {
                voto = realm.createObject(Vote.class, UUID.randomUUID().toString());
                voto.setCreator(utente);
                voto.setEventUuid(evento.getUuid());
            }
            voto.setCreatedAt(mVotoEventoModel.vote.getCreatedAt());
            voto.setLoading(false);
            voto.setType(mVotoEventoModel.vote.getType());
            if (mVotoEventoModel.vote.getType() == Constants.VOTO_SI || mVotoEventoModel.vote.getType() == Constants.VOTO_NO) {
                voto.setVoteType(Constants.VOTE_TYPE_EVENT);
            } else {
                voto.setVoteType(Constants.VOTE_TYPE_INTEREST);
            }

            if (!isInterest && AccountProvider.getInstance().getAccountId().equals(mVotoEventoModel.vote.getCreatorUuid())) {
                eventoInfo.setMyVote(voto);
            }

            evento.setNumVotesYes((int) voteDao.getVoteEventYesCount(evento.getUuid()));
            evento.setNumVotesNo((int) voteDao.getVoteEventNoCount(evento.getUuid()));

            evento.setNumInterestYes((int) voteDao.getInterestEventYesCount(evento.getUuid()));
            evento.setNumInterestNo((int) voteDao.getInterestEventNoCount(evento.getUuid()));

            letto = (App.isEventVisible(mVotoEventoModel.activityUuid) && App.isEventPageVisible(Constants.EVENT_PAGE_CHAT))
                    || voto.getCreatorUuid().equals(AccountProvider.getInstance().getAccountId());

            if(isInterest) {
                if(voto.getType() == Constants.VOTO_INTERESSE) {
                    new MessageDao(realm).createSystemMessageEventVoteInterest(evento, voto, utente, letto);
                } else {
                    new MessageDao(realm).createSystemMessageEventVoteNoInterest(evento, voto, utente, letto);
                }
            } else{
                new MessageDao(realm).createSystemMessageEventVote(evento, voto, utente, letto);
            }
//            boolean letto = App.isAttivitaChatVisible(mVotoEventoModel.activityUuid) || mVotoEventoModel.vote.getUserUuid().equals(myself.getUuid());
            // Aggiorno contatore voti
//            PropostaMethods.updateVotiPropostaConfermata(realm, propostaConfermata);
//            // Creo messaggio di sistema
//            MessaggioMethods.creaMessaggioVoto(realm, Constants.MESSAGGIO_TYPOLOGY_PROPOSTA, proposta.getUuid(), utente,
//                    Constants.MESSAGGIO_TYPE_SYSTEM, voto.getType(), letto);
        }
    }

    @Override
    public boolean createNotifications(NotificationsHelper notifications) {
        // Invio notifiche
        if (!letto && Constants.MESSAGGI_SHOW_VOTO_EVENTO) {
            notifications.setInfo(Constants.TYPE_EVENTO, uuid);
        }
        return !letto && Constants.MESSAGGI_SHOW_VOTO_EVENTO;
    }

    private static class VotoEventoModel {
        String activityUuid;
        int activityVersion;
        Vote vote;
    }
}
