package it.mozart.yeap.services.sync.operations;

import com.google.gson.Gson;

import io.realm.Realm;
import io.realm.RealmResults;
import it.mozart.yeap.App;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.EventInfo;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.Vote;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.realm.dao.VoteDao;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.services.sync.SyncTotal;
import it.mozart.yeap.services.sync.exceptions.SyncEventoMismatchException;
import it.mozart.yeap.utility.Constants;

public class SyncOperationCambioStatoEvento extends SyncOperation {

    private CambioStatoAttivitaModel mCambioStatoAttivitaModel;

    public SyncOperationCambioStatoEvento(SyncData syncData) {
        super(syncData);
    }

    @Override
    public void parseModel() {
        mCambioStatoAttivitaModel = new Gson().fromJson(mSyncDataContent, CambioStatoAttivitaModel.class);
    }

    @Override
    public void doJob(Realm realm) throws Exception {
        if (mCambioStatoAttivitaModel == null)
            throw new Exception("Model is null");

        UserDao utenteDao = new UserDao(realm);
        VoteDao voteDao = new VoteDao(realm);

        Event evento = new EventDao(realm).getEvent(mCambioStatoAttivitaModel.activityUuid);
        if (evento == null) {
            throw new SyncEventoMismatchException(mCambioStatoAttivitaModel.activityUuid);
        }

        if (evento.getVersion() < mCambioStatoAttivitaModel.activityVersion - 1 || mCambioStatoAttivitaModel.makerUuid == null) {
            throw new SyncEventoMismatchException(mCambioStatoAttivitaModel.activityUuid);
        }

        User maker = utenteDao.getUser(mCambioStatoAttivitaModel.makerUuid);
        if (maker == null) {
            throw new SyncEventoMismatchException(mCambioStatoAttivitaModel.activityUuid);
        }
        letto = (App.isEventVisible(mCambioStatoAttivitaModel.activityUuid) && App.isEventPageVisible(Constants.EVENT_PAGE_DETAILS))
                || mCambioStatoAttivitaModel.makerUuid.equals(AccountProvider.getInstance().getAccountId());

        // Modifico attivita
        evento.setVersion(mCambioStatoAttivitaModel.activityVersion);
        evento.setStatus(mCambioStatoAttivitaModel.status);
        uuid = evento.getUuid();

        evento.getEventInfo().setWhere(mCambioStatoAttivitaModel.event.getWhere());
        evento.getEventInfo().setPlaceId(mCambioStatoAttivitaModel.event.getPlaceId());
        evento.getEventInfo().setLat(mCambioStatoAttivitaModel.event.getLat());
        evento.getEventInfo().setLng(mCambioStatoAttivitaModel.event.getLng());
        evento.getEventInfo().setFromDate(mCambioStatoAttivitaModel.event.getFromDate());
        evento.getEventInfo().setFromHour(mCambioStatoAttivitaModel.event.getFromHour());
        evento.getEventInfo().setToDate(mCambioStatoAttivitaModel.event.getToDate());
        evento.getEventInfo().setToHour(mCambioStatoAttivitaModel.event.getToHour());

        if (mCambioStatoAttivitaModel.status == Constants.EVENTO_CONFERMATO) {
            evento.setWherePollEnabled(false);
            evento.setWhenPollEnabled(false);
        } else if (mCambioStatoAttivitaModel.status == Constants.EVENTO_ANNULLATO) {
            evento.getEventInfo().setMyVoteInterest(null);
            evento.getEventInfo().setMyVote(null);
            evento.setNumInterestYes(0);
            evento.setNumInterestNo(0);
            evento.setNumVotesYes(0);
            evento.setNumVotesNo(0);

            RealmResults<Vote> voti = voteDao.getListVoteEventYes(mCambioStatoAttivitaModel.activityUuid);
            if (voti != null) {
                voti.deleteAllFromRealm();
            }
            RealmResults<Vote> votiNo = voteDao.getListVoteEventNo(mCambioStatoAttivitaModel.activityUuid);
            if (votiNo != null) {
                votiNo.deleteAllFromRealm();
            }
            RealmResults<Vote> votiInteresse = voteDao.getListVoteInterestEventYes(mCambioStatoAttivitaModel.activityUuid);
            if (votiInteresse != null) {
                votiInteresse.deleteAllFromRealm();
            }
            RealmResults<Vote> votiInteresseNo = voteDao.getListVoteInterestEventNo(mCambioStatoAttivitaModel.activityUuid);
            if (votiInteresseNo != null) {
                votiInteresseNo.deleteAllFromRealm();
            }
        } else {
            Event tempEvent = new Event();
            tempEvent.setUuid(mCambioStatoAttivitaModel.activityUuid);
            tempEvent.setEventInfo(mCambioStatoAttivitaModel.event);
            SyncTotal.syncEventoVoti(utenteDao, voteDao, evento, tempEvent);
        }

        new MessageDao(realm).createSystemMessageEventChangeStatus(evento, maker, letto).setCreatedAt(mCambioStatoAttivitaModel.timestamp);
    }

    @Override
    public boolean createNotifications(NotificationsHelper notifications) {
        // Invio notifiche
        if (!letto) {
            notifications.setInfo(Constants.TYPE_EVENTO, uuid);
        }
        return !letto;
    }

    private static class CambioStatoAttivitaModel {
        String activityUuid;
        int activityVersion;
        long timestamp;
        String makerUuid;
        int status;
        EventInfo event;
    }
}
