package it.mozart.yeap.services.voti;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import io.realm.Realm;
import io.realm.RealmResults;
import it.mozart.yeap.App;
import it.mozart.yeap.realm.Vote;
import it.mozart.yeap.realm.dao.VoteDao;
import it.mozart.yeap.utility.Utils;

public class VotiService extends Service {

    private int mRefCounter = 0;

    private static final String ACTION_CREATE = "create";
    private static final String ACTION_BIND = "bind";
    private static final String ACTION_UNBIND = "unbind";
    private static final String ACTION_INVIO_VOTO = "invioVoto";

    private static final String PARAM_VOTO_ID = "paramVotoID";
    private static final String PARAM_PARENT_ID = "paramParentID";

    private static final int STOP_SERVICE_AFTER_SECONDS = 10;

    // SYSTEM

    public static void initService(Context context) {
        Intent intent = new Intent(context, VotiService.class);
        intent.setAction(ACTION_CREATE);
        if (Utils.isOreo()) {
            context.startForegroundService(intent);
        } else {
            context.startService(intent);
        }
    }

    public static void bindService(Context context) {
        Intent intent = new Intent(context, VotiService.class);
        intent.setAction(ACTION_BIND);
        context.startService(intent);
    }

    public static void unbindService(Context context) {
        Intent intent = new Intent(context, VotiService.class);
        intent.setAction(ACTION_UNBIND);
        context.startService(intent);
    }

    public static void sendVote(Context context, String votoId, String parentId) {
        Intent intent = new Intent(context, VotiService.class);
        intent.setAction(ACTION_INVIO_VOTO);
        intent.putExtra(PARAM_VOTO_ID, votoId);
        intent.putExtra(PARAM_PARENT_ID, parentId);
        context.startService(intent);
    }

    public VotiService() {
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (Utils.isOreo()) {
            startForeground(666, new NotificationCompat.Builder(this, "yeap!low")
                    .setDefaults(0)
                    .setVibrate(new long[]{0L})
                    .setSound(null)
                    .build());
        }
        App.feather().injectFields(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            if (ACTION_CREATE.equals(intent.getAction())) {
                onCreateReceived();
            } else if (ACTION_BIND.equals(intent.getAction())) {
                mRefCounter++;
            } else if (ACTION_UNBIND.equals(intent.getAction())) {
                mRefCounter--;
            } else if (ACTION_INVIO_VOTO.equals(intent.getAction())) {
                send(intent.getStringExtra(PARAM_VOTO_ID), intent.getStringExtra(PARAM_PARENT_ID));
            }
        }

        if (mRefCounter <= 0) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mRefCounter <= 0) {
                        stopSelf();
                    }
                }
            }, STOP_SERVICE_AFTER_SECONDS * 1000);
        }

        return START_STICKY;
    }

    // Rimetto nei job tutti i messaggi non ancora inviati
    private void onCreateReceived() {
        Realm realm = App.getRealm();
        RealmResults<Vote> results = new VoteDao(realm).getVoteNotSent();
        for (Vote voto : results) {
            App.jobManager().addJobInBackground(new InvioVotoJob(voto.getUuid(), "DEFAULT"));
        }
        RealmResults<Vote> resultsInterest = new VoteDao(realm).getVoteInterestNotSent();
        for (Vote voto : resultsInterest) {
            App.jobManager().addJobInBackground(new InvioVotoJob(voto.getUuid(), "DEFAULT"));
        }
        realm.close();
    }

    // Creo il job per l'invio del voto
    private void send(String votoId, String parentId) {
        if (votoId != null) {
            App.jobManager().addJobInBackground(new InvioVotoJob(votoId, parentId));
        }
    }
}