package it.mozart.yeap.services.messaggi;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.RetryStrategy;
import com.firebase.jobdispatcher.Trigger;

import org.parceler.Parcels;

import io.realm.Realm;
import io.realm.RealmResults;
import it.mozart.yeap.App;
import it.mozart.yeap.common.MessaggioInfo;
import it.mozart.yeap.realm.Message;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.utility.Utils;

public class MessaggiService extends Service {

    private int mRefCounter = 0;

    private static final String ACTION_CREATE = "create";
    private static final String ACTION_BIND = "bind";
    private static final String ACTION_UNBIND = "unbind";
    private static final String ACTION_CREA_MESSAGGIO = "creaMessaggio";

    private static final String PARAM_MESSAGE_INFO = "paramMessageInfo";

    private static final int STOP_SERVICE_AFTER_SECONDS = 10;

    // SYSTEM

    public static void initService(Context context) {
        Intent intent = new Intent(context, MessaggiService.class);
        intent.setAction(ACTION_CREATE);
        if (Utils.isOreo()) {
            context.startForegroundService(intent);
        } else {
            context.startService(intent);
        }
    }

    public static void createMessage(Context context, MessaggioInfo messaggioInfo) {
        Intent intent = new Intent(context, MessaggiService.class);
        intent.setAction(ACTION_CREA_MESSAGGIO);
        intent.putExtra(PARAM_MESSAGE_INFO, Parcels.wrap(messaggioInfo));
        context.startService(intent);
    }

    public MessaggiService() {
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (Utils.isOreo()) {
            startForeground(666, new NotificationCompat.Builder(this, "yeap!low")
                    .setDefaults(0)
                    .setVibrate(new long[]{0L})
                    .setSound(null)
                    .build());
        }
        App.feather().injectFields(this);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            if (ACTION_CREATE.equals(intent.getAction())) {
                onCreateReceived();
            } else if (ACTION_BIND.equals(intent.getAction())) {
                mRefCounter++;
            } else if (ACTION_UNBIND.equals(intent.getAction())) {
                mRefCounter--;
            } else if (ACTION_CREA_MESSAGGIO.equals(intent.getAction())) {
                create((MessaggioInfo) Parcels.unwrap(intent.getParcelableExtra(PARAM_MESSAGE_INFO)));
            }
        } else {
            onCreateReceived();
        }

        if (mRefCounter <= 0) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mRefCounter <= 0) {
                        stopSelf();
                    }
                }
            }, STOP_SERVICE_AFTER_SECONDS * 1000);
        }

        return START_STICKY;
    }

    // Rimetto nei job tutti i messaggi non ancora inviati
    private void onCreateReceived() {
        Realm realm = App.getRealm();
        //noinspection TryFinallyCanBeTryWithResources
        try {
            RealmResults<Message> results = new MessageDao(realm).getListMessagesNotSent();
            for (Message messaggio : results) {
                Bundle bundle = new Bundle();
                bundle.putString("id", messaggio.getUuid());
                bundle.putString("parentId", "DEFAULT");

                App.jobDispatcher().mustSchedule(App.jobDispatcher().newJobBuilder()
                        .setService(InvioMessaggioJob.class)
                        .setTrigger(Trigger.executionWindow(0, 0))
                        .setTag(messaggio.getUuid())
                        .setLifetime(Lifetime.FOREVER)
                        .setReplaceCurrent(true)
                        .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                        .setConstraints(
                                Constraint.ON_ANY_NETWORK
                        )
                        .setExtras(bundle)
                        .build());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            realm.close();
        }
    }

    // Creo il job per la creazione del messaggio
    private void create(MessaggioInfo messaggioInfo) {
        if (messaggioInfo != null) {
            App.jobManager().addJobInBackground(new CreaMessaggioJob(messaggioInfo));
        }
    }
}