package it.mozart.yeap.services.sync.operations;

import com.google.gson.Gson;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Group;
import it.mozart.yeap.realm.Message;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.dao.GroupDao;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.services.sync.exceptions.SyncGruppoMismatchException;
import it.mozart.yeap.utility.Constants;

public class SyncOperationRimossoPartecipanteGruppo extends SyncOperation {

    private RimossoPartecipanteGruppoModel mRimossoPartecipanteModel;

    public SyncOperationRimossoPartecipanteGruppo(SyncData syncData) {
        super(syncData);
    }

    @Override
    public void parseModel() {
        mRimossoPartecipanteModel = new Gson().fromJson(mSyncDataContent, RimossoPartecipanteGruppoModel.class);
    }

    @Override
    public void doJob(Realm realm) throws Exception {
        if (mRimossoPartecipanteModel == null)
            throw new Exception("Model is null");

        GroupDao gruppoDao = new GroupDao(realm);
        UserDao utenteDao = new UserDao(realm);

        Group gruppo = gruppoDao.getGroup(mRimossoPartecipanteModel.groupUuid);
        if (gruppo == null) {
            throw new SyncGruppoMismatchException(mRimossoPartecipanteModel.groupUuid);
        }

        if (gruppo.getVersion() < mRimossoPartecipanteModel.groupVersion - 1 || mRimossoPartecipanteModel.userUuid == null) {
            throw new SyncGruppoMismatchException(mRimossoPartecipanteModel.groupUuid);
        }

        if (gruppo.getVersion() + 1 == mRimossoPartecipanteModel.groupVersion) {
            User utente = utenteDao.getUser(mRimossoPartecipanteModel.userUuid);
            if (gruppo.getGuests().contains(utente)) {
                gruppo.getGuests().remove(utente);
                if (utente.getUuid().equals(AccountProvider.getInstance().getAccountId())) {
                    gruppo.setMyselfPresent(false);
                }
            } else {
                return;
            }
        }

        gruppo.setVersion(mRimossoPartecipanteModel.groupVersion);
        uuid = gruppo.getUuid();

        // Recupero utenti
        User maker = utenteDao.getUser(mRimossoPartecipanteModel.makerUuid);
        if (maker == null) {
            throw new SyncGruppoMismatchException(mRimossoPartecipanteModel.groupUuid);
        }
        User utente = utenteDao.getUser(mRimossoPartecipanteModel.userUuid);
        letto = (App.isGroupVisible(mRimossoPartecipanteModel.groupUuid) && App.isGroupPageVisible(Constants.GROUP_PAGE_CHAT))
                || mRimossoPartecipanteModel.makerUuid.equals(AccountProvider.getInstance().getAccountId());

        // Creo messaggio
        Message message = new MessageDao(realm).createSystemMessageGroupUserRemoved(gruppo, maker, utente, letto);
        message.setCreatedAt(mRimossoPartecipanteModel.timestamp);
        letto = !letto && !message.isIgnoreRead();
    }

    @Override
    public boolean createNotifications(NotificationsHelper notifications) {
        // Invio notifiche
        if (letto) {
            notifications.setInfo(Constants.TYPE_GRUPPO, uuid);
        }
        return letto;
    }

    private static class RimossoPartecipanteGruppoModel {
        String groupUuid;
        int groupVersion;
        String makerUuid;
        long timestamp;
        String userUuid;
    }
}
