package it.mozart.yeap.services.sync;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;
import io.socket.client.IO;
import io.socket.client.Manager;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.socket.engineio.client.Transport;
import it.mozart.yeap.App;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.realm.dao.SyncDataDao;
import it.mozart.yeap.services.sync.models.SyncErrorVersion;
import it.mozart.yeap.services.sync.models.SyncUnit;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.Prefs;
import it.mozart.yeap.utility.Utils;

public class SyncUnitService extends Service {

    private Handler mHandler = new Handler();
    private Socket mSocket;
    private boolean mDestroyed;
    private boolean mConnecting;
    private boolean mRedoLoginFired;
    private int mRetryWaitMs = INITIAL_RETRY_WAIT_MS;
    private int mRefCounter = 0;

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            if (mRefCounter <= 0) {
                stopSelf();
            }
        }
    };

    //    private static final String CONNECT_URL = "http://192.168.91.122:8075";
    private static final String CONNECT_URL = Utils.getWsURL();

    private static final String ACTION_CREATE = "create";
    private static final String ACTION_BIND = "bind";
    private static final String ACTION_UNBIND = "unbind";
    private static final String ACTION_MESSAGE = "message";

    private static final String PARAM_MESSAGE = "paramMessage";

    private static final int STOP_SERVICE_AFTER_SECONDS = 10000;
    private static final int INITIAL_RETRY_WAIT_MS = 300;
    private static final float RETRY_WAIT_FACTOR = 1.8f;

    // SYSTEM

    public static void initService(Context context) {
        Intent intent = new Intent(context, SyncUnitService.class);
        intent.setAction(ACTION_CREATE);
        if (Utils.isOreo()) {
            context.startForegroundService(intent);
        } else {
            context.startService(intent);
        }
    }

    public static void bindService(Context context) {
        Intent intent = new Intent(context, SyncUnitService.class);
        intent.setAction(ACTION_BIND);
        context.startService(intent);
    }

    public static void unbindService(Context context) {
        Intent intent = new Intent(context, SyncUnitService.class);
        intent.setAction(ACTION_UNBIND);
        context.startService(intent);
    }

    public static void newGcmMessage(Context context) {
        Intent intent = new Intent(context, SyncUnitService.class);
        intent.setAction(ACTION_MESSAGE);
        if (Utils.isOreo()) {
            context.startForegroundService(intent);
        } else {
            context.startService(intent);
        }
    }

    public SyncUnitService() {
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (Utils.isOreo()) {
            startForeground(666, new NotificationCompat.Builder(this, "yeap!low")
                    .setDefaults(0)
                    .setVibrate(new long[]{0L})
                    .setSound(null)
                    .build());
        }
        App.feather().injectFields(this);
        registerConnectivityChange();
        connect();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("SYNC", "DESTROYED");
        unregisterConnectivityChange();
        mDestroyed = true;
        if (mSocket != null) {
            mSocket.off();
            mSocket.close();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            if (ACTION_CREATE.equals(intent.getAction())) {
                onCreateReceived();
            } else if (ACTION_BIND.equals(intent.getAction())) {
                mRefCounter++;
                mRetryWaitMs = INITIAL_RETRY_WAIT_MS;
                connect();
            } else if (ACTION_UNBIND.equals(intent.getAction())) {
                mRefCounter--;
            } else if (ACTION_MESSAGE.equals(intent.getAction())) {
                connect();
            }
        }

        if (mRefCounter <= 0) {
            mHandler.postDelayed(mRunnable, STOP_SERVICE_AFTER_SECONDS);
        } else {
            mHandler.removeCallbacks(mRunnable);
        }

        return START_STICKY;
    }

    private void connect() {
        if (AccountProvider.getInstance().getAccountId() == null) {
            return;
        }

        if (mSocket != null) {
            return;
        }

        if (!Utils.isNetworkAvailable(this)) {
            return;
        }

        if (mConnecting) {
            return;
        }

        if (mRedoLoginFired) {
            return;
        }

        mConnecting = true;

        String packageName;
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            packageName = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            packageName = "999";
        }

        try {
            IO.Options opts = new IO.Options();
            opts.port = 80;
            opts.transports = new String[]{"websocket"};
            mSocket = IO.socket(CONNECT_URL, opts);

            final String finalPackageName = packageName;
            mSocket.io().on(Manager.EVENT_TRANSPORT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Transport transport = (Transport) args[0];

                    transport.on(Transport.EVENT_REQUEST_HEADERS, new Emitter.Listener() {
                        @Override
                        public void call(Object... args) {
                            //noinspection unchecked
                            Map<String, List<String>> headers = (Map<String, List<String>>) args[0];
                            //noinspection ArraysAsListWithZeroOrOneArgument
                            headers.put("x-access-token", Arrays.asList(Prefs.getString(Constants.PREF_ACCESS_TOKEN, "")));
                            //noinspection ArraysAsListWithZeroOrOneArgument
                            headers.put("x-client-version", Arrays.asList("android/" + finalPackageName));
                        }
                    });
                }
            });
            mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.d("SYNC", "CONNECTION OPEN");

                    mRetryWaitMs = INITIAL_RETRY_WAIT_MS;
                    mConnecting = false;

                    mHandler.removeCallbacks(mRunnable);
                    mHandler.postDelayed(mRunnable, STOP_SERVICE_AFTER_SECONDS);
                }
            });
            mSocket.on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.d("SYNC", "CONNECTION CONNECT ERROR " + args[0]);
                }
            });
            mSocket.on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.d("SYNC", "CONNECTION CLOSE");

                    retryConnectDelayed();
                }
            });
            mSocket.on(Socket.EVENT_ERROR, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.d("SYNC", "CONNECTION ERROR " + args[0]);
                    retryConnectDelayed();
                }
            });
            // Mettere vero listener
            mSocket.on(Socket.EVENT_MESSAGE, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    onMessageReceived((JSONObject) args[0]);

                    if (mRefCounter <= 0) {
                        mHandler.removeCallbacks(mRunnable);
                        mHandler.postDelayed(mRunnable, STOP_SERVICE_AFTER_SECONDS);
                    }
                }
            });
            mSocket.on("err", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    onErrReceived((JSONObject) args[0]);
                }
            });
            mSocket.connect();
        } catch (URISyntaxException e) {
            mSocket = null;
            e.printStackTrace();
        }
    }

    private void retryConnectDelayed() {
        mSocket.off();
        mSocket = null;
        mConnecting = false;

        if (mDestroyed)
            return;

        if (mRefCounter <= 0)
            return;

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!mDestroyed) {
                    connect();
                }
            }
        }, mRetryWaitMs);

        mRetryWaitMs *= RETRY_WAIT_FACTOR;
    }

    private void onErrReceived(JSONObject obj) {
        SyncErrorVersion syncErrorVersion;
        try {
            syncErrorVersion = new Gson().fromJson(obj.toString(), SyncErrorVersion.class);
        } catch (Exception ex) {
            ex.printStackTrace();
            syncErrorVersion = null;
        }

        if (syncErrorVersion != null) {
            boolean ignore = false;
            try {
                PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                ignore = Prefs.getString(Constants.UPDATE_APP, "").equals(packageInfo.versionName);
                Prefs.putString(Constants.UPDATE_APP, packageInfo.versionName);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            if (!ignore) {
                Intent intent = new Intent("it.mozart.yeap.UPDATE_APP");
                sendBroadcast(intent);
            }
        }
    }

    private void onMessageReceived(JSONObject obj) {
        if (!Prefs.getBoolean(Constants.LOGIN_COMPLETED, false)) {
            return;
        }

        String message = obj.toString();
        Log.d("SYNC", "MESSAGE " + message);

        SyncUnit syncUnit = parseSyncUnit(message);
        if (syncUnit != null) {
            try {
                elaborateSyncData(syncUnit);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Log.w("SYNC", "MESSAGGIO NON RICONOSCIUTO");
        }
    }

    // Servizio appena creato, rifà partire i job
    private void onCreateReceived() {
        Realm realm = App.getRealm();
        final SyncDataDao syncUnitDao = new SyncDataDao(realm);
        //noinspection TryFinallyCanBeTryWithResources
        try {
            // Elimino tutti quelli già elaborati
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(@NonNull Realm realm) {
                    syncUnitDao.deleteAllSyncDataDone();
                }
            });

            List<String> syncIdToDelete = new ArrayList<>();
            RealmResults<SyncData> results = syncUnitDao.getListSyncDataNotDone();
            for (SyncData unit : results) {
                // Creo i job
                if (!createJob(unit)) {
                    // Aggiungo la syncUnit a quelle da eliminare
                    syncIdToDelete.add(unit.getId());
                }
            }
            // Elimino le syncUnit
            for (String id : syncIdToDelete) {
                final SyncData syncData = syncUnitDao.getSyncData(id);
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(@NonNull Realm realm) {
                        // Rimuovo da realm
                        syncData.deleteFromRealm();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            realm.close();
        }
    }

    // Parso la sync unit
    private SyncUnit parseSyncUnit(String sync) {
        try {
            return new Gson().fromJson(sync, SyncUnit.class);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    // Salvo la sync unit su db e invio l'ack
    private void elaborateSyncData(SyncUnit syncUnit) {
        Realm realm = App.getRealm();
        String id = syncUnit.getId();

        try {
            final SyncData syncData = syncUnit.getBody();
            if (syncData != null && syncData.getData() != null) {
                String data = syncData.getData().toString();
                syncData.setContent(data);
                syncData.setId(id);

                if (id != null && new SyncDataDao(realm).getSyncData(id) == null) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(@NonNull Realm realm) {
                            syncData.setTimestamp(new Date().getTime());
                            realm.copyToRealm(syncData);
                        }
                    });
                }
            } else {
                Log.w("SYNC", "SYNC DATA NULL");
            }

            if (mSocket != null && mSocket.connected() && id != null) {
                // Mando Ack
                mSocket.emit("ack", id);
            }

            if (syncData != null && syncData.getData() != null) {
                // Creo il job
                if (!createJob(syncData)) {
                    // Elimino la syncUnit
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(@NonNull Realm realm) {
                            if (syncData.isManaged())
                                syncData.deleteFromRealm();
                        }
                    });
                }
            }
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
            if (realm.isInTransaction())
                realm.cancelTransaction();
        } finally {
            realm.close();
        }
    }

    // Creo il job corrispondente
    private boolean createJob(SyncData syncData) {
        try {
            App.jobManager().addJobInBackground(SyncJobFactory.create(syncData));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    // NETWORK

    private void registerConnectivityChange() {
        registerReceiver(mConnectivityReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    private void unregisterConnectivityChange() {
        unregisterReceiver(mConnectivityReceiver);
    }

    private BroadcastReceiver mConnectivityReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);

            if (!noConnectivity && mSocket == null && !mConnecting) {
                connect();
            }
        }
    };
}
