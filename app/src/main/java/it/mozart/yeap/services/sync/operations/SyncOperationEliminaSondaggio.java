package it.mozart.yeap.services.sync.operations;

import com.google.gson.Gson;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.Poll;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.realm.dao.PollDao;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.services.sync.exceptions.SyncEventoMismatchException;
import it.mozart.yeap.utility.Constants;

public class SyncOperationEliminaSondaggio extends SyncOperation {

    private EliminaSondaggioModel mEliminaSondaggioModel;

    public SyncOperationEliminaSondaggio(SyncData syncData) {
        super(syncData);
    }

    @Override
    public void parseModel() {
        mEliminaSondaggioModel = new Gson().fromJson(mSyncDataContent, EliminaSondaggioModel.class);
    }

    @Override
    public void doJob(Realm realm) throws Exception {
        if (mEliminaSondaggioModel == null)
            throw new Exception("Model is null");

        EventDao eventDao = new EventDao(realm);
        PollDao sondaggioDao = new PollDao(realm);
        MessageDao messageDao = new MessageDao(realm);

        Event evento = eventDao.getEvent(mEliminaSondaggioModel.activityUuid);
        if (evento == null) {
            throw new SyncEventoMismatchException(mEliminaSondaggioModel.activityUuid);
        }

        if (evento.getVersion() < mEliminaSondaggioModel.activityVersion - 1) {
            throw new SyncEventoMismatchException(mEliminaSondaggioModel.activityUuid);
        }

        evento.setVersion(mEliminaSondaggioModel.activityVersion);
        uuid = evento.getUuid();

        Poll sondaggio = sondaggioDao.getPoll(mEliminaSondaggioModel.pollUuid);
        if (sondaggio != null) {
            letto = (App.isEventVisible(mEliminaSondaggioModel.activityUuid) && App.isEventPageVisible(Constants.EVENT_PAGE_CHAT))
                    || mEliminaSondaggioModel.makerUuid.equals(AccountProvider.getInstance().getAccountId());

            messageDao.createSystemMessagePollDeleted(evento, sondaggio, letto).setCreatedAt(mEliminaSondaggioModel.timestamp);
            sondaggioDao.deletePoll(sondaggio.getUuid());
        }
    }

    @Override
    public boolean createNotifications(NotificationsHelper notifications) {
        // Invio notifiche
//        if (!letto) {
//            notifications.setInfo(Constants.TYPE_EVENTO, uuid);
//        }
//        return !letto;
        return false;
    }

    private static class EliminaSondaggioModel {
        String activityUuid;
        int activityVersion;
        long timestamp;
        String makerUuid;
        String pollUuid;
    }
}
