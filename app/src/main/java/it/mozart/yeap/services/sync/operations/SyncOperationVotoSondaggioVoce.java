package it.mozart.yeap.services.sync.operations;

import com.google.gson.Gson;

import java.util.UUID;

import io.realm.Realm;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.Poll;
import it.mozart.yeap.realm.PollItem;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.Vote;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.PollDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.realm.dao.VoteDao;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.services.sync.exceptions.SyncEventoMismatchException;
import it.mozart.yeap.utility.Constants;

public class SyncOperationVotoSondaggioVoce extends SyncOperation {

    private VotoSondaggioVoceModel mVotoSondaggioVoceModel;

    public SyncOperationVotoSondaggioVoce(SyncData syncData) {
        super(syncData);
    }

    @Override
    public void parseModel() {
        mVotoSondaggioVoceModel = new Gson().fromJson(mSyncDataContent, VotoSondaggioVoceModel.class);
    }

    @Override
    public void doJob(Realm realm) throws Exception {
        if (mVotoSondaggioVoceModel == null)
            throw new Exception("Model is null");

        EventDao eventoDao = new EventDao(realm);
        PollDao pollDao = new PollDao(realm);
        VoteDao votoDao = new VoteDao(realm);
        UserDao userDao = new UserDao(realm);

        Event evento = eventoDao.getEvent(mVotoSondaggioVoceModel.activityUuid);
        if (evento == null) {
            throw new SyncEventoMismatchException(mVotoSondaggioVoceModel.activityUuid);
        }

        Poll sondaggio = pollDao.getPoll(mVotoSondaggioVoceModel.pollUuid);
        if (sondaggio == null) {
            throw new SyncEventoMismatchException(mVotoSondaggioVoceModel.activityUuid);
        }

        PollItem sondaggioVoce = pollDao.getPollItem(mVotoSondaggioVoceModel.pollItemUuid);
        if (sondaggioVoce == null) {
            throw new SyncEventoMismatchException(mVotoSondaggioVoceModel.activityUuid);
        }

        if (evento.getVersion() < mVotoSondaggioVoceModel.activityVersion - 1 || mVotoSondaggioVoceModel.vote == null) {
            throw new SyncEventoMismatchException(mVotoSondaggioVoceModel.activityUuid);
        }

        // Modifico o creo voto
        evento.setVersion(mVotoSondaggioVoceModel.activityVersion);
        if (mVotoSondaggioVoceModel.vote.getCreatorUuid() != null) {
            User utente = userDao.getUser(mVotoSondaggioVoceModel.vote.getCreatorUuid());
            if (utente == null) {
                throw new SyncEventoMismatchException(mVotoSondaggioVoceModel.activityUuid);
            }

            Vote voto = votoDao.getVotePollItemFromUser(mVotoSondaggioVoceModel.pollItemUuid, mVotoSondaggioVoceModel.vote.getCreatorUuid());
            if (voto != null && mVotoSondaggioVoceModel.vote.getType() == Constants.VOTO_NO) {
                voto.deleteFromRealm();
            } else if (voto == null && mVotoSondaggioVoceModel.vote.getType() == Constants.VOTO_SI) {
                voto = realm.createObject(Vote.class, UUID.randomUUID().toString());
                voto.setCreator(utente);
                voto.setPollItemUuid(sondaggioVoce.getUuid());
                voto.setCreatedAt(mVotoSondaggioVoceModel.vote.getCreatedAt());
                voto.setLoading(false);
                voto.setType(Constants.VOTO_SI);
                if (AccountProvider.getInstance().getAccountId().equals(mVotoSondaggioVoceModel.vote.getCreatorUuid())) {
                    sondaggioVoce.setMyVote(voto);
                }
            } else if (voto != null && mVotoSondaggioVoceModel.vote.getType() == Constants.VOTO_SI) {
                voto.setCreatedAt(mVotoSondaggioVoceModel.vote.getCreatedAt());
                voto.setLoading(false);
                voto.setType(Constants.VOTO_SI);
                if (AccountProvider.getInstance().getAccountId().equals(mVotoSondaggioVoceModel.vote.getCreatorUuid())) {
                    sondaggioVoce.setMyVote(voto);
                }
            }

            sondaggioVoce.setNumVoteYes((int) votoDao.getVotePollItemYesCount(sondaggioVoce.getUuid()));
        }
    }

    @Override
    public boolean createNotifications(NotificationsHelper notifications) {
        return false;
    }

    private static class VotoSondaggioVoceModel {
        String activityUuid;
        int activityVersion;
        String pollUuid;
        String pollItemUuid;
        Vote vote;
    }
}
