package it.mozart.yeap.services.sync.operations;

import com.google.gson.Gson;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.Poll;
import it.mozart.yeap.realm.PollItem;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.PollDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.services.sync.SyncTotal;
import it.mozart.yeap.services.sync.exceptions.SyncEventoMismatchException;
import it.mozart.yeap.utility.Constants;

public class SyncOperationCreaSondaggioVoce extends SyncOperation {

    private CreaSondaggioVoceModel mCreaSondaggioVoceModel;

    public SyncOperationCreaSondaggioVoce(SyncData syncData) {
        super(syncData);
    }

    @Override
    public void parseModel() {
        mCreaSondaggioVoceModel = new Gson().fromJson(mSyncDataContent, CreaSondaggioVoceModel.class);
    }

    @Override
    public void doJob(Realm realm) throws Exception {
        if (mCreaSondaggioVoceModel == null)
            throw new Exception("Model is null");

        EventDao eventoDao = new EventDao(realm);
        PollDao sondaggioDao = new PollDao(realm);
        UserDao userDao = new UserDao(realm);

        Event evento = eventoDao.getEvent(mCreaSondaggioVoceModel.activityUuid);
        if (evento == null) {
            throw new SyncEventoMismatchException(mCreaSondaggioVoceModel.activityUuid);
        }

        if (evento.getVersion() < mCreaSondaggioVoceModel.activityVersion - 1 || mCreaSondaggioVoceModel.pollItem == null) {
            throw new SyncEventoMismatchException(mCreaSondaggioVoceModel.activityUuid);
        }

        Poll sondaggio = sondaggioDao.getPoll(mCreaSondaggioVoceModel.pollUuid);
        if (sondaggio == null) {
            throw new SyncEventoMismatchException(mCreaSondaggioVoceModel.activityUuid);
        }

        evento.setVersion(mCreaSondaggioVoceModel.activityVersion);
        uuid = evento.getUuid();

        if (mCreaSondaggioVoceModel.pollItem.isDeleted()) {
            return;
        }

        PollItem sondaggioVoce = sondaggioDao.getPollItem(mCreaSondaggioVoceModel.pollItem.getUuid());
        boolean notPresent = false;
        if (sondaggioVoce == null) {
            notPresent = true;
            sondaggioVoce = new PollItem();
            sondaggioVoce.setUuid(mCreaSondaggioVoceModel.pollItem.getUuid());
        }
        SyncTotal.syncSondaggioVoce(userDao, sondaggioVoce, mCreaSondaggioVoceModel.pollItem, sondaggio);

        letto = (App.isEventVisible(mCreaSondaggioVoceModel.activityUuid) && App.isEventPageVisible(Constants.EVENT_PAGE_POLLS))
                || mCreaSondaggioVoceModel.pollItem.getCreatorUuid().equals(AccountProvider.getInstance().getAccountId());

        if (notPresent) {
            if (sondaggio.getType() == Constants.POLL_TYPE_WHERE || sondaggio.getType() == Constants.POLL_TYPE_WHEN) {
                sondaggioDao.createPollItem(sondaggioVoce, true, letto, false);
            } else {
                sondaggioDao.createPollItem(sondaggioVoce, false, letto, false);
            }
        }

        letto = notPresent & !letto;
    }

    @Override
    public boolean createNotifications(NotificationsHelper notifications) {
        // Invio notifiche
        if (letto) {
            notifications.setInfo(Constants.TYPE_EVENTO, uuid);
        }
        return letto;
    }

    private static class CreaSondaggioVoceModel {
        String activityUuid;
        int activityVersion;
        String pollUuid;
        PollItem pollItem;
    }
}
