package it.mozart.yeap.services.sync;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.CancelReason;
import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.api.services.EventApi;
import it.mozart.yeap.api.services.GroupApi;
import it.mozart.yeap.events.ApiCallResultEvent;
import it.mozart.yeap.helpers.AnalitycsProvider;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.realm.dao.SyncDataDao;
import it.mozart.yeap.services.Priority;
import it.mozart.yeap.services.sync.exceptions.SyncEventoMismatchException;
import it.mozart.yeap.services.sync.exceptions.SyncGruppoMismatchException;
import it.mozart.yeap.services.sync.operations.SyncOperation;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.Prefs;

class SyncJob extends Job {

    @SuppressWarnings("unused")
    @Inject
    private EventApi eventApi;
    @SuppressWarnings("unused")
    @Inject
    private GroupApi groupApi;
    @Inject
    private NotificationsHelper notifications;

    private SyncOperation mSyncOperation;
    private SyncData mUnit;
    private String mUnitId;

    SyncJob(String unitId, SyncOperation syncOperation) {
        super(new Params(Priority.HIGH).groupBy(syncOperation.queueId()));
        mUnitId = unitId;
        mSyncOperation = syncOperation;
    }

    @Override
    public void onAdded() {
        App.feather().injectFields(this);
    }

    @Override
    public void onRun() throws Throwable {
        boolean isLoggedIn = Prefs.getBoolean(Constants.LOGIN_COMPLETED, false);
        if (!isLoggedIn) {
            return;
        }

        Realm realm = App.getRealm();
        try {
            mUnit = new SyncDataDao(realm).getSyncData(mUnitId);

            if(mUnit.getV() == 2) {
                // Se non è in elaborazione lo imposto
                if (!mUnit.isElaborato() && !mUnit.isInElaborazione()) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(@NonNull Realm realm) {
                            mUnit.setInElaborazione(true);
                        }
                    });

                    // Eseguo l'operazione e metto la syncUnit come elaborata
                    realm.beginTransaction();
                    mSyncOperation.doJob(realm);
                    mUnit.setElaborato(true);
                    realm.commitTransaction();

                    if (mUnit.getRequestId() != null) {
                        EventBus.getDefault().post(new ApiCallResultEvent(mUnit.getRequestId()));
                    }
                }
            }

            if (mUnit.isManaged() && mUnit.isElaborato()) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(@NonNull Realm realm) {
                        // Rimuovo la sync unit
                        mUnit.deleteFromRealm();
                    }
                });
            }
        } catch (SyncEventoMismatchException e1) {
            if (realm.isInTransaction())
                realm.cancelTransaction();
            resetInElaborazione(realm);
            AnalitycsProvider.getInstance().logCustom(null, "syncActivity");
            // Sync totale attività
            SyncTotal.syncEventoInJob(realm, eventApi, e1.getAttivitaId());

            throw new Exception();
        } catch (SyncGruppoMismatchException e2) {
            if (realm.isInTransaction())
                realm.cancelTransaction();
            resetInElaborazione(realm);
            AnalitycsProvider.getInstance().logCustom(null,"syncGroup");
            // Sync totale attività
            SyncTotal.syncGruppiInJob(realm, groupApi, e2.getGruppoId());

            throw new Exception();
        } catch (Exception ex) {
            ex.printStackTrace();
            if (realm.isInTransaction())
                realm.cancelTransaction();
            resetInElaborazione(realm);

            throw new Exception();
        } finally {
            realm.close();
        }

        if (mSyncOperation.createNotifications(notifications)) {
//            notifications.createNotifications(App.getContext());
            notifications.callNotification();
        }
    }

    private void resetInElaborazione(Realm realm) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                mUnit.setInElaborazione(false);
            }
        });
    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {
        // Gestisco il caso in cui qualcosa va in errore
        if (cancelReason == CancelReason.REACHED_RETRY_LIMIT) {
            Realm realm = App.getRealm();
            //noinspection TryFinallyCanBeTryWithResources
            try {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(@NonNull Realm realm) {
                        // Rimuovo la sync unit
                        SyncData syncDataToDelete = new SyncDataDao(realm).getSyncData(mUnitId);
                        syncDataToDelete.deleteFromRealm();
                    }
                });
            } finally {
                realm.close();
            }
        }
    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        return null;
    }

    @Override
    protected int getRetryLimit() {
        return 10;
    }
}
