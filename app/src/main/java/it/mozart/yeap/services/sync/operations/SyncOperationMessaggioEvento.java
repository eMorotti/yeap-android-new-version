package it.mozart.yeap.services.sync.operations;

import com.google.gson.Gson;

import java.util.Date;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.common.MediaInfo;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Event;
import it.mozart.yeap.realm.Message;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.dao.EventDao;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.services.sync.exceptions.SyncEventoMismatchException;
import it.mozart.yeap.services.sync.models.SyncMessaggioData;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.Utils;

public class SyncOperationMessaggioEvento extends SyncOperation {

    private MessaggioEventoModel mMessaggioEventoModel;

    public SyncOperationMessaggioEvento(SyncData syncData) {
        super(syncData);
    }

    @Override
    public void parseModel() {
        mMessaggioEventoModel = new Gson().fromJson(mSyncDataContent, MessaggioEventoModel.class);
    }

    @Override
    public void doJob(Realm realm) throws Exception {
        if (mMessaggioEventoModel == null)
            throw new Exception("Model is null");

        EventDao eventoDao = new EventDao(realm);
        UserDao utenteDao = new UserDao(realm);
        MessageDao messaggioDao = new MessageDao(realm);

        // Controllo se l'attività è presente
        final Event evento = eventoDao.getEvent(mMessaggioEventoModel.activityUuid);
        if (evento == null) {
            throw new SyncEventoMismatchException(mMessaggioEventoModel.activityUuid);
        }

        if (mMessaggioEventoModel.msg == null || mMessaggioEventoModel.msg.getUserUuid() == null) {
            return;
        }

        uuid = evento.getUuid();

        // Controllo se il messaggio è già presente
        Message messaggio = messaggioDao.getMessage(mMessaggioEventoModel.msg.getUuid());
        if (messaggio != null) {
            messaggio.setThumbData(mMessaggioEventoModel.msg.getThumbData());
            messaggio.setMediaWidth(mMessaggioEventoModel.msg.getImageWidth());
            messaggio.setMediaHeight(mMessaggioEventoModel.msg.getImageHeight());
            return;
        }

        // Media presente
        MediaInfo mediaInfo = null;
        if (mMessaggioEventoModel.msg.getMediaUrl() != null) {
            mediaInfo = new MediaInfo(mMessaggioEventoModel.msg.getMediaUrl(), 0, mMessaggioEventoModel.msg.getThumbData(), true);
            mediaInfo.setWidth(mMessaggioEventoModel.msg.getImageWidth());
            mediaInfo.setHeight(mMessaggioEventoModel.msg.getImageHeight());
        }

        String url = Utils.findUrlInText(mMessaggioEventoModel.msg.getText());

        User utente = utenteDao.getUser(mMessaggioEventoModel.msg.getUserUuid());
        if (utente != null) {
            // Creo il messaggio
            messaggio = messaggioDao.createMessage(
                    mMessaggioEventoModel.msg.getUuid(),
                    mMessaggioEventoModel.msg.getText(),
                    utente,
                    mediaInfo,
                    Constants.MESSAGGIO_TYPOLOGY_CHAT_EVENTO,
                    evento,
                    (App.isEventVisible(mMessaggioEventoModel.activityUuid) && App.isEventPageVisible(Constants.EVENT_PAGE_CHAT))
                            || mMessaggioEventoModel.msg.getUserUuid().equals(AccountProvider.getInstance().getAccountId()),
                    true);
            if (url != null) {
                messaggio.setUrl(url);
            } else {
                messaggio.setUrlChecked(true);
            }
            messaggio.setCreatedAt(mMessaggioEventoModel.msg.getCreatedAt());
            messaggio.setCreatedAtForVisualization(new Date().getTime());

            // Invio notifiche
            if (App.isEventVisible(mMessaggioEventoModel.activityUuid) && App.isEventPageVisible(Constants.EVENT_PAGE_CHAT)) {
                messaggioDao.markChatAsReadAsync(true, evento.getUuid());
            }
        }
    }

    @Override
    public boolean createNotifications(NotificationsHelper notifications) {
        if ((!App.isEventVisible(mMessaggioEventoModel.activityUuid) || !App.isEventPageVisible(Constants.EVENT_PAGE_CHAT))
                && !mMessaggioEventoModel.msg.getUserUuid().equals(AccountProvider.getInstance().getAccountId())) {
            notifications.setInfo(Constants.TYPE_EVENTO, uuid);
        }
        return (!App.isEventVisible(mMessaggioEventoModel.activityUuid) || App.isEventPageVisible(Constants.EVENT_PAGE_CHAT))
                && !mMessaggioEventoModel.msg.getUserUuid().equals(AccountProvider.getInstance().getAccountId());
    }

    private static class MessaggioEventoModel {
        SyncMessaggioData msg;
        String activityUuid;
    }
}
