package it.mozart.yeap.services.sync.operations;

import com.google.gson.Gson;

import io.realm.Realm;
import it.mozart.yeap.App;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.realm.Group;
import it.mozart.yeap.realm.SyncData;
import it.mozart.yeap.realm.User;
import it.mozart.yeap.realm.dao.GroupDao;
import it.mozart.yeap.realm.dao.MessageDao;
import it.mozart.yeap.realm.dao.UserDao;
import it.mozart.yeap.services.sync.NotificationsHelper;
import it.mozart.yeap.services.sync.exceptions.SyncGruppoMismatchException;
import it.mozart.yeap.utility.Constants;

public class SyncOperationUscitaPartecipanteGruppo extends SyncOperation {

    private UscitaPartecipanteGruppoModel mUscitaPartecipanteModel;

    public SyncOperationUscitaPartecipanteGruppo(SyncData syncData) {
        super(syncData);
    }

    @Override
    public void parseModel() {
        mUscitaPartecipanteModel = new Gson().fromJson(mSyncDataContent, UscitaPartecipanteGruppoModel.class);
    }

    @Override
    public void doJob(Realm realm) throws Exception {
        if (mUscitaPartecipanteModel == null)
            throw new Exception("Model is null");

        GroupDao gruppoDao = new GroupDao(realm);
        UserDao utenteDao = new UserDao(realm);
        MessageDao messaggioDao = new MessageDao(realm);

        Group gruppo = gruppoDao.getGroup(mUscitaPartecipanteModel.groupUuid);
        if (gruppo == null) {
            throw new SyncGruppoMismatchException(mUscitaPartecipanteModel.groupUuid);
        }

        if (gruppo.getVersion() < mUscitaPartecipanteModel.groupVersion - 1 || mUscitaPartecipanteModel.userUuid == null) {
            throw new SyncGruppoMismatchException(mUscitaPartecipanteModel.groupUuid);
        }

        gruppo.setVersion(mUscitaPartecipanteModel.groupVersion);

        User utente = utenteDao.getUser(mUscitaPartecipanteModel.userUuid);
        if (mUscitaPartecipanteModel.adminUuid != null) {
            User admin = utenteDao.getUser(mUscitaPartecipanteModel.adminUuid);
            gruppo.setAdmin(admin);
            messaggioDao.createSystemMessageGroupNewAdmin(admin, gruppo).setCreatedAt(mUscitaPartecipanteModel.timestamp - 1);
        }
        if (gruppo.getGuests().contains(utente)) {
            gruppo.getGuests().remove(utente);
        } else {
            return;
        }

        uuid = gruppo.getUuid();

        if (utente == null) {
            throw new SyncGruppoMismatchException(mUscitaPartecipanteModel.groupUuid);
        }
        letto = (App.isGroupVisible(mUscitaPartecipanteModel.groupUuid) && App.isGroupPageVisible(Constants.GROUP_PAGE_CHAT))
                || utente.getUuid().equals(AccountProvider.getInstance().getAccountId());

        if (utente.getUuid().equals(AccountProvider.getInstance().getAccountId())) {
            gruppo.setMyselfPresent(false);
        }

        // Creo messaggio
        messaggioDao.createSystemMessageGroupUserLeft(gruppo, utente, letto).setCreatedAt(mUscitaPartecipanteModel.timestamp);
    }

    @Override
    public boolean createNotifications(NotificationsHelper notifications) {
        // Invio notifiche
        if (!letto) {
            notifications.setInfo(Constants.TYPE_GRUPPO, uuid);
        }
        return !letto;
    }

    private static class UscitaPartecipanteGruppoModel {
        String groupUuid;
        int groupVersion;
        long timestamp;
        String userUuid;
        String adminUuid;
    }
}
