package it.mozart.yeap;

import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.graphics.Color;
import android.graphics.drawable.Icon;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDexApplication;

import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.config.Configuration;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;

import org.codejargon.feather.Feather;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import io.fabric.sdk.android.Fabric;
import io.realm.DynamicRealm;
import io.realm.DynamicRealmObject;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmMigration;
import io.realm.RealmSchema;
import it.mozart.yeap.di.AppModule;
import it.mozart.yeap.di.DataModule;
import it.mozart.yeap.di.NetworkModule;
import it.mozart.yeap.helpers.AccountProvider;
import it.mozart.yeap.ui.events.creation.Act_EventoCrea;
import it.mozart.yeap.ui.groups.creation.Act_GruppoCrea;
import it.mozart.yeap.utility.Constants;
import it.mozart.yeap.utility.Prefs;
import it.mozart.yeap.utility.Utils;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class App extends MultiDexApplication {

    private static App instance;
    private static Feather feather;
    private static JobManager jobManager;
    private static FirebaseJobDispatcher jobDispatcher;
    private static RealmConfiguration realmConfiguration;

    private static String event;
    private static boolean eventVisible;
    private static int eventPageVisible;
    private static String group;
    private static boolean groupVisible;
    private static int groupPageVisible;

    private static boolean syncing;

    public App() {
        instance = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (!BuildConfig.DEBUG) {
            Fabric.with(this, new Crashlytics(), new Answers());
        }
        initFeather();

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/OpenSans-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());

        new Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true)
                .setDefaultIntValue(-1)
                .setDefaultBooleanValue(false)
                .setDefaultStringValue(null)
                .build();

        jobManager = new JobManager(new Configuration.Builder(this)
                .minConsumerCount(1)
                .maxConsumerCount(20)
                .build());

        jobDispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));

        resetEventVisibility();
        resetGroupVisibility();

        setSyncing(false);

        if (Prefs.getBoolean(Constants.LOGIN_EFFECTUATED, true)) {
            initRealm(Prefs.getString(Constants.PREF_PROFILE_ID));
        }

        if (Utils.isNougatMR1()) {
            showShortcut(this);
        }

        if (Utils.isOreo()) {
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            NotificationChannel channel = new NotificationChannel("yeap!", "Yeap!", NotificationManager.IMPORTANCE_DEFAULT);
            channel.enableLights(true);
            channel.setLightColor(Color.BLUE);
            channel.enableVibration(true);
            notificationManager.createNotificationChannel(channel);

            NotificationChannel channelLow = new NotificationChannel("yeap!low", "Yeap!", NotificationManager.IMPORTANCE_NONE);
            channelLow.enableLights(false);
            channelLow.enableVibration(false);
            notificationManager.createNotificationChannel(channelLow);
        }
    }

    public static void initFeather() {
        feather = Feather.with(new AppModule(), new DataModule(), new NetworkModule());
    }

    public static Feather feather() {
        return feather;
    }

    public static JobManager jobManager() {
        return jobManager;
    }

    public static FirebaseJobDispatcher jobDispatcher() {
        return jobDispatcher;
    }

    public static Context getContext() {
        return instance.getApplicationContext();
    }

    public static void updateEventVisibility(String uuid, boolean visible) {
        event = uuid;
        eventVisible = visible;
    }

    public static void updateEventPage(int page) {
        eventPageVisible = page;
    }

    public static boolean isEventVisible(String uuid) {
        return uuid.equals(event) && eventVisible;
    }

    public static boolean isEventPageVisible(int type) {
        return eventPageVisible == type;
    }

    public static void resetEventVisibility() {
        event = "";
        eventVisible = false;
        eventPageVisible = 0;
    }

    public static String getEventUuid() {
        return event;
    }

    public static void updateGroupVisibility(String uuid, boolean visible) {
        group = uuid;
        groupVisible = visible;
    }

    public static void updateGroupPage(int page) {
        groupPageVisible = page;
    }

    public static boolean isGroupVisible(String uuid) {
        return uuid.equals(group) && groupVisible;
    }

    public static boolean isGroupPageVisible(int type) {
        return groupPageVisible == type;
    }

    public static void resetGroupVisibility() {
        group = "";
        groupVisible = false;
        groupPageVisible = 0;
    }

    public static String getGroupUuid() {
        return group;
    }

    public static void setSyncing(boolean value) {
        syncing = value;
    }

    public static boolean isSyncing() {
        return syncing;
    }

    public static Realm getRealm() {
        return Realm.getInstance(realmConfiguration);
    }

    public static void initRealm(String utenteId) {
        Realm.init(getContext());
        realmConfiguration = new RealmConfiguration.Builder()
                .name(String.format(getContext().getString(R.string.database_name), utenteId))
                .schemaVersion(4)
                .migration(migrationModule())
                .build();
    }

    public static RealmMigration migrationModule() {
        return new RealmMigration() {
            @Override
            public void migrate(@NonNull DynamicRealm realm, long oldVersion, long newVersion) {
                RealmSchema schema = realm.getSchema();
                if (oldVersion < 2) {
                    schema.get("Event")
                            .addField("whenPollUuid", String.class)
                            .addField("whenPollEnabled", boolean.class)
                            .addField("wherePollUuid", String.class)
                            .addField("wherePollEnabled", boolean.class)
                            .addField("numInterestYes", long.class)
                            .addField("numInterestNo", long.class)
                            .removeField("numNewProposals");
                    schema.get("EventInfo")
                            .addRealmObjectField("myVoteInterest", schema.get("Vote"));
                    schema.get("Poll")
                            .addField("usersCanAddItems", boolean.class)
                            .addField("type", int.class).addIndex("type");
                    schema.get("PollItem")
                            .addField("where", String.class)
                            .addField("placeId", String.class)
                            .addField("lat", double.class)
                            .addField("lng", double.class)
                            .addField("fromDate", String.class)
                            .addField("fromHour", String.class)
                            .addField("toDate", String.class)
                            .addField("toHour", String.class);
                    schema.get("Vote")
                            .removeField("proposalUuid")
                            .addField("voteType", int.class).addIndex("voteType");
                    schema.get("SyncData")
                            .addField("v", int.class);
                    schema.get("Message")
                            .removeField("proposal");
                    schema.remove("Proposal");

                    for (DynamicRealmObject event : realm.where("Event").findAll()) {
                        DynamicRealmObject eventInfo = event.getObject("event");
                        if (eventInfo == null) {
                            continue;
                        }

                        if (eventInfo.getString("where") != null) {
                            String uuid = UUID.randomUUID().toString();
                            DynamicRealmObject wherePoll = realm.createObject("Poll", uuid);
                            wherePoll.set("eventUuid", event.getString("uuid"));
                            wherePoll.set("question", getContext().getString(R.string.system_question_where));
                            wherePoll.set("creator", event.getObject("creator"));
                            wherePoll.set("creatorUuid", event.getString("creatorUuid"));
                            wherePoll.set("createdAt", new Date().getTime());
                            wherePoll.set("type", Constants.POLL_TYPE_WHERE);

                            event.set("wherePollUuid", uuid);
                            event.set("wherePollEnabled", true);
                        }

                        if (eventInfo.getString("fromDate") != null || eventInfo.getString("toDate") != null
                                || eventInfo.getString("fromHour") != null || eventInfo.getString("toHour") != null) {
                            String uuid = UUID.randomUUID().toString();
                            DynamicRealmObject whenPoll = realm.createObject("Poll", uuid);
                            whenPoll.set("eventUuid", event.getString("uuid"));
                            whenPoll.set("question", getContext().getString(R.string.system_question_when));
                            whenPoll.set("creator", event.getObject("creator"));
                            whenPoll.set("creatorUuid", event.getString("creatorUuid"));
                            whenPoll.set("createdAt", new Date().getTime());
                            whenPoll.set("type", Constants.POLL_TYPE_WHEN);

                            event.set("whenPollUuid", uuid);
                            event.set("whenPollEnabled", true);
                        }
                    }
                }
                if (oldVersion < 4) {
                    schema.get("Message")
                            .addField("urlChecked", boolean.class)
                            .addField("urlInfo", String.class)
                            .addField("url", String.class);
                }


//                    DynamicRealmObject whenPoll = null;
//                    DynamicRealmObject wherePoll = null;
//                    for (DynamicRealmObject event : realm.where("Event").findAll()) {
//                        RealmResults<DynamicRealmObject> proposals = realm.where("Proposal")
//                                .equalTo(Proposal.EVENT_ID, event.getString("uuid")).equalTo(Proposal.DELETED, false)
//                                .findAll();
//
//                        for (DynamicRealmObject proposal : proposals) {
//                            if (proposal.getString("where") != null) {
//                                if (wherePoll != null) {
//
//                                } else {
//                                    String uuid = UUID.randomUUID().toString();
//                                    wherePoll = realm.createObject("Poll");
//                                    wherePoll.set("uuid", uuid);
//                                    wherePoll.set("eventUuid", event.getString("uuid"));
//                                    wherePoll.set("question", "Domanda");
//                                    wherePoll.set("creator", event.getObject("creator"));
//                                    wherePoll.set("creatorUuid", event.getString("creatorUuid"));
//                                    wherePoll.set("createdAt", new Date().getTime());
//
//                                    DynamicRealmObject pollItem = realm.createObject("PollItem");
//                                    pollItem.set("uuid", UUID.randomUUID().toString());
//                                    pollItem.set("pollUuid", uuid);
//                                    pollItem.set("where", proposal.getString("where"));
//                                    pollItem.set("placeId", proposal.getString("placeId"));
//                                    pollItem.set("lat", proposal.getDouble("lat"));
//                                    pollItem.set("lng", proposal.getDouble("lng"));
//                                }
//                            }
//                            if (proposal.getString("fromDate") != null || proposal.getString("toDate") != null
//                                    || proposal.getString("fromHour") != null || proposal.getString("toHour") != null) {
//                                if (whenPoll != null) {
//
//                                } else {
//
//                                }
//                            }
//                        }
//
//                        whenPoll = null;
//                        wherePoll = null;
//                    }
            }

            @Override
            public int hashCode() {
                return 17;
            }

            @Override
            public boolean equals(Object obj) {
                return (obj instanceof RealmMigration);
            }
        };
    }


    @TargetApi(Build.VERSION_CODES.N_MR1)
    private void showShortcut(final Context context) {
        ShortcutManager shortcutManager = context.getSystemService(ShortcutManager.class);
        List<ShortcutInfo> shortcuts = new ArrayList<>();

        if (AccountProvider.getInstance().isLoggedIn()) {
            Intent creaGruppoIntent = new Intent(context, Act_GruppoCrea.class);
            creaGruppoIntent.setAction("createGroup");
            shortcuts.add(new ShortcutInfo.Builder(context, "createGroup")
                    .setShortLabel(getString(R.string.system_nougat_shortcut_crea_gruppo))
                    .setLongLabel(getString(R.string.system_nougat_shortcut_crea_gruppo))
                    .setIcon(Icon.createWithResource(context, R.drawable.avatar_gruppo))
                    .setIntent(creaGruppoIntent)
                    .build());

            Intent creaEventoIntent = new Intent(context, Act_EventoCrea.class);
            creaEventoIntent.setAction("createEvent");
            shortcuts.add(new ShortcutInfo.Builder(context, "createEvent")
                    .setShortLabel(getString(R.string.system_nougat_shortcut_crea_attivita))
                    .setLongLabel(getString(R.string.system_nougat_shortcut_crea_attivita))
                    .setIcon(Icon.createWithResource(context, R.drawable.avatar_attivita))
                    .setIntent(creaEventoIntent)
                    .build());
        }

        shortcutManager.setDynamicShortcuts(shortcuts);
    }
}
