package com.lapism.searchview;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;


public class SearchItem implements Parcelable {

    public static final Creator<SearchItem> CREATOR = new Creator<SearchItem>() {
        public SearchItem createFromParcel(Parcel source) {
            return new SearchItem(source);
        }

        public SearchItem[] newArray(int size) {
            return new SearchItem[size];
        }
    };
    private int icon;
    private CharSequence text;
    private CharSequence fullText;
    private CharSequence secondaryText;
    private String placeId;

    @SuppressWarnings("WeakerAccess")
    public SearchItem() {
    }

    public SearchItem(CharSequence text) {
        this(R.drawable.search_ic_search_black_24dp, text);
    }

    @SuppressWarnings("WeakerAccess")
    public SearchItem(int icon, CharSequence text) {
        this.icon = icon;
        this.text = text;
    }

    @SuppressWarnings("WeakerAccess")
    public SearchItem(CharSequence fullText, CharSequence text, CharSequence secondaryText, String placeId) {
        this.icon = R.drawable.search_ic_search_black_24dp;
        this.text = text;
        this.fullText = fullText;
        this.secondaryText = secondaryText;
        this.placeId = placeId;
    }

    @SuppressWarnings("WeakerAccess")
    public SearchItem(Parcel in) {
        this.icon = in.readInt();
        this.text = in.readParcelable(CharSequence.class.getClassLoader());
        this.fullText = in.readParcelable(CharSequence.class.getClassLoader());
        this.secondaryText = in.readParcelable(CharSequence.class.getClassLoader());
        this.placeId = in.readString();
    }

    @SuppressWarnings("WeakerAccess")
    public int get_icon() {
        return this.icon;
    }

    @SuppressWarnings("WeakerAccess")
    public void set_icon(int icon) {
        this.icon = icon;
    }

    @SuppressWarnings("WeakerAccess")
    public CharSequence get_text() {
        return this.text;
    }

    @SuppressWarnings("WeakerAccess")
    public void set_text(CharSequence text) {
        this.text = text;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public CharSequence getSecondaryText() {
        return secondaryText;
    }

    public void setSecondaryText(CharSequence secondaryText) {
        this.secondaryText = secondaryText;
    }

    public CharSequence getFullText() {
        return fullText;
    }

    public void setFullText(CharSequence fullText) {
        this.fullText = fullText;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.icon);
        TextUtils.writeToParcel(this.text, dest, flags); // dest.writeValue(this.text);
        TextUtils.writeToParcel(this.fullText, dest, flags);
        TextUtils.writeToParcel(this.secondaryText, dest, flags);
        dest.writeString(this.placeId);
    }

}